package com.gennext.offlinegst.user.dashboard;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.gennext.offlinegst.MainActivity;
import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.model.user.DashboardModel;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.user.Dashboard;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.RequestBuilder;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.DefaultValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;

import java.util.ArrayList;

/**
 * Created by Admin on 7/4/2017.
 */

public class SalePurchasePieChart extends CompactFragment implements View.OnClickListener {

    protected Typeface mTfRegular;
    protected Typeface mTfLight;

    public static final int[] PIE_CHART_SALE = {
            Color.rgb(160, 212, 104), Color.rgb(255, 206, 85)
    };

    public static final int[] PIE_CHART_DEBTOR = {
            Color.rgb(144, 164, 174), Color.rgb(172, 146, 235)
    };

    public static final int[] PIE_CHART_TAX = {
             Color.rgb(143, 206, 242), Color.rgb(189, 189, 189)
    };

    protected String[] mSaleAndPurchase = new String[]{"Sale", "Purchase"};
    protected String[] mDebtorsAndCreditors = new String[]{"Debtors", "Creditors"};
    protected String[] mTaxPayable = new String[]{"Tax Payable", "Input Tax Credit"};


    private AssignTask assignTask;
    private PieChart mChartSalePurchase;
    private PieChart mChartDebtorCreditor;
    private PieChart mChartTaxPayable;
    private LinearLayout llSalePurchaseReport, llTaxPayableReport, llDebtorCreditorReport, llSalePurchaseChart, llTaxPayableChart, llDebtorCreditorChart;
    private Button btnTaxPayableTrans, btnDebtorCreditorTrans, btnSalePurchaseTrans;
    private OnChartValueSelectedListener buyerListener, sellerListener;
    private Dashboard dashboard;
    private String mFilterType;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static Fragment newInstance(Dashboard dashboard) {
        SalePurchasePieChart fragment = new SalePurchasePieChart();
        fragment.dashboard = dashboard;
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_dashboard_pie_chart, container, false);
        screenAnalytics(getContext(), AppUser.getUserId(getContext()), "SalePurchasePieChart");
        initUi(v);
        checkVisibility();
        initPiChartSalePurchase(v);
        initPiChartDebtorCreditor(v);
        initPiChartTaxPayable(v);
        executeTask();
        return v;
    }

    public void checkVisibility() {
        Context mContext = getContext();
        if (AppConfig.getSalePurchaseChart(mContext)) {
            llSalePurchaseChart.setVisibility(View.VISIBLE);
        } else {
            llSalePurchaseChart.setVisibility(View.GONE);
        }
        if (AppConfig.getDebtorCreditorChart(mContext)) {
            llDebtorCreditorChart.setVisibility(View.VISIBLE);
        } else {
            llDebtorCreditorChart.setVisibility(View.GONE);
        }
        if (AppConfig.getTaxPayableChart(mContext)) {
            boolean isCompositionScheme = AppUser.getCompositionScheme(getContext());
            if(isCompositionScheme){
                llTaxPayableChart.setVisibility(View.GONE);
            }else{
                llTaxPayableChart.setVisibility(View.VISIBLE);
            }
        } else {
            llTaxPayableChart.setVisibility(View.GONE);
        }

    }

    private void initUi(View v) {
        llSalePurchaseChart = (LinearLayout) v.findViewById(R.id.ll_sale_purchase);
        llDebtorCreditorChart = (LinearLayout) v.findViewById(R.id.ll_debtor_credtor);
        llTaxPayableChart = (LinearLayout) v.findViewById(R.id.ll_tax_payable_main);

        llSalePurchaseReport = (LinearLayout) v.findViewById(R.id.ll_seller_report);
        llDebtorCreditorReport = (LinearLayout) v.findViewById(R.id.ll_buyer_report);
        llTaxPayableReport = (LinearLayout) v.findViewById(R.id.ll_tax_payable);

        btnSalePurchaseTrans = (Button) v.findViewById(R.id.btn_seller_trans);
        btnDebtorCreditorTrans = (Button) v.findViewById(R.id.btn_buyer_trans);
        btnTaxPayableTrans = (Button) v.findViewById(R.id.btn_tax_payable);

        btnDebtorCreditorTrans.setOnClickListener(this);
        btnSalePurchaseTrans.setOnClickListener(this);
        btnTaxPayableTrans.setOnClickListener(this);

        buyerListener = new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                if (e == null)
                    return;
                if (h.getX() == 0.0) {
//                    addFragment(DebtorCreditorPaidReceipt.newInstance(), "buyerPaidReceipt");
                } else if (h.getX() == 1.0) {
//                    addFragment(DebtorCreditorUnpaidReceipt.newInstance(),"buyerUnpaidReceipt");
                } else if (h.getX() == 2.0) {
//                    addFragment(DebtorCreditorOverPaidReceipt.newInstance(),"buyerOverPaidReceipt");
                }
            }

            @Override
            public void onNothingSelected() {

            }
        };
        sellerListener = new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                if (e == null)
                    return;
                if (h.getX() == 0.0) {
//                    addFragment(SellPaidReceipt.newInstance(), "sellPaidReceipt");

                } else if (h.getX() == 1.0) {
//                    addFragment(SellUnPaidReceipt.newInstance(), "sellUnPaidReceipt");

                } else if (h.getX() == 2.0) {
//                    addFragment(SellOverPaidReceipt.newInstance(), "sellOverPaidReceipt");

                }
//                Log.i("VAL SELECTED",
//                        "ValueY: " + e.getY() + ", index: " + h.getX()
//                                + ", DataSet index: " + h.getDataSetIndex());
            }

            @Override
            public void onNothingSelected() {

            }
        };
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_buyer_trans:
                refreshTask();
                break;
            case R.id.btn_seller_trans:
                refreshTask();
                break;
            case R.id.btn_tax_payable:
                refreshTask();
                break;
        }
    }


    public void executeTask() {
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.DASHBOARD);
    }

    public void refreshTask() {
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.DASHBOARD);

    }


    private void initPiChartSalePurchase(View v) {
        mChartSalePurchase = (PieChart) v.findViewById(R.id.piechart_sale_purchase);
        mChartSalePurchase.setUsePercentValues(false);
        mChartSalePurchase.getDescription().setEnabled(false);
        mChartSalePurchase.setExtraOffsets(5, 10, 5, 5);

        mChartSalePurchase.setDragDecelerationFrictionCoef(0.95f);

        mChartSalePurchase.setCenterTextTypeface(mTfLight);
        mChartSalePurchase.setCenterText(generateCenterSpannableText(""));

        mChartSalePurchase.setDrawHoleEnabled(true);
        mChartSalePurchase.setHoleColor(Color.WHITE);

        mChartSalePurchase.setTransparentCircleColor(Color.WHITE);
        mChartSalePurchase.setTransparentCircleAlpha(110);

        mChartSalePurchase.setHoleRadius(10f);
        mChartSalePurchase.setTransparentCircleRadius(15f);

        mChartSalePurchase.setDrawCenterText(false);

        mChartSalePurchase.setRotationAngle(0);
        // enable rotation of the chart by touch
        mChartSalePurchase.setRotationEnabled(true);
        mChartSalePurchase.setHighlightPerTapEnabled(true);

        // mChartSalePurchase.setUnit(" €");
        // mChartSalePurchase.setDrawUnitsInChart(true);

        // add a selection listener
        mChartSalePurchase.setOnChartValueSelectedListener(sellerListener);


        mChartSalePurchase.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // mChartSalePurchase.spin(2000, 0, 360);


        Legend l = mChartSalePurchase.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);
        l.setEnabled(true); //show ot hide legend from right side
        // entry label styling
        mChartSalePurchase.setEntryLabelColor(Color.BLACK);
        mChartSalePurchase.setEntryLabelTypeface(mTfRegular);
        mChartSalePurchase.setEntryLabelTextSize(12f);
    }

    private void initPiChartDebtorCreditor(View v) {
        mChartDebtorCreditor = (PieChart) v.findViewById(R.id.piechart_debtor_credtor);
        mChartDebtorCreditor.setUsePercentValues(false);
        mChartDebtorCreditor.getDescription().setEnabled(false);
        mChartDebtorCreditor.setExtraOffsets(5, 10, 5, 5);

        mChartDebtorCreditor.setDragDecelerationFrictionCoef(0.95f);

//        mChartDebtorCreditor.setCenterTextTypeface(mTfLight);
//        mChartDebtorCreditor.setCenterText(generateCenterSpannableText("Purchase"));

        mChartDebtorCreditor.setDrawHoleEnabled(true);
        mChartDebtorCreditor.setHoleColor(Color.WHITE);

        mChartDebtorCreditor.setTransparentCircleColor(Color.WHITE);
        mChartDebtorCreditor.setTransparentCircleAlpha(110);

        mChartDebtorCreditor.setHoleRadius(10f);
        mChartDebtorCreditor.setTransparentCircleRadius(15f);

        mChartDebtorCreditor.setDrawCenterText(false);

        mChartDebtorCreditor.setRotationAngle(0);
        // enable rotation of the chart by touch
        mChartDebtorCreditor.setRotationEnabled(true);
        mChartDebtorCreditor.setHighlightPerTapEnabled(true);

        // mChartDebtorCreditor.setUnit(" €");
        // mChartDebtorCreditor.setDrawUnitsInChart(true);

        // add a selection listener

        mChartDebtorCreditor.setOnChartValueSelectedListener(buyerListener);


        mChartDebtorCreditor.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // mChartDebtorCreditor.spin(2000, 0, 360);


        Legend l = mChartDebtorCreditor.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);
        l.setEnabled(true); //show ot hide legend from right side
        // entry label styling
        mChartDebtorCreditor.setEntryLabelColor(Color.BLACK);
        mChartDebtorCreditor.setEntryLabelTypeface(mTfRegular);
        mChartDebtorCreditor.setEntryLabelTextSize(12f);
    }


    private void initPiChartTaxPayable(View v) {
        mChartTaxPayable = (PieChart) v.findViewById(R.id.piechart_tax_payable);
        mChartTaxPayable.setUsePercentValues(false);
        mChartTaxPayable.getDescription().setEnabled(false);
        mChartTaxPayable.setExtraOffsets(5, 10, 5, 5);

        mChartTaxPayable.setDragDecelerationFrictionCoef(0.95f);

//        mChartTaxPayable.setCenterTextTypeface(mTfLight);
//        mChartTaxPayable.setCenterText(generateCenterSpannableText("Purchase"));

        mChartTaxPayable.setDrawHoleEnabled(true);
        mChartTaxPayable.setHoleColor(Color.WHITE);

        mChartTaxPayable.setTransparentCircleColor(Color.WHITE);
        mChartTaxPayable.setTransparentCircleAlpha(110);

        mChartTaxPayable.setHoleRadius(10f);
        mChartTaxPayable.setTransparentCircleRadius(15f);

        mChartTaxPayable.setDrawCenterText(false);

        mChartTaxPayable.setRotationAngle(0);
        // enable rotation of the chart by touch
        mChartTaxPayable.setRotationEnabled(true);
        mChartTaxPayable.setHighlightPerTapEnabled(true);

        // mChartTaxPayable.setUnit(" €");
        // mChartTaxPayable.setDrawUnitsInChart(true);

        // add a selection listener

        mChartTaxPayable.setOnChartValueSelectedListener(buyerListener);


        mChartTaxPayable.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // mChartTaxPayable.spin(2000, 0, 360);


        Legend l = mChartTaxPayable.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);
        l.setEnabled(true); //show ot hide legend from right side
        // entry label styling
        mChartTaxPayable.setEntryLabelColor(Color.BLACK);
        mChartTaxPayable.setEntryLabelTypeface(mTfRegular);
        mChartTaxPayable.setEntryLabelTextSize(12f);
    }


    private SpannableString generateCenterSpannableText(String title) {

        SpannableString s = new SpannableString(title);
//        SpannableString s = new SpannableString("Paid Amt.\nRs 5000");
//        s.setSpan(new RelativeSizeSpan(1.2f), 0, 9, 0);
//        s.setSpan(new StyleSpan(Typeface.NORMAL), 9, s.length(), 0);
        s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), 0, s.length(), 0);
        return s;
    }

    private class AssignTask extends AsyncTask<String, Void, DashboardModel> {
        private Context context;

        private void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        private void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        private AssignTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            btnDebtorCreditorTrans.setVisibility(View.GONE);
            btnTaxPayableTrans.setVisibility(View.GONE);
            btnSalePurchaseTrans.setVisibility(View.GONE);
        }

        @Override
        protected DashboardModel doInBackground(String... urls) {
            String userId = AppUser.getUserId(context);
            String profileId = AppUser.getProfileId(context);
            mFilterType = Const.YTD;
            String response = ApiCall.POST(urls[0], RequestBuilder.DefaultType(userId, profileId, mFilterType));
            return JsonParser.getDashboardDetail(context, response, userId, profileId);
        }


        @Override
        protected void onPostExecute(DashboardModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                if (result != null) {
                    if (result.getOutput().equals(Const.SUCCESS)) {
                        if (!TextUtils.isEmpty(result.getBusinessType())) {
                            String businessType = AppUser.getBusinessType(context);
                            if (result.getBusinessType().toLowerCase().equals(businessType)) {
                                llSalePurchaseReport.setVisibility(View.VISIBLE);
                                llDebtorCreditorReport.setVisibility(View.VISIBLE);
                                llTaxPayableReport.setVisibility(View.VISIBLE);
                                AppUser.setPackageStatus(context,result.getStatus());
                                AppUser.setPackageMessage(context,result.getMessage());
                                AppUser.setSignature(context,result.getSignature());
                                ((MainActivity)getActivity()).checkAppValidity();
                                setDataSalePurchase(parseFloat(result.getSales()), parseFloat(result.getPurchases()));
                                setDataDebtorCreditor(parseFloat(result.getDebit()), parseFloat(result.getCredit()));
                                setDataTaxPayable(parseFloat(result.getTaxRecieved()), parseFloat(result.getInputTaxCredit()));
                            }else{
                                AppUser.setName(context,result.getCompanyName());
                                AppUser.setBusinessType(context,result.getBusinessType());
                                AppUser.setSignature(context,result.getSignature());
                                startActivity(new Intent(getActivity(), MainActivity.class));
                                getActivity().finish();
                            }
                        }
                    } else {
                        llSalePurchaseReport.setVisibility(View.GONE);
                        llDebtorCreditorReport.setVisibility(View.GONE);
                        llTaxPayableReport.setVisibility(View.GONE);
                        btnDebtorCreditorTrans.setVisibility(View.VISIBLE);
                        btnTaxPayableTrans.setVisibility(View.VISIBLE);
                        btnSalePurchaseTrans.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }

    private void setDataSalePurchase(float sale, float purchase) {

        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();

        entries.add(new PieEntry(sale,
                mSaleAndPurchase[0],
                ContextCompat.getDrawable(getActivity(), R.drawable.ic_about)));

        entries.add(new PieEntry(purchase,
                mSaleAndPurchase[1],
                ContextCompat.getDrawable(getActivity(), R.drawable.ic_about)));


        PieDataSet dataSet = new PieDataSet(entries, "");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : PIE_CHART_SALE)
            colors.add(c);

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new DefaultValueFormatter(0));
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.BLACK);
        data.setValueTypeface(mTfLight);
//        data.setDrawValues(false); // hide entry values from pichart
        mChartSalePurchase.setData(data);
        mChartSalePurchase.setDrawEntryLabels(false);

        // undo all highlights
        mChartSalePurchase.highlightValues(null);

        mChartSalePurchase.invalidate();
    }

    private void setDataDebtorCreditor(float debtor, float creditor) {

        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();

        entries.add(new PieEntry(debtor,
                mDebtorsAndCreditors[0],
                ContextCompat.getDrawable(getActivity(), R.drawable.ic_about)));

        entries.add(new PieEntry(creditor,
                mDebtorsAndCreditors[1],
                ContextCompat.getDrawable(getActivity(), R.drawable.ic_about)));

        PieDataSet dataSet = new PieDataSet(entries, "");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : PIE_CHART_DEBTOR)
            colors.add(c);

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new DefaultValueFormatter(0));
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.BLACK);
        data.setValueTypeface(mTfLight);
//        data.setDrawValues(false); // hide entry values from pichart
        mChartDebtorCreditor.setData(data);
        mChartDebtorCreditor.setDrawEntryLabels(false);

        // undo all highlights
        mChartDebtorCreditor.highlightValues(null);

        mChartDebtorCreditor.invalidate();
    }

    private void setDataTaxPayable(float debtor, float creditor) {

        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();

        entries.add(new PieEntry(debtor,
                mTaxPayable[0],
                ContextCompat.getDrawable(getActivity(), R.drawable.ic_about)));

        entries.add(new PieEntry(creditor,
                mTaxPayable[1],
                ContextCompat.getDrawable(getActivity(), R.drawable.ic_about)));

        PieDataSet dataSet = new PieDataSet(entries, "");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : PIE_CHART_TAX)
            colors.add(c);

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new DefaultValueFormatter(0));
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.BLACK);
        data.setValueTypeface(mTfLight);
//        data.setDrawValues(false); // hide entry values from pichart
        mChartTaxPayable.setData(data);
        mChartTaxPayable.setDrawEntryLabels(false);

        // undo all highlights
        mChartTaxPayable.highlightValues(null);

        mChartTaxPayable.invalidate();
    }


}
