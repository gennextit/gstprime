package com.gennext.offlinegst.model.user;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.panel.ActionMenuInvoice;
import com.gennext.offlinegst.user.services.sale.ServicesSale;
import com.gennext.offlinegst.util.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ReyclerViewHolder>
        implements ActionMenuInvoice.MenuServices {

    private final ServicesSale parentRef;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<InvoiceModel> items;
    private List<InvoiceModel> originalData;
    private ItemFilter mFilter = new ItemFilter();

    public ServicesAdapter(Activity context, ArrayList<InvoiceModel> items,ServicesSale parentRef) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef = parentRef;
        this.items = items;
        this.originalData = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_user_service_format1, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final InvoiceModel item = items.get(position);

        holder.tvInvoiceNumber.setText(item.getInvoiceNumber());
        holder.tvInvDate.setText(item.getInvoiceDate());
        holder.tvCustomerName.setText(item.getCustomerName());
        holder.tvBeforeAmount.setText(Utility.decimalFormatWith2Digit(item.getTotalAmount()));
        holder.tvAfterAmount.setText(Utility.decimalFormatWith2Digit(item.getInvoiceTotal()));
        if(item.getCancelled()!=null&&item.getCancelled().toLowerCase().equals("y")){
            holder.tvStatus.setVisibility(View.VISIBLE);
        }else{
            holder.tvStatus.setVisibility(View.GONE);
        }
        setColor(holder.llSideLine);

//        String produsts="";
//        for (ServiceModel prod:item.getServicesList()){
//            produsts+=prod.getServiceName()+", ";
//        }
//        holder.tvAfterAmount.setText(produsts);

        holder.llSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActionMenuInvoice.newInstance(ServicesAdapter.this,item,position)
                        .show(parentRef.getFragmentManager(),"actionMenu");
            }
        });
    }

    public void setColor(LinearLayout tv) {
        Random rnd = new Random();
        int color= Utility.SIDE_LINE_COLORS[rnd.nextInt(21)];
        tv.setBackgroundColor(color);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onEditClick(DialogFragment dialog, InvoiceModel invoiceServiceModel, int position) {
        parentRef.editServicesSale(invoiceServiceModel);
    }

    @Override
    public void onDeleteClick(DialogFragment dialog, InvoiceModel invoiceServiceModel, int position) {
        parentRef.deleteServicesSale(invoiceServiceModel,position);

    }

    public void deleteItem(int position) {
        items.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position,getItemCount());
    }

    @Override
    public void onCopyClick(DialogFragment dialog, InvoiceModel invoiceServiceModel, int position) {
        parentRef.copyServicesSale(invoiceServiceModel);
    }

    @Override
    public void onViewClick(DialogFragment dialog, InvoiceModel invoiceServiceModel, int position) {
        parentRef.viewInvoice(invoiceServiceModel);
    }

    @Override
    public void onPrintClick(DialogFragment dialog, InvoiceModel invoiceServiceModel, int position) {
        parentRef.printInvoice(invoiceServiceModel);
    }

    @Override
    public void onShareClick(DialogFragment dialog, InvoiceModel invoiceServiceModel, int position) {
        parentRef.shareInvoice(invoiceServiceModel);
    }

    @Override
    public void onSendEmail(DialogFragment dialog, InvoiceModel invoiceServiceModel, int position) {
        parentRef.sendEmail(invoiceServiceModel);
    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout llSlot,llSideLine;
        private TextView tvStatus,tvInvoiceNumber,tvInvDate,tvCustomerName,tvBeforeAmount,tvAfterAmount;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSideLine = (LinearLayout) v.findViewById(R.id.ll_slot);
            llSlot = (LinearLayout) v.findViewById(R.id.layoutSlot);
            tvStatus = (TextView) v.findViewById(R.id.tv_invoice_status);
            tvInvoiceNumber = (TextView) v.findViewById(R.id.tv_slot_0);
            tvInvDate = (TextView) v.findViewById(R.id.tv_slot_1);
            tvCustomerName = (TextView) v.findViewById(R.id.tv_slot_2);
            tvBeforeAmount = (TextView) v.findViewById(R.id.tv_slot_3);
            tvAfterAmount = (TextView) v.findViewById(R.id.tv_slot_4); 
        }
    }

    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<InvoiceModel> list = originalData;

            int count = list.size();
            final ArrayList<InvoiceModel> nlist = new ArrayList<>(count);
            String filterableText;

            if (!filterString.equals("")) {
                for (InvoiceModel model : list) {
                    if (!filterString.equals("")) {
                        filterableText = model.getInvoiceNumber();
                        filterableText+= model.getCustomerName();
                        if (filterableText.toLowerCase().contains(filterString)) {
                            nlist.add(model);
                        }
                    } else {
                        nlist.add(model);
                    }
                }
                results.values = nlist;
                results.count = nlist.size();
            } else {
                results.values = originalData;
                results.count = originalData.size();
            }

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            items = (ArrayList<InvoiceModel>) results.values;
            notifyDataSetChanged();
        }

    }

}

