package com.gennext.offlinegst.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.gennext.offlinegst.MainActivity;
import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.user.reports.InvoicesReport;
import com.gennext.offlinegst.user.reports.PaymentReport;
import com.gennext.offlinegst.user.reports.PurchaseReport;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;

/**
 * Created by Admin on 7/4/2017.
 */

public class Reports extends CompactFragment {

    private DBManager dbManager;
    private RelativeLayout rlContainer1, rlContainer2, rlContainer3;

    public static Fragment newInstance() {
        Reports fragment=new Reports();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.user_reports,container,false);
        ((MainActivity)getActivity()).updateTitle(getString(R.string.report));
        dbManager= DBManager.newIsntance(getActivity());
        screenAnalytics(getContext(), AppUser.getUserId(getContext()),"Reports");
        initUi(v);
        return v;
    }


    private void initUi(View v) {
        rlContainer1 = (RelativeLayout) v.findViewById(R.id.container1);
        rlContainer2 = (RelativeLayout) v.findViewById(R.id.container2);
        rlContainer3 = (RelativeLayout) v.findViewById(R.id.container3);

    }
}
