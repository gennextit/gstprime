package com.gennext.offlinegst.model.user;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.user.invoice.InvoiceManager;
import com.gennext.offlinegst.util.Utility;

import java.util.ArrayList;

public class InvoiceProductAdapter extends RecyclerView.Adapter<InvoiceProductAdapter.ReyclerViewHolder>{

    private final InvoiceManager parentRef;
    private final boolean isCompositeScheme;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<ProductModel> items;

    public InvoiceProductAdapter(Activity context, ArrayList<ProductModel> items, InvoiceManager parentRef, boolean isCompositeScheme) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef = parentRef;
        this.items = items;
        this.isCompositeScheme = isCompositeScheme;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_selected_product, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final ProductModel item = items.get(position);
        float sale = Utility.parseFloat(item.getSalePrice());
        float qun = Utility.parseFloat(item.getQuantity());
        float dis = Utility.parseFloat(item.getDis());
        float cessRate = Utility.parseFloat(item.getCessRate());
        float taxRate = Utility.parseFloat(item.getTaxRate());

        float taxable = (sale * qun)-dis;
        float totalTax = (taxable * (taxRate + cessRate)) / 100;
        String mNetAmt = Utility.decimalFormat(taxable + totalTax,"#.##");

        holder.tvName.setText(item.getProductName());
        holder.etQuantity.setText(Utility.decimalFormat(qun,"#.##"));
        holder.tvNet.setText(context.getString(R.string.rs)+ mNetAmt);
        if(isCompositeScheme){
            holder.tvTaxableTag.setText("Price");
            holder.tvTaxable.setText(context.getString(R.string.rs)+ Utility.decimalFormat(sale,"#.##"));
        }else{
            holder.tvTaxableTag.setText("Taxable");
            holder.tvTaxable.setText(context.getString(R.string.rs)+ Utility.decimalFormat(taxable,"#.##"));
        }

        holder.llSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parentRef.editProduct(item,position);
//                ProductMenu.newInstance(InvoiceProductAdapter.this,item,position)
//                        .show(parentRef.getFragmentManager(),"actionMenu");
            }
        });
        holder.btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                increaseQuantity(item, position);
            }
        });
        holder.btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                decreseQuantity(item, position);
            }
        });
        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                items.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position,getItemCount());
                parentRef.deleteProduct(item,position);
            }
        });

    }



//    private void setProductDetail(TextView tvNet, float qty, float netAmount) {
//        String amount = String.valueOf(Utility.decimalFormat((qty * netAmount),"#.##"));
//        tvNet.setText(context.getString(R.string.rs)+amount);
//    }

    private void increaseQuantity(ProductModel item, int position) {
        float qty =  Utility.parseFloat(item.getQuantity());
        float initQty = item.getInitQuantity();
        if(initQty==0){
            initQty=1;
        }
        qty+=initQty;
        item.setQuantity(String.valueOf(qty));
        items.set(position, item);
        notifyDataSetChanged();
        parentRef.refreshAdapter();
    }

    private void decreseQuantity(ProductModel item, int position) {
        float qty =  Utility.parseFloat(item.getQuantity());
        float initQty = item.getInitQuantity();
        if(initQty==0){
            initQty=1;
        }
        qty-=initQty;
        if (qty==initQty || qty > initQty) {
            item.setQuantity(String.valueOf(qty));
            items.set(position, item);
            notifyDataSetChanged();
            parentRef.refreshAdapter();
        }
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

//    @Override
//    public void onEditClick(DialogFragment dialog, ProductModel itemProduct, int position) {
//        parentRef.editProduct(itemProduct,position);
//    }
//
//    @Override
//    public void onDeleteClick(DialogFragment dialog, ProductModel itemProduct, int position) {
//        items.remove(position);
//        notifyItemRemoved(position);
//        notifyItemRangeChanged(position,getItemCount());
//        parentRef.deleteProduct(itemProduct,position);
//    }
//
//    @Override
//    public void onQuantityClick(DialogFragment dialog, ProductModel itemProduct, int position) {
//        parentRef.quantityProduct(itemProduct,position);
//    }




    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private final ImageButton btnPlus, btnMinus,btnDelete;
        private final TextView etQuantity;
        private LinearLayout llSlot;
        private TextView tvName,tvTaxable,tvNet,tvTaxableTag;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSlot = (LinearLayout) v.findViewById(R.id.layoutSlot);
            btnDelete = (ImageButton) v.findViewById(R.id.btn_delete);
            btnPlus = (ImageButton) v.findViewById(R.id.btn_plus);
            btnMinus = (ImageButton) v.findViewById(R.id.btn_minus);
            tvName = (TextView) v.findViewById(R.id.tv_slot_1);
            tvTaxableTag = (TextView) v.findViewById(R.id.tv_tag_taxable);
            tvTaxable = (TextView) v.findViewById(R.id.tv_slot_2);
            tvNet = (TextView) v.findViewById(R.id.tv_slot_3);
            etQuantity = (TextView) v.findViewById(R.id.tv_slot_4);
        }
    }
}

