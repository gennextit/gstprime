package com.gennext.offlinegst.panel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.model.user.CommonModel;
import com.gennext.offlinegst.model.user.CommonSelectorAdapter;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.JsonParser;

import java.util.ArrayList;


public class TaxCategorySelector extends BottomSheetDialogFragment {
//    private static final String FILE_NAME = "global/taxCategory.json";
    private SelectListener mListener;
    private CommonSelectorAdapter adapter;

    public void onSelectedItem(CommonModel item) {
        dismiss();
        if (mListener != null) {
            mListener.onTaxCategorySelect(TaxCategorySelector.this, item);
        }
    }

    public interface SelectListener {
        void onTaxCategorySelect(DialogFragment dialog, CommonModel selectedTerm);

    }


    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }


    public static TaxCategorySelector newInstance(SelectListener listener) {
        TaxCategorySelector fragment = new TaxCategorySelector();
        fragment.mListener = listener;
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_menu_pay_terms, container, false);
        RecyclerView lvMain = (RecyclerView) v.findViewById(R.id.lv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());
        TextView tvTitle = (TextView) v.findViewById(R.id.et_search);
        tvTitle.setText("Select Tax Category");

//        ArrayList<CommonModel> cModel = JsonParser.parseTranspoartMode(ApiCall.getAssets(getContext(),FILE_NAME));
        ArrayList<CommonModel> cModel = JsonParser.parseTaxCategory(AppUser.getTaxCategories(getContext()));
        if (cModel != null) {
            adapter = new CommonSelectorAdapter(getActivity(), cModel, TaxCategorySelector.this);
            lvMain.setAdapter(adapter);
        }

        return v;
    }


}
