package com.gennext.offlinegst.model.user;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.user.purchas.PurchaseManager;
import com.gennext.offlinegst.user.services.purchase.ServicePurchaseManager;
import com.gennext.offlinegst.util.Utility;

import java.util.ArrayList;

public class PurchaseServiceAdapter extends RecyclerView.Adapter<PurchaseServiceAdapter.ReyclerViewHolder>{

    private final ServicePurchaseManager parentRef;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<ServiceModel> items;

    public PurchaseServiceAdapter(Activity context, ArrayList<ServiceModel> items, ServicePurchaseManager parentRef) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef = parentRef;
        this.items = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_selected_service, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final ServiceModel item = items.get(position);

        holder.tvName.setText(item.getServiceName());
        holder.tvPrice.setText(context.getString(R.string.rs)+item.getSalePrice());
        holder.tvTotal.setText(context.getString(R.string.rs)+item.getNetAmount());

        holder.llSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(parentRef.purchaseType!=ServicePurchaseManager.VIEW_PURCHASE) {
                    parentRef.editProduct(item, position);
                }
            }
        });
        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(parentRef.purchaseType!=ServicePurchaseManager.VIEW_PURCHASE){
                    items.remove(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position,getItemCount());
                    parentRef.deleteProduct(item,position);
                }
            }
        });
    }



    @Override
    public int getItemCount() {
        return items.size();
    }

//    @Override
//    public void onEditClick(DialogFragment dialog, ProductModel itemProduct, int position) {
//        parentRef.editProduct(itemProduct, position);
//    }
//
//    @Override
//    public void onDeleteClick(DialogFragment dialog, ProductModel itemProduct, int position) {
//        items.remove(position);
//        notifyItemRemoved(position);
//        notifyItemRangeChanged(position, getItemCount());
//        parentRef.deleteProduct(itemProduct, position);
//    }
//
//
//    @Override
//    public void onQuantityClick(DialogFragment dialog, ProductModel itemProduct, int position) {
//        parentRef.quantityProduct(itemProduct,position);
//    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private final ImageButton btnDelete;
        private LinearLayout llSlot;
        private TextView tvName,tvPrice,tvTotal;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSlot = (LinearLayout) v.findViewById(R.id.layoutSlot);
            btnDelete = (ImageButton) v.findViewById(R.id.btn_delete);
            tvName = (TextView) v.findViewById(R.id.tv_slot_1);
            tvPrice = (TextView) v.findViewById(R.id.tv_slot_2);
            tvTotal = (TextView) v.findViewById(R.id.tv_slot_3);
        }
    }
}

