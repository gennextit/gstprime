package com.gennext.offlinegst.model.user;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.user.company.ManageProfile;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class ManageCompanyAdapter extends RecyclerView.Adapter<ManageCompanyAdapter.ReyclerViewHolder> {

    private final ManageProfile parentRef;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<CompanyModel> items;

    public ManageCompanyAdapter(Activity context, ArrayList<CompanyModel> items, ManageProfile parentRef) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef = parentRef;
        this.items = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_company_manage, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final CompanyModel item = items.get(position);

        holder.tvCompanyName.setText(item.getCompanyName());
        holder.tvEmail.setText(item.getEmail());
        holder.tvMobile.setText(item.getTelephone());
        if (item.getSelected()) {
            holder.llSlot.setBackgroundResource(R.color.selectedSlot);
        } else {
            holder.llSlot.setBackgroundResource(R.color.deselectSlot);
        }

        String mImagePath = item.getLogo();
        if (!TextUtils.isEmpty(mImagePath)) {
            Glide.with(context)
                    .load(mImagePath)
                    .into(holder.ivProfile);
        } else {
            Glide.with(context)
                    .load(R.drawable.profile)
                    .into(holder.ivProfile);
        }

        holder.llSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectSlot(position);
                parentRef.onSelectedProfile(item);
            }
        });
        holder.ibMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup(holder.ibMenu,item);
            }
        });

    }

    private void selectSlot(int position) {
        for (int i = 0; i < items.size(); i++) {
            CompanyModel item = items.get(i);
            if (i == position) {
                item.setSelected(true);
            } else {
                item.setSelected(false);
            }
            items.set(i, item);
        }
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

//    @Override
//    public void onEditClick(DialogFragment dialog, CompanyModel profile, int position) {
//        parentRef.editProfile(profile);
//    }
//
//    @Override
//    public void onDeleteClick(DialogFragment dialog, CompanyModel profile, int position) {
//        parentRef.deleteProfile(profile);
//    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private final ImageView ivProfile;
        private final ImageButton ibMenu;
        private LinearLayout llSlot;
        private TextView tvCompanyName, tvEmail, tvMobile;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSlot = (LinearLayout) v.findViewById(R.id.layoutSlot);
            ibMenu = (ImageButton) v.findViewById(R.id.btn_action_menu);
            ivProfile = (ImageView) v.findViewById(R.id.iv_profile);
            tvCompanyName = (TextView) v.findViewById(R.id.tv_slot_1);
            tvEmail = (TextView) v.findViewById(R.id.tv_slot_2);
            tvMobile = (TextView) v.findViewById(R.id.tv_slot_3);
        }
    }

    private void showPopup(ImageButton more, final CompanyModel profile) {
        final PopupMenu popup = new PopupMenu(context, more);
        popup.getMenuInflater().inflate(R.menu.profile_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                int i = item.getItemId();
                if (i == R.id.profile_edit) {
                    parentRef.editProfile(profile);
                    return true;
                } else {
                    return onMenuItemClick(item);
                }
            }
        });
        popup.show();

    }

}

