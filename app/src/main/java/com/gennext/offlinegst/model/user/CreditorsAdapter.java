package com.gennext.offlinegst.model.user;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.user.purchas.Creditors;

import java.util.ArrayList;

public class CreditorsAdapter extends RecyclerView.Adapter<CreditorsAdapter.ReyclerViewHolder> {

    private final Creditors parentRef;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<PaymentModel> items;

    public CreditorsAdapter(Activity context, ArrayList<PaymentModel> items,Creditors parentRef) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef = parentRef;
        this.items = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_user_creditors, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final PaymentModel item = items.get(position);

        holder.tvInvoiceNo.setText(item.getInvoiceNo());
        holder.tvPayMode.setText(item.getPaymentMode());
        holder.tvCustName.setText(item.getVendorName());
        holder.tvPayableAmt.setText(item.getPayableAmount());
        holder.tvPaidAmt.setText(item.getPaidAmount());
        if(item.getBalanceAmount().contains("-")){
            holder.tvBalanceTag.setText(context.getString(R.string.advance_balance));
            holder.tvBalance.setText(item.getBalanceAmount().replace("-",""));
        }else {
            holder.tvBalanceTag.setText(context.getString(R.string.borrow_amount));
            holder.tvBalance.setText(item.getBalanceAmount());
        }

        holder.llSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout llSlot;
        private TextView tvInvoiceNo,tvPayMode,tvCustName,tvPayableAmt,tvPaidAmt,tvBalance,tvBalanceTag;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSlot = (LinearLayout) v.findViewById(R.id.layoutSlot);
            tvCustName= (TextView) v.findViewById(R.id.tv_slot_0);
            tvPayMode = (TextView) v.findViewById(R.id.tv_slot_1);
            tvInvoiceNo = (TextView) v.findViewById(R.id.tv_slot_2);
            tvPayableAmt = (TextView) v.findViewById(R.id.tv_slot_3);
            tvPaidAmt = (TextView) v.findViewById(R.id.tv_slot_4);
            tvBalance = (TextView) v.findViewById(R.id.tv_slot_5);
            tvBalanceTag = (TextView) v.findViewById(R.id.tv_slot_6);
        }
    }
}

