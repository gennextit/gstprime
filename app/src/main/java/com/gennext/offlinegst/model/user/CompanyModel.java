package com.gennext.offlinegst.model.user;

import java.util.ArrayList;

/**
 * Created by Admin on 7/29/2017.
 */

public class CompanyModel {
    private String companyId;
    private String companyUserId;
    private String companyName;
    private String telephone;
    private String email;
    private String gstin;
    private String state;
    private String stateCode;
    private String address;
    private String dlNumber;
    private String tAndC;
    private String accName;
    private String ifscCode;
    private String accNumber;
    private String branchAddress;
    private String compositScheme;
    private Boolean selected;
    //updates
    private String businessType;
    private String tin;
    private String turnover;
    private String invoicePrefix;
    private String invoiceStart;
    private String logo;
    private String dateModified;
    private String bankName;
    private String swiftCode;

    private String output;
    private String outputMsg;
    private String outputDB;
    private String outputDBMsg;

    public String getCompositScheme() {
        return compositScheme;
    }

    public void setCompositScheme(String compositScheme) {
        this.compositScheme = compositScheme;
    }

    public String getSwiftCode() {
        return swiftCode;
    }

    public void setSwiftCode(String swiftCode) {
        this.swiftCode = swiftCode;
    }

    private ArrayList<CompanyModel> list;

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getTurnover() {
        return turnover;
    }

    public void setTurnover(String turnover) {
        this.turnover = turnover;
    }

    public String getDateModified() {
        return dateModified;
    }

    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getTin() {
        return tin;
    }

    public void setTin(String tin) {
        this.tin = tin;
    }

    public String getInvoicePrefix() {
        return invoicePrefix;
    }

    public void setInvoicePrefix(String invoicePrefix) {
        this.invoicePrefix = invoicePrefix;
    }

    public String getInvoiceStart() {
        return invoiceStart;
    }

    public void setInvoiceStart(String invoiceStart) {
        this.invoiceStart = invoiceStart;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public ArrayList<CompanyModel> getList() {
        return list;
    }

    public void setList(ArrayList<CompanyModel> list) {
        this.list = list;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public String getOutputDB() {
        return outputDB;
    }

    public void setOutputDB(String outputDB) {
        this.outputDB = outputDB;
    }

    public String getOutputDBMsg() {
        return outputDBMsg;
    }

    public void setOutputDBMsg(String outputDBMsg) {
        this.outputDBMsg = outputDBMsg;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyUserId() {
        return companyUserId;
    }

    public void setCompanyUserId(String companyUserId) {
        this.companyUserId = companyUserId;
    }


    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGstin() {
        return gstin;
    }

    public void setGstin(String gstin) {
        this.gstin = gstin;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDlNumber() {
        return dlNumber;
    }

    public void setDlNumber(String dlNumber) {
        this.dlNumber = dlNumber;
    }

    public String gettAndC() {
        return tAndC;
    }

    public void settAndC(String tAndC) {
        this.tAndC = tAndC;
    }

    public String getAccName() {
        return accName;
    }

    public void setAccName(String accName) {
        this.accName = accName;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getAccNumber() {
        return accNumber;
    }

    public void setAccNumber(String accNumber) {
        this.accNumber = accNumber;
    }

    public String getBranchAddress() {
        return branchAddress;
    }

    public void setBranchAddress(String branchAddress) {
        this.branchAddress = branchAddress;
    }
}
