package com.gennext.offlinegst.model.user;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.panel.ActionMenu;
import com.gennext.offlinegst.user.services.purchase.ServicesPurchase;
import com.gennext.offlinegst.util.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ServicesPurchaseAdapter extends RecyclerView.Adapter<ServicesPurchaseAdapter.ReyclerViewHolder>
        implements ActionMenu.MenuPurchases {

    private final ServicesPurchase parentRef;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<PurchasesModel> items;
    private List<PurchasesModel> originalData;
    private ItemFilter mFilter = new ItemFilter();

    public ServicesPurchaseAdapter(Activity context, ArrayList<PurchasesModel> items,ServicesPurchase parentRef) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef = parentRef;
        this.items = items;
        this.originalData = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_user_purchases_format1, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final PurchasesModel item = items.get(position);

        holder.tvInvoiceNumber.setText(item.getInvoiceNumber());
        holder.tvInvDate.setText(item.getInvoiceDate());
        holder.tvVendorName.setText(item.getVendorName());
        holder.tvBeforeAmount.setText(Utility.decimalFormatWith2Digit(item.getTotalAmount()));
        holder.tvAfterAmount.setText(Utility.decimalFormatWith2Digit(item.getInvoiceTotal()));
        setColor(holder.llSideLine);
//        String produsts="";
//        if(item.getServiceList()!=null) {
//            for (ProductModel prod : item.getProductList()) {
//                produsts += prod.getProductName() + "), ";
//            }
//        }
//        holder.tvAfterAmount.setText(produsts);


        holder.llSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActionMenu.newInstance(ServicesPurchaseAdapter.this,item,position)
                        .show(parentRef.getFragmentManager(),"actionMenu");
            }
        });
    }

    public void setColor(LinearLayout tv) {
        Random rnd = new Random();
        int color= Utility.SIDE_LINE_COLORS[rnd.nextInt(21)];
        tv.setBackgroundColor(color);
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onViewClick(DialogFragment dialog, PurchasesModel itemPurchases, int position) {
        parentRef.viewServicesPurchase(itemPurchases);
    }

    @Override
    public void onEditClick(DialogFragment dialog, PurchasesModel itemPurchases, int position) {
        parentRef.editServicesPurchase(itemPurchases);
    }

    @Override
    public void onDeleteClick(DialogFragment dialog, PurchasesModel itemPurchases, int position) {
        parentRef.deleteServicesPurchase(itemPurchases,position);

    }

    public void deleteItem(int position) {
        items.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position,getItemCount());
    }

    @Override
    public void onCopyClick(DialogFragment dialog, PurchasesModel itemPurchases, int position) {
        parentRef.copyServicesPurchase(itemPurchases);
    }



    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout llSlot,llSideLine;
        private TextView tvInvoiceNumber,tvInvDate,tvVendorName,tvBeforeAmount,tvAfterAmount;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSideLine = (LinearLayout) v.findViewById(R.id.ll_slot);
            llSlot = (LinearLayout) v.findViewById(R.id.layoutSlot);
            tvInvoiceNumber = (TextView) v.findViewById(R.id.tv_slot_0);
            tvInvDate = (TextView) v.findViewById(R.id.tv_slot_1);
            tvVendorName = (TextView) v.findViewById(R.id.tv_slot_2);
            tvBeforeAmount = (TextView) v.findViewById(R.id.tv_slot_3);
            tvAfterAmount = (TextView) v.findViewById(R.id.tv_slot_4);
        }
    }


    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<PurchasesModel> list = originalData;

            int count = list.size();
            final ArrayList<PurchasesModel> nlist = new ArrayList<>(count);
            String filterableText;

            if (!filterString.equals("")) {
                for (PurchasesModel model : list) {
                    if (!filterString.equals("")) {
                        filterableText = model.getInvoiceNumber();
                        filterableText+= model.getVendorName();
                        if (filterableText.toLowerCase().contains(filterString)) {
                            nlist.add(model);
                        }
                    } else {
                        nlist.add(model);
                    }
                }
                results.values = nlist;
                results.count = nlist.size();
            } else {
                results.values = originalData;
                results.count = originalData.size();
            }

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            items = (ArrayList<PurchasesModel>) results.values;
            notifyDataSetChanged();
        }

    }

}

