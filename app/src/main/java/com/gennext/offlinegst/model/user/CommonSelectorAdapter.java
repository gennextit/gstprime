package com.gennext.offlinegst.model.user;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.panel.BusinessTypeSelector;
import com.gennext.offlinegst.panel.FileSelector;
import com.gennext.offlinegst.panel.InvoiceTypeSelector;
import com.gennext.offlinegst.panel.NoteReasonSelector;
import com.gennext.offlinegst.panel.NoteTypeSelector;
import com.gennext.offlinegst.panel.PaymentTermsSelector;
import com.gennext.offlinegst.panel.PurchaseCategorySelector;
import com.gennext.offlinegst.panel.PurchaseTypeSelector;
import com.gennext.offlinegst.panel.TaxCategorySelector;
import com.gennext.offlinegst.panel.TaxSelector;
import com.gennext.offlinegst.panel.TransportSelector;

import java.util.ArrayList;

public class CommonSelectorAdapter extends RecyclerView.Adapter<CommonSelectorAdapter.ReyclerViewHolder> {

    private PaymentTermsSelector parentRef;
    private TransportSelector parentRef2;
    private NoteTypeSelector parentRef3;
    private NoteReasonSelector parentRef4;
    private FileSelector parentRef5;
    private TaxSelector parentRef6;
    private InvoiceTypeSelector parentRef7;
    private TaxCategorySelector parentRef8;
    private PurchaseTypeSelector parentRef9;
    private PurchaseCategorySelector parentRef10;
    private BusinessTypeSelector parentRef11;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<CommonModel> items;

    public CommonSelectorAdapter(Activity context, ArrayList<CommonModel> items, PaymentTermsSelector parentRef) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef = parentRef;
        this.items = items;
    }

    public CommonSelectorAdapter(Activity context, ArrayList<CommonModel> items, TransportSelector parentRef2) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef2 = parentRef2;
        this.items = items;
    }

    public CommonSelectorAdapter(Activity context, ArrayList<CommonModel> items, NoteTypeSelector parentRef3) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef3 = parentRef3;
        this.items = items;
    }

    public CommonSelectorAdapter(Activity context, ArrayList<CommonModel> items, NoteReasonSelector parentRef4) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef4 = parentRef4;
        this.items = items;
    }

    public CommonSelectorAdapter(Activity context, ArrayList<CommonModel> items, FileSelector parentRef5) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef5 = parentRef5;
        this.items = items;
    }

    public CommonSelectorAdapter(Activity context, ArrayList<CommonModel> items, TaxSelector parentRef6) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef6 = parentRef6;
        this.items = items;
    }

    public CommonSelectorAdapter(Activity context, ArrayList<CommonModel> items, InvoiceTypeSelector parentRef7) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef7 = parentRef7;
        this.items = items;
    }

    public CommonSelectorAdapter(Activity context, ArrayList<CommonModel> items, TaxCategorySelector parentRef8) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef8 = parentRef8;
        this.items = items;
    }

    public CommonSelectorAdapter(Activity context, ArrayList<CommonModel> items, PurchaseTypeSelector parentRef9) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef9 = parentRef9;
        this.items = items;
    }

    public CommonSelectorAdapter(Activity context, ArrayList<CommonModel> items, PurchaseCategorySelector parentRef10) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef10 = parentRef10;
        this.items = items;
    }

    public CommonSelectorAdapter(Activity context, ArrayList<CommonModel> items, BusinessTypeSelector parentRef11) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef11 = parentRef11;
        this.items = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_empty, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final CommonModel item = items.get(position);
        if (parentRef5 != null) {
            holder.tvName.setTextSize(15);
        }
        holder.tvName.setText(item.getName());

        holder.llSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (parentRef != null) {
                    parentRef.onSelectedItem(item);
                } else if (parentRef2 != null) {
                    parentRef2.onSelectedItem(item);
                } else if (parentRef3 != null) {
                    parentRef3.onSelectedItem(item);
                } else if (parentRef4 != null) {
                    parentRef4.onSelectedItem(item);
                } else if (parentRef5 != null) {
                    parentRef5.onSelectedItem(item);
                }else if (parentRef6 != null) {
                    parentRef6.onSelectedItem(item);
                }else if (parentRef7 != null) {
                    parentRef7.onSelectedItem(item);
                }else if (parentRef8 != null) {
                    parentRef8.onSelectedItem(item);
                }else if (parentRef9 != null) {
                    parentRef9.onSelectedItem(item);
                }else if (parentRef10 != null) {
                    parentRef10.onSelectedItem(item);
                }else if (parentRef11 != null) {
                    parentRef11.onSelectedItem(item);
                }

            }
        });
    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout llSlot;
        private TextView tvName;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSlot = (LinearLayout) v.findViewById(R.id.layoutSlot);
            tvName = (TextView) v.findViewById(R.id.tv_message);
        }
    }
}

