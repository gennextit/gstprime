package com.gennext.offlinegst.user.product;


/**
 * Created by Admin on 5/22/2017.
 */

import android.Manifest;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.DatePickerDialog;
import com.gennext.offlinegst.global.PopupAlert;
import com.gennext.offlinegst.model.app.Model;
import com.gennext.offlinegst.model.user.CommonModel;
import com.gennext.offlinegst.model.user.ProductModel;
import com.gennext.offlinegst.panel.BarcodeScanner;
import com.gennext.offlinegst.panel.TaxCategorySelector;
import com.gennext.offlinegst.panel.TaxSelector;
import com.gennext.offlinegst.panel.UnitSelector;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.user.invoice.InvoiceManager;
import com.gennext.offlinegst.user.purchas.PurchaseManager;
import com.gennext.offlinegst.user.vouchers.AddVouchers;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.ApiCallError;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.FieldValidation;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.ProgressButton;
import com.gennext.offlinegst.util.RequestBuilder;
import com.gennext.offlinegst.util.Utility;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;


public class AddProduct extends CompactFragment implements TaxCategorySelector.SelectListener, UnitSelector.SelectListener, TaxSelector.SelectListener, BarcodeScanner.Barcodelistener, ApiCallError.ErrorParamListener, DatePickerDialog.DateSelectFlagListener {

    public static final int VIEW_PRODUCT = -1, ADD_PRODUCT_WITH_RESPONSE = 0, ADD_PRODUCT = 1, EDIT_PRODUCT = 2, COPY_PRODUCT = 3;
    private EditText etBarcode, etName, etOpeningStock, etHSN, etCostPrice, etSalePrice, etTaxRate, etTaxCategory, etBatch, etExpiry, etProductMRP, etNetAmount, etUnit, etCessRate;
    private DBManager dbManager;
    private CoordinatorLayout coordinatorLayout;
    private ProductModel itemProduct;
    private int productType;
    private FloatingActionButton btnScanBarcode;
    public static final String BARCODE_KEY = "BARCODE";

//    private Barcode barcodeResult;

    private int sDay, sMonth, sYear;

    private int eDay, eMonth, eYear;
    private String mBarcodeResult;
    private Product product;
    private PurchaseManager purchaseManager;
    private InvoiceManager invoiceManager;
    private AddVouchers addVouchers;
    private String mTaxRate;
    private ProgressButton btnAction;
    private AssignTask assignTask;
    private String mSelectedUnitCode, mSelectedUnitName;
    private String mTaxCategoryId = "", mTaxCategoryName;


    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }


    public static Fragment newInstance(Product product) {
        AddProduct fragment = new AddProduct();
        AppAnimation.setSlideAnimation(fragment, Gravity.RIGHT);
        fragment.productType = ADD_PRODUCT;
        fragment.product = product;
        return fragment;
    }

    public static Fragment newInstance(ProductModel itemProduct, int productType, Product product) {
        AddProduct fragment = new AddProduct();
        fragment.itemProduct = itemProduct;
        fragment.productType = productType;
        fragment.product = product;
        AppAnimation.setSlideAnimation(fragment, Gravity.BOTTOM);
        return fragment;
    }

    public static Fragment newInstance(ProductModel itemProduct, int productType, PurchaseManager purchaseManager) {
        AddProduct fragment = new AddProduct();
        fragment.itemProduct = itemProduct;
        fragment.productType = productType;
        fragment.purchaseManager = purchaseManager;
        AppAnimation.setSlideAnimation(fragment, Gravity.BOTTOM);
        return fragment;
    }

    public static Fragment newInstance(ProductModel itemProduct, int productType, InvoiceManager invoiceManager) {
        AddProduct fragment = new AddProduct();
        fragment.itemProduct = itemProduct;
        fragment.productType = productType;
        fragment.invoiceManager = invoiceManager;
        AppAnimation.setSlideAnimation(fragment, Gravity.BOTTOM);
        return fragment;
    }

    public static Fragment newInstance(ProductModel itemProduct, int productType, AddVouchers addVouchers) {
        AddProduct fragment = new AddProduct();
        fragment.itemProduct = itemProduct;
        fragment.productType = productType;
        fragment.addVouchers = addVouchers;
        AppAnimation.setSlideAnimation(fragment, Gravity.BOTTOM);
        return fragment;
    }

//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        if (savedInstanceState != null) {
//            Barcode restoredBarcode = savedInstanceState.getParcelable(BARCODE_KEY);
//            if (restoredBarcode != null) {
//                barcodeResult = restoredBarcode;
//            }
//        }
//    }
//
//    @Override
//    public void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        outState.putParcelable(BARCODE_KEY, barcodeResult);
//        super.onSaveInstanceState(outState);
//    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.user_product_manager, container, false);

        initToolBar(getActivity(), v, getString(R.string.product));
        screenAnalytics(getContext(), AppUser.getUserId(getContext()), "Add Product");
        dbManager = DBManager.newIsntance(getActivity());
        initUI(v);

        updateUi();
        return v;
    }


    private void updateUi() {
        if (itemProduct != null) {
            etName.setText(itemProduct.getProductName() != null ? itemProduct.getProductName() : "");
            etHSN.setText(itemProduct.getCodeHSN() != null ? itemProduct.getCodeHSN() : "");
            etOpeningStock.setText(itemProduct.getOpeningStock() != null ? itemProduct.getOpeningStock() : "");
            etBarcode.setText(itemProduct.getBarcode() != null ? itemProduct.getBarcode() : "");
            etCostPrice.setText(itemProduct.getCostPrice() != null ? itemProduct.getCostPrice() : "");
            etSalePrice.setText(itemProduct.getSalePrice() != null ? itemProduct.getSalePrice() : "");
            etTaxRate.setText(itemProduct.getTaxRate() != null ? itemProduct.getTaxRate() : "");
            etProductMRP.setText(itemProduct.getMrp() != null ? itemProduct.getMrp() : "");
            mTaxRate = itemProduct.getTaxRate();
            mTaxCategoryId = itemProduct.getTaxCategoryId();
            mTaxCategoryName = itemProduct.getTaxCategory();
            if (mTaxRate.equals("0") || mTaxRate.equals("00") || mTaxRate.equals("0.0")) {
                etTaxCategory.setVisibility(View.VISIBLE);
            } else {
                etTaxCategory.setVisibility(View.GONE);
            }
            etTaxCategory.setText(mTaxCategoryName);
            etUnit.setText(itemProduct.getUnitName() != null ? itemProduct.getUnitName() : "");
            mSelectedUnitName = itemProduct.getUnitName();
            mSelectedUnitCode = itemProduct.getUnitCode();
            etCessRate.setText(itemProduct.getCessRate() != null ? itemProduct.getCessRate() : "");
            String itemSale = itemProduct.getSalePrice();
            String taxRate = itemProduct.getTaxRate();

            etBatch.setText(itemProduct.getBatch() != null ? itemProduct.getBatch() : "");
            etExpiry.setText(itemProduct.getExp() != null ? itemProduct.getExp() : "");

        }

        if (productType == VIEW_PRODUCT) {
            disableEditFields();
        }
        hideKeybord(getActivity());
    }

    private void enableEditFields() {
        enableEditText(etBarcode);
        enableEditText(etName);
        enableEditText(etHSN);
        enableEditText(etOpeningStock);
        enableEditText(etCostPrice);
        enableEditText(etSalePrice);
        enableEditText(etBatch);
        enableEditText(etProductMRP);
        enableEditText(etCessRate);
    }

    private void disableEditFields() {
        disableEditText(etBarcode);
        disableEditText(etName);
        disableEditText(etHSN);
        disableEditText(etOpeningStock);
        disableEditText(etCostPrice);
        disableEditText(etSalePrice);
        disableEditText(etBatch);
        disableEditText(etProductMRP);
        disableEditText(etCessRate);
    }

    private void setProductNetAmount(float salePrice, float taxRate, float taxCess) {
        float totalTax = (salePrice * (taxRate + taxCess)) / 100;
        etNetAmount.setText(parseString(salePrice + totalTax));
    }


    private void initUI(View v) {
        btnScanBarcode = (FloatingActionButton) v.findViewById(R.id.fab);
        coordinatorLayout = (CoordinatorLayout) v.findViewById(R.id.coordinatorLayout);
        etName = (EditText) v.findViewById(R.id.et_product_name);
        etUnit = (EditText) v.findViewById(R.id.et_product_unit);
        etHSN = (EditText) v.findViewById(R.id.et_product_hsn);
        etOpeningStock = (EditText) v.findViewById(R.id.et_product_opening);
        etBarcode = (EditText) v.findViewById(R.id.et_product_barcode);
        etCostPrice = (EditText) v.findViewById(R.id.et_product_cost);
        etSalePrice = (EditText) v.findViewById(R.id.et_product_sale);
        etTaxRate = (EditText) v.findViewById(R.id.et_product_tax);
        etTaxCategory = (EditText) v.findViewById(R.id.et_product_tax_category);
        etBatch = (EditText) v.findViewById(R.id.et_product_batch);
        etExpiry = (EditText) v.findViewById(R.id.et_product_exp);
        etCessRate = (EditText) v.findViewById(R.id.et_product_cess);
        etProductMRP = (EditText) v.findViewById(R.id.et_product_mrp);
        etNetAmount = (EditText) v.findViewById(R.id.et_product_net_amount);

        etSalePrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setProductNetAmount(parseFloat(etSalePrice.getText().toString()), parseFloat(etTaxRate.getText().toString()), parseFloat(etCessRate.getText().toString()));
            }
        });
        etName.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        etName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    // Handle pressing "Enter" key here
                    UnitSelector.newInstance(AddProduct.this).show(getFragmentManager(), "unitSelector");
                    handled = true;
                }
                return handled;
            }
        });
        etSalePrice.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        etSalePrice.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    // Handle pressing "Enter" key here
                    TaxSelector.newInstance(AddProduct.this).show(getFragmentManager(), "taxSelector");
                    handled = true;
                }
                return handled;
            }
        });
        etCessRate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setProductNetAmount(parseFloat(etSalePrice.getText().toString()), parseFloat(etTaxRate.getText().toString()), parseFloat(etCessRate.getText().toString()));
            }
        });


        etExpiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (productType == VIEW_PRODUCT) {
                    return;
                }
                hideKeybord(getActivity());
                DatePickerDialog.newInstance(AddProduct.this, false, DatePickerDialog.END_DATE, eDay, eMonth, eYear)
                        .show(getFragmentManager(), "datePickerDialog");
            }
        });

        btnAction = ProgressButton.newInstance(getContext(), v);
        btnAction.setOnEditorActionListener(etProductMRP);
        switch (productType) {
            case VIEW_PRODUCT:
                btnAction.setText(getString(R.string.edit));
                break;
            case ADD_PRODUCT:
                btnAction.setText(getString(R.string.add));
                break;
            case EDIT_PRODUCT:
                btnAction.setText(getString(R.string.update));
                break;
            case COPY_PRODUCT:
                btnAction.setText(getString(R.string.copy));
                break;
            default:
                btnAction.setText(getString(R.string.add));
                break;
        }
        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnAction.getText().equals("Edit")) {
                    btnAction.setText("Update");
                    productType = EDIT_PRODUCT;
                    enableEditFields();
                    etName.requestFocus();
                    return;
                }
                if (!FieldValidation.isProductName(getContext(), etName, true)) {
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), mSelectedUnitCode, getString(R.string.please_select_product_unit))) {
                    return;
                } else if (!FieldValidation.isHSNCode(getContext(), etHSN, getString(R.string.invalid_hsn_code))) {
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), etSalePrice, getString(R.string.invalid_sale_price))) {
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), etTaxRate, getString(R.string.invalid_tax_rate))) {
                    return;
                } else if (etTaxCategory.getVisibility() == View.VISIBLE && !FieldValidation.isEmpty(getContext(), etTaxCategory, getString(R.string.invalid_tax_category))) {
                    return;
                } else if (!validateMRP(Utility.parseFloat(etNetAmount.getText().toString()), Utility.parseFloat(etProductMRP.getText().toString()))) {
                    return;
                }
                hideKeybord(getActivity());
                executeTask(etName.getText().toString(), etHSN.getText().toString(), etOpeningStock.getText().toString(), etBarcode.getText().toString(), etCostPrice.getText().toString()
                        , etSalePrice.getText().toString(), etTaxRate.getText().toString(), etBatch.getText().toString(), etExpiry.getText().toString(), mSelectedUnitCode, mSelectedUnitName, etCessRate.getText().toString(), etProductMRP.getText().toString(), mTaxCategoryId);


            }
        });

        btnScanBarcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (productType == VIEW_PRODUCT) {
                    return;
                }
                startScan();
            }
        });

        etTaxRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (productType == VIEW_PRODUCT) {
                    return;
                }
                TaxSelector.newInstance(AddProduct.this).show(getFragmentManager(), "taxSelector");
            }
        });
        etTaxCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (productType == VIEW_PRODUCT) {
                    return;
                }
                TaxCategorySelector.newInstance(AddProduct.this).show(getFragmentManager(), "taxCategorySelector");
            }
        });
        etUnit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (productType == VIEW_PRODUCT) {
                    return;
                }
                UnitSelector.newInstance(AddProduct.this).show(getFragmentManager(), "unitSelector");
            }
        });
        if (AppConfig.isBatchAvailable(getContext())) {
            etBatch.setVisibility(View.VISIBLE);
        } else {
            etBatch.setVisibility(View.GONE);
        }
        if (AppConfig.isExpAvailable(getContext())) {
            etExpiry.setVisibility(View.VISIBLE);
        } else {
            etExpiry.setVisibility(View.GONE);
        }
        if (AppConfig.getCessAvailable(getContext())) {
            etCessRate.setVisibility(View.VISIBLE);
        } else {
            etCessRate.setVisibility(View.GONE);
        }
    }

    private boolean validateMRP(float netAmt, float prodMRP) {
        if (netAmt <= prodMRP) {
            return true;
        } else {
            showSnakBar(coordinatorLayout, getString(R.string.mrp_error_msg));
            return true;
        }
    }


    public void startScan() {
        /**
         * Build a new MaterialBarcodeScanner
         */
        PermissionListener onPermissionListener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                addFragment(BarcodeScanner.newInstance(AddProduct.this), "barcodeScanner");
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(getContext(), getString(R.string.permission_denied) + "\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }
        };
        new TedPermission(getActivity())
                .setPermissionListener(onPermissionListener)
                .setDeniedMessage(getString(R.string.if_you_reject_permission_you_can_not_use_this_service_please_turn_on_permissions))
                .setPermissions(Manifest.permission.CAMERA)
                .check();
    }

    @Override
    public void onBarcodeDetected(String barcode) {
        mBarcodeResult = barcode;
        etBarcode.setText(barcode);
    }

    private void executeTask(String productDescription, String codeHSN, String openingStock, String barcode
            , String costPrice, String salePrice, String taxRate, String batch
            , String expiry, String unitCode, String unitName, String cessRate, String mrp, String taxCategory) {
        hideKeybord(getActivity());
        assignTask = new AssignTask(getActivity(), valString(productDescription), valString(codeHSN), valString(openingStock), valString(barcode), costPrice, salePrice, taxRate, batch, expiry, unitCode, unitName, cessRate, mrp, taxCategory);
        assignTask.execute(AppSettings.ADD_PRODUCT);
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog, String[] param) {
        executeTask(param[0], param[1], param[2], param[3], param[4], param[5], param[6], param[7], param[8], param[9], param[10], param[11], param[12], param[13]);
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog, String[] param) {

    }

    @Override
    public void onStartDateSelected(DialogFragment dialog, int day, int month, int year, String ddMMMyy) {
        sDay = day;
        sMonth = month;
        sYear = year;
        etBatch.setText(ddMMMyy);
    }

    @Override
    public void onEndDateSelected(DialogFragment dialog, int day, int month, int year, String ddMMMyy) {
        eDay = day;
        eMonth = month;
        eYear = year;
        etExpiry.setText(ddMMMyy);
    }

    @Override
    public void onTaxSelect(DialogFragment dialog, CommonModel selectedItem) {
        mTaxRate = selectedItem.getId();
        etTaxRate.setText(selectedItem.getName());
        FieldValidation.isEmpty(getContext(), etTaxRate, getString(R.string.invalid_tax_rate));
        setProductNetAmount(parseFloat(etSalePrice.getText().toString()), parseFloat(etTaxRate.getText().toString()), parseFloat(etCessRate.getText().toString()));
        if (mTaxRate.equals("0") || mTaxRate.equals("00") || mTaxRate.equals("0.0")) {
            etTaxCategory.setVisibility(View.VISIBLE);
            TaxCategorySelector.newInstance(AddProduct.this).show(getFragmentManager(), "taxCategorySelector");
        } else {
            etTaxCategory.setVisibility(View.GONE);
            etProductMRP.requestFocus();
            showKeybord(getActivity());
        }

    }

    @Override
    public void onUnitSelect(DialogFragment dialog, ProductModel selectUnit) {
        mSelectedUnitCode = selectUnit.getUnitCode();
        mSelectedUnitName = selectUnit.getUnitName();
        etUnit.setText(selectUnit.getUnitName());
        etHSN.requestFocus();
        showKeybord(getActivity());
    }

    @Override
    public void onTaxCategorySelect(DialogFragment dialog, CommonModel selectedTerm) {
        mTaxCategoryId = selectedTerm.getId();
        mTaxCategoryName = selectedTerm.getName();
        etTaxCategory.setText(mTaxCategoryName);
        FieldValidation.isEmpty(getContext(), etTaxCategory, getString(R.string.invalid_tax_category));
        etProductMRP.requestFocus();
        showKeybord(getActivity());
    }


    private class AssignTask extends AsyncTask<String, Void, Model> {
        private final String productDescription, codeHSN, openingStock, barcode, costPrice, salePrice, taxRate, batch, expiry, unitCode, unitName, cessRate, mrp, taxCategory;
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context, String productDescription, String codeHSN, String openingStock, String barcode
                , String costPrice, String salePrice, String taxRate, String batch
                , String expiry, String unitCode, String unitName, String cessRate, String mrp, String taxCategory) {
            this.productDescription = productDescription;
            this.codeHSN = codeHSN;
            this.openingStock = openingStock;
            this.barcode = barcode;
            this.costPrice = costPrice;
            this.salePrice = salePrice;
            this.taxRate = taxRate;
            this.batch = batch;
            this.expiry = expiry;
            this.unitCode = unitCode;
            this.unitName = unitName;
            this.cessRate = cessRate;
            this.mrp = mrp;
            this.taxCategory = taxCategory;
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (context != null) {
                btnAction.startAnimation();
            }
        }

        @Override
        protected Model doInBackground(String... urls) {
            Model result = new Model();
            String userId = AppUser.getUserId(context);
            String response;
            if (AppConfig.isUpdateToServer(context)) {
                switch (productType) {
                    case ADD_PRODUCT:
                        response = ApiCall.POST(urls[0], RequestBuilder.insertProduct(userId, productDescription, codeHSN, openingStock, barcode, costPrice, salePrice, taxRate, batch, expiry, unitCode, unitName, cessRate, mrp, taxCategory));
                        return JsonParser.defaultStaticParser(response);
                    case EDIT_PRODUCT:
                        response = ApiCall.POST(urls[0], RequestBuilder.editProduct(itemProduct.getProductId(), userId, productDescription, codeHSN, openingStock, barcode, costPrice, salePrice, taxRate, batch, expiry, unitCode, unitName, cessRate, mrp, taxCategory));
                        return JsonParser.defaultStaticParser(response);
                    case COPY_PRODUCT:
                        response = ApiCall.POST(urls[0], RequestBuilder.insertProduct(userId, productDescription, codeHSN, openingStock, barcode, costPrice, salePrice, taxRate, batch, expiry, unitCode, unitName, cessRate, mrp, taxCategory));
                        return JsonParser.defaultStaticParser(response);
                    default:
                        response = ApiCall.POST(urls[0], RequestBuilder.insertProduct(userId, productDescription, codeHSN, openingStock, barcode, costPrice, salePrice, taxRate, batch, expiry, unitCode, unitName, cessRate, mrp, taxCategory));
                        return JsonParser.defaultStaticParser(response);
                }
            }
//            switch (productType) {
//                case ADD_PRODUCT:
//                    return dbManager.insertProduct(result, userId, productDescription, codeHSN, openingStock, barcode, costPrice, salePrice, taxRate, batch, expiry, unitCode, unitName, cessRate);
//                case EDIT_PRODUCT:
//                    return dbManager.editProduct(result, itemProduct.getProductId(), userId, productDescription, codeHSN, openingStock, barcode, costPrice, salePrice, taxRate, batch, expiry, unitCode, unitName, cessRate);
//                case COPY_PRODUCT:
//                    return dbManager.copyProduct(result, itemProduct.getProductId(), userId, productDescription, codeHSN, openingStock, barcode, costPrice, salePrice, taxRate, batch, expiry, unitCode, unitName, cessRate);
//                default:
//                    return dbManager.insertProduct(result, userId, productDescription, codeHSN, openingStock, barcode, costPrice, salePrice, taxRate, batch, expiry, unitCode, unitName, cessRate);
//            }
            return JsonParser.defaultFailureResponse();
        }


        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                if (!AppConfig.isUpdateToServer(context)) {
                    if (result.getOutputDB().equals(Const.SUCCESS)) {
                        showSnakBar(coordinatorLayout, result.getOutputDBMsg());
                        if (product != null) {
                            product.refreshList();
                        }
                        if (productType == ADD_PRODUCT_WITH_RESPONSE) {
                            if (purchaseManager != null) {
                                purchaseManager.fetchFromServer(barcode);
                            }
                            if (invoiceManager != null) {
                                invoiceManager.fetchFromServer(barcode);
                            }
                            if (addVouchers != null) {
                                addVouchers.fetchFromDB(barcode);
                            }
                        }
                        animateButtonAndRevert();
                    } else if (result.getOutputDB().equals(Const.FAILURE)) {
                        btnAction.revertAnimation();
                        showSnakBar(coordinatorLayout, result.getOutputDBMsg());
                    }
                } else {
                    if (result.getOutput().equals(Const.SUCCESS)) {
                        showSnakBar(coordinatorLayout, result.getOutputMsg());
                        if (product != null) {
                            product.refreshList();
                        }
                        if (productType == ADD_PRODUCT_WITH_RESPONSE) {
                            if (purchaseManager != null) {
                                purchaseManager.fetchFromServer(barcode);
                            }
                            if (invoiceManager != null) {
                                invoiceManager.fetchFromServer(barcode);
                            }
                            if (addVouchers != null) {
                                addVouchers.fetchFromDB(barcode);
                            }
                        }
                        animateButtonAndRevert();
                    } else if (result.getOutput().equals(Const.FAILURE)) {
                        btnAction.revertAnimation();
                        PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(), "popupAlert");
                    } else {
                        btnAction.revertAnimation();
                        String[] errorSoon = {productDescription, codeHSN, openingStock, barcode, costPrice, salePrice, taxRate, batch, expiry, unitCode, unitName, cessRate, mrp, taxCategory};
                        ApiCallError.newInstance(result.getOutput(), result.getOutputMsg(), errorSoon, AddProduct.this)
                                .show(getFragmentManager(), "apiCallError");
                    }
                }
            }
        }

        private void animateButtonAndRevert() {
            Handler handler = new Handler();

//            circularProgressButton.doneLoadingAnimation(
//                    fillColor,
//                    bitmap);

            Runnable runnableRevert = new Runnable() {
                @Override
                public void run() {
                    if (context != null) {
                        getActivity().onBackPressed();
                    }
                }
            };
            btnAction.revertSuccessAnimation(false);
//            circularProgressButton.startAnimation();
            handler.postDelayed(runnableRevert, Const.BUTTON_PROGRESS_TIME);
        }
    }
}
