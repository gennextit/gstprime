package com.gennext.offlinegst.user.invoice;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.gennext.offlinegst.MainActivity;
import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.util.AppAnimation;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;

/**
 * Created by Admin on 7/4/2017.
 */

public class InvoicePrint extends CompactFragment {

    private static Activity activity;
    private LinearLayout invoiceView;
        private String filename = "SampleFile.pdf";
    private String filepath = "BackupInPDF";
    private File myExternalFile;
    private View mRootView;
//    private File myExternalFile;

    public static Fragment newInstance() {
        InvoicePrint fragment=new InvoicePrint();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.user_invoice_print,container,false);
        ((MainActivity)getActivity()).updateTitle("Invoice Print");
        mRootView=v;
        initUi(v);
        printView();
        showPdf();
        return v;
    }

    private void showPdf() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(myExternalFile);
        intent.setDataAndType(uri, "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

    private void printView() {
        Bitmap screen = getBitmapFromView();

        try {
            Document document = new Document();
            myExternalFile = new File(getActivity().getExternalFilesDir(filepath), filename);

            PdfWriter.getInstance(document, new FileOutputStream(myExternalFile));
            document.open();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            screen.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            addImage(document,byteArray);
            document.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private static void addImage(Document document, byte[] byteArray)
    {
        Image image = null;
        try
        {
            image = Image.getInstance(byteArray);
        }
        catch (BadElementException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (MalformedURLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // image.scaleAbsolute(150f, 150f);
        try
        {
            document.add(image);
        } catch (DocumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public Bitmap getBitmapFromView() {
        //Then take the screen shot
        Bitmap screen;
        View v1 = mRootView.getRootView();
        v1.setDrawingCacheEnabled(true);
        screen = Bitmap.createBitmap(v1.getDrawingCache());
        v1.setDrawingCacheEnabled(false);
        return screen;
    }
    private void initUi(View v) {
        invoiceView=(LinearLayout)v.findViewById(R.id.invoice_view);

    }
}
