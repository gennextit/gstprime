package com.gennext.offlinegst.panel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.model.user.InvoiceModel;
import com.gennext.offlinegst.model.user.VoucherModel;


public class ActionMenuInvoice extends BottomSheetDialogFragment implements View.OnClickListener {

    private int position;
    private InvoiceModel itemInvoice;
    private MenuInvoice mInvoiceListener;
    private MenuVouchers mVoucherListener;
    private MenuServices mServiceListener;
    private VoucherModel itemVoucher;
    private InvoiceModel invoiceServiceModel;

    public interface MenuInvoice {
        void onEditClick(DialogFragment dialog, InvoiceModel customer, int position);

        void onDeleteClick(DialogFragment dialog, InvoiceModel customer, int position);

        void onCopyClick(DialogFragment dialog, InvoiceModel customer, int position);

        void onViewClick(DialogFragment dialog, InvoiceModel customer, int position);

        void onPrintClick(DialogFragment dialog, InvoiceModel customer, int position);

        void onShareClick(DialogFragment dialog, InvoiceModel customer, int position);
        void onSendEmail(DialogFragment dialog, InvoiceModel customer, int position);

    }

    public interface MenuVouchers {
        void onEditClick(DialogFragment dialog, VoucherModel itemVoucher, int position);

        void onDeleteClick(DialogFragment dialog, VoucherModel itemVoucher, int position);

        void onCopyClick(DialogFragment dialog, VoucherModel itemVoucher, int position);

        void onViewClick(DialogFragment dialog, VoucherModel itemVoucher, int position);

        void onPrintClick(DialogFragment dialog, VoucherModel itemVoucher, int position);

    }

     public interface MenuServices {
         void onEditClick(DialogFragment dialog, InvoiceModel invoiceServiceModel, int position);

         void onDeleteClick(DialogFragment dialog, InvoiceModel invoiceServiceModel, int position);

         void onCopyClick(DialogFragment dialog, InvoiceModel invoiceServiceModel, int position);

         void onViewClick(DialogFragment dialog, InvoiceModel invoiceServiceModel, int position);

         void onPrintClick(DialogFragment dialog, InvoiceModel invoiceServiceModel, int position);

         void onShareClick(DialogFragment dialog, InvoiceModel invoiceServiceModel, int position);
         void onSendEmail(DialogFragment dialog, InvoiceModel invoiceServiceModel, int position);

     }

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static ActionMenuInvoice newInstance(MenuVouchers mVoucherListener, VoucherModel itemVoucher, int position) {
        ActionMenuInvoice fragment = new ActionMenuInvoice();
        fragment.mVoucherListener = mVoucherListener;
        fragment.itemVoucher = itemVoucher;
        fragment.mInvoiceListener = null;
        fragment.itemInvoice = null;
        fragment.position = position;
        return fragment;
    }

    public static ActionMenuInvoice newInstance(MenuInvoice mInvoiceListener, InvoiceModel itemInvoice, int position) {
        ActionMenuInvoice fragment = new ActionMenuInvoice();
        fragment.mInvoiceListener = mInvoiceListener;
        fragment.itemInvoice = itemInvoice;
        fragment.mVoucherListener = null;
        fragment.itemVoucher = null;
        fragment.position = position;
        return fragment;
    }


    public static ActionMenuInvoice newInstance(MenuServices mServiceListener, InvoiceModel invoiceServiceModel, int position) {
        ActionMenuInvoice fragment = new ActionMenuInvoice();
        fragment.mInvoiceListener = null;
        fragment.itemInvoice = null;
        fragment.mVoucherListener = null;
        fragment.itemVoucher = null;
        fragment.mServiceListener = mServiceListener;
        fragment.invoiceServiceModel = invoiceServiceModel;
        fragment.position = position;
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.menu_action_invoice, container, false);

        TextView tvShareCustomerName = (TextView) v.findViewById(R.id.tv_share_invoice);
        TextView tvTitle = (TextView) v.findViewById(R.id.action_menu_title);
        ImageView ivClose = (ImageView) v.findViewById(R.id.action_menu_close);
        LinearLayout edit = (LinearLayout) v.findViewById(R.id.action_menu_edit);
        LinearLayout delete = (LinearLayout) v.findViewById(R.id.action_menu_delete);
        LinearLayout copy = (LinearLayout) v.findViewById(R.id.action_menu_copy);
        LinearLayout view = (LinearLayout) v.findViewById(R.id.action_menu_view);
        LinearLayout print = (LinearLayout) v.findViewById(R.id.action_menu_print);
        LinearLayout shareEmail = (LinearLayout) v.findViewById(R.id.action_menu_share_email);
        LinearLayout share = (LinearLayout) v.findViewById(R.id.action_menu_share);

        if (itemInvoice != null) {
            edit.setVisibility(View.GONE);
            tvTitle.setText(getString(R.string.options_for)+" " + itemInvoice.getInvoiceNumber());
            if(itemInvoice.getCancelled()!=null&&itemInvoice.getCancelled().toLowerCase().equals("y")){
                delete.setVisibility(View.GONE);
            }else{
                if(itemInvoice.getCancellation()){
                    delete.setVisibility(View.VISIBLE);
                }else {
                    delete.setVisibility(View.GONE);
                }
            }
        }

        if (invoiceServiceModel != null) {
            edit.setVisibility(View.GONE);
            tvTitle.setText(getString(R.string.options_for)+" " + invoiceServiceModel.getInvoiceNumber());
            if(invoiceServiceModel.getCancelled()!=null&&invoiceServiceModel.getCancelled().toLowerCase().equals("y")){
                delete.setVisibility(View.GONE);
            }else{
                if(invoiceServiceModel.getCancellation()){
                    delete.setVisibility(View.VISIBLE);
                }else {
                    delete.setVisibility(View.GONE);
                }
            }
        }

        if (itemInvoice != null) {
            tvShareCustomerName.setText(getString(R.string.share_invoice_to)+" " + itemInvoice.getCustomerName());
        }

        if (itemVoucher != null) {
            tvTitle.setText(getString(R.string.options_for)+" " + itemVoucher.getVoucherNo());
        }


        ivClose.setOnClickListener(this);
        edit.setOnClickListener(this);
        delete.setOnClickListener(this);
        copy.setOnClickListener(this);
        view.setOnClickListener(this);
        print.setOnClickListener(this);
        share.setOnClickListener(this);
        shareEmail.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.action_menu_close:
                dismiss();
                break;
            case R.id.action_menu_edit:
                dismiss();
                if (mInvoiceListener != null) {
                    mInvoiceListener.onEditClick(ActionMenuInvoice.this, itemInvoice, position);
                }
                if (mVoucherListener != null) {
                    mVoucherListener.onEditClick(ActionMenuInvoice.this, itemVoucher, position);
                }

                if (mServiceListener != null) {
                    mServiceListener.onEditClick(ActionMenuInvoice.this, invoiceServiceModel, position);
                }

                break;
            case R.id.action_menu_delete:
                dismiss();
                if (mInvoiceListener != null) {
                    mInvoiceListener.onDeleteClick(ActionMenuInvoice.this, itemInvoice, position);
                }
                if (mVoucherListener != null) {
                    mVoucherListener.onDeleteClick(ActionMenuInvoice.this, itemVoucher, position);
                }
                if (mServiceListener != null) {
                    mServiceListener.onDeleteClick(ActionMenuInvoice.this, invoiceServiceModel, position);
                }
                break;
            case R.id.action_menu_copy:
                dismiss();
                if (mInvoiceListener != null) {
                    mInvoiceListener.onCopyClick(ActionMenuInvoice.this, itemInvoice, position);
                }
                if (mVoucherListener != null) {
                    mVoucherListener.onCopyClick(ActionMenuInvoice.this, itemVoucher, position);
                }
                if (mServiceListener != null) {
                    mServiceListener.onCopyClick(ActionMenuInvoice.this, invoiceServiceModel, position);
                }
                break;
            case R.id.action_menu_view:
                dismiss();
                if (mInvoiceListener != null) {
                    mInvoiceListener.onViewClick(ActionMenuInvoice.this, itemInvoice, position);
                }
                if (mVoucherListener != null) {
                    mVoucherListener.onViewClick(ActionMenuInvoice.this, itemVoucher, position);
                }
                if (mServiceListener != null) {
                    mServiceListener.onViewClick(ActionMenuInvoice.this, invoiceServiceModel, position);
                }
                break;
            case R.id.action_menu_print:
                dismiss();
                if (mInvoiceListener != null) {
                    mInvoiceListener.onPrintClick(ActionMenuInvoice.this, itemInvoice, position);
                }
                if (mVoucherListener != null) {
                    mVoucherListener.onPrintClick(ActionMenuInvoice.this, itemVoucher, position);
                }
                if (mServiceListener != null) {
                    mServiceListener.onPrintClick(ActionMenuInvoice.this, invoiceServiceModel, position);
                }
                break;
            case R.id.action_menu_share_email:
                dismiss();
                if (mInvoiceListener != null) {
                    mInvoiceListener.onSendEmail(ActionMenuInvoice.this, itemInvoice, position);
                }
                if (mServiceListener != null) {
                    mServiceListener.onSendEmail(ActionMenuInvoice.this, invoiceServiceModel, position);
                }
                break;
            case R.id.action_menu_share:
                dismiss();
                if (mInvoiceListener != null) {
                    mInvoiceListener.onShareClick(ActionMenuInvoice.this, itemInvoice, position);
                }
//                if (mVoucherListener != null) {
//                    mVoucherListener.onShareClick(ActionMenuInvoice.this, itemInvoice, position);
//                }
                if (mServiceListener != null) {
                    mServiceListener.onShareClick(ActionMenuInvoice.this, invoiceServiceModel, position);
                }
                break;



        }


    }


}
