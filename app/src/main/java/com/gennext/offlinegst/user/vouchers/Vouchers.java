package com.gennext.offlinegst.user.vouchers;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.offlinegst.MainActivity;
import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.PopupAlert;
import com.gennext.offlinegst.model.PDFMaker;
import com.gennext.offlinegst.model.user.CompanyModel;
import com.gennext.offlinegst.model.user.CustomerModel;
import com.gennext.offlinegst.model.user.VoucherAdapter;
import com.gennext.offlinegst.model.user.VoucherModel;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.ApiCallError;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.RequestBuilder;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.itextpdf.text.DocumentException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Admin on 7/4/2017.
 */

public class Vouchers extends CompactFragment implements ApiCallError.ErrorListener {
    private RecyclerView lvMain;
    private ArrayList<VoucherModel> cList;
    private VoucherAdapter adapter;
    private DBManager dbManager;

    private AssignTask assignTask;
    private FloatingActionButton btnAdd;
    private CoordinatorLayout coordinatorLayout;
    private PDFMaker pdfMaker;
    private ProgressDialog progressDialog;
    private GeneratePDFTask genPdfTask;
    private TextView tvAddNote;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
        if (genPdfTask != null) {
            genPdfTask.onAttach(context);
        }

    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
        if (genPdfTask != null) {
            genPdfTask.onDetach();
        }
    }

    public static Fragment newInstance() {
        Vouchers fragment = new Vouchers();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_vouchers, container, false);
        ((MainActivity) getActivity()).updateTitle(getString(R.string.vouchers));
        screenAnalytics(getContext(),AppUser.getUserId(getContext()),"Vouchers");
        pdfMaker = PDFMaker.newInstance(getActivity());
        dbManager = DBManager.newIsntance(getActivity());
        initUi(v);
        return v;
    }

    private void initUi(View v) {
        coordinatorLayout = (CoordinatorLayout) v.findViewById(R.id.coordinatorLayout);
        tvAddNote = (TextView) v.findViewById(R.id.tv_add_note);
        tvAddNote.setText(getString(R.string.vouchers_add_message));
        lvMain = (RecyclerView) v.findViewById(R.id.lv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());

        btnAdd = (FloatingActionButton) v.findViewById(R.id.fab);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFragment(AddVouchers.newInstance(Vouchers.this,null, AddVouchers.ADD_VOUCHER), "addVouchers");
            }
        });
        lvMain.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && btnAdd.getVisibility() == View.VISIBLE) {
                    btnAdd.hide();
                } else if (dy < 0 && btnAdd.getVisibility() != View.VISIBLE) {
                    btnAdd.show();
                }
            }
        });
        executeTask();
    }


    private void executeTask() {
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.GET_VOUCHERS);
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        executeTask();
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }

    public void refreshList() {
        executeTask();
    }

    public void deleteVouchers(VoucherModel itemVouchers) {
        String result = dbManager.deleteVouchers(itemVouchers);
        showSnakBar(coordinatorLayout, result);
    }

    public void editVouchers(VoucherModel itemVouchers) {
        addFragment(AddVouchers.newInstance(Vouchers.this,itemVouchers, AddVouchers.EDIT_VOUCHER), "addVouchers");
    }

    public void copyVouchers(VoucherModel itemVouchers) {
        addFragment(AddVouchers.newInstance(Vouchers.this,itemVouchers, AddVouchers.COPY_VOUCHER), "addVouchers");
    }

    public void viewVoucher(final VoucherModel inv) {
        screenAnalytics(getContext(),AppUser.getUserId(getContext()),"ViewVoucherPDF");
        PermissionListener onPermissionListener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                genPdfTask=new GeneratePDFTask(getContext(),Const.VIEW_PDF,inv);
                genPdfTask.execute();
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(getContext(), getString(R.string.permission_denied)+"\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }
        };

        new TedPermission(getActivity())
                .setPermissionListener(onPermissionListener)
                .setDeniedMessage(getString(R.string.if_you_reject_permission_you_can_not_use_this_service_please_turn_on_permissions))
                .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE)
                .check();
    }


    private Uri getUriFromFile(File docOutput) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            return FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", docOutput);
        } else {
            return Uri.fromFile(docOutput);
        }
    }

    public void printVoucher(final VoucherModel inv) {
        screenAnalytics(getContext(),AppUser.getUserId(getContext()),"PrintVoucherPDF");
        PermissionListener onPermissionListener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                genPdfTask=new GeneratePDFTask(getContext(),Const.PRINT_PDF,inv);
                genPdfTask.execute();
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(getContext(), getString(R.string.permission_denied)+"\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }
        };

        new TedPermission(getActivity())
                .setPermissionListener(onPermissionListener)
                .setDeniedMessage(getString(R.string.if_you_reject_permission_you_can_not_use_this_service_please_turn_on_permissions))
                .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE)
                .check();
    }


    private class AssignTask extends AsyncTask<String, Void, VoucherModel> {
        private Context context;

        private void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        private void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        private AssignTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected VoucherModel doInBackground(String... urls) {
            String userId = AppUser.getUserId(context);
            String profileId = AppUser.getProfileId(context);
            VoucherModel result = new VoucherModel();
            if (AppConfig.isUpdateToServer(context)) {
                String response = ApiCall.POST(urls[0], RequestBuilder.Default(userId, profileId));
                result = JsonParser.getVouchersDetail(response);
            }
            return dbManager.getVouchersList(result, userId, profileId);
        }


        @Override
        protected void onPostExecute(VoucherModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                if (!AppConfig.isUpdateToServer(context)) {
                    if (result.getOutputDB().equals(Const.SUCCESS)) {
                        cList = result.getList();
                        adapter = new VoucherAdapter(getActivity(), cList, Vouchers.this);
                        lvMain.setAdapter(adapter);
                    } else if (result.getOutputDB().equals(Const.FAILURE)) {
//                        showSnakBar(coordinatorLayout,result.getOutputDBMsg());
                        addFragment(AddVouchers.newInstance(Vouchers.this,null, AddVouchers.ADD_VOUCHER), "addVouchers");
                    }
                } else {
                    if (result.getOutput().equals(Const.SUCCESS)) {
                        if (!result.getOutputDB().equals(Const.SUCCESS)) {
                            showSnakBar(coordinatorLayout, result.getOutputDBMsg());
                        }
                        cList = result.getList();
                        adapter = new VoucherAdapter(getActivity(), cList, Vouchers.this);
                        lvMain.setAdapter(adapter);
                    } else if (result.getOutput().equals(Const.FAILURE)) {
                        if (result.getOutputDB().equals(Const.SUCCESS)) {
                            showSnakBar(coordinatorLayout, "Local " + result.getOutputDBMsg());
                        }
                        PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(), "popupAlert");
                    } else {
                        if (result.getOutputDB().equals(Const.SUCCESS)) {
                            showSnakBar(coordinatorLayout, "Local " + result.getOutputDBMsg());
                        }
                        ApiCallError.newInstance(result.getOutput(),result.getOutputMsg(), Vouchers.this)
                                .show(getFragmentManager(), "apiCallError");
                    }
                }
            }
        }
    }



    private class GeneratePDFTask extends AsyncTask<String, Void, File> {
        private Context context;
        private int task;
        private VoucherModel inv;
        private ProgressDialog pDialog;

        private void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        private void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        private GeneratePDFTask(Context context,int task,VoucherModel inv) {
            this.context = context;
            this.task = task;
            this.inv = inv;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setTitle("Generating Voucher Pdf");
            pDialog.setMessage("Please wait ...");
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.show();

        }

        @Override
        protected File doInBackground(String... urls) {
            if(task==Const.VIEW_PDF){
                File mOutputFile = null;
                CompanyModel companyDetail = dbManager.getCompanyDetail(inv.getVoucherUserId(), inv.getVoucherProfileId(), inv.getVoucherCompanyId());
                CustomerModel customerDetail = dbManager.getCustomerByCustomerId(inv.getVoucherUserId(), inv.getCustomerId());
                try {
                    mOutputFile = pdfMaker.generateVoucherPDF(inv, companyDetail, customerDetail);
                    return mOutputFile;
                } catch (IOException | DocumentException e) {
                    e.printStackTrace();
                    return null;
                }
            }else{
                File mOutputFile = null;
                CompanyModel companyDetail = dbManager.getCompanyDetail(inv.getVoucherUserId(), inv.getVoucherProfileId(), inv.getVoucherCompanyId());
                CustomerModel customerDetail = dbManager.getCustomerByCustomerId(inv.getVoucherUserId(), inv.getCustomerId());
                try {
                    mOutputFile = pdfMaker.generateVoucherPDF(inv, companyDetail, customerDetail);
                    return mOutputFile;
                } catch (IOException | DocumentException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }


        @Override
        protected void onPostExecute(File mOutputFile) {
            // TODO Auto-generated method stub
            super.onPostExecute(mOutputFile);
            if (context != null) {
                pDialog.dismiss();
                if(task==Const.VIEW_PDF) {
                    if (mOutputFile != null) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        Uri uri = getUriFromFile(mOutputFile);
                        intent.setDataAndType(uri, "application/pdf");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        startActivity(intent);
                    }
                }else{
                    if (mOutputFile != null) {
                        Uri uri =getUriFromFile(mOutputFile);
                        Intent in = new Intent(Intent.ACTION_SEND);
                        in.setDataAndType(uri,"application/pdf");
                        in.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                    in.putExtra("android.intent.extra.TEXT", mOutputFile.getName());
                        in.putExtra(Intent.EXTRA_STREAM, uri);
                        try {
                            startActivity(Intent.createChooser(in, "Share With Fiends"));
                        } catch (Exception e) {
                            Toast.makeText(getActivity(), "Error: Cannot open or share created PDF report.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        }
    }
}
