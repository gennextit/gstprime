package com.gennext.offlinegst.model;

import android.app.Activity;

import com.gennext.offlinegst.model.user.CompanyModel;
import com.gennext.offlinegst.model.user.CustomerModel;
import com.gennext.offlinegst.model.user.InvoiceModel;
import com.gennext.offlinegst.model.user.ProductModel;
import com.gennext.offlinegst.model.user.VoucherModel;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DateTimeUtility;
import com.gennext.offlinegst.util.Utility;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Admin on 8/2/2017.
 */

public class PDFBillOfSupply {
    private Activity mContext;
    private String directoryName = "Invoices";

    private static final float PADDING_NORMAL = 4;
    private static final float PADDING_SMALL = 2;
    private static final float PADDING_ZERO = 0;
    private static Font catFontBold = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.NORMAL);
    private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
    private static Font small = new Font(Font.FontFamily.TIMES_ROMAN, 5, Font.NORMAL);
    private static Font normal = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);

    private static Font normalBold = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
    private static Font normalBodyBold = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
    private static Font normalBody = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);
    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
    private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);

    public static PDFBillOfSupply newInstance(Activity mContext) {
        PDFBillOfSupply pdfMaker = new PDFBillOfSupply();
        pdfMaker.mContext = mContext;
        return pdfMaker;
    }

    public File generateInvoicePDF(InvoiceModel invModel, CompanyModel companyDetail, String selectedPrintType) throws IOException, DocumentException {

        ArrayList<String> unionTerritoryList = new ArrayList<>();
        unionTerritoryList.add("Chandigarh");
        unionTerritoryList.add("Lakshadweep");
        unionTerritoryList.add("Daman and Diu");
        unionTerritoryList.add("Dadar and Nagar Haveli");
        unionTerritoryList.add("Andaman and Nicobar Islands");
        // step 1
        Boolean isBatchAvailabe = AppConfig.isBatchAvailable(mContext);
        Boolean isExpAvailabe = AppConfig.isExpAvailable(mContext);
        Boolean isBankAvailabe = AppConfig.isBankAvailable(mContext);
        Boolean isIGSTAvailabe = AppConfig.isIGSTAvailable(mContext);
        Boolean isVehicelAvailabe = AppConfig.isVehicelAvailable(mContext);
        Boolean isDLNoAvailabe = AppConfig.isDLNoAvailable(mContext);
        Boolean isPOSAvailabe = AppConfig.isPOSAvailable(mContext);

        Document document;
//        if (isPOSAvailabe) {
//            Rectangle pageSizePOS = new Rectangle(164.41f, 14400); //58 mm into pt: 164.409448819 pt
//            document = new Document(pageSizePOS);
//        } else {
            document = new Document();
//        }
        String filename = invModel.getCustomerName() + "_Invoice" + DateTimeUtility.getTimeStamp() + ".pdf";
        // step 2
        File filePath = new File(mContext.getExternalFilesDir(AppConfig.FOLDER_INVOICES), filename);
        PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(filePath));

        // step 3
        document.open();
        // step 4
        if (invModel.getCancelled().toLowerCase().equals("y")) {
            pdfWriter.setPageEvent(new PDFEventListener(35,295,700));// watermark
        }
        // stem 5
        PdfPTable table;
        table = addInvoiceHearderTable(invModel, companyDetail, isVehicelAvailabe, isDLNoAvailabe, selectedPrintType);
        table.setWidthPercentage(105);
        table.setHorizontalAlignment(Element.ALIGN_MIDDLE);
        document.add(table);

        boolean result = unionTerritoryList.contains(invModel.getCustomerState());
        String gstHeader;
        if (result) {
            gstHeader = "UTGST";
        } else {
            gstHeader = "SGST";
        }
        boolean isIGSTApplicable;
        if (companyDetail.getStateCode().equalsIgnoreCase(invModel.getCustomerStateCode())) {
            isIGSTApplicable = false;
        } else {
            isIGSTApplicable = true;
        }
        table = addInvoiceBodyTable(invModel, isIGSTAvailabe, isIGSTApplicable, gstHeader);
        table.setWidthPercentage(105);
        table.setHorizontalAlignment(Element.ALIGN_MIDDLE);
        table.setSpacingAfter(1);
        document.add(table);

        table = addInvoiceFooterTable(invModel, companyDetail, isBankAvailabe, isIGSTAvailabe, isIGSTApplicable, gstHeader);
        table.setWidthPercentage(105);
        table.setHorizontalAlignment(Element.ALIGN_MIDDLE);
        table.setSpacingAfter(5);
        document.add(table);

//        String validity = AppUser.getPackageStatus(mContext);
//        if (!validity.toLowerCase().equals(Const.PAID)) {
//            Paragraph phrase = new Paragraph();
//            phrase.add(new Chunk(Const.FOOTER_PDF_1, normal));
//            phrase.add(new Chunk(Const.FOOTER_PDF_2, normal));
//            phrase.add(new Chunk(Const.FOOTER_PDF_3, normal));
//            phrase.add(new Chunk(Const.FOOTER_PDF_4, normal));
//            phrase.setAlignment(Element.ALIGN_CENTER);
//            document.add(phrase);
//        }

        // step 5
        document.close();
        return filePath;
    }

    private PdfPTable addInvoiceBodyTable(InvoiceModel invModel, Boolean isIGSTAvailabe, Boolean isIGSTApplicable, String gstHeader) throws DocumentException {
        boolean isSerialAvailable = AppConfig.getSerialAvailable(mContext);
        boolean isHSNAvailable = AppConfig.getHSNAvailable(mContext);
        boolean isBatchAvailable = AppConfig.isBatchAvailable(mContext);
        boolean isExpAvailable = AppConfig.isExpAvailable(mContext);
        boolean isBlankRowsAvailable = AppConfig.getBlankRowsAvailable(mContext);

        PdfPTable table = new PdfPTable(7);
        int[] tableWidths = new int[]{2, 10, 3, 3, 3, 3, 3};
        if (!isSerialAvailable) {
            tableWidths[0] = 0;
        }
        // product name[1]

        if (!isBatchAvailable) {
            tableWidths[2] = 0;
        }
        if (!isExpAvailable) {
            tableWidths[3] = 0;
        }
        if (!isHSNAvailable) {
            tableWidths[4] = 0;
        }
//            quantity [5] = 0;
//            amount [6] = 0;


        table.setWidths(tableWidths);

        table.addCell(setBodyHeader("S/N", Element.ALIGN_LEFT));

        table.addCell(setBodyHeader("Item", Element.ALIGN_LEFT));

        table.addCell(setBodyHeader("Batch"));

        table.addCell(setBodyHeader("Exp"));

        table.addCell(setBodyHeader("HSN"));

        table.addCell(setBodyHeader("QTY"));

        table.addCell(setBodyHeader("Amt", Element.ALIGN_RIGHT));


        ArrayList<ProductModel> prodList = invModel.getProductList();
        for (int i = 0; i < prodList.size(); i++) {
            ProductModel prod = prodList.get(i);
            String totalProductsAmt = getTotalProductPrice2(prod.getQuantity(), prod.getSalePrice(), prod.getDis());
            setAllProductInInvoice(table, String.valueOf(i + 1), decimalFormat2(prod.getQuantity()), prod.getProductName(), prod.getBatch(), prod.getExp()
                    , prod.getCodeHSN(), totalProductsAmt);
        }
        String frightAmt = decimalFormatWith2Digit(convertToFloat(invModel.getFreightAmount()));
        if (frightAmt.equals("")) {
            frightAmt = "0";
        }

        setAllProductInInvoice(table, " ", " ", "Freight", " ", " ", " ", frightAmt);

        if (isBlankRowsAvailable) {
            if (prodList.size() + 1 < 19) {
                for (int i = 0; i < 20 - prodList.size() + 1; i++) {
                    setAllProductInInvoice(table, " ", " ", " ", " ", " ", " ", " ");
                }
            }
        }
        return table;
    }


    private String getTotalProductPrice2(String quantity, String salePrice, String discount) {
        float qun = convertToFloat(quantity);
        float price = convertToFloat(salePrice);
        float dis = convertToFloat(discount);
//        DecimalFormat df = new DecimalFormat();
//        df.setMaximumFractionDigits(2);
        return decimalFormatWith2Digit((qun * price) - dis);
    }

    private String decimalFormatWith2Digit(float value) {
        return String.format(Locale.US,"%.02f", value);
    }


    private String getTotalProductPrice(String quantity, String salePrice, String discount) {
        float qun = convertToFloat(quantity);
        float price = convertToFloat(salePrice);
        float dis = convertToFloat(discount);
        return new DecimalFormat("#.##").format((qun * price) - dis);
    }

    private String totalPrice(String salePrice, String totalTaxAmount) {
        float saleAmt = convertToFloat(salePrice);
        float taxAmt = convertToFloat(totalTaxAmount);
        return new DecimalFormat("#.##").format(saleAmt + taxAmt);
    }

    private String decimalFormat(String value) {
        return new DecimalFormat("#").format(convertToFloat(value));
    }

    private String decimalFormat2(float value) {
        String ures = new DecimalFormat("#.##").format(value);
        return ures;
    }

    private String decimalFormat2(String value) {
        String ures = new DecimalFormat("#.##").format(convertToFloat(value));
        return ures;
    }

    private String totalPrice(String salePrice, String frieghtAmount, String frieghtTaxAmount, String totalTaxAmount) {
        float saleAmt = convertToFloat(salePrice);
        float frieghtAmt = convertToFloat(frieghtAmount);
        float frightTaxAmt = convertToFloat(frieghtTaxAmount);
        float totalTaxAmt = convertToFloat(totalTaxAmount);
        return new DecimalFormat("#.##").format(saleAmt + frieghtAmt + frightTaxAmt + totalTaxAmt);
    }

    private float convertToFloat(String value) {
        try {
            return Float.parseFloat(value);
        } catch (NumberFormatException e) {
            return 0;
        } catch (NullPointerException e) {
            return 0;
        }
    }

    private void setAllProductInInvoice(PdfPTable table, String sNo, String qty, String pName, String batch, String exp
            , String hsn, String total) {

        table.addCell(setBodyEntity(sNo, Element.ALIGN_LEFT));

        table.addCell(setBodyEntity(pName, Element.ALIGN_LEFT));

        table.addCell(setBodyEntity(batch));

        table.addCell(setBodyEntity(exp));

        table.addCell(setBodyEntity(validateHSNCode(hsn)));

        table.addCell(setBodyEntity(qty));

        table.addCell(setBodyEntity(total, Element.ALIGN_RIGHT));

    }


    private String calTaxValue(String taxable, String cessRate) {
        float mTaxable = convertToFloat(taxable);
        float mCessRate = convertToFloat(cessRate);
        return new DecimalFormat("#.##").format((mTaxable * mCessRate) / 100);
    }

//    private String calTaxValue(String taxable, String cessRate) {
//        float mTaxable = convertToFloat(taxable);
//        float mCessRate = convertToFloat(cessRate);
//        return new DecimalFormat("#.##").format((mTaxable * mCessRate)/100);
//    }

    private String validateHSNCode(String hsn) {
        try {
            if (hsn.length() <= 4) {
                return hsn;
            } else {
                return hsn.substring(0, 4);
            }
        } catch (IndexOutOfBoundsException e) {
            return hsn;
        }
    }

    private PdfPTable addInvoiceFooterTable(InvoiceModel invModel, CompanyModel companyDetail
            , Boolean isBankAvailabe, Boolean isIGSTAvailabe, Boolean isIGSTApplicable, String gstHeader) throws DocumentException {
        PdfPTable table = new PdfPTable(2);
        String[] allGST = calculateProductGST(invModel.getProductList(), invModel);

        String totalTaxableAmount = totalPrice(invModel.getTotalAmount(), invModel.getFreightAmount());
        String amountAfterTax = totalPrice(totalTaxableAmount, allGST[0]);
        String totalAmount = decimalFormat(amountAfterTax);

//        table.setWidthPercentage(288 / 5.23f);
        int[] tableWidth = new int[]{1, 1};

        table.setWidths(tableWidth);

        table.addCell(setDividerDotted(2, PdfPCell.TOP));

        table.addCell(setBodyEntity("Total", Element.ALIGN_LEFT));

        table.addCell(setBodyEntity(totalTaxableAmount, Element.ALIGN_RIGHT));

        String roundValue = decimalFormat2(convertToFloat(totalAmount) - convertToFloat(amountAfterTax));

        table.addCell(setBodyEntity("Round Off", Element.ALIGN_LEFT));

        table.addCell(setBodyEntity(roundValue, Element.ALIGN_RIGHT));

        table.addCell(setBodyHeader("Total", Element.ALIGN_LEFT));

        table.addCell(setBodyHeader(totalAmount, Element.ALIGN_RIGHT));

        table.addCell(setDividerDotted(2, PdfPCell.BOTTOM));

        return table;
    }


    private String[] calculateProductGST(ArrayList<ProductModel> productList, InvoiceModel invoiceModel) {
//        String[] allGst = new String[5];
        float totalTax, totalCGST = 0, totalSGST = 0, totalIGST = 0, totalCessVal = 0;
        for (ProductModel mod : productList) {
            float cgst = convertToFloat(mod.getCgstVal());
            float sgst = convertToFloat(mod.getSgstVal());
            float igst = convertToFloat(mod.getIgstVal());
            totalCGST += cgst;
            totalSGST += sgst;
            totalIGST += igst;
            String totalProductsAmt = getTotalProductPrice(mod.getQuantity(), mod.getSalePrice(), mod.getDis());
            float mTaxable = convertToFloat(totalProductsAmt);
            float mCessRate = convertToFloat(mod.getCessRate());
            float mCessValue = (mTaxable * mCessRate) / 100;
            totalCessVal += mCessValue;
        }
        float freightCgst = convertToFloat(invoiceModel.getCgstVal());
        float freightSgst = convertToFloat(invoiceModel.getSgstVal());
        float freightIgst = convertToFloat(invoiceModel.getIgstVal());

        totalTax = totalCGST + totalSGST + totalIGST + freightCgst + freightSgst + freightIgst + totalCessVal;
//        allGst[0] = new DecimalFormat("#.##").format(totalTax);
//        allGst[1] = new DecimalFormat("#.##").format(totalCGST + freightCgst);
//        allGst[2] = new DecimalFormat("#.##").format(totalSGST + freightSgst);
//        allGst[3] = new DecimalFormat("#.##").format(totalIGST + freightIgst);
//        allGst[4] = new DecimalFormat("#.##").format(totalCessVal);
        return new String[]{decimalFormat2(totalTax), decimalFormat2(totalCGST + freightCgst)
                , decimalFormat2(totalSGST + freightSgst), decimalFormat2(totalIGST + freightIgst)
                , decimalFormat2(totalCessVal)};
    }

    public static int parseInt(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return 0;
        } catch (NullPointerException e) {
            return 0;
        }
    }

    public static float parseFloat(String value) {
        try {
            return Float.parseFloat(value);
        } catch (NumberFormatException e) {
            return 0;
        } catch (NullPointerException e) {
            return 0;
        }
    }

    private void setAllGST(Boolean isIGSTAvailabe, PdfPTable table, String rate, float taxableAmt, float cgst, float sgst, float igst, float cessAmount, float total, boolean isSignature) {
        PdfPCell cell;
        cell = new PdfPCell(new Phrase(rate, normalBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPadding(PADDING_SMALL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(String.valueOf(new DecimalFormat("#.##").format(taxableAmt)), normalBold));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setPadding(PADDING_SMALL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(String.valueOf(new DecimalFormat("#.##").format(cgst)), normalBold));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setPadding(PADDING_SMALL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(String.valueOf(new DecimalFormat("#.##").format(sgst)), normalBold));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setPadding(PADDING_SMALL);
        table.addCell(cell);

        if (isIGSTAvailabe) {
            cell = new PdfPCell(new Phrase(new DecimalFormat("#.##").format(igst), normalBold));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setPadding(PADDING_SMALL);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(new DecimalFormat("#.##").format(cessAmount), normalBold));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setPadding(PADDING_SMALL);
            table.addCell(cell);
        } else {
            cell = new PdfPCell(new Phrase(new DecimalFormat("#.##").format(cessAmount), normalBold));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setPadding(PADDING_SMALL);
            cell.setColspan(2);
            table.addCell(cell);
        }

        cell = new PdfPCell(new Phrase(new DecimalFormat("#.##").format(total), normalBold));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setPadding(PADDING_SMALL);
        table.addCell(cell);

        if (isSignature) {
            cell = new PdfPCell(new Phrase("This is system generated invoice hence no signature required", normalBody));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setPadding(PADDING_SMALL);
            cell.setColspan(3);
            cell.setRowspan(5);
            table.addCell(cell);
        }
    }


    private PdfPTable addInvoiceHearderTable(InvoiceModel invModel, CompanyModel companyDetail, Boolean isVehicelAvailabe, Boolean isDLNoAvailabe, String selectedPrintType) throws DocumentException {
        PdfPTable table = new PdfPTable(5);
//        table.setWidthPercentage(288 / 5.23f);
        int[] tableWidths = new int[]{15, 20, 30, 20, 15};
        table.setWidths(tableWidths);
        PdfPCell cell;
        Phrase phrase;

        cell = new PdfPCell();
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        phrase = new Phrase();
        phrase.add(new Chunk("Bill of Supply\n", normal));
        phrase.add(new Chunk(companyDetail.getCompanyName(), normal));
        phrase.add(new Chunk("\n", normal));
        if (isDLNoAvailabe) {
            phrase.add(new Chunk(companyDetail.getAddress() + "\n" + companyDetail.getTelephone() + "\nD.L.No." + companyDetail.getDlNumber(), normal));
        } else {
            phrase.add(new Chunk(companyDetail.getAddress() + "\n" + companyDetail.getTelephone(), normal));
        }
        phrase.add(new Chunk("\n" + "GSTIN:" + companyDetail.getGstin(), normal));

        cell = new PdfPCell(phrase);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPadding(PADDING_NORMAL);
        cell.setVerticalAlignment(Element.ALIGN_CENTER);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setColspan(3);
        table.addCell(cell);


        cell = new PdfPCell(new Phrase());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setVerticalAlignment(Element.ALIGN_CENTER);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        table.addCell(setDividerDotted(5, PdfPCell.TOP));

        phrase = new Phrase();
        phrase.add(new Chunk(invModel.getCustomerName(), normal));
//        phrase.add(new Chunk(invModel.getCustomerAddress() + ", " + invModel.getCustomerState() + "\n  ", catFontNormal));

        cell = new PdfPCell(phrase);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setVerticalAlignment(Element.ALIGN_LEFT);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setColspan(3);
        table.addCell(cell);

        phrase = new Phrase();
        phrase.add(new Chunk("Bill Number :-", normal));
        phrase.add(new Chunk(invModel.getInvoiceNumber() + "\n", normal));
        phrase.add(new Chunk("Date:-", normal));
        phrase.add(new Chunk(invModel.getInvoiceDate(), normal));

        cell = new PdfPCell(phrase);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setVerticalAlignment(Element.ALIGN_RIGHT);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//        cell.setNoWrap(false);
//        cell.setMinimumHeight(50);
        cell.setColspan(2);
        table.addCell(cell);

        table.addCell(setDividerDotted(5, PdfPCell.BOTTOM));

        return table;
    }


    private PdfPCell setDividerDotted(int colSpan, int gravity) {
        PdfPCell cell = new PdfPCell();
        cell.setBorder(PdfPCell.NO_BORDER);
        cell.setCellEvent(new DottedCell(gravity));
        cell.setColspan(colSpan);
        return cell;
    }

    private PdfPCell setBodyEntity(String title) {
        return setBodyEntity(title, Element.ALIGN_CENTER);
    }

    private PdfPCell setBodyEntity(String title, int gravity) {
        PdfPCell cell;
        cell = new PdfPCell(new Phrase(title, normalBody));
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(gravity);
        cell.setVerticalAlignment(Element.ALIGN_CENTER);
//        cell.setBackgroundColor (new BaseColor (140, 221, 8));
        return cell;
    }

    private PdfPCell setBodyHeader(String title) {
        return setBodyHeader(title, Element.ALIGN_CENTER);
    }

    private PdfPCell setBodyHeader(String title, int gravity) {
        PdfPCell cell;
        cell = new PdfPCell(new Phrase(title, normalBodyBold));
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        cell.setHorizontalAlignment(gravity);
        cell.setVerticalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setRowspan(2);
//        cell.setBackgroundColor (new BaseColor (140, 221, 8));
        return cell;
    }
}
