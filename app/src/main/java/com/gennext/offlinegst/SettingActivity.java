package com.gennext.offlinegst;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;

import com.gennext.offlinegst.setting.GlobalSetting;
import com.gennext.offlinegst.user.SettingDefaults;
import com.gennext.offlinegst.util.LocaleHelper;

/**
 * Created by Admin on 7/4/2017.
 */

public class SettingActivity extends BaseActivity{

    private Toolbar mToolbar;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        mToolbar=initToolBar(getString(R.string.setting));
        updateUi();
    }

    private void updateUi() {
        addFragmentWithoutBackstack(SettingDefaults.newInstance(),R.id.container_2,"settingDefaults");
        addFragmentWithoutBackstack(GlobalSetting.newInstance(),R.id.container_1,"globalSetting");
    }

    public void updateLanguage() {

    }
}
