package com.gennext.offlinegst;

import android.content.Intent;
import android.os.Bundle;

import com.gennext.offlinegst.user.company.AddCompany;
import com.gennext.offlinegst.user.company.AddProfile;
import com.gennext.offlinegst.user.company.UpdateProfile;

/**
 * Created by Admin on 7/20/2017.
 */

public class ProfileActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        addFragmentWithoutBackstack(AddCompany.newInstance(),R.id.container,"addCompany");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == ImageMaster.REQUEST_PROFILE) {
            AddProfile addProfile= (AddProfile) getSupportFragmentManager().findFragmentByTag("addProfile");
            if(addProfile!=null){
                addProfile.OnActivityResult(requestCode, resultCode, data);
            }
            UpdateProfile updateProfile= (UpdateProfile) getSupportFragmentManager().findFragmentByTag("updateProfile");
            if(updateProfile!=null){
                updateProfile.OnActivityResult(requestCode, resultCode, data);
            }

        } else {
        }
    }
}
