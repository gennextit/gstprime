package com.gennext.offlinegst.util;

import android.content.Context;

import com.gennext.offlinegst.setting.InvoiceSetting;

import java.text.DecimalFormat;


/**
 * Created by Admin on 7/4/2017.
 */

public class AppConfig {
    public static final String FOLDER_INVOICES = "Invoices";
    public static final String FOLDER_DATABASE = "databases";
    public static final String FOLDER_PROFILE = "profile";
    public static final String FOLDER_XLS = "BackupInExel";
    private static final String COMMON = AppUser.COMMON;
    private static final String UPDATE_TO_SERVER = COMMON + "UpdateToServer";
    private static final String PURCHASE_INVOICE_PREFIX = COMMON + "pip";
    private static final String PURCHASE_NUMBER_STARTING = COMMON + "pns";
    private static final String PURCHASE_NUMBER_WIDTH = COMMON + "pnw";
    private static final String SELL_INVOICE_PREFIX = COMMON + "sip";
    private static final String SELL_NUMBER_STARTING = COMMON + "sns";
    private static final String SELL_NUMBER_WIDTH = COMMON + "snw";
    private static final String BATCH_AVAILABLE = COMMON + "batch";
    private static final String EXP_AVAILABLE = COMMON + "exp";
    private static final String MRP_AVAILABLE = COMMON + "mrp";
    private static final String BANK_AVAILABLE = COMMON + "Bank";
    private static final String IGST_AVAILABLE = COMMON + "igst";
    private static final String VEHICEL_AVAILABLE = COMMON + "Vehicel";
    private static final String DLNO_AVAILABLE = COMMON + "DLNo";
    private static final String POS_AVAILABLE = COMMON + "POS";
    private static final String PURCHASE_INVOICE_EMAIL_BODY = COMMON+"emailBody";

    private static final String SERIAL_AVAILABLE = COMMON + "serialAvl";
    private static final String UNIT_AVAILABLE = COMMON + "unitAvl";
    private static final String HSN_AVAILABLE = COMMON + "hsnAvl";
    private static final String TAX_AVAILABLE = COMMON + "taxAvl";
    private static final String DISCOUNT_AVAILABLE = COMMON + "discountAvl";
    private static final String CESS_AVAILABLE = COMMON + "cessAvl";
    private static final String TAXABLE_AVAILABLE = COMMON + "taxableAvl";
    private static final String BLANKROWS_AVAILABLE = COMMON + "blankRowAvl";
    private static final String OPTION_BARCODE_AVAILABLE = COMMON + "optBarcodeAvl";
    private static final String SALE_PURCHASE_CHART = COMMON + "sale_purchase_chart";
    private static final String DEBTOR_CREDITOR_CHART = COMMON + "debtor_creditor_chart";
    private static final String DASHBOARD_MENU = COMMON + "dashboard_menu";
    private static final String TAX_PAYABLE_CHART = COMMON + "tax_payable_chart";

    public static boolean isUpdateToServer(Context context) {
        return Utility.LoadPrefBoolean(context, UPDATE_TO_SERVER,true);
    }

    public static void setUpdateToServer(Context context, Boolean status) {
        Utility.SavePrefBoolean(context, UPDATE_TO_SERVER, status);
    }

    public static void setPurchaseInvoicePrefix(Context context, String prefix, String userId, String profileId) {
        Utility.SavePref(context, profileId + userId + PURCHASE_INVOICE_PREFIX, prefix);
    }

    public static String getPurchaseInvoicePrefix(Context context, String userId, String profileId) {
        return Utility.LoadPref(context, profileId + userId + PURCHASE_INVOICE_PREFIX, InvoiceSetting.DEFAULT_INVOICE_PREFIX);
    }

    public static String getSellInvoicePrefix(Context context, String userId, String profileId, String companyId) {
        return Utility.LoadPref(context, companyId + profileId + userId + SELL_INVOICE_PREFIX, InvoiceSetting.DEFAULT_INVOICE_PREFIX);
    }

    public static void setPurchaseNumberStarting(Context context, int value, String userId, String profileId) {
        Utility.SavePrefInt(context, profileId + userId + PURCHASE_NUMBER_STARTING, value);

    }

    public static String getPurchaseInvoiceNumber(Context context, String userId, String profileId) {
        int startNumber = Utility.LoadPrefInt(context, profileId + userId + PURCHASE_NUMBER_STARTING, InvoiceSetting.DEFAULT_NUMBER_STARTING);
        return generateInvoiceNumber(context, userId, profileId, startNumber);
    }

    public static String generateInvoiceWithExistingNumber(Context context, String userId, String profileId, String invoiceNumber) {
        String prefix = Utility.LoadPref(context, profileId + userId + PURCHASE_INVOICE_PREFIX, InvoiceSetting.DEFAULT_INVOICE_PREFIX);
        int numWidth = Utility.LoadPrefInt(context, profileId + userId + PURCHASE_NUMBER_WIDTH, InvoiceSetting.DEFAULT_NUMBER_WIDTH);
        int innNo = Utility.parseInt(invoiceNumber.replace(prefix, ""));
        innNo++;
        DecimalFormat formatter;
        switch (numWidth) {
            default:
                formatter = new DecimalFormat("0");
                break;
            case 2:
                formatter = new DecimalFormat("00");
                break;
            case 3:
                formatter = new DecimalFormat("000");
                break;
            case 4:
                formatter = new DecimalFormat("0000");
                break;
            case 5:
                formatter = new DecimalFormat("00000");
                break;
        }
        String aFormatted = formatter.format(innNo);
        return prefix + aFormatted;
    }

    public static String generateInvoiceNumber(Context context, String userId, String profileId, int finalNumber) {
        String prefix = Utility.LoadPref(context, profileId + userId + PURCHASE_INVOICE_PREFIX, InvoiceSetting.DEFAULT_INVOICE_PREFIX);
        int numWidth = Utility.LoadPrefInt(context, profileId + userId + PURCHASE_NUMBER_WIDTH, InvoiceSetting.DEFAULT_NUMBER_WIDTH);
        DecimalFormat formatter;
        switch (numWidth) {
            default:
                formatter = new DecimalFormat("0");
                break;
            case 2:
                formatter = new DecimalFormat("00");
                break;
            case 3:
                formatter = new DecimalFormat("000");
                break;
            case 4:
                formatter = new DecimalFormat("0000");
                break;
            case 5:
                formatter = new DecimalFormat("00000");
                break;
        }
        String aFormatted = formatter.format(finalNumber);
        return prefix + aFormatted;
    }

    public static void setPurchaseNumberWidth(Context context, int value, String userId, String profileId) {
        Utility.SavePrefInt(context, profileId + userId + PURCHASE_NUMBER_WIDTH, value);
    }

    public static int getPurchaseNumberStarting(Context context, String userId, String profileId) {
        return Utility.LoadPrefInt(context, profileId + userId + PURCHASE_NUMBER_STARTING, InvoiceSetting.DEFAULT_NUMBER_STARTING);
    }

    public static int getSellNumberStarting(Context context, String userId, String profileId, String companyId) {
        return Utility.LoadPrefInt(context, companyId + profileId + userId + SELL_NUMBER_STARTING, InvoiceSetting.DEFAULT_NUMBER_STARTING);
    }

    public static int getPurchaseNumberWidth(Context context, String userId, String profileId) {
        return Utility.LoadPrefInt(context, profileId + userId + PURCHASE_NUMBER_WIDTH, InvoiceSetting.DEFAULT_NUMBER_WIDTH);
    }

    public static int getSellNumberWidth(Context context, String userId, String profileId, String companyId) {
        return Utility.LoadPrefInt(context, companyId + profileId + userId + SELL_NUMBER_WIDTH, InvoiceSetting.DEFAULT_NUMBER_WIDTH);
    }


    public static void setSellInvoicePrefix(Context context, String prefix, String userId, String profileId, String companyId) {
        Utility.SavePref(context, companyId + profileId + userId + SELL_INVOICE_PREFIX, prefix);
    }

    public static void setSellNumberStarting(Context context, int value, String userId, String profileId, String companyId) {
        Utility.SavePrefInt(context, companyId + profileId + userId + SELL_NUMBER_STARTING, value);
    }

    public static void setSellNumberWidth(Context context, int value, String userId, String profileId, String companyId) {
        Utility.SavePrefInt(context, companyId + profileId + userId + SELL_NUMBER_WIDTH, value);
    }

    public static String getSellInvoiceNumber(Context context, String userId, String profileId, String companyId) {
        int startNumber = Utility.LoadPrefInt(context, companyId + profileId + userId + SELL_NUMBER_STARTING, InvoiceSetting.DEFAULT_NUMBER_STARTING);
        return generateSellInvoiceNumber(context, userId, profileId, companyId, startNumber);
    }

    private static String generateSellInvoiceNumber(Context context, String userId, String profileId, String companyId, int finalNumber) {
        String prefix = Utility.LoadPref(context, companyId + profileId + userId + SELL_INVOICE_PREFIX, InvoiceSetting.DEFAULT_INVOICE_PREFIX);
        int numWidth = Utility.LoadPrefInt(context, companyId + profileId + userId + SELL_NUMBER_WIDTH, InvoiceSetting.DEFAULT_NUMBER_WIDTH);
        DecimalFormat formatter;
        switch (numWidth) {
            default:
                formatter = new DecimalFormat("0");
                break;
            case 2:
                formatter = new DecimalFormat("00");
                break;
            case 3:
                formatter = new DecimalFormat("000");
                break;
            case 4:
                formatter = new DecimalFormat("0000");
                break;
            case 5:
                formatter = new DecimalFormat("00000");
                break;
        }
        String aFormatted = formatter.format(finalNumber);
        return prefix + aFormatted;
    }

    public static String generateSellInvoiceWithExistingNumber(Context context, String userId, String profileId, String companyId, String invoiceNumber) {
        String prefix = Utility.LoadPref(context, companyId + profileId + userId + SELL_INVOICE_PREFIX, InvoiceSetting.DEFAULT_INVOICE_PREFIX);
        int numWidth = Utility.LoadPrefInt(context, companyId + profileId + userId + SELL_NUMBER_WIDTH, InvoiceSetting.DEFAULT_NUMBER_WIDTH);
        int innNo = Utility.parseInt(invoiceNumber.replace(prefix, ""));
        innNo++;
        DecimalFormat formatter;
        switch (numWidth) {
            default:
                formatter = new DecimalFormat("0");
                break;
            case 2:
                formatter = new DecimalFormat("00");
                break;
            case 3:
                formatter = new DecimalFormat("000");
                break;
            case 4:
                formatter = new DecimalFormat("0000");
                break;
            case 5:
                formatter = new DecimalFormat("00000");
                break;
        }
        String aFormatted = formatter.format(innNo);
        return prefix + aFormatted;
    }

    public static String generateVoucherWithExistingNumber(Context context, String userId, String profileId, String companyId, String invoiceNumber) {
        int numWidth = invoiceNumber.length();
        int innNo = Utility.parseInt(invoiceNumber);
        innNo++;
        DecimalFormat formatter;
        switch (numWidth) {
            default:
                formatter = new DecimalFormat("0");
                break;
            case 2:
                formatter = new DecimalFormat("00");
                break;
            case 3:
                formatter = new DecimalFormat("000");
                break;
            case 4:
                formatter = new DecimalFormat("0000");
                break;
            case 5:
                formatter = new DecimalFormat("00000");
                break;
        }
        String aFormatted = formatter.format(innNo);
        return aFormatted;
    }

    public static boolean isBatchAvailable(Context context) {
        return Utility.LoadPrefBoolean(context, BATCH_AVAILABLE, false);
    }

    public static void setBatchAvailable(Context context, Boolean status) {
        Utility.SavePrefBoolean(context, BATCH_AVAILABLE, status);
    }

    public static boolean isExpAvailable(Context context) {
        return Utility.LoadPrefBoolean(context, EXP_AVAILABLE, false);
    }

    public static void setExpAvailable(Context context, Boolean status) {
        Utility.SavePrefBoolean(context, EXP_AVAILABLE, status);
    }

    public static boolean isMrpAvailable(Context context) {
        return Utility.LoadPrefBoolean(context, MRP_AVAILABLE, false);
    }

    public static void setMrpAvailable(Context context, Boolean status) {
        Utility.SavePrefBoolean(context, MRP_AVAILABLE, status);
    }

    public static boolean isBankAvailable(Context context) {
        return Utility.LoadPrefBoolean(context, BANK_AVAILABLE, true);
    }

    public static void setBankAvailable(Context context, Boolean status) {
        Utility.SavePrefBoolean(context, BANK_AVAILABLE, status);
    }

    public static boolean isIGSTAvailable(Context context) {
        return Utility.LoadPrefBoolean(context, IGST_AVAILABLE, true);
    }

    public static void setIGSTAvailable(Context context, Boolean status) {
        Utility.SavePrefBoolean(context, IGST_AVAILABLE, status);
    }

    public static boolean isVehicelAvailable(Context context) {
        return Utility.LoadPrefBoolean(context, VEHICEL_AVAILABLE, true);
    }

    public static void setVehicelAvailable(Context context, Boolean status) {
        Utility.SavePrefBoolean(context, VEHICEL_AVAILABLE, status);
    }

    public static boolean isDLNoAvailable(Context context) {
        return Utility.LoadPrefBoolean(context, DLNO_AVAILABLE, false);
    }

    public static void setDLNoAvailable(Context context, Boolean status) {
        Utility.SavePrefBoolean(context, DLNO_AVAILABLE, status);
    }


     public static boolean isPOSAvailable(Context context) {
        return Utility.LoadPrefBoolean(context, POS_AVAILABLE, false);
    }

    public static void setPOSAvailable(Context context, Boolean status) {
        Utility.SavePrefBoolean(context, POS_AVAILABLE, status);
    }


    public static String getPurchaseInvoiceEmailBody(Context context, String userId, String profileId, String defaultBody) {
        return Utility.LoadPref(context, profileId + userId + PURCHASE_INVOICE_EMAIL_BODY, defaultBody);
    }

    public static void setPurchaseInvoiceEmailBody(Context context, String val, String userId, String profileId) {
        Utility.SavePref(context, profileId + userId + PURCHASE_INVOICE_EMAIL_BODY, val);
    }


    public static boolean getSerialAvailable(Context context) {
        return Utility.LoadPrefBoolean(context, SERIAL_AVAILABLE, true);
    }

    public static void setSerialAvailable(Context context, Boolean status) {
        Utility.SavePrefBoolean(context, SERIAL_AVAILABLE, status);
    }

    public static boolean getUnitAvailable(Context context) {
        return Utility.LoadPrefBoolean(context, UNIT_AVAILABLE, true);
    }

    public static void setUnitAvailable(Context context, Boolean status) {
        Utility.SavePrefBoolean(context, UNIT_AVAILABLE, status);
    }
    public static boolean getDashboardMenu(Context context) {
        return Utility.LoadPrefBoolean(context, DASHBOARD_MENU, true);
    }

    public static void setDashboardMenu(Context context, Boolean status) {
        Utility.SavePrefBoolean(context, DASHBOARD_MENU, status);
    }
    public static boolean getSalePurchaseChart(Context context) {
        return Utility.LoadPrefBoolean(context, SALE_PURCHASE_CHART, true);
    }

    public static void setSalePurchaseChart(Context context, Boolean status) {
        Utility.SavePrefBoolean(context, SALE_PURCHASE_CHART, status);
    }
    public static boolean getDebtorCreditorChart(Context context) {
        return Utility.LoadPrefBoolean(context, DEBTOR_CREDITOR_CHART, true);
    }

    public static void setDebtorCreditorChart(Context context, Boolean status) {
        Utility.SavePrefBoolean(context, DEBTOR_CREDITOR_CHART, status);
    }
     public static boolean getTaxPayableChart(Context context) {
        return Utility.LoadPrefBoolean(context, TAX_PAYABLE_CHART, true);
    }

    public static void setTaxPayableChart(Context context, Boolean status) {
        Utility.SavePrefBoolean(context, TAX_PAYABLE_CHART, status);
    }
    public static boolean getHSNAvailable(Context context) {
        return Utility.LoadPrefBoolean(context, HSN_AVAILABLE, true);
    }

    public static void setHSNAvailable(Context context, Boolean status) {
        Utility.SavePrefBoolean(context, HSN_AVAILABLE, status);
    }
    public static boolean getTAXAvailable(Context context) {
        return Utility.LoadPrefBoolean(context, TAX_AVAILABLE, true);
    }

    public static void setTAXAvailable(Context context, Boolean status) {
        Utility.SavePrefBoolean(context, TAX_AVAILABLE, status);
    }
    public static boolean getDiscountAvailable(Context context) {
        return Utility.LoadPrefBoolean(context, DISCOUNT_AVAILABLE, true);
    }

    public static void setDiscountAvailable(Context context, Boolean status) {
        Utility.SavePrefBoolean(context, DISCOUNT_AVAILABLE, status);
    }
    public static boolean getCessAvailable(Context context) {
        return Utility.LoadPrefBoolean(context, CESS_AVAILABLE, true);
    }

    public static void setCessAvailable(Context context, Boolean status) {
        Utility.SavePrefBoolean(context, CESS_AVAILABLE, status);
    }
    public static boolean getTaxableAvailable(Context context) {
        return Utility.LoadPrefBoolean(context, TAXABLE_AVAILABLE, true);
    }

    public static void setTaxableAvailable(Context context, Boolean status) {
        Utility.SavePrefBoolean(context, TAXABLE_AVAILABLE, status);
    }
    public static boolean getBlankRowsAvailable(Context context) {
        return Utility.LoadPrefBoolean(context, BLANKROWS_AVAILABLE, false);
    }

    public static void setBlankRowsAvailable(Context context, Boolean status) {
        Utility.SavePrefBoolean(context, BLANKROWS_AVAILABLE, status);
    }
    public static boolean getOptionBarcode(Context context) {
        return Utility.LoadPrefBoolean(context, OPTION_BARCODE_AVAILABLE, true);
    }

    public static void setOptionBarcode(Context context, Boolean status) {
        Utility.SavePrefBoolean(context, OPTION_BARCODE_AVAILABLE, status);
    }


}
