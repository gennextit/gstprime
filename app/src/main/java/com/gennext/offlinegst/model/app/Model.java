package com.gennext.offlinegst.model.app;

/**
 * Created by Abhijit on 13-Dec-16.
 */

public class Model {

    private String id;
    private String date;
    private String output;
    private String outputMsg;
    private String outputDB;
    private String outputDBMsg;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOutputDBMsg() {
        return outputDBMsg;
    }

    public void setOutputDBMsg(String outputDBMsg) {
        this.outputDBMsg = outputDBMsg;
    }

    public String getOutputDB() {
        return outputDB;
    }

    public void setOutputDB(String outputDB) {
        this.outputDB = outputDB;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }
}
