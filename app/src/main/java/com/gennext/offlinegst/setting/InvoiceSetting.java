package com.gennext.offlinegst.setting;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppUser;

/**
 * Created by Admin on 7/4/2017.
 */

public class InvoiceSetting extends CompactFragment implements CompoundButton.OnCheckedChangeListener {

    public static final String DEFAULT_INVOICE_PREFIX = "INV-";
    public static final int DEFAULT_NUMBER_STARTING = 1;
    public static final int DEFAULT_NUMBER_WIDTH = 3;

    private String userId;
    private String profileId, companyId;
    private Switch swSerial, swUnit, swHSN, swTax, swMrp, swDiscount,swCess, swTaxable, swBatch, swExp, swBank, swVehicel, swDLNo, swBlankRows,swIGST,swOptionBarcode;
    private Context mContext;

    public static Fragment newInstance() {
        InvoiceSetting fragment = new InvoiceSetting();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.setting_invoice, container, false);
        initToolBar(getActivity(), v, getString(R.string.invoice_setting));
        mContext = getContext();
        userId = AppUser.getUserId(getActivity());
        profileId = AppUser.getProfileId(getActivity());
        companyId = AppUser.getProfileId(getActivity());
        initUi(v);
        updateUi();
        return v;
    }

    private void updateUi() {
        swSerial.setChecked(AppConfig.getSerialAvailable(mContext));
        swUnit.setChecked(AppConfig.getUnitAvailable(mContext));
        swHSN.setChecked(AppConfig.getHSNAvailable(mContext));
        swTax.setChecked(AppConfig.getTAXAvailable(mContext));
        swMrp.setChecked(AppConfig.isMrpAvailable(mContext));
        swDiscount.setChecked(AppConfig.getDiscountAvailable(mContext));
        swCess.setChecked(AppConfig.getCessAvailable(mContext));
        swTaxable.setChecked(AppConfig.getTaxableAvailable(mContext));
        swBatch.setChecked(AppConfig.isBatchAvailable(mContext));
        swExp.setChecked(AppConfig.isExpAvailable(mContext));
        swBank.setChecked(AppConfig.isBankAvailable(mContext));
        swVehicel.setChecked(AppConfig.isVehicelAvailable(mContext));
        swDLNo.setChecked(AppConfig.isDLNoAvailable(mContext));
        swIGST.setChecked(AppConfig.isIGSTAvailable(mContext));
        swBlankRows.setChecked(AppConfig.getBlankRowsAvailable(mContext));
        swOptionBarcode.setChecked(AppConfig.getOptionBarcode(mContext));
    }

    private void initUi(View v) {
        swSerial = (Switch) v.findViewById(R.id.sw_invoice_serial);
        swUnit = (Switch) v.findViewById(R.id.sw_invoice_unit);
        swHSN = (Switch) v.findViewById(R.id.sw_invoice_hsn);
        swTax = (Switch) v.findViewById(R.id.sw_invoice_tax);
        swMrp = (Switch) v.findViewById(R.id.sw_invoice_mrp);
        swDiscount = (Switch) v.findViewById(R.id.sw_invoice_discount);
        swCess = (Switch) v.findViewById(R.id.sw_invoice_cess);
        swTaxable = (Switch) v.findViewById(R.id.sw_invoice_taxable);
        swBatch = (Switch) v.findViewById(R.id.sw_invoice_batch);
        swExp = (Switch) v.findViewById(R.id.sw_invoice_exp);
        swBank = (Switch) v.findViewById(R.id.sw_invoice_bank);
        swVehicel = (Switch) v.findViewById(R.id.sw_invoice_vehicel);
        swDLNo = (Switch) v.findViewById(R.id.sw_invoice_dlno);
        swIGST = (Switch) v.findViewById(R.id.sw_invoice_igst);
        swBlankRows = (Switch) v.findViewById(R.id.sw_invoice_blank_rows);
        swOptionBarcode = (Switch) v.findViewById(R.id.sw_invoice_mplti_product_selection);

        swSerial.setOnCheckedChangeListener(this);
        swUnit.setOnCheckedChangeListener(this);
        swHSN.setOnCheckedChangeListener(this);
        swTax.setOnCheckedChangeListener(this);
        swDiscount.setOnCheckedChangeListener(this);
        swCess.setOnCheckedChangeListener(this);
        swMrp.setOnCheckedChangeListener(this);
        swTaxable.setOnCheckedChangeListener(this);
        swMrp.setOnCheckedChangeListener(this);
        swBatch.setOnCheckedChangeListener(this);
        swExp.setOnCheckedChangeListener(this);
        swBank.setOnCheckedChangeListener(this);
        swVehicel.setOnCheckedChangeListener(this);
        swDLNo.setOnCheckedChangeListener(this);
        swIGST.setOnCheckedChangeListener(this);
        swBlankRows.setOnCheckedChangeListener(this);
        swOptionBarcode.setOnCheckedChangeListener(this);

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.sw_invoice_serial:
                AppConfig.setSerialAvailable(mContext, isChecked);
                break;
            case R.id.sw_invoice_unit:
                AppConfig.setUnitAvailable(mContext, isChecked);
                break;
            case R.id.sw_invoice_hsn:
                AppConfig.setHSNAvailable(mContext, isChecked);
                break;
            case R.id.sw_invoice_tax:
                AppConfig.setTAXAvailable(mContext, isChecked);
                break;
            case R.id.sw_invoice_mrp:
                AppConfig.setMrpAvailable(mContext, isChecked);
                break;
            case R.id.sw_invoice_discount:
                AppConfig.setDiscountAvailable(mContext, isChecked);
                break;
            case R.id.sw_invoice_cess:
                AppConfig.setCessAvailable(mContext, isChecked);
                break;
            case R.id.sw_invoice_taxable:
                AppConfig.setTaxableAvailable(mContext, isChecked);
                break;
            case R.id.sw_invoice_batch:
                AppConfig.setBatchAvailable(mContext, isChecked);
                break;
            case R.id.sw_invoice_exp:
                AppConfig.setExpAvailable(mContext, isChecked);
                break;
            case R.id.sw_invoice_bank:
                AppConfig.setBankAvailable(mContext, isChecked);
                break;
            case R.id.sw_invoice_vehicel:
                AppConfig.setVehicelAvailable(mContext, isChecked);
                break;
            case R.id.sw_invoice_dlno:
                AppConfig.setDLNoAvailable(mContext, isChecked);
                break;
            case R.id.sw_invoice_igst:
                AppConfig.setIGSTAvailable(mContext, isChecked);
                break;
            case R.id.sw_invoice_blank_rows:
                AppConfig.setBlankRowsAvailable(mContext, isChecked);
                break;
            case R.id.sw_invoice_mplti_product_selection:
                AppConfig.setOptionBarcode(mContext, isChecked);
                break;

        }
    }
}
