package com.gennext.offlinegst.model.user;

/**
 * Created by Admin on 7/28/2017.
 */

public class CommonModel {

    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
