package com.gennext.offlinegst.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gennext.offlinegst.MainActivity;
import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppUser;

/**
 * Created by Admin on 7/4/2017.
 */

public class Transactions extends CompactFragment {

    public static Fragment newInstance() {
        Transactions fragment=new Transactions();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.user_transactions,container,false);
        ((MainActivity)getActivity()).updateTitle("Transactions");
        screenAnalytics(getContext(), AppUser.getUserId(getContext()),"Transactions");
        initUi(v);
        return v;
    }

    private void initUi(View v) {
    }
}
