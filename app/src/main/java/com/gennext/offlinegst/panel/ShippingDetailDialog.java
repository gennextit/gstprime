package com.gennext.offlinegst.panel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.model.user.StateModel;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.FieldValidation;
import com.gennext.offlinegst.util.Utility;

import java.util.Map;


public class ShippingDetailDialog extends DialogFragment implements StateSelector.SelectListener,View.OnClickListener{

    private ShipDetail mShipDetailListener;
    private String name,GSTIN,address;
    private EditText etName,etGSTIN,etAddress,etState;
    private String shipStateCode,shipStateName;
    private Map<String, String> defaultStateList;

    @Override
    public void onStateSelect(DialogFragment dialog, StateModel selectedState) {
        shipStateCode=selectedState.getStateCode();
        shipStateName=selectedState.getStateName();
        etState.setText(shipStateName);
        FieldValidation.isEmpty(getContext(), etState, getString(R.string.invalid_state));
    }

    public interface ShipDetail {
        void onUpdateShippingDetailClick(DialogFragment dialog, String name, String GSTIN, String address, String shipStateCode,String shipStateName);
    }

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static ShippingDetailDialog newInstance(ShipDetail mShipDetailListener, String name, String GSTIN,String address,String shipStateCode,String shipStateName) {
        ShippingDetailDialog fragment = new ShippingDetailDialog();
        fragment.mShipDetailListener = mShipDetailListener;
        fragment.name = name;
        fragment.GSTIN = GSTIN;
        fragment.address = address;
        fragment.shipStateCode = shipStateCode;
        fragment.shipStateName = shipStateName;
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_shipping_detail, container, false);

        TextView tvTitle = (TextView) v.findViewById(R.id.action_menu_title);
        ImageView ivClose = (ImageView) v.findViewById(R.id.action_menu_close);
        etName = (EditText) v.findViewById(R.id.et_customer_name);
        etGSTIN = (EditText) v.findViewById(R.id.et_customer_gstin);
        etAddress = (EditText) v.findViewById(R.id.et_customer_address);
        etState = (EditText) v.findViewById(R.id.et_customer_state);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
        defaultStateList = AppUser.defaultStates(getContext());

        etName.setText(name!=null?name:"");
        etGSTIN.setText(GSTIN!=null?GSTIN:"");
        etAddress.setText(address!=null?address:"");
        etState.setText(shipStateName!=null?shipStateName:"");

        etGSTIN.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setStateInUi(etGSTIN.getText().toString());
            }
        });

        ivClose.setOnClickListener(this);
        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        etState.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.action_menu_close:
                dismiss();
                break;
            case R.id.btn_alert_dialog_button1:
                if (mShipDetailListener != null) {
                    if (!FieldValidation.isGSTIfAvailable(getContext(), etGSTIN, getString(R.string.invalid_gst_number),true)) {
                        return;
                    } else if (!FieldValidation.isEmpty(getContext(), etState, getString(R.string.invalid_state))) {
                        return;
                    }
                    mShipDetailListener.onUpdateShippingDetailClick(ShippingDetailDialog.this, etName.getText().toString(), etGSTIN.getText().toString(),etAddress.getText().toString(),shipStateCode,etState.getText().toString());
                    dismiss();
                }
                break;
            case R.id.btn_alert_dialog_button2:
                dismiss();
                break;
            case R.id.et_customer_state:
                StateSelector.newInstance(ShippingDetailDialog.this).show(getFragmentManager(), "stateSelector");
                break;
        }


    }
    private void setStateInUi(String gstinNo) {
        if(gstinNo.length()==15){
            String stateCode = gstinNo.substring(0, 2);
            int sCode= Utility.parseInt(stateCode);
            String stateName = AppUser.getStateName(defaultStateList, String.valueOf(sCode));
            if(!TextUtils.isEmpty(stateName)){
                shipStateCode=stateCode;
                shipStateName=stateName;
                etState.setText(stateName);
            }else{
                shipStateCode=stateCode;
                shipStateName=stateName;
                etState.setText(stateName);
            }
        }else{
            shipStateCode="";
            shipStateName="";
            etState.setText("");
        }
    }

}
