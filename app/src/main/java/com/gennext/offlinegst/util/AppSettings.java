package com.gennext.offlinegst.util;

/**
 * Created by Abhijit on 13-Dec-16.
 */

/**
 * ERROR-101 (Unformetted Json Response)
 * ERROR-102 ()
 */
public class AppSettings {

    public static final String LOCALHOST = "http://192.168.88.105";
//    public static final String SERVER = "http://www.techlyt.com";
    public static final String SERVER = "https://www.gstprime.co.in";
//    public static final String COMMON = LOCALHOST+ "/gst/App/";
    public static final String COMMON = SERVER+ "/App/";

    public static final String SEND_FEEDBACK  = COMMON + "sendFeedback "; //personName,bankId,branch,accountNumber,ifscCode,userId,checkbookPassbook
    public static final String TERMS_OF_USE  = SERVER + "/files/Terms%20of%20Use.pdf"; //personName,bankId,branch,accountNumber,ifscCode,userId,checkbookPassbook
    public static final String PRIVACY_PACY_POLICY  = SERVER + "/files/Privacy%20Policy.pdf"; //personName,bankId,branch,accountNumber,ifscCode,userId,checkbookPassbook


    public static final String REPORT_SERVER_ERROR = COMMON + "serverErrorReporting";;
    public static final String LOGIN = COMMON + "login";;
    public static final String ADD_PROFILE = COMMON +"addProfile";
    public static final String UPDATE_PROFILE = COMMON + "updateProfile";
    public static final String SAVE_COMPANY_DETAIL = COMMON+ "saveCompanyDetails";
    public static final String GET_PRODUCTS = COMMON +"getProducts";
    public static final String DELETE_PRODUCT = COMMON +"deleteProduct";
    public static final String ADD_PRODUCT = COMMON + "saveProductDetails";
    public static final String EDIT_PRODUCT = COMMON + "editProductDetails";
    public static final String ADD_SERVICES = COMMON + "addServices";
    public static final String GET_SERVICES = COMMON + "getServiceDetails";
    public static final String ADD_CUSTOMER = COMMON +"saveCustomerDetails";
    public static final String GET_CUSTOMERS = COMMON + "getCustomers";
    public static final String DELETE_CUSTOMER = COMMON + "deleteCustomer";
    public static final String ADD_VENDOR = COMMON + "saveVendorDetails";
    public static final String GET_VENDORS = COMMON + "getVendors";
    public static final String DELETE_VENDOR = COMMON + "deleteVendor";
    public static final String ADD_INVOICE = COMMON +"addInvoice";
    public static final String GET_INVOICES = COMMON + "getProductInvoices";
    public static final String GET_COMPANY_DETAILS = COMMON + "getCompanyDetails";
    public static final String GET_PURCHASES = COMMON + "getProductPurchases";
//    public static final String CANCEL_PURCHASES = COMMON + "cancelProductPurchase";
    public static final String ADD_VOUCHER = COMMON + "addVoucher";
    public static final String GET_VOUCHERS = COMMON + "getVouchers";
    public static final String SAVE_INVOICE_DETAILS = COMMON + "saveProductInvoiceDetails";
    public static final String SAVE_SERVICE_DETAILS = COMMON + "saveServiceInvoiceDetails";
    public static final String GENERATE_INVOICE_NO = COMMON+"generateProductInvoiceNumber";
    public static final String SAVE_PURCHASE_DETAILS = COMMON+"saveProductPurchaseDetails";
    public static final String CANCEL_INVOICES = COMMON+"cancelInvoice";// type=G for goods and type=S for services
    public static final String GET_SERVICE_INVOICES = COMMON+"getServiceInvoices";
    public static final String GET_SERVICE_PURCHASES = COMMON + "getServicePurchase";
//    public static final String CANCEL_SERVICE_PURCHASES = COMMON + "cancelServicePurchase";
    public static final String DELETE_PURCHASE = COMMON + "deletePurchase";// type=G for goods and type=S for services
    public static final String SAVE_PURCHASE_SERVICE_DETAILS = COMMON + "saveServicePurchaseDetails";
    public static final String DASHBOARD = COMMON + "dashboard";
}

/* New API's required for GST prime App

API 1:   /login

Params: email,mobile

Response: success/failure.

API 2:   /addProfile

Params: userId,name,email,phone,pan

Response: success/failure.

API 3:   /updateProfile

Params: userId,profileId,name,email,phone,pan

Response: success/failure.

API 4:   /updateCompanyDetail

Params: userId,profileId,companyName,telephone,email,gstin,state,stateCode,address,dlNumber,tAndC,accName,ifscCode,accNumber,branchAddress

Response: success/failure.

API 5:   /getProducts

Params: userId

Response: success/failure with list of items


*/

/* New API's required for BizzzWizzz App

API 1:   http://bizzzwizzz.com/index.php/Rest/signup

Params: name,mobile,email,panNo,pass,profileType

Response: success/failure.(return success and basic info similar to login api)

*/

/*
    1 1xx Informational.
    2 2xx Success.
    3 3xx Redirection.
    4 4xx Client Error.
    5 5xx Server Error.
*/
