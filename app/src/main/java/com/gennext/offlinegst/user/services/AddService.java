package com.gennext.offlinegst.user.services;


/**
 * Created by Admin on 5/22/2017.
 */

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.model.user.CommonModel;
import com.gennext.offlinegst.model.user.ServiceModel;
import com.gennext.offlinegst.panel.TaxCategorySelector;
import com.gennext.offlinegst.panel.TaxSelector;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.user.services.purchase.ServicePurchaseManager;
import com.gennext.offlinegst.user.services.sale.ServiceManager;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.FieldValidation;
import com.gennext.offlinegst.util.ProgressButton;
import com.gennext.offlinegst.util.Utility;


public class AddService extends CompactFragment implements TaxCategorySelector.SelectListener, TaxSelector.SelectListener {

    private static final int ADD_SERVICE = 1, EDIT_SERVICE = 2;
    private EditText etName, etSAC, etCostPrice, etSalePrice, etTaxRate, etTaxCategory;
    private static final int TYPE_RUPEE = 0, TYPE_PERCENT = 1;
    private CoordinatorLayout coordinatorLayout;
    private ServiceModel itemService;
    private int serviceType;

    private String mTaxRate;
    private EditText etNetAmount, etDiscount;
    private ProgressButton btnAction;
    private String mTaxCategoryId = "", mTaxCategoryName;
    private ServiceManager serviceManager;
    private RadioButton rbRupee;
    private int mDiscountType;
    private float mDiscountValue;
    private String mNetAmount;
    private ServicePurchaseManager servicePurchaseManager;
    private int position;
    private LinearLayout llDiscount;
    private String mVendorName;


    public static Fragment newInstance(ServiceManager serviceManager) {
        AddService fragment = new AddService();
        AppAnimation.setSlideAnimation(fragment, Gravity.RIGHT);
        fragment.serviceType = ADD_SERVICE;
        fragment.serviceManager = serviceManager;
        return fragment;
    }


    public static Fragment newInstance(ServiceManager serviceManager, ServiceModel itemService,int position) {
        AddService fragment = new AddService();
        AppAnimation.setSlideAnimation(fragment, Gravity.RIGHT);
        fragment.serviceType = ADD_SERVICE;
        fragment.itemService = itemService;
        fragment.position = position;
        fragment.serviceManager = serviceManager;
        return fragment;
    }


    public static Fragment newInstance(ServicePurchaseManager servicePurchaseManager, ServiceModel itemService, int position, String mVendorName) {
        AddService fragment = new AddService();
        AppAnimation.setSlideAnimation(fragment, Gravity.RIGHT);
        fragment.serviceType = ADD_SERVICE;
        fragment.position = position;
        fragment.itemService = itemService;
        fragment.mVendorName = mVendorName;
        fragment.servicePurchaseManager = servicePurchaseManager;
        return fragment;
    }


    public static Fragment newInstance(ServicePurchaseManager servicePurchaseManager, String mVendorName) {
        AddService fragment = new AddService();
        AppAnimation.setSlideAnimation(fragment, Gravity.RIGHT);
        fragment.serviceType = ADD_SERVICE;
        fragment.mVendorName = mVendorName;
        fragment.servicePurchaseManager = servicePurchaseManager;
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.user_add_service, container, false);

        initToolBar(getActivity(), v, getString(R.string.services));
        screenAnalytics(getContext(), AppUser.getUserId(getContext()), "Add Services");
        initUI(v);

        updateUi();
        return v;
    }


    private void updateUi() {
        if (itemService != null) {
            etName.setText(itemService.getServiceName() != null ? itemService.getServiceName() : "");
            etSAC.setText(itemService.getCodeSAC() != null ? itemService.getCodeSAC() : "");
            etSalePrice.setText(itemService.getSalePrice() != null ? itemService.getSalePrice() : "");
            etTaxRate.setText(itemService.getTaxRate() != null ? itemService.getTaxRate() : "");
            mTaxRate = itemService.getTaxRate();
            mTaxCategoryId = itemService.getTaxCategoryId();
            mTaxCategoryName = itemService.getTaxCategory();
            if (mTaxRate.equals("0") || mTaxRate.equals("00") || mTaxRate.equals("0.0")) {
                etTaxCategory.setVisibility(View.VISIBLE);
            } else {
                etTaxCategory.setVisibility(View.GONE);
            }
            etTaxCategory.setText(mTaxCategoryName);

            String itemSale = itemService.getSalePrice();
            String taxRate = itemService.getTaxRate();

            rbRupee.setChecked(true);
            etDiscount.setText(itemService.getDis());

            if (itemSale != null && taxRate != null) {
                setProductNetAmount(mDiscountType, parseFloat(etDiscount.getText().toString()), parseFloat(etSalePrice.getText().toString()), parseFloat(etTaxRate.getText().toString()));
            }
        }
        if(servicePurchaseManager!=null){
            llDiscount.setVisibility(View.GONE);
        }
        if(mVendorName!=null&&mVendorName.equalsIgnoreCase(Const.CASH_PURCHASE)) {
            etTaxRate.setText("0");
            etTaxCategory.setVisibility(View.GONE);
            etTaxRate.setClickable(true);
            etTaxRate.setFocusable(false);
            etTaxRate.setFocusableInTouchMode(false);
            etTaxRate.setBackgroundResource(R.drawable.et_row_disable);
        }
    }

//    private void setProductNetAmount(float salePrice, float taxRate) {
//        float totalTax=(salePrice*(taxRate))/100;
//        etNetAmount.setText(parseString(salePrice+totalTax));
//    }


    private void initUI(View v) {
        coordinatorLayout = (CoordinatorLayout) v.findViewById(R.id.coordinatorLayout);
        llDiscount = (LinearLayout) v.findViewById(R.id.ll_discount);
        etName = (EditText) v.findViewById(R.id.et_service_name);
        etSAC = (EditText) v.findViewById(R.id.et_service_sac);
        etCostPrice = (EditText) v.findViewById(R.id.et_service_cost);
        etSalePrice = (EditText) v.findViewById(R.id.et_service_sale);
        etTaxRate = (EditText) v.findViewById(R.id.et_service_tax);
        etTaxCategory = (EditText) v.findViewById(R.id.et_service_tax_category);
        etNetAmount = (EditText) v.findViewById(R.id.et_service_net_amount);
        etDiscount = (EditText) v.findViewById(R.id.et_product_discount);

        etTaxRate.setText("18");
        mTaxRate = "18";
        etTaxCategory.setVisibility(View.GONE);

        etSalePrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setProductNetAmount(mDiscountType, parseFloat(etDiscount.getText().toString()), parseFloat(etSalePrice.getText().toString()), parseFloat(etTaxRate.getText().toString()));
            }
        });

        etDiscount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setProductNetAmount(mDiscountType, parseFloat(etDiscount.getText().toString()), parseFloat(etSalePrice.getText().toString()), parseFloat(etTaxRate.getText().toString()));
            }
        });


        etSalePrice.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        etSalePrice.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    // Handle pressing "Enter" key here
                    TaxSelector.newInstance(AddService.this).show(getFragmentManager(), "taxSelector");
                    handled = true;
                }
                return handled;
            }
        });

        btnAction = ProgressButton.newInstance(getContext(), v);
        btnAction.setOnEditorActionListener(etSalePrice);
        btnAction.setText(getString(R.string.add));

        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FieldValidation.isProductName(getContext(), etName, true)) {
                    return;
                } else if (!FieldValidation.isHSNCode(getContext(), etSAC, getString(R.string.invalid_sac_code))) {
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), etSalePrice, getString(R.string.invalid_sale_price))) {
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), etTaxRate, getString(R.string.invalid_tax_rate))) {
                    return;
                } else if (etTaxCategory.getVisibility() == View.VISIBLE && !FieldValidation.isEmpty(getContext(), etTaxCategory, getString(R.string.invalid_tax_category))) {
                    return;
                }
                hideKeybord(getActivity());
                addService(etName.getText().toString(), etSAC.getText().toString(), etCostPrice.getText().toString()
                        , etSalePrice.getText().toString(), etTaxRate.getText().toString(), etTaxCategory.getText().toString(), mTaxCategoryId, mNetAmount, String.valueOf(mDiscountValue));


            }
        });


        etTaxRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mVendorName!=null&&!mVendorName.equalsIgnoreCase(Const.CASH_PURCHASE)) {
                    TaxSelector.newInstance(AddService.this).show(getFragmentManager(), "taxSelector");
                }
            }
        });
        etTaxCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TaxCategorySelector.newInstance(AddService.this).show(getFragmentManager(), "taxCategorySelector");
            }
        });

        RadioGroup rgPaymentType = (RadioGroup) v.findViewById(R.id.rg_discount_type);
        rbRupee = (RadioButton) v.findViewById(R.id.type_rupee);
        mDiscountType = TYPE_PERCENT;
        rgPaymentType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.type_percent:
                        mDiscountType = TYPE_PERCENT;
                        setProductNetAmount(mDiscountType, parseFloat(etDiscount.getText().toString()), parseFloat(etSalePrice.getText().toString()), parseFloat(etTaxRate.getText().toString()));
                        break;
                    case R.id.type_rupee:
                        mDiscountType = TYPE_RUPEE;
                        setProductNetAmount(mDiscountType, parseFloat(etDiscount.getText().toString()), parseFloat(etSalePrice.getText().toString()), parseFloat(etTaxRate.getText().toString()));
                        break;
                }
            }
        });
    }

    private void setProductNetAmount(int discountType, float discount, float salePrice, float taxRate) {
        if (discountType == TYPE_RUPEE) {
            mDiscountValue = discount;
            float afterDiscount = (salePrice - discount);
            float totalTax = (afterDiscount * (taxRate)) / 100;
            mNetAmount = Utility.decimalFormat((afterDiscount + totalTax), "#.##");
            etNetAmount.setText(mNetAmount);
        } else {
            float afterDiscount = (salePrice * discount) / 100;
            mDiscountValue = afterDiscount;
            float discValue = salePrice - afterDiscount;
            float totalTax = (discValue * (taxRate)) / 100;
            mNetAmount = Utility.decimalFormat((discValue + totalTax), "#.##");
            etNetAmount.setText(mNetAmount);
        }
    }


    @Override
    public void onTaxSelect(DialogFragment dialog, CommonModel selectedItem) {
        mTaxRate = selectedItem.getId();
        etTaxRate.setText(selectedItem.getName());
        FieldValidation.isEmpty(getContext(), etTaxRate, getString(R.string.invalid_tax_rate));
        setProductNetAmount(mDiscountType, parseFloat(etDiscount.getText().toString()), parseFloat(etSalePrice.getText().toString()), parseFloat(etTaxRate.getText().toString()));
        if (mTaxRate.equals("0") || mTaxRate.equals("00") || mTaxRate.equals("0.0")) {
            etTaxCategory.setVisibility(View.VISIBLE);
            TaxCategorySelector.newInstance(AddService.this).show(getFragmentManager(), "taxCategorySelector");
        } else {
            etTaxCategory.setVisibility(View.GONE);
        }
    }

    @Override
    public void onTaxCategorySelect(DialogFragment dialog, CommonModel selectedTerm) {
        mTaxCategoryId = selectedTerm.getId();
        mTaxCategoryName = selectedTerm.getName();
        etTaxCategory.setText(mTaxCategoryName);
        FieldValidation.isEmpty(getContext(), etTaxCategory, getString(R.string.invalid_tax_category));
    }


    private void addService(String serviceName, String SACCode, String costPrice, String salePrice
            , String taxRate, String taxCategory, String taxCategoryId, String netAmount, String discount) {
        ServiceModel serviceModel = new ServiceModel();
        serviceModel.setServiceName(serviceName);
        serviceModel.setCodeSAC(SACCode);
        serviceModel.setSalePrice(salePrice);
        serviceModel.setTaxRate(taxRate);
        serviceModel.setTaxCategory(taxCategory);
        serviceModel.setTaxCategoryId(taxCategoryId);
        serviceModel.setNetAmount(netAmount);
        serviceModel.setDis(discount);
        if (serviceManager != null) {
            if(itemService==null){
                serviceManager.addListItemByModel(serviceModel);
            }else{
                serviceManager.addListItemByModel(serviceModel,position);
            }
        }
        if (servicePurchaseManager != null) {
            if(itemService==null){
                servicePurchaseManager.addListItemByModel(serviceModel);
            }else{
                servicePurchaseManager.addListItemByModel(serviceModel,position);
            }
        }

        getFragmentManager().popBackStack();
    }
}
