package com.gennext.offlinegst.model.user;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.user.reports.InvoicesReport;
import com.gennext.offlinegst.util.Utility;

import java.util.ArrayList;

public class InvoiceRepAdapter extends RecyclerView.Adapter<InvoiceRepAdapter.ReyclerViewHolder> {

    private final InvoicesReport parentRef;
    private final FragmentManager fragmentManager;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<PaymentModel> items;

    public InvoiceRepAdapter(Context context, ArrayList<PaymentModel> items, InvoicesReport parentRef, FragmentManager fragmentManager) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef=parentRef;
        this.fragmentManager=fragmentManager;
        this.items = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_report_invoice, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final PaymentModel item = items.get(position);

        holder.tvPersonName.setText(item.getCustomerName());
        holder.tvInvoiceNo.setText(item.getInvoiceNo());
        if(item.getPaymentMode().equalsIgnoreCase(Const.CREDIT)){
            holder.tvStatus.setText(context.getString(R.string.unpaid));
            holder.tvStatus.setTextColor(ContextCompat.getColor(context,R.color.statusCancelled));
        }else{
            float balAmt = Utility.parseFloat(item.getBalanceAmount());
            if(balAmt<=0){
                holder.tvStatus.setText(context.getString(R.string.paid));
                holder.tvStatus.setTextColor(ContextCompat.getColor(context,R.color.statusInprogress));
            }else{
                holder.tvStatus.setText(context.getString(R.string.partially));
                holder.tvStatus.setTextColor(ContextCompat.getColor(context,R.color.statusComplete));
            }
        }



        holder.llSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parentRef.openReportDetail(item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private final LinearLayout llSlot;
        private TextView tvPersonName,tvInvoiceNo,tvStatus,tvStatusTag;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSlot = (LinearLayout) v.findViewById(R.id.layoutSlot);
            tvPersonName = (TextView) v.findViewById(R.id.tv_slot_1);
            tvInvoiceNo = (TextView) v.findViewById(R.id.tv_slot_2);
            tvStatusTag = (TextView) v.findViewById(R.id.tv_slot_3);
            tvStatus= (TextView) v.findViewById(R.id.tv_slot_4); 
        }
    }


}
