package com.gennext.offlinegst.model.user;

import java.util.ArrayList;

/**
 * Created by Admin on 7/22/2017.
 */

public class ServiceModel {

    private String purchasesServiceId;
    private String serviceId;
    private String serviceUserId;
    private String serviceProfileId;
    private String serviceCompanyId;
    private String serviceName;
    private String invoiceNumber;
    private String codeSAC;
    private String salePrice;
    private String taxRate;
    private String taxCategory;
    private String taxCategoryId;
    private String netAmount;
    private String fromDate;
    private String toDate;
    private String cgstTax;
    private String cgstVal;
    private String sgstTax;
    private String sgstVal;
    private String igstTax;
    private String igstVal;
    private String dis;

    private String output;
    private String outputMsg;
    private String outputDB;
    private String outputDBMsg;
    private ArrayList<ServiceModel> list;

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public String getDis() {
        return dis;
    }

    public void setDis(String dis) {
        this.dis = dis;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getServiceProfileId() {
        return serviceProfileId;
    }

    public void setServiceProfileId(String serviceProfileId) {
        this.serviceProfileId = serviceProfileId;
    }

    public String getServiceCompanyId() {
        return serviceCompanyId;
    }

    public void setServiceCompanyId(String serviceCompanyId) {
        this.serviceCompanyId = serviceCompanyId;
    }

    public String getPurchasesServiceId() {
        return purchasesServiceId;
    }

    public void setPurchasesServiceId(String purchasesServiceId) {
        this.purchasesServiceId = purchasesServiceId;
    }

    public String getCgstTax() {
        return cgstTax;
    }

    public void setCgstTax(String cgstTax) {
        this.cgstTax = cgstTax;
    }

    public String getCgstVal() {
        return cgstVal;
    }

    public void setCgstVal(String cgstVal) {
        this.cgstVal = cgstVal;
    }

    public String getSgstTax() {
        return sgstTax;
    }

    public void setSgstTax(String sgstTax) {
        this.sgstTax = sgstTax;
    }

    public String getSgstVal() {
        return sgstVal;
    }

    public void setSgstVal(String sgstVal) {
        this.sgstVal = sgstVal;
    }

    public String getIgstTax() {
        return igstTax;
    }

    public void setIgstTax(String igstTax) {
        this.igstTax = igstTax;
    }

    public String getIgstVal() {
        return igstVal;
    }

    public void setIgstVal(String igstVal) {
        this.igstVal = igstVal;
    }

    public String getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(String netAmount) {
        this.netAmount = netAmount;
    }

    public String getTaxCategoryId() {
        return taxCategoryId;
    }

    public void setTaxCategoryId(String taxCategoryId) {
        this.taxCategoryId = taxCategoryId;
    }

    public String getTaxCategory() {
        return taxCategory;
    }

    public void setTaxCategory(String taxCategory) {
        this.taxCategory = taxCategory;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceUserId() {
        return serviceUserId;
    }

    public void setServiceUserId(String serviceUserId) {
        this.serviceUserId = serviceUserId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getCodeSAC() {
        return codeSAC;
    }

    public void setCodeSAC(String codeSAC) {
        this.codeSAC = codeSAC;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }

    public String getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(String taxRate) {
        this.taxRate = taxRate;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public String getOutputDB() {
        return outputDB;
    }

    public void setOutputDB(String outputDB) {
        this.outputDB = outputDB;
    }

    public String getOutputDBMsg() {
        return outputDBMsg;
    }

    public void setOutputDBMsg(String outputDBMsg) {
        this.outputDBMsg = outputDBMsg;
    }

    public ArrayList<ServiceModel> getList() {
        return list;
    }

    public void setList(ArrayList<ServiceModel> list) {
        this.list = list;
    }
}
