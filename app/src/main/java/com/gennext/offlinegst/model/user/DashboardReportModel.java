package com.gennext.offlinegst.model.user;

import java.util.ArrayList;

/**
 * Created by Admin on 8/23/2017.
 */

public class DashboardReportModel {

    private String paidByCust;
    private String paidByVendor;
    private String unpaidByCust;
    private String unpaidByVendor;
    private String overPaidByCust;
    private String overPaidByVendor;
    private Boolean isDebitorsAvailable;
    private Boolean isCreditorsAvailable;


    private ArrayList<String> topThreeCustomer;
    private ArrayList<String> topThreeVendor;


    public void setAllFields(String paidByCust, String paidByVendor, String unpaidByCust
            , String unpaidByVendor, String overPaidByCust, String overPaidByVendor) {
        this.paidByCust=paidByCust;
        this.paidByVendor=paidByVendor;
        this.unpaidByCust=unpaidByCust;
        this.unpaidByVendor=unpaidByVendor;
        this.overPaidByCust=overPaidByCust;
        this.overPaidByVendor=overPaidByVendor;
    }

    public Boolean getDebitorsAvailable() {
        return isDebitorsAvailable;
    }

    public void setDebitorsAvailable(Boolean debitorsAvailable) {
        isDebitorsAvailable = debitorsAvailable;
    }

    public Boolean getCreditorsAvailable() {
        return isCreditorsAvailable;
    }

    public void setCreditorsAvailable(Boolean creditorsAvailable) {
        isCreditorsAvailable = creditorsAvailable;
    }

    public String getPaidByCust() {
        return paidByCust;
    }

    public void setPaidByCust(String paidByCust) {
        this.paidByCust = paidByCust;
    }

    public String getPaidByVendor() {
        return paidByVendor;
    }

    public void setPaidByVendor(String paidByVendor) {
        this.paidByVendor = paidByVendor;
    }

    public String getUnpaidByCust() {
        return unpaidByCust;
    }

    public void setUnpaidByCust(String unpaidByCust) {
        this.unpaidByCust = unpaidByCust;
    }

    public String getUnpaidByVendor() {
        return unpaidByVendor;
    }

    public void setUnpaidByVendor(String unpaidByVendor) {
        this.unpaidByVendor = unpaidByVendor;
    }

    public String getOverPaidByCust() {
        return overPaidByCust;
    }

    public void setOverPaidByCust(String overPaidByCust) {
        this.overPaidByCust = overPaidByCust;
    }

    public String getOverPaidByVendor() {
        return overPaidByVendor;
    }

    public void setOverPaidByVendor(String overPaidByVendor) {
        this.overPaidByVendor = overPaidByVendor;
    }

    public ArrayList<String> getTopThreeCustomer() {
        return topThreeCustomer;
    }

    public void setTopThreeCustomer(ArrayList<String> topThreeCustomer) {
        this.topThreeCustomer = topThreeCustomer;
    }

    public ArrayList<String> getTopThreeVendor() {
        return topThreeVendor;
    }

    public void setTopThreeVendor(ArrayList<String> topThreeVendor) {
        this.topThreeVendor = topThreeVendor;
    }
}
