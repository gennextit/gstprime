package com.gennext.offlinegst.util;

import android.support.annotation.NonNull;

import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * Created by Abhijit on 13-Dec-16.
 */

public class RequestBuilder {
    @NonNull
    public static RequestBody Default(String userId) {
        return new FormBody.Builder()
                .addEncoded("userId", userId)
                .build();
    }

    @NonNull
    public static RequestBody Default(String userId, String profileId) {
        return new FormBody.Builder()
                .addEncoded("userId", userId)
                .addEncoded("setupId", profileId)
                .build();

    }

    @NonNull
    public static RequestBody DefaultType(String userId, String profileId, String type) {
        return new FormBody.Builder()
                .addEncoded("userId", userId)
                .addEncoded("setupId", profileId)
                .addEncoded("type", type)
                .build();

    }


    @NonNull
    public static RequestBody DefaultType(String userId, String profileId, String type, String startDate, String endDate) {
        return new FormBody.Builder()
                .addEncoded("userId", userId)
                .addEncoded("setupId", profileId)
                .addEncoded("type", type)
                .addEncoded("startDate", startDate)
                .addEncoded("endDate", endDate)
                .build();

    }

    @NonNull
    public static RequestBody Default(String userId, String profileId, String companyId) {
        return new FormBody.Builder()
                .addEncoded("userId", userId)
                .addEncoded("profileId", profileId)
                .addEncoded("companyId", companyId)
                .build();

    }

    @NonNull
    public static RequestBody LoginBody(String username, String password) {
        return new FormBody.Builder()
                .addEncoded("userId", username)
                .add("password", password)
                .build();

    }

    @NonNull
    public static RequestBody FeedbackDetail(String userId, String feedback) {
        return new FormBody.Builder()
                .addEncoded("userId", userId)
                .addEncoded("feedback", feedback)
                .build();

    }


    @NonNull
    public static RequestBody verifyOtpMobile(String mobile, String otp) {
        return new FormBody.Builder()
                .addEncoded("mobile", mobile)
                .addEncoded("otp", otp)
                .build();

    }

    @NonNull
    public static RequestBody verifyMobile(String mobile) {
        return new FormBody.Builder()
                .addEncoded("mobile", mobile)
                .build();

    }

    @NonNull
    public static RequestBody verifyEmail(String email) {
        return new FormBody.Builder()
                .addEncoded("email", email)
                .build();

    }

    @NonNull
    public static RequestBody verifyOtpEmail(String email, String otp) {
        return new FormBody.Builder()
                .addEncoded("email", email)
                .addEncoded("otp", otp)
                .build();

    }

    @NonNull
    public static RequestBody createPassThroughMobile(String password, String input) {
        return new FormBody.Builder()
                .addEncoded("mobile", input)
                .addEncoded("password", password)
                .build();

    }

    @NonNull
    public static RequestBody createPassThroughEmail(String password, String input) {
        return new FormBody.Builder()
                .addEncoded("email", input)
                .addEncoded("password", password)
                .build();

    }

    @NonNull
    public static RequestBody ErrorReport(String report) {
        return new FormBody.Builder()
                .addEncoded("reportLog", report)
                .build();

    }

    @NonNull
    public static RequestBody login(String email, String password) {
        return new FormBody.Builder()
                .addEncoded("email", email)
                .addEncoded("password", password)
                .build();

    }

    @NonNull
    public static RequestBody updateProfile(String userId, String profileId, String name, String email, String mobile, String pan) {
        return new FormBody.Builder()
                .addEncoded("userId", userId)
                .addEncoded("profileId", profileId)
                .addEncoded("name", name)
                .addEncoded("email", email)
                .addEncoded("phone", mobile)
                .addEncoded("pan", pan)
                .build();

    }

    @NonNull
    public static RequestBody addProduct(String userId, String productName, String codeHSN, String barcode, String costPrice, String salePrice, String taxRate, String fromDate
            , String toDate, String unitCode, String unitName, String cessRate) {
        return new FormBody.Builder()
                .addEncoded("userId", userId)
                .addEncoded("productName", productName)
                .addEncoded("codeHSN", codeHSN)
                .addEncoded("barcode", barcode)
                .addEncoded("costPrice", costPrice)
                .addEncoded("salePrice", salePrice)
                .addEncoded("taxRate", taxRate)
                .addEncoded("fromDate", fromDate)
                .addEncoded("toDate", toDate)
                .addEncoded("unitCode", unitCode)
                .addEncoded("unitName", unitName)
                .addEncoded("cessRate", cessRate)
                .build();

    }

    @NonNull
    public static RequestBody addService(String userId, String serviceName, String codeSAC, String salePrice, String taxRate, String fromDate
            , String toDate) {
        return new FormBody.Builder()
                .addEncoded("userId", userId)
                .addEncoded("serviceName", serviceName)
                .addEncoded("codeSAC", codeSAC)
                .addEncoded("salePrice", salePrice)
                .addEncoded("taxRate", taxRate)
                .addEncoded("fromDate", fromDate)
                .addEncoded("toDate", toDate)
                .build();

    }


    @NonNull
    public static RequestBody addVendor(String userId, String personName, String companyName, String contactEmail, String workPhone
            , String mobile, String contactDisplayName, String website, String gstin, String dlNumber
            , String state, String city, String country, String zip, String address, String fax
            , String currency, String paymentTerms, String remarks, String selectedStateCode, String selectedTermCode) {
        return new FormBody.Builder()
                .addEncoded("userId", userId)
                .addEncoded("personName", personName)
                .addEncoded("companyName", companyName)
                .addEncoded("contactEmail", contactEmail)
                .addEncoded("workPhone", workPhone)
                .addEncoded("mobile", mobile)
                .addEncoded("contactDisplayName", contactDisplayName)
                .addEncoded("website", website)
                .addEncoded("GSTIN", gstin)
                .addEncoded("DLNumber", dlNumber)
                .addEncoded("state", state)
                .addEncoded("city", city)
                .addEncoded("country", country)
                .addEncoded("zip", zip)
                .addEncoded("address", address)
                .addEncoded("fax", fax)
                .addEncoded("currency", currency)
                .addEncoded("paymentTerms", paymentTerms)
                .addEncoded("remarks", remarks)
                .addEncoded("selectedStateCode", selectedStateCode)
                .addEncoded("selectedTermCode", selectedTermCode)
                .build();

    }

    @NonNull
    public static RequestBody addPurchases(String invoiceGenerateType, String userId, String profileId, String reverceCharge, String invoiceNumber, String invoiceDate, String vendorId, String vendorName, String placeOfPurchase, String totalPrice, String selectedPlaceId, String receiveDate, String invoicePurchaseTypeId, String purchaseCategoryId, String mentionedGST, String freightAndProductTxableAmt, String totalIncAllTax, String discount, String freightMaxTaxRate, String freightAmount, String freightIncTaxValue, String freightCgstRate, String freightCgstAmount, String freightSgstRate, String freightSgstAmount, String freightIgstRate, String freightIgstAmount, String productJson) {
        return new FormBody.Builder()
                .addEncoded("invoiceCriteria", "new")
//                .addEncoded("type", invoiceGenerateType)
                .addEncoded("userId", userId)
                .addEncoded("set_up", profileId)
                .addEncoded("invoiceNo", invoiceNumber)
                .addEncoded("invoiceDate", invoiceDate)
                .addEncoded("vendorId", vendorId)
//                .addEncoded("customerName", customerName)
//                .addEncoded("termId", "") // not available
                .addEncoded("reverseCharge", reverceCharge)
                .addEncoded("receiveDate", receiveDate)
                .addEncoded("purchaseType", invoicePurchaseTypeId)
                .addEncoded("freightTax", freightMaxTaxRate)
                .addEncoded("freightCgstRate", freightCgstRate)
                .addEncoded("freightCgstAmount", freightCgstAmount)
                .addEncoded("freightSgstRate", freightSgstRate)
                .addEncoded("freightSgstAmount", freightSgstAmount)
                .addEncoded("freightIgstRate", freightIgstRate)
                .addEncoded("freightIgstAmount", freightIgstAmount)
                .addEncoded("taxableAmount", totalPrice)
                .addEncoded("invoiceTotal", totalIncAllTax)
                .addEncoded("freightAmount", freightAmount)
                .addEncoded("freight", freightIncTaxValue)
//                .addEncoded("transpoartMode", transpoartMode)
                .addEncoded("dateOfPurchase", invoiceDate) // not available
                .addEncoded("placeStateId", selectedPlaceId)
                .addEncoded("discount", discount)
                .addEncoded("purchaseCategory", purchaseCategoryId)
                .addEncoded("mentionedGST", mentionedGST)
                .addEncoded("productDetails", productJson)
                .build();

    }

    @NonNull
    public static RequestBody addPurchasesService(String invoiceGenerateType, String userId, String profileId, String reverceCharge, String invoiceNumber, String invoiceDate, String vendorId, String vendorName, String placeOfPurchase, String totalPrice, String selectedPlaceId, String receiveDate, String invoicePurchaseTypeId, String purchaseCategoryId, String mentionedGST, String freightAndProductTxableAmt, String totalIncAllTax, String discount, String freightMaxTaxRate, String freightAmount, String freightIncTaxValue, String freightCgstRate, String freightCgstAmount, String freightSgstRate, String freightSgstAmount, String freightIgstRate, String freightIgstAmount, String productJson) {
        return new FormBody.Builder()
//                .addEncoded("invoiceCriteria", "new")
//                .addEncoded("type", invoiceGenerateType)
                .addEncoded("userId", userId)
                .addEncoded("set_up", profileId)
                .addEncoded("invoiceNo", invoiceNumber)
                .addEncoded("invoiceDate", invoiceDate)
                .addEncoded("vendorId", vendorId)
//                .addEncoded("customerName", customerName)
//                .addEncoded("termId", "") // not available
                .addEncoded("reverseCharge", reverceCharge)
                .addEncoded("receiveDate", receiveDate)
                .addEncoded("purchaseType", invoicePurchaseTypeId)
//                .addEncoded("freightTax", freightMaxTaxRate)
//                .addEncoded("freightCgstRate", freightCgstRate)
//                .addEncoded("freightCgstAmount", freightCgstAmount)
//                .addEncoded("freightSgstRate", freightSgstRate)
//                .addEncoded("freightSgstAmount", freightSgstAmount)
//                .addEncoded("freightIgstRate", freightIgstRate)
//                .addEncoded("freightIgstAmount", freightIgstAmount)
                .addEncoded("taxableAmount", totalPrice)
                .addEncoded("invoiceTotal", totalIncAllTax)
//                .addEncoded("freightAmount", freightAmount)
//                .addEncoded("freight", freightIncTaxValue)
//                .addEncoded("transpoartMode", transpoartMode)
                .addEncoded("dateOfPurchase", invoiceDate) // not available
                .addEncoded("placeStateId", selectedPlaceId)
                .addEncoded("discount", discount)
                .addEncoded("purchaseCategory", purchaseCategoryId)
                .addEncoded("mentionedGST", mentionedGST)
                .addEncoded("serviceDetails", productJson)
                .build();

    }

    @NonNull
    public static RequestBody addCompany(String userId, String name, String email, String mobile, String pan) {
        return new FormBody.Builder()
                .addEncoded("userId", userId)
                .addEncoded("name", name)
                .addEncoded("email", email)
                .addEncoded("phone", mobile)
                .addEncoded("pan", pan)
                .build();

    }

    @NonNull
    public static RequestBody addCompanyDetail(String userId, String companyName, String telephone, String email
            , String gstin, String state, String stateCode, String address, String dlNumber, String tAndC, String accName, String ifscCode, String swiftCode, String accNumber
            , String branchAddress, String businessType, String tinNo, String bankName, String turnover, String invoicePrefix, String invoiceStart) {
        return new FormBody.Builder()
                .addEncoded("userId", userId)
                .addEncoded("companyName", companyName)
                .addEncoded("telephone", telephone)
                .addEncoded("email", email)
                .addEncoded("gstin", gstin)
                .addEncoded("state", stateCode)
                .addEncoded("companyAddress", address)
                .addEncoded("dlNumber", dlNumber)
                .addEncoded("termsAndCondition", tAndC)
                .addEncoded("accountName", accName)
                .addEncoded("ifsc", ifscCode)
                .addEncoded("accountNumber", accNumber)
                .addEncoded("branchAddress", branchAddress)
                .addEncoded("turnover", turnover)
                .addEncoded("businessType", businessType)
                .addEncoded("bankName", bankName)
                .addEncoded("invoicePrefix", invoicePrefix)
                .addEncoded("invoiceStart", invoiceStart)
                .addEncoded("swiftCode", swiftCode)
                .build();

    }

    @NonNull
    public static RequestBody updateCompanyDetail(String userId, String companyId, String companyName, String telephone, String email
            , String gstin, String state, String stateCode, String address, String dlNumber, String tAndC, String accName, String ifscCode, String swiftCode, String accNumber
            , String branchAddress, String businessType, String tinNo, String bankName, String turnover, String invoicePrefix, String invoiceStart, String compositionScheme) {
        return new FormBody.Builder()
                .addEncoded("id", companyId)
                .addEncoded("userId", userId)
                .addEncoded("companyName", companyName)
                .addEncoded("telephone", telephone)
                .addEncoded("email", email)
                .addEncoded("gstin", gstin)
                .addEncoded("state", stateCode)
                .addEncoded("companyAddress", address)
                .addEncoded("dlNumber", dlNumber)
                .addEncoded("termsAndCondition", tAndC)
                .addEncoded("accountName", accName)
                .addEncoded("ifsc", ifscCode)
                .addEncoded("accountNumber", accNumber)
                .addEncoded("branchAddress", branchAddress)
                .addEncoded("turnover", turnover)
                .addEncoded("businessType", businessType)
                .addEncoded("bankName", bankName)
                .addEncoded("invoicePrefix", invoicePrefix)
                .addEncoded("invoiceStart", invoiceStart)
                .addEncoded("swiftCode", swiftCode)
                .addEncoded("compositScheme", compositionScheme)
                .build();

    }

    @NonNull
    public static RequestBody addInvoice(String invoiceCriteria, String invoiceGenerateType, String userId, String profileId, String invoiceNumber, String invoiceDate, String customerId
            , String customerName, String freightAmount, String transpoartMode, String transpoartModeId, String vehicleNumber
            , String dateOfSupply, String placeofSupply, String selectedPlaceId, String shipName, String shipGSTIN, String shipAddress
            , String totalPrice, String invTypeId, String invTypeName, String portCode, String invTermId, String invTermName, String reverceCharge
            , String operatorGstin, String freightTax, String freightCgstRate, String freightCgstAmount, String freightSgstRate, String freightSgstAmount
            , String freightIgstRate, String freightIgstAmount, String freight, String productMinTaxRate, String discountTaxAmount, String discountTaxableAmount, String discount, String invoiceTotal, String productJson) {
        return new FormBody.Builder()
                .addEncoded("invoiceCriteria", invoiceCriteria)
//                .addEncoded("type", invoiceGenerateType)
                .addEncoded("userId", userId)
                .addEncoded("customerId", customerId)
                .addEncoded("invoiceNo", invoiceNumber)
                .addEncoded("invoiceType", invTypeName)
                .addEncoded("portCode", portCode)
                .addEncoded("invoiceDate", invoiceDate)
                .addEncoded("shipName", shipName)
                .addEncoded("termId", invTermId)
                .addEncoded("reverseCharge", reverceCharge)
                .addEncoded("shipGstin", shipGSTIN)
                .addEncoded("shipAddress", shipAddress)
                .addEncoded("freightTax", freightTax)
//                .addEncoded("customerName", customerName)
                .addEncoded("freightCgstRate", freightCgstRate)
                .addEncoded("freightCgstAmount", freightCgstAmount)
                .addEncoded("freightSgstRate", freightSgstRate)
                .addEncoded("freightSgstAmount", freightSgstAmount)
                .addEncoded("freightIgstRate", freightIgstRate)
                .addEncoded("freightIgstAmount", freightIgstAmount)
                .addEncoded("invoiceTotal", invoiceTotal)
                .addEncoded("freightAmount", freightAmount)
                .addEncoded("freight", freight)
                .addEncoded("transportMode", transpoartModeId)
                .addEncoded("vehicleNo", vehicleNumber)
                .addEncoded("discount", discount)
                .addEncoded("dateOfSupply", dateOfSupply)
                .addEncoded("shipStateId", selectedPlaceId)
                .addEncoded("set_up", profileId)
                .addEncoded("operatorGSTIN", operatorGstin)
                .addEncoded("taxableAmount", totalPrice)
//                .addEncoded("transpoartMode", transpoartMode)
                .addEncoded("productDetails", productJson)

                .addEncoded("discountTaxRate", productMinTaxRate)
                .addEncoded("discountTaxAmount", discountTaxAmount)
                .addEncoded("discountTaxableAmount", discountTaxableAmount)
                .build();


    }

    @NonNull
    public static RequestBody addService(String invoiceCriteria, String invoiceGenerateType, String userId, String profileId, String invoiceNumber, String invoiceDate, String customerId
            , String customerName, String freightAmount, String transpoartMode, String transpoartModeId, String vehicleNumber
            , String dateOfSupply, String placeofSupply, String selectedPlaceId, String shipName, String shipGSTIN, String shipAddress
            , String totalPrice, String invTypeId, String invTypeName, String portCode, String invTermId, String invTermName, String reverceCharge
            , String operatorGstin, String freightTax, String freightCgstRate, String freightCgstAmount, String freightSgstRate, String freightSgstAmount
            , String freightIgstRate, String freightIgstAmount, String freight, String productMinTaxRate, String discountTaxAmount, String discountTaxableAmount, String discount, String invoiceTotal, String productJson) {
        return new FormBody.Builder()
                .addEncoded("invoiceCriteria", invoiceCriteria)
//                .addEncoded("type", invoiceGenerateType)
                .addEncoded("userId", userId)
                .addEncoded("customerId", customerId)
                .addEncoded("invoiceNo", invoiceNumber)
                .addEncoded("invoiceType", invTypeName)
                .addEncoded("portCode", portCode)
                .addEncoded("invoiceDate", invoiceDate)
//                .addEncoded("shipName", shipName)
                .addEncoded("termId", invTermId)
                .addEncoded("reverseCharge", reverceCharge)
//                .addEncoded("shipGstin", shipGSTIN)
//                .addEncoded("shipAddress", shipAddress)
                .addEncoded("freightTax", freightTax)
//                .addEncoded("customerName", customerName)
                .addEncoded("freightCgstRate", freightCgstRate)
                .addEncoded("freightCgstAmount", freightCgstAmount)
                .addEncoded("freightSgstRate", freightSgstRate)
                .addEncoded("freightSgstAmount", freightSgstAmount)
                .addEncoded("freightIgstRate", freightIgstRate)
                .addEncoded("freightIgstAmount", freightIgstAmount)
                .addEncoded("invoiceTotal", invoiceTotal)
                .addEncoded("freightAmount", freightAmount)
                .addEncoded("freight", freight)
//                .addEncoded("transportMode", transpoartModeId)
//                .addEncoded("vehicleNo", vehicleNumber)
                .addEncoded("discount", discount)
//                .addEncoded("dateOfSupply", dateOfSupply)
//                .addEncoded("shipStateId", selectedPlaceId)
                .addEncoded("set_up", profileId)
                .addEncoded("operatorGSTIN", operatorGstin)
                .addEncoded("taxableAmount", totalPrice)
//                .addEncoded("transpoartMode", transpoartMode)
                .addEncoded("serviceDetails", productJson)

                .addEncoded("discountTaxRate", productMinTaxRate)
                .addEncoded("discountTaxAmount", discountTaxAmount)
                .addEncoded("discountTaxableAmount", discountTaxableAmount)
                .build();


    }

    @NonNull
    public static RequestBody addVoucher(String userId, String profileId, String companyId, String voucherNo, String voucherDate, String noteType
            , String noteTypeId, String noteReason, String noteReasonId, String customerId, String customerName, String invoiceNo
            , String freightAmt, String invoiceDate, String preGST, String placeOfSupply, String placeofSupplyId, String noteValue
            , String taxRate, String taxableValue, String cessAmt, String productJson) {
        return new FormBody.Builder()
                .addEncoded("userId", userId)
                .addEncoded("profileId", profileId)
                .addEncoded("companyId", companyId)
                .addEncoded("voucherNo", voucherNo)
                .addEncoded("voucherDate", voucherDate)
                .addEncoded("noteType", noteType)
                .addEncoded("noteTypeId", noteTypeId)
                .addEncoded("noteReason", noteReason)
                .addEncoded("noteReasonId", noteReasonId)
                .addEncoded("customerId", customerId)
                .addEncoded("customerName", customerName)
                .addEncoded("invoiceNo", invoiceNo)
                .addEncoded("freightAmt", freightAmt)
                .addEncoded("invoiceDate", invoiceDate)
                .addEncoded("preGST", preGST)
                .addEncoded("placeOfSupply", placeOfSupply)
                .addEncoded("placeofSupplyId", placeofSupplyId)
                .addEncoded("noteValue", noteValue)
                .addEncoded("taxRate", taxRate)
                .addEncoded("taxableValue", taxableValue)
                .addEncoded("cessAmt", cessAmt)
                .addEncoded("productJson", productJson)
                .build();

    }

    @NonNull
    public static RequestBody insertProduct(String userId, String productDescription, String codeHSN, String openingStock, String barcode
            , String costPrice, String salePrice, String taxRate, String batch
            , String expiry, String unitCode, String unitName, String cessRate, String mrp, String taxCategory) {
        return new FormBody.Builder()
                .addEncoded("userId", userId)
                .addEncoded("barcode", barcode)
                .addEncoded("productDescription", productDescription)
                .addEncoded("HSNCode", codeHSN)
                .addEncoded("openingStock", openingStock)
//                .addEncoded("costPrice", costPrice)
                .addEncoded("salePrice", salePrice)
                .addEncoded("taxRate", taxRate)
                .addEncoded("unitCode", unitCode)
                .addEncoded("mrp", mrp)
                .addEncoded("batch", batch)
                .addEncoded("expiry", expiry)
                .addEncoded("taxCategory", taxCategory)
                .addEncoded("cessRate", cessRate)
                .build();
    }

    @NonNull
    public static RequestBody editProduct(String productId, String userId, String productDescription, String codeHSN, String openingStock, String barcode
            , String costPrice, String salePrice, String taxRate, String batch
            , String expiry, String unitCode, String unitName, String cessRate, String mrp, String taxCategory) {
        return new FormBody.Builder()
                .addEncoded("id", productId)
                .addEncoded("userId", userId)
                .addEncoded("barcode", barcode)
                .addEncoded("productDescription", productDescription)
                .addEncoded("HSNCode", codeHSN)
                .addEncoded("openingStock", openingStock)
//                .addEncoded("costPrice", costPrice)
                .addEncoded("salePrice", salePrice)
                .addEncoded("taxRate", taxRate)
                .addEncoded("unitCode", unitCode)
//                .addEncoded("unitName", unitName)
                .addEncoded("mrp", mrp)
                .addEncoded("batch", batch)
                .addEncoded("expiry", expiry)
                .addEncoded("taxCategory", taxCategory)
                .addEncoded("cessRate", cessRate)
                .build();
    }

    @NonNull
    public static RequestBody insertCustomer(String userId, String setupId, String personName, String companyName, String contactEmail, String workPhone, String mobile, String contactDisplayName, String website, String gstin, String dlNumber, String state, String city, String country, String zip, String address, String fax, String currency, String paymentTerms, String remarks, String selectedStateCode, String selectedTermCode, String openingBalance) {
        return new FormBody.Builder()
                .addEncoded("userId", userId)
                .addEncoded("set_up", setupId)
                .addEncoded("primaryName", personName)
                .addEncoded("companyName", companyName)
                .addEncoded("email", contactEmail)
                .addEncoded("workPhone", workPhone)
                .addEncoded("mobile", mobile)
                .addEncoded("contactDisplayName", contactDisplayName)
                .addEncoded("website", website)
                .addEncoded("gstin", gstin)
                .addEncoded("dlNumber", dlNumber)
                .addEncoded("customerState", selectedStateCode)
                .addEncoded("customerCity", city)
                .addEncoded("country", country)
                .addEncoded("zip", zip)
                .addEncoded("address", address)
                .addEncoded("fax", fax)
                .addEncoded("currency", currency)
                .addEncoded("customerTerms", selectedTermCode != null ? selectedTermCode : "")
                .addEncoded("remarks", remarks)
                .addEncoded("openingBalance", openingBalance)
//                .addEncoded("selectedStateCode", selectedStateCode)
//                .addEncoded("selectedTermCode", selectedTermCode)
                .build();
    }

    @NonNull
    public static RequestBody editCustomer(String custometId, String userId, String setupId, String personName, String companyName, String contactEmail, String workPhone, String mobile, String contactDisplayName, String website, String gstin, String dlNumber, String state, String city, String country, String zip, String address, String fax, String currency, String paymentTerms, String remarks, String selectedStateCode, String selectedTermCode, String openingBalance) {
        return new FormBody.Builder()
                .addEncoded("id", custometId)
                .addEncoded("userId", userId)
                .addEncoded("set_up", setupId)
                .addEncoded("primaryName", personName)
                .addEncoded("companyName", companyName)
                .addEncoded("email", contactEmail)
                .addEncoded("workPhone", workPhone)
                .addEncoded("mobile", mobile)
//                .addEncoded("contactDisplayName", contactDisplayName)
                .addEncoded("website", website)
                .addEncoded("gstin", gstin)
                .addEncoded("dlNumber", dlNumber)
                .addEncoded("customerState", selectedStateCode)
                .addEncoded("customerCity", city)
                .addEncoded("country", country)
                .addEncoded("zip", zip)
                .addEncoded("address", address)
                .addEncoded("fax", fax)
                .addEncoded("currency", currency)
                .addEncoded("customerTerms", selectedTermCode != null ? selectedTermCode : "")
                .addEncoded("remarks", remarks)
                .addEncoded("openingBalance", openingBalance)
//                .addEncoded("selectedStateCode", selectedStateCode)
//                .addEncoded("selectedTermCode", selectedTermCode)
                .build();
    }

    @NonNull
    public static RequestBody insertVendor(String userId, String setupId, String personName, String companyName, String contactEmail, String workPhone, String mobile, String contactDisplayName, String website, String gstin, String dlNumber, String state, String city, String country, String zip, String address, String fax, String currency, String paymentTerms, String remarks, String selectedStateCode, String selectedTermCode, String openingBalance) {
        return new FormBody.Builder()
                .addEncoded("userId", userId)
                .addEncoded("set_up", setupId)
                .addEncoded("primaryName", personName)
                .addEncoded("companyName", companyName)
                .addEncoded("email", contactEmail)
                .addEncoded("workPhone", workPhone)
                .addEncoded("mobile", mobile)
//                .addEncoded("contactDisplayName", contactDisplayName)
                .addEncoded("website", website)
                .addEncoded("gstin", gstin)
                .addEncoded("dlNumber", dlNumber)
                .addEncoded("vendorState", selectedStateCode)
                .addEncoded("vendorCity", city)
                .addEncoded("country", country)
                .addEncoded("zip", zip)
                .addEncoded("address", address)
                .addEncoded("fax", fax)
                .addEncoded("currency", currency)
                .addEncoded("vendorTerms", selectedTermCode != null ? selectedTermCode : "")
                .addEncoded("remarks", remarks)
                .addEncoded("openingBalance", openingBalance)
//                .addEncoded("vendorStateCode", selectedStateCode)
//                .addEncoded("vendorTermsCode", selectedTermCode)
                .build();
    }

    @NonNull
    public static RequestBody editVendor(String vendorId, String userId, String setupId, String personName, String companyName, String contactEmail, String workPhone, String mobile, String contactDisplayName, String website, String gstin, String dlNumber, String state, String city, String country, String zip, String address, String fax, String currency, String paymentTerms, String remarks, String selectedStateCode, String selectedTermCode, String openingBalance) {
        return new FormBody.Builder()
                .addEncoded("id", vendorId)
                .addEncoded("userId", userId)
                .addEncoded("set_up", setupId)
                .addEncoded("primaryName", personName)
                .addEncoded("companyName", companyName)
                .addEncoded("email", contactEmail)
                .addEncoded("workPhone", workPhone)
                .addEncoded("mobile", mobile)
//                .addEncoded("contactDisplayName", contactDisplayName)
                .addEncoded("website", website)
                .addEncoded("gstin", gstin)
                .addEncoded("dlNumber", dlNumber)
                .addEncoded("vendorState", selectedStateCode)
                .addEncoded("vendorCity", city)
                .addEncoded("country", country)
                .addEncoded("zip", zip)
                .addEncoded("address", address)
                .addEncoded("fax", fax)
                .addEncoded("currency", currency)
                .addEncoded("vendorTerms", selectedTermCode != null ? selectedTermCode : "")
                .addEncoded("remarks", remarks)
                .addEncoded("openingBalance", openingBalance)
//                .addEncoded("vendorStateCode", selectedStateCode)
//                .addEncoded("vendorTermsCode", selectedTermCode)
                .build();
    }

    @NonNull
    public static RequestBody cancelInvoice(String type, String userId, String profileId, String invoiceNumber) {
        return new FormBody.Builder()
                .addEncoded("type", type)
                .addEncoded("userId", userId)
                .addEncoded("setupId", profileId)
                .addEncoded("invoiceNumber", invoiceNumber)
                .build();
    }

    @NonNull
    public static RequestBody deletePurchase(String type, String userId, String profileId, String invoiceNumber, String vendorId) {
        return new FormBody.Builder()
                .addEncoded("type", type)
                .addEncoded("userId", userId)
                .addEncoded("setupId", profileId)
                .addEncoded("invoiceNo", invoiceNumber)
                .addEncoded("vendorId", vendorId)
                .build();
    }

    @NonNull
    public static RequestBody deleteCustomer(String userId, String setupId, String customerId) {
        return new FormBody.Builder()
                .addEncoded("userId", userId)
                .addEncoded("setupId", setupId)
                .addEncoded("customerId", customerId)
                .build();
    }

    @NonNull
    public static RequestBody deleteItem(String userId, String setupId, String productId) {
        return new FormBody.Builder()
                .addEncoded("userId", userId)
                .addEncoded("setupId", setupId)
                .addEncoded("productId", productId)
                .build();
    }

    @NonNull
    public static RequestBody deleteVendor(String userId, String setupId, String vendorId) {
        return new FormBody.Builder()
                .addEncoded("userId", userId)
                .addEncoded("setupId", setupId)
                .addEncoded("vendorId", vendorId)
                .build();
    }
}
