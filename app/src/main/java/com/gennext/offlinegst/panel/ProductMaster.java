package com.gennext.offlinegst.panel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.model.user.ProductModel;


public class ProductMaster extends BottomSheetDialogFragment implements View.OnClickListener {
    private MenuListener mListener;
    private int position;
    private ProductModel itemProduct;


    public interface MenuListener {
        void onEditClick(DialogFragment dialog, ProductModel itemProduct, int position);
        void onDeleteClick(DialogFragment dialog, ProductModel itemProduct, int position);
        void onViewClick(DialogFragment dialog, ProductModel itemProduct, int position);
        void onQuantityClick(DialogFragment dialog, ProductModel itemProduct, int position);
    }

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }


    public static ProductMaster newInstance(MenuListener listener, ProductModel itemProduct, int position) {
        ProductMaster fragment = new ProductMaster();
        fragment.mListener = listener;
        fragment.itemProduct=itemProduct;
        fragment.position=position;
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.menu_product, container, false);

        TextView tvTitle = (TextView) v.findViewById(R.id.action_menu_title);
        ImageView ivClose = (ImageView) v.findViewById(R.id.action_menu_close);
        LinearLayout edit = (LinearLayout) v.findViewById(R.id.action_menu_edit);
        LinearLayout delete = (LinearLayout) v.findViewById(R.id.action_menu_delete);
        LinearLayout copy = (LinearLayout) v.findViewById(R.id.action_menu_view);
        LinearLayout quantity = (LinearLayout) v.findViewById(R.id.action_menu_quantity);

        ivClose.setOnClickListener(this);
        edit.setOnClickListener(this);
        delete.setOnClickListener(this);
        copy.setOnClickListener(this);
        quantity.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.action_menu_close:
                dismiss();
                break;
            case R.id.action_menu_edit:
                dismiss();
                if (mListener != null) {
                    mListener.onEditClick(ProductMaster.this,itemProduct,position);
                }
                break;
            case R.id.action_menu_delete:
                dismiss();
                if (mListener != null) {
                    mListener.onDeleteClick(ProductMaster.this,itemProduct,position);
                }
                break;
            case R.id.action_menu_view:
                dismiss();
                if (mListener != null) {
                    mListener.onViewClick(ProductMaster.this,itemProduct,position);
                }
                break;
            case R.id.action_menu_quantity:
                dismiss();
                if (mListener != null) {
                    mListener.onQuantityClick(ProductMaster.this,itemProduct,position);
                }
                break;

        }


    }


}
