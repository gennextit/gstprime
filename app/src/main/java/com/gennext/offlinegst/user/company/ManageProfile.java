package com.gennext.offlinegst.user.company;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gennext.offlinegst.MainActivity;
import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.PopupAlert;
import com.gennext.offlinegst.model.user.CompanyModel;
import com.gennext.offlinegst.model.user.ManageCompanyAdapter;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.user.Dashboard;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.ApiCallError;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.RequestBuilder;

import java.util.ArrayList;

/**
 * Created by Admin on 7/4/2017.
 */

public class ManageProfile extends CompactFragment implements ApiCallError.ErrorListener{
    private RecyclerView lvMain;
    private ArrayList<CompanyModel> cList;
    private ManageCompanyAdapter adapter;
    private DBManager dbManager;

    private AssignTask assignTask;
    private FloatingActionButton btnAdd;
    private CoordinatorLayout coordinatorLayout;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static Fragment newInstance() {
        ManageProfile fragment=new ManageProfile();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.user_manage_company,container,false);
        ((MainActivity)getActivity()).updateTitle(getString(R.string.manage_profile));
        screenAnalytics(getContext(),AppUser.getUserId(getContext()),"Manage Profile");
        dbManager= DBManager.newIsntance(getActivity());
        initUi(v);
        return v;
    }

    private void initUi(View v) {
        coordinatorLayout = (CoordinatorLayout)v.findViewById(R.id.coordinatorLayout);
        lvMain = (RecyclerView)v.findViewById(R.id.lv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());
        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                executeTask();
            }
        });

        btnAdd = (FloatingActionButton) v.findViewById(R.id.fab);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFragment(AddCompany.newInstance(ManageProfile.this),"addCompany");
            }
        });
        lvMain.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && btnAdd.getVisibility() == View.VISIBLE) {
                    btnAdd.hide();
                } else if (dy < 0 && btnAdd.getVisibility() != View.VISIBLE) {
                    btnAdd.show();
                }
            }
        });
        executeTask();
    }



    private void executeTask() {
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.GET_COMPANY_DETAILS);
    }

    private void hideProgressBar() {
        if(mSwipeRefreshLayout!=null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        executeTask();
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }

    public void refreshList() {
        String name = AppUser.getName(getContext());
        String image = AppUser.getImage(getContext());
        Dashboard dashboard= (Dashboard) getFragmentManager().findFragmentByTag("dashboard");
        if(dashboard!=null){
            dashboard.setProfileNameImage(name,image);
        }
        ((MainActivity)getActivity()).updateProfile(getContext(),name,image);
        executeTask();
    }


    public void editProfile(CompanyModel profile) {
        addFragment(CompanyDetail.newInstance(ManageProfile.this,profile),"updateProfile");
    }

    public void deleteProfile(CompanyModel profile) {
        showSnakBar(coordinatorLayout,getString(R.string.update_later));
    }

    public void onSelectedProfile(CompanyModel item) {
        Context context = getContext();
        AppUser.setName(context,item.getCompanyName());
        AppUser.setImage(context,item.getLogo());
        AppUser.setProfileId(context,item.getCompanyId());
        AppUser.setCompanyId(context,item.getCompanyId());

        Intent intent = new Intent(getActivity(), MainActivity.class);
        startActivity(intent);
        getActivity().finish();
    }


    private class AssignTask extends AsyncTask<String, Void, CompanyModel> {
        private Context context;

        private void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        private void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        private AssignTask(Context context ) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected CompanyModel doInBackground(String... urls) {
            String userId = AppUser.getUserId(context);
            String companyId = AppUser.getCompanyId(context);
            CompanyModel result = new CompanyModel();
            if (AppConfig.isUpdateToServer(context)) {
                String response = ApiCall.POST(urls[0], RequestBuilder.Default(userId));
                return JsonParser.getAllCompanyDetail(context,response,companyId);
            }
            return new CompanyModel();
//            return dbManager.getAllProfileList(result,userId,companyId);
        }


        @Override
        protected void onPostExecute(CompanyModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressBar();
                if (!AppConfig.isUpdateToServer(context)) {
                    if (result.getOutputDB().equals(Const.SUCCESS)) {
                        cList=result.getList();
                        adapter = new ManageCompanyAdapter(getActivity(), cList,ManageProfile.this);
                        lvMain.setAdapter(adapter);
                    } else if (result.getOutputDB().equals(Const.FAILURE)) {
                        showSnakBar(coordinatorLayout,result.getOutputDBMsg());
                        addFragment(AddProfile.newInstance(ManageProfile.this),"addProfile");
                    }
                } else {
                    if (result.getOutput().equals(Const.SUCCESS)) {
                        cList=result.getList();
                        adapter = new ManageCompanyAdapter(getActivity(), cList,ManageProfile.this);
                        lvMain.setAdapter(adapter);
                        ((MainActivity)getActivity()).updateNavCompanyList();
                    } else if (result.getOutput().equals(Const.FAILURE)) {
                        PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(), "popupAlert");
                    } else {
                        ApiCallError.newInstance(result.getOutput(),result.getOutputMsg(), ManageProfile.this)
                                .show(getFragmentManager(), "apiCallError");
                    }
                }
            }
        }
    }

}
