package com.gennext.offlinegst.global;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gennext.offlinegst.MainActivity;
import com.gennext.offlinegst.R;
import com.gennext.offlinegst.util.AppAnimation;


/**
 * Created by Abhijit on 05-Aug-16.
 */
public class AboutUs extends CompactFragment implements View.OnClickListener {
    ImageView ivProfile;
    TextView tvAbout;
    Activity activity;
    ImageView ivFacebook, ivGooglePlus, ivWebsite;
    TextView tvEmail;
    private static final int FACEBOOK = 1, GOOGLE = 2, WEBSITE = 3;
    private TextView tvContactNo;
    private Dialog dialog;
    private RelativeLayout rlShare;


    public static Fragment newInstance() {
        AboutUs fragment=new AboutUs();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_aboutus, container, false);
        ((MainActivity) getActivity()).updateTitle(getString(R.string.about_us_tag));
        activity = getActivity();
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        rlShare = (RelativeLayout) v.findViewById(R.id.rl_about_us_share);
        ivProfile = (ImageView) v.findViewById(R.id.iv_about_us_profile);
        tvAbout = (TextView) v.findViewById(R.id.tv_about_us_detail);

        ivFacebook = (ImageView) v.findViewById(R.id.iv_about_facebook);
        ivGooglePlus = (ImageView) v.findViewById(R.id.iv_about_google_plus);
        ivWebsite = (ImageView) v.findViewById(R.id.iv_about_website);
        rlShare.setOnClickListener(this);
        ivFacebook.setOnClickListener(this);
        ivGooglePlus.setOnClickListener(this);
        ivWebsite.setOnClickListener(this);


        tvEmail = (TextView) v.findViewById(R.id.tv_about_email);
        tvContactNo = (TextView) v.findViewById(R.id.tv_about_contact);

        tvAbout.setText(getString(R.string.about_us));
        tvEmail.setText(" info@gennextit.com");
        tvContactNo.setText("  +91-7840079095");

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_about_facebook:
//                showDefaultAlertBox("facebook", "http://www.facebook.com/appsfeature", FACEBOOK);
                break;
            case R.id.iv_about_google_plus:
//                showDefaultAlertBox("Google Plus", "http://plus.google.com/u/0/101394178516199421328", GOOGLE);
                break;
            case R.id.iv_about_website:
                showDefaultAlertBox("Website", "http://gennextIt.com", WEBSITE);
                break;
            case R.id.rl_about_us_share:
                Intent in = new Intent("android.intent.action.SEND");
                in.setType("text/plain");
                in.putExtra("android.intent.extra.TEXT", getString(R.string.app_name)+" "+getString(R.string.share_message)+" \n+91-78400 79095\n\nPlayStore Link:https://goo.gl/piKD2H\n\nOr visit: http://gennextIt.com");
                startActivity(Intent.createChooser(in, getString(R.string.share_with_fiends)));
                break;

        }
    }

    public void showDefaultAlertBox(String title, final String url, final int switchStatus) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.alert_dialog, null);
        dialogBuilder.setView(v);
        ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);

        tvTitle.setText(R.string.open_as);
        tvDescription.setText(getString(R.string.drawer_open)+" " + title + " "+getString(R.string.page_in_internal_or_external_app));

        button1.setText(R.string.internal);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                openInInternal(switchStatus, url);
            }
        });
        button2.setText(R.string.external);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                openInExternal(getActivity(), switchStatus, url);
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();
    }


    public void openInExternal(Context context, int switchStatus, String url) {
        switch (switchStatus) {
            case FACEBOOK:
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                break;
            case GOOGLE:
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                break;
            case WEBSITE:
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                break;

        }
    }

    public void openInInternal(int switchStatus, String url) {
        switch (switchStatus) {
            case FACEBOOK:
                addFragment(AppWebView.newInstance(getString(R.string.facebook), url, AppWebView.URL), "appWebView");
                break;
            case GOOGLE:
                addFragment(AppWebView.newInstance(getString(R.string.google), url, AppWebView.URL), "appWebView");
                break;
            case WEBSITE:
                addFragment(AppWebView.newInstance(getString(R.string.website), url, AppWebView.URL), "appWebView");
                break;
        }
    }

}

