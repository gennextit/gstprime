package com.gennext.offlinegst;

/**
 * Created by Abhijit on 14-Jul-16.
 */


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.StateListDrawable;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gennext.offlinegst.global.AboutUs;
import com.gennext.offlinegst.global.AppFeedback;
import com.gennext.offlinegst.global.PopupAlert;
import com.gennext.offlinegst.model.app.SideMenu;
import com.gennext.offlinegst.model.app.SideMenuAdapter;
import com.gennext.offlinegst.model.user.CompanyModel;
import com.gennext.offlinegst.search.SortBarVIew;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.user.company.AddCompany;
import com.gennext.offlinegst.user.company.AddProfile;
import com.gennext.offlinegst.user.company.CompanyDetail;
import com.gennext.offlinegst.user.company.ManageProfile;
import com.gennext.offlinegst.user.customer.Customer;
import com.gennext.offlinegst.user.invoice.Invoices;
import com.gennext.offlinegst.user.product.Product;
import com.gennext.offlinegst.user.purchas.Purchases;
import com.gennext.offlinegst.user.services.purchase.ServicesPurchase;
import com.gennext.offlinegst.user.services.sale.ServicesSale;
import com.gennext.offlinegst.user.vendor.Vendor;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.JsonParser;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;


public abstract class NavDrawer extends AppCompatActivity {

    protected static final String PLACE_ID = "placeId", PLACE_NAME = "placeName", PLACE_ADDRESS = "placeAddress",
            PLACE_LATITUDE = "placeLatitude", PLACE_LONGITUDE = "placeLongitude";
    public static final int REQUEST_ADD_PRODUCT = 101;
    public static final int DRAWER_DEFAULT = 1, DRAWER_PROFILE = 2;

    protected Location mCurrentLocation;

    DrawerLayout dLayout;
    ListView dList;
    List<SideMenu> sideMenuList;
    private List<SideMenu> profileList;
    SideMenuAdapter slideMenuAdapter;


    private ProgressDialog navPDialog;
    private AlertDialog aDialog;
    private LinearLayout dMenuLayout;
    private int viewType;
    public static final int MAP_VIEW = 1, LIST_VIEW = 2;
    private ImageView ivProfileBackground;
    private TextView tvProfileName;
    private ImageView ivProfile;
    private DBManager dbManager;
    private List<SideMenu> mSideMenuDrawerList;
    private List<SideMenu> mSideMenuProfileList;
    private int drawerSelection;
    private ImageView ivExpendMore;
    private int rotationAngle = 0;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
//        primaryActivity=base;//SAVE ORIGINAL INSTANCE
        MultiDex.install(this);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbManager = DBManager.newIsntance(this);
    }


    public Toolbar setToolBar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_menu);
        return toolbar;
    }


    public Toolbar setNavBar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        return toolbar;
    }


    public boolean APICheck() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return true;
        } else {
            return false;
        }
    }


    public void closeFragmentDialog(String dialogName) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        DialogFragment fragment = (DialogFragment) fragmentManager.findFragmentByTag(dialogName);
        if (fragment != null) {
            fragment.dismiss();
        }
    }


    public void hideKeybord(Activity act) {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(act.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }


    public void showPDialog(String msg) {
        navPDialog = new ProgressDialog(NavDrawer.this);
        navPDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // navPDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.dialog_animation));
        navPDialog.setMessage(msg);
        navPDialog.setIndeterminate(false);
        navPDialog.setCancelable(false);
        navPDialog.show();
    }

    public void dismissPDialog() {
        if (navPDialog != null)
            navPDialog.dismiss();
    }


    public int getCol(Context context, int id) {
        return ContextCompat.getColor(context, id);
    }

    public String LoadPref(String key) {
        return LoadPref(NavDrawer.this, key);
    }

    public String LoadPref(Context context, String key) {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            return sharedPreferences.getString(key, "");
        } else {
            return "";
        }
    }

    public void SavePref(String key, String value) {
        SavePref(NavDrawer.this, key, value);
    }

    public void SavePref(Context context, String key, String value) {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(key, value);
            editor.apply();
        }
    }

    public String encodeUrl(String res) {
        String UrlEncoding = null;
        try {
            UrlEncoding = URLEncoder.encode(res, "UTF-8");

        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        return UrlEncoding;
    }

    public boolean isOnline() {
        boolean conn = false;
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(NavDrawer.this.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        if (haveConnectedWifi == true || haveConnectedMobile == true) {
            conn = true;
        } else {
            conn = false;
        }

        return conn;
    }


    public String getSt(int id) {

        return getResources().getString(id);
    }


    public void showToast(String txt) {
        Toast.makeText(NavDrawer.this, txt, Toast.LENGTH_LONG).show();
    }

    public void showToast(String txt, int gravity) {
        // Inflate the Layout
        Toast toast = Toast.makeText(NavDrawer.this, txt, Toast.LENGTH_SHORT);
        toast.setGravity(gravity | gravity, 0, 20);
        toast.show();
    }

    public void disableDrawerSelector() {
        if (dList != null)
            dList.setSelector(new StateListDrawable());
    }

    protected void SetDrawer(final Activity act, Toolbar toolbar) {
        // TODO Auto-generated method stub

        toolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean drawerOpen = dLayout.isDrawerOpen(dList);
                        if (!drawerOpen) {
                            dLayout.openDrawer(dList);
                        } else {
                            if (drawerSelection == DRAWER_DEFAULT) {
                                dLayout.closeDrawer(dList);
                            }
                        }
                    }
                }

        );

        dLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        dList = (ListView) findViewById(R.id.left_drawer);
        LayoutInflater inflater = getLayoutInflater();
        View listHeaderView = inflater.inflate(R.layout.side_menu_header, null, false);
        tvProfileName = (TextView) listHeaderView.findViewById(R.id.tv_slide_menu_header_name);
        ivProfile = (ImageView) listHeaderView.findViewById(R.id.iv_slide_menu_header_profile);
        ivExpendMore = (ImageView) listHeaderView.findViewById(R.id.iv_expend_more);

        String name = AppUser.getName(act);
        String image = AppUser.getImage(act);
        updateProfile(act, name, image);

//        listHeaderView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dList.setSelector(new StateListDrawable());
//                String profileId = AppUser.getProfileId(act);
//                rotationAngle = rotationAngle == 0 ? 180 : 0;  //toggle
//                ivExpendMore.animate().rotation(rotationAngle).setDuration(300).start();
//                if (profileId.equals("")) {
//                    showToast(getString(R.string.app_needs_to_setup_company_detail));
//                    addFragment(AddProfile.newInstance(null), "addProfile");
//                } else {
//                    if (drawerSelection == DRAWER_DEFAULT) {
//                        setCompanyProfileInDrawer(act);
//                    } else {
//                        setNavagationDrawer();
//                    }
//                }
//            }
//        });
        mSideMenuDrawerList = getNavagationList(act);
        mSideMenuProfileList = getProfileList(act);

        dList.addHeaderView(listHeaderView);

        sideMenuList = new ArrayList<>();
        slideMenuAdapter = new SideMenuAdapter(act, R.layout.side_menu_list_slot, sideMenuList, DRAWER_DEFAULT);
        dList.setAdapter(slideMenuAdapter);
        dList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position, long id) {
                dList.setSelector(R.drawable.bg_nav_selector);
                dLayout.closeDrawers();
                closePrevoiusScreen();
                if (drawerSelection == DRAWER_PROFILE) {
                    if (sideMenuList != null) {
                        int tempPos = position - 1;
                        String slotName = sideMenuList.get(tempPos).getName();
                        if (!slotName.equals(getString(R.string.manage_company))) {
                            onSelectedProfile(sideMenuList.get(tempPos).getProfile(), act);
                        } else {
                            setNavagationDrawer(act);
                            drawerSelection = DRAWER_DEFAULT;
                            dList.setSelector(new StateListDrawable());
                            addFragment(ManageProfile.newInstance(), R.id.container, "manageProfile");
//                            addFragment(AddProfile.newInstance(ManageProfile.this),"addProfile");
                        }
                    }
                } else {
                    if (position != 11 && position != 12 && AppUser.getPackageStatus(act).toLowerCase().equals(Const.EXPIRED)) {
                        PopupAlert.newInstance("Alert", AppUser.getPackageMessage(act), PopupAlert.POPUP_DIALOG)
                                .show(getSupportFragmentManager(), "popupAlert");
                        return;
                    }
                    switch (position) {
                        case 1:
                            if (AppUser.getCompanyId(act).equals("")) {
                                showToast(getString(R.string.app_needs_to_setup_company_detail));
                                addFragment(AddCompany.newInstance(), "addCompany");
                                dList.setSelector(new StateListDrawable());
                            } else {
                                addFragment(Invoices.newInstance(), "invoices");
                            }
                            break;
                        case 2:
                            addFragment(Purchases.newInstance(), "purchases");
                            break;
                        case 3:
                            addFragment(ServicesSale.newInstance(), "servicesSale");
                            break;
                        case 4:
                            addFragment(ServicesPurchase.newInstance(), "servicesPurchase");
                            break;
                        case 5:
                            addFragment(Product.newInstance(), "product");
                            break;
                        case 6:
                            addFragment(Customer.newInstance(), "customer");
                            break;
                        case 7:
                            addFragment(Vendor.newInstance(), "vendor");
                            break;
                        case 8:
                            addFragment(CompanyDetail.newInstance(), "companyDetail");
                            dList.setSelector(new StateListDrawable());
                            break;

                        case 9:
                            dList.setSelector(new StateListDrawable());
                            startActivity(new Intent(act, SettingActivity.class));
//                          addFragment(SettingDefaults.newInstance(), R.id.container, "settingDefaults");
                            break;
                        case 10:
                            screenAnalytics(act, AppUser.getUserId(act), "feedback");
                            addFragment(AppFeedback.newInstance(), R.id.container, "appFeedback");

                            break;
                        case 11:
                            screenAnalytics(act, AppUser.getUserId(act), "aboutUs");
                            addFragment(AboutUs.newInstance(), R.id.container, "aboutUs");

                            break;
                       case 12:
                           startActivity(new Intent(act, ChatSupportActivity.class));

                           break;
                        case 13:
                            AppUser.setUserId(act, "");
                            AppUser.setProfileId(act, "");
                            AppUser.setCompanyId(act, "");
                            AppUser.setName(act, "");
                            startActivity(new Intent(act, LoginActivity.class));
                            finish();
                            break;

                    }
                }
            }

        });

        setNavagationDrawer(act);
    }

    public void onSelectedProfile(CompanyModel item, Activity context) {
        AppUser.setName(context, item.getCompanyName());
        AppUser.setImage(context, item.getLogo());
        AppUser.setCompanyId(context, item.getCompanyId());
        AppUser.setProfileId(context, item.getCompanyId());

        Intent intent = new Intent(context, MainActivity.class);
        startActivity(intent);
        context.finish();
    }


    protected void updateCompanyProfileInDrawer(Context context) {
        mSideMenuDrawerList = getNavagationList(context);
        mSideMenuProfileList = getProfileList(context);
        slideMenuAdapter.notifyDataSetChanged();
    }

    private void setCompanyProfileInDrawer(Context context) {
        if (sideMenuList == null) {
            sideMenuList = new ArrayList<>();
        }

        if (mSideMenuProfileList != null) {
            slideMenuAdapter.setDrawerType(DRAWER_PROFILE);
            sideMenuList.clear();
            sideMenuList.addAll(mSideMenuProfileList);
            slideMenuAdapter.notifyDataSetChanged();
        } else {
            slideMenuAdapter.setDrawerType(DRAWER_PROFILE);
            sideMenuList.clear();
            mSideMenuProfileList = getProfileList(context);
            sideMenuList.addAll(mSideMenuProfileList);
            slideMenuAdapter.notifyDataSetChanged();
        }
        drawerSelection = DRAWER_PROFILE;
    }

    private void setNavagationDrawer(Context context) {
        if (sideMenuList == null) {
            sideMenuList = new ArrayList<>();
        }

        if (mSideMenuDrawerList != null) {
            slideMenuAdapter.setDrawerType(DRAWER_DEFAULT);
            sideMenuList.clear();
            sideMenuList.addAll(mSideMenuDrawerList);
            slideMenuAdapter.notifyDataSetChanged();
        } else {
            slideMenuAdapter.setDrawerType(DRAWER_DEFAULT);
            sideMenuList.clear();
            mSideMenuDrawerList = getNavagationList(context);
            sideMenuList.addAll(mSideMenuDrawerList);
            slideMenuAdapter.notifyDataSetChanged();
        }
        drawerSelection = DRAWER_DEFAULT;
    }

    public void closePrevoiusScreen() {
//        SortBarVIew fragment = (SortBarVIew) getSupportFragmentManager().findFragmentByTag("sortBarVIew");
//        if (fragment != null) {
//            fragment.onDismiss();
//        }
//        else if(getSupportFragmentManager().findFragmentByTag("sortBarVIew")!= null){
//            getSupportFragmentManager().popBackStack();
//        }else if(getSupportFragmentManager().findFragmentByTag("filterBarView")!= null){
//            getSupportFragmentManager().popBackStack();
//        }else if(getSupportFragmentManager().findFragmentByTag("orderBarView")!= null){
//            getSupportFragmentManager().popBackStack();
//        }
        getSupportFragmentManager().popBackStack();
    }

    public void screenAnalytics(Context context, String id, String screenName) {
//        FirebaseAnalytics mFAnalitics = FirebaseAnalytics.getInstance(context);
//        Bundle bundle = new Bundle();
//        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, id);
//        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, screenName);
//        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "screen");
//        mFAnalitics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    public void openFile(Activity activity, File url) {

        Uri uri = Uri.fromFile(url);

        Intent intent = new Intent(Intent.ACTION_VIEW);
        if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
            // Word document
            intent.setDataAndType(uri, "application/msword");
        } else if (url.toString().contains(".pdf")) {
            // PDF file
            intent.setDataAndType(uri, "application/pdf");
        } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
            // Powerpoint file
            intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
        } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
            // Excel file
            intent.setDataAndType(uri, "application/vnd.ms-excel");
        } else if (url.toString().contains(".zip") || url.toString().contains(".rar")) {
            // WAV audio file
            intent.setDataAndType(uri, "application/x-wav");
        } else if (url.toString().contains(".rtf")) {
            // RTF file
            intent.setDataAndType(uri, "application/rtf");
        } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
            // WAV audio file
            intent.setDataAndType(uri, "audio/x-wav");
        } else if (url.toString().contains(".gif")) {
            // GIF file
            intent.setDataAndType(uri, "image/gif");
        } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
            // JPG file
            intent.setDataAndType(uri, "image/jpeg");
        } else if (url.toString().contains(".txt")) {
            // Text file
            intent.setDataAndType(uri, "text/plain");
        } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") || url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
            // Video files
            intent.setDataAndType(uri, "video/*");
        } else {
            //if you want you can also define the intent type for any other file
            //additionally use else clause below, to manage other unknown extensions
            //in this case, Android will show all applications installed on the device
            //so you can choose which application to use
            intent.setDataAndType(uri, "*/*");
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);

    }

    public int parseInt(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public float parseFloat(String value) {
        try {
            return Float.parseFloat(value);
        } catch (NumberFormatException e) {
            return 0;
        }
    }


    public String parseString(float value) {
        try {
            return String.valueOf(value);
        } catch (Exception e) {
            return "";
        }
    }

    public String parseString(int value) {
        try {
            return String.valueOf(value);
        } catch (Exception e) {
            return "";
        }
    }


    public void updateProfile(Context act, String name, String image) {
        if (name.equals("")) {
            tvProfileName.setVisibility(View.GONE);
        } else {
            tvProfileName.setText(name);
        }
        if (!TextUtils.isEmpty(image)) {
            Glide.with(act)
                    .load(image)
                    .into(ivProfile);
        } else {
            Glide.with(act)
                    .load(R.drawable.profile)
                    .into(ivProfile);
        }
    }


    protected void repFragmentWithoutBackstack(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.commit();
    }

    protected void replaceFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    protected void setFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    public void addFragment(Fragment fragment, String tag) {
        addFragment(fragment, android.R.id.content, tag);
    }

    public void addFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    public void removeFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.remove(fragment);
        transaction.commit();
    }

    protected void addFragmentWithoutBackstack(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.commit();
    }

    // for 2 tabs
    public void setTabFragment(int containerId, Fragment addFragment, String fragTag
            , Fragment rFrag1, Fragment rFrag2, Fragment rFrag3) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (addFragment != null) {
            if (addFragment.isAdded()) { // if the fragment is already in container
                ft.show(addFragment);
            } else { // fragment needs to be added to frame container
                ft.add(containerId, addFragment, fragTag);
            }
        }

        // Hide 1st fragments
        if (rFrag1 != null && rFrag1.isAdded()) {
            ft.hide(rFrag1);
        }
        // Hide 2nd fragments
        if (rFrag2 != null && rFrag2.isAdded()) {
            ft.hide(rFrag2);
        }
        // Hide 2nd fragments
        if (rFrag3 != null && rFrag3.isAdded()) {
            ft.hide(rFrag3);
        }

        ft.commit();
    }

    public List<SideMenu> getNavagationList(Context context) {
        String businessType = AppUser.getBusinessType(context);
        ArrayList<SideMenu> listItem = new ArrayList<>();
        boolean isCompositionScheme = AppUser.getCompositionScheme(context);
        if (businessType.toLowerCase().equals(Const.BOTH_BUSINESS_TYPE)) {
            listItem.add(new SideMenu(getString(R.string.goods_sales), R.drawable.ic_nav_invoices));
            listItem.add(new SideMenu(getString(R.string.goods_purchases), R.drawable.ic_nav_purchases));
            if (isCompositionScheme) {
                listItem.add(new SideMenu(null, 0));
            } else {
                listItem.add(new SideMenu(getString(R.string.service_sale), R.drawable.ic_nav_invoices));
            }
            listItem.add(new SideMenu(getString(R.string.services_purchase), R.drawable.ic_nav_invoices));
            listItem.add(new SideMenu(getString(R.string.product), R.drawable.ic_nav_product));
        } else if (businessType.toLowerCase().equals(Const.TRADER_BUSINESS_TYPE)) {
            listItem.add(new SideMenu(getString(R.string.goods_sales), R.drawable.ic_nav_invoices));
            listItem.add(new SideMenu(getString(R.string.goods_purchases), R.drawable.ic_nav_purchases));
            listItem.add(new SideMenu(null, 0));
            listItem.add(new SideMenu(null, 0));
            listItem.add(new SideMenu(getString(R.string.product), R.drawable.ic_nav_product));
        } else if (businessType.toLowerCase().equals(Const.SERVICE_BUSINESS_TYPE)) {
            listItem.add(new SideMenu(null, 0));
            listItem.add(new SideMenu(null, 0));
            if (isCompositionScheme) {
                listItem.add(new SideMenu(null, 0));
            } else {
                listItem.add(new SideMenu(getString(R.string.service_sale), R.drawable.ic_nav_invoices));
            }
            listItem.add(new SideMenu(getString(R.string.services_purchase), R.drawable.ic_nav_invoices));
            listItem.add(new SideMenu(null, 0));
        } else {
            listItem.add(new SideMenu(null, 0));
            listItem.add(new SideMenu(null, 0));
            listItem.add(new SideMenu(null, 0));
            listItem.add(new SideMenu(null, 0));
            listItem.add(new SideMenu(null, 0));
        }
        listItem.add(new SideMenu(getString(R.string.customers), R.drawable.ic_nav_customer));
        listItem.add(new SideMenu(getString(R.string.vendors), R.drawable.ic_nav_vendor));


//        listItem.add(new SideMenu(getString(R.string.vouchers), R.drawable.ic_nav_credit));
//        listItem.add(new SideMenu(getString(R.string.creditors), R.drawable.ic_nav_transactions));
//        listItem.add(new SideMenu(getString(R.string.debtors), R.drawable.ic_nav_debit));
        listItem.add(new SideMenu(getString(R.string.company_setup), R.drawable.ic_nav_vendor));
        listItem.add(new SideMenu(getString(R.string.setting), R.drawable.ic_nav_service));
        listItem.add(new SideMenu(getString(R.string.feedback), R.drawable.ic_nav_feedback));
        listItem.add(new SideMenu(getString(R.string.about_us_tag), R.drawable.ic_nav_about));
        listItem.add(new SideMenu(getString(R.string.chat_support), R.drawable.ic_nav_about));
        listItem.add(new SideMenu(getString(R.string.logout), R.drawable.ic_nav_logout));
        return listItem;
    }

    public List<SideMenu> getProfileList(Context context) {
        ArrayList<SideMenu> listItem = new ArrayList<>();
        CompanyModel profileModel = JsonParser.getAllCompanyList(new CompanyModel(), AppUser.getCompanyDetails(context), AppUser.getCompanyId(context));
        if (profileModel.getOutput().equals(Const.SUCCESS)) {
            for (CompanyModel model : profileModel.getList()) {
                listItem.add(new SideMenu(model.getCompanyName(), model.getLogo(), model));
            }
            listItem.add(new SideMenu(getString(R.string.manage_company), "", new CompanyModel()));
        } else {
            listItem.add(new SideMenu(getString(R.string.manage_company), "", new CompanyModel()));
        }
        return listItem;
    }
}