package com.gennext.offlinegst.model.user;

/**
 * Created by Admin on 11/22/2017.
 */

public class DashboardModel {

    private String sales;
    private String purchases;
    private String debit;
    private String credit;
    private String totalInvoices;
    private String totalCancelledInvoices;
    private String taxRecieved;
    private String inputTaxCredit;
    private String businessType;
    private String companyName;
    private String status;
    private String message;
    private String signature;
    private String output;
    private String outputMsg;


    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getSales() {
        return sales;
    }

    public void setSales(String sales) {
        this.sales = sales;
    }

    public String getPurchases() {
        return purchases;
    }

    public void setPurchases(String purchases) {
        this.purchases = purchases;
    }

    public String getDebit() {
        return debit;
    }

    public void setDebit(String debit) {
        this.debit = debit;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getTotalInvoices() {
        return totalInvoices;
    }

    public void setTotalInvoices(String totalInvoices) {
        this.totalInvoices = totalInvoices;
    }

    public String getTotalCancelledInvoices() {
        return totalCancelledInvoices;
    }

    public void setTotalCancelledInvoices(String totalCancelledInvoices) {
        this.totalCancelledInvoices = totalCancelledInvoices;
    }

    public String getTaxRecieved() {
        return taxRecieved;
    }

    public void setTaxRecieved(String taxRecieved) {
        this.taxRecieved = taxRecieved;
    }

    public String getInputTaxCredit() {
        return inputTaxCredit;
    }

    public void setInputTaxCredit(String inputTaxCredit) {
        this.inputTaxCredit = inputTaxCredit;
    }
}
