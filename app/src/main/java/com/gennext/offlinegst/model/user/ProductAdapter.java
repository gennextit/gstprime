package com.gennext.offlinegst.model.user;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.panel.ActionMenu;
import com.gennext.offlinegst.user.product.Product;
import com.gennext.offlinegst.util.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ReyclerViewHolder>
        implements ActionMenu.MenuProduct {

    private final Product parentRef;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<ProductModel> items;
    private List<ProductModel> originalData;
    private ItemFilter mFilter = new ItemFilter();

    public ProductAdapter(Activity context, ArrayList<ProductModel> items,Product parentRef) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef = parentRef;
        this.items = items;
        this.originalData = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_user_product_format1, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final ProductModel item = items.get(position);

        holder.tvName.setText(item.getProductName());
        String stock;
        if(!TextUtils.isEmpty(item.getQuantity())&&!item.getQuantity().equals("null")){
            stock=item.getQuantity();
        }else {
            stock="0";
        }
        holder.tvQty.setText(Utility.generateBoldTitle("Stock:",stock));
        holder.tvSalePrice.setText(item.getSalePrice());
        holder.tvRate.setText(item.getTaxRate());
        setColor(holder.llSideLine);

        holder.llSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActionMenu.newInstance(ProductAdapter.this,item,position)
                        .show(parentRef.getFragmentManager(),"actionMenu");
            }
        });
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onViewClick(DialogFragment dialog, ProductModel product, int position) {
        parentRef.viewProduct(product);
    }

    @Override
    public void onEditClick(DialogFragment dialog, ProductModel itemProduct, int position) {
        parentRef.editProduct(itemProduct);
    }

    @Override
    public void onDeleteClick(DialogFragment dialog, ProductModel itemProduct, int position) {
        parentRef.deleteProduct(itemProduct,position);
    }



    @Override
    public void onCopyClick(DialogFragment dialog, ProductModel itemProduct, int position) {
        parentRef.copyProduct(itemProduct);
    }

    public void itemDeletedSuccessful(int position) {
        items.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position,getItemCount());
    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout llSlot,llSideLine;
        private TextView tvName,tvRate,tvQty,tvSalePrice;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSideLine = (LinearLayout) v.findViewById(R.id.ll_slot);
            llSlot = (LinearLayout) v.findViewById(R.id.layoutSlot);
            tvName = (TextView) v.findViewById(R.id.tv_slot_0);
            tvQty = (TextView) v.findViewById(R.id.tv_slot_1);
            tvSalePrice = (TextView) v.findViewById(R.id.tv_slot_2); 
            tvRate = (TextView) v.findViewById(R.id.tv_slot_3);
        }
    }

    public void setColor(LinearLayout tv) {
        Random rnd = new Random();
        int color= Utility.SIDE_LINE_COLORS[rnd.nextInt(21)];
        tv.setBackgroundColor(color);
    }

    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<ProductModel> list = originalData;

            int count = list.size();
            final ArrayList<ProductModel> nlist = new ArrayList<>(count);
            String filterableText;

            if (!filterString.equals("")) {
                for (ProductModel model : list) {
                    if (!filterString.equals("")) {
                        filterableText = model.getProductName();
                        filterableText+= model.getUnitName();
                        filterableText+= model.getCodeHSN();
                        if (filterableText.toLowerCase().contains(filterString)) {
                            nlist.add(model);
                        }
                    } else {
                        nlist.add(model);
                    }
                }
                results.values = nlist;
                results.count = nlist.size();
            } else {
                results.values = originalData;
                results.count = originalData.size();
            }

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            items = (ArrayList<ProductModel>) results.values;
            notifyDataSetChanged();
        }

    }
}

