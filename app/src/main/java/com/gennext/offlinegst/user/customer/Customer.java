package com.gennext.offlinegst.user.customer;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gennext.offlinegst.MainActivity;
import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.PopupDialog;
import com.gennext.offlinegst.model.Sort;
import com.gennext.offlinegst.model.user.CustomerAdapter;
import com.gennext.offlinegst.model.user.CustomerModel;
import com.gennext.offlinegst.model.user.CustomerModel;
import com.gennext.offlinegst.search.SearchBar;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.user.product.Product;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.ApiCallError;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.RequestBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Admin on 7/4/2017.
 */

public class Customer extends CompactFragment implements ApiCallError.ErrorListener ,PopupDialog.DialogListener, SearchBar.SearchOrder{
    private RecyclerView lvMain;
    private ArrayList<CustomerModel> cList;
    private CustomerAdapter adapter;
    private DBManager dbManager;

    private AssignTask assignTask;
    private FloatingActionButton btnAdd;
    private CoordinatorLayout coordinatorLayout;
    private TextView tvAddNote;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private CustomerModel mSelectedItem;
    private int mSelectedItemPos;
    private int mSelectedTask;
    private String mSortType;
    private int mListOrder;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static Fragment newInstance() {
        Customer fragment = new Customer();
        AppAnimation.setFadeAnimation(fragment);
        fragment.mListOrder = Sort.Ascending;
        fragment.mSortType = Sort.Type1;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_customer, container, false);
        initToolBarHome(getActivity(),v,getString(R.string.customer),"customer");
        screenAnalytics(getContext(),AppUser.getUserId(getContext()),"Customer");
        dbManager = DBManager.newIsntance(getActivity());
        initUi(v);
        addSearchTag(savedInstanceState);
        return v;
    }

    private void addSearchTag(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            Fragment fragment = SearchBar.newInstance(Customer.this, new String[]{"Name", "State", "GSTIN"},"Search by name, state or GSTIN");
            AppAnimation.setSlideAnimation(fragment, Gravity.TOP);
            getFragmentManager().beginTransaction()
                    .replace(R.id.container_bar, fragment , "searchBar")
                    .commit();
        }
    }
    @Override
    public void onTextChanged(CharSequence charSequence) {
        if (adapter != null)
            adapter.getFilter().filter(charSequence.toString());
    }

    @Override
    public void onShortButtonPressed(String type) {
        mSortType = type;
        if (cList==null){
            return;
        }
        if(type.equals(Sort.Type1)) {
            Collections.sort(cList, new Comparator<CustomerModel>() {
                @Override
                public int compare(CustomerModel item, CustomerModel item2) {
                    String s1 = item.getPersonName();
                    String s2 = item2.getPersonName();
                    if(mListOrder == Sort.Ascending) {
                        return s1.compareToIgnoreCase(s2);
                    }else{
                        return s2.compareToIgnoreCase(s1);
                    }
                }
            });
        } else if(type.equals(Sort.Type2)) {
            Collections.sort(cList, new Comparator<CustomerModel>() {
                @Override
                public int compare(CustomerModel item, CustomerModel item2) {
                    String s1 = item.getState();
                    String s2 = item2.getState();
                    if(mListOrder == Sort.Ascending) {
                        return s1.compareToIgnoreCase(s2);
                    }else{
                        return s2.compareToIgnoreCase(s1);
                    }
                }
            });
        }else if(type.equals(Sort.Type3)) {
            Collections.sort(cList, new Comparator<CustomerModel>() {
                @Override
                public int compare(CustomerModel item, CustomerModel item2) {
                    String s1 = item.getGSTIN();
                    String s2 = item2.getGSTIN();
                    if(mListOrder == Sort.Ascending) {
                        return s1.compareToIgnoreCase(s2);
                    }else{
                        return s2.compareToIgnoreCase(s1);
                    }
                }
            });
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onShortOrderPressed(int order) {
        mListOrder = order;
        if(mSortType!=null){
            onShortButtonPressed(mSortType);
        }
    }


    private void initUi(View v) {
        coordinatorLayout = (CoordinatorLayout) v.findViewById(R.id.coordinatorLayout);
        tvAddNote = (TextView) v.findViewById(R.id.tv_add_note);
        lvMain = (RecyclerView) v.findViewById(R.id.lv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());
        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                executeTask(TASK_LOAD_LIST);
            }
        });

        btnAdd = (FloatingActionButton) v.findViewById(R.id.fab);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFragment(CustomerManager.newInstance(Customer.this), "customerManager");
            }
        });
        lvMain.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && btnAdd.getVisibility() == View.VISIBLE) {
                    btnAdd.hide();
                } else if (dy < 0 && btnAdd.getVisibility() != View.VISIBLE) {
                    btnAdd.show();
                }
            }
        });
        executeTask(TASK_LOAD_LIST);
    }


    private void executeTask(int task) {
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });

        mSelectedTask=task;
        if(task==TASK_LOAD_LIST) {
            assignTask = new AssignTask(getActivity());
            assignTask.execute(AppSettings.GET_CUSTOMERS);
        }else{
            assignTask = new AssignTask(getActivity());
            assignTask.execute(AppSettings.DELETE_CUSTOMER);
        }
    }
    private void hideProgressBar() {
        if(mSwipeRefreshLayout!=null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        executeTask(mSelectedTask);
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }

    public void refreshList() {
        executeTask(TASK_LOAD_LIST);
    }

    public void deleteCustomer(CustomerModel itemCustomer,int pos) {
//        String result = dbManager.deleteCustomer(itemCustomer);
//        showSnakBar(coordinatorLayout, result);
        mSelectedItem = itemCustomer;
        mSelectedItemPos = pos;
        PopupDialog.newInstance(getString(R.string.delete_customer), getString(R.string.are_you_sure_to_delete)+" " + itemCustomer.getPersonName() +" " + getString(R.string.customer_lowercase), Customer.this)
                .show(getFragmentManager(), "popupDialog");
    }

    public void editCustomer(CustomerModel itemCustomer) {
        addFragment(CustomerManager.newInstance(Customer.this,itemCustomer, CustomerManager.EDIT_CUSTOMER), "customerManager");
    }

    public void copyCustomer(CustomerModel itemCustomer) {
        addFragment(CustomerManager.newInstance(Customer.this,itemCustomer, CustomerManager.COPY_CUSTOMER), "customerManager");
    }

    @Override
    public void onOkClick(DialogFragment dialog) {
        executeTask(TASK_DELETE);
    }

    @Override
    public void onCancelClick(DialogFragment dialog) {

    }

    public void viewCustomer(CustomerModel itemCustomer) {
        addFragment(CustomerManager.newInstance(Customer.this,itemCustomer, CustomerManager.VIEW_CUSTOMER), "customerManager");
    }


    private class AssignTask extends AsyncTask<String, Void, CustomerModel> {
        private Context context;

        private void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        private void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        private AssignTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected CustomerModel doInBackground(String... urls) {
            String userId = AppUser.getUserId(context);
            String setupId = AppUser.getCompanyId(context);
            CustomerModel result = new CustomerModel();
//            if (AppConfig.isUpdateToServer(context)) {

            if(mSelectedTask==TASK_LOAD_LIST) {
                String response = ApiCall.POST(urls[0], RequestBuilder.Default(userId,setupId));
                return JsonParser.getCustomerDetail(context,new CustomerModel(),response);
            }else{
                String id = null;
                if( mSelectedItem!=null){
                    id = mSelectedItem.getCustometId();
                }
                String response = ApiCall.POST(urls[0], RequestBuilder.deleteCustomer(userId, setupId, id != null ? id : ""));
                return JsonParser.deleteCustomer(response);
            }

//            }
//            return dbManager.getCustomerList(result,userId);
        }


        @Override
        protected void onPostExecute(CustomerModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressBar();
                if (!AppConfig.isUpdateToServer(context)) {
                    if (result.getOutputDB().equals(Const.SUCCESS)) {
                        cList = result.getList();
                        adapter = new CustomerAdapter(getActivity(), cList, Customer.this);
                        lvMain.setAdapter(adapter);
                        tvAddNote.setVisibility(View.GONE);
                    } else if (result.getOutputDB().equals(Const.FAILURE)) {
                        addFragment(CustomerManager.newInstance(Customer.this), "customerManager");
                    }
                } else {
                    if (result.getOutput().equals(Const.SUCCESS)) {
                        if(mSelectedTask==TASK_LOAD_LIST) {
                            cList = result.getList();
                            adapter = new CustomerAdapter(getActivity(), cList, Customer.this);
                            lvMain.setAdapter(adapter);
                            onShortButtonPressed(mSortType);
                            tvAddNote.setVisibility(View.GONE);
                        }else{
                            if(mSelectedItem!=null) {
                                adapter.itemDeletedSuccessful(mSelectedItemPos);
                            }
                            showSnakBar(coordinatorLayout, result.getOutputMsg());
                        }

                    } else if (result.getOutput().equals(Const.FAILURE)) {
                        showSnakBar(coordinatorLayout,result.getOutputMsg());
                    } else {
                        ApiCallError.newInstance(result.getOutput(),result.getOutputMsg(), Customer.this)
                                .show(getFragmentManager(), "apiCallError");
                    }
                }
            }
        }
    }

}
