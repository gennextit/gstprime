package com.gennext.offlinegst.setting;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.gennext.offlinegst.MainActivity;
import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.model.user.CommonModel;
import com.gennext.offlinegst.panel.FileSelector;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.DateTimeUtility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

/**
 * Created by Admin on 7/4/2017.
 */

public class BackupSetting extends CompactFragment implements FileSelector.SelectListener{

    public static Fragment newInstance() {
        BackupSetting fragment = new BackupSetting();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_backup, container, false);
        initToolBar(getActivity(),v,getString(R.string.backup_setting));
        screenAnalytics(getContext(), AppUser.getUserId(getContext()), "BackupSetting");
        initUi(v);
        return v;
    }

    private void initUi(View v) {
        ((Button)v.findViewById(R.id.btn_import)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File folderThatContainsAllFiles = getActivity().getExternalFilesDir(AppConfig.FOLDER_DATABASE);
                FileSelector.newInstance(BackupSetting.this,folderThatContainsAllFiles).show(getFragmentManager(),"fileSelector");
            }
        });
        ((Button)v.findViewById(R.id.btn_export)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exportDB();
//                exportBackupWithCheck();
            }
        });
    }

    private void importDB(String filePath) {
        try {
            File currentDB =  getActivity().getDatabasePath(DBManager.getDatabaseName());
            File backupDB = new File(filePath);

            FileChannel src = new FileInputStream(backupDB).getChannel();
            FileChannel dst = new FileOutputStream(currentDB).getChannel();
            dst.transferFrom(src, 0, src.size());
            src.close();
            dst.close();
            Toast.makeText(getContext(), "Import Successful!",
                    Toast.LENGTH_SHORT).show();

        } catch (Exception e) {

            Toast.makeText(getContext(), "Import Failed!", Toast.LENGTH_SHORT)
                    .show();

        }
    }


    //  /data/storage/emulated/0/Android/data/com.gennext.offlinegst/files/databases/ugstDB001.db
    //  /storage/emulated/0/Android/data/com.gennext.offlinegst/files/databases/ugstDB001.db

    private void exportDB() {
        try {
            File currentDB =  getActivity().getDatabasePath(DBManager.getDatabaseName());
            String databaseName = DBManager.DATABASE_BACKUP_PREFIX + DateTimeUtility.getTimeStamp() + ".db";
            File backupDB = new File(getActivity().getExternalFilesDir(AppConfig.FOLDER_DATABASE), databaseName);

//            File path1 = Environment.getExternalStorageDirectory();
//            String path2 = getActivity().getFilesDir().getAbsolutePath();
//            File backupDB = new File(path1, databaseName);

            FileChannel src = new FileInputStream(currentDB).getChannel();
            FileChannel dst = new FileOutputStream(backupDB).getChannel();
            dst.transferFrom(src, 0, src.size());
            src.close();
            dst.close();

            Toast.makeText(getContext(), "Backup Successful!",
                    Toast.LENGTH_SHORT).show();

        } catch (Exception e) {

            Toast.makeText(getContext(), "Backup Failed!", Toast.LENGTH_SHORT)
                    .show();

        }
    }

    @Override
    public void onFileSelect(DialogFragment dialog, CommonModel fileSelected) {
        importDB(fileSelected.getId());
    }

    public void allowStoragePermission() {
        exportDB();
    }


    private void exportBackupWithCheck() {
        // BEGIN_INCLUDE(startCamera)
        // Check if the Camera permission has been granted
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            // Permission is already available, start camera preview
            exportDB();
        } else {
            // Permission is missing and must be requested.
            ((MainActivity)getActivity()).requestStoragePermission();
        }
        // END_INCLUDE(startCamera)
    }
//    public void allowStoragePermission() {
//        exportDB2();
//    }
//    private void exportDB2() {
//        try {
//            File currentDB =  getActivity().getDatabasePath(DBManager.getDatabaseName());
//            String databaseName = DBManager.DATABASE_BACKUP_PREFIX + DateTimeUtility.getTimeStamp() + ".db";
//            File file = Environment.getExternalStorageDirectory();
//            File filepathname = new File(file.getPath()+"/OfflineGST");
//            if(filepathname.mkdir()){
//                L.m("directory created");
//            }else{
//                L.m("directory not created");
//            }
//            File backupDB = new File(filepathname, databaseName);
//
//            FileChannel src = new FileInputStream(currentDB).getChannel();
//            FileChannel dst = new FileOutputStream(backupDB).getChannel();
//            dst.transferFrom(src, 0, src.size());
//            src.close();
//            dst.close();
//            Toast.makeText(getContext(), "Backup Successful!",
//                    Toast.LENGTH_SHORT).show();
//
//        } catch (Exception e) {
//
//            Toast.makeText(getContext(), "Backup Failed!", Toast.LENGTH_SHORT)
//                    .show();
//
//        }
//    }
}

