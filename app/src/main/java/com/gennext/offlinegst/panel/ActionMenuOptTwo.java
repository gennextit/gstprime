package com.gennext.offlinegst.panel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.model.user.ProfileModel;


public class ActionMenuOptTwo extends BottomSheetDialogFragment implements View.OnClickListener {
    private int position;
    private MenuProfile mListener;
    private ProfileModel itemProfile;

    public interface MenuProfile {
        void onEditClick(DialogFragment dialog, ProfileModel profile, int position);

        void onDeleteClick(DialogFragment dialog, ProfileModel profile, int position);
    }


    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }


    public static ActionMenuOptTwo newInstance(MenuProfile listener,ProfileModel itemProfile,int position) {
        ActionMenuOptTwo fragment = new ActionMenuOptTwo();
        fragment.mListener = listener;
        fragment.itemProfile = itemProfile;
        fragment.position = position;
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.menu_action_opt_two, container, false);

        TextView tvTitle = (TextView) v.findViewById(R.id.action_menu_title);
        ImageView ivClose = (ImageView) v.findViewById(R.id.action_menu_close);
        LinearLayout edit = (LinearLayout) v.findViewById(R.id.action_menu_edit);
        LinearLayout delete = (LinearLayout) v.findViewById(R.id.action_menu_delete);

        if (itemProfile != null) {
            tvTitle.setText(itemProfile.getProfileName());
        }
        ivClose.setOnClickListener(this);
        edit.setOnClickListener(this);
        delete.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.action_menu_close:
                dismiss();
                break;
            case R.id.action_menu_edit:
                dismiss();
                if (mListener != null) {
                    mListener.onEditClick(ActionMenuOptTwo.this,itemProfile,position);
                }

                break;
            case R.id.action_menu_delete:
                dismiss();
                if (mListener != null) {
                    mListener.onDeleteClick(ActionMenuOptTwo.this,itemProfile,position);
                }

                break;


        }


    }


}
