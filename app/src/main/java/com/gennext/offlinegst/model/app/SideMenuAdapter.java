package com.gennext.offlinegst.model.app;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gennext.offlinegst.NavDrawer;
import com.gennext.offlinegst.R;

import java.util.List;


public class SideMenuAdapter extends ArrayAdapter<SideMenu> {
    List<SideMenu> sideMenuList;
    private Activity activity;
    private LayoutInflater inflater;
    int resource;
    private int drawerType;

    public SideMenuAdapter(Activity activity, int resource, List<SideMenu> sideMenuList,int drawerType) {
        super(activity, resource, sideMenuList);
        this.activity = activity;
        this.resource = resource;
        this.drawerType = drawerType;
        this.sideMenuList = sideMenuList;
    }

    public void setDrawerType(int drawerType) {
        this.drawerType = drawerType;
    }

    public class ViewHolder {
        TextView name;
        LinearLayout slot;
        ImageView image1,image2;
        public View breakline;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(resource, parent, false);

            holder.slot = (LinearLayout) view.findViewById(R.id.ll_slot);
            holder.name = (TextView) view.findViewById(R.id.name);
            holder.breakline = (View) view.findViewById(R.id.breakline);
            holder.image1 = (ImageView) view.findViewById(R.id.image1);
            holder.image2 = (ImageView) view.findViewById(R.id.image2);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

//		if(position==getCount()-1){
//			holder.breakline.setVisibility(View.GONE);
//		}
        if(sideMenuList.get(position).getName()==null){
            holder.slot.setVisibility(View.GONE);
        }else {
            if (drawerType == NavDrawer.DRAWER_DEFAULT) {
                holder.name.setText(sideMenuList.get(position).getName());
                holder.image2.setImageResource(sideMenuList.get(position).getImage());
                holder.image2.setVisibility(View.VISIBLE);
                holder.image1.setVisibility(View.GONE);
            } else {
                holder.name.setText(sideMenuList.get(position).getName());
                String mImagePath = sideMenuList.get(position).getImagePath();
                holder.image2.setVisibility(View.GONE);
                holder.image1.setVisibility(View.VISIBLE);
                if (getCount() - 1 == position) {
                    holder.image1.setImageResource(R.drawable.ic_nav_service);
                } else {
                    holder.image1.setBackgroundResource(R.drawable.bg_profile_half);
                    if (!TextUtils.isEmpty(mImagePath)) {
                        Glide.with(activity)
                                .load(mImagePath)
                                .into(holder.image1);
                    } else {
                        Glide.with(activity)
                                .load(R.drawable.profile)
                                .into(holder.image1);
                    }
                }
            }
        }

        return view;
    }
}
