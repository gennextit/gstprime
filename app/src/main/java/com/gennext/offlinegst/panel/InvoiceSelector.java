package com.gennext.offlinegst.panel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.model.user.InvoiceModel;
import com.gennext.offlinegst.model.user.InvoiceSelectorAdapter;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;

import java.util.ArrayList;


public class InvoiceSelector extends DialogFragment{
    private SelectListener mListener;
    private InvoiceSelectorAdapter adapter;
    private DBManager dbManager;

    public interface SelectListener {
        void onInvoiceSelect(DialogFragment dialog, InvoiceModel selectedInvoice);

    }


    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }


    public static InvoiceSelector newInstance(SelectListener listener) {
        InvoiceSelector fragment = new InvoiceSelector();
        fragment.mListener = listener;
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_menu_state, container, false);
        dbManager= DBManager.newIsntance(getActivity());
        EditText etSearch = (EditText) v.findViewById(R.id.et_search);
        ListView lvMain = (ListView) v.findViewById(R.id.lv_main);


        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (adapter != null)
                    adapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                if (adapter != null) {
                    InvoiceModel model = adapter.getItem(pos);
                    if (mListener != null) {
                        mListener.onInvoiceSelect(InvoiceSelector.this,model);
                    }
                    dismiss();
                }
            }
        });
        InvoiceModel cModel = dbManager.getInvoicesList(new InvoiceModel(), AppUser.getUserId(getActivity()), AppUser.getProfileId(getActivity()));
        if (cModel!=null&& cModel.getOutputDB().equals(Const.SUCCESS)) {
            adapter = new InvoiceSelectorAdapter(getActivity(), cModel.getList());
            lvMain.setAdapter(adapter);
        }else{
            ArrayList<String> errorList = new ArrayList<>();
            errorList.add(getString(R.string.invoice_not_fount_message));
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.slot_empty_product,
                    R.id.tv_message, errorList);
            lvMain.setAdapter(adapter);
        }

        return v;
    }


}
