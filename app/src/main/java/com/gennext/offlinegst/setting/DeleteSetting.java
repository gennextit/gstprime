package com.gennext.offlinegst.setting;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.PopupDialog;
import com.gennext.offlinegst.model.user.SettingAdapter;
import com.gennext.offlinegst.model.user.SettingModel;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.DBManager;

import java.util.ArrayList;

/**
 * Created by Admin on 7/4/2017.
 */

public class DeleteSetting extends CompactFragment implements PopupDialog.DialogParamListener {
    private RecyclerView lvMain;
    private ArrayList<SettingModel> cList;
    private SettingAdapter adapter;
    private DBManager dbManager;
    private CoordinatorLayout coLayout;


    //    private Toolbar mToolbar;
    public static DeleteSetting newInstance() {
        DeleteSetting fragment = new DeleteSetting();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.setting_delete, container, false);
        initToolBar(getActivity(), v, "Delete Database");
        dbManager = DBManager.newIsntance(getActivity());
        initUi(v);
        return v;
    }

    private void initUi(View v) {
        coLayout = (CoordinatorLayout) v.findViewById(R.id.coordinatorLayout);
        lvMain = (RecyclerView) v.findViewById(R.id.lv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, true);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setHasFixedSize(true);
        lvMain.setItemAnimator(new DefaultItemAnimator());


        cList = getDeleteSettingList();
        adapter = new SettingAdapter(getActivity(), cList, DeleteSetting.this);
        lvMain.setAdapter(adapter);
    }

    // Const params are must be same
    private ArrayList<SettingModel> getDeleteSettingList() {
        ArrayList<SettingModel> list = new ArrayList<>();
        list.add(setItem(Const.DELETE_VOUCHERS, "All Vouchers"));
        list.add(setItem(Const.DELETE_PRODUCT_STOCK, "Product stocks"));
        list.add(setItem(Const.DELETE_SELL_INVOICES, "All Sell Invoices"));
        list.add(setItem(Const.DELETE_PURCHASE_INVOICES, "All Purchase Invoices"));
        list.add(setItem(Const.DELETE_VENDOR, "All vendors"));
        list.add(setItem(Const.DELETE_CUSTOMER, "All customers"));
        list.add(setItem(Const.DELETE_SERVICES, "All Services Details"));
        list.add(setItem(Const.DELETE_PRODUCT, "All Product details"));
        list.add(setItem(Const.DELETE_COMPANY, "All Company details"));
        list.add(setItem(Const.DELETE_PROFILE, "UpdateProfile detail"));
        return list;
    }

    private SettingModel setItem(String id, String title) {
        SettingModel model = new SettingModel();
        model.setId(id);
        model.setTitle(title);
        return model;
    }

    public void onSelectedItem(SettingModel item) {
        String[] params = {item.getId(), item.getTitle()};
        PopupDialog.newInstance(item.getTitle(), "Are you sure to delete " + item.getTitle().toLowerCase(), params, DeleteSetting.this).
                show(getFragmentManager(), "popupDialog");
    }

    @Override
    public void onDialogOkClick(DialogFragment dialog, String[] params) {
        switch (params[0]) {
            case Const.DELETE_PROFILE:
                dbManager.dropProfileTable();
                break;
            case Const.DELETE_PRODUCT:
                dbManager.dropProductTable();
                break;
            case Const.DELETE_SERVICES:
                dbManager.dropServicesTable();
                break;
            case Const.DELETE_CUSTOMER:
                dbManager.dropCustomerTable();
                break;
            case Const.DELETE_VENDOR:
                dbManager.dropVendorTable();
                break;
            case Const.DELETE_COMPANY:
                dbManager.dropCompanyTable();
                break;
            case Const.DELETE_PURCHASE_INVOICES:
                dbManager.dropPurchaseInvoicesTable();
                break;
            case Const.DELETE_SELL_INVOICES:
                dbManager.dropSellInvoicesTable();
                break;
            case Const.DELETE_PRODUCT_STOCK:
                dbManager.dropProductStockTable();
                break;
            case Const.DELETE_VOUCHERS:
                dbManager.dropVouchersTable();
                break;

        }
        showSnakBar(coLayout, "Successful deleted " + params[1].toLowerCase());
    }

    @Override
    public void onDialogCancelClick(DialogFragment dialog, String[] params) {

    }
}
