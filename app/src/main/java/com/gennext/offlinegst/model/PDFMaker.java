package com.gennext.offlinegst.model;

import android.app.Activity;
import android.text.TextUtils;

import com.gennext.offlinegst.MainActivity;
import com.gennext.offlinegst.global.PopupAlert;
import com.gennext.offlinegst.model.user.CompanyModel;
import com.gennext.offlinegst.model.user.CustomerModel;
import com.gennext.offlinegst.model.user.InvoiceModel;
import com.gennext.offlinegst.model.user.ProductModel;
import com.gennext.offlinegst.model.user.VoucherModel;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DateTimeUtility;
import com.gennext.offlinegst.util.L;
import com.gennext.offlinegst.util.NumberToWordsConverter;
import com.gennext.offlinegst.util.Words;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Admin on 8/2/2017.
 */

public class PDFMaker {
    private static final int NO_ROWSPAN = -1;
    private static final int NO_COLSPAN = -1;
    private Activity mContext;
    private String directoryName = "Invoices";

    private static final float PADDING_LOGO = 15;private static final float PADDING_NORMAL = 4;
    private static final float PADDING_SMALL = 2;
    private static final float PADDING_ZERO = 0;
    private static Font catFontBold = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.NORMAL);
    private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
    private static Font small = new Font(Font.FontFamily.TIMES_ROMAN, 5, Font.NORMAL);
    private static Font normal = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);

    private static Font normalBold = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
    private static Font normalBodyBold = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
    private static Font normalBody = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);
    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
    private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
    private Font currencyFont;

    public static PDFMaker newInstance(Activity mContext) {
        PDFMaker pdfMaker = new PDFMaker();
        pdfMaker.mContext = mContext;
        return pdfMaker;
    }

    public File generateInvoicePDF(InvoiceModel invModel, CompanyModel companyDetail, String selectedPrintType) throws IOException, DocumentException {

        ArrayList<String> unionTerritoryList = new ArrayList<>();
        unionTerritoryList.add("Chandigarh");
        unionTerritoryList.add("Lakshadweep");
        unionTerritoryList.add("Daman and Diu");
        unionTerritoryList.add("Dadar and Nagar Haveli");
        unionTerritoryList.add("Andaman and Nicobar Islands");
        // step 1
        Boolean isBatchAvailabe = AppConfig.isBatchAvailable(mContext);
        Boolean isExpAvailabe = AppConfig.isExpAvailable(mContext);
        Boolean isBankAvailabe = AppConfig.isBankAvailable(mContext);
        Boolean isIGSTAvailabe = AppConfig.isIGSTAvailable(mContext);
        Boolean isVehicelAvailabe = AppConfig.isVehicelAvailable(mContext);
        Boolean isDLNoAvailabe = AppConfig.isDLNoAvailable(mContext);
        Document document = new Document();
        String filename = invModel.getCustomerName() + "_Invoice" + DateTimeUtility.getTimeStamp() + ".pdf";
        // step 2
        File filePath = new File(mContext.getExternalFilesDir(AppConfig.FOLDER_INVOICES), filename);
        PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(filePath));

        currencyFont = FontFactory.getFont("assets/font/PlayfairDisplay-Regular.ttf", BaseFont.IDENTITY_H
                , BaseFont.EMBEDDED, 7, Font.BOLD);

        // step 3
        document.open();
        // step 4
        if (invModel.getCancelled().toLowerCase().equals("y")) {
            pdfWriter.setPageEvent(new PDFEventListener());// watermark
        }
        // stem 5
        PdfPTable table;
        table = addInvoiceHearderTable(invModel, companyDetail, isVehicelAvailabe, isDLNoAvailabe, selectedPrintType);
        table.setWidthPercentage(105);
        table.setHorizontalAlignment(Element.ALIGN_CENTER);
        document.add(table);

        boolean result = unionTerritoryList.contains(invModel.getCustomerState());
        String gstHeader;
        if (result) {
            gstHeader = "UTGST";
        } else {
            gstHeader = "SGST";
        }
        boolean isIGSTApplicable;
        if (companyDetail.getStateCode().equalsIgnoreCase(invModel.getCustomerStateCode())) {
            isIGSTApplicable = false;
        } else {
            isIGSTApplicable = true;
        }
        String[] allGST = calculateProductGST(invModel.getProductList(), invModel);

        table = addInvoiceBodyTable(invModel, isIGSTAvailabe, isIGSTApplicable, gstHeader,allGST[4]);
        table.setWidthPercentage(105);
        table.setHorizontalAlignment(Element.ALIGN_CENTER);
        document.add(table);

        table = addInvoiceFooterTable(allGST, invModel, companyDetail, isBankAvailabe, isIGSTAvailabe, isIGSTApplicable, gstHeader);
        table.setWidthPercentage(105);
        table.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.setSpacingAfter(5);
        document.add(table);

//        String validity = AppUser.getPackageStatus(mContext);
//        if (!validity.toLowerCase().equals(Const.PAID)) {
//            Paragraph phrase = new Paragraph();
//            phrase.add(new Chunk(Const.FOOTER_PDF_1, normal));
//            phrase.add(new Chunk(Const.FOOTER_PDF_2, normal));
//            phrase.add(new Chunk(Const.FOOTER_PDF_3, normal));
//            phrase.add(new Chunk(Const.FOOTER_PDF_4, normal));
//            phrase.setAlignment(Element.ALIGN_CENTER);
//            document.add(phrase);
//        }
        // step 5
        document.close();
        return filePath;
    }

    private PdfPTable addInvoiceBodyTable(InvoiceModel invModel, Boolean isIGSTAvailabe, Boolean isIGSTApplicable, String gstHeader, String totalCessValue) throws DocumentException {
        boolean isSerialAvailable = AppConfig.getSerialAvailable(mContext);
        boolean isUnitAvailable = AppConfig.getUnitAvailable(mContext);
        boolean isHSNAvailable = AppConfig.getHSNAvailable(mContext);
        boolean isTaxAvailable = AppConfig.getTAXAvailable(mContext);
        boolean isMRPAvailable = AppConfig.isMrpAvailable(mContext);
        boolean isDiscountAvailable = AppConfig.getDiscountAvailable(mContext);
        boolean isCessAvailable = AppConfig.getCessAvailable(mContext);
        boolean isTaxableAvailable = AppConfig.getTaxableAvailable(mContext);
        boolean isBlankRowsAvailable = AppConfig.getBlankRowsAvailable(mContext);

        PdfPTable table = new PdfPTable(19);
        int[] tableWidths = new int[]{2, 7, 2, 4, 3, 3, 3, 4, 2, 4, 3, 3, 3, 3, 3, 3, 3, 3, 5};
//        table.setWidthPercentage(288 / 5.23f);
        if (!isSerialAvailable) {
            tableWidths[0] = 0;
        }
        // product name[1]

        // quantity [2]

        if (!isUnitAvailable) {
            tableWidths[3] = 0;
        }
        if (!isHSNAvailable) {
            tableWidths[4] = 0;
        }
        if (!isTaxAvailable) {
            tableWidths[5] = 0;
        }
        if (!isMRPAvailable) {
            tableWidths[6] = 0;
        }
        // rate [7]
        if (!isDiscountAvailable) {
            tableWidths[8] = 0;
        }
        if (!isTaxableAvailable) {
            tableWidths[9] = 0;
        }
        if (isIGSTApplicable) {
            tableWidths[10] = 0;
            tableWidths[11] = 0;
            tableWidths[12] = 0;
            tableWidths[13] = 0;
        }
        if (!isIGSTAvailabe) {
            tableWidths[14] = 0;
            tableWidths[15] = 0;
        } else {
            if (!isIGSTApplicable) {
                tableWidths[14] = 0;
                tableWidths[15] = 0;
            }
        }
        if (!isCessAvailable) {
            tableWidths[16] = 0;
            tableWidths[17] = 0;
        }else{
            if(parseFloat(totalCessValue)==0) {
                tableWidths[16] = 0;
                tableWidths[17] = 0;
            }
        }

        // total Amount[18]


        table.setWidths(tableWidths);
        PdfPCell cell;

//        cell = new PdfPCell(new Phrase("S/N", normalBodyBold));
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setVerticalAlignment(Element.ALIGN_CENTER);
//        cell.setRowspan(2);
//        table.addCell(cell);
        table.addCell(setHeader("S/N", Element.ALIGN_CENTER, 2));

//        cell = new PdfPCell(new Phrase("Product Name", normalBodyBold));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setVerticalAlignment(Element.ALIGN_CENTER);
//        cell.setRowspan(2);
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        table.addCell(cell);
        table.addCell(setHeader("Product Name", Element.ALIGN_CENTER, 2));

//        cell = new PdfPCell(new Phrase("Qty", normalBodyBold));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setRowspan(2);
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        table.addCell(cell);
        table.addCell(setHeader("Qty", Element.ALIGN_CENTER, 2));

//        cell = new PdfPCell(new Phrase("Unit", normalBodyBold));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setVerticalAlignment(Element.ALIGN_CENTER);
//        cell.setRowspan(2);
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        table.addCell(cell);
        table.addCell(setHeader("Unit", Element.ALIGN_CENTER, 2));

//        cell = new PdfPCell(new Phrase("HSN", normalBodyBold));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setVerticalAlignment(Element.ALIGN_CENTER);
//        cell.setRowspan(2);
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        table.addCell(cell);
        table.addCell(setHeader("HSN", Element.ALIGN_CENTER, 2));

//        cell = new PdfPCell(new Phrase("Tax%", normalBodyBold));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setVerticalAlignment(Element.ALIGN_CENTER);
//        cell.setRowspan(2);
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        table.addCell(cell);
        table.addCell(setHeader("Tax%", Element.ALIGN_CENTER, 2));

//        cell = new PdfPCell(new Phrase("MRP", normalBodyBold));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setVerticalAlignment(Element.ALIGN_CENTER);
//        cell.setRowspan(2);
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        table.addCell(cell);
        table.addCell(setHeader("MRP", Element.ALIGN_CENTER, 2));

        Phrase phrase = new Phrase();
        phrase.add(new Chunk("Rate", normalBodyBold));
        phrase.add(setCurrency(invModel.getCurrency()));
//        cell = new PdfPCell(phrase);
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setVerticalAlignment(Element.ALIGN_CENTER);
//        cell.setRowspan(2);
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        table.addCell(cell);
        table.addCell(setHeader(phrase, Element.ALIGN_CENTER, 2));

        phrase = new Phrase();
        phrase.add(new Chunk("Dis", normalBodyBold));
        phrase.add(setCurrency(invModel.getCurrency()));
//        cell = new PdfPCell(phrase);
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setVerticalAlignment(Element.ALIGN_CENTER);
//        cell.setRowspan(2);
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        table.addCell(cell);
        table.addCell(setHeader(phrase, Element.ALIGN_CENTER, 2));

        phrase = new Phrase();
        phrase.add(new Chunk("Taxable", normalBodyBold));
        phrase.add(setCurrency(invModel.getCurrency()));
//        cell = new PdfPCell(phrase);
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setVerticalAlignment(Element.ALIGN_CENTER);
//        cell.setRowspan(2);
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        table.addCell(cell);
        table.addCell(setHeader(phrase, Element.ALIGN_CENTER, 2));

//        cell = new PdfPCell(new Phrase("CGST", normalBodyBold));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setColspan(2);
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        table.addCell(cell);
        table.addCell(setHeader("CGST", Element.ALIGN_CENTER, NO_ROWSPAN, 2));

//        cell = new PdfPCell(new Phrase(gstHeader, normalBodyBold));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setColspan(2);
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        table.addCell(cell);
        table.addCell(setHeader(gstHeader, Element.ALIGN_CENTER, 0, 2));

//        cell = new PdfPCell(new Phrase("IGST", normalBodyBold));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setColspan(2);
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        table.addCell(cell);
        table.addCell(setHeader("IGST", Element.ALIGN_CENTER, 0, 2));

//        cell = new PdfPCell(new Phrase("Cess", normalBodyBold));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setColspan(2);
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        table.addCell(cell);
        table.addCell(setHeader("Cess", Element.ALIGN_CENTER, 0, 2));


//        cell = new PdfPCell(phrase);
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        table.addCell(cell);
        table.addCell(setHeader("Total", Element.ALIGN_CENTER));

//        cell = new PdfPCell(new Phrase("%", normalBodyBold));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        table.addCell(cell);
        table.addCell(setHeader("%", Element.ALIGN_CENTER));

//        cell = new PdfPCell(new Phrase("Val", normalBodyBold));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        table.addCell(cell);
        phrase = new Phrase();
        phrase.add(new Chunk("Val", normalBodyBold));
        phrase.add(setCurrency(invModel.getCurrency()));
        table.addCell(setHeader(phrase, Element.ALIGN_CENTER));

//        cell = new PdfPCell(new Phrase("%", normalBodyBold));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        table.addCell(cell);
        table.addCell(setHeader("%", Element.ALIGN_CENTER));

//        cell = new PdfPCell(new Phrase("Val", normalBodyBold));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        table.addCell(cell);
        phrase = new Phrase();
        phrase.add(new Chunk("Val", normalBodyBold));
        phrase.add(setCurrency(invModel.getCurrency()));
        table.addCell(setHeader(phrase, Element.ALIGN_CENTER));

//        cell = new PdfPCell(new Phrase("%", normalBodyBold));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        table.addCell(cell);
        table.addCell(setHeader("%", Element.ALIGN_CENTER));

//        cell = new PdfPCell(new Phrase("Val", normalBodyBold));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        table.addCell(cell);
        phrase = new Phrase();
        phrase.add(new Chunk("Val", normalBodyBold));
        phrase.add(setCurrency(invModel.getCurrency()));
        table.addCell(setHeader(phrase, Element.ALIGN_CENTER));

//        cell = new PdfPCell(new Phrase("%", normalBodyBold));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        table.addCell(cell);
        table.addCell(setHeader("%", Element.ALIGN_CENTER));

//        cell = new PdfPCell(new Phrase("Val", normalBodyBold));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        table.addCell(cell);
        phrase = new Phrase();
        phrase.add(new Chunk("Val", normalBodyBold));
        phrase.add(setCurrency(invModel.getCurrency()));
        table.addCell(setHeader(phrase, Element.ALIGN_CENTER));

//        cell = new PdfPCell(new Phrase("Amt", normalBodyBold));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        table.addCell(cell);
        phrase = new Phrase();
        phrase.add(new Chunk("Amt", normalBodyBold));
        phrase.add(setCurrency(invModel.getCurrency()));
        table.addCell(setHeader(phrase, Element.ALIGN_CENTER));

        ArrayList<ProductModel> prodList = invModel.getProductList();
        for (int i = 0; i < prodList.size(); i++) {
            ProductModel prod = prodList.get(i);
            String totalProductsAmt = getTotalProductPrice(prod.getQuantity(), prod.getSalePrice(), prod.getDis());
            String mCessValue = calTaxValue(totalProductsAmt, prod.getCessRate());
            String totalProductTax = String.valueOf(convertToFloat(prod.getCgstVal()) + convertToFloat(prod.getSgstVal()) + convertToFloat(prod.getIgstVal()) + convertToFloat(mCessValue));
            setAllProductInInvoice(table, String.valueOf(i + 1), decimalFormat2(prod.getQuantity()), prod.getProductName(), prod.getBatch(), prod.getExp()
                    , prod.getCodeHSN(), decimalFormat2(prod.getTaxRate()), "", prod.getSalePrice(), decimalFormat2(prod.getDis()), totalProductsAmt, decimalFormat2(prod.getCgstTax()), decimalFormat2(prod.getCgstVal())
                    , decimalFormat2(prod.getSgstTax()), decimalFormat2(prod.getSgstVal()), decimalFormat2(prod.getIgstTax()), decimalFormat2(prod.getIgstVal()), totalPrice(totalProductsAmt, totalProductTax)
                    , prod.getUnitName(), prod.getCessRate(), mCessValue);
        }
        String frightAmt = invModel.getFreightAmount();
        if (frightAmt.equals("")) {
            frightAmt = "0";
        }
        float mFreightAmt = convertToFloat(frightAmt);
        float mCGST = convertToFloat(invModel.getCgstVal());
        float mSGST = convertToFloat(invModel.getSgstVal());
        float mIGST = convertToFloat(invModel.getIgstVal());
        setAllProductInInvoice(table, " ", " ", "Freight", " ", " ", " ", decimalFormat2(invModel.getFreightTaxRate()), "", frightAmt
                , "0", frightAmt, decimalFormat2(invModel.getCgstTax()), decimalFormat2(invModel.getCgstVal()), decimalFormat2(invModel.getSgstTax()), decimalFormat2(invModel.getSgstVal())
                , decimalFormat2(invModel.getIgstTax()), decimalFormat2(invModel.getIgstVal()), decimalFormat2(mFreightAmt + mCGST + mSGST + mIGST)
                , " ", "0", "0");

        if (isBlankRowsAvailable) {
            if (prodList.size() + 1 < 19) {
                for (int i = 0; i < 20 - prodList.size() + 1; i++) {
                    setAllProductInInvoice(table, " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "
                            , " ", " ", " ");
                }
            }
        }
        return table;
    }


    private void setAllProductInInvoice(PdfPTable table, String sNo, String qty, String pName, String batch, String exp, String hsn, String tax
            , String mrp, String rate, String dis, String taxable, String cgstPercent, String cgstVal, String sgstPercent, String sgstVal, String igstPercent, String igstVal, String total
            , String unitName, String cessRate, String cessValue) {
        PdfPCell cell;

//        cell = new PdfPCell(new Phrase(sNo, normalBody));
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(setEntity(sNo));

//        cell = new PdfPCell(new Phrase(pName, normalBody));
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        table.addCell(cell);
        table.addCell(setEntity(pName,Element.ALIGN_LEFT));

//        cell = new PdfPCell(new Phrase(qty, normalBody));
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        table.addCell(cell);
        table.addCell(setEntity(qty));

//        cell = new PdfPCell(new Phrase(unitName == null ? "" : unitName, normalBody));
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        table.addCell(cell);
        table.addCell(setEntity(unitName == null ? "" : unitName));

//        cell = new PdfPCell(new Phrase(validateHSNCode(hsn), normalBody));
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        table.addCell(cell);
        table.addCell(setEntity(validateHSNCode(hsn)));

//        cell = new PdfPCell(new Phrase(tax, normalBody));
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        table.addCell(cell);
        table.addCell(setEntity(tax));

//        cell = new PdfPCell(new Phrase(mrp, normalBody));
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//        table.addCell(cell);
        table.addCell(setEntity(mrp, Element.ALIGN_RIGHT));

//        cell = new PdfPCell(new Phrase(rate, normalBody));
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//        table.addCell(cell);
        table.addCell(setEntity(rate, Element.ALIGN_RIGHT));

//        cell = new PdfPCell(new Phrase(dis == null ? "0" : dis, normalBody));
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        table.addCell(cell);
        table.addCell(setEntity(dis == null ? "0" : dis));

//        cell = new PdfPCell(new Phrase(taxable, normalBody));
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//        table.addCell(cell);
        table.addCell(setEntity(taxable));

//        cell = new PdfPCell(new Phrase(cgstPercent, normalBody));
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        table.addCell(cell);
        table.addCell(setEntity(cgstPercent));

//        cell = new PdfPCell(new Phrase(cgstVal, normalBody));
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//        table.addCell(cell);
        table.addCell(setEntity(cgstVal, Element.ALIGN_RIGHT));

//        cell = new PdfPCell(new Phrase(sgstPercent, normalBody));
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        table.addCell(cell);
        table.addCell(setEntity(sgstPercent));

//        cell = new PdfPCell(new Phrase(sgstVal, normalBody));
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//        table.addCell(cell);
        table.addCell(setEntity(sgstVal, Element.ALIGN_RIGHT));

//        cell = new PdfPCell(new Phrase(igstPercent.equalsIgnoreCase("0") ? "" : igstPercent, normalBody));
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        table.addCell(cell);
        table.addCell(setEntity(igstPercent.equalsIgnoreCase("0") ? "" : igstPercent));

//        cell = new PdfPCell(new Phrase(igstVal.equalsIgnoreCase("0") ? "" : igstVal, normalBody));
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//        table.addCell(cell);
        table.addCell(setEntity(igstVal.equalsIgnoreCase("0") ? "" : igstVal, Element.ALIGN_RIGHT));

        if (TextUtils.isEmpty(cessRate)) {
            cessRate = "0";
        }
//        cell = new PdfPCell(new Phrase(cessRate, normalBody));
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        table.addCell(cell);
        table.addCell(setEntity(cessRate));

//        cell = new PdfPCell(new Phrase(cessValue, normalBody));
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//        table.addCell(cell);
        table.addCell(setEntity(cessValue, Element.ALIGN_RIGHT));

//        cell = new PdfPCell(new Phrase(decimalFormatWith2Digit(total), normalBody));
//        cell.setPaddingTop(PADDING_SMALL);
//        cell.setPaddingBottom(PADDING_SMALL);
//        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//        table.addCell(cell);
        table.addCell(setEntity(decimalFormatWith2Digit(total), Element.ALIGN_RIGHT));


    }

    public static String decimalFormatWith2Digit(float value) {
        return String.format(Locale.US, "%.02f", value);
    }

    public static String decimalFormatWith2Digit(String value) {
        return String.format(Locale.US, "%.02f", parseFloat(value));
    }

    private String calTaxValue(String taxable, String cessRate) {
        float mTaxable = convertToFloat(taxable);
        float mCessRate = convertToFloat(cessRate);
        return new DecimalFormat("#.##").format((mTaxable * mCessRate) / 100);
    }

//    private String calTaxValue(String taxable, String cessRate) {
//        float mTaxable = convertToFloat(taxable);
//        float mCessRate = convertToFloat(cessRate);
//        return new DecimalFormat("#.##").format((mTaxable * mCessRate)/100);
//    }

    private String validateHSNCode(String hsn) {
        try {
            if (hsn.length() <= 4) {
                return hsn;
            } else {
                return hsn.substring(0, 4);
            }
        } catch (IndexOutOfBoundsException e) {
            return hsn;
        }
    }

    private PdfPTable addInvoiceFooterTable(String[] allGST, InvoiceModel invModel, CompanyModel companyDetail
            , Boolean isBankAvailabe, Boolean isIGSTAvailabe, Boolean isIGSTApplicable, String gstHeader) throws DocumentException {
        PdfPTable table = new PdfPTable(10);

        String totalTaxableAmount = totalPrice(invModel.getTotalAmount(), invModel.getFreightAmount());
        String amountAfterTax = totalPrice(totalTaxableAmount, allGST[0]);
        String totalAmount = decimalFormat(amountAfterTax);

//        table.setWidthPercentage(288 / 5.23f);
        int[] tableWidth = new int[]{1, 1, 1, 1, 1, 1, 1, 1, 2, 1};
        if (!isBankAvailabe) {
            tableWidth[6] = 0;
        }
        if (!isIGSTAvailabe) {
            tableWidth[4] = 0;
            tableWidth[6] = 2;
        }
        table.setWidths(tableWidth);
        PdfPCell cell;

//        cell = new PdfPCell(new Phrase("Total Invoice Amount in Words", normalBold));
//        cell.setPadding(PADDING_NORMAL);
//        cell.setColspan(8);
//        table.addCell(cell);
        table.addCell(setEntityBold("Total Invoice Amount in Words",NO_ROWSPAN,8));


//        cell = new PdfPCell(new Phrase("Total Amount BeforeTax", normalBold));
//        cell.setPadding(PADDING_NORMAL);
//        table.addCell(cell);
        table.addCell(setEntityBold("Total Amount BeforeTax"));

//        cell = new PdfPCell(new Phrase(decimalFormatWith2Digit(totalTaxableAmount), normalBold));
//        cell.setPadding(PADDING_NORMAL);
//        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//        table.addCell(cell);
        table.addCell(setEntityBoldGravity(decimalFormatWith2Digit(totalTaxableAmount),Element.ALIGN_RIGHT));

//        cell = new PdfPCell(new Phrase(Words.convertToWord(totalAmount) + " only.", normalBold));
//        cell.setPadding(PADDING_NORMAL);
//        cell.setRowspan(2);
//        cell.setColspan(8);
//        table.addCell(cell);
        table.addCell(setEntityBold(NumberToWordsConverter.convertToWord(totalAmount) + " RUPEES ONLY.",2,8));
//        table.addCell(setEntityBold(Words.convertToWord(totalAmount) + " RUPEES ONLY.",2,8));

//        cell = new PdfPCell(new Phrase("CGST", normalBold));
//        cell.setPadding(PADDING_NORMAL);
//        table.addCell(cell);
        table.addCell(setEntityBold("CGST"));

//        cell = new PdfPCell(new Phrase(decimalFormatWith2Digit(allGST[1]), normalBold));
//        cell.setPadding(PADDING_NORMAL);
//        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//        table.addCell(cell);
        table.addCell(setEntityBoldGravity(decimalFormatWith2Digit(allGST[1]),Element.ALIGN_RIGHT));

//        cell = new PdfPCell(new Phrase(gstHeader, normalBold));
//        cell.setPadding(PADDING_NORMAL);
//        table.addCell(cell);
        table.addCell(setEntityBold(gstHeader));

//        cell = new PdfPCell(new Phrase(decimalFormatWith2Digit(allGST[2]), normalBold));
//        cell.setPadding(PADDING_NORMAL);
//        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//        table.addCell(cell);
        table.addCell(setEntityBoldGravity(decimalFormatWith2Digit(allGST[2]),Element.ALIGN_RIGHT));

        if (isBankAvailabe) {
//            cell = new PdfPCell(new Phrase("Terms & Condition", normalBold));
//            cell.setPadding(PADDING_NORMAL);
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setColspan(4);
//            table.addCell(cell);
            table.addCell(setEntityBold("Terms & Condition",Element.ALIGN_CENTER,NO_ROWSPAN,4));

//            cell = new PdfPCell(new Phrase("Bank Details", normalBold));
//            cell.setPadding(PADDING_NORMAL);
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setColspan(4);
//            table.addCell(cell);
            table.addCell(setEntityBold("Bank Details",Element.ALIGN_CENTER, NO_ROWSPAN, 4));
        } else {
//            cell = new PdfPCell(new Phrase("Terms & Condition", normalBold));
//            cell.setPadding(PADDING_NORMAL);
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setColspan(8);
//            table.addCell(cell);
            table.addCell(setEntityBold("Terms & Condition",Element.ALIGN_CENTER, NO_ROWSPAN, 8));
        }

//        if (isIGSTAvailabe) {
//        cell = new PdfPCell(new Phrase("IGST", normalBold));
//        cell.setPadding(PADDING_NORMAL);
//        table.addCell(cell);
        table.addCell(setEntityBold("IGST"));

//        cell = new PdfPCell(new Phrase(decimalFormatWith2Digit(allGST[3]), normalBold));
//        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//        cell.setPadding(PADDING_NORMAL);
//        table.addCell(cell);
        table.addCell(setEntityBoldGravity(decimalFormatWith2Digit(allGST[3]),Element.ALIGN_RIGHT));

//        }

        if (isBankAvailabe) {
//            cell = new PdfPCell(new Phrase(companyDetail.gettAndC(), normal));
//            cell.setPadding(PADDING_NORMAL);
////            if (isIGSTAvailabe) {
//            cell.setRowspan(6);
////            } else {
////                cell.setRowspan(4);
////            }
//            cell.setColspan(4);
//            table.addCell(cell);
            table.addCell(setEntityBoldFont(normal,companyDetail.gettAndC(),Element.ALIGN_LEFT,6,4));


            String swiftCode;
            if (companyDetail.getSwiftCode().equals("")) {
                swiftCode = "Not Available";
            } else {
                swiftCode = companyDetail.getSwiftCode();
            }
            String bankDetail="Account Name :" + companyDetail.getAccName() + "\n" +
                    "Bank Name :" + companyDetail.getBankName() + "\n" +
                    "Bank Account :" + companyDetail.getAccNumber() + "\n" +
                    "Bank IFSC :" + companyDetail.getIfscCode() + "\n" +
                    "SWIFT Code :" + swiftCode + "\n" +
                    "Bank Branch Address :" + companyDetail.getBranchAddress() + "\n";

//            cell = new PdfPCell(new Phrase(bankDetail, normalBold));
//            cell.setPadding(PADDING_NORMAL);
////            if (isIGSTAvailabe) {
//            cell.setRowspan(6);
////            } else {
////                cell.setRowspan(4);
////            }
//            cell.setColspan(4);
//            table.addCell(cell);

            table.addCell(setEntityBold(bankDetail,6,4));

        } else {
//            cell = new PdfPCell(new Phrase(companyDetail.gettAndC(), normal));
//            cell.setPadding(PADDING_NORMAL);
////            if (isIGSTAvailabe) {
//            cell.setRowspan(6);
////            } else {
////                cell.setRowspan(4);
////            }
//            cell.setColspan(8);
//            table.addCell(cell);
            table.addCell(setEntityBoldFont(normal,companyDetail.gettAndC(),Element.ALIGN_LEFT,6,8));

        }

//        cell = new PdfPCell(new Phrase("Cess", normalBold));
//        cell.setPadding(PADDING_NORMAL);
//        table.addCell(cell);
        table.addCell(setEntityBold("Cess"));

//        cell = new PdfPCell(new Phrase(decimalFormatWith2Digit(allGST[4]), normalBold));
//        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//        cell.setPadding(PADDING_NORMAL);
//        table.addCell(cell);
        table.addCell(setEntityBoldGravity(decimalFormatWith2Digit(allGST[4]),Element.ALIGN_RIGHT));

//        cell = new PdfPCell(new Phrase("Tax Amount", normalBold));
//        cell.setPadding(PADDING_NORMAL);
//        table.addCell(cell);
        table.addCell(setEntityBold("Tax Amount"));

//        cell = new PdfPCell(new Phrase(decimalFormatWith2Digit(allGST[0]), normalBold));
//        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//        cell.setPadding(PADDING_NORMAL);
//        table.addCell(cell);
        table.addCell(setEntityBoldGravity(decimalFormatWith2Digit(allGST[0]),Element.ALIGN_RIGHT));

//        cell = new PdfPCell(new Phrase("Amount After Tax", normalBold));
//        cell.setPadding(PADDING_NORMAL);
//        table.addCell(cell);
        table.addCell(setEntityBold("Amount After Tax"));

//        cell = new PdfPCell(new Phrase(decimalFormatWith2Digit(amountAfterTax), normalBold));
//        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//        cell.setPadding(PADDING_NORMAL);
//        table.addCell(cell);
        table.addCell(setEntityBoldGravity(decimalFormatWith2Digit(amountAfterTax),Element.ALIGN_RIGHT));

//        cell = new PdfPCell(new Phrase("Round Off", normalBold));
//        cell.setPadding(PADDING_NORMAL);
//        table.addCell(cell);
        table.addCell(setEntityBold("Round Off"));

        String roundValue = decimalFormat2(convertToFloat(totalAmount) - convertToFloat(amountAfterTax));

//        cell = new PdfPCell(new Phrase(roundValue, normalBold));
//        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//        cell.setPadding(PADDING_NORMAL);
//        table.addCell(cell);
        table.addCell(setEntityBoldGravity(roundValue,Element.ALIGN_RIGHT));

//        cell = new PdfPCell(new Phrase("Total Amount", normalBold));
//        cell.setPadding(PADDING_NORMAL);
//        table.addCell(cell);
        table.addCell(setEntityBold("Total Amount"));

//        cell = new PdfPCell(new Phrase(decimalFormatWith2Digit(totalAmount), normalBold));
//        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//        cell.setPadding(PADDING_NORMAL);
//        table.addCell(cell);
        table.addCell(setEntityBoldGravity(decimalFormatWith2Digit(totalAmount),Element.ALIGN_RIGHT));

//        cell = new PdfPCell(new Phrase("GST on Reverse Charge", normalBold));
//        cell.setPadding(PADDING_NORMAL);
//        table.addCell(cell);
        table.addCell(setEntityBold("GST on Reverse Charge"));

//        cell = new PdfPCell(new Phrase("No", normalBold));
//        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//        cell.setPadding(PADDING_NORMAL);
//        table.addCell(cell);
        table.addCell(setEntityBoldGravity("No",Element.ALIGN_RIGHT));

//        cell = new PdfPCell(new Phrase("Rate", normalBold));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setPadding(PADDING_NORMAL);
//        table.addCell(cell);
        table.addCell(setEntityBoldGravity("Rate",Element.ALIGN_CENTER));

//        cell = new PdfPCell(new Phrase("Taxable Amount", normalBold));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setPadding(PADDING_NORMAL);
//        table.addCell(cell);
        table.addCell(setEntityBoldGravity("Taxable Amount",Element.ALIGN_CENTER));

//        cell = new PdfPCell(new Phrase("CGST Amount", normalBold));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setPadding(PADDING_NORMAL);
//        table.addCell(cell);
        table.addCell(setEntityBoldGravity("CGST Amount",Element.ALIGN_CENTER));

//        cell = new PdfPCell(new Phrase(gstHeader + " Amount", normalBold));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setPadding(PADDING_NORMAL);
//        table.addCell(cell);
        table.addCell(setEntityBoldGravity(gstHeader + " Amount",Element.ALIGN_CENTER));

        if (isIGSTAvailabe) {
//            cell = new PdfPCell(new Phrase("IGST Amount", normalBold));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setPadding(PADDING_NORMAL);
//            table.addCell(cell);
            table.addCell(setEntityBoldGravity("IGST Amount",Element.ALIGN_CENTER));

//            cell = new PdfPCell(new Phrase("Cess Amount", normalBold));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setPadding(PADDING_NORMAL);
//            table.addCell(cell);
            table.addCell(setEntityBoldGravity("Cess Amount",Element.ALIGN_CENTER));
        } else {
//            cell = new PdfPCell(new Phrase("Cess Amount", normalBold));
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell.setPadding(PADDING_NORMAL);
//            cell.setColspan(2);
//            table.addCell(cell);
            table.addCell(setEntityBoldGravity("Cess Amount",Element.ALIGN_CENTER,NO_ROWSPAN,2));
        }

//        cell = new PdfPCell(new Phrase("Total", normalBold));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setPadding(PADDING_NORMAL);
//        table.addCell(cell);
        table.addCell(setEntityBoldGravity("Total",Element.ALIGN_CENTER));

//        cell = new PdfPCell(new Phrase("Ceritified that the particulars given above are true and correct", normal));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setPadding(PADDING_NORMAL);
//        cell.setColspan(3);
//        table.addCell(cell);
        table.addCell(setEntityBoldGravity("Ceritified that the particulars given above are true and correct",Element.ALIGN_CENTER,NO_ROWSPAN,3));

        float[] GSTZero = {0, 0, 0, 0, 0, 0};
        float[] GSTThree = {0, 0, 0, 0, 0, 0};
        float[] GSTFive = {0, 0, 0, 0, 0, 0};
        float[] GSTTwelve = {0, 0, 0, 0, 0, 0};
        float[] GSTEighteen = {0, 0, 0, 0, 0, 0};
        float[] GSTTwoEight = {0, 0, 0, 0, 0, 0};
        for (ProductModel model : invModel.getProductList()) {
            float taxRate = parseFloat(model.getTaxRate());
            String totalProductsAmt = getTotalProductPrice(model.getQuantity(), model.getSalePrice(), model.getDis());
            float salePrice = convertToFloat(totalProductsAmt);
            float cgst = convertToFloat(model.getCgstVal());
            float sgst = convertToFloat(model.getSgstVal());
            float igst = convertToFloat(model.getIgstVal());
            float cessValue = (salePrice * convertToFloat(model.getCessRate())) / 100;
            if (taxRate == 0) {
                GSTZero[0] += salePrice;
                GSTZero[4] += cessValue;
                GSTZero[5] += salePrice + cessValue;
            }
            if (taxRate == 3) {
                GSTThree[0] += salePrice;
                GSTThree[1] += cgst;
                GSTThree[2] += sgst;
                GSTThree[3] += igst;
                GSTThree[4] += cessValue;
                GSTThree[5] += salePrice + cgst + sgst + igst + cessValue;
            }
            if (taxRate == 5) {
                GSTFive[0] += salePrice;
                GSTFive[1] += cgst;
                GSTFive[2] += sgst;
                GSTFive[3] += igst;
                GSTFive[4] += cessValue;
                GSTFive[5] += salePrice + cgst + sgst + igst + cessValue;
            }
            if (taxRate == 12) {
                GSTTwelve[0] += salePrice;
                GSTTwelve[1] += cgst;
                GSTTwelve[2] += sgst;
                GSTTwelve[3] += igst;
                GSTTwelve[4] += cessValue;
                GSTTwelve[5] += salePrice + cgst + sgst + igst + cessValue;
            }
            if (taxRate == 18) {
                GSTEighteen[0] += salePrice;
                GSTEighteen[1] += cgst;
                GSTEighteen[2] += sgst;
                GSTEighteen[3] += igst;
                GSTEighteen[4] += cessValue;
                GSTEighteen[5] += salePrice + cgst + sgst + igst + cessValue;
            }
            if (taxRate == 28) {
                GSTTwoEight[0] += salePrice;
                GSTTwoEight[1] += cgst;
                GSTTwoEight[2] += sgst;
                GSTTwoEight[3] += igst;
                GSTTwoEight[4] += cessValue;
                GSTTwoEight[5] += salePrice + cgst + sgst + igst + cessValue;
            }

        }

        float freightAmt = parseFloat(invModel.getFreightAmount());
        float cgst = parseFloat(invModel.getCgstVal());
        float sgst = parseFloat(invModel.getSgstVal());
        float igst = parseFloat(invModel.getIgstVal());
        if (parseFloat(invModel.getFreightTaxRate()) == 0) {
            GSTZero[0] += freightAmt;
            GSTZero[5] += freightAmt;
        } else if (parseFloat(invModel.getFreightTaxRate()) == 3) {
            GSTThree[0] += freightAmt;
            GSTThree[1] += cgst;
            GSTThree[2] += sgst;
            GSTThree[3] += igst;
            GSTThree[5] += freightAmt + cgst + sgst + igst;
        } else if (parseFloat(invModel.getFreightTaxRate()) == 5) {
            GSTFive[0] += freightAmt;
            GSTFive[1] += cgst;
            GSTFive[2] += sgst;
            GSTFive[3] += igst;
            GSTFive[5] += freightAmt + cgst + sgst + igst;
        } else if (parseFloat(invModel.getFreightTaxRate()) == 12) {
            GSTTwelve[0] += freightAmt;
            GSTTwelve[1] += cgst;
            GSTTwelve[2] += sgst;
            GSTTwelve[3] += igst;
            GSTTwelve[5] += freightAmt + cgst + sgst + igst;
        } else if (parseFloat(invModel.getFreightTaxRate()) == 18) {
            GSTEighteen[0] += freightAmt;
            GSTEighteen[1] += cgst;
            GSTEighteen[2] += sgst;
            GSTEighteen[3] += igst;
            GSTEighteen[5] += freightAmt + cgst + sgst + igst;
        } else if (parseFloat(invModel.getFreightTaxRate()) == 28) {
            GSTTwoEight[0] += freightAmt;
            GSTTwoEight[1] += cgst;
            GSTTwoEight[2] += sgst;
            GSTTwoEight[3] += igst;
            GSTTwoEight[5] += freightAmt + cgst + sgst + igst;
        }
        setAllGST(isIGSTAvailabe, table, "GST-00", GSTZero[0], GSTZero[1], GSTZero[2], GSTZero[3], GSTZero[4], GSTZero[5], true);
        setAllGST(isIGSTAvailabe, table, "GST-03", GSTThree[0], GSTThree[1], GSTThree[2], GSTThree[3], GSTThree[4], GSTThree[5], false);
        setAllGST(isIGSTAvailabe, table, "GST-05", GSTFive[0], GSTFive[1], GSTFive[2], GSTFive[3], GSTFive[4], GSTFive[5], false);
        setAllGST(isIGSTAvailabe, table, "GST-12", GSTTwelve[0], GSTTwelve[1], GSTTwelve[2], GSTTwelve[3], GSTTwelve[4], GSTTwelve[5], false);
        setAllGST(isIGSTAvailabe, table, "GST-18", GSTEighteen[0], GSTEighteen[1], GSTEighteen[2], GSTEighteen[3], GSTEighteen[4], GSTEighteen[5], false);
        setAllGST(isIGSTAvailabe, table, "GST-28", GSTTwoEight[0], GSTTwoEight[1], GSTTwoEight[2], GSTTwoEight[3], GSTTwoEight[4], GSTTwoEight[5], false);

        cell = new PdfPCell();
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        cell = new PdfPCell();
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        cell = new PdfPCell();
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        cell = new PdfPCell();
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        if (isBankAvailabe && !isIGSTAvailabe) {
            cell = new PdfPCell();
            cell.setPadding(PADDING_NORMAL);
            cell.setColspan(2);
            table.addCell(cell);
        } else {
            cell = new PdfPCell();
            cell.setPadding(PADDING_NORMAL);
            table.addCell(cell);

            cell = new PdfPCell();
            cell.setPadding(PADDING_NORMAL);
            table.addCell(cell);
        }

        cell = new PdfPCell();
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

//        cell = new PdfPCell(new Phrase("Authorised signatory", normalBold));
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setPadding(PADDING_NORMAL);
//        cell.setColspan(3);
//        table.addCell(cell);
        table.addCell(setEntityBoldGravity("Authorised signatory",Element.ALIGN_CENTER,NO_ROWSPAN,3));


        return table;
    }

    private String[] calculateProductGST(ArrayList<ProductModel> productList, InvoiceModel invoiceModel) {
//        String[] allGst = new String[5];
        float totalTax, totalCGST = 0, totalSGST = 0, totalIGST = 0, totalCessVal = 0;
        for (ProductModel mod : productList) {
            float cgst = convertToFloat(mod.getCgstVal());
            float sgst = convertToFloat(mod.getSgstVal());
            float igst = convertToFloat(mod.getIgstVal());
            totalCGST += cgst;
            totalSGST += sgst;
            totalIGST += igst;
            String totalProductsAmt = getTotalProductPrice(mod.getQuantity(), mod.getSalePrice(), mod.getDis());
            float mTaxable = convertToFloat(totalProductsAmt);
            float mCessRate = convertToFloat(mod.getCessRate());
            float mCessValue = (mTaxable * mCessRate) / 100;
            totalCessVal += mCessValue;
        }
        float freightCgst = convertToFloat(invoiceModel.getCgstVal());
        float freightSgst = convertToFloat(invoiceModel.getSgstVal());
        float freightIgst = convertToFloat(invoiceModel.getIgstVal());

        totalTax = totalCGST + totalSGST + totalIGST + freightCgst + freightSgst + freightIgst + totalCessVal;
//        allGst[0] = new DecimalFormat("#.##").format(totalTax);
//        allGst[1] = new DecimalFormat("#.##").format(totalCGST + freightCgst);
//        allGst[2] = new DecimalFormat("#.##").format(totalSGST + freightSgst);
//        allGst[3] = new DecimalFormat("#.##").format(totalIGST + freightIgst);
//        allGst[4] = new DecimalFormat("#.##").format(totalCessVal);
        return new String[]{decimalFormat2(totalTax), decimalFormat2(totalCGST + freightCgst)
                , decimalFormat2(totalSGST + freightSgst), decimalFormat2(totalIGST + freightIgst)
                , decimalFormat2(totalCessVal)};
    }

    public static int parseInt(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return 0;
        } catch (NullPointerException e) {
            return 0;
        }
    }

    public static float parseFloat(String value) {
        try {
            return Float.parseFloat(value);
        } catch (NumberFormatException e) {
            return 0;
        } catch (NullPointerException e) {
            return 0;
        }
    }

    private void setAllGST(Boolean isIGSTAvailabe, PdfPTable table, String rate, float taxableAmt, float cgst, float sgst, float igst, float cessAmount, float total, boolean isSignature) {
        PdfPCell cell;
        cell = new PdfPCell(new Phrase(rate, normalBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPadding(PADDING_SMALL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(String.valueOf(new DecimalFormat("#.##").format(taxableAmt)), normalBold));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setPadding(PADDING_SMALL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(String.valueOf(new DecimalFormat("#.##").format(cgst)), normalBold));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setPadding(PADDING_SMALL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(String.valueOf(new DecimalFormat("#.##").format(sgst)), normalBold));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setPadding(PADDING_SMALL);
        table.addCell(cell);

        if (isIGSTAvailabe) {
            cell = new PdfPCell(new Phrase(new DecimalFormat("#.##").format(igst), normalBold));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setPadding(PADDING_SMALL);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(new DecimalFormat("#.##").format(cessAmount), normalBold));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setPadding(PADDING_SMALL);
            table.addCell(cell);
        } else {
            cell = new PdfPCell(new Phrase(new DecimalFormat("#.##").format(cessAmount), normalBold));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setPadding(PADDING_SMALL);
            cell.setColspan(2);
            table.addCell(cell);
        }

        cell = new PdfPCell(new Phrase(new DecimalFormat("#.##").format(total), normalBold));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setPadding(PADDING_SMALL);
        table.addCell(cell);

        if (isSignature) {
            String signatureUrl= AppUser.getSignature(mContext);
            if(TextUtils.isEmpty(signatureUrl)){
                cell = new PdfPCell(new Phrase("This is system generated invoice hence no signature required", normalBody));
            }else{
                cell = createImageCell(signatureUrl);
                table.setWidthPercentage(100);
                if (cell == null) {
                    cell = new PdfPCell(new Phrase("This is system generated invoice hence no signature required", normalBody));
                }
            }
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setPadding(PADDING_SMALL);
            cell.setColspan(3);
            cell.setRowspan(6);
            table.addCell(cell);

        }
    }


    private PdfPTable addInvoiceHearderTable(InvoiceModel invModel, CompanyModel companyDetail, Boolean isVehicelAvailabe, Boolean isDLNoAvailabe, String selectedPrintType) throws DocumentException {
        PdfPTable table = new PdfPTable(7);
//        table.setWidthPercentage(288 / 5.23f);
        int[] tableWidths = new int[]{20, 14, 14, 14, 14, 16, 8};
        table.setWidths(tableWidths);
        PdfPCell cell;
        Phrase phrase;
        phrase = new Phrase();
        phrase.add(new Chunk(companyDetail.getCompanyName(), catFontBold));
        phrase.add(new Chunk("\n\n", small));
        String address = "";
        if (!companyDetail.getAddress().equals("")) {
            address = companyDetail.getAddress();
        }
        if (!companyDetail.getState().equals("")) {
            address = address + ", " + companyDetail.getState();
        }

        if (isDLNoAvailabe && !companyDetail.getDlNumber().equals("")) {
            phrase.add(new Chunk(address + "\n" + companyDetail.getTelephone() + "\nGSTIN:" + companyDetail.getGstin() + "\nD.L.No." + companyDetail.getDlNumber(), normal));
        } else {
            phrase.add(new Chunk(address + "\n" + companyDetail.getTelephone() + "\nGSTIN:" + companyDetail.getGstin(), normal));
        }
        cell = new PdfPCell(phrase);
        cell.setPadding(PADDING_NORMAL);
        cell.setVerticalAlignment(Element.ALIGN_CENTER);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        if (isVehicelAvailabe) {
            cell.setRowspan(4);
        } else {
            cell.setRowspan(3);
        }
        table.addCell(cell);

        phrase = new Phrase();
        phrase.add(new Chunk("GST Invoice (" + selectedPrintType + ")", catFont));
        phrase.add(new Chunk("\nTax Invoice", normal));

        cell = new PdfPCell(phrase);
        cell.setVerticalAlignment(Element.ALIGN_CENTER);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setNoWrap(false);
        cell.setMinimumHeight(50);
        cell.setPadding(PADDING_NORMAL);
        cell.setColspan(4);
        table.addCell(cell);

        phrase = new Phrase();
        phrase.add(new Chunk("BILL TO PARTY", normalBold));
        phrase.add(new Chunk("\n"));
        if (isDLNoAvailabe && !invModel.getCustomerDLNumber().equals("")) {
            phrase.add(new Chunk(invModel.getCustomerName() + "\n" + invModel.getCustomerGSTIN() + "\n" + invModel.getCustomerState() + "\n" + invModel.getCustomerAddress() + "\n" + invModel.getCustomerDLNumber(), normal));
        } else {
            phrase.add(new Chunk(invModel.getCustomerName() + "\n" + invModel.getCustomerGSTIN() + "\n" + invModel.getCustomerState() + "\n" + invModel.getCustomerAddress(), normal));
        }
        cell = new PdfPCell(phrase);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPadding(PADDING_NORMAL);
        cell.setColspan(2);
        table.addCell(cell);


//        cell = new PdfPCell(new Phrase(invModel.getCustomerGSTIN() + "\n" + invModel.getCustomerState(), normal));
//        cell.setPadding(PADDING_NORMAL);
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        table.addCell(cell);

        cell = new PdfPCell(new Phrase(new Phrase("Invoice No.", normal)));
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(new Phrase(invModel.getInvoiceNumber(), normal)));
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(new Phrase("Invoice Date", normal)));
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(new Phrase(invModel.getInvoiceDate(), normal)));
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);


        phrase = new Phrase();
        phrase.add(new Chunk("SHIP TO PARTY", normalBold));
        phrase.add(new Chunk("\n"));
        phrase.add(new Chunk(invModel.getShipName() + "\n" + invModel.getShipGSTIN() + "\n" + invModel.getPlaceofSupply() + "\n" + invModel.getShipAddress() + "\n" + invModel.getVehicleNumber(), normal));
        cell = new PdfPCell(phrase);
        cell.setPadding(PADDING_NORMAL);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        if (isVehicelAvailabe) {
            cell.setRowspan(3);
        } else {
            cell.setRowspan(2);
        }
        cell.setColspan(2);
        table.addCell(cell);

//        cell = new PdfPCell(new Phrase(invModel.getShipGSTIN() + "\n" + invModel.getPlaceofSupply(), normal));
//        if (isVehicelAvailabe) {
//            cell.setRowspan(3);
//        } else {
//            cell.setRowspan(2);
//        }
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        table.addCell(cell);

        if (isVehicelAvailabe) {
            cell = new PdfPCell(new Phrase("Transport Mode", normal));
            cell.setPadding(PADDING_NORMAL);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(new Phrase(invModel.getTranspoartMode(), normal)));
            cell.setPadding(PADDING_NORMAL);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(new Phrase("Vehicle No.", normal)));
            cell.setPadding(PADDING_NORMAL);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(new Phrase(invModel.getVehicleNumber(), normal)));
            cell.setPadding(PADDING_NORMAL);
            table.addCell(cell);
        }

        cell = new PdfPCell(new Phrase(new Phrase("Date Of Supply", normal)));
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(new Phrase(invModel.getDateOfSupply(), normal)));
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(new Phrase("Place Of Supply", normal)));
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(new Phrase(invModel.getPlaceofSupply(), normal)));
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        return table;
    }


    private Chunk setCurrency(String currency) {
        Chunk chunk;
        if (currency != null) {
            switch (currency) {
                case "INR":
                    chunk = new Chunk(" (\u20B9)", currencyFont);
                    break;
                case "USD":
                    chunk = new Chunk(" (\u0024)", currencyFont);
                    break;
                case "EUR":
                    chunk = new Chunk(" (\u20AC)", currencyFont);
                    break;
                default:
                    chunk = new Chunk();
                    break;
            }
            return chunk;
        } else {
            return new Chunk();
        }
    }

    private String getTotalProductPrice(String quantity, String salePrice, String discount) {
        float qun = convertToFloat(quantity);
        float price = convertToFloat(salePrice);
        float dis = convertToFloat(discount);
        return new DecimalFormat("#.##").format((qun * price) - dis);
    }

    private String totalPrice(String salePrice, String totalTaxAmount) {
        float saleAmt = convertToFloat(salePrice);
        float taxAmt = convertToFloat(totalTaxAmount);
        return new DecimalFormat("#.##").format(saleAmt + taxAmt);
    }

    private String decimalFormat(String value) {
        return new DecimalFormat("#").format(convertToFloat(value));
    }

    private String decimalFormat2(float value) {
        String ures = new DecimalFormat("#.##").format(value);
        return ures;
    }

    private String decimalFormat2(String value) {
        String ures = new DecimalFormat("#.##").format(convertToFloat(value));
        return ures;
    }

    private String totalPrice(String salePrice, String frieghtAmount, String frieghtTaxAmount, String totalTaxAmount) {
        float saleAmt = convertToFloat(salePrice);
        float frieghtAmt = convertToFloat(frieghtAmount);
        float frightTaxAmt = convertToFloat(frieghtTaxAmount);
        float totalTaxAmt = convertToFloat(totalTaxAmount);
        return new DecimalFormat("#.##").format(saleAmt + frieghtAmt + frightTaxAmt + totalTaxAmt);
    }

    private float convertToFloat(String value) {
        try {
            return Float.parseFloat(value);
        } catch (NumberFormatException e) {
            return 0;
        } catch (NullPointerException e) {
            return 0;
        }
    }

    private PdfPCell setDividerDotted(int colSpan, int gravity) {
        PdfPCell cell = new PdfPCell();
        cell.setBorder(PdfPCell.NO_BORDER);
        cell.setCellEvent(new DottedCell(gravity));
        cell.setColspan(colSpan);
        return cell;
    }

    private PdfPCell setHeader(String title) {
        return setHeader(title, null, false, Element.ALIGN_CENTER, NO_ROWSPAN, NO_COLSPAN);
    }

    private PdfPCell setHeader(String title, int gravity) {
        return setHeader(title, null, false, gravity, NO_ROWSPAN, NO_COLSPAN);
    }

    private PdfPCell setHeader(String title, int gravity, int rowSpan) {
        return setHeader(title, null, false, gravity, rowSpan, NO_COLSPAN);
    }

    private PdfPCell setHeader(String title, int gravity, int rowSpan, int colSpan) {
        return setHeader(title, null, false, gravity, rowSpan, colSpan);
    }

    private PdfPCell setHeader(Phrase phrase) {
        return setHeader(null, phrase, false, Element.ALIGN_CENTER, NO_ROWSPAN, NO_COLSPAN);
    }

    private PdfPCell setHeader(Phrase phrase, int gravity) {
        return setHeader(null, phrase, false, gravity, NO_ROWSPAN, NO_COLSPAN);
    }

    private PdfPCell setHeader(Phrase phrase, int gravity, int rowSpan) {
        return setHeader(null, phrase, false, gravity, rowSpan, NO_COLSPAN);
    }

    private PdfPCell setHeader(Phrase phrase, int gravity, int rowSpan, int colSpan) {
        return setHeader(null, phrase, false, gravity, rowSpan, colSpan);
    }

    private PdfPCell setHeader(String title, Phrase phrase, Boolean isNoBorder, int gravity, int rowSpan, int colSpan) {
        PdfPCell cell = null;
        if (title != null) {
            cell = new PdfPCell(new Phrase(title, normalBodyBold));
        }
        if (phrase != null) {
            cell = new PdfPCell(phrase);
        }
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        cell.setHorizontalAlignment(gravity);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_CENTER);
        if(rowSpan!=NO_ROWSPAN) {
            cell.setRowspan(rowSpan);
        }
        if(colSpan!=NO_COLSPAN) {
            cell.setColspan(colSpan);
        }
        if (isNoBorder) {
            cell.setBorder(Rectangle.NO_BORDER);
        }
//        cell.setBackgroundColor (new BaseColor (140, 221, 8));
        return cell;
    }


    private PdfPCell setEntity(String title) {
        return setEntity(title, null, false, Element.ALIGN_CENTER);
    }

    private PdfPCell setEntity(String title, int gravity) {
        return setEntity(title, null, false, gravity);
    }

    private PdfPCell setEntity(Phrase phrase, int gravity) {
        return setEntity(null, phrase, false, gravity);
    }

    private PdfPCell setEntity(String title, Phrase phrase, Boolean isNoBorder, int gravity) {
        PdfPCell cell = null;
        if (title != null) {
            cell = new PdfPCell(new Phrase(title, normalBody));
        }
        if (phrase != null) {
            cell = new PdfPCell(phrase);
        }
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        cell.setHorizontalAlignment(gravity);
        cell.setVerticalAlignment(Element.ALIGN_CENTER);
        if (isNoBorder) {
            cell.setBorder(Rectangle.NO_BORDER);
        }
//        cell.setBackgroundColor (new BaseColor (140, 221, 8));
        return cell;
    }


    private PdfPCell setEntityBold(String title) {
        return setEntityBold(normalBold, title, null, false, Element.ALIGN_LEFT, NO_ROWSPAN,NO_COLSPAN);
    }
    private PdfPCell setEntityBold(String title, int rowSpan) {
        return setEntityBold(normalBold, title, null, false, Element.ALIGN_LEFT, rowSpan,NO_COLSPAN);
    }
    private PdfPCell setEntityBoldGravity(String title, int gravity) {
        return setEntityBold(normalBold, title, null, false, gravity, NO_ROWSPAN, NO_COLSPAN);
    }
    private PdfPCell setEntityBoldGravity(String title, int gravity,int rowSpan, int colSpan) {
        return setEntityBold(normalBold, title, null, false, gravity, rowSpan, colSpan);
    }
    private PdfPCell setEntityBold(String title, int rowSpan, int colSpan) {
        return setEntityBold(normalBold, title, null, false, Element.ALIGN_LEFT, rowSpan,colSpan);
    }
    private PdfPCell setEntityBold(Phrase phrase, int rowSpan) {
        return setEntityBold(normalBold, null, phrase, false, Element.ALIGN_LEFT, rowSpan,NO_COLSPAN);
    }
    private PdfPCell setEntityBold(String title, int gravity,int rowSpan, int colSpan) {
        return setEntityBold(normalBold, title, null, false, gravity, rowSpan, colSpan);
    }
    private PdfPCell setEntityBoldFont(Font font, String title, int gravity,int rowSpan, int colSpan) {
        return setEntityBold(font, title, null, false, gravity, rowSpan, colSpan);
    }
    private PdfPCell setEntityBold(Font font,String title, Phrase phrase, Boolean isNoBorder, int gravity,int rowSpan, int colSpan) {
        PdfPCell cell = null;
        if (title != null) {
            cell = new PdfPCell(new Phrase(title, font));
        }
        if (phrase != null) {
            cell = new PdfPCell(phrase);
        }
        cell.setPadding(PADDING_NORMAL);
        cell.setHorizontalAlignment(gravity);
        cell.setVerticalAlignment(Element.ALIGN_LEFT);
        if(rowSpan!=NO_ROWSPAN) {
            cell.setRowspan(rowSpan);
        }
        if(colSpan!=NO_COLSPAN) {
            cell.setColspan(colSpan);
        }
        if (isNoBorder) {
            cell.setBorder(Rectangle.NO_BORDER);
        }
//        cell.setBackgroundColor (new BaseColor (140, 221, 8));
        return cell;
    }

    private PdfPCell createImageCell(String path) {
        Image img = null;
        PdfPCell cell = null;
        try {
            img = Image.getInstance(path);
//            img.scaleAbsolute(826, 1100);
            img.setScaleToFitLineWhenOverflow(false);
            cell = new PdfPCell(img, true);
            cell.setPadding(PADDING_SMALL);
            cell.setColspan(3);
            cell.setRowspan(5);
        } catch (BadElementException | IOException e) {
            e.printStackTrace();
        }
        return cell;
    }

    private static PdfPCell createCompanyLogoCell(String path, Boolean isVehicelAvailabe) {
        Image img = null;
        PdfPCell cell = null;
        try {
            img = Image.getInstance(path);
            img.scaleAbsolute(50, 50);
//            img.setScaleToFitLineWhenOverflow(false);
            cell = new PdfPCell(img, true);//true= fit image on cell
            cell.setPadding(PADDING_LOGO);
            cell.setVerticalAlignment(Element.ALIGN_CENTER);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            if (isVehicelAvailabe) {
                cell.setRowspan(4);
            } else {
                cell.setRowspan(3);
            }
        } catch (BadElementException | IOException e) {
            e.printStackTrace();
        }
        return cell;
    }


    public File generateVoucherPDF(VoucherModel inv, CompanyModel companyDetail, CustomerModel customerDetail) throws IOException, DocumentException {
        return null;
    }
}
