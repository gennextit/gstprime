package com.gennext.offlinegst.user.services.purchase;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.PopupAlert;
import com.gennext.offlinegst.model.app.Model;
import com.gennext.offlinegst.model.user.PurchasesModel;
import com.gennext.offlinegst.model.user.ServiceModel;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.ApiCallError;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.ProgressButtonRounded;
import com.gennext.offlinegst.util.RequestBuilder;
import com.gennext.offlinegst.util.Utility;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by Admin on 7/4/2017.
 */

public class PaymentServicePurchase extends CompactFragment implements ApiCallError.ErrorParamListener {

    private EditText etCurrentSale,etDiscount, etTotal, etFreight,etFreightValue,etTaxAmount,etBeforeTax;
    private static final int TYPE_RUPEE = 0,TYPE_PERCENT=1;
    private static final String REVERCE_CHARGE = "n";
    private String[] purchaseDetail;
    private ArrayList<ServiceModel> serviceList;
    private ProgressButtonRounded btnAction;

    private AssignTask assignTask;
    private int sellingType;
    private PurchasesModel itemPurchases;
    private ServicesPurchase servicePurchases;
    private CoordinatorLayout coardLayout;
    private int mDiscountType;
    private String mFreightMaxTaxRate;
    private TextView tvFreightTaxRate;
    private String mProductTaxableAmount;
    private String taxableAmountOfFreightAndProduct;
    private String mCustomerStateCode;
    private float mDiscountValue;
    private RadioButton rbRupee;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static Fragment newInstance(ServicesPurchase servicePurchases, int sellingType, PurchasesModel itemPurchases, ArrayList<String> deletedProduct, String[] purchaseDetail,String vendorStateCode, ArrayList<ServiceModel> serviceList) {
        PaymentServicePurchase fragment = new PaymentServicePurchase();
        AppAnimation.setSlideAnimation(fragment, Gravity.BOTTOM);
        fragment.servicePurchases = servicePurchases;
        fragment.sellingType = sellingType;
        fragment.itemPurchases = itemPurchases;
//        fragment.deletedProduct = deletedProduct;
        fragment.purchaseDetail = purchaseDetail;
        fragment.serviceList = serviceList;
        fragment.mCustomerStateCode=vendorStateCode;
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_payment_service_details, container, false);
        initToolBar(getActivity(), v, getString(R.string.payment_details));
        screenAnalytics(getContext(), AppUser.getUserId(getContext()), "Payment Details");
        initUi(v);
        updateUi();
        return v;
    }

    // (+ amount)=Borrow amount
    // (- amount)=Advance amount
    private void updateUi() {
        mProductTaxableAmount = purchaseDetail[5];
        Float[] allProdusts = calculateProductGST(serviceList);
        float allProdPrice = allProdusts[0];
        float allProdPriceIncTax = allProdusts[1];
        float allProdTaxValue = allProdusts[2];
        String mAllProdPrice = validateFloat(allProdPrice);
        String mAllProdPriceIncTax = validateFloat(allProdPriceIncTax);
        String mAllProdTaxValue = validateFloat(allProdTaxValue);

        etBeforeTax.setText(mAllProdPrice);
        etCurrentSale.setText(mAllProdPriceIncTax);
        etTaxAmount.setText(mAllProdTaxValue);

        if (sellingType == ServicePurchaseManager.EDIT_PURCHASE) {
            etFreight.setText(itemPurchases.getFreightAmount());
            rbRupee.setChecked(true);
            etDiscount.setText(itemPurchases.getDiscount());
        } else if (sellingType == ServicePurchaseManager.COPY_PURCHASE) {
            rbRupee.setChecked(true);
            etDiscount.setText(itemPurchases.getDiscount());
        }
        if(sellingType==ServicePurchaseManager.VIEW_PURCHASE){
            etFreight.setText(itemPurchases.getFreightAmount());
            disableEditFields();
        }

        mFreightMaxTaxRate = String.valueOf(getMaxTaxRate());// freight max apply tax rate
        tvFreightTaxRate.setText(getString(R.string.freight_tax)+mFreightMaxTaxRate+"%");

        setFreightValue(parseFloat(etFreight.getText().toString()),parseFloat(mFreightMaxTaxRate));


    }

    private void disableEditFields() {
        btnAction.setText("Close");
        disableEditText(etFreight);
    }


    private void initUi(View v) {
        coardLayout = (CoordinatorLayout) v.findViewById(R.id.coordinatorLayout);
        btnAction = ProgressButtonRounded.newInstance(getContext(), v);
        etBeforeTax = (EditText) v.findViewById(R.id.et_payment_before_tax);
        etTaxAmount = (EditText) v.findViewById(R.id.et_payment_tax_amount);
        etCurrentSale = (EditText) v.findViewById(R.id.et_payment_current_sale);
        etDiscount = (EditText) v.findViewById(R.id.et_product_discount);
        etFreight = (EditText) v.findViewById(R.id.et_payment_freight);
        etFreightValue = (EditText) v.findViewById(R.id.et_payment_freight_tax_value);
        tvFreightTaxRate = (TextView) v.findViewById(R.id.tv_freight_tax_rate);
        etTotal = (EditText) v.findViewById(R.id.et_payment_total);
        etFreight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setFreightValue(parseFloat(etFreight.getText().toString()),parseFloat(mFreightMaxTaxRate));
            }
        });
        etDiscount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setTotalAmount(parseFloat(etDiscount.getText().toString()),parseFloat(etCurrentSale.getText().toString()),parseFloat(etFreight.getText().toString()),parseFloat(etFreightValue.getText().toString()));
            }
        });
        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(!FieldValidation.isEmpty(getContext(),etAccNumber)){
//                    return;
//                }
                if(btnAction.getText().equals("Close")){
                    getFragmentManager().popBackStack("servicePurchaseManager", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    return;
                }
                taxableAmountOfFreightAndProduct=calculateTotalTaxableAmount(parseFloat(etFreight.getText().toString()),parseFloat(etFreightValue.getText().toString()),parseFloat(mProductTaxableAmount));
                hideKeybord(getActivity());
                paymentAcceptedTask(purchaseDetail, serviceList,taxableAmountOfFreightAndProduct,etTotal.getText().toString(),String.valueOf(mDiscountValue),String.valueOf(mFreightMaxTaxRate),etFreight.getText().toString(),etFreightValue.getText().toString());
            }
        });
        RadioGroup rgPaymentType = (RadioGroup) v.findViewById(R.id.rg_discount_type);
        rbRupee = (RadioButton) v.findViewById(R.id.type_rupee);
        mDiscountType=TYPE_PERCENT;
        rgPaymentType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.type_percent:
                        mDiscountType=TYPE_PERCENT;
                        setTotalAmount(parseFloat(etDiscount.getText().toString()),parseFloat(etCurrentSale.getText().toString()),parseFloat(etFreight.getText().toString()),parseFloat(etFreightValue.getText().toString()));
                        break;
                    case R.id.type_rupee:
                        mDiscountType=TYPE_RUPEE;
                        setTotalAmount(parseFloat(etDiscount.getText().toString()),parseFloat(etCurrentSale.getText().toString()),parseFloat(etFreight.getText().toString()),parseFloat(etFreightValue.getText().toString()));
                        break;
                }
            }
        });
    }

    private void setFreightValue(float freight, float taxRate) {
        float afterDiscount = (freight * taxRate) / 100;
        String freightDis = Utility.decimalFormat(afterDiscount, "#.##");
        etFreightValue.setText(freightDis);
        setTotalAmount(parseFloat(etDiscount.getText().toString()),parseFloat(etCurrentSale.getText().toString()),parseFloat(etFreight.getText().toString()),parseFloat(etFreightValue.getText().toString()));
    }

    private void setTotalAmount(float discount, float currentSale, float freight, float freightValue) {
        if(mDiscountType==TYPE_RUPEE) {
            mDiscountValue=discount;
            float afterDiscount = (currentSale-discount)+(freight+freightValue);
            etTotal.setText(Utility.decimalFormat(afterDiscount, "#.##"));
        }else{
            float afterDiscount = (currentSale * discount) / 100;
            mDiscountValue=afterDiscount;
            etTotal.setText(Utility.decimalFormat((currentSale - afterDiscount)+(freight+freightValue), "#.##"));
        }
    }

    private String calculateTotalTaxableAmount(float freight,float freightAmount, float productAmount) {
        return validateFloat((freight+freightAmount) + productAmount);
    }


    private Float[] calculateProductGST(ArrayList<ServiceModel> serviceList) {
        float allProdPrice = 0;
        float allProdPriceIncTax = 0;
        float allProdTaxValue = 0;
        for (ServiceModel model : serviceList) {
            float prodPrice = convertToFloat(model.getSalePrice());
            float productTaxValue = calTax(String.valueOf(prodPrice), model.getTaxRate());
            allProdPriceIncTax += (prodPrice + productTaxValue);
            allProdPrice += prodPrice;
            allProdTaxValue += productTaxValue;
        }
        return new Float[]{allProdPrice,allProdPriceIncTax,allProdTaxValue};
    }

    private float calTax(String freightAmount, String taxRate) {
        float maxTaxRate = convertToFloat(taxRate);
        float totalAmt = convertToFloat(freightAmount);
        return (totalAmt * maxTaxRate) / 100;
    }


    private float convertToFloat(String value) {
        try {
            return Float.parseFloat(value);
        } catch (NumberFormatException e) {
            return 0;
        } catch (NullPointerException e) {
            return 0;
        }
    }

    private String validateFloat(float value) {
        return new DecimalFormat("#.##").format(value);
    }

    public void paymentAcceptedTask(String[] param, ArrayList<ServiceModel> serviceList, String freightAndProductTxableAmt, String totalIncAllTax, String discountValue, String freightMaxTaxRate, String freightAmt, String freightIncTaxValue) {
        hideKeybord(getActivity());
        executeTask(param[0], param[1], param[2], param[3], param[4], param[5], param[6], param[7], param[8], param[9], param[10], serviceList,freightAndProductTxableAmt, totalIncAllTax, discountValue, freightMaxTaxRate, freightAmt, freightIncTaxValue);
    }


    private void executeTask(String invoiceNumber, String invoiceDate, String vendorId, String vendorName, String placeOfPurchase, String totalPrice, String selectedPlaceId, String receiveDate, String invoicePurchaseTypeId,String purchaseCategoryId ,String mentionedGST, ArrayList<ServiceModel> serviceList, String freightAndProductTxableAmt, String totalIncAllTax, String discountValue, String freightMaxTaxRate, String freightAmt, String freightIncTaxValue) {
        hideKeybord(getActivity());
        assignTask = new AssignTask(getActivity(),invoiceNumber, invoiceDate, vendorId, vendorName, placeOfPurchase, totalPrice, selectedPlaceId, receiveDate, invoicePurchaseTypeId, purchaseCategoryId, mentionedGST, serviceList, freightAndProductTxableAmt, totalIncAllTax, discountValue, freightMaxTaxRate, freightAmt, freightIncTaxValue);
        assignTask.execute();
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog, String[] param) {
        executeTask(param[0], param[1], param[2], param[3], param[4], param[5], param[6], param[7], param[8], param[9], param[10], serviceList, param[11], param[12], param[13], param[14], param[15], param[16]);
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog, String[] param) {

    }


    private class AssignTask extends AsyncTask<String, Void, Model> {
        private final String invoiceNumber, invoiceDate, vendorId, vendorName, freightAmount;
        private final String freightAndProductTxableAmt,totalIncAllTax,freightMaxTaxRate, freightIncTaxValue, discount;
        private final String placeOfPurchase,totalPrice,selectedPlaceId,receiveDate,invoicePurchaseTypeId,purchaseCategoryId,mentionedGST;
        private String freightCgstRate, freightCgstAmount, freightSgstRate, freightSgstAmount, freightIgstRate, freightIgstAmount;
        private final ArrayList<ServiceModel> serviceList;
        private Context context;

        public AssignTask(Context context, String invoiceNumber, String invoiceDate, String vendorId
                , String vendorName, String placeOfPurchase, String totalPrice, String selectedPlaceId
                , String receiveDate, String invoicePurchaseTypeId,String purchaseCategoryId ,String mentionedGST, ArrayList<ServiceModel> serviceList
                , String freightAndProductTxableAmt, String totalIncAllTax, String discountValue, String freightMaxTaxRate, String freightAmt, String freightIncTaxValue) {
            this.invoiceNumber = invoiceNumber;
            this.invoiceDate = invoiceDate;
            this.vendorId = vendorId;
            this.vendorName = vendorName;
            this.context = context;
            this.serviceList = serviceList;
            this.placeOfPurchase = placeOfPurchase;
            this.totalPrice = totalPrice;
            this.selectedPlaceId = selectedPlaceId;
            this.receiveDate = receiveDate;
            this.invoicePurchaseTypeId = invoicePurchaseTypeId;
            this.purchaseCategoryId = purchaseCategoryId;
            this.mentionedGST = mentionedGST;
            this.freightMaxTaxRate = freightMaxTaxRate;
            this.freightAmount = freightAmt;
            this.freightIncTaxValue = freightIncTaxValue;
            this.totalIncAllTax = totalIncAllTax;
            this.freightAndProductTxableAmt = freightAndProductTxableAmt;
            this.discount = discountValue;
        }

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            btnAction.startAnimation();
        }

        @Override
        protected Model doInBackground(String... urls) {
            boolean isIGSTApplicable;
            String companyStateCode;
            String userId = AppUser.getUserId(context);
            String profileId = AppUser.getProfileId(context);
            String companyId = AppUser.getCompanyId(context);
            String[] freightResult;
            String productJson,response;
            String invoiceGenerateType="S";// for goods

            switch (sellingType) {
                case ServicePurchaseManager.ADD_PURCHASE:
                    companyStateCode = JsonParser.getSelectedCompanyStateCode(AppUser.getCompanyDetails(context), companyId);
                    if (companyStateCode.equalsIgnoreCase(mCustomerStateCode)) {
                        isIGSTApplicable = false;
                    } else {
                        isIGSTApplicable = true;
                    }
//                    freightResult = getFreightTaxRate(freightAmount, freightMaxTaxRate, isIGSTApplicable);
//                    freightCgstRate = freightResult[1];
//                    freightCgstAmount = freightResult[2];
//                    freightSgstRate = freightResult[3];
//                    freightSgstAmount = freightResult[4];
//                    freightIgstRate = freightResult[5];
//                    freightIgstAmount = freightResult[6];

                    productJson = JsonParser.generateInvoiceServiceJSON(userId, companyId, invoiceNumber, serviceList, isIGSTApplicable);
                    response = ApiCall.POST(AppSettings.SAVE_PURCHASE_SERVICE_DETAILS, RequestBuilder.addPurchasesService(invoiceGenerateType,userId, profileId, REVERCE_CHARGE , invoiceNumber, invoiceDate, vendorId, vendorName, placeOfPurchase, totalPrice, selectedPlaceId, receiveDate, invoicePurchaseTypeId, purchaseCategoryId , mentionedGST,freightAndProductTxableAmt, totalIncAllTax, discount,freightMaxTaxRate,freightAmount, freightIncTaxValue, freightCgstRate, freightCgstAmount, freightSgstRate
                            , freightSgstAmount, freightIgstRate, freightIgstAmount, productJson));
                    return JsonParser.defaultStaticParser(response);

                case ServicePurchaseManager.EDIT_PURCHASE:
                    //return new DBManager().addPurchases(result, userId, profileId, companyId, invoiceNumber, invoiceDate, vendorId, vendorName, freightAmount, transpoartMode, transpoartModeId
                    //              , vehicleNumber, dateOfSupply, placeofSupply, placeofSupplyId, shipName, shipGSTIN, shipAddress, totalPrice, serviceList, isIGSTApplicable, paymentDetail, Purchases.STOCK_SALE);
                    return JsonParser.defaultFailureResponse(getSt(R.string.edit_error));
                case ServicePurchaseManager.COPY_PURCHASE:
                    companyStateCode = JsonParser.getSelectedCompanyStateCode(AppUser.getCompanyDetails(context), companyId);
                    if (companyStateCode.equalsIgnoreCase(mCustomerStateCode)) {
                        isIGSTApplicable = false;
                    } else {
                        isIGSTApplicable = true;
                    }
//                    freightResult = getFreightTaxRate(freightAmount, freightMaxTaxRate, isIGSTApplicable);
//                    freightCgstRate = freightResult[1];
//                    freightCgstAmount = freightResult[2];
//                    freightSgstRate = freightResult[3];
//                    freightSgstAmount = freightResult[4];
//                    freightIgstRate = freightResult[5];
//                    freightIgstAmount = freightResult[6];

                    productJson = JsonParser.generateInvoiceServiceJSON(userId, companyId, invoiceNumber, serviceList, isIGSTApplicable);
                    response = ApiCall.POST(AppSettings.SAVE_PURCHASE_SERVICE_DETAILS, RequestBuilder.addPurchasesService(invoiceGenerateType,userId, profileId, REVERCE_CHARGE , invoiceNumber, invoiceDate, vendorId, vendorName, placeOfPurchase, totalPrice, selectedPlaceId, receiveDate, invoicePurchaseTypeId, purchaseCategoryId , mentionedGST,freightAndProductTxableAmt, totalIncAllTax, discount,freightMaxTaxRate,freightAmount, freightIncTaxValue, freightCgstRate, freightCgstAmount, freightSgstRate
                            , freightSgstAmount, freightIgstRate, freightIgstAmount, productJson));
                    return JsonParser.defaultStaticParser(response);
                default:
                    return JsonParser.defaultFailureResponse(getSt(R.string.update_later));
            }

        }

        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                if (!AppConfig.isUpdateToServer(context)) {
                    if (result.getOutputDB().equals(Const.SUCCESS)) {
                        showSnakBar(coardLayout, result.getOutputDBMsg());
                        animateButtonAndRevert();
                    } else if (result.getOutputDB().equals(Const.FAILURE)) {
                        btnAction.revertAnimation();
                        showSnakBar(coardLayout, result.getOutputDBMsg());
                    }
                } else {
                    if (result.getOutput().equals(Const.SUCCESS)) {
                        showSnakBar(coardLayout, result.getOutputMsg());
                        animateButtonAndRevert();
                    } else if (result.getOutput().equals(Const.FAILURE)) {
                        btnAction.revertAnimation();
                        PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(), "popupAlert");
                    } else {
                        btnAction.revertAnimation();
                        String[] errorSoon = {invoiceNumber, invoiceDate, vendorId, vendorName, placeOfPurchase, totalPrice, selectedPlaceId, receiveDate, invoicePurchaseTypeId, purchaseCategoryId , mentionedGST,freightAndProductTxableAmt, totalIncAllTax, discount, freightMaxTaxRate, freightAmount, freightIncTaxValue};
                        ApiCallError.newInstance(result.getOutput(), result.getOutputMsg(), errorSoon, PaymentServicePurchase.this)
                                .show(getFragmentManager(), "apiCallError");
                    }
                }
            }
        }

        private void animateButtonAndRevert() {
            Handler handler = new Handler();

            Runnable runnableRevert = new Runnable() {
                @Override
                public void run() {
                    if (context != null) {
                        if (servicePurchases != null) {
                            servicePurchases.refreshList();
                        }
                        getFragmentManager().popBackStack("servicePurchaseManager", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                }
            };

            btnAction.revertSuccessAnimation();
            handler.postDelayed(runnableRevert, Const.BUTTON_PROGRESS_TIME);
        }

    }

    private float getMaxTaxRate() {
        float maxTaxRate = 0;
        for (ServiceModel model : serviceList) {
            float taxRate = convertToFloat(model.getTaxRate());
            if (maxTaxRate < taxRate) {
                maxTaxRate = taxRate;
            }
        }
        return maxTaxRate;
    }


    private String[] getFreightTaxRate(String freightAmount, String maxTaxRate, Boolean isIGSTApplicable) {
        return calAllTaxes(freightAmount, maxTaxRate, isIGSTApplicable);
    }

    private String[] calAllTaxes(String freightAmount, String taxRate, Boolean isIGSTApplicable) {
        float maxTaxRate = convertToFloat(taxRate);
        float totalAmt = convertToFloat(freightAmount);
        String[] taxCal = new String[7];
        if (isIGSTApplicable) {
            taxCal[0] = String.valueOf((totalAmt * maxTaxRate) / 100);
            taxCal[1] = "0";
            taxCal[2] = "0";
            taxCal[3] = "0";
            taxCal[4] = "0";
            taxCal[5] = String.valueOf(maxTaxRate);
            taxCal[6] = String.valueOf((totalAmt * maxTaxRate) / 100);
        } else {
            taxCal[0] = String.valueOf((totalAmt * maxTaxRate) / 100);
            maxTaxRate = maxTaxRate / 2;
            taxCal[1] = String.valueOf(maxTaxRate);
            taxCal[2] = String.valueOf((totalAmt * maxTaxRate) / 100);
            taxCal[3] = String.valueOf(maxTaxRate);
            taxCal[4] = String.valueOf((totalAmt * maxTaxRate) / 100);
            taxCal[5] = "0";
            taxCal[6] = "0";
        }
        return taxCal;
    }


}
