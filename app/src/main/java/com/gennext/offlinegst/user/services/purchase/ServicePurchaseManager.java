package com.gennext.offlinegst.user.services.purchase;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.DatePickerDialog;
import com.gennext.offlinegst.global.PopupDialog;
import com.gennext.offlinegst.model.user.CommonModel;
import com.gennext.offlinegst.model.user.PurchaseServiceAdapter;
import com.gennext.offlinegst.model.user.PurchaseServiceViewAdapter;
import com.gennext.offlinegst.model.user.PurchasesModel;
import com.gennext.offlinegst.model.user.ServiceModel;
import com.gennext.offlinegst.model.user.StateModel;
import com.gennext.offlinegst.model.user.VendorModel;
import com.gennext.offlinegst.panel.PurchaseCategorySelector;
import com.gennext.offlinegst.panel.PurchaseTypeSelector;
import com.gennext.offlinegst.panel.StateSelector;
import com.gennext.offlinegst.panel.VendorSelector;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.user.purchas.PurchaseManager;
import com.gennext.offlinegst.user.services.AddService;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.DateTimeUtility;
import com.gennext.offlinegst.util.FieldValidation;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.ProgressButton;
import com.gennext.offlinegst.util.Utility;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Admin on 7/4/2017.
 */

public class ServicePurchaseManager extends CompactFragment implements View.OnClickListener
        , VendorSelector.SelectListener
        , DatePickerDialog.DateSelectFlagListener, StateSelector.SelectListener
        , PurchaseTypeSelector.SelectListener
        , PurchaseCategorySelector.SelectListener ,PopupDialog.DialogTaskListener{

    private static final int REQUEST_PRODUCT_NOT_FOUND = 1,REQUEST_SWITCH_VENDOR=2;
    public static final int VIEW_PURCHASE = 0,EDIT_PURCHASE = 1, COPY_PURCHASE = 2, ADD_PURCHASE = 3;
    private static final String TYPE_NO = "n", TYPE_YES = "y";
    private RecyclerView lvMain;
    private EditText etInvoiceNo;
    private TextView tvDate, tvVendor, tvPlaceOfPurchase, tvPurchaseType, tvReceiveDate;
    //    private FloatingActionButton addProduct;
    private String mVendorId, mVendorName;
    private PurchaseServiceAdapter adapter;
    private PurchaseServiceViewAdapter adapterView;
    private ArrayList<ServiceModel> cList;
    private int sDay, sMonth, sYear;
    private int eDay, eMonth, eYear;
    private TextView tvAddNote;
//    private FloatingActionsMenu multiMenu;

    private DBManager dbManager;
    private CoordinatorLayout coardLayout;
    private TextView tvTitle, tvQuantity, tvAmount, tvTotalAmount;
    private LinearLayout llTotalAmount;
    private float mTotalQuan = 0, mTotalAmtWithoutTax = 0;
    private ProgressButton btnAction;

    private PurchasesModel itemPurchases;
    public int purchaseType;
    private ArrayList<String> deletedProduct;
    private String mSelectedPlaceId;
    private ServicesPurchase servicesPurchase;
    private String mPurchaseTypeId;
    private String mReceiveDate;
    private String mVendorStateCode;
    private TextView tvPurchaseCategory;
    private RadioButton rbTypeYes, rbTypeNo;
    private String mMentionedGST;
    private String mPurchaseCategoryId;
    private NestedScrollView nsView;
    private FloatingActionButton fabAdd;
    private boolean isIGSTApplicable;


    public static Fragment newInstance(ServicesPurchase servicesPurchase, PurchasesModel itemPurchases, int purchaseType) {
        ServicePurchaseManager fragment = new ServicePurchaseManager();
        AppAnimation.setFadeAnimation(fragment);
        fragment.servicesPurchase = servicesPurchase;
        fragment.itemPurchases = itemPurchases;
        fragment.purchaseType = purchaseType;
        return fragment;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (dbManager != null)
            dbManager.closeDB();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_service_purchase_manager, container, false);
        initToolBar(getActivity(), v, getString(R.string.purchases_service));
        screenAnalytics(getContext(), AppUser.getUserId(getContext()), "PurchasesManager");
        dbManager = DBManager.newIsntance(getActivity());
        initUi(v);
//        setBarcodeListener();
        updateUi();

        return v;
    }

    private void updateUi() {
        if (purchaseType == ADD_PURCHASE || purchaseType == COPY_PURCHASE) {
            initDate();
        } else {
            if (itemPurchases != null) {
                etInvoiceNo.setText(itemPurchases.getInvoiceNumber());
                tvDate.setText(itemPurchases.getInvoiceDate());
            }
        }

        if (itemPurchases != null) {
            mVendorId = itemPurchases.getVendorId();
            mVendorName = itemPurchases.getVendorName();
            tvVendor.setText(itemPurchases.getVendorName());
            tvPlaceOfPurchase.setText(itemPurchases.getPlaceOfPurchase());
            mSelectedPlaceId = itemPurchases.getPlaceOfPurchaseId();
            addListItemByArrayList(itemPurchases.getServicesList());
            deletedProduct = new ArrayList<>();

            mReceiveDate = itemPurchases.getReceiveDate();
            tvReceiveDate.setText(itemPurchases.getReceiveDate());
            mPurchaseTypeId = itemPurchases.getPurchaseTypeId();
            tvPurchaseType.setText(itemPurchases.getPurchaseType());

            mPurchaseCategoryId = itemPurchases.getPurchaseCategoryId();
            tvPurchaseCategory.setText(itemPurchases.getPurchaseCategory());

            mPurchaseCategoryId = itemPurchases.getPurchaseCategoryId();
            tvPurchaseCategory.setText(itemPurchases.getPurchaseCategory());
            if (itemPurchases.getMentionedGST().toLowerCase().equals(TYPE_YES) || itemPurchases.getMentionedGST().toLowerCase().equals("yes")) {
                mMentionedGST = TYPE_YES;
                rbTypeYes.setChecked(true);
            } else {
                mMentionedGST = TYPE_NO;
                rbTypeNo.setChecked(true);
            }
        }
        if(purchaseType == VIEW_PURCHASE){
            btnAction.setText("Next");
            disableEditFields();
            hideKeybord(getActivity());
        }
    }

    private void enableEditFields() {
        fabAdd.show();
        enableEditText(etInvoiceNo);
    }

    private void disableEditFields() {
        fabAdd.hide();
        disableEditText(etInvoiceNo);
        disableRadioButton(rbTypeYes);
        disableRadioButton(rbTypeNo);
    }


    private void initDate() {
        final Calendar calendar = Calendar.getInstance();
        eYear = sYear = calendar.get(Calendar.YEAR);
        eMonth = sMonth = calendar.get(Calendar.MONTH);
        eDay = sDay = calendar.get(Calendar.DAY_OF_MONTH);
        tvDate.setText(DateTimeUtility.cDateDDMMMYY(sDay, sMonth, sYear));

        mReceiveDate = DateTimeUtility.cDateDDMMMYY(sDay, sMonth, sYear);
        tvReceiveDate.setText(mReceiveDate);
    }

    private void initUi(View v) {
        coardLayout = (CoordinatorLayout) v.findViewById(R.id.coordinatorLayout);
        btnAction = ProgressButton.newInstance(getContext(), v);
        lvMain = (RecyclerView) v.findViewById(R.id.lv_main);

        etInvoiceNo = (EditText) v.findViewById(R.id.et_purchases_inv_no);
        LinearLayout llDate = (LinearLayout) v.findViewById(R.id.ll_purchases_inv_date);
        LinearLayout llVendor = (LinearLayout) v.findViewById(R.id.ll_purchases_inv_vendor);
        LinearLayout llPurchasePlace = (LinearLayout) v.findViewById(R.id.ll_purchases_place);
        LinearLayout llPurchaseType = (LinearLayout) v.findViewById(R.id.ll_purchases_type);
        LinearLayout llReceiveDate = (LinearLayout) v.findViewById(R.id.ll_purchases_receive_date);
        LinearLayout llPurchaseCategory = (LinearLayout) v.findViewById(R.id.ll_purchases_category);
        tvDate = (TextView) v.findViewById(R.id.tv_purchases_inv_date);
        tvAddNote = (TextView) v.findViewById(R.id.tv_add_note);
        tvVendor = (TextView) v.findViewById(R.id.tv_purchases_inv_vendor);
        tvPlaceOfPurchase = (TextView) v.findViewById(R.id.tv_purchases_place);
        tvPurchaseType = (TextView) v.findViewById(R.id.tv_purchases_type);
        tvReceiveDate = (TextView) v.findViewById(R.id.tv_purchases_receive_date);
        tvPurchaseCategory = (TextView) v.findViewById(R.id.tv_purchases_category);
        fabAdd = (FloatingActionButton) v.findViewById(R.id.fab);

        llTotalAmount = (LinearLayout) v.findViewById(R.id.layoutSlot);
        tvTitle = (TextView) v.findViewById(R.id.tv_slot_1);
        tvQuantity = (TextView) v.findViewById(R.id.tv_slot_2);
        tvAmount = (TextView) v.findViewById(R.id.tv_slot_3);
        tvTotalAmount = (TextView) v.findViewById(R.id.tv_slot_4);
        nsView = (NestedScrollView) v.findViewById(R.id.nested_scroll);

//        multiMenu = (FloatingActionsMenu) v.findViewById(R.id.multiple_actions);
//        final FloatingActionButton btnBarcode = (FloatingActionButton) v.findViewById(R.id.fab_barcode);
//        final FloatingActionButton btnDirectory = (FloatingActionButton) v.findViewById(R.id.fab_directory);

        llDate.setOnClickListener(this);
        llVendor.setOnClickListener(this);
        llPurchasePlace.setOnClickListener(this);
        llPurchaseType.setOnClickListener(this);
        llReceiveDate.setOnClickListener(this);
//        btnBarcode.setOnClickListener(this);
//        btnDirectory.setOnClickListener(this);
        llPurchaseCategory.setOnClickListener(this);
        btnAction.setOnClickListener(this);
        fabAdd.setOnClickListener(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(linearLayoutManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());

        cList = new ArrayList<>();
        if(!isPurchaseView()) {
            adapter = new PurchaseServiceAdapter(getActivity(), cList, ServicePurchaseManager.this);
            lvMain.setAdapter(adapter);
        }else{
            if(itemPurchases!=null) {
                mSelectedPlaceId = itemPurchases.getPlaceOfPurchaseId();
            }else{
                mSelectedPlaceId="";
            }
            Context context=getActivity();
            String companyId = AppUser.getCompanyId(context);
            String companyStateCode = JsonParser.getSelectedCompanyStateCode(AppUser.getCompanyDetails(context), companyId);
            if (companyStateCode.equalsIgnoreCase(mSelectedPlaceId)) {
                isIGSTApplicable = false;
            } else {
                isIGSTApplicable = true;
            }
            adapterView = new PurchaseServiceViewAdapter(getActivity(), cList, ServicePurchaseManager.this,isIGSTApplicable);
            lvMain.setAdapter(adapterView);
        }

        RadioGroup rgPaymentType = (RadioGroup) v.findViewById(R.id.rg_sale);
        rbTypeYes = (RadioButton) v.findViewById(R.id.rb_yes);
        rbTypeNo = (RadioButton) v.findViewById(R.id.rb_no);
        rgPaymentType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.rb_no:
                        mMentionedGST = TYPE_NO;
                        break;
                    case R.id.rb_yes:
                        mMentionedGST = TYPE_YES;
                        break;
                }
            }
        });

    }

    private void executeTask(String invoiceNumber, String invoiceDate, String vendorId, String vendorName, String placeOfPurchase, String totalPrice, String selectedPlaceId, String receiveDate, String invoicePurchaseTypeId, String purchaseCategoryId, String mentionedGST, ArrayList<ServiceModel> productList) {
        hideKeybord(getActivity());
        String[] purchaseDetail = {invoiceNumber, invoiceDate, vendorId, vendorName, placeOfPurchase, totalPrice, selectedPlaceId, receiveDate, invoicePurchaseTypeId, purchaseCategoryId, mentionedGST};

        addFragment(PaymentServicePurchase.newInstance(servicesPurchase, purchaseType, itemPurchases, deletedProduct, purchaseDetail, mVendorStateCode, productList), "paymentCreditors");

    }

    public boolean isPurchaseView() {
        return purchaseType == VIEW_PURCHASE;
    }


    @Override
    public void onClick(View v) {
        if(v.getId()!=R.id.btn_action&&purchaseType==VIEW_PURCHASE){
            showSnakBar(coardLayout, getString(R.string.could_not_edit_this_field));
            return;
        }
        switch (v.getId()) {
            case R.id.btn_action:
                if (!FieldValidation.isEmpty(getContext(), etInvoiceNo)) {
                    showSnakBar(coardLayout, getString(R.string.invalid_invoice_no));
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), mVendorId, getString(R.string.please_select_vendor))) {
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), mReceiveDate, getString(R.string.please_select_receive_date))) {
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), mPurchaseTypeId, getString(R.string.please_select_purchase_type))) {
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), mPurchaseCategoryId, getString(R.string.please_select_purchase_category))) {
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), mSelectedPlaceId, getString(R.string.please_select_place_of_purchase))) {
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), mMentionedGST, getString(R.string.mentioned_gst_error))) {
                    return;
                } else if (cList == null || cList.size() == 0) {
                    showSnakBar(coardLayout, getString(R.string.please_add_product));
                    return;
                }
                hideKeybord(getActivity());
                executeTask(etInvoiceNo.getText().toString(), tvDate.getText().toString(), mVendorId, mVendorName
                        , tvPlaceOfPurchase.getText().toString(), String.valueOf(mTotalAmtWithoutTax), mSelectedPlaceId, mReceiveDate, mPurchaseTypeId, mPurchaseCategoryId, mMentionedGST, cList);
                break;
//            case R.id.fab_barcode:
//                startScan();
//                multiMenu.collapse();
//                break;
//            case R.id.fab_directory:
//                ProductSelector.newInstance(ServicePurchaseManager.this,mProductList)
//                        .show(getFragmentManager(), "productSelector");
//                multiMenu.collapse();
//                break;
            case R.id.fab:
                if (!FieldValidation.isEmpty(getContext(), mVendorId, getString(R.string.please_select_vendor)+" first")) {
                    return;
                }
                addFragment(AddService.newInstance(ServicePurchaseManager.this,mVendorName), "addService");
                break;
            case R.id.ll_purchases_inv_date:
                DatePickerDialog.newInstance(ServicePurchaseManager.this, false, true, 0, DatePickerDialog.START_DATE, sDay, sMonth, sYear)
                        .show(getFragmentManager(), "datePickerDialog");
                break;
            case R.id.ll_purchases_inv_vendor:
                if(tvVendor.getText().toString().equalsIgnoreCase(Const.CASH_PURCHASE)&&cList!=null&&cList.size()>0){
                    PopupDialog.newInstance("Alset!","If you switch from Cash Purchase to another vendor, all products will be clear from list below.",REQUEST_SWITCH_VENDOR,ServicePurchaseManager.this)
                            .show(getFragmentManager(),"popupDialog");
                }else {
                    VendorSelector.newInstance(ServicePurchaseManager.this)
                            .show(getFragmentManager(), "vendorSelector");
                }
                break;
            case R.id.ll_purchases_place:
                StateSelector.newInstance(ServicePurchaseManager.this).show(getFragmentManager(), "stateSelector");
                break;
            case R.id.ll_purchases_type:
                PurchaseTypeSelector.newInstance(ServicePurchaseManager.this).show(getFragmentManager(), "purchaseTypeSelector");
                break;
            case R.id.ll_purchases_receive_date:
                DatePickerDialog.newInstance(ServicePurchaseManager.this, false, DatePickerDialog.END_DATE, eDay, eMonth, eYear)
                        .show(getFragmentManager(), "datePickerDialog");
                break;
            case R.id.ll_purchases_category:
                PurchaseCategorySelector.newInstance(ServicePurchaseManager.this).show(getFragmentManager(), "purchaseCategorySelector");
                break;

        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onVendorSelect(DialogFragment dialog, VendorModel selectedVendor) {
        mVendorId = selectedVendor.getVendorId();
        mVendorName = selectedVendor.getPersonName();
        tvVendor.setText(selectedVendor.getPersonName());

        mVendorStateCode = selectedVendor.getSelectedStateCode();
        mSelectedPlaceId = selectedVendor.getSelectedStateCode();
        tvPlaceOfPurchase.setText(selectedVendor.getState());
    }

    private void NotifyDataSetChanged() {
        if(!isPurchaseView()) {
            adapter.notifyDataSetChanged();
            lvMain.smoothScrollToPosition(adapter.getItemCount());
        }else{
            adapterView.notifyDataSetChanged();
            lvMain.smoothScrollToPosition(adapterView.getItemCount());
        }
    }


    public void addListItemByModel(ServiceModel selectedProduct) {
        if(mVendorName.equalsIgnoreCase(Const.CASH_PURCHASE)) {
            selectedProduct.setTaxRate("0");
        }
        if (selectedProduct != null) {
            cList.add(selectedProduct);
        }
        NotifyDataSetChanged();

        addTotalItemOnFooter();
    }

    public void addListItemByModel(ServiceModel selectedProduct, int position) {

        if (selectedProduct != null) {
            cList.set(position, selectedProduct);
        }
        NotifyDataSetChanged();

        addTotalItemOnFooter();
    }

    private void updateListItemByModel(ServiceModel selectedProduct) {
        if(mVendorName.equalsIgnoreCase(Const.CASH_PURCHASE)) {
            selectedProduct.setTaxRate("0");
        }
        if (selectedProduct != null) {
            cList.add(selectedProduct);
        }
        NotifyDataSetChanged();

        addTotalItemOnFooter();
    }

    private void addListItemByArrayList(ArrayList<ServiceModel> prodList) {
        if (prodList != null) {
            cList.addAll(prodList);
        }
        NotifyDataSetChanged();

        addTotalItemOnFooter();
    }

    public void refreshAdapter() {
        NotifyDataSetChanged();
        addTotalItemOnFooter();
    }

    private void addTotalItemOnFooter() {
        float taxableAmount = 0, totalNetAmt = 0;
        for (int i = 0; i < cList.size(); i++) {
            float sale = parseFloat(cList.get(i).getSalePrice());
            float dis = parseFloat(cList.get(i).getDis());
            float taxRate = parseFloat(cList.get(i).getTaxRate());

            float taxable = sale-dis;
            float totalTax = taxable+((taxable * taxRate) / 100);

            taxableAmount += taxable;
            totalNetAmt += totalTax;
        }
        float quantity = cList.size();
        addTotalItemOnUI(quantity, taxableAmount, totalNetAmt);
    }


    private void addTotalItemOnUI(float quantity, float taxableAmount, float netAmt) {
        llTotalAmount.setVisibility(View.VISIBLE);
        tvTitle.setText(getString(R.string.total));
        mTotalAmtWithoutTax = taxableAmount;
        mTotalQuan = quantity;
        tvQuantity.setText(String.valueOf(quantity));
        tvAmount.setText(Utility.decimalFormat(taxableAmount, "#.##"));
        tvTotalAmount.setText(Utility.decimalFormat(netAmt, "#.##"));
        nsView.fullScroll(View.FOCUS_DOWN);
    }

    private void removeTotalItemFromUI() {
        llTotalAmount.setVisibility(View.GONE);
        tvTitle.setText(getString(R.string.total));
        tvQuantity.setText("0");
        tvAmount.setText("0");
    }

    public void editProduct(ServiceModel itemProduct, int position) {
        addFragment(AddService.newInstance(ServicePurchaseManager.this, itemProduct,position,mVendorName), "addService");
    }

    public void onProductEdited(ServiceModel itemProduct) {
        updateListItemByModel(itemProduct);
    }

    public void deleteProduct(ServiceModel itemProduct, int position) {

        if (cList.size() == 0) {
            tvAddNote.setVisibility(View.VISIBLE);
            removeTotalItemFromUI();
            return;
        }
        addTotalItemOnFooter();
    }


    public void fetchVendorFromDB(String personName, String companyName) {
//        VendorModel vendorModel = dbManager.getVendorIdByVendorAndCompanyName(AppUser.getUserId(getActivity()), personName, companyName);
//        if (vendorModel.getOutputDB().equals(Const.SUCCESS)) {
//            mVendorId = vendorModel.getVendorId();
//            mVendorName = vendorModel.getPersonName();
//            tvVendor.setText(vendorModel.getPersonName());
//        } else {
//            showSnakBar(coardLayout, vendorModel.getOutputDBMsg());
//        }
    }


    @Override
    public void onStateSelect(DialogFragment dialog, StateModel selectedState) {
        mSelectedPlaceId = selectedState.getStateCode();
        tvPlaceOfPurchase.setText(selectedState.getStateName());
    }

    @Override
    public void onStartDateSelected(DialogFragment dialog, int day, int month, int year, String ddMMMyy) {
        sDay = day;
        sMonth = month;
        sYear = year;
        tvDate.setText(ddMMMyy);
    }

    @Override
    public void onEndDateSelected(DialogFragment dialog, int day, int month, int year, String ddMMMyy) {
        eDay = day;
        eMonth = month;
        eYear = year;
        mReceiveDate = ddMMMyy;
        tvReceiveDate.setText(ddMMMyy);
    }

    @Override
    public void onPurchaseTypeSelect(DialogFragment dialog, CommonModel selectedItem) {
        mPurchaseTypeId = selectedItem.getName();
        tvPurchaseType.setText(selectedItem.getName());
    }

    @Override
    public void onPurchaseCategorySelect(DialogFragment dialog, CommonModel selectedItem) {
        mPurchaseCategoryId = selectedItem.getId();
        tvPurchaseCategory.setText(selectedItem.getName());
    }


    @Override
    public void onDialogOkClick(DialogFragment dialog, int task) {
        cList.clear();
        refreshAdapter();
        VendorSelector.newInstance(ServicePurchaseManager.this)
                .show(getFragmentManager(), "vendorSelector");
    }

    @Override
    public void onDialogCancelClick(DialogFragment dialog, int task) {

    }
}
