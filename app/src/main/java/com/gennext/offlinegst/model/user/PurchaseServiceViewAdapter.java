package com.gennext.offlinegst.model.user;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.user.services.purchase.ServicePurchaseManager;
import com.gennext.offlinegst.util.Utility;

import java.util.ArrayList;
import java.util.Random;

public class PurchaseServiceViewAdapter extends RecyclerView.Adapter<PurchaseServiceViewAdapter.ReyclerViewHolder>{

    private final ServicePurchaseManager parentRef;
    private final boolean isIGSTApplicable;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<ServiceModel> items;

    public PurchaseServiceViewAdapter(Activity context, ArrayList<ServiceModel> items, ServicePurchaseManager parentRef, boolean isIGSTApplicable) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef = parentRef;
        this.isIGSTApplicable = isIGSTApplicable;
        this.items = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_selected_service_view, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final ServiceModel item = items.get(position);
        float price = convertToFloat(item.getSalePrice());
        float taxRate = convertToFloat(item.getTaxRate());
        float allTax = calTax(price, taxRate);

        holder.tvName.setText(item.getServiceName());
        holder.tvSAC.setText(generateBoldTitle("SAC:",item.getCodeSAC()));
        if (isIGSTApplicable) {
            String mTaxable = Utility.decimalFormat(allTax, "#.##");
            holder.tvTaxRate.setText(generateBoldTitle("IGST:", mTaxable));
        } else {
            String mTaxable = Utility.decimalFormat(allTax, "#.##");
            holder.tvTaxRate.setText(generateBoldTitle("SGST+CGST:", mTaxable));
        }
        holder.tvTaxable.setText(generateBoldTitle("Taxable:",context.getString(R.string.rs) + item.getSalePrice()));
        holder.tvNet.setText(generateBoldTitle("Net:",context.getString(R.string.rs) + item.getNetAmount()));
        setColor(holder.llSlot);
    }
    private static float calTax(float taxableAmount, float maxTaxRate) {
        return (taxableAmount * maxTaxRate) / 100;
    }
    public void setColor(LinearLayout tv) {
        Random rnd = new Random();
        int color= Utility.SIDE_LINE_COLORS[rnd.nextInt(21)];
        tv.setBackgroundColor(color);
    }
    public static SpannableString generateBoldTitle(String title, String noramalText) {
        SpannableString s = new SpannableString(title + " " + noramalText);
        s.setSpan(new StyleSpan(Typeface.BOLD), 0, title.length(), 0);
        return s;
    }
    private static float convertToFloat(String salePrice) {
        try {
            return Float.parseFloat(salePrice);
        } catch (NumberFormatException e) {
            return 0;
        } catch (NullPointerException e) {
            return 0;
        }
    }
    @Override
    public int getItemCount() {
        return items.size();
    }



    class ReyclerViewHolder extends RecyclerView.ViewHolder { 
        private LinearLayout llSlot;
        private TextView tvName, tvSAC,tvPrice,tvTaxRate, tvTaxable, tvNet;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSlot = (LinearLayout) v.findViewById(R.id.ll_slot);
            tvName = (TextView) v.findViewById(R.id.tv_slot_0);
            tvSAC = (TextView) v.findViewById(R.id.tv_slot_1);
            tvPrice = (TextView) v.findViewById(R.id.tv_slot_2);
            tvTaxRate = (TextView) v.findViewById(R.id.tv_slot_3);
            tvTaxable = (TextView) v.findViewById(R.id.tv_slot_4);
            tvNet = (TextView) v.findViewById(R.id.tv_slot_5);
        }
    }
}

