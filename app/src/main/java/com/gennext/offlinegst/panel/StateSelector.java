package com.gennext.offlinegst.panel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.model.user.StateModel;
import com.gennext.offlinegst.model.user.StateSelectorAdapter;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.JsonParser;

import java.util.ArrayList;


public class StateSelector extends DialogFragment{
//    private static final String STATE_FILE_NAME = "global/stateList.json";
    private SelectListener mListener;
    private StateSelectorAdapter adapter;

    public interface SelectListener {
        void onStateSelect(DialogFragment dialog, StateModel selectedState);

    }


    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }


    public static StateSelector newInstance(SelectListener listener) {
        StateSelector fragment = new StateSelector();
        fragment.mListener = listener;
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_menu_state, container, false);
        EditText etSearch = (EditText) v.findViewById(R.id.et_search);
        ListView lvMain = (ListView) v.findViewById(R.id.lv_main);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (adapter != null)
                    adapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                if (adapter != null) {
                    StateModel model = adapter.getItem(pos);
                    if (mListener != null) {
                        mListener.onStateSelect(StateSelector.this,model);
                    }
                }
                dismiss();
            }
        });
//        ArrayList<StateModel> cModel = JsonParser.parseStateList(ApiCall.getAssets(getContext(),STATE_FILE_NAME));
        ArrayList<StateModel> cModel = JsonParser.parseStateList(AppUser.getStatesList(getContext()));
        if (cModel!=null) {
            adapter = new StateSelectorAdapter(getActivity(), cModel);
            lvMain.setAdapter(adapter);
        }else{
            ArrayList<String> errorList = new ArrayList<>();
            errorList.add("State not found");
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.slot_empty_product,
                    R.id.tv_message, errorList);
            lvMain.setAdapter(adapter);
        }

        return v;
    }


}
