package com.gennext.offlinegst.search;

/**
 * Created by Admin on 5/22/2017.
 */

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.transition.Fade;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.model.Sort;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.DetailsTransition;

public class SearchBar extends CompactFragment {


    private ImageView ivImage;
    private FrameLayout flSearch;
    private Search listener;
    private SearchOrder listener2;
    private Button btnShort, btnFilter;
    private String mStartDate, mEndDate;
    private int mOrderType;
    private String[] sortName;
    private String searchHint;

    public void updateDate(String startDate, String endDate) {
        this.mStartDate = startDate;
        this.mEndDate = endDate;
    }

    public void updateOrderType(int mType) {
        this.mOrderType = mType;
    }

    public interface Search {
        void onTextChanged(CharSequence charSequence);

        void onShortButtonPressed(String type);

        void onFilterButtonPressed(String startDate, String endDate);
    }

    public interface SearchOrder {
        void onTextChanged(CharSequence charSequence);

        void onShortButtonPressed(String type);

        void onShortOrderPressed(int order);
    }

    public static Fragment newInstance(SearchBar.Search listener) {
        return newInstance(listener, null, null,null);
    }

    public static Fragment newInstance(SearchBar.Search listener, String startDate, String endDate) {
        return newInstance(listener, startDate, endDate, null);
    }

    public static Fragment newInstance(SearchBar.Search listener, String startDate, String endDate, String searchHint) {
        SearchBar fragment = new SearchBar();
        fragment.listener = listener;
        fragment.mStartDate = startDate;
        fragment.mEndDate = endDate;
        fragment.searchHint = searchHint;
        return fragment;
    }


    public static Fragment newInstance(SearchBar.SearchOrder listener) {
        return newInstance(listener, null,null);
    }

    public static Fragment newInstance(SearchBar.SearchOrder listener,String[] sortName, String searchHint) {
        SearchBar fragment = new SearchBar();
        fragment.listener2 = listener;
        fragment.sortName = sortName;
        fragment.mOrderType = Sort.Ascending;
        fragment.searchHint = searchHint;
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.search_bar, container, false);
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        ivImage = (ImageView) v.findViewById(R.id.iv_search_bar);
        flSearch = (FrameLayout) v.findViewById(R.id.fl_search);
        btnShort = (Button) v.findViewById(R.id.btn_short);
        btnFilter = (Button) v.findViewById(R.id.btn_filter);
        flSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SearchBarView fragment = SearchBarView.newInstance(listener, listener2, searchHint);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    showTargetFragmentLollipop(fragment, ivImage);
                    return;
                }
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container_bar, fragment)
                        .addToBackStack("searchBarView")
                        .commit();
            }
        });

        if (listener2 != null) {
            btnFilter.setText("Order");
        }

        btnShort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceFragment(SortBarVIew.newInstance(listener, listener2, sortName), R.id.container_bar, "sortBarVIew");

            }
        });
        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener2 == null) {
                    replaceFragment(FilterBarView.newInstance(listener, SearchBar.this, mStartDate, mEndDate), R.id.container_bar, "filterBarView");
                } else {
                    replaceFragment(OrderBarView.newInstance(listener2, SearchBar.this, mOrderType), R.id.container_bar, "orderBarView");
                }
            }
        });

    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void showTargetFragmentLollipop(SearchBarView fragment, View forImageView) {
        fragment.setSharedElementEnterTransition(new DetailsTransition());
        fragment.setEnterTransition(new Fade());
        fragment.setExitTransition(new Fade());
        fragment.setSharedElementReturnTransition(new DetailsTransition());

        String transitionName = ViewCompat.getTransitionName(forImageView);
        getFragmentManager()
                .beginTransaction()
                .addSharedElement(forImageView, transitionName)
                .replace(R.id.container_bar, fragment, "searchBarView")
                .addToBackStack("searchBarView")
                .commit();
    }

}
