package com.gennext.offlinegst.model;

/**
 * Usage
 //    private void addSearchTag(Bundle savedInstanceState) {
//        if (savedInstanceState == null) {
//            getFragmentManager().beginTransaction()
//                    .replace(R.id.container_bar, SearchBar.newInstance(ClassName.this),"searchBar")
//                    .commit();
//        }
//    }
 *
 */
public class Sort {

    public static String Type1 = "Type1";
    public static String Type2 = "Type2";
    public static String Type3 = "Type3";
    public static int Ascending = 1;
    public static int Descending = 2;
}
