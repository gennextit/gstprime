package com.gennext.offlinegst.global;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.offlinegst.R;

////8375909642
public class CompactFragment extends Fragment {
    public static final int TASK_LOAD_LIST = 0, TASK_DELETE = 1, TASK_ACTION = 2;

    public boolean APICheck() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return true;
        } else {
            return false;
        }
    }

    public void showSnakBarAction(CoordinatorLayout view, String message) {
        Snackbar snackbar = Snackbar
                .make(view, message, Snackbar.LENGTH_LONG)
                .setAction(R.string.retry, new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });

        // Changing message text color
        snackbar.setActionTextColor(Color.RED);

        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);

        snackbar.show();
    }

    public String valString(String txt) {
        return txt != null ? txt.replaceAll("'", "''") : "";
    }

    public void showToast(String txt) {
        Toast.makeText(getContext(), txt, Toast.LENGTH_LONG).show();
    }

    public void disableEditText(EditText editText) {
        editText.setFocusable(false);
        editText.setFocusableInTouchMode(false);
        editText.setClickable(true);
    }

    public void enableRadioButton(RadioButton radioButton) {
//        radioButton.setEnabled(true);
        radioButton.setClickable(true);
    }

    public void disableRadioButton(RadioButton radioButton) {
//        radioButton.setEnabled(false);
        radioButton.setClickable(false);
    }

    public void enableEditText(EditText editText) {
        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.setClickable(false);
    }

    public void showToastInCenter(String txt) {
        Toast toast = Toast.makeText(getContext(), txt, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public void screenAnalytics(Context context, String id, String screenName) {
//        FirebaseAnalytics mFAnalitics = FirebaseAnalytics.getInstance(context);
//        Bundle bundle = new Bundle();
//        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, id);
//        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, screenName);
//        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "screen");
//        mFAnalitics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    public int parseInt(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return 0;
        } catch (NullPointerException e) {
            return 0;
        }
    }

    public float parseFloat(String value) {
        try {
            return Float.parseFloat(value);
        } catch (NumberFormatException e) {
            return 0;
        } catch (NullPointerException e) {
            return 0;
        }
    }

    public String parseString(float value) {
        try {
            return String.valueOf(value);
        } catch (Exception e) {
            return "";
        }
    }

    public String parseString(int value) {
        try {
            return String.valueOf(value);
        } catch (Exception e) {
            return "";
        }
    }


    public void showSnakBar(CoordinatorLayout view, String message) {
        Snackbar snackbar = Snackbar
                .make(view, message, Snackbar.LENGTH_LONG);

        // Changing message text color
        snackbar.setActionTextColor(Color.WHITE);

//        // Changing action button text color
//        View sbView = snackbar.getView();
//        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
//        textView.setTextColor(Color.YELLOW);

        snackbar.show();
    }

    public void showSnakBarWithButton(CoordinatorLayout view, String message) {
        showSnakBarWithButton(view, message, Snackbar.LENGTH_INDEFINITE);
    }

    public void showSnakBarWithButton(CoordinatorLayout view, String message, int durationTime) {
        final Snackbar snackbar = Snackbar.make(view, message, durationTime);
        snackbar.setAction(R.string.dismiss, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        View snackbarView = snackbar.getView();
        TextView snackTextView = (TextView) snackbarView
                .findViewById(android.support.design.R.id.snackbar_text);
        snackTextView.setMaxLines(5);
        snackbar.show();
    }

    public Toolbar initToolBar(final Activity act, View v, String title) {
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        AppCompatActivity activity = (AppCompatActivity) act;
        activity.setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_back);
        toolbar.setNavigationOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeybord(getActivity());
                getActivity().onBackPressed();
            }
        });
        return toolbar;
    }

     public Toolbar initToolBarHome(final Activity act, View v, String title, final String closingTag) {
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        toolbar.setTitle(title);
         ImageButton btnActionMenu = (ImageButton) v.findViewById(R.id.btn_action_menu);
         ImageButton btnActionHome = (ImageButton) v.findViewById(R.id.btn_action_home);
         AppCompatActivity activity = (AppCompatActivity) act;
        activity.setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_back);
        toolbar.setNavigationOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeybord(getActivity());
                getActivity().onBackPressed();
            }
        });
         btnActionMenu.setVisibility(View.GONE);
         btnActionHome.setOnClickListener(new OnClickListener() {
             @Override
             public void onClick(View view) {
                 hideKeybord(getActivity());
                 getActivity().onBackPressed();
             }
         });
        return toolbar;
    }

    public void initToolBarForActivity(final Activity act, View v, String title) {
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        AppCompatActivity activity = (AppCompatActivity) act;
        activity.setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.mipmap.ic_back);
        toolbar.setNavigationOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideKeybord(act);
                        act.finish();
                    }
                }

        );
    }


    public boolean isOnline() {
        boolean conn = false;
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(getActivity().CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        if (haveConnectedWifi == true || haveConnectedMobile == true) {
            conn = true;
        } else {
            conn = false;
        }

        return conn;
    }


    public void closeFragmentDialog(String dialogName) {
        FragmentManager fragmentManager = getFragmentManager();
        DialogFragment fragment = (DialogFragment) fragmentManager.findFragmentByTag(dialogName);
        if (fragment != null) {
            fragment.dismiss();
        }
    }

    protected void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
//        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                .setAction("Action", null).show();
    }

    public void showpopupalert(String title, String description, int finishType) {
        showPopupAlert(title, description, finishType, 3);
    }

    public void showPopupAlert(String title, String description, int finishType, int popupTimeOutInSec) {
//        PopupDialog dialog=PopupDialog.newInstance()
//        PopupAlert popupAlert = PopupAlert.newInstance(title, description, finishType, popupTimeOutInSec);
//        AppAnimation.setDialogAnimation(getContext(),popupAlert);
//        addFragment(popupAlert,"popupAlert");
    }


    public static void hideKeybord(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View f = activity.getCurrentFocus();
        if (null != f && null != f.getWindowToken() && EditText.class.isAssignableFrom(f.getClass()))
            imm.hideSoftInputFromWindow(f.getWindowToken(), 0);
        else
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public static void showKeybord(Activity activity) {
//        if(activity!=null) {
//            Window win = activity.getWindow();
//            if (win != null) {
//                win.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
//            }
//        }
    }


    public String LoadPref(String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String data = sharedPreferences.getString(key, "");
        return data;
    }

    public void SavePref(String key, String value) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();

    }

    public int getCol(Context context, int id) {
        return ContextCompat.getColor(context, id);
    }

    public String getSt(int id) {
        return getResources().getString(id);
    }


    protected void replaceFragmentWithoutBackstack(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.commit();
    }

    protected void replaceFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    protected void setFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    public void addFragment(Fragment fragment, String tag) {
        addFragment(fragment, android.R.id.content, tag);
    }
    public void removeFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.detach(fragment);
        transaction.commit();
    }

    public void addFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commitAllowingStateLoss();
    }

    public void addFragmentWithoutBackStack(Fragment fragment, String tag) {
        addFragmentWithoutBackStack(fragment, android.R.id.content, tag);
    }

    public void addFragmentWithoutBackStack(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.commitAllowingStateLoss();
    }

    public void setUnSetFragment(FragmentManager manager, int containerId, Fragment addFragment, String fragTag
            , Fragment rFrag1) {
        FragmentTransaction ft = manager.beginTransaction();
        if (addFragment != null) {
            if (addFragment.isAdded()) { // if the fragment is already in container
                ft.show(addFragment);
            } else { // fragment needs to be added to frame container
                ft.add(containerId, addFragment, fragTag);
            }
        }

        // Hide other fragments
        if (rFrag1 != null && rFrag1.isAdded()) {
            ft.hide(rFrag1);
        }

        ft.commit();
    }
}
