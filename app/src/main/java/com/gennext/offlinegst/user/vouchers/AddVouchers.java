package com.gennext.offlinegst.user.vouchers;

import android.Manifest;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.DatePickerDialog;
import com.gennext.offlinegst.global.PopupAlert;
import com.gennext.offlinegst.global.PopupDialog;
import com.gennext.offlinegst.model.app.Model;
import com.gennext.offlinegst.model.user.CommonModel;
import com.gennext.offlinegst.model.user.CustomerModel;
import com.gennext.offlinegst.model.user.InvoiceModel;
import com.gennext.offlinegst.model.user.ProductModel;
import com.gennext.offlinegst.model.user.StateModel;
import com.gennext.offlinegst.model.user.VoucherModel;
import com.gennext.offlinegst.model.user.VoucherProductAdapter;
import com.gennext.offlinegst.panel.BarcodeScanner;
import com.gennext.offlinegst.panel.CustomerSelector;
import com.gennext.offlinegst.panel.InvoiceSelector;
import com.gennext.offlinegst.panel.NoteReasonSelector;
import com.gennext.offlinegst.panel.NoteTypeSelector;
import com.gennext.offlinegst.panel.ProductSelector;
import com.gennext.offlinegst.panel.ShippingDetailDialog;
import com.gennext.offlinegst.panel.StateSelector;
import com.gennext.offlinegst.panel.TaxSelector;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.user.product.AddProduct;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.ApiCallError;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.DateTimeUtility;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.ProgressButton;
import com.gennext.offlinegst.util.RequestBuilder;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Admin on 7/4/2017.
 */

public class AddVouchers extends CompactFragment implements TaxSelector.SelectListener,View.OnClickListener
        , CustomerSelector.SelectListener, ProductSelector.SelectListener, PopupDialog.DialogTaskListener
        , DatePickerDialog.DateSelectFlagListener, ApiCallError.ErrorParamListener, StateSelector.SelectListener
        , ShippingDetailDialog.ShipDetail, BarcodeScanner.Barcodelistener, NoteTypeSelector.SelectListener
        , NoteReasonSelector.SelectListener, InvoiceSelector.SelectListener {

    private static final int REQUEST_PRODUCT_NOT_FOUND = 1;
    public static final int EDIT_VOUCHER = 1, COPY_VOUCHER = 2, ADD_VOUCHER = 3;
    private RecyclerView lvMain;
    private EditText etVoucherNo, etFreightAmt, etPreGST, etNoteValue, etTaxableValue, etCessAmt;
    private LinearLayout llDate, llNoteType, llReason, llCustomer, llInvoiceNo, llInvoiceDate, llPlaceOfSupply,llTaxRate;
    private TextView tvDate, tvNoteType, tvReason, tvCustomer,tvTaxRate, tvInvoiceNo, tvInvoiceDate, tvPlaceOfSupply;
    //    private FloatingActionButton addProduct;
    private String mCustomerId, mCustomerName;
    private VoucherProductAdapter adapter;
    private ArrayList<ProductModel> cList;
    private int sDay, sMonth, sYear;
    private int eDay, eMonth, eYear;
    private TextView tvAddNote;

    public static final String BARCODE_KEY = "BARCODE";

    //    private Barcode barcodeResult;
    private String mSearchBarcode;
    private DBManager dbManager;
    private CoordinatorLayout coardLayout;
    private ProductModel mSearchBarcodeProduct;
    //    private MaterialBarcodeScanner.OnResultListener listener;
    private TextView tvTitle, tvQuantity, tvAmount;
    private LinearLayout llTotalAmount;
    private int mTotalQuan = 0, mTotalAmt = 0;
    private ProgressButton btnAction;

    private AssignTask assignTask;
    private VoucherModel itemVoucher;
    private int sellingType;
    private ArrayList<String> deletedProduct;
    private String mPlaceofSupplyId;
    private String mTranspoartModeId, mTranspoartMode;
    private String mInvoiceDate;
    private String mShipName = "", mShipGSTIN = "", mShipAddress = "";
    private String mNoteType, mNoteTypeId, mNoteReason, mNoteReasonId;
    private String mInvoiceNo;
    private Vouchers vouchers;
    private ArrayList<ProductModel> mProductList;


    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static Fragment newInstance(Vouchers vouchers, VoucherModel itemVoucher, int sellingType) {
        AddVouchers fragment = new AddVouchers();
        AppAnimation.setFadeAnimation(fragment);
        fragment.vouchers = vouchers;
        fragment.itemVoucher = itemVoucher;
        fragment.sellingType = sellingType;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_vouchers_add, container, false);
        initToolBar(getActivity(), v, getString(R.string.add_voucher));
        screenAnalytics(getContext(), AppUser.getUserId(getContext()), "AddVouchers");
        dbManager = DBManager.newIsntance(getActivity());
        initUi(v);
        updateUi();
        return v;
    }

    private void updateUi() {
        if (sellingType == ADD_VOUCHER || sellingType == COPY_VOUCHER) {
            String userId = AppUser.getUserId(getActivity());
            String profileId = AppUser.getProfileId(getActivity());
            String companyId = AppUser.getCompanyId(getActivity());
            String voucherNumber = dbManager.getVoucherNumber(userId, profileId);
            if (voucherNumber.equals("")) {
                voucherNumber = "0001";
            } else {
                voucherNumber = AppConfig.generateVoucherWithExistingNumber(getContext(), userId, profileId, companyId, voucherNumber);
            }
            etVoucherNo.setText(voucherNumber);
            initDate();
        } else {
            if (itemVoucher != null) {
                etVoucherNo.setText(itemVoucher.getVoucherNo());
                tvDate.setText(itemVoucher.getVoucherDate());
            }
        }

        if (itemVoucher != null) {
            tvDate.setText(itemVoucher.getVoucherDate());

            etFreightAmt.setText(itemVoucher.getFreightAmount());
            etPreGST.setText(itemVoucher.getPreGST());
            etNoteValue.setText(itemVoucher.getNoteValue());
            tvTaxRate.setText(itemVoucher.getTaxRate());
            etTaxableValue.setText(itemVoucher.getTaxableValue());
            etCessAmt.setText(itemVoucher.getCessAmt());
            tvNoteType.setText(itemVoucher.getNoteType());
            mNoteType = itemVoucher.getNoteType();
            mNoteTypeId = itemVoucher.getNoteTypeId();

            tvReason.setText(itemVoucher.getNoteReason());
            mNoteReason = itemVoucher.getNoteReason();
            mNoteReasonId = itemVoucher.getNoteReasonId();

            tvCustomer.setText(itemVoucher.getCustomerName());
            mCustomerId = itemVoucher.getCustomerId();
            mCustomerName = itemVoucher.getCustomerName();

            tvInvoiceNo.setText(itemVoucher.getInvoiceNumber());
            mInvoiceNo = itemVoucher.getInvoiceNumber();

            tvInvoiceDate.setText(itemVoucher.getInvoiceDate());
            mInvoiceDate = itemVoucher.getInvoiceDate();

            tvPlaceOfSupply.setText(itemVoucher.getPlaceofSupply());
            mPlaceofSupplyId = itemVoucher.getPlaceofSupplyId();

            addListItemByArrayList(itemVoucher.getProductList());
            deletedProduct = new ArrayList<>();
        }
    }

    @Override
    public void onBarcodeDetected(String barcode) {
//        barcodeResult = barcode;
        mSearchBarcode = barcode;
        fetchFromDB(mSearchBarcode);
        if (mSearchBarcodeProduct != null) {
            AddProduct addProduct = (AddProduct) getFragmentManager().findFragmentByTag("addProduct");
            if (addProduct == null) {
                PopupDialog dialog = (PopupDialog) getFragmentManager().findFragmentByTag("popupDialog");
                if (dialog == null) {
                    PopupDialog.newInstance(getString(R.string.product_not_found), getString(R.string.barcode_new_scan_found), REQUEST_PRODUCT_NOT_FOUND, AddVouchers.this)
                            .show(getFragmentManager(), "popupDialog");
                }
            }
        }
    }


    private void initDate() {
        final Calendar calendar = Calendar.getInstance();
        sYear = calendar.get(Calendar.YEAR);
        sMonth = calendar.get(Calendar.MONTH);
        sDay = calendar.get(Calendar.DAY_OF_MONTH);
        tvDate.setText(DateTimeUtility.cDateDDMMMYY(sDay, sMonth, sYear));
    }

    private void initUi(View v) {
        coardLayout = (CoordinatorLayout) v.findViewById(R.id.coordinatorLayout);
        btnAction = ProgressButton.newInstance(getContext(),v);
        lvMain = (RecyclerView) v.findViewById(R.id.lv_main);

        llDate = (LinearLayout) v.findViewById(R.id.ll_voucher_date);
        llNoteType = (LinearLayout) v.findViewById(R.id.ll_voucher_note_type);
        llReason = (LinearLayout) v.findViewById(R.id.ll_voucher_reason);
        llCustomer = (LinearLayout) v.findViewById(R.id.ll_voucher_customer);
        llInvoiceNo = (LinearLayout) v.findViewById(R.id.ll_voucher_invoice_no);
        llInvoiceDate = (LinearLayout) v.findViewById(R.id.ll_voucher_invoice_date);
        llPlaceOfSupply = (LinearLayout) v.findViewById(R.id.ll_voucher_plase_of_supply);
        llTaxRate = (LinearLayout) v.findViewById(R.id.ll_voucher_tax_rate);
        etVoucherNo = (EditText) v.findViewById(R.id.et_voucher_no);
        etFreightAmt = (EditText) v.findViewById(R.id.et_voucher_fright_amt);
        etPreGST = (EditText) v.findViewById(R.id.et_voucher_pre_gst);
        etNoteValue = (EditText) v.findViewById(R.id.et_voucher_note_value);
        tvTaxRate = (TextView) v.findViewById(R.id.tv_voucher_tax_rate);
        etTaxableValue = (EditText) v.findViewById(R.id.et_voucher_taxable_value);
        etCessAmt = (EditText) v.findViewById(R.id.et_voucher_cess_amt);
        tvDate = (TextView) v.findViewById(R.id.tv_voucher_date);
        tvNoteType = (TextView) v.findViewById(R.id.tv_voucher_note_type);
        tvReason = (TextView) v.findViewById(R.id.tv_voucher_reason);
        tvCustomer = (TextView) v.findViewById(R.id.tv_voucher_customer);
        tvInvoiceNo = (TextView) v.findViewById(R.id.tv_voucher_invoice_no);
        tvInvoiceDate = (TextView) v.findViewById(R.id.tv_voucher_invoice_date);
        tvAddNote = (TextView) v.findViewById(R.id.tv_add_note);
        tvPlaceOfSupply = (TextView) v.findViewById(R.id.tv_voucher_plase_of_supply);

        llTotalAmount = (LinearLayout) v.findViewById(R.id.layoutSlot);
        tvTitle = (TextView) v.findViewById(R.id.tv_slot_1);
        tvQuantity = (TextView) v.findViewById(R.id.tv_slot_2);
        tvAmount = (TextView) v.findViewById(R.id.tv_slot_3);


        llDate.setOnClickListener(this);
        llNoteType.setOnClickListener(this);
        llReason.setOnClickListener(this);
        llCustomer.setOnClickListener(this);
        llInvoiceNo.setOnClickListener(this);
        llInvoiceDate.setOnClickListener(this);
        llPlaceOfSupply.setOnClickListener(this);
        llTaxRate.setOnClickListener(this);
        btnAction.setOnClickListener(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(linearLayoutManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());

        cList = new ArrayList<>();
        adapter = new VoucherProductAdapter(getActivity(), cList, AddVouchers.this);
        lvMain.setAdapter(adapter);


    }


    private void executeTask(String voucherNo, String voucherDate, String noteType, String noteTypeId, String noteReason, String noteReasonId, String customerId, String customerName
            , String invoiceNo, String freightAmt, String invoiceDate, String preGST, String placeOfSupply, String placeofSupplyId, String noteValue, String taxRate
            , String taxableValue, String cessAmt, ArrayList<ProductModel> productList) {
        hideKeybord(getActivity());
        assignTask = new AssignTask(getActivity(), voucherNo, voucherDate, noteType, noteTypeId, noteReason, noteReasonId, customerId, customerName, invoiceNo, freightAmt, invoiceDate, preGST, placeOfSupply
                , placeofSupplyId, noteValue, taxRate, taxableValue, cessAmt, productList);
        assignTask.execute(AppSettings.ADD_VOUCHER);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_action:
                if (mCustomerId != null) {
                    if (cList != null && cList.size() != 0) {
                        executeTask(etVoucherNo.getText().toString(), tvDate.getText().toString(), mNoteType, mNoteTypeId, mNoteReason, mNoteReasonId, mCustomerId, mCustomerName
                                , mInvoiceNo, etFreightAmt.getText().toString(), mInvoiceDate, etPreGST.getText().toString(), tvPlaceOfSupply.getText().toString()
                                , mPlaceofSupplyId, etNoteValue.getText().toString(), tvTaxRate.getText().toString(), etTaxableValue.getText().toString(), etCessAmt.getText().toString(), cList);
                    } else {
                        showSnakBar(coardLayout, getString(R.string.please_add_product));
                    }
                } else {
                    showSnakBar(coardLayout, getString(R.string.please_select_vendor));
                }
                break;
           case R.id.ll_voucher_date:
                hideKeybord(getActivity());
                DatePickerDialog.newInstance(AddVouchers.this, false, DatePickerDialog.START_DATE, sDay, sMonth, sYear)
                        .show(getFragmentManager(), "datePickerDialog");
                break;
            case R.id.ll_voucher_note_type:
                hideKeybord(getActivity());
                NoteTypeSelector.newInstance(AddVouchers.this).show(getFragmentManager(), "noteTypeSelector");
                break;
            case R.id.ll_voucher_reason:
                hideKeybord(getActivity());
                NoteReasonSelector.newInstance(AddVouchers.this).show(getFragmentManager(), "noteReasonSelector");
                break;
            case R.id.ll_voucher_customer:
                hideKeybord(getActivity());
//                CustomerSelector.newInstance(AddVouchers.this)
//                        .show(getFragmentManager(), "customerSelector");
                break;
            case R.id.ll_voucher_invoice_no:
                hideKeybord(getActivity());
                InvoiceSelector.newInstance(AddVouchers.this).show(getFragmentManager(), "invoiceSelector");
                break;
            case R.id.ll_voucher_invoice_date:
                hideKeybord(getActivity());
//                DatePickerDialog.newInstance(AddVouchers.this, false, DatePickerDialog.END_DATE, eDay, eMonth, eYear)
//                        .show(getFragmentManager(), "datePickerDialog");
                break;
            case R.id.ll_voucher_plase_of_supply:
//                hideKeybord(getActivity());
//                StateSelector.newInstance(AddVouchers.this).show(getFragmentManager(), "stateSelector");
                break;
            case R.id.ll_voucher_tax_rate:
                hideKeybord(getActivity());
                TaxSelector.newInstance(AddVouchers.this).show(getFragmentManager(),"taxSelector");
                break;

        }
    }

    @Override
    public void onInvoiceSelect(DialogFragment dialog, InvoiceModel invoice) {
        mInvoiceNo = invoice.getInvoiceNumber();
        tvInvoiceNo.setText(invoice.getInvoiceNumber());

        mCustomerId = invoice.getCustomerId();
        mCustomerName = invoice.getCustomerName();
        tvCustomer.setText(invoice.getCustomerName());

        mInvoiceDate = invoice.getInvoiceDate();
        tvInvoiceDate.setText(invoice.getInvoiceDate());

        mPlaceofSupplyId = invoice.getPlaceofSupplyId();
        tvPlaceOfSupply.setText(invoice.getPlaceofSupply());

        etFreightAmt.setText(invoice.getFreightAmount());

        addListItemByArrayList(invoice.getProductList());
        deletedProduct = new ArrayList<>();
    }


    public void startScan() {
        /**
         * Build a new MaterialBarcodeScanner
         */
        PermissionListener onPermissionListener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                addFragment(BarcodeScanner.newInstance(AddVouchers.this), "barcodeScanner");
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(getContext(), getString(R.string.permission_denied)+"\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }
        };
        new TedPermission(getActivity())
                .setPermissionListener(onPermissionListener)
                .setDeniedMessage(getString(R.string.if_you_reject_permission_you_can_not_use_this_service_please_turn_on_permissions))
                .setPermissions(Manifest.permission.CAMERA)
                .check();
//        MaterialBarcodeScanner materialBarcodeScanner = new MaterialBarcodeScannerBuilder()
//                .withActivity(getActivity())
//                .withEnableAutoFocus(true)
//                .withBleepEnabled(true)
//                .withBackfacingCamera()
//                .withCenterTracker()
//                .withText("Product Scanning...")
//                .withResultListener(listener)
//                .build();
//
//        materialBarcodeScanner.startScan();
    }

    public void fetchFromDB(final String mSearchBarcode) {
        ProductModel selectedProduct = dbManager.filterProductByBarcode(AppUser.getUserId(getActivity()), mSearchBarcode);
        if (selectedProduct.getOutputDB().equals(Const.SUCCESS)) {
            mSearchBarcodeProduct = null;
            tvAddNote.setVisibility(View.GONE);
            boolean isQunatityIncreased = false;
            ArrayList<ProductModel> tempList = selectedProduct.getList();
            for (ProductModel model : tempList) {
                if (model.getQuantity() == null) {
                    model.setQuantity("1");
//                    cList.add(model);
//                    adapter.notifyDataSetChanged();
//                    lvMain.smoothScrollToPosition(adapter.getItemCount());
//                    addTotalItemOnFooter();
//                    return;
                }
                for (int i = 0; i < cList.size(); i++) {
                    if (model.getBarcode().equalsIgnoreCase(cList.get(i).getBarcode())) {
                        isQunatityIncreased = true;
                        ProductModel proModel = cList.get(i);
                        int res = parseInt(proModel.getQuantity()) + parseInt(model.getQuantity());
                        proModel.setQuantity(String.valueOf(res));
                        cList.set(i, proModel);
                    }
                }
                if (!isQunatityIncreased) {
                    cList.add(model);
                }
            }
            adapter.notifyDataSetChanged();
            lvMain.smoothScrollToPosition(adapter.getItemCount());
            addTotalItemOnFooter();
        } else {
            this.mSearchBarcodeProduct = new ProductModel();
            this.mSearchBarcodeProduct.setBarcode(mSearchBarcode);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onCustomerSelect(DialogFragment dialog, CustomerModel cust) {
        mCustomerId = cust.getCustometId();
        mCustomerName = cust.getPersonName();
        tvCustomer.setText(cust.getPersonName());
        mShipName = cust.getPersonName();
        mShipGSTIN = cust.getGSTIN();
        mShipAddress = cust.getAddress();
    }


    private void addListItemByModel(ProductModel selectedProduct) {
        if (selectedProduct.getQuantity() == null) {
            selectedProduct.setQuantity("1");
//            cList.add(selectedProduct);
//            adapter.notifyDataSetChanged();
//            lvMain.smoothScrollToPosition(adapter.getItemCount());
//            addTotalItemOnFooter();
//            return;
        }
        boolean isQunatityIncreased = false;
        for (int i = 0; i < cList.size(); i++) {
            if (selectedProduct.getBarcode().equalsIgnoreCase(cList.get(i).getBarcode())) {
                isQunatityIncreased = true;
                ProductModel proModel = cList.get(i);
                int res = parseInt(proModel.getQuantity()) + parseInt(selectedProduct.getQuantity());
                proModel.setQuantity(String.valueOf(res));
                cList.set(i, proModel);
            }
        }
        if (!isQunatityIncreased) {
            cList.add(selectedProduct);
        }
        adapter.notifyDataSetChanged();
        lvMain.smoothScrollToPosition(adapter.getItemCount());
        addTotalItemOnFooter();
    }

    private void addListItemByArrayList(ArrayList<ProductModel> prodList) {
        if (prodList != null) {
            cList.clear();
            cList.addAll(prodList);
        }
        adapter.notifyDataSetChanged();
        lvMain.smoothScrollToPosition(adapter.getItemCount());

        addTotalItemOnFooter();
    }

    public void refreshAdapter() {
        adapter.notifyDataSetChanged();
        lvMain.smoothScrollToPosition(adapter.getItemCount());
        addTotalItemOnFooter();
    }

    private void addTotalItemOnFooter() {
        int totalQuan = 0, totalAmt = 0;
        for (int i = 0; i < cList.size(); i++) {
            int quantity = parseInt(cList.get(i).getQuantity());
            int amt = parseInt(cList.get(i).getSalePrice());

            totalQuan += quantity;
            totalAmt += quantity * amt;
        }
        addTotalItemOnUI(totalQuan, totalAmt);
    }


    private void addTotalItemOnUI(int quantity, int amount) {
        llTotalAmount.setVisibility(View.VISIBLE);
        tvTitle.setText(getString(R.string.total));
        mTotalAmt = amount;
        mTotalQuan = quantity;
        tvQuantity.setText(String.valueOf(quantity));
        tvAmount.setText(String.valueOf(amount));
    }

    private void removeTotalItemFromUI() {
        llTotalAmount.setVisibility(View.GONE);
        tvTitle.setText(getString(R.string.total));
        tvQuantity.setText("0");
        tvAmount.setText("0");
    }

    public void editProduct(ProductModel itemProduct, int position) {

    }

    public void deleteProduct(ProductModel itemProduct, int position) {
        if (deletedProduct != null) {
            deletedProduct.add(itemProduct.getProductId());
        }
        if (cList.size() == 0) {
            tvAddNote.setVisibility(View.VISIBLE);
            removeTotalItemFromUI();
            return;
        }
        addTotalItemOnFooter();
    }

    public void viewProduct(ProductModel itemProduct) {

    }

    public void quantityProduct(ProductModel itemProduct) {

    }

    @Override
    public void onStartDateSelected(DialogFragment dialog, int day, int month, int year, String ddMMMyy) {
        sDay = day;
        sMonth = month;
        sYear = year;
        tvDate.setText(ddMMMyy);
    }

    @Override
    public void onEndDateSelected(DialogFragment dialog, int day, int month, int year, String ddMMMyy) {
        eDay = day;
        eMonth = month;
        eYear = year;
        mInvoiceDate = ddMMMyy;
        tvInvoiceDate.setText(ddMMMyy);
    }


    @Override
    public void onDialogOkClick(DialogFragment dialog, int task) {
        switch (task) {
            case REQUEST_PRODUCT_NOT_FOUND:
                addFragment(AddProduct.newInstance(mSearchBarcodeProduct, AddProduct.ADD_PRODUCT_WITH_RESPONSE, AddVouchers.this), "addProduct");
                break;
        }
    }

    @Override
    public void onDialogCancelClick(DialogFragment dialog, int task) {
        mSearchBarcodeProduct = null;
    }

    public void fetchVendorFromDB(String personName, String companyName) {
        CustomerModel customerModel = dbManager.getCustomerIdByCustomerAndCompanyName(AppUser.getUserId(getActivity()), personName, companyName);
        if (customerModel.getOutputDB().equals(Const.SUCCESS)) {
            mCustomerId = customerModel.getCustometId();
            mCustomerName = customerModel.getPersonName();
            tvCustomer.setText(customerModel.getPersonName());
        } else {
            showSnakBar(coardLayout, customerModel.getOutputDBMsg());
        }
    }


    @Override
    public void onErrorRetryClick(DialogFragment dialog, String[] param) {
        executeTask(param[0], param[1], param[2], param[3], param[4], param[5], param[6], param[7], param[8], param[9], param[10], param[11]
                , param[12], param[13], param[14], param[15], param[16], param[17], cList);
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog, String[] param) {

    }

    @Override
    public void onStateSelect(DialogFragment dialog, StateModel selectedState) {
        mPlaceofSupplyId = selectedState.getStateCode();
        tvPlaceOfSupply.setText(selectedState.getStateName());
    }


    @Override
    public void onUpdateShippingDetailClick(DialogFragment dialog, String name, String GSTIN, String address, String shipStateCode, String shipStateName) {
        this.mShipName = name;
        this.mShipGSTIN = GSTIN;
        this.mShipAddress = address;
        this.mPlaceofSupplyId=shipStateCode;
        tvPlaceOfSupply.setText(shipStateName);
    }


    @Override
    public void onNoteTypeSelect(DialogFragment dialog, CommonModel selectedItem) {
        mNoteType = selectedItem.getName();
        mNoteTypeId = selectedItem.getId();
        tvNoteType.setText(selectedItem.getName());
    }

    @Override
    public void onNoteReasonSelect(DialogFragment dialog, CommonModel selectedItem) {
        mNoteReason = selectedItem.getName();
        mNoteReasonId = selectedItem.getId();
        tvReason.setText(selectedItem.getName());
    }

    @Override
    public void onTaxSelect(DialogFragment dialog, CommonModel selectedItem) {
        tvTaxRate.setText(selectedItem.getName());
    }

    @Override
    public void onProductSelect(DialogFragment dialog, ProductModel selectedProduct, ArrayList<ProductModel> productList) {
        mProductList=productList;
        mSearchBarcodeProduct = null;
        tvAddNote.setVisibility(View.GONE);
        addListItemByModel(selectedProduct);
    }



    private class AssignTask extends AsyncTask<String, Void, Model> {
        private final String voucherNo, voucherDate, noteType, noteTypeId, noteReason, noteReasonId, customerId, customerName, invoiceNo, freightAmt, invoiceDate, preGST, placeOfSupply, placeofSupplyId, noteValue, taxRate, taxableValue, cessAmt;
        private final ArrayList<ProductModel> productList;
        private Context context;

        public AssignTask(Context context, String voucherNo, String voucherDate, String noteType
                , String noteTypeId, String noteReason, String noteReasonId, String customerId
                , String customerName, String invoiceNo, String freightAmt, String invoiceDate
                , String preGST, String placeOfSupply, String placeofSupplyId, String noteValue
                , String taxRate, String taxableValue, String cessAmt, ArrayList<ProductModel> productList) {
            this.voucherNo = voucherNo;
            this.voucherDate = voucherDate;
            this.noteType = noteType;
            this.noteTypeId = noteTypeId;
            this.noteReason = noteReason;
            this.noteReasonId = noteReasonId;
            this.customerId = customerId;
            this.customerName = customerName;
            this.invoiceNo = invoiceNo;
            this.freightAmt = freightAmt;
            this.invoiceDate = invoiceDate;
            this.preGST = preGST;
            this.placeOfSupply = placeOfSupply;
            this.placeofSupplyId = placeofSupplyId;
            this.noteValue = noteValue;
            this.taxRate = taxRate;
            this.taxableValue = taxableValue;
            this.cessAmt = cessAmt;
            this.productList = productList;
            this.context = context;
        }

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            btnAction.startAnimation();
        }

        @Override
        protected Model doInBackground(String... urls) {
            boolean isIGSTApplicable;
            String companyStateCode;
            String customerStateCode;
            Model result = new Model();
            String userId = AppUser.getUserId(context);
            String profileId = AppUser.getProfileId(context);
            String companyId = AppUser.getCompanyId(context);

            if (AppConfig.isUpdateToServer(context)) {
                String productJson = "";
                String response = ApiCall.POST(urls[0], RequestBuilder.addVoucher(userId, profileId, companyId, voucherNo, voucherDate, noteType, noteTypeId, noteReason, noteReasonId
                        , customerId, customerName, invoiceNo, freightAmt, invoiceDate, preGST, placeOfSupply
                        , placeofSupplyId, noteValue, taxRate, taxableValue, cessAmt, productJson));
                result = JsonParser.defaultStaticParser(response);
            }
            switch (sellingType) {
                case ADD_VOUCHER:
                    companyStateCode = dbManager.getSelectedCompanyStateCode(userId, profileId, companyId);
                    customerStateCode = dbManager.getSelectedCustomerStateCode(userId, customerId);
                    if (companyStateCode.equalsIgnoreCase(customerStateCode)) {
                        isIGSTApplicable = false;
                    } else {
                        isIGSTApplicable = true;
                    }
                    return dbManager.addVoucher(result, userId, profileId, companyId, voucherNo, voucherDate, noteType, noteTypeId, noteReason, noteReasonId, customerId, customerName, invoiceNo, freightAmt, invoiceDate, preGST, placeOfSupply
                            , placeofSupplyId, noteValue, taxRate, taxableValue, cessAmt, productList, isIGSTApplicable);
                case EDIT_VOUCHER:
                    companyStateCode = dbManager.getSelectedCompanyStateCode(itemVoucher.getVoucherUserId(), itemVoucher.getVoucherProfileId(), itemVoucher.getVoucherCompanyId());
                    customerStateCode = dbManager.getSelectedCustomerStateCode(userId, customerId);
                    if (companyStateCode.equalsIgnoreCase(customerStateCode)) {
                        isIGSTApplicable = false;
                    } else {
                        isIGSTApplicable = true;
                    }
                    return dbManager.editVoucher(result, itemVoucher.getVoucherId(), deletedProduct, itemVoucher.getVoucherUserId(), itemVoucher.getVoucherProfileId(), itemVoucher.getVoucherCompanyId(), voucherNo, voucherDate, noteType, noteTypeId, noteReason, noteReasonId, customerId, customerName, invoiceNo, freightAmt, invoiceDate, preGST, placeOfSupply
                            , placeofSupplyId, noteValue, taxRate, taxableValue, cessAmt, productList, isIGSTApplicable);
                case COPY_VOUCHER:
                    companyStateCode = dbManager.getSelectedCompanyStateCode(itemVoucher.getVoucherUserId(), itemVoucher.getVoucherProfileId(), itemVoucher.getVoucherCompanyId());
                    customerStateCode = dbManager.getSelectedCustomerStateCode(itemVoucher.getVoucherUserId(), customerId);
                    if (companyStateCode.equalsIgnoreCase(customerStateCode)) {
                        isIGSTApplicable = false;
                    } else {
                        isIGSTApplicable = true;
                    }
                    return dbManager.copyVoucher(result, userId, profileId, companyId, voucherNo, voucherDate, noteType, noteTypeId, noteReason, noteReasonId, customerId, customerName, invoiceNo, freightAmt, invoiceDate, preGST, placeOfSupply
                            , placeofSupplyId, noteValue, taxRate, taxableValue, cessAmt, productList, isIGSTApplicable);
                default:
                    companyStateCode = dbManager.getSelectedCompanyStateCode(userId, profileId, companyId);
                    customerStateCode = dbManager.getSelectedCustomerStateCode(userId, customerId);
                    if (companyStateCode.equalsIgnoreCase(customerStateCode)) {
                        isIGSTApplicable = false;
                    } else {
                        isIGSTApplicable = true;
                    }
                    return dbManager.addVoucher(result, userId, profileId, companyId, voucherNo, voucherDate, noteType, noteTypeId, noteReason, noteReasonId, customerId, customerName, invoiceNo, freightAmt, invoiceDate, preGST, placeOfSupply
                            , placeofSupplyId, noteValue, taxRate, taxableValue, cessAmt, productList, isIGSTApplicable);
            }

        }


        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                if (!AppConfig.isUpdateToServer(context)) {
                    if (result.getOutputDB().equals(Const.SUCCESS)) {
                        showSnakBar(coardLayout, result.getOutputDBMsg());
                        if (vouchers != null) {
                            vouchers.refreshList();
                        }
                        animateButtonAndRevert();
                    } else if (result.getOutputDB().equals(Const.FAILURE)) {
                        btnAction.revertAnimation();
                        showSnakBar(coardLayout, result.getOutputDBMsg());
                    }
                } else {
                    if (result.getOutput().equals(Const.SUCCESS)) {
                        if (!result.getOutputDB().equals(Const.SUCCESS)) {
                            showSnakBar(coardLayout, result.getOutputDBMsg());
                        }
                        showSnakBar(coardLayout, result.getOutputMsg());
                        if (vouchers != null) {
                            vouchers.refreshList();
                        }
                        animateButtonAndRevert();
                    } else if (result.getOutput().equals(Const.FAILURE)) {
                        if (result.getOutputDB().equals(Const.SUCCESS)) {
                            showSnakBar(coardLayout, "Local " + result.getOutputDBMsg());
                        }
                        btnAction.revertAnimation();
                        PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(), "popupAlert");
                    } else {
                        if (result.getOutputDB().equals(Const.SUCCESS)) {
                            showSnakBar(coardLayout, "Local " + result.getOutputDBMsg());
                        }
                        btnAction.revertAnimation();
                        String[] errorSoon = {voucherNo, voucherDate, noteType, noteTypeId, noteReason, noteReasonId
                                , customerId, customerName, invoiceNo, freightAmt, invoiceDate, preGST, placeOfSupply
                                , placeofSupplyId, noteValue, taxRate, taxableValue, cessAmt};
                        ApiCallError.newInstance(result.getOutput(),result.getOutputMsg(), errorSoon, AddVouchers.this)
                                .show(getFragmentManager(), "apiCallError");
                    }
                }
            }
        }

        private void animateButtonAndRevert() {
            Handler handler = new Handler();


            Runnable runnableRevert = new Runnable() {
                @Override
                public void run() {
                    if (context != null) {
                        getActivity().onBackPressed();
                    }
                }
            };

            btnAction.revertSuccessAnimation();
            handler.postDelayed(runnableRevert, Const.BUTTON_PROGRESS_TIME);
        }

    }
}
