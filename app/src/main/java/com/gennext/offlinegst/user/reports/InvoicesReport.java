package com.gennext.offlinegst.user.reports;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.PopupAlert;
import com.gennext.offlinegst.model.user.InvoiceRepAdapter;
import com.gennext.offlinegst.model.user.PaymentModel;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.user.Dashboard;
import com.gennext.offlinegst.user.invoice.DebtorsTransaction;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.GravitySnapHelper;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.RequestBuilder;

import java.util.ArrayList;

/**
 * Created by Admin on 7/4/2017.
 */

public class InvoicesReport extends CompactFragment {

    private ProgressBar pBar;
    private Button btnMore;

    private AssignTask assignTask;
    private DBManager dbManager;
    private RecyclerView rvMain;
    private ArrayList<PaymentModel> mList;
    private InvoiceRepAdapter adapter;
    private Dashboard dashboard;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static Fragment newInstance(Dashboard dashboard) {
        InvoicesReport fragment = new InvoicesReport();
        AppAnimation.setFadeAnimation(fragment);
        fragment.dashboard=dashboard;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_reports_invoice, container, false);
        dbManager = DBManager.newIsntance(getActivity());
        screenAnalytics(getContext(), AppUser.getUserId(getContext()), "InvoicesReport");
        initUi(v);
        executeTask();
        return v;
    }

    private void initUi(View v) {
        pBar = (ProgressBar) v.findViewById(R.id.progressBar);
        btnMore = (Button) v.findViewById(R.id.btn_more);
        btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInvoicesReport();
            }
        });
        rvMain = (RecyclerView) v.findViewById(R.id.rv_main);

        SnapHelper snapHelper = new GravitySnapHelper(Gravity.START);
        snapHelper.attachToRecyclerView(rvMain);


        // HORIZONTAL for Gravity START/END and VERTICAL for TOP/BOTTOM
        rvMain.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvMain.setHasFixedSize(true);
        ViewCompat.setNestedScrollingEnabled(rvMain, false);
        mList = new ArrayList<>();
        adapter = new InvoiceRepAdapter(getActivity(), mList, InvoicesReport.this, getFragmentManager());
        rvMain.setAdapter(adapter);
    }

    private void openInvoicesReport() {
        addFragment(DebtorsTransaction.newInstance(InvoicesReport.this),"debtorsTransaction");
    }

    private void executeTask() {
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.GET_PRODUCTS);
    }

    public void openReportDetail(PaymentModel item) {
        addFragment(ReportDetail.newInstance(item,ReportDetail.SELLER),"reportDetail");
    }


    private class AssignTask extends AsyncTask<String, Void, PaymentModel> {
        private Context context;

        private void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        private void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        private AssignTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected PaymentModel doInBackground(String... urls) {
            String userId = AppUser.getUserId(context);
            String profileId = AppUser.getProfileId(context);
            PaymentModel result = new PaymentModel();
            if (AppConfig.isUpdateToServer(context)) {
                String response = ApiCall.POST(urls[0], RequestBuilder.Default(userId));
                result = JsonParser.getDebtorsDetail(response);
            }
            return dbManager.getAllDebtorsListWithStock(result, userId, profileId);
        }


        @Override
        protected void onPostExecute(PaymentModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                pBar.setVisibility(View.GONE);
                if (!AppConfig.isUpdateToServer(context)) {
                    if (result.getOutputDB().equals(Const.SUCCESS)) {
                        mList.addAll(result.getList());
                        adapter.notifyDataSetChanged();
                    } else if (result.getOutputDB().equals(Const.FAILURE)) {
                        btnMore.setText(R.string.not_found);
                        dashboard.hideInvoiceReport();
                    }
                } else {
                    if (result.getOutput().equals(Const.SUCCESS)) {
                        if (!result.getOutputDB().equals(Const.SUCCESS)) {
                            showToast(result.getOutputDBMsg());
                        }
                        mList.addAll(result.getList());
                        adapter.notifyDataSetChanged();
                    } else if (result.getOutput().equals(Const.FAILURE)) {
                        if (result.getOutputDB().equals(Const.SUCCESS)) {
                            showToast("Local " + result.getOutputDBMsg());
                        }
                        btnMore.setText(getString(R.string.not_found));
                        dashboard.hideInvoiceReport();
                        PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(), "popupAlert");
                    } else {
                        if (result.getOutputDB().equals(Const.SUCCESS)) {
                            showToast("Local " + result.getOutputDBMsg());
                        }
                        showToast("Local " + result.getOutputMsg());
                    }
                }
            }
        }
    }
}
