package com.gennext.offlinegst.model.user;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.setting.DeleteSetting;

import java.util.ArrayList;

public class SettingAdapter extends RecyclerView.Adapter<SettingAdapter.ReyclerViewHolder> {

    private DeleteSetting parentRef;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<SettingModel> items;

    public SettingAdapter(Activity context, ArrayList<SettingModel> items, DeleteSetting parentRef) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef=parentRef;
        this.items = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_setting_delete, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final SettingModel item = items.get(position);

        holder.tvTitle.setText(item.getTitle());

        holder.ibAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//               shopListViewActivity.addFragment(ShopDetail.newInstance(context,item,mCurrentLocation),"shopDetail");
                parentRef.onSelectedItem(item);
            }
        });
    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private final ImageButton ibAction;
        private LinearLayout llSlot;
        private TextView tvTitle;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSlot = (LinearLayout) v.findViewById(R.id.slot_main);
            tvTitle = (TextView) v.findViewById(R.id.tv_title);
            ibAction = (ImageButton) v.findViewById(R.id.ib_action);
        }
    }
}

