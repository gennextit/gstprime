package com.gennext.offlinegst.user.purchas;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.PopupAlert;
import com.gennext.offlinegst.model.app.Model;
import com.gennext.offlinegst.model.user.PaymentModel;
import com.gennext.offlinegst.model.user.VendorModel;
import com.gennext.offlinegst.panel.VendorSelector;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.user.Dashboard;
import com.gennext.offlinegst.user.reports.PaymentReport;
import com.gennext.offlinegst.user.reports.PurchaseReport;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.ApiCallError;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.FieldValidation;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.ProgressButtonRounded;
import com.gennext.offlinegst.util.RequestBuilder;

import java.text.DecimalFormat;

/**
 * Created by Admin on 7/4/2017.
 */

public class CreditorsTransaction extends CompactFragment implements ApiCallError.ErrorParamListener,VendorSelector.SelectListener {

    private static final String CASH_MODE = "cash", CHEQUE_MODE = "cheque";
    private static final int RECEIVE_TYPE = 0, RETURN_TYPE = 1;
    private EditText etBankName, etChequeNo, etAccName, etIFSC, etAccNumber, etBranchAddress;
    private LinearLayout llChequeOption;
    private EditText etAmtType, etPaymentReceived, etBalance;
    private TextView tvAmtType, tvBalance;
    private float mPreviousBaclance;
    private DBManager dbManager;
    private float mBalance;
    private ProgressButtonRounded btnAction;

    private AssignTask assignTask;
    private Creditors creditors;
    private CoordinatorLayout coardLayout;
    private String mPaymentMode;
    private String mOpeningBalance;
    private String mVendorId;
    private String mVendorName;
    private int mTransType;
    private TextView tvPaymentReceived;
    private EditText etVendor;
    private RadioButton rbTransReturn;
    private PaymentReport paymentReport;
    private PurchaseReport purchaseReport;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static Fragment newInstance(Creditors creditors) {
        CreditorsTransaction fragment = new CreditorsTransaction();
        AppAnimation.setSlideAnimation(fragment, Gravity.BOTTOM);
        fragment.creditors = creditors;
        return fragment;
    }

    public static Fragment newInstance(PaymentReport paymentReport) {
        CreditorsTransaction fragment = new CreditorsTransaction();
        AppAnimation.setSlideAnimation(fragment, Gravity.BOTTOM);
        fragment.paymentReport = paymentReport;
        return fragment;
    }

     public static Fragment newInstance(PurchaseReport purchaseReport) {
        CreditorsTransaction fragment = new CreditorsTransaction();
        AppAnimation.setSlideAnimation(fragment, Gravity.BOTTOM);
        fragment.purchaseReport = purchaseReport;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_creditors_trans, container, false);
        initToolBar(getActivity(), v, getString(R.string.payment_transaction));
        dbManager = DBManager.newIsntance(getActivity());
        screenAnalytics(getContext(), AppUser.getUserId(getContext()), "Payment Transaction");
        mPaymentMode = CASH_MODE;
        mTransType = RECEIVE_TYPE;
        initUi(v);
        return v;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        dbManager.closeDB();
    }
    // (+ amount)=Borrow amount
    // (- amount)=Advance amount
    private void updateUi(String vendorId) {
        String result[];
        result = dbManager.getVendorBalanceAmount(AppUser.getUserId(getContext()), AppUser.getProfileId(getContext()), vendorId);

        mOpeningBalance = result[0];
        mPreviousBaclance = convertToFloat(result[0]);
        if (mPreviousBaclance < 0) {
            tvAmtType.setText(getString(R.string.advance_balance_of_vendor));
            etAmtType.setText(String.valueOf(mPreviousBaclance).replace("-", ""));

            etBalance.setText(validateFloat(mPreviousBaclance).replace("-", ""));
            tvBalance.setText(getString(R.string.advance_payment_of_vendor));
            mBalance = mPreviousBaclance;
            rbTransReturn.setVisibility(View.VISIBLE);
        } else {
            tvAmtType.setText(getString(R.string.borrow_amount));
            etAmtType.setText(validateFloat(mPreviousBaclance));

            etBalance.setText(validateFloat(mPreviousBaclance));
            tvBalance.setText(R.string.credit_balance_of_vendor);
            mBalance = 0;
            rbTransReturn.setVisibility(View.GONE);
        }

        float paymentReceived = convertToFloat(etPaymentReceived.getText().toString());
        calculateBalanceAmount(paymentReceived);
    }

    private void calculateBalanceAmount(float paymentReceived) {
        if(mTransType==RECEIVE_TYPE) {
            mBalance = convertToFloat(validateFloat(mPreviousBaclance - paymentReceived));
        }else{
            mBalance = convertToFloat(validateFloat(mPreviousBaclance + paymentReceived));
        }
        if (mBalance < 0) {
            etBalance.setText(String.valueOf(mBalance).replace("-", ""));
            tvBalance.setText(getString(R.string.advance_payment_of_vendor));
        } else {
            etBalance.setText(String.valueOf(mBalance));
            tvBalance.setText(getString(R.string.credit_balance_of_vendor));
        }
    }

    private PaymentModel setPaymentDetail() {
        PaymentModel model = new PaymentModel();
        String invoiceDetail;
        if(mTransType==RECEIVE_TYPE){
            invoiceDetail=Const.RECEIVE;
        }else{
            invoiceDetail=Const.RETURN;
        }
        model.setVendorAllFields(invoiceDetail, mVendorId, mVendorName, etPaymentReceived.getText().toString()
                , "0", String.valueOf(mBalance), mPaymentMode, etBankName.getText().toString()
                , etChequeNo.getText().toString(), etAccName.getText().toString(), etIFSC.getText().toString()
                , etAccNumber.getText().toString(), etBranchAddress.getText().toString(), mOpeningBalance, etPaymentReceived.getText().toString());
        return model;
    }

    private void initUi(View v) {
        coardLayout = (CoordinatorLayout) v.findViewById(R.id.coordinatorLayout);
        btnAction = ProgressButtonRounded.newInstance(getContext(),v);
        tvAmtType = (TextView) v.findViewById(R.id.tv_payment_amt_type);
        etAmtType = (EditText) v.findViewById(R.id.et_payment_amt_type);
        etPaymentReceived = (EditText) v.findViewById(R.id.et_payment_cash_received);
        tvPaymentReceived = (TextView) v.findViewById(R.id.tv_payment_cash_received);
        etBalance = (EditText) v.findViewById(R.id.et_payment_balance);
        etVendor = (EditText) v.findViewById(R.id.et_payment_customer);
        tvBalance = (TextView) v.findViewById(R.id.tv_payment_balance);

        llChequeOption = (LinearLayout) v.findViewById(R.id.ll_cheque_option);
        etBankName = (EditText) v.findViewById(R.id.et_bank_account_bank);
        etChequeNo = (EditText) v.findViewById(R.id.et_bank_account_cheque);
        etAccName = (EditText) v.findViewById(R.id.et_bank_account_name);
        etIFSC = (EditText) v.findViewById(R.id.et_bank_ifsc);
        etAccNumber = (EditText) v.findViewById(R.id.et_bank_acc_number);
        etBranchAddress = (EditText) v.findViewById(R.id.et_bank_branch_address);

        rbTransReturn = (RadioButton) v.findViewById(R.id.type_return);
        RadioGroup rgTransType = (RadioGroup) v.findViewById(R.id.trans_type);
        RadioGroup rgPaymentType = (RadioGroup) v.findViewById(R.id.payment_type);
        rgPaymentType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.type_cash:
                        mPaymentMode = CASH_MODE;
                        if (convertToFloat(etPaymentReceived.getText().toString()) == 0) {
                            etPaymentReceived.setText("");
                        }
                        etPaymentReceived.setFocusable(true);
                        etPaymentReceived.setFocusableInTouchMode(true);
                        etPaymentReceived.setClickable(false);
                        etPaymentReceived.setBackgroundResource(R.drawable.et_multi_row);
                        AppAnimation.slideUp(llChequeOption);
                        break;
                    case R.id.type_cheque:
                        mPaymentMode = CHEQUE_MODE;
                        if (convertToFloat(etPaymentReceived.getText().toString()) == 0) {
                            etPaymentReceived.setText("");
                        }
                        etPaymentReceived.setFocusable(true);
                        etPaymentReceived.setFocusableInTouchMode(true);
                        etPaymentReceived.setClickable(false);
                        etPaymentReceived.setBackgroundResource(R.drawable.et_multi_row);
                        AppAnimation.slideDown(llChequeOption);
                        break;

                }
            }
        });

        rgTransType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                float paymentReceived;
                switch (checkedId) {
                    case R.id.type_receive:
                        mTransType = RECEIVE_TYPE;
                        tvPaymentReceived.setText(getString(R.string.cash_received));
                        paymentReceived = convertToFloat(etPaymentReceived.getText().toString());
                        calculateBalanceAmount(paymentReceived);
                        break;
                    case R.id.type_return:
                        mTransType = RETURN_TYPE;
                        tvPaymentReceived.setText(getString(R.string.cash_return));
                        paymentReceived = convertToFloat(etPaymentReceived.getText().toString());
                        calculateBalanceAmount(paymentReceived);
                        break;

                }
            }
        });

        etPaymentReceived.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                float paymentReceived = convertToFloat(etPaymentReceived.getText().toString());
                calculateBalanceAmount(paymentReceived);
            }
        });

        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPaymentMode.equalsIgnoreCase(CASH_MODE)) {
                    if (!FieldValidation.isEmpty(getContext(), etVendor, getString(R.string.invalid_vendor))) {
                        return;
                    }else if (!FieldValidation.isEmpty(getContext(), etPaymentReceived, getString(R.string.invalid_amount))) {
                        return;
                    }
                    hideKeybord(getActivity());
                    paymentAcceptedTask();
                } else if (mPaymentMode.equalsIgnoreCase(CHEQUE_MODE)) {
                    if (!FieldValidation.isEmpty(getContext(), etVendor, getString(R.string.invalid_vendor))) {
                        return;
                    }else if (!FieldValidation.isEmpty(getContext(), etPaymentReceived, getString(R.string.invalid_amount))) {
                        return;
                    } else if (!FieldValidation.isEmpty(getContext(), etBankName)) {
                        return;
                    } else if (!FieldValidation.isEmpty(getContext(), etChequeNo)) {
                        return;
                    } else if (!FieldValidation.isEmpty(getContext(), etAccName)) {
                        return;
                    } else if (!FieldValidation.isIFSCCode(getContext(), etIFSC, true)) {
                        return;
                    } else if (!FieldValidation.isEmpty(getContext(), etAccNumber)) {
                        return;
                    }
                    hideKeybord(getActivity());
                    paymentAcceptedTask();
                }
            }
        });

        etVendor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VendorSelector.newInstance(CreditorsTransaction.this).show(getFragmentManager(),"vendorSelector");
            }
        });

    }


    private float convertToFloat(String value) {
        try {
            return Float.parseFloat(value);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    private String validateFloat(float value) {
        return new DecimalFormat("#.##").format(value);
    }

    public void paymentAcceptedTask() {
        hideKeybord(getActivity());
        PaymentModel paymentDetail = setPaymentDetail();
        executeTask(paymentDetail);
    }


    private void executeTask(PaymentModel paymentDetail) {
        hideKeybord(getActivity());
        assignTask = new AssignTask(getActivity(), paymentDetail);
        assignTask.execute(AppSettings.ADD_INVOICE);
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog, String[] param) {
        PaymentModel paymentDetail = setPaymentDetail();
        executeTask(paymentDetail);
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog, String[] param) {

    }

    public void fetchVendorFromDB(String personName, String companyName) {
        VendorModel vendorModel = dbManager.getVendorIdByVendorAndCompanyName(AppUser.getUserId(getActivity()), personName, companyName);
        if (vendorModel.getOutputDB().equals(Const.SUCCESS)) {
            mVendorId = vendorModel.getVendorId();
            mVendorName = vendorModel.getPersonName();
            etVendor.setText(vendorModel.getPersonName());
            updateUi(mVendorId); 
        } else {
            showSnakBar(coardLayout, vendorModel.getOutputDBMsg());
        }
    }

    @Override
    public void onVendorSelect(DialogFragment dialog, VendorModel selectedVendor) {
        mVendorId = selectedVendor.getVendorId();
        mVendorName = selectedVendor.getPersonName();
        etVendor.setText(selectedVendor.getPersonName());
        updateUi(mVendorId);
    }

    private class AssignTask extends AsyncTask<String, Void, Model> {
        private final PaymentModel paymentDetail;
        private Context context;

        public AssignTask(Context context, PaymentModel paymentDetail) {
            this.paymentDetail = paymentDetail;
            this.context = context;
        }

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            btnAction.startAnimation();
        }

        @Override
        protected Model doInBackground(String... urls) {
            Model result = new Model();
            String userId = AppUser.getUserId(context);
            String profileId = AppUser.getProfileId(context);

            if (AppConfig.isUpdateToServer(context)) {
                String productJson = "";
                String response = ApiCall.POST(urls[0], RequestBuilder.Default(userId));
                result = JsonParser.defaultStaticParser(response);
            }
            return dbManager.setCreditorsByTrans(new Model(),userId, profileId, paymentDetail);
        }


        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                if (!AppConfig.isUpdateToServer(context)) {
                    if (result.getOutputDB().equals(Const.SUCCESS)) {
                        showSnakBar(coardLayout, result.getOutputDBMsg());
                        animateButtonAndRevert();
                    } else if (result.getOutputDB().equals(Const.FAILURE)) {
                        btnAction.revertAnimation();
                        showSnakBar(coardLayout, result.getOutputDBMsg());
                    }
                } else {
                    if (result.getOutput().equals(Const.SUCCESS)) {
                        if (!result.getOutputDB().equals(Const.SUCCESS)) {
                            showSnakBar(coardLayout, result.getOutputDBMsg());
                        }
                        showSnakBar(coardLayout, result.getOutputMsg());
                        animateButtonAndRevert();
                    } else if (result.getOutput().equals(Const.FAILURE)) {
                        if (result.getOutputDB().equals(Const.SUCCESS)) {
                            showSnakBar(coardLayout, "Local " + result.getOutputDBMsg());
                        }
                        btnAction.revertAnimation();
                        PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(), "popupAlert");
                    } else {
                        if (result.getOutputDB().equals(Const.SUCCESS)) {
                            showSnakBar(coardLayout, "Local " + result.getOutputDBMsg());
                        }
                        btnAction.revertAnimation();
                        String[] errorSoon = new String[1];
                        ApiCallError.newInstance(result.getOutput(),result.getOutputMsg(), errorSoon, CreditorsTransaction.this)
                                .show(getFragmentManager(), "apiCallError");
                    }
                }
            }
        }

        private void animateButtonAndRevert() {
            Handler handler = new Handler();

            Runnable runnableRevert = new Runnable() {
                @Override
                public void run() {
                    if (context != null) {
                        if (creditors != null) {
                            creditors.refreshList();
                        }
                        if (paymentReport != null) {
                            Dashboard dashboard= (Dashboard) getFragmentManager().findFragmentByTag("dashboard");
                            if(dashboard!=null){
                                dashboard.updateUi();
                            }
                        }
                        if (purchaseReport != null) {
                            Dashboard dashboard= (Dashboard) getFragmentManager().findFragmentByTag("dashboard");
                            if(dashboard!=null){
                                dashboard.updateUi();
                            }
                        }
                        getFragmentManager().popBackStack();
                    }
                }
            };

            btnAction.revertSuccessAnimation();
            handler.postDelayed(runnableRevert, Const.BUTTON_PROGRESS_TIME);
        }

    }

}
