package com.gennext.offlinegst.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Abhijit on 13-Dec-16.
 */

public class AppUser {
    public static final String COMMON = "ogst";
    private static final String APP_USER_ID = "appuserid" + COMMON;
    private static final String APP_USER_NAME = "app_user_name" + COMMON;
    private static final String APP_USER_IMAGE = "app_user_image" + COMMON;
    private static final String APP_BUSINESS_TYPE = "app_business_type" + COMMON;
    private static final String APP_PROFILE_ID = "appprofileid" + COMMON;
    private static final String APP_COMPANY_ID = "appCompid" + COMMON;
    private static final String SIGNATURE = "signature" + COMMON;
    private static final String APP_FCM_ID = "fcm" + COMMON;
    private static final String APP_EMAIL = "email" + COMMON;
    private static final String APP_LANGUAGE = "appLang" + COMMON;
    private static final String GLOBAL_USER_DETAILS = "UserDetails" + COMMON;
    private static final String GLOBAL_COMPANY_DETAILS = "CompanyDetails" + COMMON;
    private static final String GLOBAL_UNIT_QUANTITY_CODE = "UnitQuantityCode" + COMMON;
    private static final String GLOBAL_TAX_DETAILS = "TaxDetails" + COMMON;
    private static final String GLOBAL_TAX_CATEGORIES = "TaxCategories" + COMMON;
    private static final String GLOBAL_STATES_LIST = "StatesList" + COMMON;
    private static final String GLOBAL_TERMS = "Terms" + COMMON;
    private static final String GLOBAL_PURCHASE_TYPE = "PurchaseType" + COMMON;
    private static final String GLOBAL_PURCHASE_CATEGORY = "PurchaseCategory" + COMMON;
    private static final String GLOBAL_INVOICE_TYPES = "InvoiceTypes" + COMMON;
    private static final String GLOBAL_TRANSPOART_MODE = "transpoartMode" + COMMON;
    private static final String PACKAGE_STATUS = "packageStatus" + COMMON;
    private static final String PACKAGE_MESSAGS = "packageMessags" + COMMON;
    private static final String COMPOSITION_SCHEME = "compositionScheme" + COMMON;


    public static String LoadPref(Context context, String key) {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            String data = sharedPreferences.getString(key, "");
            return data;
        }
        return "";
    }

    public static String getName(Context act) {
        return LoadPref(act, APP_USER_NAME);
    }

    public static String getImage(Context act) {
        return LoadPref(act, APP_USER_IMAGE);
    }

    public static void setName(Context act, String name) {
        Utility.SavePref(act, APP_USER_NAME, name);
    }

    public static void setImage(Context act, String image) {
        Utility.SavePref(act, APP_USER_IMAGE, image);
    }

    public static String getBusinessType(Context act) {
        return LoadPref(act, APP_BUSINESS_TYPE);
    }


    public static void setBusinessType(Context act, String value) {
        Utility.SavePref(act, APP_BUSINESS_TYPE, value);
    }


    public static boolean getCompositionScheme(Context act) {
        String res = LoadPref(act, COMPOSITION_SCHEME);
        if (!res.equals("") && res.equals("Y")) {
            return true;
        } else {
            return false;
        }
    }


    public static void setCompositionScheme(Context act, String value) {
        Utility.SavePref(act, COMPOSITION_SCHEME, value);
    }


    public static void setUserId(Context act, String username) {
        Utility.SavePref(act, APP_USER_ID, username);
    }

    public static String getUserId(Context activity) {
        return LoadPref(activity, APP_USER_ID);
    }


    public static void setProfileId(Context context, String profileId) {
        Utility.SavePref(context, APP_PROFILE_ID, profileId);
    }

    public static String getProfileId(Context context) {
        return Utility.LoadPref(context, APP_PROFILE_ID);
    }

    public static void setCompanyId(Context context, String companyId) {
        Utility.SavePref(context, APP_COMPANY_ID, companyId);
    }

    public static String getCompanyId(Context context) {
        return Utility.LoadPref(context, APP_COMPANY_ID);
    }


    public static void setSignature(Context context, String signature) {
        if(signature==null){
            return;
        }
        Utility.SavePref(context, SIGNATURE, signature);
    }

    public static String getSignature(Context context) {
        return Utility.LoadPref(context, SIGNATURE);
    }

    public static void setPackageStatus(Context context, String companyId) {
        Utility.SavePref(context, PACKAGE_STATUS, companyId);
    }

    public static String getPackageStatus(Context context) {
        return Utility.LoadPref(context, PACKAGE_STATUS);
    }


    public static void setPackageMessage(Context context, String companyId) {
        Utility.SavePref(context, PACKAGE_MESSAGS, companyId);
    }

    public static String getPackageMessage(Context context) {
        return Utility.LoadPref(context, PACKAGE_MESSAGS);
    }


    public static void setFCMId(Context context, String token) {
        Utility.SavePref(context, APP_FCM_ID, token);
    }

    public static String getFCMId(Context context) {
        return Utility.LoadPref(context, APP_FCM_ID);
    }

    public static void setGmailId(Context context, String email) {
        Utility.SavePref(context, APP_EMAIL, email);
    }

    public static String getGmailId(Context context) {
        return Utility.LoadPref(context, APP_EMAIL);
    }

    public static void setLanguage(Context context, String value) {
        Utility.SavePref(context, APP_LANGUAGE, value);
    }

    public static String getLanguage(Context context) {
        return Utility.LoadPref(context, APP_LANGUAGE, LocaleHelper.LANGUAGE_ENGLISH);
    }

    public static void setUserDetails(Context context, String value) {
        Utility.SavePref(context, GLOBAL_USER_DETAILS, value);
    }

    public static void setCompanyDetails(Context context, String value) {
        Utility.SavePref(context, GLOBAL_COMPANY_DETAILS, value);
    }

    public static void setUnitQuantityCode(Context context, String value) {
        Utility.SavePref(context, GLOBAL_UNIT_QUANTITY_CODE, value);
    }

    public static void setTaxDetails(Context context, String value) {
        Utility.SavePref(context, GLOBAL_TAX_DETAILS, value);
    }

    public static void setTaxCategories(Context context, String value) {
        Utility.SavePref(context, GLOBAL_TAX_CATEGORIES, value);
    }

    public static void setTransportMode(Context context, String value) {
        Utility.SavePref(context, GLOBAL_TRANSPOART_MODE, value);
    }

    public static void setStatesList(Context context, String value) {
        Utility.SavePref(context, GLOBAL_STATES_LIST, value);
    }

    public static void setTerms(Context context, String value) {
        Utility.SavePref(context, GLOBAL_TERMS, value);
    }

    public static void setInvoiceTypes(Context context, String value) {
        Utility.SavePref(context, GLOBAL_INVOICE_TYPES, value);
    }

    public static void setPurchaseType(Context context, String value) {
        Utility.SavePref(context, GLOBAL_PURCHASE_TYPE, value);
    }

    public static void setPurchaseCategory(Context context, String value) {
        Utility.SavePref(context, GLOBAL_PURCHASE_CATEGORY, value);
    }

    public static String getUserDetails(Context context) {
        return Utility.LoadPref(context, GLOBAL_USER_DETAILS);
    }

    public static String getCompanyDetails(Context context) {
        return Utility.LoadPref(context, GLOBAL_COMPANY_DETAILS);
    }

    public static String getUnitQuantityCode(Context context) {
        return Utility.LoadPref(context, GLOBAL_UNIT_QUANTITY_CODE);
    }

    public static String getTaxDetails(Context context) {
        return Utility.LoadPref(context, GLOBAL_TAX_DETAILS);
    }

    public static String getTaxCategories(Context context) {
        return Utility.LoadPref(context, GLOBAL_TAX_CATEGORIES);
    }

    public static String getTransportMode(Context context) {
        return Utility.LoadPref(context, GLOBAL_TRANSPOART_MODE);
    }

    public static String getStatesList(Context context) {
        return Utility.LoadPref(context, GLOBAL_STATES_LIST);
    }

    public static String getTerms(Context context) {
        return Utility.LoadPref(context, GLOBAL_TERMS);
    }

    public static String getPurchaseType(Context context) {
        return Utility.LoadPref(context, GLOBAL_PURCHASE_TYPE);
    }

    public static String getPurchaseCategory(Context context) {
        return Utility.LoadPref(context, GLOBAL_PURCHASE_CATEGORY);
    }

    public static String getInvoiceTypes(Context context) {
        return Utility.LoadPref(context, GLOBAL_INVOICE_TYPES);
    }


    protected static String getUnitName(Map<String, String> mList, String inputKey) {
        String result = mList.get(inputKey);
        return result != null ? result : "";
    }

    protected static Map<String, String> defaultUnitCodes(Context context) {
        Map<String, String> reverseMap = new HashMap<>();
        if (context != null) {
            try {
                JSONArray stateArray = new JSONArray(getUnitQuantityCode(context));
                for (int i = 0; i < stateArray.length(); i++) {
                    JSONObject stateObj = stateArray.getJSONObject(i);
                    reverseMap.put(stateObj.optString("code"), stateObj.optString("name"));
                }
            } catch (JSONException e) {
                return reverseMap;
            }
        }
        return reverseMap;
    }

    protected static String getTaxName(Map<String, String> mList, String inputKey) {
        String result = mList.get(inputKey);
        return result != null ? result : "";
    }

    protected static Map<String, String> defaultTaxRates(Context context) {
        Map<String, String> reverseMap = new HashMap<>();
        if (context != null) {
            try {
                JSONArray stateArray = new JSONArray(getTaxDetails(context));
                for (int i = 0; i < stateArray.length(); i++) {
                    JSONObject stateObj = stateArray.getJSONObject(i);
                    reverseMap.put(stateObj.optString("percent"), stateObj.optString("name"));
                }
            } catch (JSONException e) {
                return reverseMap;
            }
        }
        return reverseMap;
    }

    public static String getStateName(Map<String, String> mList, String inputKey) {
        if (mList == null || inputKey == null) {
            return "";
        }
        String result = mList.get(inputKey);
        return result != null ? result : "";
    }

    public static Map<String, String> defaultStates(Context context) {
        Map<String, String> reverseMap = new HashMap<>();
        if (context != null) {
            try {
                JSONArray stateArray = new JSONArray(getStatesList(context));
                for (int i = 0; i < stateArray.length(); i++) {
                    JSONObject stateObj = stateArray.getJSONObject(i);
                    reverseMap.put(stateObj.optString("StateCode"), stateObj.optString("StateName"));
                }
            } catch (JSONException e) {
                return reverseMap;
            }
        }
        return reverseMap;
    }

    protected static String getTermName(Map<String, String> mList, String inputKey) {
        String result = mList.get(inputKey);
        return result != null ? result : "";
    }

    protected static Map<String, String> defaultTerms(Context context) {
        Map<String, String> reverseMap = new HashMap<>();
        if (context != null) {
            try {
                JSONArray stateArray = new JSONArray(getTerms(context));
                for (int i = 0; i < stateArray.length(); i++) {
                    JSONObject stateObj = stateArray.getJSONObject(i);
                    reverseMap.put(stateObj.optString("termId"), stateObj.optString("termName"));
                }
            } catch (JSONException e) {
                return reverseMap;
            }
        }
        return reverseMap;
    }

    protected static String getInvoiceTypesName(Map<String, String> mList, String inputKey) {
        String result = mList.get(inputKey);
        return result != null ? result : "";
    }

    protected static Map<String, String> defaultInvoiceTypes(Context context) {
        Map<String, String> reverseMap = new HashMap<>();
        if (context != null) {
            try {
                JSONArray stateArray = new JSONArray(getInvoiceTypes(context));
                for (int i = 0; i < stateArray.length(); i++) {
                    JSONObject stateObj = stateArray.getJSONObject(i);
                    reverseMap.put(stateObj.optString("id"), stateObj.optString("typeName"));
                }
            } catch (JSONException e) {
                return reverseMap;
            }
        }
        return reverseMap;
    }

    protected static String getPurchaseCategoryName(Map<String, String> mList, String inputKey) {
        String result = mList.get(inputKey);
        return result != null ? result : "";
    }

    protected static Map<String, String> defaultPurchaseCategory(Context context) {
        Map<String, String> reverseMap = new HashMap<>();
        if (context != null) {
            try {
                JSONArray stateArray = new JSONArray(getPurchaseCategory(context));
                for (int i = 0; i < stateArray.length(); i++) {
                    JSONObject stateObj = stateArray.getJSONObject(i);
                    reverseMap.put(stateObj.optString("id"), stateObj.optString("categoryName"));
                }
            } catch (JSONException e) {
                return reverseMap;
            }
        }
        return reverseMap;
    }

    protected static String getPurchaseTypeName(Map<String, String> mList, String inputKey) {
        String result = mList.get(inputKey);
        return result != null ? result : "";
    }

    protected static Map<String, String> defaultPurchaseType(Context context) {
        Map<String, String> reverseMap = new HashMap<>();
        if (context != null) {
            try {
                JSONArray stateArray = new JSONArray(getPurchaseType(context));
                for (int i = 0; i < stateArray.length(); i++) {
                    JSONObject stateObj = stateArray.getJSONObject(i);
                    reverseMap.put(stateObj.optString("id"), stateObj.optString("name"));
                }
            } catch (JSONException e) {
                return reverseMap;
            }
        }
        return reverseMap;
    }

    protected static String getTaxCategoryName(Map<String, String> mList, String inputKey) {
        String result = mList.get(inputKey);
        return result != null ? result : "";
    }

    protected static Map<String, String> defaultTaxCategory(Context context) {
        Map<String, String> reverseMap = new HashMap<>();
        if (context != null) {
            try {
                JSONArray stateArray = new JSONArray(getTaxCategories(context));
                for (int i = 0; i < stateArray.length(); i++) {
                    JSONObject stateObj = stateArray.getJSONObject(i);
                    reverseMap.put(stateObj.optString("id"), stateObj.optString("category"));
                }
            } catch (JSONException e) {
                return reverseMap;
            }
        }
        return reverseMap;
    }

    protected static String getTranspoartModeName(Map<String, String> mList, String inputKey) {
        String result = mList.get(inputKey);
        return result != null ? result : "";
    }

    protected static Map<String, String> defaultTranspoartMode(Context context) {
        Map<String, String> reverseMap = new HashMap<>();
        if (context != null) {
            try {
                JSONArray stateArray = new JSONArray(getTransportMode(context));
                for (int i = 0; i < stateArray.length(); i++) {
                    JSONObject stateObj = stateArray.getJSONObject(i);
                    reverseMap.put(stateObj.optString("id"), stateObj.optString("name"));
                }
            } catch (JSONException e) {
                return reverseMap;
            }
        }
        return reverseMap;
    }

}