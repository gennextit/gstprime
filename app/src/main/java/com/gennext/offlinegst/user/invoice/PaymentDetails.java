package com.gennext.offlinegst.user.invoice;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.PopupAlert;
import com.gennext.offlinegst.model.app.Model;
import com.gennext.offlinegst.model.user.InvoiceModel;
import com.gennext.offlinegst.model.user.ProductModel;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.ApiCallError;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.ProgressButtonRounded;
import com.gennext.offlinegst.util.RequestBuilder;
import com.gennext.offlinegst.util.Utility;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by Admin on 7/4/2017.
 */

public class PaymentDetails extends CompactFragment implements ApiCallError.ErrorParamListener {

    private EditText etCurrentSale,etDiscount, etTotal, etFreight,etFreightValue;
    private static final int TYPE_RUPEE = 0,TYPE_PERCENT=1;
    private String[] invoiceDetail;
    private ArrayList<ProductModel> productList;
    private ProgressButtonRounded btnAction;

    private AssignTask assignTask;
    private int sellingType;
    private InvoiceModel itemInvoice;
    private Invoices invoices;
    private CoordinatorLayout coardLayout;
    private int mDiscountType;
    private String mFreightMaxTaxRate;
    private TextView tvFreightTaxRate;
    private String mProductTaxableAmount;
    private String taxableAmountOfFreightAndProduct;
    private String mCustomerStateCode;
    private float mDiscountValue;
    private RadioButton rbRupee;
    private String mProductMinTaxRate;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static Fragment newInstance(Invoices invoices, int sellingType, InvoiceModel itemInvoice, ArrayList<String> deletedProduct, String[] invoiceDetail,String customerStateCode, ArrayList<ProductModel> productList) {
        PaymentDetails fragment = new PaymentDetails();
        AppAnimation.setSlideAnimation(fragment, Gravity.BOTTOM);
        fragment.invoices = invoices;
        fragment.sellingType = sellingType;
        fragment.itemInvoice = itemInvoice;
//        fragment.deletedProduct = deletedProduct;
        fragment.invoiceDetail = invoiceDetail;
        fragment.productList = productList;
        fragment.mCustomerStateCode=customerStateCode;
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_payment_details, container, false);
        initToolBar(getActivity(), v, getString(R.string.payment_details));
        screenAnalytics(getContext(), AppUser.getUserId(getContext()), "Payment Details");
        initUi(v);
        updateUi();
        return v;
    }

    // (+ amount)=Borrow amount
    // (- amount)=Advance amount
    private void updateUi() {
        mProductTaxableAmount = invoiceDetail[14];
        String totalAmt = validateFloat(calculateProductGST(productList));

        if (sellingType == InvoiceManager.EDIT_INVOICE) {
            etFreight.setText(itemInvoice.getFreightAmount());
            rbRupee.setChecked(true);
            etDiscount.setText(itemInvoice.getDiscount());
        } else if (sellingType == InvoiceManager.COPY_INVOICE) {
            rbRupee.setChecked(true);
            etDiscount.setText(itemInvoice.getDiscount());
        }

        etCurrentSale.setText(totalAmt);
        float[] minMaxTax = getProductMinAndMaxTaxRate();
        mProductMinTaxRate = String.valueOf(minMaxTax[0]);// discount apply tax rate
        mFreightMaxTaxRate = String.valueOf(minMaxTax[1]);// freight apply tax rate
        tvFreightTaxRate.setText(getString(R.string.freight_tax)+mFreightMaxTaxRate+"%");

        setFreightValue(parseFloat(etFreight.getText().toString()),parseFloat(mFreightMaxTaxRate));
    }


    private void initUi(View v) {
        coardLayout = (CoordinatorLayout) v.findViewById(R.id.coordinatorLayout);
        btnAction = ProgressButtonRounded.newInstance(getContext(), v);
        etCurrentSale = (EditText) v.findViewById(R.id.et_payment_current_sale);
        etDiscount = (EditText) v.findViewById(R.id.et_product_discount);
        etFreight = (EditText) v.findViewById(R.id.et_payment_freight);
        etFreightValue = (EditText) v.findViewById(R.id.et_payment_freight_tax_value);
        tvFreightTaxRate = (TextView) v.findViewById(R.id.tv_freight_tax_rate);
        etTotal = (EditText) v.findViewById(R.id.et_payment_total);
        etFreight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setFreightValue(parseFloat(etFreight.getText().toString()),parseFloat(mFreightMaxTaxRate));
            }
        });
        etDiscount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setTotalAmount(parseFloat(etDiscount.getText().toString()),parseFloat(etCurrentSale.getText().toString()),parseFloat(etFreight.getText().toString()),parseFloat(etFreightValue.getText().toString()));
            }
        });
        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(!FieldValidation.isEmpty(getContext(),etAccNumber)){
//                    return;
//                }
                taxableAmountOfFreightAndProduct=calculateTotalTaxableAmount(parseFloat(etFreight.getText().toString()),parseFloat(etFreightValue.getText().toString()),parseFloat(mProductTaxableAmount));
                hideKeybord(getActivity());
                paymentAcceptedTask(invoiceDetail, productList,taxableAmountOfFreightAndProduct,etTotal.getText().toString(),String.valueOf(mDiscountValue),String.valueOf(mFreightMaxTaxRate),etFreight.getText().toString(),etFreightValue.getText().toString(),mProductMinTaxRate);
            }
        });
        RadioGroup rgPaymentType = (RadioGroup) v.findViewById(R.id.rg_discount_type);
        rbRupee = (RadioButton) v.findViewById(R.id.type_rupee);
        mDiscountType=TYPE_PERCENT;
        rgPaymentType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.type_percent:
                        mDiscountType=TYPE_PERCENT;
                        setTotalAmount(parseFloat(etDiscount.getText().toString()),parseFloat(etCurrentSale.getText().toString()),parseFloat(etFreight.getText().toString()),parseFloat(etFreightValue.getText().toString()));
                        break;
                    case R.id.type_rupee:
                        mDiscountType=TYPE_RUPEE;
                        setTotalAmount(parseFloat(etDiscount.getText().toString()),parseFloat(etCurrentSale.getText().toString()),parseFloat(etFreight.getText().toString()),parseFloat(etFreightValue.getText().toString()));
                        break;
                }
            }
        });
    }

    private void setFreightValue(float freight, float taxRate) {
        float afterDiscount = (freight * taxRate) / 100;
        String freightDis = Utility.decimalFormat(afterDiscount, "#.##");
        etFreightValue.setText(freightDis);
        setTotalAmount(parseFloat(etDiscount.getText().toString()),parseFloat(etCurrentSale.getText().toString()),parseFloat(etFreight.getText().toString()),parseFloat(etFreightValue.getText().toString()));
    }

    private void setTotalAmount(float discount, float currentSale, float freight, float freightValue) {
        if(mDiscountType==TYPE_RUPEE) {
            mDiscountValue=discount;
            float afterDiscount = (currentSale-discount)+(freight+freightValue);
            etTotal.setText(Utility.decimalFormat(afterDiscount, "#.##"));
        }else{
            float afterDiscount = (currentSale * discount) / 100;
            mDiscountValue=afterDiscount;
            etTotal.setText(Utility.decimalFormat((currentSale - afterDiscount)+(freight+freightValue), "#.##"));
        }
    }

    private String calculateTotalTaxableAmount(float freight,float freightAmount, float productAmount) {
        return validateFloat((freight+freightAmount) + productAmount);
    }


    private float calculateProductGST(ArrayList<ProductModel> productList) {
        float allProdTax = 0;
        for (ProductModel model : productList) {
            float prodPrice = convertToFloat(model.getSalePrice()) * convertToFloat(model.getQuantity());
            float afterDiscount = (prodPrice-convertToFloat(model.getDis()));
            float productTaxValue = calTax(String.valueOf(afterDiscount), model.getTaxRate(), model.getCessRate());
            allProdTax += (afterDiscount + productTaxValue);
        }
        return allProdTax;
    }

    private float calTax(String freightAmount, String taxRate, String cessRate) {
        float maxTaxRate = convertToFloat(taxRate)+convertToFloat(cessRate);
        float totalAmt = convertToFloat(freightAmount);
        return (totalAmt * maxTaxRate) / 100;
    }


    private float convertToFloat(String value) {
        try {
            return Float.parseFloat(value);
        } catch (NumberFormatException e) {
            return 0;
        } catch (NullPointerException e) {
            return 0;
        }
    }

    private String validateFloat(float value) {
        return new DecimalFormat("#.##").format(value);
    }

    public void paymentAcceptedTask(String[] param, ArrayList<ProductModel> productList, String freightAndProductTxableAmt, String totalIncAllTax, String discountValue, String freightMaxTaxRate, String freightAmt, String freightIncTaxValue, String mProductMinTaxRate) {
        hideKeybord(getActivity());
        executeTask(param[0], param[1], param[2], param[3], param[4], param[5], param[6], param[7], param[8], param[9], param[10], param[11]
                , param[12], param[13], param[14], param[15], param[16], param[17], param[18], param[19], param[20], param[21], productList,freightAndProductTxableAmt, totalIncAllTax, discountValue, freightMaxTaxRate, freightAmt, freightIncTaxValue,mProductMinTaxRate);
    }


    private void executeTask(String invoiceNumber, String invoiceDate, String customerId, String customerName, String freightAmount
            , String transpoartMode, String transpoartModeId, String vehicleNumber, String dateOfSupply, String placeofSupply
            , String placeofSupplyId, String shipName, String shipGSTIN, String shipAddress, String totalPrice, String invTypeId, String invTypeName, String portCode, String invTermId, String invTermName, String reverceCharge, String operatorGstin, ArrayList<ProductModel> productList, String freightAndProductTxableAmt, String totalIncAllTax, String discountValue, String freightMaxTaxRate, String freightAmt, String freightIncTaxValue,String mProductMinTaxRate) {
        hideKeybord(getActivity());
        assignTask = new AssignTask(getActivity(), valString(invoiceNumber), invoiceDate, customerId, valString(customerName), freightAmount
                , transpoartMode, transpoartModeId, vehicleNumber, dateOfSupply, placeofSupply
                , placeofSupplyId, valString(shipName), valString(shipGSTIN), valString(shipAddress), totalPrice, invTypeId, invTypeName, portCode, invTermId, invTermName, reverceCharge, operatorGstin, productList, freightAndProductTxableAmt, totalIncAllTax, discountValue, freightMaxTaxRate, freightAmt, freightIncTaxValue, mProductMinTaxRate);
        assignTask.execute();
    }

//    private String totalTaxableAmount(String totalPrice, String freightAmount) {
//        float mTotalPrice = parseFloat(totalPrice);
//        float mFreight = parseFloat(freightAmount);
//        return Utility.decimalFormat(mTotalPrice + mFreight,"#.##");
//    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog, String[] param) {
        executeTask(param[0], param[1], param[2], param[3], param[4], param[5], param[6], param[7], param[8], param[9], param[10], param[11]
                , param[12], param[13], param[14], param[15], param[16], param[17], param[18], param[19], param[20], param[21], productList, param[22], param[23], param[24], param[25], param[26], param[27], param[28]);
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog, String[] param) {

    }


    private class AssignTask extends AsyncTask<String, Void, Model> {
        private final String invoiceNumber, invoiceDate, customerId, customerName, freightAmount, transpoartMode, transpoartModeId, vehicleNumber, dateOfSupply, placeofSupply, placeofSupplyId, shipName, shipGSTIN, shipAddress, totalPrice;
        private final String invTypeId, invTypeName, portCode, invTermId, invTermName, reverceCharge, operatorGstin;
        private final String freightAndProductTxableAmt,totalIncAllTax,freightTax, freight, discount;
        private String freightCgstRate, freightCgstAmount, freightSgstRate, freightSgstAmount, freightIgstRate, freightIgstAmount, productMinTaxRate;
        private final ArrayList<ProductModel> productList;
        private Context context;

        public AssignTask(Context context, String invoiceNumber, String invoiceDate, String customerId, String customerName, String freightAmount
                , String transpoartMode, String transpoartModeId, String vehicleNumber, String dateOfSupply, String placeofSupply
                , String placeofSupplyId, String shipName, String shipGSTIN, String shipAddress, String totalPrice, String invTypeId, String invTypeName, String portCode, String invTermId, String invTermName, String reverceCharge, String operatorGstin, ArrayList<ProductModel> productList
                , String freightAndProductTxableAmt, String totalIncAllTax, String discountValue, String freightMaxTaxRate, String freightAmt, String freightIncTaxValue,String productMinTaxRate) {
            this.invoiceNumber = invoiceNumber;
            this.invoiceDate = invoiceDate;
            this.customerId = customerId;
            this.customerName = customerName;
            this.transpoartMode = transpoartMode;
            this.transpoartModeId = transpoartModeId;
            this.vehicleNumber = vehicleNumber;
            this.dateOfSupply = dateOfSupply;
            this.placeofSupply = placeofSupply;
            this.placeofSupplyId = placeofSupplyId;
            this.shipName = shipName;
            this.shipGSTIN = shipGSTIN;
            this.shipAddress = shipAddress;
            this.invTypeId = invTypeId;
            this.invTypeName = invTypeName;
            this.portCode = portCode;
            this.invTermId = invTermId;
            this.invTermName = invTermName;
            this.reverceCharge = reverceCharge;
            this.operatorGstin = operatorGstin;
            this.context = context;
            this.productList = productList;
            this.totalPrice = totalPrice;
            this.freightTax = freightMaxTaxRate;
            this.freightAmount = freightAmt;
            this.freight = freightIncTaxValue;
            this.totalIncAllTax = totalIncAllTax;
            this.freightAndProductTxableAmt = freightAndProductTxableAmt;
            this.productMinTaxRate =  productMinTaxRate;
            this.discount = discountValue;
        }

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            btnAction.startAnimation();
        }

        @Override
        protected Model doInBackground(String... urls) {
            boolean isIGSTApplicable;
            String companyStateCode;
            String userId = AppUser.getUserId(context);
            String profileId = AppUser.getProfileId(context);
            String companyId = AppUser.getCompanyId(context);
            String[] freightResult;
            String productJson,response;
            String discountTaxAmount="";
            String discountTaxableAmount="";
            String invoiceGenerateType="G";// for goods

            switch (sellingType) {
                case InvoiceManager.ADD_INVOICE:
                    companyStateCode = JsonParser.getSelectedCompanyStateCode(AppUser.getCompanyDetails(context), companyId);
                     if (companyStateCode.equalsIgnoreCase(mCustomerStateCode)) {
                        isIGSTApplicable = false;
                    } else {
                        isIGSTApplicable = true;
                    }
                    freightResult = getFreightTaxRate(freightAmount, freightTax, isIGSTApplicable);
                    freightCgstRate = freightResult[1];
                    freightCgstAmount = freightResult[2];
                    freightSgstRate = freightResult[3];
                    freightSgstAmount = freightResult[4];
                    freightIgstRate = freightResult[5];
                    freightIgstAmount = freightResult[6];

                    productJson = JsonParser.generateInvoiceProductJSON(userId, companyId, invoiceNumber, productList, isIGSTApplicable);

                    response = ApiCall.POST(AppSettings.SAVE_INVOICE_DETAILS, RequestBuilder.addInvoice("new",invoiceGenerateType,userId, profileId, invoiceNumber, invoiceDate, customerId, customerName, freightAmount, transpoartMode, transpoartModeId
                            , vehicleNumber, dateOfSupply, placeofSupply, placeofSupplyId, shipName, shipGSTIN, shipAddress, totalPrice, invTypeId, invTypeName, portCode, invTermId, invTermName, reverceCharge, operatorGstin, freightTax, freightCgstRate, freightCgstAmount, freightSgstRate
                            , freightSgstAmount, freightIgstRate, freightIgstAmount, freight,productMinTaxRate,discountTaxAmount,discountTaxableAmount, discount,totalIncAllTax, productJson));
                    return JsonParser.defaultStaticParser(response);

                case InvoiceManager.EDIT_INVOICE:
                    companyStateCode = JsonParser.getSelectedCompanyStateCode(AppUser.getCompanyDetails(context), companyId);
                    if (companyStateCode.equalsIgnoreCase(mCustomerStateCode)) {
                        isIGSTApplicable = false;
                    } else {
                        isIGSTApplicable = true;
                    }
                    freightResult = getFreightTaxRate(freightAmount, freightTax, isIGSTApplicable);
                    freightCgstRate = freightResult[1];
                    freightCgstAmount = freightResult[2];
                    freightSgstRate = freightResult[3];
                    freightSgstAmount = freightResult[4];
                    freightIgstRate = freightResult[5];
                    freightIgstAmount = freightResult[6];

                    productJson = JsonParser.generateInvoiceProductJSON(userId, companyId, invoiceNumber, productList, isIGSTApplicable);

                    response = ApiCall.POST(AppSettings.SAVE_INVOICE_DETAILS, RequestBuilder.addInvoice("edit",invoiceGenerateType,userId, profileId, invoiceNumber, invoiceDate, customerId, customerName, freightAmount, transpoartMode, transpoartModeId
                            , vehicleNumber, dateOfSupply, placeofSupply, placeofSupplyId, shipName, shipGSTIN, shipAddress, totalPrice, invTypeId, invTypeName, portCode, invTermId, invTermName, reverceCharge, operatorGstin, freightTax, freightCgstRate, freightCgstAmount, freightSgstRate
                            , freightSgstAmount, freightIgstRate, freightIgstAmount, freight,productMinTaxRate,discountTaxAmount,discountTaxableAmount, discount,totalIncAllTax, productJson));
                    return JsonParser.defaultStaticParser(response);
                case InvoiceManager.COPY_INVOICE:
                    companyStateCode = JsonParser.getSelectedCompanyStateCode(AppUser.getCompanyDetails(context), companyId);
                    if (companyStateCode.equalsIgnoreCase(mCustomerStateCode)) {
                        isIGSTApplicable = false;
                    } else {
                        isIGSTApplicable = true;
                    }
                    freightResult = getFreightTaxRate(freightAmount, freightTax, isIGSTApplicable);
                    freightCgstRate = freightResult[1];
                    freightCgstAmount = freightResult[2];
                    freightSgstRate = freightResult[3];
                    freightSgstAmount = freightResult[4];
                    freightIgstRate = freightResult[5];
                    freightIgstAmount = freightResult[6];

                    productJson = JsonParser.generateInvoiceProductJSON(userId, companyId, invoiceNumber, productList, isIGSTApplicable);

                    response = ApiCall.POST(AppSettings.SAVE_INVOICE_DETAILS, RequestBuilder.addInvoice("new",invoiceGenerateType,userId, profileId, invoiceNumber, invoiceDate, customerId, customerName, freightAmount, transpoartMode, transpoartModeId
                            , vehicleNumber, dateOfSupply, placeofSupply, placeofSupplyId, shipName, shipGSTIN, shipAddress, totalPrice, invTypeId, invTypeName, portCode, invTermId, invTermName, reverceCharge, operatorGstin, freightTax, freightCgstRate, freightCgstAmount, freightSgstRate
                            , freightSgstAmount, freightIgstRate, freightIgstAmount, freight,productMinTaxRate,discountTaxAmount,discountTaxableAmount, discount,totalIncAllTax, productJson));
                    return JsonParser.defaultStaticParser(response);
                default:
                    return JsonParser.defaultFailureResponse(getSt(R.string.update_later));
            }

        }

        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                if (!AppConfig.isUpdateToServer(context)) {
                    if (result.getOutputDB().equals(Const.SUCCESS)) {
                        showSnakBar(coardLayout, result.getOutputDBMsg());
                        animateButtonAndRevert();
                    } else if (result.getOutputDB().equals(Const.FAILURE)) {
                        btnAction.revertAnimation();
                        showSnakBar(coardLayout, result.getOutputDBMsg());
                    }
                } else {
                    if (result.getOutput().equals(Const.SUCCESS)) {
                        showSnakBar(coardLayout, result.getOutputMsg());
                        animateButtonAndRevert();
                    } else if (result.getOutput().equals(Const.FAILURE)) {
                        btnAction.revertAnimation();
                        PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(), "popupAlert");
                    } else {
                        btnAction.revertAnimation();
                        String[] errorSoon = {invoiceNumber, invoiceDate, customerId, customerName, freightAmount, transpoartMode, transpoartModeId
                                , vehicleNumber, dateOfSupply, placeofSupply, placeofSupplyId, shipName, shipGSTIN, shipAddress, totalPrice, invTypeId
                                , invTypeName, portCode, invTermId, invTermName, reverceCharge, operatorGstin, freightAndProductTxableAmt, totalIncAllTax, discount, freightTax, freightAmount, freight};
                        ApiCallError.newInstance(result.getOutput(), result.getOutputMsg(), errorSoon, PaymentDetails.this)
                                .show(getFragmentManager(), "apiCallError");
                    }
                }
            }
        }

        private void animateButtonAndRevert() {
            Handler handler = new Handler();

            Runnable runnableRevert = new Runnable() {
                @Override
                public void run() {
                    if (context != null) {
                        if (invoices != null) {
                            invoices.refreshList();
                        }
                        getFragmentManager().popBackStack("invoiceManager", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                }
            };

            btnAction.revertSuccessAnimation();
            handler.postDelayed(runnableRevert, Const.BUTTON_PROGRESS_TIME);
        }

    }

    private float[] getProductMinAndMaxTaxRate() {
        float maxTaxRate = 0;
        float minTaxRate = 0;
        for (ProductModel model : productList) {
            float taxRate = convertToFloat(model.getTaxRate());
            if (maxTaxRate < taxRate) {
                maxTaxRate = taxRate;
            }
            if (minTaxRate > taxRate) {
                minTaxRate = taxRate;
            }
        }
        return new float[]{minTaxRate, maxTaxRate};
    }


    private String[] getFreightTaxRate(String freightAmount, String maxTaxRate, Boolean isIGSTApplicable) {
        return calAllTaxes(freightAmount, maxTaxRate, isIGSTApplicable);
    }

    private String[] calAllTaxes(String freightAmount, String taxRate, Boolean isIGSTApplicable) {
        float maxTaxRate = convertToFloat(taxRate);
        float totalAmt = convertToFloat(freightAmount);
        String[] taxCal = new String[7];
        if (isIGSTApplicable) {
            taxCal[0] = String.valueOf((totalAmt * maxTaxRate) / 100);
            taxCal[1] = "0";
            taxCal[2] = "0";
            taxCal[3] = "0";
            taxCal[4] = "0";
            taxCal[5] = String.valueOf(maxTaxRate);
            taxCal[6] = String.valueOf((totalAmt * maxTaxRate) / 100);
        } else {
            taxCal[0] = String.valueOf((totalAmt * maxTaxRate) / 100);
            maxTaxRate = maxTaxRate / 2;
            taxCal[1] = String.valueOf(maxTaxRate);
            taxCal[2] = String.valueOf((totalAmt * maxTaxRate) / 100);
            taxCal[3] = String.valueOf(maxTaxRate);
            taxCal[4] = String.valueOf((totalAmt * maxTaxRate) / 100);
            taxCal[5] = "0";
            taxCal[6] = "0";
        }
        return taxCal;
    }


}
