package com.gennext.offlinegst.model;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.model.user.CompanyModel;
import com.gennext.offlinegst.model.user.CustomerModel;
import com.gennext.offlinegst.model.user.InvoiceModel;
import com.gennext.offlinegst.model.user.ServiceModel;
import com.gennext.offlinegst.model.user.VoucherModel;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DateTimeUtility;
import com.gennext.offlinegst.util.L;
import com.gennext.offlinegst.util.NumberToWordsConverter;
import com.gennext.offlinegst.util.Utility;
import com.gennext.offlinegst.util.Words;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfDocument;
import com.itextpdf.text.pdf.PdfEncodings;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Admin on 8/2/2017.
 */

public class PDFServiceMaker {
    private Activity mContext;
    private String directoryName = "Invoices";

    private static final float PADDING_LOGO = 15;
    private static final float PADDING_NORMAL = 4;
    private static final float PADDING_SMALL = 2;
    private static final float PADDING_ZERO = 0;
    private static Font catFontBold = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
    private static Font catFontNormal = new Font(Font.FontFamily.TIMES_ROMAN, 13, Font.NORMAL);
    private static Font catFontBoldNormal = new Font(Font.FontFamily.TIMES_ROMAN, 13, Font.BOLD);
    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.NORMAL);
    private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
    private static Font small = new Font(Font.FontFamily.TIMES_ROMAN, 5, Font.NORMAL);
    private static Font normal = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);

    private static Font normalBold = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
    private static Font normalBodyBold = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
    private static Font normalBody = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);
    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
    private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
    private Font currencyFont;

    public static PDFServiceMaker newInstance(Activity mContext) {
        PDFServiceMaker pdfMaker = new PDFServiceMaker();
        pdfMaker.mContext = mContext;
        return pdfMaker;
    }



    public File generateInvoicePDF(InvoiceModel invModel, CompanyModel companyDetail, String selectedPrintType) throws IOException, DocumentException {

        ArrayList<String> unionTerritoryList = new ArrayList<>();
        unionTerritoryList.add("Chandigarh");
        unionTerritoryList.add("Lakshadweep");
        unionTerritoryList.add("Daman and Diu");
        unionTerritoryList.add("Dadar and Nagar Haveli");
        unionTerritoryList.add("Andaman and Nicobar Islands");
        // step 1
        Boolean isBatchAvailabe = AppConfig.isBatchAvailable(mContext);
        Boolean isExpAvailabe = AppConfig.isExpAvailable(mContext);
        Boolean isBankAvailabe = AppConfig.isBankAvailable(mContext);
        Boolean isIGSTAvailabe = AppConfig.isIGSTAvailable(mContext);
        Boolean isVehicelAvailabe = AppConfig.isVehicelAvailable(mContext);
        Boolean isDLNoAvailabe = AppConfig.isDLNoAvailable(mContext);
        Document document = new Document();
        String filename = invModel.getCustomerName() + "_Service_Invoice" + DateTimeUtility.getTimeStamp() + ".pdf";
        // step 2
        File filePath = new File(mContext.getExternalFilesDir(AppConfig.FOLDER_INVOICES), filename);
        PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(filePath));

        currencyFont = FontFactory.getFont("assets/font/PlayfairDisplay-Regular.ttf", BaseFont.IDENTITY_H
                , BaseFont.EMBEDDED, 7, Font.BOLD);
        // step 3
        document.open();
        // step 4
        if (invModel.getCancelled().toLowerCase().equals("y")) {
            pdfWriter.setPageEvent(new PDFEventListener());// watermark
        }
        // stem 5
        PdfPTable table;
        table = addInvoiceHearderTable(invModel, companyDetail, isVehicelAvailabe, isDLNoAvailabe, selectedPrintType);
        table.setWidthPercentage(105);
        table.setHorizontalAlignment(Element.ALIGN_CENTER);
        document.add(table);

        boolean result = unionTerritoryList.contains(invModel.getCustomerState());
        String gstHeader;
        if (result) {
            gstHeader = "UTGST";
        } else {
            gstHeader = "SGST";
        }
        boolean isIGSTApplicable;
        if (companyDetail.getStateCode().equalsIgnoreCase(invModel.getCustomerStateCode())) {
            isIGSTApplicable = false;
        } else {
            isIGSTApplicable = true;
        }
        table = addInvoiceBodyTable(invModel, isIGSTAvailabe, isIGSTApplicable, gstHeader);
        table.setWidthPercentage(105);
        table.setHorizontalAlignment(Element.ALIGN_CENTER);
        document.add(table);

        table = addInvoiceFooterTable(invModel, companyDetail, isBankAvailabe, isIGSTAvailabe, isIGSTApplicable, gstHeader);
        table.setWidthPercentage(105);
        table.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.setSpacingAfter(5);
        document.add(table);


//        String validity = AppUser.getPackageStatus(mContext);
//        if (!validity.toLowerCase().equals(Const.PAID)) {
//            Paragraph phrase = new Paragraph();
//            phrase.add(new Chunk(Const.FOOTER_PDF_1, normal));
//            phrase.add(new Chunk(Const.FOOTER_PDF_2, normal));
//            phrase.add(new Chunk(Const.FOOTER_PDF_3, normal));
//            phrase.add(new Chunk(Const.FOOTER_PDF_4, normal));
//            phrase.setAlignment(Element.ALIGN_CENTER);
//            document.add(phrase);
//        }
        // step 5
        document.close();
        return filePath;
    }

    private PdfPTable addInvoiceBodyTable(InvoiceModel invModel, Boolean isIGSTAvailabe, Boolean isIGSTApplicable, String gstHeader) throws DocumentException {
        boolean isSerialAvailable = AppConfig.getSerialAvailable(mContext);
        boolean isSACAvailable = true;//= AppConfig.getHSNAvailable(mContext);
        boolean isTaxAvailable = AppConfig.getTAXAvailable(mContext);
        boolean isMRPAvailable = AppConfig.isMrpAvailable(mContext);
        boolean isDiscountAvailable = AppConfig.getDiscountAvailable(mContext);
        boolean isTaxableAvailable = AppConfig.getTaxableAvailable(mContext);
        boolean isBlankRowsAvailable = false;


        PdfPTable table = new PdfPTable(14);
        int[] tableWidths = new int[]{2, 9, 2, 3, 4, 4, 4, 3, 3, 3, 3, 3, 3, 5};
//        table.setWidthPercentage(288 / 5.23f);
        if (!isSerialAvailable) {
            tableWidths[0] = 0;
        }
        // service name[1]

        if (!isSACAvailable) {
            tableWidths[2] = 0;
        }
        if (!isTaxAvailable) {
            tableWidths[3] = 0;
        }
        // Rate [4]

        if (!isDiscountAvailable) {
            tableWidths[5] = 0;
        }
        if (!isTaxableAvailable) {
            tableWidths[6] = 0;
        }
        if (isIGSTApplicable) {
            tableWidths[7] = 0;
            tableWidths[8] = 0;
            tableWidths[9] = 0;
            tableWidths[10] = 0;
        }
        if (!isIGSTAvailabe) {
            tableWidths[11] = 0;
            tableWidths[12] = 0;
        } else {
            if (!isIGSTApplicable) {
                tableWidths[11] = 0;
                tableWidths[12] = 0;
            }
        }

        // Total Amount [13]

        table.setWidths(tableWidths);
        PdfPCell cell;

        cell = new PdfPCell(new Phrase("S/N", normalBodyBold));
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Service Detail", normalBodyBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("SAC", normalBodyBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Tax%", normalBodyBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        table.addCell(cell);

        Phrase phrase = new Phrase();
        phrase.add(new Chunk("Rate", normalBodyBold));
        phrase.add(setCurrency(invModel.getCurrency()));
        cell = new PdfPCell(phrase);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        table.addCell(cell);

        phrase = new Phrase();
        phrase.add(new Chunk("Dis", normalBodyBold));
        phrase.add(setCurrency(invModel.getCurrency()));
        cell = new PdfPCell(phrase);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        table.addCell(cell);

        phrase = new Phrase();
        phrase.add(new Chunk("Taxable", normalBodyBold));
        phrase.add(setCurrency(invModel.getCurrency()));
        cell = new PdfPCell(phrase);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_CENTER);
        cell.setRowspan(2);
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("CGST", normalBodyBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setColspan(2);
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(gstHeader, normalBodyBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setColspan(2);
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("IGST", normalBodyBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setColspan(2);
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Total", normalBodyBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("%", normalBodyBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        table.addCell(cell);

        phrase = new Phrase();
        phrase.add(new Chunk("Val", normalBodyBold));
        phrase.add(setCurrency(invModel.getCurrency()));
        cell = new PdfPCell(phrase);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("%", normalBodyBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        table.addCell(cell);

        phrase = new Phrase();
        phrase.add(new Chunk("Val", normalBodyBold));
        phrase.add(setCurrency(invModel.getCurrency()));
        cell = new PdfPCell(phrase);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("%", normalBodyBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        table.addCell(cell);

        phrase = new Phrase();
        phrase.add(new Chunk("Val", normalBodyBold));
        phrase.add(setCurrency(invModel.getCurrency()));
        cell = new PdfPCell(phrase);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        table.addCell(cell);

        phrase = new Phrase();
        phrase.add(new Chunk("Amt", normalBodyBold));
        phrase.add(setCurrency(invModel.getCurrency()));
        cell = new PdfPCell(phrase);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        table.addCell(cell);

        ArrayList<ServiceModel> serviceList = invModel.getServicesList();
        for (int i = 0; i < serviceList.size(); i++) {
            ServiceModel prod = serviceList.get(i);
            String totalTaxableAmt = getTaxableProductPrice(prod.getSalePrice(), prod.getDis());
            String totalProductTax = String.valueOf(convertToFloat(prod.getCgstVal()) + convertToFloat(prod.getSgstVal()) + convertToFloat(prod.getIgstVal()));
            setAllServiceInInvoice(table, String.valueOf(i + 1), prod.getServiceName()
                    , prod.getCodeSAC(), decimalFormat(prod.getTaxRate()), prod.getSalePrice(), decimalFormat(prod.getDis()), totalTaxableAmt, decimalFormat(prod.getCgstTax()), decimalFormat(prod.getCgstVal())
                    , decimalFormat(prod.getSgstTax()), decimalFormat(prod.getSgstVal()), decimalFormat(prod.getIgstTax()), decimalFormat(prod.getIgstVal()), totalPrice(totalTaxableAmt, totalProductTax));
        }
//        String frightAmt = invModel.getFreightAmount();
//        if(frightAmt.equals("")){
//            frightAmt="0";
//        }
//        setAllProductInInvoice(table, " ", " ", "Freight", " ", " ", " ", invModel.getFreightTaxRate(), "", frightAmt
//                , "0", frightAmt, invModel.getCgstTax(), invModel.getCgstVal(), invModel.getSgstTax(), invModel.getSgstVal()
//                , invModel.getIgstTax(), invModel.getIgstVal(), totalPrice(invModel.getFreightAmount(), invModel.getFreightTotalAmount())
//                , " "," ");

        if (isBlankRowsAvailable) {
            if (serviceList.size() + 1 < 19) {
                for (int i = 0; i < 20 - serviceList.size() + 1; i++) {
                    setAllServiceInInvoice(table, " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ");
                }
            }
        }
        return table;
    }


    private String getTaxableProductPrice(String SalePrice, String discount) {
        float mSale = convertToFloat(SalePrice);
        float mTax = convertToFloat(discount);
        return new DecimalFormat("#.##").format(mSale - mTax);
    }


    private String decimalFormat(float value) {
        String ures = new DecimalFormat("#.##").format(value);
        L.m(ures);
        return ures;
    }

    private String decimalFormat(String value) {
        String ures = new DecimalFormat("#.##").format(convertToFloat(value));
        L.m(ures);
        return ures;
    }


    private String totalPrice(String salePrice, String totalTaxAmount) {
        float saleAmt = convertToFloat(salePrice);
        float taxAmt = convertToFloat(totalTaxAmount);
        return new DecimalFormat("#.##").format(saleAmt + taxAmt);
    }

    private String totalPrice(String salePrice, String frieghtAmount, String frieghtTaxAmount, String totalTaxAmount) {
        float saleAmt = convertToFloat(salePrice);
        float frieghtAmt = convertToFloat(frieghtAmount);
        float frightTaxAmt = convertToFloat(frieghtTaxAmount);
        float totalTaxAmt = convertToFloat(totalTaxAmount);
        return new DecimalFormat("#.##").format(saleAmt + frieghtAmt + frightTaxAmt + totalTaxAmt);
    }

    private String totalPriceRoundOff(String salePrice, String frieghtAmount, String frieghtTaxAmount, String totalTaxAmount) {
        float saleAmt = convertToFloat(salePrice);
        float frieghtAmt = convertToFloat(frieghtAmount);
        float frightTaxAmt = convertToFloat(frieghtTaxAmount);
        float totalTaxAmt = convertToFloat(totalTaxAmount);
        return new DecimalFormat("#").format(saleAmt + frieghtAmt + frightTaxAmt + totalTaxAmt);
    }

    private float convertToFloat(String value) {
        try {
            return Float.parseFloat(value);
        } catch (NumberFormatException e) {
            return 0;
        } catch (NullPointerException e) {
            return 0;
        }
    }

    private void setAllServiceInInvoice(PdfPTable table, String sNo, String pName, String sac, String tax
            , String rate, String dis, String taxable, String cgstPercent, String cgstVal, String sgstPercent, String sgstVal, String igstPercent, String igstVal, String total) {
        PdfPCell cell;

        cell = new PdfPCell(new Phrase(sNo, normalBody));
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(pName, normalBody));
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(validateHSNCode(sac), normalBody));
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(tax, normalBody));
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(rate, normalBody));
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(dis == null ? "0" : dis, normalBody));
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(taxable, normalBody));
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(cgstPercent, normalBody));
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(cgstVal, normalBody));
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(sgstPercent, normalBody));
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(sgstVal, normalBody));
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(igstPercent.equalsIgnoreCase("0") ? "" : igstPercent, normalBody));
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(igstVal.equalsIgnoreCase("0") ? "" : igstVal, normalBody));
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(decimalFormatWith2Digit(total), normalBody));
        cell.setPaddingTop(PADDING_SMALL);
        cell.setPaddingBottom(PADDING_SMALL);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        table.addCell(cell);

    }

    public static String decimalFormatWith2Digit(float value) {
        return String.format(Locale.US, "%.02f", value);
    }

    public static String decimalFormatWith2Digit(String value) {
        return String.format(Locale.US, "%.02f", parseFloat(value));
    }

    private String validateHSNCode(String hsn) {
        try {
            if (hsn.length() <= 4) {
                return hsn;
            } else {
                return hsn.substring(0, 4);
            }
        } catch (IndexOutOfBoundsException e) {
            return hsn;
        }
    }

    private PdfPTable addInvoiceFooterTable(InvoiceModel invModel, CompanyModel companyDetail
            , Boolean isBankAvailabe, Boolean isIGSTAvailabe, Boolean isIGSTApplicable, String gstHeader) throws DocumentException {
        PdfPTable table = new PdfPTable(9);
        String[] allGST = calculateProductGST(invModel.getServicesList());
        String totalTaxableAmount = decimalFormatWith2Digit(invModel.getTotalAmount());
        String amountAfterTax = totalPrice(totalTaxableAmount, allGST[0]);
        String totalAmount = Utility.decimalFormat(convertToFloat(amountAfterTax));

//        table.setWidthPercentage(288 / 5.23f);
        int[] tableWidth = new int[]{1, 1, 1, 1, 1, 1, 1, 2, 1};
        if (!isBankAvailabe) {
            tableWidth[6] = 0;
        }
        if (!isIGSTAvailabe) {
            tableWidth[4] = 0;
            tableWidth[6] = 2;
        }
        table.setWidths(tableWidth);
        PdfPCell cell;
        cell = new PdfPCell(new Phrase("Total Invoice Amount in Words", normalBold));
        cell.setPadding(PADDING_NORMAL);
        cell.setColspan(7);
        table.addCell(cell);


        cell = new PdfPCell(new Phrase("Total Amount Before Tax", normalBold));
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(totalTaxableAmount, normalBold));
        cell.setPadding(PADDING_NORMAL);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(NumberToWordsConverter.convertToWord(totalAmount) + " only.", normalBold));
//        cell = new PdfPCell(new Phrase(Words.convertToWord(totalAmount) + " only.", normalBold));
        cell.setPadding(PADDING_NORMAL);
        cell.setRowspan(2);
        cell.setColspan(7);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("CGST", normalBold));
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(decimalFormatWith2Digit(allGST[1]), normalBold));
        cell.setPadding(PADDING_NORMAL);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(gstHeader, normalBold));
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(decimalFormatWith2Digit(allGST[2]), normalBold));
        cell.setPadding(PADDING_NORMAL);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        table.addCell(cell);

        if (isBankAvailabe) {
            cell = new PdfPCell(new Phrase("Terms & Condition", normalBold));
            cell.setPadding(PADDING_NORMAL);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setColspan(4);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Bank Details", normalBold));
            cell.setPadding(PADDING_NORMAL);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setColspan(3);
            table.addCell(cell);
        } else {
            cell = new PdfPCell(new Phrase("Terms & Condition", normalBold));
            cell.setPadding(PADDING_NORMAL);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setColspan(7);
            table.addCell(cell);
        }

        cell = new PdfPCell(new Phrase("IGST", normalBold));
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(decimalFormatWith2Digit(allGST[3]), normalBold));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        if (isBankAvailabe) {
            cell = new PdfPCell(new Phrase(companyDetail.gettAndC(), normal));
            cell.setPadding(PADDING_NORMAL);
            if (isIGSTAvailabe) {
                cell.setRowspan(5);
            } else {
                cell.setRowspan(4);
            }
            cell.setColspan(4);
            table.addCell(cell);

            String swiftCode;
            if (companyDetail.getSwiftCode().equals("")) {
                swiftCode = "Not Available";
            } else {
                swiftCode = companyDetail.getSwiftCode();

            }
            cell = new PdfPCell(new Phrase("Account Name :" + companyDetail.getAccName() + "\n" +
                    "Bank Name :" + companyDetail.getBankName() + "\n" +
                    "Bank Account :" + companyDetail.getAccNumber() + "\n" +
                    "Bank IFSC :" + companyDetail.getIfscCode() + "\n" +
                    "SWIFT Code :" + swiftCode + "\n" +
                    "Bank Branch Address :" + companyDetail.getBranchAddress() + "\n", normalBold));
            cell.setPadding(PADDING_NORMAL);
            if (isIGSTAvailabe) {
                cell.setRowspan(5);
            } else {
                cell.setRowspan(4);
            }
            cell.setColspan(3);
            table.addCell(cell);
        } else {
            cell = new PdfPCell(new Phrase(companyDetail.gettAndC(), normal));
            cell.setPadding(PADDING_NORMAL);
            if (isIGSTAvailabe) {
                cell.setRowspan(5);
            } else {
                cell.setRowspan(4);
            }
            cell.setColspan(7);
            table.addCell(cell);
        }

        cell = new PdfPCell(new Phrase("Tax Amount", normalBold));
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(decimalFormatWith2Digit(allGST[0]), normalBold));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Amount After Tax", normalBold));
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);


//        String totalAmt = totalPriceRoundOff(invModel.getTotalAmount(), invModel.getFreightAmount(), invModel.getFreightTotalAmount()
//                , allGST[0]);

        cell = new PdfPCell(new Phrase(decimalFormatWith2Digit(amountAfterTax), normalBold));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);


        cell = new PdfPCell(new Phrase("Rounded Off", normalBold));
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

//        String roundValue = decimalFormat2(convertToFloat(totalAmount) - convertToFloat(amountAfterTax));
        String roundValue = decimalFormat(convertToFloat(totalAmount) - convertToFloat(amountAfterTax));

        cell = new PdfPCell(new Phrase(roundValue, normalBold));

        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Total Amount", normalBold));
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(decimalFormatWith2Digit(totalAmount), normalBold));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("GST on Reverse Charge", normalBold));
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("No", normalBold));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Rate", normalBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Taxable Amount", normalBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("CGST Amount", normalBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(gstHeader + " Amount", normalBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        if (isIGSTAvailabe) {
            cell = new PdfPCell(new Phrase("IGST Amount", normalBold));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setPadding(PADDING_NORMAL);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Total", normalBold));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setPadding(PADDING_NORMAL);
            table.addCell(cell);
        } else {
            cell = new PdfPCell(new Phrase("Total", normalBold));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setPadding(PADDING_NORMAL);
            cell.setColspan(2);
            table.addCell(cell);
        }

        cell = new PdfPCell(new Phrase("Ceritified that the particulars given above are true and correct", normal));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPadding(PADDING_NORMAL);
        cell.setColspan(3);
        table.addCell(cell);

        float[] GSTZero = {0, 0, 0, 0, 0};
        float[] GSTFive = {0, 0, 0, 0, 0};
        float[] GSTTwelve = {0, 0, 0, 0, 0};
        float[] GSTEighteen = {0, 0, 0, 0, 0};
        float[] GSTTwoEight = {0, 0, 0, 0, 0};
        for (ServiceModel model : invModel.getServicesList()) {
            float taxRate = parseFloat(model.getTaxRate());
            String mTaxable = getTaxableProductPrice(model.getSalePrice(), model.getDis());
            float taxable = convertToFloat(mTaxable);
            float cgst = convertToFloat(model.getCgstVal());
            float sgst = convertToFloat(model.getSgstVal());
            float igst = convertToFloat(model.getIgstVal());

            if (taxRate == 0) {
                GSTZero[0] += taxable;
                GSTZero[4] += taxable;
            }
            if (taxRate == 5) {
                GSTFive[0] += taxable;
                GSTFive[1] += cgst;
                GSTFive[2] += sgst;
                GSTFive[3] += igst;
                GSTFive[4] += taxable + cgst + sgst + igst;
            }
            if (taxRate == 12) {
                GSTTwelve[0] += taxable;
                GSTTwelve[1] += cgst;
                GSTTwelve[2] += sgst;
                GSTTwelve[3] += igst;
                GSTTwelve[4] += taxable + cgst + sgst + igst;
            }
            if (taxRate == 18) {
                GSTEighteen[0] += taxable;
                GSTEighteen[1] += cgst;
                GSTEighteen[2] += sgst;
                GSTEighteen[3] += igst;
                GSTEighteen[4] += taxable + cgst + sgst + igst;
            }
            if (taxRate == 28) {
                GSTTwoEight[0] += taxable;
                GSTTwoEight[1] += cgst;
                GSTTwoEight[2] += sgst;
                GSTTwoEight[3] += igst;
                GSTTwoEight[4] += taxable + cgst + sgst + igst;
            }

        }
        setAllGST(isIGSTAvailabe, table, "GST-00", GSTZero[0], GSTZero[1], GSTZero[2], GSTZero[3], GSTZero[4], true);
        setAllGST(isIGSTAvailabe, table, "GST-05", GSTFive[0], GSTFive[1], GSTFive[2], GSTFive[3], GSTFive[4], false);
        setAllGST(isIGSTAvailabe, table, "GST-12", GSTTwelve[0], GSTTwelve[1], GSTTwelve[2], GSTTwelve[3], GSTTwelve[4], false);
        setAllGST(isIGSTAvailabe, table, "GST-18", GSTEighteen[0], GSTEighteen[1], GSTEighteen[2], GSTEighteen[3], GSTEighteen[4], false);
        setAllGST(isIGSTAvailabe, table, "GST-28", GSTTwoEight[0], GSTTwoEight[1], GSTTwoEight[2], GSTTwoEight[3], GSTTwoEight[4], false);

        cell = new PdfPCell();
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        cell = new PdfPCell();
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        cell = new PdfPCell();
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        cell = new PdfPCell();
        cell.setPadding(PADDING_NORMAL);
        table.addCell(cell);

        if (isBankAvailabe && !isIGSTAvailabe) {
            cell = new PdfPCell();
            cell.setPadding(PADDING_NORMAL);
            cell.setColspan(2);
            table.addCell(cell);
        } else {
            cell = new PdfPCell();
            cell.setPadding(PADDING_NORMAL);
            table.addCell(cell);

            cell = new PdfPCell();
            cell.setPadding(PADDING_NORMAL);
            table.addCell(cell);
        }

        cell = new PdfPCell(new Phrase("Authorised signatory", normalBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPadding(PADDING_NORMAL);
        cell.setColspan(3);
        table.addCell(cell);


        return table;
    }

    private String[] calculateProductGST(ArrayList<ServiceModel> serviceList) {
        String[] allGst = new String[4];
        float totalTax, totalCGST = 0, totalSGST = 0, totalIGST = 0;
        for (ServiceModel mod : serviceList) {
            float cgst = convertToFloat(mod.getCgstVal());
            float sgst = convertToFloat(mod.getSgstVal());
            float igst = convertToFloat(mod.getIgstVal());
            totalCGST += cgst;
            totalSGST += sgst;
            totalIGST += igst;
        }
        totalTax = totalCGST + totalSGST + totalIGST;
        allGst[0] = new DecimalFormat("#.##").format(totalTax);
        allGst[1] = new DecimalFormat("#.##").format(totalCGST);
        allGst[2] = new DecimalFormat("#.##").format(totalSGST);
        allGst[3] = new DecimalFormat("#.##").format(totalIGST);
        return allGst;
    }

    public static int parseInt(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return 0;
        } catch (NullPointerException e) {
            return 0;
        }
    }

    public static float parseFloat(String value) {
        try {
            return Float.parseFloat(value);
        } catch (NumberFormatException e) {
            return 0;
        } catch (NullPointerException e) {
            return 0;
        }
    }

    private void setAllGST(Boolean isIGSTAvailabe, PdfPTable table, String rate, float taxableAmt, float cgst, float sgst, float igst, float total, boolean isSignature) {
        PdfPCell cell;
        cell = new PdfPCell(new Phrase(rate, normalBold));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPadding(PADDING_SMALL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(String.valueOf(new DecimalFormat("#.##").format(taxableAmt)), normalBold));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setPadding(PADDING_SMALL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(String.valueOf(new DecimalFormat("#.##").format(cgst)), normalBold));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setPadding(PADDING_SMALL);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(String.valueOf(new DecimalFormat("#.##").format(sgst)), normalBold));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setPadding(PADDING_SMALL);
        table.addCell(cell);

        if (isIGSTAvailabe) {
            cell = new PdfPCell(new Phrase(new DecimalFormat("#.##").format(igst), normalBold));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setPadding(PADDING_SMALL);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(new DecimalFormat("#.##").format(total), normalBold));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setPadding(PADDING_SMALL);
            table.addCell(cell);
        } else {
            cell = new PdfPCell(new Phrase(new DecimalFormat("#.##").format(total), normalBold));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setPadding(PADDING_SMALL);
            cell.setColspan(2);
            table.addCell(cell);
        }

        if (isSignature) {
            cell = new PdfPCell(new Phrase("This is system generated invoice hence no signature required", normalBody));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setPadding(PADDING_SMALL);
            cell.setColspan(3);
            cell.setRowspan(5);
            table.addCell(cell);
        }
    }


    private PdfPTable addInvoiceHearderTable(InvoiceModel invModel, CompanyModel companyDetail, Boolean isVehicelAvailabe, Boolean isDLNoAvailabe, String selectedPrintType) throws DocumentException {
        PdfPTable table = new PdfPTable(5);
//        table.setWidthPercentage(288 / 5.23f);
        int[] tableWidths = new int[]{15, 20, 30, 20, 15};
        table.setWidths(tableWidths);
        PdfPCell cell;
        Phrase phrase;

        PdfPCell companyLogo;
        if (!TextUtils.isEmpty(companyDetail.getLogo())) {
            companyLogo = createCompanyLogoCell(AppUser.getImage(mContext));
        } else {
            companyLogo = null;
        }
        if (companyLogo == null) {
            companyLogo = new PdfPCell((new Phrase("", normal)));
            companyLogo.setBorder(Rectangle.NO_BORDER);
            companyLogo.setVerticalAlignment(Element.ALIGN_CENTER);
            companyLogo.setHorizontalAlignment(Element.ALIGN_CENTER);
            companyLogo.setPadding(PADDING_NORMAL);
        }
        table.addCell(companyLogo);

        phrase = new Phrase();
        phrase.add(new Chunk(companyDetail.getCompanyName(), catFontBold));
        phrase.add(new Chunk("\n", catFontBoldNormal));
        if (isDLNoAvailabe) {
            phrase.add(new Chunk(companyDetail.getAddress() + "\n" + companyDetail.getTelephone() + "\nGSTIN:" + companyDetail.getGstin() + "\nD.L.No." + companyDetail.getDlNumber(), catFontBoldNormal));
        } else {
            phrase.add(new Chunk(companyDetail.getAddress() + "\n" + companyDetail.getTelephone() + "\nGSTIN:" + companyDetail.getGstin(), catFontBoldNormal));
        }
        phrase.add(new Chunk("\n\n" + "Tax Invoice", catFontNormal));

        cell = new PdfPCell(phrase);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPadding(PADDING_NORMAL);
        cell.setVerticalAlignment(Element.ALIGN_CENTER);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setColspan(3);
        table.addCell(cell);

//        phrase = new Phrase();
//        phrase.add(new Chunk("Invoice No\n", catFontBoldNormal));
//        phrase.add(new Chunk(invModel.getInvoiceNumber() + "\n", catFontBoldNormal));
//        phrase.add(new Chunk("Invoice Date\n", catFontBoldNormal));
//        phrase.add(new Chunk(invModel.getInvoiceDate(), catFontBoldNormal));

        cell = new PdfPCell(new Phrase());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setVerticalAlignment(Element.ALIGN_CENTER);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        cell.setNoWrap(false);
//        cell.setMinimumHeight(50);
        cell.setPadding(PADDING_NORMAL);
//        cell.setColspan(4);
        table.addCell(cell);

        phrase = new Phrase();
        phrase.add(new Chunk("Billing Details\n", catFontBoldNormal));
        phrase.add(new Chunk(invModel.getCustomerCompany() + "\n", catFontNormal));
        phrase.add(new Chunk(invModel.getCustomerAddress() + ", " + invModel.getCustomerState() + "\n  ", catFontNormal));

        cell = new PdfPCell(phrase);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setVerticalAlignment(Element.ALIGN_LEFT);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//        cell.setNoWrap(false);
//        cell.setMinimumHeight(50);
        cell.setPadding(PADDING_NORMAL);
        cell.setColspan(3);
        table.addCell(cell);

        phrase = new Phrase();
        phrase.add(new Chunk("Invoice No\n", catFontBoldNormal));
        phrase.add(new Chunk(invModel.getInvoiceNumber() + "\n", catFontNormal));
        phrase.add(new Chunk("Invoice Date\n", catFontBoldNormal));
        phrase.add(new Chunk(invModel.getInvoiceDate(), catFontNormal));

        cell = new PdfPCell(phrase);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setVerticalAlignment(Element.ALIGN_RIGHT);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//        cell.setNoWrap(false);
//        cell.setMinimumHeight(50);
        cell.setPadding(PADDING_NORMAL);
        cell.setColspan(2);
        table.addCell(cell);

        return table;
    }

    private PdfPCell createCompanyLogoCell(String path) {
        Image img = null;
        PdfPCell cell = null;
        try {
            img = Image.getInstance(path);
//            img.scaleAbsolute(50, 50);
//            img.setScaleToFitLineWhenOverflow(false);
            cell = new PdfPCell(img, true);//true= fit image on cell
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setVerticalAlignment(Element.ALIGN_CENTER);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setPadding(PADDING_ZERO);
        } catch (BadElementException | IOException e) {
            e.printStackTrace();
        }
        return cell;
    }

    private Chunk setCurrency(String currency) {
        Chunk chunk;
        if(currency!=null){
            switch (currency){
                case "INR":
                    chunk=new Chunk(" (\u20B9)", currencyFont);
                    break;
                case "USD":
                    chunk=new Chunk(" (\u0024)", currencyFont);
                    break;
                case "EUR":
                    chunk=new Chunk(" (\u20AC)", currencyFont);
                    break;
                default:
                    chunk=new Chunk();
                    break;
            }
            return chunk;
        }else {
            return new Chunk();
        }
    }

    public File generateVoucherPDF(VoucherModel inv, CompanyModel companyDetail, CustomerModel customerDetail) throws IOException, DocumentException {
        return null;
    }
}
