package com.gennext.offlinegst.model.user;

import java.util.ArrayList;

/**
 * Created by Admin on 7/21/2017.
 */

public class ProductModel {

    private String productId;
    private String productUserId;
    private String productProfileId;
    private String productCompanyId;
    private String productName;
    private String codeHSN;
    private String barcode;
    private String costPrice;
    private String salePrice;
    private String taxRate;
    private String fromDate;
    private String toDate;
    private String totalTaxAmount;
    private String cgstTax;
    private String cgstVal;
    private String sgstTax;
    private String sgstVal;
    private String igstTax;
    private String igstVal;
    private String quantity;
    private float initQuantity;
    private String batch;
    private String exp;
    private String dis;
    private String purchasesProdId;
    private String invNumber;
    private String voucherNo;
    private String unitCode;
    private String unitName;
    private String cessRate;
    private String mrp;
    private String taxCategory;
    private String taxCategoryId;
    private String stockBatch;
    private String stockExpiry;
    private String stockMrp;
    private String netAmount;
    private String serviceDes;
    private String serviceId;
    private String sacCode;
    private String openingStock;

    private String output;
    private String outputMsg;
    private String outputDB;
    private String outputDBMsg;
    private ArrayList<ProductModel>list;

    public String getOpeningStock() {
        return openingStock;
    }

    public void setOpeningStock(String openingStock) {
        this.openingStock = openingStock;
    }

    public String getServiceDes() {
        return serviceDes;
    }

    public void setServiceDes(String serviceDes) {
        this.serviceDes = serviceDes;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getSacCode() {
        return sacCode;
    }

    public void setSacCode(String sacCode) {
        this.sacCode = sacCode;
    }

    public String getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(String netAmount) {
        this.netAmount = netAmount;
    }

    public String getTaxCategory() {
        return taxCategory;
    }


    public String getStockBatch() {
        return stockBatch;
    }

    public void setStockBatch(String stockBatch) {
        this.stockBatch = stockBatch;
    }

    public String getStockExpiry() {
        return stockExpiry;
    }

    public void setStockExpiry(String stockExpiry) {
        this.stockExpiry = stockExpiry;
    }

    public String getStockMrp() {
        return stockMrp;
    }

    public void setStockMrp(String stockMrp) {
        this.stockMrp = stockMrp;
    }

    public String getTaxCategoryId() {
        return taxCategoryId;
    }

    public void setTaxCategoryId(String taxCategoryId) {
        this.taxCategoryId = taxCategoryId;
    }

    public void setTaxCategory(String taxCategory) {
        this.taxCategory = taxCategory;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getUnitCode() {
        return unitCode;
    }

    public void setUnitCode(String unitCode) {
        this.unitCode = unitCode;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getCessRate() {
        return cessRate;
    }

    public void setCessRate(String cessRate) {
        this.cessRate = cessRate;
    }

    public String getPurchasesProdId() {
        return purchasesProdId;
    }

    public void setPurchasesProdId(String purchasesProdId) {
        this.purchasesProdId = purchasesProdId;
    }

    public float getInitQuantity() {
        return initQuantity;
    }

    public void setInitQuantity(float initQuantity) {
        this.initQuantity = initQuantity;
    }

    public String getVoucherNo() {
        return voucherNo;
    }

    public void setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
    }

    public String getInvNumber() {
        return invNumber;
    }

    public void setInvNumber(String invNumber) {
        this.invNumber = invNumber;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public String getDis() {
        return dis;
    }

    public void setDis(String dis) {
        this.dis = dis;
    }

    public String getTotalTaxAmount() {
        return totalTaxAmount;
    }

    public void setTotalTaxAmount(String totalTaxAmount) {
        this.totalTaxAmount = totalTaxAmount;
    }

    public String getCgstTax() {
        return cgstTax;
    }

    public void setCgstTax(String cgstTax) {
        this.cgstTax = cgstTax;
    }

    public String getCgstVal() {
        return cgstVal;
    }

    public void setCgstVal(String cgstVal) {
        this.cgstVal = cgstVal;
    }

    public String getSgstTax() {
        return sgstTax;
    }

    public void setSgstTax(String sgstTax) {
        this.sgstTax = sgstTax;
    }

    public String getSgstVal() {
        return sgstVal;
    }

    public void setSgstVal(String sgstVal) {
        this.sgstVal = sgstVal;
    }

    public String getIgstTax() {
        return igstTax;
    }

    public void setIgstTax(String igstTax) {
        this.igstTax = igstTax;
    }

    public String getIgstVal() {
        return igstVal;
    }

    public void setIgstVal(String igstVal) {
        this.igstVal = igstVal;
    }

    public String getProductCompanyId() {
        return productCompanyId;
    }

    public void setProductCompanyId(String productCompanyId) {
        this.productCompanyId = productCompanyId;
    }

    public String getProductProfileId() {
        return productProfileId;
    }

    public void setProductProfileId(String productProfileId) {
        this.productProfileId = productProfileId;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getOutputDB() {
        return outputDB;
    }

    public void setOutputDB(String outputDB) {
        this.outputDB = outputDB;
    }

    public String getOutputDBMsg() {
        return outputDBMsg;
    }

    public void setOutputDBMsg(String outputDBMsg) {
        this.outputDBMsg = outputDBMsg;
    }

    public String getProductUserId() {
        return productUserId;
    }

    public void setProductUserId(String productUserId) {
        this.productUserId = productUserId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public ArrayList<ProductModel> getList() {
        return list;
    }

    public void setList(ArrayList<ProductModel> list) {
        this.list = list;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCodeHSN() {
        return codeHSN;
    }

    public void setCodeHSN(String codeHSN) {
        this.codeHSN = codeHSN;
    }

    public String getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(String costPrice) {
        this.costPrice = costPrice;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }

    public String getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(String taxRate) {
        this.taxRate = taxRate;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }
}
