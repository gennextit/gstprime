package com.gennext.offlinegst.model.user;

import java.util.ArrayList;

/**
 * Created by Admin on 8/18/2017.
 */

public class PaymentModel {

    private String debtorsId;
    private String userId;
    private String profileId;
    private String invoiceNo;
    private String customerId;
    private String customerName;
    private String vendorId;
    private String vendorName;
    private String paidAmount;
    private String payableAmount;
    private String balanceAmount;
    private String paymentMode;
    private String BankName;
    private String chequeNo;
    private String AccName;
    private String IFSC;
    private String AccNumber;
    private String BranchAddress;
    private String openingBalance;
    private String paymentReceived;

    private ArrayList<PaymentModel> list;


    private String output;
    private String outputMsg;
    private String outputDB;
    private String outputDBMsg;


    public void setCustomerAllFields(String invoiceNo,
                                     String customerId,
                                     String customerName,
                                     String paidAmount,
                                     String payableAmount,
                                     String balanceAmount,
                                     String paymentMode,
                                     String BankName,
                                     String chequeNo,
                                     String AccName,
                                     String IFSC,
                                     String AccNumber,
                                     String BranchAddress,
                                     String openingBalance,
                                     String paymentReceived) {
        this.invoiceNo = invoiceNo;
        this.customerId = customerId;
        this.customerName = customerName;
        this.paidAmount = paidAmount;
        this.payableAmount = payableAmount;
        this.balanceAmount = balanceAmount;
        this.paymentMode = paymentMode;
        this.BankName = BankName;
        this.chequeNo = chequeNo;
        this.AccName = AccName;
        this.IFSC = IFSC;
        this.AccNumber = AccNumber;
        this.BranchAddress = BranchAddress;
        this.openingBalance = openingBalance;
        this.paymentReceived = paymentReceived;

    }

    public void setVendorAllFields(String invoiceNo,
                                   String vendorId,
                                   String vendorName,
                                   String paidAmount,
                                   String payableAmount,
                                   String balanceAmount,
                                   String paymentMode,
                                   String BankName,
                                   String chequeNo,
                                   String AccName,
                                   String IFSC,
                                   String AccNumber,
                                   String BranchAddress,
                                   String openingBalance,
                                   String paymentReceived) {
        this.invoiceNo = invoiceNo;
        this.vendorId = vendorId;
        this.vendorName = vendorName;
        this.paidAmount = paidAmount;
        this.payableAmount = payableAmount;
        this.balanceAmount = balanceAmount;
        this.paymentMode = paymentMode;
        this.BankName = BankName;
        this.chequeNo = chequeNo;
        this.AccName = AccName;
        this.IFSC = IFSC;
        this.AccNumber = AccNumber;
        this.BranchAddress = BranchAddress;
        this.openingBalance = openingBalance;
        this.paymentReceived = paymentReceived;

    }

    public String getPaymentReceived() {
        return paymentReceived;
    }

    public void setPaymentReceived(String paymentReceived) {
        this.paymentReceived = paymentReceived;
    }

    public String getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(String openingBalance) {
        this.openingBalance = openingBalance;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getDebtorsId() {
        return debtorsId;
    }

    public void setDebtorsId(String debtorsId) {
        this.debtorsId = debtorsId;
    }

    public ArrayList<PaymentModel> getList() {
        return list;
    }

    public void setList(ArrayList<PaymentModel> list) {
        this.list = list;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(String paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getPayableAmount() {
        return payableAmount;
    }

    public void setPayableAmount(String payableAmount) {
        this.payableAmount = payableAmount;
    }

    public String getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(String balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public String getAccName() {
        return AccName;
    }

    public void setAccName(String accName) {
        AccName = accName;
    }

    public String getIFSC() {
        return IFSC;
    }

    public void setIFSC(String IFSC) {
        this.IFSC = IFSC;
    }

    public String getAccNumber() {
        return AccNumber;
    }

    public void setAccNumber(String accNumber) {
        AccNumber = accNumber;
    }

    public String getBranchAddress() {
        return BranchAddress;
    }

    public void setBranchAddress(String branchAddress) {
        BranchAddress = branchAddress;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public String getOutputDB() {
        return outputDB;
    }

    public void setOutputDB(String outputDB) {
        this.outputDB = outputDB;
    }

    public String getOutputDBMsg() {
        return outputDBMsg;
    }

    public void setOutputDBMsg(String outputDBMsg) {
        this.outputDBMsg = outputDBMsg;
    }


    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

}
