package com.gennext.offlinegst.login;


/**
 * Created by Admin on 5/22/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.gennext.offlinegst.MainActivity;
import com.gennext.offlinegst.ProfileActivity;
import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.PopupAlert;
import com.gennext.offlinegst.model.app.Model;
import com.gennext.offlinegst.model.user.LoginModel;
import com.gennext.offlinegst.model.user.ProfileModel;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.user.company.AddProfile;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.ApiCallError;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.FieldValidation;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.ProgressButtonRounded;
import com.gennext.offlinegst.util.RequestBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class LoginOption extends CompactFragment implements ApiCallError.ErrorParamListener{

    private EditText etEmail, etPass;
    private ProgressButtonRounded btnSigninOtp;
    private DBManager dbManager;
    private CoordinatorLayout codLayout;

    private AssignTask assignTask;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.login_option, container, false);
        initToolBarForActivity(getActivity(), v, getString(R.string.login));
        dbManager = DBManager.newIsntance(getActivity());
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        codLayout = (CoordinatorLayout) v.findViewById(R.id.coordinatorLayout);
        etEmail = (EditText) v.findViewById(R.id.et_login_email);
        etPass = (EditText) v.findViewById(R.id.et_login_pass);

        btnSigninOtp = ProgressButtonRounded.newInstance(getContext(),v);
        btnSigninOtp.setText(getString(R.string.start_session));

        btnSigninOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FieldValidation.isEmail(getContext(), etEmail, true)) {
                    return;
                }
                if (!FieldValidation.isEmpty(getContext(), etPass)) {
                    return;
                }
                hideKeybord(getActivity());
                executeTask(etEmail.getText().toString(), etPass.getText().toString());
            }
        });
    }



    private void executeTask(String email,String password) {
        assignTask = new AssignTask(getActivity(), valString(email), valString(password));
        assignTask.execute(AppSettings.LOGIN);
    }



    @Override
    public void onErrorRetryClick(DialogFragment dialog, String[] param) {
        executeTask(param[0],param[1]);
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog, String[] param) {

    }

    private class AssignTask extends AsyncTask<String, Void, LoginModel> {
        private final String email, password;
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context,String email,String password) {
            this.email = email;
            this.password = password;
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            btnSigninOtp.startAnimation();
        }

        @Override
        protected LoginModel doInBackground(String... urls) {
            String response = ApiCall.POST(urls[0], RequestBuilder.login(email, password));
            return JsonParser.login(new LoginModel(),context,response);
        }


        @Override
        protected void onPostExecute(LoginModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                if (result.getOutput().equals(Const.SUCCESS)) {
                    animateButtonAndRevert(getActivity());
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    btnSigninOtp.revertAnimation();
                    PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                            .show(getFragmentManager(), "popupAlert");
                } else {
                    btnSigninOtp.revertAnimation();
                    String[] errorSoon = {email, password};
                    ApiCallError.newInstance(result.getOutput(), result.getOutputMsg(), errorSoon, LoginOption.this)
                            .show(getFragmentManager(), "apiCallError");

                }
            }
        }

        private void animateButtonAndRevert(final Activity activity) {
            Handler handler = new Handler();


            Runnable runnableRevert = new Runnable() {
                @Override
                public void run() {
                    if(context!=null) {
                        if(AppUser.getProfileId(getContext()).equals("")) {
                            startActivity(new Intent(getActivity(), ProfileActivity.class));
                            getActivity().finish();
                        }else{
                            startActivity(new Intent(getActivity(), MainActivity.class));
                            getActivity().finish();
                        }
                    }
                }
            };

            btnSigninOtp.revertSuccessAnimation();
            handler.postDelayed(runnableRevert, Const.BUTTON_PROGRESS_TIME);
        }

    }



}
