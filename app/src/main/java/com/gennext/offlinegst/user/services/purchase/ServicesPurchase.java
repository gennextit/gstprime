package com.gennext.offlinegst.user.services.purchase;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gennext.offlinegst.MainActivity;
import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.DatePickerDialog;
import com.gennext.offlinegst.global.PopupDialog;
import com.gennext.offlinegst.model.Sort;
import com.gennext.offlinegst.model.user.PurchasesModel;
import com.gennext.offlinegst.model.user.ServicesPurchaseAdapter;
import com.gennext.offlinegst.search.FilterBarView;
import com.gennext.offlinegst.search.SearchBar;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.user.purchas.PurchaseManager;
import com.gennext.offlinegst.user.purchas.Purchases;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.ApiCallError;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.RequestBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Admin on 7/4/2017.
 */

public class ServicesPurchase extends CompactFragment implements ApiCallError.ErrorListener, PopupDialog.DialogListener, SearchBar.Search{
    public static final String STOCK_BUY = "stockBuy";
    private RecyclerView lvMain;
    private ArrayList<PurchasesModel> cList;
    private ServicesPurchaseAdapter adapter;
    private DBManager dbManager;

    private AssignTask assignTask;
    private FloatingActionButton btnAdd;
    private CoordinatorLayout coordinatorLayout;
    private TextView tvAddNote;
    private PurchasesModel mSelectedItem;
    private int mSelectedItemPos;
    private int mSelectedTask;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private String mStartDate,mEndDate;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static Fragment newInstance() {
        ServicesPurchase fragment=new ServicesPurchase();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(dbManager!=null)
            dbManager.closeDB();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.user_purchases,container,false);
        initToolBarHome(getActivity(),v,getString(R.string.purchases_service), "servicesPurchase");
        screenAnalytics(getContext(),AppUser.getUserId(getContext()),"ServicesPurchase");
        dbManager= DBManager.newIsntance(getActivity());
        initDate();
        initUi(v);
        addSearchTag(savedInstanceState);
        return v;
    }

    private void initDate() {
        //Calendar set to the current date
        Calendar calendar = Calendar.getInstance();
        int eYear = calendar.get(Calendar.YEAR);
        int eMonth = calendar.get(Calendar.MONTH);
        int eDay = calendar.get(Calendar.DAY_OF_MONTH);
        mEndDate = DatePickerDialog.getFormattedDate(eDay, eMonth, eYear);

        //rollback 90 days
        calendar.add(Calendar.DAY_OF_YEAR, -FilterBarView.MONTH_GAP);
        //now the date is 90 days back
        int sYear = calendar.get(Calendar.YEAR);
        int sMonth = calendar.get(Calendar.MONTH);
        int sDay = calendar.get(Calendar.DAY_OF_MONTH);
        mStartDate = DatePickerDialog.getFormattedDate(sDay, sMonth, sYear);

    }

    private void addSearchTag(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            Fragment fragment = SearchBar.newInstance(ServicesPurchase.this, mStartDate, mEndDate, "Search by name, date, invoice no.");
            AppAnimation.setSlideAnimation(fragment, Gravity.TOP);
            getFragmentManager().beginTransaction()
                    .replace(R.id.container_bar, fragment, "searchBar")
                    .commit();
        }
    }

    @Override
    public void onTextChanged(CharSequence charSequence) {
        if (adapter != null)
            adapter.getFilter().filter(charSequence.toString());
    }

    @Override
    public void onShortButtonPressed(String type) {
        sortArray(type);
    }
    private void sortArray(String type) {
        if (cList==null){
            return;
        }
        if(type.equals(Sort.Type1)) {
            Collections.sort(cList, new Comparator<PurchasesModel>() {
                @Override
                public int compare(PurchasesModel item, PurchasesModel item2) {
                    Date date = getDate(item.getInvoiceDate());
                    Date date2 = getDate(item2.getInvoiceDate());
                    return date2.compareTo(date);
                }
            });
        } else if(type.equals(Sort.Type2)) {
            Collections.sort(cList, new Comparator<PurchasesModel>() {
                @Override
                public int compare(PurchasesModel item, PurchasesModel item2) {
                    String s1 = item.getVendorName();
                    String s2 = item2.getVendorName();
                    return s1.compareToIgnoreCase(s2);
                }
            });
        }else if(type.equals(Sort.Type3)) {
            Collections.sort(cList, new Comparator<PurchasesModel>() {
                @Override
                public int compare(PurchasesModel item, PurchasesModel item2) {
                    String s1 = item.getInvoiceNumber();
                    String s2 = item2.getInvoiceNumber();
//                    Integer data = Integer.valueOf(32);
//                    data.compareTo()
                    return s2.compareToIgnoreCase(s1);
                }
            });
        }
        adapter.notifyDataSetChanged();

    }
    @Override
    public void onFilterButtonPressed(String startDate, String endDate) {
        this.mStartDate = startDate;
        this.mEndDate = endDate;

        executeTask(TASK_LOAD_LIST);
    }

    private Date getDate(String invoiceDate) {
        Date date = new Date();
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
            date = format.parse(invoiceDate);
            //calculate the difference in days here
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return date;
    }
    private void initUi(View v) {
        coordinatorLayout = (CoordinatorLayout)v.findViewById(R.id.coordinatorLayout);
        tvAddNote = (TextView) v.findViewById(R.id.tv_add_note);
        tvAddNote.setText(getString(R.string.purchase_add_message));
        lvMain = (RecyclerView)v.findViewById(R.id.lv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());
        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                executeTask(TASK_LOAD_LIST);
            }
        });
        btnAdd = (FloatingActionButton) v.findViewById(R.id.fab);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFragment(ServicePurchaseManager.newInstance(ServicesPurchase.this,null , PurchaseManager.ADD_PURCHASE),"servicePurchaseManager");
            }
        });
        lvMain.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && btnAdd.getVisibility() == View.VISIBLE) {
                    btnAdd.hide();
                } else if (dy < 0 && btnAdd.getVisibility() != View.VISIBLE) {
                    btnAdd.show();
                }
            }
        });
        executeTask(TASK_LOAD_LIST);
    }



    private void executeTask(int task) {
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
        mSelectedTask=task;
        if(task==TASK_LOAD_LIST) {
            assignTask = new AssignTask(getActivity());
            assignTask.execute(AppSettings.GET_SERVICE_PURCHASES);
        }else{
            assignTask = new AssignTask(getActivity());
            assignTask.execute(AppSettings.DELETE_PURCHASE);
        }
    }
    private void hideProgressBar() {
        if(mSwipeRefreshLayout!=null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        executeTask(mSelectedTask);
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }

    public void refreshList() {
        executeTask(TASK_LOAD_LIST);
    }

    public void deleteServicesPurchase(PurchasesModel itemServicesPurchase, int position) {
        mSelectedItem = itemServicesPurchase;
        mSelectedItemPos = position;
        PopupDialog.newInstance(getString(R.string.delete_invoice), getString(R.string.are_you_sure_to_delete)+" " + itemServicesPurchase.getInvoiceNumber() +" " + getString(R.string.purchase_lowercase), ServicesPurchase.this)
                .show(getFragmentManager(), "popupDialog");
    }

    public void editServicesPurchase(PurchasesModel itemServicesPurchase) {
        addFragment(ServicePurchaseManager.newInstance(ServicesPurchase.this,itemServicesPurchase,PurchaseManager.EDIT_PURCHASE),"servicePurchaseManager");
    }

    public void copyServicesPurchase(PurchasesModel itemServicesPurchase) {
        addFragment(ServicePurchaseManager.newInstance(ServicesPurchase.this,itemServicesPurchase,PurchaseManager.COPY_PURCHASE),"servicePurchaseManager");
    }

    @Override
    public void onOkClick(DialogFragment dialog) {
        executeTask(TASK_DELETE);
    }

    @Override
    public void onCancelClick(DialogFragment dialog) {

    }

    public void viewServicesPurchase(PurchasesModel itemServicesPurchase) {
        addFragment(ServicePurchaseManager.newInstance(ServicesPurchase.this,itemServicesPurchase,PurchaseManager.VIEW_PURCHASE),"servicePurchaseManager");
    }


    private class AssignTask extends AsyncTask<String, Void, PurchasesModel> {
        private Context context;

        private void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        private void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        private AssignTask(Context context ) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected PurchasesModel doInBackground(String... urls) {
            String userId = AppUser.getUserId(context);
            String profileId = AppUser.getProfileId(context);
            PurchasesModel result = new PurchasesModel();
//            if (AppConfig.isUpdateToServer(context)) {
            if(mSelectedTask==TASK_LOAD_LIST) {
                String response = ApiCall.POST(urls[0], RequestBuilder.DefaultType(userId,profileId,"G",mStartDate,mEndDate));
                return JsonParser.getServicesPurchaseDetail(context,response);
            }else{
                String invNo,vendorId;
                String type="S";
                if( mSelectedItem!=null){
                    invNo = mSelectedItem.getInvoiceNumber();
                    vendorId = mSelectedItem.getVendorId();
                }else{
                    invNo="";
                    vendorId="";
                }
                String response = ApiCall.POST(urls[0], RequestBuilder.deletePurchase(type,userId, profileId, invNo, vendorId));
                return JsonParser.cancelPurchaseInvoice(response);
            }
//            }
//            return dbManager.getServicesPurchaseList(result,userId,profileId);
        }


        @Override
        protected void onPostExecute(PurchasesModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressBar();
                if (!AppConfig.isUpdateToServer(context)) {
                    if (result.getOutputDB().equals(Const.SUCCESS)) {
                        cList=result.getList();
                        adapter = new ServicesPurchaseAdapter(getActivity(), cList,ServicesPurchase.this);
                        lvMain.setAdapter(adapter);
                        tvAddNote.setVisibility(View.GONE);
                    } else if (result.getOutputDB().equals(Const.FAILURE)) {
//                        showSnakBar(coordinatorLayout,result.getOutputDBMsg());
                        addFragment(ServicePurchaseManager.newInstance(ServicesPurchase.this,null,PurchaseManager.ADD_PURCHASE),"servicePurchaseManager");
                    }
                } else {
                    if (result.getOutput().equals(Const.SUCCESS)) {
                        if(mSelectedTask==TASK_LOAD_LIST) {
                            cList=result.getList();
                            adapter = new ServicesPurchaseAdapter(getActivity(), cList,ServicesPurchase.this);
                            lvMain.setAdapter(adapter);
                            tvAddNote.setVisibility(View.GONE);
                        }else{
//                            if(mSelectedItem!=null) {
//                                adapter.deleteItem(mSelectedItemPos);
//                            }
                            refreshList();
                            showSnakBar(coordinatorLayout, result.getOutputMsg());
                        }
                    } else if (result.getOutput().equals(Const.FAILURE)) {
                        showSnakBar(coordinatorLayout,result.getOutputMsg());
                    } else {
                        ApiCallError.newInstance(result.getOutput(),result.getOutputMsg(), ServicesPurchase.this)
                                .show(getFragmentManager(), "apiCallError");
                    }
                }
            }
        }
    }

}
