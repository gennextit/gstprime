package com.gennext.offlinegst.user.purchas;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gennext.offlinegst.MainActivity;
import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.PopupAlert;
import com.gennext.offlinegst.model.user.CreditorsAdapter;
import com.gennext.offlinegst.model.user.PaymentModel;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.ApiCallError;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.RequestBuilder;

import java.util.ArrayList;

/**
 * Created by Admin on 7/4/2017.
 */

public class Creditors extends CompactFragment implements ApiCallError.ErrorListener {
    private RecyclerView lvMain;
    private ArrayList<PaymentModel> cList;
    private CreditorsAdapter adapter;
    private DBManager dbManager;

    private AssignTask assignTask;
    private CoordinatorLayout coordinatorLayout;
    private FloatingActionButton btnAdd;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static Fragment newInstance() {
        Creditors fragment = new Creditors();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(dbManager!=null)
            dbManager.closeDB();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_debtors, container, false);
        ((MainActivity) getActivity()).updateTitle(getString(R.string.creditors));
        screenAnalytics(getContext(), AppUser.getUserId(getContext()), "Creditors");
        dbManager = DBManager.newIsntance(getActivity());
        initUi(v);
        return v;
    }

    private void initUi(View v) {
        coordinatorLayout = (CoordinatorLayout) v.findViewById(R.id.coordinatorLayout);
        lvMain = (RecyclerView) v.findViewById(R.id.lv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());

        btnAdd = (FloatingActionButton) v.findViewById(R.id.fab);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFragment(CreditorsTransaction.newInstance(Creditors.this),"creditorsTransaction");
            }
        });
        executeTask();
    }


    private void executeTask() {
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.GET_PRODUCTS);
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        executeTask();
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }

    public void refreshList() {
        executeTask();
    }


    private class AssignTask extends AsyncTask<String, Void, PaymentModel> {
        private Context context;

        private void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        private void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        private AssignTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected PaymentModel doInBackground(String... urls) {
            String userId = AppUser.getUserId(context);
            String profileId = AppUser.getProfileId(context);
            PaymentModel result = new PaymentModel();
            if (AppConfig.isUpdateToServer(context)) {
                String response = ApiCall.POST(urls[0], RequestBuilder.Default(userId));
                result = JsonParser.getCreditorsDetail(response);
            }
            return dbManager.getCreditorsListWithStock(result, userId, profileId);
        }


        @Override
        protected void onPostExecute(PaymentModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                if (!AppConfig.isUpdateToServer(context)) {
                    if (result.getOutputDB().equals(Const.SUCCESS)) {
                        cList = result.getList();
                        if(cList.size()==0){
                            showSnakBar(coordinatorLayout,getString(R.string.no_creditors_available));
                        }
                        adapter = new CreditorsAdapter(getActivity(), cList, Creditors.this);
                        lvMain.setAdapter(adapter);
                    } else if (result.getOutputDB().equals(Const.FAILURE)) {
                        showSnakBar(coordinatorLayout,result.getOutputDBMsg());
                    }
                } else {
                    if (result.getOutput().equals(Const.SUCCESS)) {
                        if (!result.getOutputDB().equals(Const.SUCCESS)) {
                            showSnakBar(coordinatorLayout, result.getOutputDBMsg());
                        }
                        if(cList.size()==0){
                            showSnakBar(coordinatorLayout,getString(R.string.no_creditors_available));
                        }
                        cList = result.getList();
                        adapter = new CreditorsAdapter(getActivity(), cList, Creditors.this);
                        lvMain.setAdapter(adapter);
                    } else if (result.getOutput().equals(Const.FAILURE)) {
                        if (result.getOutputDB().equals(Const.SUCCESS)) {
                            showSnakBar(coordinatorLayout, "Local " + result.getOutputDBMsg());
                        }
                        PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(), "popupAlert");
                    } else {
                        if (result.getOutputDB().equals(Const.SUCCESS)) {
                            showSnakBar(coordinatorLayout, "Local " + result.getOutputDBMsg());
                        }
                        ApiCallError.newInstance(result.getOutput(),result.getOutputMsg(), Creditors.this)
                                .show(getFragmentManager(), "apiCallError");
                    }
                }
            }
        }
    }

}

