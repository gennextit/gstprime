package com.gennext.offlinegst.setting;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;

/**
 * Created by Admin on 7/4/2017.
 */

public class DashboardSetting extends CompactFragment implements CompoundButton.OnCheckedChangeListener {
 
    private Switch swDashboardMenu, swSalePurchaseChart, swTaxPayableChart, swDebtorCreditorChart;
    private Context mContext;

    public static Fragment newInstance() {
        DashboardSetting fragment = new DashboardSetting();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.setting_dashboard, container, false);
        initToolBar(getActivity(), v, getString(R.string.dashboard_setting));
        mContext = getContext(); 
        initUi(v);
        updateUi();
        return v;
    }

    private void updateUi() {
        swDashboardMenu.setChecked(AppConfig.getDashboardMenu(mContext));
        swSalePurchaseChart.setChecked(AppConfig.getSalePurchaseChart(mContext));
        swDebtorCreditorChart.setChecked(AppConfig.getDebtorCreditorChart(mContext));
        swTaxPayableChart.setChecked(AppConfig.getTaxPayableChart(mContext));
    }

    private void initUi(View v) {
        swDashboardMenu = (Switch) v.findViewById(R.id.sw_dashboard_menu);
        swSalePurchaseChart = (Switch) v.findViewById(R.id.sw_sale_purchase);
        swDebtorCreditorChart = (Switch) v.findViewById(R.id.sw_debtor_creditor);
        swTaxPayableChart = (Switch) v.findViewById(R.id.sw_tax_payable);

        swDashboardMenu.setOnCheckedChangeListener(this);
        swSalePurchaseChart.setOnCheckedChangeListener(this);
        swDebtorCreditorChart.setOnCheckedChangeListener(this);
        swTaxPayableChart.setOnCheckedChangeListener(this);

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.sw_dashboard_menu:
                AppConfig.setDashboardMenu(mContext, isChecked);
                break;
            case R.id.sw_sale_purchase:
                AppConfig.setSalePurchaseChart(mContext, isChecked);
                break;
            case R.id.sw_debtor_creditor:
                AppConfig.setDebtorCreditorChart(mContext, isChecked);
                break;
            case R.id.sw_tax_payable:
                AppConfig.setTaxPayableChart(mContext, isChecked);
                break;

        }
    }
}
