package com.gennext.offlinegst.user.vendor;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gennext.offlinegst.MainActivity;
import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.PopupAlert;
import com.gennext.offlinegst.global.PopupDialog;
import com.gennext.offlinegst.model.Sort;
import com.gennext.offlinegst.model.user.VendorModel;
import com.gennext.offlinegst.model.user.VendorAdapter;
import com.gennext.offlinegst.model.user.VendorModel;
import com.gennext.offlinegst.search.SearchBar;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.user.customer.Customer;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.ApiCallError;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.RequestBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Admin on 7/4/2017.
 */

public class Vendor extends CompactFragment implements ApiCallError.ErrorListener ,PopupDialog.DialogListener,SearchBar.SearchOrder{
    private RecyclerView lvMain;
    private ArrayList<VendorModel> cList;
    private VendorAdapter adapter;
    private DBManager dbManager;

    private AssignTask assignTask;
    private FloatingActionButton btnAdd;
    private CoordinatorLayout coordinatorLayout;
    private TextView tvAddNote;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private VendorModel mSelectedItem;
    private int mSelectedItemPos;
    private int mSelectedTask;
    private String mSortType;
    private int mListOrder;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static Fragment newInstance() {
        Vendor fragment = new Vendor();
        AppAnimation.setFadeAnimation(fragment);
        fragment.mListOrder = Sort.Ascending;
        fragment.mSortType = Sort.Type1;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_vendor, container, false);
        initToolBarHome(getActivity(),v,getString(R.string.vendor), "vendor");
        screenAnalytics(getContext(),AppUser.getUserId(getContext()),"Vendor");
        dbManager = DBManager.newIsntance(getActivity());
        initUi(v);
        addSearchTag(savedInstanceState);
        return v;
    }

    private void addSearchTag(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            Fragment fragment = SearchBar.newInstance(Vendor.this, new String[]{"Name", "State", "GSTIN"},"Search by name, state or GSTIN");
            AppAnimation.setSlideAnimation(fragment, Gravity.TOP);
            getFragmentManager().beginTransaction()
                    .replace(R.id.container_bar, fragment , "searchBar")
                    .commit();
        }
    }
    @Override
    public void onTextChanged(CharSequence charSequence) {
        if (adapter != null)
            adapter.getFilter().filter(charSequence.toString());
    }

    @Override
    public void onShortButtonPressed(String type) {
        mSortType = type;
        if (cList==null){
            return;
        }
        if(type.equals(Sort.Type1)) {
            Collections.sort(cList, new Comparator<VendorModel>() {
                @Override
                public int compare(VendorModel item, VendorModel item2) {
                    String s1 = item.getPersonName();
                    String s2 = item2.getPersonName();
                    if(mListOrder == Sort.Ascending) {
                        return s1.compareToIgnoreCase(s2);
                    }else{
                        return s2.compareToIgnoreCase(s1);
                    }
                }
            });
        } else if(type.equals(Sort.Type2)) {
            Collections.sort(cList, new Comparator<VendorModel>() {
                @Override
                public int compare(VendorModel item, VendorModel item2) {
                    String s1 = item.getState();
                    String s2 = item2.getState();
                    if(mListOrder == Sort.Ascending) {
                        return s1.compareToIgnoreCase(s2);
                    }else{
                        return s2.compareToIgnoreCase(s1);
                    }
                }
            });
        }else if(type.equals(Sort.Type3)) {
            Collections.sort(cList, new Comparator<VendorModel>() {
                @Override
                public int compare(VendorModel item, VendorModel item2) {
                    String s1 = item.getGSTIN();
                    String s2 = item2.getGSTIN();
                    if(mListOrder == Sort.Ascending) {
                        return s1.compareToIgnoreCase(s2);
                    }else{
                        return s2.compareToIgnoreCase(s1);
                    }
                }
            });
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onShortOrderPressed(int order) {
        mListOrder = order;
        if(mSortType!=null){
            onShortButtonPressed(mSortType);
        }
    }


    private void initUi(View v) {
        coordinatorLayout = (CoordinatorLayout) v.findViewById(R.id.coordinatorLayout);
        tvAddNote = (TextView) v.findViewById(R.id.tv_add_note);
        tvAddNote.setText(getString(R.string.vendor_add_message));
        lvMain = (RecyclerView) v.findViewById(R.id.lv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());
        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                executeTask(TASK_LOAD_LIST);
            }
        });

        btnAdd = (FloatingActionButton) v.findViewById(R.id.fab);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFragment(VendorManager.newInstance(Vendor.this), "vendorManager");
            }
        });
        lvMain.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && btnAdd.getVisibility() == View.VISIBLE) {
                    btnAdd.hide();
                } else if (dy < 0 && btnAdd.getVisibility() != View.VISIBLE) {
                    btnAdd.show();
                }
            }
        });
        executeTask(TASK_LOAD_LIST);
    }


    private void executeTask(int task) {
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
        mSelectedTask=task;
        if(task==TASK_LOAD_LIST) {
            assignTask = new AssignTask(getActivity());
            assignTask.execute(AppSettings.GET_VENDORS);
        }else{
            assignTask = new AssignTask(getActivity());
            assignTask.execute(AppSettings.DELETE_VENDOR);
        }
    }

    private void hideProgressBar() {
        if(mSwipeRefreshLayout!=null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        executeTask(mSelectedTask);
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }

    public void refreshList() {
        executeTask(TASK_LOAD_LIST);
    }

    public void deleteVendor(VendorModel itemVendor, int position) {
//        String result = dbManager.deleteVendor(itemVendor);
//        showSnakBar(coordinatorLayout, result);
        mSelectedItem = itemVendor;
        mSelectedItemPos = position;
        PopupDialog.newInstance(getString(R.string.delete_vendor), getString(R.string.are_you_sure_to_delete)+" " + mSelectedItem.getPersonName() +" " + getString(R.string.vendor_lowercase), Vendor.this)
                .show(getFragmentManager(), "popupDialog");
    }

    public void editVendor(VendorModel itemVendor) {
        addFragment(VendorManager.newInstance(Vendor.this,itemVendor, VendorManager.EDIT_VENDOR), "vendorManager");
    }

    public void copyVendor(VendorModel itemVendor) {
        addFragment(VendorManager.newInstance(Vendor.this,itemVendor, VendorManager.COPY_VENDOR), "vendorManager");
    }

    @Override
    public void onOkClick(DialogFragment dialog) {
        executeTask(TASK_DELETE);
    }

    @Override
    public void onCancelClick(DialogFragment dialog) {

    }

    public void viewVendor(VendorModel itemVendor) {
        addFragment(VendorManager.newInstance(Vendor.this,itemVendor, VendorManager.VIEW_VENDOR), "vendorManager");
    }


    private class AssignTask extends AsyncTask<String, Void, VendorModel> {
        private Context context;

        private void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        private void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        private AssignTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected VendorModel doInBackground(String... urls) {
            String userId = AppUser.getUserId(context);
            String setupId = AppUser.getCompanyId(context);
            VendorModel result = new VendorModel();
//            if (AppConfig.isUpdateToServer(context)) {
            if(mSelectedTask==TASK_LOAD_LIST) {
                String response = ApiCall.POST(urls[0], RequestBuilder.Default(userId,setupId));
                return JsonParser.getVendorDetail(context,new VendorModel(),response);
            }else{
                String id = null;
                if( mSelectedItem!=null){
                    id = mSelectedItem.getVendorId();
                }
                String response = ApiCall.POST(urls[0], RequestBuilder.deleteVendor(userId, setupId, id != null ? id : ""));
                return JsonParser.deleteVendor(response);
            }
//            }
//            return dbManager.getVendorList(result,userId);
        }


        @Override
        protected void onPostExecute(VendorModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressBar();
                if (!AppConfig.isUpdateToServer(context)) {
                    if (result.getOutputDB().equals(Const.SUCCESS)) {
                        cList = result.getList();
                        adapter = new VendorAdapter(getActivity(), cList, Vendor.this);
                        lvMain.setAdapter(adapter);
                        tvAddNote.setVisibility(View.GONE);
                    } else if (result.getOutputDB().equals(Const.FAILURE)) {
//                        showSnakBar(coordinatorLayout, result.getOutputDBMsg());
                        addFragment(VendorManager.newInstance(Vendor.this), "vendorManager");
                    }
                } else {
                    if (result.getOutput().equals(Const.SUCCESS)) {
                        if(mSelectedTask==TASK_LOAD_LIST) {
                            cList = result.getList();
                            adapter = new VendorAdapter(getActivity(), cList, Vendor.this);
                            lvMain.setAdapter(adapter);
                            onShortButtonPressed(mSortType);
                            tvAddNote.setVisibility(View.GONE);
                        }else{
                            if(mSelectedItem!=null) {
                                adapter.itemDeletedSuccessful(mSelectedItemPos);
                            }
                            showSnakBar(coordinatorLayout, result.getOutputMsg());
                        }
                    } else if (result.getOutput().equals(Const.FAILURE)) {
                        showSnakBar(coordinatorLayout,result.getOutputMsg());
//                        PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
//                                .show(getFragmentManager(), "popupAlert");
                    } else {
                        ApiCallError.newInstance(result.getOutput(),result.getOutputMsg(), Vendor.this)
                                .show(getFragmentManager(), "apiCallError");
                    }
                }
            }
        }
    }

}
