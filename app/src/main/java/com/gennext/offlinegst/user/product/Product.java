package com.gennext.offlinegst.user.product;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.offlinegst.MainActivity;
import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.PopupAlert;
import com.gennext.offlinegst.global.PopupDialog;
import com.gennext.offlinegst.model.EXELMaker;
import com.gennext.offlinegst.model.Sort;
import com.gennext.offlinegst.model.user.ProductModel;
import com.gennext.offlinegst.model.user.ProductAdapter;
import com.gennext.offlinegst.model.user.ProductModel;
import com.gennext.offlinegst.search.SearchBar;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.user.customer.Customer;
import com.gennext.offlinegst.user.invoice.Invoices;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.ApiCallError;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.RequestBuilder;
import com.gennext.offlinegst.util.Utility;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by Admin on 7/4/2017.
 */

public class Product extends CompactFragment implements ApiCallError.ErrorListener, PopupDialog.DialogListener, SearchBar.SearchOrder{
    private RecyclerView lvMain;
    private ArrayList<ProductModel> cList;
    private ProductAdapter adapter;
    private DBManager dbManager;

    private FloatingActionButton btnAdd;
    private CoordinatorLayout coordinatorLayout;
    private TextView tvAddNote;
    private AssignTask assignTask;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ProductModel mSelectedItem;
    private int mSelectedItemPos;
    private int mSelectedTask;
    private EXELMaker exelMaker;
    private String mSortType;
    private int mListOrder;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static Fragment newInstance() {
        Product fragment=new Product();
        AppAnimation.setFadeAnimation(fragment);
        fragment.mListOrder = Sort.Ascending;
        fragment.mSortType = Sort.Type1;
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.user_product,container,false);
        initToolBarHome(getActivity(),v,getString(R.string.product), "product");
        screenAnalytics(getContext(),AppUser.getUserId(getContext()),"Product");
        dbManager= DBManager.newIsntance(getActivity());
        exelMaker= EXELMaker.newInstance(getActivity());
        initUi(v);
        addSearchTag(savedInstanceState);
        return v;
    }

    private void addSearchTag(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            Fragment fragment = SearchBar.newInstance(Product.this, new String[]{"Name", "Stock", "Price"},"Search by name, stock, price");
            AppAnimation.setSlideAnimation(fragment, Gravity.TOP);
            getFragmentManager().beginTransaction()
                    .replace(R.id.container_bar, fragment , "searchBar")
                    .commit();
        }
    }
    @Override
    public void onTextChanged(CharSequence charSequence) {
        if (adapter != null)
            adapter.getFilter().filter(charSequence.toString());
    }

    @Override
    public void onShortButtonPressed(String type) {
        mSortType = type;
        if (cList==null){
            return;
        }
        if(type.equals(Sort.Type1)) {
            Collections.sort(cList, new Comparator<ProductModel>() {
                @Override
                public int compare(ProductModel item, ProductModel item2) {
                    String s1 = item.getProductName();
                    String s2 = item2.getProductName();
                    if(mListOrder == Sort.Ascending) {
                        return s1.compareToIgnoreCase(s2);
                    }else{
                        return s2.compareToIgnoreCase(s1);
                    }
                }
            });
        } else if(type.equals(Sort.Type2)) {
            Collections.sort(cList, new Comparator<ProductModel>() {
                @Override
                public int compare(ProductModel item, ProductModel item2) {
                    Double data = Double.valueOf( item.getQuantity());
                    Double data2 = Double.valueOf( item2.getQuantity());
                    if(mListOrder == Sort.Ascending) {
                        return data.compareTo(data2);
                    }else{
                        return data2.compareTo(data);
                    }
                }
            });
        }else if(type.equals(Sort.Type3)) {
            Collections.sort(cList, new Comparator<ProductModel>() {
                @Override
                public int compare(ProductModel item, ProductModel item2) {
                    Double data = Double.valueOf( item.getSalePrice());
                    Double data2 = Double.valueOf( item2.getSalePrice());
                    if(mListOrder == Sort.Ascending) {
                        return data.compareTo(data2);
                    }else{
                        return data2.compareTo(data);
                    }
                }
            });
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onShortOrderPressed(int order) {
        mListOrder = order;
        if(mSortType!=null){
            onShortButtonPressed(mSortType);
        }
    }

    private void initUi(View v) {
        coordinatorLayout = (CoordinatorLayout)v.findViewById(R.id.coordinatorLayout);
        tvAddNote = (TextView) v.findViewById(R.id.tv_add_note);
        tvAddNote.setText(getString(R.string.product_add_message));
        lvMain = (RecyclerView)v.findViewById(R.id.lv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());
        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                executeTask(TASK_LOAD_LIST);
            }
        });

        btnAdd = (FloatingActionButton) v.findViewById(R.id.fab);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 addFragment(AddProduct.newInstance(Product.this),"addProduct");
            }
        });
        lvMain.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && btnAdd.getVisibility() == View.VISIBLE) {
                    btnAdd.hide();
                } else if (dy < 0 && btnAdd.getVisibility() != View.VISIBLE) {
                    btnAdd.show();
                }
            }
        });
        executeTask(TASK_LOAD_LIST);
    }



    private void executeTask(int task) {
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
        mSelectedTask=task;
        if(task==TASK_LOAD_LIST) {
            assignTask = new AssignTask(getActivity());
            assignTask.execute(AppSettings.GET_PRODUCTS);
        }else{
            assignTask = new AssignTask(getActivity());
            assignTask.execute(AppSettings.DELETE_PRODUCT);
        }
    }

    private void hideProgressBar() {
        if(mSwipeRefreshLayout!=null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        executeTask(mSelectedTask);
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }

    public void refreshList() {
        executeTask(TASK_LOAD_LIST);
    }

    public void deleteProduct(ProductModel itemProduct, int position) {
//        String result=dbManager.deleteProduct(itemProduct);
//        showSnakBar(coordinatorLayout,result);
        mSelectedItem = itemProduct;
        mSelectedItemPos = position;
        PopupDialog.newInstance(getString(R.string.delete_product), getString(R.string.are_you_sure_to_delete)+" " + mSelectedItem.getProductName() +" " + getString(R.string.product_lowercase), Product.this)
                .show(getFragmentManager(), "popupDialog");
    }

    public void editProduct(ProductModel itemProduct) {
        addFragment(AddProduct.newInstance(itemProduct,AddProduct.EDIT_PRODUCT,Product.this),"addProduct");
    }

    public void copyProduct(ProductModel itemProduct) {
        addFragment(AddProduct.newInstance(itemProduct,AddProduct.COPY_PRODUCT,Product.this),"addProduct");
    }

    @Override
    public void onOkClick(DialogFragment dialog) {
        executeTask(TASK_DELETE);
    }

    @Override
    public void onCancelClick(DialogFragment dialog) {

    }

    public void exportInExel() {
        File mOutputFile = exelMaker.exportProductInExel(cList);
        if(mOutputFile!=null){
            Uri uri = Utility.getUriFromFile(getActivity(),mOutputFile);
            Intent in = new Intent(Intent.ACTION_SEND);
            in.setDataAndType(uri, "application/vnd.ms-excel");
            in.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            in.putExtra(Intent.EXTRA_STREAM, uri);
            try {
                startActivity(Intent.createChooser(in, "Share With Fiends"));
            } catch (Exception e) {
                Toast.makeText(getActivity(), "Error: Cannot open or share created PDF report.", Toast.LENGTH_SHORT).show();
            }

        }
    }

    public void viewProduct(ProductModel itemProduct) {
        addFragment(AddProduct.newInstance(itemProduct,AddProduct.VIEW_PRODUCT,Product.this),"addProduct");
    }



    private class AssignTask extends AsyncTask<String, Void, ProductModel> {
        private Context context;

        private void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        private void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        private AssignTask(Context context ) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ProductModel doInBackground(String... urls) {
            String userId = AppUser.getUserId(context);
            String companyId = AppUser.getCompanyId(context);
            ProductModel result = new ProductModel();
//            if (AppConfig.isUpdateToServer(context)) {
            if(mSelectedTask==TASK_LOAD_LIST) {
                String response = ApiCall.POST(urls[0], RequestBuilder.Default(userId,companyId));
                return JsonParser.getProductDetail(context,new ProductModel(),response);
            }else{
                String id = null;
                if( mSelectedItem!=null){
                    id = mSelectedItem.getProductId();
                }
                String response = ApiCall.POST(urls[0], RequestBuilder.deleteItem(userId, companyId, id != null ? id : ""));
                return JsonParser.deleteProduct(response);
            }
//            }
//            return dbManager.getProductList(result,userId);
//            return dbManager.getProductListWithStock(result,userId,companyId);
        }


        @Override
        protected void onPostExecute(ProductModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressBar();
                if (!AppConfig.isUpdateToServer(context)) {
                    if (result.getOutputDB().equals(Const.SUCCESS)) {
                        cList=result.getList();
                        adapter = new ProductAdapter(getActivity(), cList,Product.this);
                        lvMain.setAdapter(adapter);
                        tvAddNote.setVisibility(View.GONE);
                    } else if (result.getOutputDB().equals(Const.FAILURE)) {
//                        showSnakBar(coordinatorLayout,result.getOutputDBMsg());
                        addFragment(AddProduct.newInstance(Product.this),"addProduct");
                    }
                } else {
                    if (result.getOutput().equals(Const.SUCCESS)) {
                        if(mSelectedTask==TASK_LOAD_LIST) {
                            cList=result.getList();
                            adapter = new ProductAdapter(getActivity(), cList,Product.this);
                            lvMain.setAdapter(adapter);
                            onShortButtonPressed(mSortType);
                            tvAddNote.setVisibility(View.GONE);
                        }else{
                            if(mSelectedItem!=null) {
                                adapter.itemDeletedSuccessful(mSelectedItemPos);
                            }
                            showSnakBar(coordinatorLayout, result.getOutputMsg());
                        }
                    } else if (result.getOutput().equals(Const.FAILURE)) {
                        showSnakBar(coordinatorLayout,result.getOutputMsg());
//                        PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
//                                .show(getFragmentManager(), "popupAlert");
                    } else {
                        ApiCallError.newInstance(result.getOutput(),result.getOutputMsg(), Product.this)
                                .show(getFragmentManager(), "apiCallError");
                    }
                }
            }
        }
    }

}
