package com.gennext.offlinegst.user.product;


/**
 * Created by Admin on 5/22/2017.
 */

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.DatePickerDialog;
import com.gennext.offlinegst.model.user.CommonModel;
import com.gennext.offlinegst.model.user.ProductModel;
import com.gennext.offlinegst.panel.TaxCategorySelector;
import com.gennext.offlinegst.panel.TaxSelector;
import com.gennext.offlinegst.panel.UnitSelector;
import com.gennext.offlinegst.user.invoice.InvoiceManager;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.FieldValidation;
import com.gennext.offlinegst.util.ProgressButton;
import com.gennext.offlinegst.util.Utility;


public class EditInvoiceProduct extends CompactFragment implements TaxCategorySelector.SelectListener, UnitSelector.SelectListener, TaxSelector.SelectListener, DatePickerDialog.DateSelectFlagListener {

    public static final int ADD_PRODUCT_WITH_RESPONSE = 0, ADD_PRODUCT = 1, EDIT_PRODUCT = 2, COPY_PRODUCT = 3;
    private EditText etQuantity,etName, etHSN, etCostPrice, etSalePrice, etTaxRate,etTaxCategory, etBatch, etExpiry;
    private static final int TYPE_RUPEE = 0,TYPE_PERCENT=1;
    private CoordinatorLayout coordinatorLayout;
    private ProductModel itemProduct;
    private EditText etBarcode;


    private int sDay, sMonth, sYear;

    private int eDay, eMonth, eYear;
    private InvoiceManager invoiceManager;
    private String mTaxRate;
    private EditText etProductMRP,etNetAmount,etDiscount,etTaxable;
    private ProgressButton btnAction;
    private EditText etUnit,etCessRate;
    private String mSelectedUnitCode,mSelectedUnitName;
    private String mTaxCategoryId="",mTaxCategoryName;
    private RadioButton rbRupee;
    private int mDiscountType;
    private float mDiscountValue;
//    private String mNetAmount;
    private String mNetAmtForSingleProd;


    public static Fragment newInstance(ProductModel itemProduct, InvoiceManager invoiceManager) {
        EditInvoiceProduct fragment = new EditInvoiceProduct();
        fragment.itemProduct = itemProduct;
        fragment.invoiceManager = invoiceManager;
        AppAnimation.setSlideAnimation(fragment, Gravity.BOTTOM);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.dialog_change_price, container, false);

        initToolBar(getActivity(), v, getString(R.string.product));
        initUI(v);

        updateUi();
        return v;
    }


    private void updateUi() {
        if (itemProduct != null) {
            etQuantity.setText(itemProduct.getQuantity() != null ? itemProduct.getQuantity() : "");
            etName.setText(itemProduct.getProductName() != null ? itemProduct.getProductName() : "");
            etHSN.setText(itemProduct.getCodeHSN() != null ? itemProduct.getCodeHSN() : "");
            etBarcode.setText(itemProduct.getBarcode() != null ? itemProduct.getBarcode() : "");
            etCostPrice.setText(itemProduct.getCostPrice() != null ? itemProduct.getCostPrice() : "");
            etSalePrice.setText(itemProduct.getSalePrice() != null ? itemProduct.getSalePrice() : "");
            etTaxRate.setText(itemProduct.getTaxRate() != null ? itemProduct.getTaxRate() : "");
            etProductMRP.setText(itemProduct.getMrp() != null ? itemProduct.getMrp() : "");

            mTaxRate=itemProduct.getTaxRate();
            if(AppUser.getCompositionScheme(getContext())){
                mTaxRate="0";
                etTaxRate.setText("0");
            }else{
                if(mTaxRate.equals("0")||mTaxRate.equals("00")||mTaxRate.equals("0.0")){
                    etTaxCategory.setVisibility(View.VISIBLE);
                }else{
                    etTaxCategory.setVisibility(View.GONE);
                }
                etTaxCategory.setText(mTaxCategoryName);
            }
            mTaxCategoryId=itemProduct.getTaxCategoryId();
            mTaxCategoryName=itemProduct.getTaxCategory();

            etUnit.setText(itemProduct.getUnitName() != null ? itemProduct.getUnitName() : "");
            mSelectedUnitName=itemProduct.getUnitName();
            mSelectedUnitCode=itemProduct.getUnitCode();
            etCessRate.setText(itemProduct.getCessRate() != null ? itemProduct.getCessRate() : "");
            String itemSale=itemProduct.getSalePrice();
            String taxRate=itemProduct.getTaxRate();

            etBatch.setText(itemProduct.getBatch() != null ? itemProduct.getBatch() : "");
            etExpiry.setText(itemProduct.getExp() != null ? itemProduct.getExp() : "");
            rbRupee.setChecked(true);
            etDiscount.setText(itemProduct.getDis());

            if(itemSale!=null&& taxRate!=null){
                setTaxableAmount(etSalePrice.getText().toString(),etQuantity.getText().toString());
                setProductNetAmount(mDiscountType,parseFloat(etDiscount.getText().toString()), parseFloat(etQuantity.getText().toString()), parseFloat(etSalePrice.getText().toString()), parseFloat(etTaxRate.getText().toString()), parseFloat(etCessRate.getText().toString()));
            }
        }
    }

    private void setTaxableAmount(String salePrice, String quantity) {
        float mSalePrice = parseFloat(salePrice);
        float mQuantity = parseFloat(quantity);
        etTaxable.setText(Utility.decimalFormat((mSalePrice*mQuantity), "#.##"));
    }

    private void setProductNetAmount(int discountType,float discount,float quantity ,float salePrice, float taxRate, float cessRate) {
        if(discountType==TYPE_RUPEE) {
            mDiscountValue=discount;
            float afterDiscount = ((salePrice*quantity)-discount);
            float totalTax = (afterDiscount * (taxRate + cessRate)) / 100;
//            mNetAmount=Utility.decimalFormat((afterDiscount+totalTax), "#.##");
            etNetAmount.setText(Utility.decimalFormat((afterDiscount+totalTax), "#.##"));
            mNetAmtForSingleProd=Utility.decimalFormat((afterDiscount+totalTax)/quantity, "#.##");
        }else{
            float afterDiscount = ((salePrice*quantity) * discount) / 100;
            mDiscountValue=afterDiscount;
            float discValue=salePrice - afterDiscount;
            float totalTax = (discValue * (taxRate + cessRate)) / 100;
//            mNetAmount=Utility.decimalFormat((discValue+totalTax), "#.##");
            etNetAmount.setText(Utility.decimalFormat((discValue+totalTax), "#.##"));
            mNetAmtForSingleProd=Utility.decimalFormat((discValue+totalTax)/quantity, "#.##");
        }
    }
    private void initUI(View v) {
        coordinatorLayout = (CoordinatorLayout) v.findViewById(R.id.coordinatorLayout);
        etQuantity = (EditText) v.findViewById(R.id.et_product_qty);
        etName = (EditText) v.findViewById(R.id.et_product_name);
        etUnit = (EditText) v.findViewById(R.id.et_product_unit);
        etHSN = (EditText) v.findViewById(R.id.et_product_hsn);
        etBarcode = (EditText) v.findViewById(R.id.et_product_barcode);
        etCostPrice = (EditText) v.findViewById(R.id.et_product_cost);
        etSalePrice = (EditText) v.findViewById(R.id.et_product_sale);
        etTaxRate = (EditText) v.findViewById(R.id.et_product_tax);
        etTaxCategory = (EditText) v.findViewById(R.id.et_product_tax_category);
        etBatch = (EditText) v.findViewById(R.id.et_product_batch);
        etExpiry = (EditText) v.findViewById(R.id.et_product_exp);
        etCessRate = (EditText) v.findViewById(R.id.et_product_cess);
        etProductMRP = (EditText) v.findViewById(R.id.et_product_mrp);
        etNetAmount = (EditText) v.findViewById(R.id.et_product_net_amount);
        etDiscount = (EditText) v.findViewById(R.id.et_product_discount);
        etTaxable = (EditText) v.findViewById(R.id.et_product_net_taxable);

        if(AppUser.getCompositionScheme(getContext())){
            etTaxRate.setVisibility(View.GONE);
            etTaxCategory.setVisibility(View.GONE);
            etCessRate.setVisibility(View.GONE);
            etTaxCategory.setVisibility(View.GONE);
            mTaxRate="0";
            etTaxRate.setText("0");
        }else{
            if(AppConfig.getCessAvailable(getContext())){
                etCessRate.setVisibility(View.VISIBLE);
            }else{
                etCessRate.setVisibility(View.GONE);
            }
        }
        if(AppConfig.isBatchAvailable(getContext())){
            etBatch.setVisibility(View.VISIBLE);
        }else{
            etBatch.setVisibility(View.GONE);
        }
        if(AppConfig.isExpAvailable(getContext())){
            etExpiry.setVisibility(View.VISIBLE);
        }else{
            etExpiry.setVisibility(View.GONE);
        }

        etSalePrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setTaxableAmount(etSalePrice.getText().toString(),etQuantity.getText().toString());
                setProductNetAmount(mDiscountType,parseFloat(etDiscount.getText().toString()), parseFloat(etQuantity.getText().toString()), parseFloat(etSalePrice.getText().toString()), parseFloat(etTaxRate.getText().toString()), parseFloat(etCessRate.getText().toString()));
            }
        });

        etCessRate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setProductNetAmount(mDiscountType,parseFloat(etDiscount.getText().toString()), parseFloat(etQuantity.getText().toString()), parseFloat(etSalePrice.getText().toString()), parseFloat(etTaxRate.getText().toString()), parseFloat(etCessRate.getText().toString()));
            }
        });
        etDiscount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setProductNetAmount(mDiscountType,parseFloat(etDiscount.getText().toString()), parseFloat(etQuantity.getText().toString()), parseFloat(etSalePrice.getText().toString()), parseFloat(etTaxRate.getText().toString()), parseFloat(etCessRate.getText().toString()));
            }
        });

        etQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setTaxableAmount(etSalePrice.getText().toString(),etQuantity.getText().toString());
                setProductNetAmount(mDiscountType,parseFloat(etDiscount.getText().toString()), parseFloat(etQuantity.getText().toString()), parseFloat(etSalePrice.getText().toString()), parseFloat(etTaxRate.getText().toString()), parseFloat(etCessRate.getText().toString()));
            }
        });

        etExpiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeybord(getActivity());
                DatePickerDialog.newInstance(EditInvoiceProduct.this, false, DatePickerDialog.END_DATE, eDay, eMonth, eYear)
                        .show(getFragmentManager(), "datePickerDialog");
            }
        });

        btnAction= ProgressButton.newInstance(getContext(),v);
        btnAction.setText(getString(R.string.apply));

        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!FieldValidation.isProductName(getContext(),etName,true)){
                    return;
                }else if(!FieldValidation.isEmpty(getContext(),mSelectedUnitCode,getString(R.string.please_select_product_unit))){
                    return;
                }else if(!FieldValidation.isHSNCode(getContext(),etHSN,getString(R.string.invalid_hsn_code))){
                    return;
                }else if(!FieldValidation.isEmpty(getContext(),etSalePrice,getString(R.string.invalid_sale_price))){
                    return;
                }else if(!FieldValidation.isEmpty(getContext(),etTaxRate,getString(R.string.invalid_tax_rate))){
                    return;
                }else if (etTaxCategory.getVisibility()==View.VISIBLE && !FieldValidation.isEmpty(getContext(), etTaxCategory,getString(R.string.invalid_tax_category))) {
                    return;
                }else if (!validateMRP(Utility.parseFloat(etNetAmount.getText().toString()),Utility.parseFloat(etProductMRP.getText().toString()))) {
                    return;
                }
                hideKeybord(getActivity());
                executeTask(etName.getText().toString(),etQuantity.getText().toString(), etHSN.getText().toString(), etBarcode.getText().toString(), etCostPrice.getText().toString()
                        , etSalePrice.getText().toString(), etTaxRate.getText().toString(), etBatch.getText().toString(), etExpiry.getText().toString(),mSelectedUnitCode,mSelectedUnitName,etCessRate.getText().toString(),etProductMRP.getText().toString(),etTaxCategory.getText().toString(),mTaxCategoryId,String.valueOf(mDiscountValue));


            }
        });


//        etTaxRate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                hideKeybord(getActivity());
//                TaxSelector.newInstance(EditInvoiceProduct.this).show(getFragmentManager(),"taxSelector");
//            }
//        });
//        etTaxCategory.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                hideKeybord(getActivity());
//                TaxCategorySelector.newInstance(EditInvoiceProduct.this).show(getFragmentManager(),"taxCategorySelector");
//            }
//        });
        etUnit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                hideKeybord(getActivity());
//                UnitSelector.newInstance(EditInvoiceProduct.this).show(getFragmentManager(),"unitSelector");
            }
        });


        RadioGroup rgPaymentType = (RadioGroup) v.findViewById(R.id.rg_discount_type);
        rbRupee = (RadioButton) v.findViewById(R.id.type_rupee);
        mDiscountType=TYPE_PERCENT;
        rgPaymentType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.type_percent:
                        mDiscountType=TYPE_PERCENT;
                        setProductNetAmount(mDiscountType,parseFloat(etDiscount.getText().toString()), parseFloat(etQuantity.getText().toString()), parseFloat(etSalePrice.getText().toString()), parseFloat(etTaxRate.getText().toString()), parseFloat(etCessRate.getText().toString()));
                        break;
                    case R.id.type_rupee:
                        mDiscountType=TYPE_RUPEE;
                        setProductNetAmount(mDiscountType,parseFloat(etDiscount.getText().toString()), parseFloat(etQuantity.getText().toString()), parseFloat(etSalePrice.getText().toString()), parseFloat(etTaxRate.getText().toString()), parseFloat(etCessRate.getText().toString()));
                        break;
                }
            }
        });
    }

    private boolean validateMRP(float netAmt, float prodMRP) {
        if(netAmt<=prodMRP){
            return true;
        }else{
            showSnakBar(coordinatorLayout,getString(R.string.mrp_error_msg));
            return true;
        }
    }



    private void executeTask(String productDescription,String quentity, String codeHSN, String barcode
            , String costPrice, String salePrice, String taxRate, String batch , String expiry
            , String unitCode, String unitName, String cessRate,String mrp,String taxCategory,String taxCategoryId,String discountValue) {
        itemProduct.setProductName(productDescription);
        itemProduct.setCodeHSN(codeHSN);
        itemProduct.setBarcode(barcode);
        itemProduct.setUnitCode(unitCode);
        itemProduct.setUnitName(unitName);
        itemProduct.setCostPrice(salePrice);
        itemProduct.setSalePrice(salePrice);
        itemProduct.setMrp(mrp);
        itemProduct.setTaxRate(taxRate);
        itemProduct.setBatch(batch);
        itemProduct.setExp(expiry);
        itemProduct.setTaxCategoryId(taxCategoryId);
        itemProduct.setTaxCategory(taxCategory);
        itemProduct.setCessRate(cessRate);
        itemProduct.setQuantity(quentity);
        itemProduct.setNetAmount(mNetAmtForSingleProd);
        itemProduct.setDis(discountValue);
        if(invoiceManager!=null){
//            ((InvoiceManager)getParentFragment()).onProductEdited(itemProduct);
            invoiceManager.onProductEdited(itemProduct);
            getFragmentManager().popBackStack();
        }
    }

    @Override
    public void onStartDateSelected(DialogFragment dialog, int day, int month, int year, String ddMMMyy) {
        sDay = day;
        sMonth = month;
        sYear = year;
        etBatch.setText(ddMMMyy);
    }

    @Override
    public void onEndDateSelected(DialogFragment dialog, int day, int month, int year, String ddMMMyy) {
        eDay = day;
        eMonth = month;
        eYear = year;
        etExpiry.setText(ddMMMyy);
    }

    @Override
    public void onTaxSelect(DialogFragment dialog, CommonModel selectedItem) {
        mTaxRate=selectedItem.getId();
        etTaxRate.setText(selectedItem.getName());
        FieldValidation.isEmpty(getContext(),etTaxRate,getString(R.string.invalid_tax_rate));
        setProductNetAmount(mDiscountType,parseFloat(etDiscount.getText().toString()), parseFloat(etQuantity.getText().toString()), parseFloat(etSalePrice.getText().toString()), parseFloat(etTaxRate.getText().toString()), parseFloat(etCessRate.getText().toString()));
        if(mTaxRate.equals("0")||mTaxRate.equals("00")||mTaxRate.equals("0.0")){
            etTaxCategory.setVisibility(View.VISIBLE);
        }else{
            etTaxCategory.setVisibility(View.GONE);
        }
    }

    @Override
    public void onUnitSelect(DialogFragment dialog, ProductModel selectUnit) {
        mSelectedUnitCode=selectUnit.getUnitCode();
        mSelectedUnitName=selectUnit.getUnitName();
        etUnit.setText(selectUnit.getUnitName());
    }

    @Override
    public void onTaxCategorySelect(DialogFragment dialog, CommonModel selectedTerm) {
        mTaxCategoryId=selectedTerm.getId();
        mTaxCategoryName=selectedTerm.getName();
        etTaxCategory.setText(mTaxCategoryName);
        FieldValidation.isEmpty(getContext(), etTaxCategory,getString(R.string.invalid_tax_category));
    }

}
