package com.gennext.offlinegst.search;

/**
 * Created by Admin on 3/15/2018.
 */

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.model.Sort;
import com.gennext.offlinegst.util.AppAnimation;


public class SortBarVIew extends CompactFragment implements View.OnClickListener {


    private EditText etSearch;
    private SearchBar.Search mListener;
    private SearchBar.SearchOrder mListener2;
    private String[] sortName;


    public static SortBarVIew newInstance(SearchBar.Search listener, SearchBar.SearchOrder listener2, String[] sortName) {
        SortBarVIew fragment = new SortBarVIew();
        AppAnimation.setSlideAnimation(fragment, Gravity.TOP);
        fragment.mListener = listener;
        fragment.mListener2 = listener2;
        fragment.sortName = sortName;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = inflater.inflate(R.layout.search_bar_short, container, false);
        initUi(v);

        return v;
    }


    private void initUi(View v) {
        Button btnAction = (Button) v.findViewById(R.id.btn_apply);
        Button btnDate = (Button) v.findViewById(R.id.btn_short_date);
        Button btnName = (Button) v.findViewById(R.id.btn_short_name);
        Button btnInvoice = (Button) v.findViewById(R.id.btn_short_invoice);
        btnAction.setOnClickListener(this);
        btnDate.setOnClickListener(this);
        btnName.setOnClickListener(this);
        btnInvoice.setOnClickListener(this);

        if(sortName!=null){
            btnDate.setText(sortName[0]);
            btnName.setText(sortName[1]);
            btnInvoice.setText(sortName[2]);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_apply:
                hideKeybord(getActivity());
                getFragmentManager().popBackStack();
                break;
            case R.id.btn_short_date:
                if(mListener!=null) {
                    mListener.onShortButtonPressed(Sort.Type1);
                }
                if(mListener2!=null) {
                    mListener2.onShortButtonPressed(Sort.Type1);
                }
                getFragmentManager().popBackStack();
                break;
            case R.id.btn_short_name:
                if(mListener!=null) {
                    mListener.onShortButtonPressed(Sort.Type2);
                }else if(mListener2!=null) {
                    mListener2.onShortButtonPressed(Sort.Type2);
                }
                getFragmentManager().popBackStack();
                break;
            case R.id.btn_short_invoice:
                if(mListener!=null) {
                    mListener.onShortButtonPressed(Sort.Type3);
                }else if(mListener2!=null) {
                    mListener2.onShortButtonPressed(Sort.Type3);
                }
                getFragmentManager().popBackStack();
                break;
        }
    }

    public void onDismiss() {
        getFragmentManager().popBackStack();
    }
}
