package com.gennext.offlinegst.user.reports;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.model.user.DashboardReportModel;
import com.gennext.offlinegst.model.user.PaymentModel;
import com.gennext.offlinegst.user.invoice.DebtorsTransaction;
import com.gennext.offlinegst.user.invoice.SellOverPaidReceipt;
import com.gennext.offlinegst.user.invoice.SellPaidReceipt;
import com.gennext.offlinegst.user.invoice.SellUnPaidReceipt;
import com.gennext.offlinegst.user.purchas.BuyerOverPaidReceipt;
import com.gennext.offlinegst.user.purchas.BuyerPaidReceipt;
import com.gennext.offlinegst.user.purchas.BuyerUnpaidReceipt;
import com.gennext.offlinegst.user.purchas.CreditorsTransaction;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.RequestBuilder;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.DefaultValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;

import java.util.ArrayList;

/**
 * Created by Admin on 7/4/2017.
 */

public class PaymentReport extends CompactFragment implements View.OnClickListener {

    protected Typeface mTfRegular;
    protected Typeface mTfLight;

    public static final int[] PIE_CHART_PAYMENT = {
            Color.rgb(160, 212, 104), Color.rgb(255, 206, 85), Color.rgb(172, 146, 235),
            Color.rgb(251, 110, 82), Color.rgb(143, 206, 242)
    };

    protected String[] mParties = new String[]{"Paid", "Unpaid", "OverPaid"};


    private AssignTask assignTask;
    private DBManager dbManager;
    private PieChart mChartSeller;
    private PieChart mChartBuyer;
    private TextView tvTopCustomers, tvTopVendors;
    private Button btnBuyerPaid, btnBuyerUnpaid, btnBuyerOverPaid;
    private Button btnSellerPaid, btnSellerUnpaid, btnSellerOverPaid;
    private LinearLayout llSellerReport,llBuyerReport;
    private Button btnBuyerTrans,btnSellerTrans;
    private OnChartValueSelectedListener buyerListener,sellerListener;
    private int mBuyerPaid,mBuyerUnpaid,mBuyerOverPaid,mSellerPaid,mSellerUnpaid,mSellerOverPaid;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static Fragment newInstance() {
        PaymentReport fragment = new PaymentReport();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_reports_payment, container, false);
        dbManager = DBManager.newIsntance(getActivity());
        screenAnalytics(getContext(), AppUser.getUserId(getContext()), "PaymentReport");
        initUi(v);
        initPiChartSeller(v);
        initPiChartBuyer(v);
        executeTask();
        return v;
    }


    private void initUi(View v) {
        llSellerReport = (LinearLayout) v.findViewById(R.id.ll_seller_report);
        llBuyerReport = (LinearLayout) v.findViewById(R.id.ll_buyer_report);

        tvTopCustomers = (TextView) v.findViewById(R.id.tv_top_customers);
        tvTopVendors = (TextView) v.findViewById(R.id.tv_top_vendors);

        btnBuyerPaid = (Button) v.findViewById(R.id.btn_buyer_paid);
        btnBuyerUnpaid = (Button) v.findViewById(R.id.btn_buyer_unpaid);
        btnBuyerOverPaid = (Button) v.findViewById(R.id.btn_buyer_overpaid);
        btnBuyerTrans = (Button) v.findViewById(R.id.btn_buyer_trans);

        btnSellerPaid = (Button) v.findViewById(R.id.btn_seller_paid);
        btnSellerUnpaid = (Button) v.findViewById(R.id.btn_seller_unpaid);
        btnSellerOverPaid = (Button) v.findViewById(R.id.btn_seller_overpaid);
        btnSellerTrans = (Button) v.findViewById(R.id.btn_seller_trans);

        btnBuyerPaid.setOnClickListener(this);
        btnBuyerUnpaid.setOnClickListener(this);
        btnBuyerOverPaid.setOnClickListener(this);
        btnBuyerTrans.setOnClickListener(this);

        btnSellerPaid.setOnClickListener(this);
        btnSellerUnpaid.setOnClickListener(this);
        btnSellerOverPaid.setOnClickListener(this);
        btnSellerTrans.setOnClickListener(this);

        buyerListener=new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                if (e == null)
                    return;
                if(h.getX()==0.0){
                    addFragment(BuyerPaidReceipt.newInstance(), "buyerPaidReceipt");
                }else if(h.getX()==1.0){
                    addFragment(BuyerUnpaidReceipt.newInstance(),"buyerUnpaidReceipt");
                }else if(h.getX()==2.0){
                    addFragment(BuyerOverPaidReceipt.newInstance(),"buyerOverPaidReceipt");
                }
//                Log.i("VAL SELECTED",
//                        "ValueY: " + e.getY() + ", index: " + h.getX()
//                                + ", DataSet index: " + h.getDataSetIndex());
            }

            @Override
            public void onNothingSelected() {

            }
        };
        sellerListener=new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                if (e == null)
                    return;
                if(h.getX()==0.0){
                    addFragment(SellPaidReceipt.newInstance(), "sellPaidReceipt");

                }else if(h.getX()==1.0){
                    addFragment(SellUnPaidReceipt.newInstance(), "sellUnPaidReceipt");

                }else if(h.getX()==2.0){
                    addFragment(SellOverPaidReceipt.newInstance(), "sellOverPaidReceipt");

                }
//                Log.i("VAL SELECTED",
//                        "ValueY: " + e.getY() + ", index: " + h.getX()
//                                + ", DataSet index: " + h.getDataSetIndex());
            }

            @Override
            public void onNothingSelected() {

            }
        };
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_buyer_paid:
                addFragment(BuyerPaidReceipt.newInstance(), "buyerPaidReceipt");

                break;
            case R.id.btn_buyer_unpaid:
                addFragment(BuyerUnpaidReceipt.newInstance(),"buyerUnpaidReceipt");

                break;
            case R.id.btn_buyer_overpaid:
                addFragment(BuyerOverPaidReceipt.newInstance(),"buyerOverPaidReceipt");

                break;
            case R.id.btn_buyer_trans:
                addFragment(CreditorsTransaction.newInstance(PaymentReport.this),"creditorsTransaction");
                break;
            case R.id.btn_seller_paid:
                addFragment(SellPaidReceipt.newInstance(), "sellPaidReceipt");

                break;
            case R.id.btn_seller_unpaid:
                addFragment(SellUnPaidReceipt.newInstance(), "sellUnPaidReceipt");

                break;
            case R.id.btn_seller_overpaid:
                addFragment(SellOverPaidReceipt.newInstance(), "sellOverPaidReceipt");

                break;
            case R.id.btn_seller_trans:
                addFragment(DebtorsTransaction.newInstance(PaymentReport.this),"debtorsTransaction");
                break;

        }
    }

    private void initPiChartBuyer(View v) {
        mChartBuyer = (PieChart) v.findViewById(R.id.piechart_buyer);
        mChartBuyer.setUsePercentValues(false);
        mChartBuyer.getDescription().setEnabled(false);
        mChartBuyer.setExtraOffsets(5, 10, 5, 5);

        mChartBuyer.setDragDecelerationFrictionCoef(0.95f);

//        mChartBuyer.setCenterTextTypeface(mTfLight);
//        mChartBuyer.setCenterText(generateCenterSpannableText("Purchase"));

        mChartBuyer.setDrawHoleEnabled(true);
        mChartBuyer.setHoleColor(Color.WHITE);

        mChartBuyer.setTransparentCircleColor(Color.WHITE);
        mChartBuyer.setTransparentCircleAlpha(110);

        mChartBuyer.setHoleRadius(10f);
        mChartBuyer.setTransparentCircleRadius(15f);

        mChartBuyer.setDrawCenterText(false);

        mChartBuyer.setRotationAngle(0);
        // enable rotation of the chart by touch
        mChartBuyer.setRotationEnabled(true);
        mChartBuyer.setHighlightPerTapEnabled(true);

        // mChartBuyer.setUnit(" €");
        // mChartBuyer.setDrawUnitsInChart(true);

        // add a selection listener

        mChartBuyer.setOnChartValueSelectedListener(buyerListener);


        mChartBuyer.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // mChartBuyer.spin(2000, 0, 360);


        Legend l = mChartBuyer.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);
        l.setEnabled(false); //show ot hide legend from right side
        // entry label styling
        mChartBuyer.setEntryLabelColor(Color.BLACK);
        mChartBuyer.setEntryLabelTypeface(mTfRegular);
        mChartBuyer.setEntryLabelTextSize(12f);
    }

    private void initPiChartSeller(View v) {
        mChartSeller = (PieChart) v.findViewById(R.id.piechart_seller);
        mChartSeller.setUsePercentValues(false);
        mChartSeller.getDescription().setEnabled(false);
        mChartSeller.setExtraOffsets(5, 10, 5, 5);

        mChartSeller.setDragDecelerationFrictionCoef(0.95f);

        mChartSeller.setCenterTextTypeface(mTfLight);
        mChartSeller.setCenterText(generateCenterSpannableText(""));

        mChartSeller.setDrawHoleEnabled(true);
        mChartSeller.setHoleColor(Color.WHITE);

        mChartSeller.setTransparentCircleColor(Color.WHITE);
        mChartSeller.setTransparentCircleAlpha(110);

        mChartSeller.setHoleRadius(10f);
        mChartSeller.setTransparentCircleRadius(15f);

        mChartSeller.setDrawCenterText(false);

        mChartSeller.setRotationAngle(0);
        // enable rotation of the chart by touch
        mChartSeller.setRotationEnabled(true);
        mChartSeller.setHighlightPerTapEnabled(true);

        // mChartSeller.setUnit(" €");
        // mChartSeller.setDrawUnitsInChart(true);

        // add a selection listener
        mChartSeller.setOnChartValueSelectedListener(sellerListener);


        mChartSeller.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // mChartSeller.spin(2000, 0, 360);


        Legend l = mChartSeller.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);
        l.setEnabled(false); //show ot hide legend from right side
        // entry label styling
        mChartSeller.setEntryLabelColor(Color.BLACK);
        mChartSeller.setEntryLabelTypeface(mTfRegular);
        mChartSeller.setEntryLabelTextSize(12f);
    }


    public void executeTask() {
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.GET_PRODUCTS);
    }

     public void refreshTask() {
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.GET_PRODUCTS);

    }

    private SpannableString generateCenterSpannableText(String title) {

        SpannableString s = new SpannableString(title);
//        SpannableString s = new SpannableString("Paid Amt.\nRs 5000");
//        s.setSpan(new RelativeSizeSpan(1.2f), 0, 9, 0);
//        s.setSpan(new StyleSpan(Typeface.NORMAL), 9, s.length(), 0);
        s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), 0, s.length(), 0);
        return s;
    }





    private class AssignTask extends AsyncTask<String, Void, DashboardReportModel> {
        private Context context;

        private void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        private void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        private AssignTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected DashboardReportModel doInBackground(String... urls) {
            String userId = AppUser.getUserId(context);
            String profileId = AppUser.getProfileId(context);
            if (AppConfig.isUpdateToServer(context)) {
                String response = ApiCall.POST(urls[0], RequestBuilder.Default(userId));
                PaymentModel result = JsonParser.getDebtorsDetail(response);
            }
            return dbManager.getPaymentReport(new DashboardReportModel(), userId, profileId);
        }


        @Override
        protected void onPostExecute(DashboardReportModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                if (result != null) {
                    if (result.getDebitorsAvailable()) {
                        llSellerReport.setVisibility(View.VISIBLE);
                        String overPaidByCustomer = result.getOverPaidByCust();
                        if (overPaidByCustomer.contains("-")) {
                            overPaidByCustomer = overPaidByCustomer.replace("-", "");
                        }
                        setDataSeller(true,parseInt(result.getPaidByCust()), parseInt(result.getUnpaidByCust()), parseInt(overPaidByCustomer));
                    } else {
                        setDataSeller(false,1, 1, 1);
                    }
                    if (result.getCreditorsAvailable()) {
                        String overPaidByVendor = result.getOverPaidByVendor();
                        if (overPaidByVendor.contains("-")) {
                            overPaidByVendor = overPaidByVendor.replace("-", "");
                        }
                        setDataBuyer(true,parseInt(result.getPaidByVendor()), parseInt(result.getUnpaidByVendor()), parseInt(overPaidByVendor));
                    } else {
                        setDataBuyer(false,1, 1, 1);
                    }

                }
            }
        }
    }

    private void setTopCustomer(ArrayList<String> topThreeCustomer) {
        String topCustomers = "";
        if (topThreeCustomer != null) {
            if (topThreeCustomer.size() <= 3) {
                for (int i = 0; i < topThreeCustomer.size(); i++) {
                    topCustomers += (i + 1) + topThreeCustomer.get(i) + "\n";
                }
            } else {
                for (int i = 0; i <= 3; i++) {
                    topCustomers += (i + 1) + topThreeCustomer.get(i) + "\n";
                }
            }
            tvTopCustomers.setText(topCustomers);
        } else {
            tvTopCustomers.setText("NA");
        }
    }

    private void setTopVendors(ArrayList<String> topThreeVendor) {
        String topVendor = "";
        if (topThreeVendor != null) {
            if (topThreeVendor.size() <= 3) {
                for (int i = 0; i < topThreeVendor.size(); i++) {
                    topVendor += (i + 1) + topThreeVendor.get(i) + "\n";
                }
            } else {
                for (int i = 0; i <= 3; i++) {
                    topVendor += (i + 1) + topThreeVendor.get(i) + "\n";
                }
            }
            tvTopVendors.setText(topVendor);
        } else {
            tvTopVendors.setText("NA");
        }
    }

    private void setDataBuyer(boolean isDataAvail,int paid, int unpaid, int overDue) {

        mBuyerPaid=paid;
        mBuyerUnpaid=unpaid;
        mBuyerOverPaid=overDue;

//        if(isDataAvail) {
//            btnBuyerPaid.setText("Paid\nRs." + String.valueOf(paid));
//            btnBuyerUnpaid.setText("Unpaid\nRs." + String.valueOf(unpaid));
//            btnBuyerOverPaid.setText("OverPaid\nRs." + String.valueOf(overDue));
//        }else{
//            btnBuyerPaid.setText("Paid\nRs." + String.valueOf(0));
//            btnBuyerUnpaid.setText("Unpaid\nRs." + String.valueOf(0));
//            btnBuyerOverPaid.setText("OverPaid\nRs." + String.valueOf(0));
//        }
        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();

        entries.add(new PieEntry(paid,
                mParties[0],
                ContextCompat.getDrawable(getActivity(), R.drawable.ic_about)));

        entries.add(new PieEntry(unpaid,
                mParties[1],
                ContextCompat.getDrawable(getActivity(), R.drawable.ic_about)));

        entries.add(new PieEntry(overDue,
                mParties[2],
                ContextCompat.getDrawable(getActivity(), R.drawable.ic_about)));


        PieDataSet dataSet = new PieDataSet(entries, "");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : PIE_CHART_PAYMENT)
            colors.add(c);

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new DefaultValueFormatter(0));
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.BLACK);
        data.setValueTypeface(mTfLight);
//        data.setDrawValues(false); // hide entry values from pichart
        mChartBuyer.setData(data);
        mChartBuyer.setDrawEntryLabels(false);

        // undo all highlights
        mChartBuyer.highlightValues(null);

        mChartBuyer.invalidate();
    }

    private void setDataSeller(boolean isDataAvail,int paid, int unpaid, int overDue) {

        mSellerPaid=paid;
        mSellerUnpaid=unpaid;
        mSellerOverPaid=overDue;
//        if(isDataAvail) {
//            btnSellerPaid.setText("Paid\nRs." + String.valueOf(paid));
//            btnSellerUnpaid.setText("Unpaid\nRs." + String.valueOf(unpaid));
//            btnSellerOverPaid.setText("OverPaid\nRs." + String.valueOf(overDue));
//        }else{
//            btnSellerPaid.setText("Paid\nRs." + String.valueOf(0));
//            btnSellerUnpaid.setText("Unpaid\nRs." + String.valueOf(0));
//            btnSellerOverPaid.setText("OverPaid\nRs." + String.valueOf(0));
//        }
        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();

        entries.add(new PieEntry(paid,
                mParties[0],
                ContextCompat.getDrawable(getActivity(), R.drawable.ic_about)));

        entries.add(new PieEntry(unpaid,
                mParties[1],
                ContextCompat.getDrawable(getActivity(), R.drawable.ic_about)));

        entries.add(new PieEntry(overDue,
                mParties[2],
                ContextCompat.getDrawable(getActivity(), R.drawable.ic_about)));


        PieDataSet dataSet = new PieDataSet(entries, "");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : PIE_CHART_PAYMENT)
            colors.add(c);

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new DefaultValueFormatter(0));
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.BLACK);
        data.setValueTypeface(mTfLight);
//        data.setDrawValues(false); // hide entry values from pichart
        mChartSeller.setData(data);
        mChartSeller.setDrawEntryLabels(false);

        // undo all highlights
        mChartSeller.highlightValues(null);

        mChartSeller.invalidate();
    }
}
