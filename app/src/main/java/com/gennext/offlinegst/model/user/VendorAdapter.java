package com.gennext.offlinegst.model.user;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.panel.ActionMenu;
import com.gennext.offlinegst.user.vendor.Vendor;
import com.gennext.offlinegst.util.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class VendorAdapter extends RecyclerView.Adapter<VendorAdapter.ReyclerViewHolder>
        implements ActionMenu.MenuVendor {

    private final Vendor parentRef;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<VendorModel> items;
    private List<VendorModel> originalData;
    private ItemFilter mFilter = new ItemFilter();

    public VendorAdapter(Activity context, ArrayList<VendorModel> items,Vendor parentRef) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef = parentRef;
        this.items = items;
        this.originalData = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_user_customer_format1, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final VendorModel item = items.get(position);

        holder.tvName.setText(item.getPersonName());
        holder.tvCompanyName.setText(Utility.generateBoldTitle("Company:",item.getCompanyName()));
        holder.tvState.setText(item.getState());
        holder.tvGSTIN.setText(item.getGSTIN());
        setColor(holder.llSideLine);


        holder.llSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActionMenu.newInstance(VendorAdapter.this,item,position)
                        .show(parentRef.getFragmentManager(),"actionMenu");
            }
        });
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onViewClick(DialogFragment dialog, VendorModel itemVendor, int position) {
        parentRef.viewVendor(itemVendor);
    }

    @Override
    public void onEditClick(DialogFragment dialog, VendorModel itemVendor, int position) {
        parentRef.editVendor(itemVendor);
    }

    @Override
    public void onDeleteClick(DialogFragment dialog, VendorModel itemVendor, int position) {
        parentRef.deleteVendor(itemVendor,position);

    }

    @Override
    public void onCopyClick(DialogFragment dialog, VendorModel itemVendor, int position) {
        parentRef.copyVendor(itemVendor);
    }

    public void itemDeletedSuccessful(int position) {
        items.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position,getItemCount());
    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout llSlot,llSideLine;
        private TextView tvName,tvCompanyName,tvState,tvGSTIN;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSideLine = (LinearLayout) v.findViewById(R.id.ll_slot);
            llSlot = (LinearLayout) v.findViewById(R.id.layoutSlot);
            tvName = (TextView) v.findViewById(R.id.tv_slot_0);
            tvCompanyName = (TextView) v.findViewById(R.id.tv_slot_1);
            tvState = (TextView) v.findViewById(R.id.tv_slot_2);
            tvGSTIN = (TextView) v.findViewById(R.id.tv_slot_3);
        }
    }



    public void setColor(LinearLayout tv) {
        Random rnd = new Random(); 
        int color= Utility.SIDE_LINE_COLORS[rnd.nextInt(21)];
        tv.setBackgroundColor(color);
    }

    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<VendorModel> list = originalData;

            int count = list.size();
            final ArrayList<VendorModel> nlist = new ArrayList<>(count);
            String filterableText;

            if (!filterString.equals("")) {
                for (VendorModel model : list) {
                    if (!filterString.equals("")) {
                        filterableText = model.getPersonName();
                        filterableText+= model.getState();
                        filterableText+= model.getCompanyName();
                        filterableText+= model.getGSTIN();
                        if (filterableText.toLowerCase().contains(filterString)) {
                            nlist.add(model);
                        }
                    } else {
                        nlist.add(model);
                    }
                }
                results.values = nlist;
                results.count = nlist.size();
            } else {
                results.values = originalData;
                results.count = originalData.size();
            }

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            items = (ArrayList<VendorModel>) results.values;
            notifyDataSetChanged();
        }

    }
}

