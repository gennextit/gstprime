package com.gennext.offlinegst.model.user;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.user.services.sale.ServiceManager;
import com.gennext.offlinegst.util.Utility;

import java.util.ArrayList;

public class InvoiceServiceAdapter extends RecyclerView.Adapter<InvoiceServiceAdapter.ReyclerViewHolder>{

    private final ServiceManager parentRef;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<ServiceModel> items;

    public InvoiceServiceAdapter(Activity context, ArrayList<ServiceModel> items, ServiceManager parentRef) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef = parentRef;
        this.items = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_selected_service, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final ServiceModel item = items.get(position);
        float sale = Utility.parseFloat(item.getSalePrice());
        float dis = Utility.parseFloat(item.getDis());
        float taxRate = Utility.parseFloat(item.getTaxRate());

        float taxable = sale-dis;
        float totalTax =taxable + ((taxable * taxRate) / 100);
        String mTaxable = Utility.decimalFormat(taxable,"#.##");
        String mNetAmt = Utility.decimalFormat(totalTax,"#.##");

        holder.tvName.setText(item.getServiceName());
        holder.tvTaxable.setText(context.getString(R.string.rs)+mTaxable);
        holder.tvNet.setText(context.getString(R.string.rs)+mNetAmt);


        holder.llSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parentRef.editProduct(item,position);
            }
        });
        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                items.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position,getItemCount());
                parentRef.deleteProduct(item,position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private final ImageButton btnDelete;
        private LinearLayout llSlot;
        private TextView tvName,tvTaxable,tvNet;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSlot = (LinearLayout) v.findViewById(R.id.layoutSlot);
            btnDelete = (ImageButton) v.findViewById(R.id.btn_delete);
            tvName = (TextView) v.findViewById(R.id.tv_slot_1);
            tvTaxable = (TextView) v.findViewById(R.id.tv_slot_2);
            tvNet = (TextView) v.findViewById(R.id.tv_slot_3);
        }
    }
}

