package com.gennext.offlinegst.model.user;

import java.util.ArrayList;

/**
 * Created by Admin on 7/27/2017.
 */

public class PurchasesModel {

    private String invoiceId;
    private String invoiceUserId;
    private String invoiceProfileId;
    private String invoiceNumber;
    private String invoiceDate;
    private String vendorId;
    private String vendorName;
    private String freightAmount;
    private String totalAmount;
    private String placeOfPurchase;
    private String placeOfPurchaseId;
    private String discount;
    private String purchaseCategory;
    private String purchaseCategoryId;
    private String mentionedGST;
    private String receiveDate;
    private String reverseCharge;
    private String purchaseType;
    private String purchaseTypeId;
    private String dateOfPurchase;

    private String freightTaxRate;
    private String freightTotalAmount;
    private String cgstTax;
    private String cgstVal;
    private String sgstTax;
    private String sgstVal;
    private String igstTax;
    private String igstVal;
    private String invoiceTotal;
    private String lastDate;
    private Boolean cancellation;

    private String output;
    private String outputMsg;
    private String outputDB;
    private String outputDBMsg;
    private ArrayList<PurchasesModel> list;
    private ArrayList<ProductModel> productList;
    private ArrayList<ServiceModel> servicesList;

    public String getLastDate() {
        return lastDate;
    }

    public void setLastDate(String lastDate) {
        this.lastDate = lastDate;
    }

    public Boolean getCancellation() {
        return cancellation;
    }

    public void setCancellation(Boolean cancellation) {
        this.cancellation = cancellation;
    }

    public ArrayList<ServiceModel> getServicesList() {
        return servicesList;
    }

    public void setServicesList(ArrayList<ServiceModel> servicesList) {
        this.servicesList = servicesList;
    }

    public String getFreightTaxRate() {
        return freightTaxRate;
    }

    public void setFreightTaxRate(String freightTaxRate) {
        this.freightTaxRate = freightTaxRate;
    }

    public String getFreightTotalAmount() {
        return freightTotalAmount;
    }

    public void setFreightTotalAmount(String freightTotalAmount) {
        this.freightTotalAmount = freightTotalAmount;
    }

    public String getCgstTax() {
        return cgstTax;
    }

    public void setCgstTax(String cgstTax) {
        this.cgstTax = cgstTax;
    }

    public String getCgstVal() {
        return cgstVal;
    }

    public void setCgstVal(String cgstVal) {
        this.cgstVal = cgstVal;
    }

    public String getSgstTax() {
        return sgstTax;
    }

    public void setSgstTax(String sgstTax) {
        this.sgstTax = sgstTax;
    }

    public String getSgstVal() {
        return sgstVal;
    }

    public void setSgstVal(String sgstVal) {
        this.sgstVal = sgstVal;
    }

    public String getIgstTax() {
        return igstTax;
    }

    public void setIgstTax(String igstTax) {
        this.igstTax = igstTax;
    }

    public String getIgstVal() {
        return igstVal;
    }

    public void setIgstVal(String igstVal) {
        this.igstVal = igstVal;
    }

    public String getInvoiceTotal() {
        return invoiceTotal;
    }

    public void setInvoiceTotal(String invoiceTotal) {
        this.invoiceTotal = invoiceTotal;
    }

    public String getDateOfPurchase() {
        return dateOfPurchase;
    }

    public void setDateOfPurchase(String dateOfPurchase) {
        this.dateOfPurchase = dateOfPurchase;
    }

    public String getReverseCharge() {
        return reverseCharge;
    }

    public void setReverseCharge(String reverseCharge) {
        this.reverseCharge = reverseCharge;
    }

    public String getPurchaseType() {
        return purchaseType;
    }

    public void setPurchaseType(String purchaseType) {
        this.purchaseType = purchaseType;
    }

    public String getPurchaseTypeId() {
        return purchaseTypeId;
    }

    public void setPurchaseTypeId(String purchaseTypeId) {
        this.purchaseTypeId = purchaseTypeId;
    }

    public String getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(String receiveDate) {
        this.receiveDate = receiveDate;
    }

    public String getPurchaseCategory() {
        return purchaseCategory;
    }

    public void setPurchaseCategory(String purchaseCategory) {
        this.purchaseCategory = purchaseCategory;
    }

    public String getPurchaseCategoryId() {
        return purchaseCategoryId;
    }

    public void setPurchaseCategoryId(String purchaseCategoryId) {
        this.purchaseCategoryId = purchaseCategoryId;
    }

    public String getMentionedGST() {
        return mentionedGST;
    }

    public void setMentionedGST(String mentionedGST) {
        this.mentionedGST = mentionedGST;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getInvoiceProfileId() {
        return invoiceProfileId;
    }

    public void setInvoiceProfileId(String invoiceProfileId) {
        this.invoiceProfileId = invoiceProfileId;
    }

    public String getPlaceOfPurchaseId() {
        return placeOfPurchaseId;
    }

    public void setPlaceOfPurchaseId(String placeOfPurchaseId) {
        this.placeOfPurchaseId = placeOfPurchaseId;
    }

    public String getFreightAmount() {
        return freightAmount;
    }

    public void setFreightAmount(String freightAmount) {
        this.freightAmount = freightAmount;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getInvoiceUserId() {
        return invoiceUserId;
    }

    public void setInvoiceUserId(String invoiceUserId) {
        this.invoiceUserId = invoiceUserId;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getPlaceOfPurchase() {
        return placeOfPurchase;
    }

    public void setPlaceOfPurchase(String placeOfPurchase) {
        this.placeOfPurchase = placeOfPurchase;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public String getOutputDB() {
        return outputDB;
    }

    public void setOutputDB(String outputDB) {
        this.outputDB = outputDB;
    }

    public String getOutputDBMsg() {
        return outputDBMsg;
    }

    public void setOutputDBMsg(String outputDBMsg) {
        this.outputDBMsg = outputDBMsg;
    }

    public ArrayList<PurchasesModel> getList() {
        return list;
    }

    public void setList(ArrayList<PurchasesModel> list) {
        this.list = list;
    }

    public ArrayList<ProductModel> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<ProductModel> productList) {
        this.productList = productList;
    }

}
