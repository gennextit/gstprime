package com.gennext.offlinegst.search;

/**
 * Created by Admin on 3/15/2018.
 */

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.DatePickerDialog;
import com.gennext.offlinegst.util.AppAnimation;

import java.util.Calendar;


public class FilterBarView extends CompactFragment implements DatePickerDialog.DateSelectFlagListener {


    public static final int MONTH_GAP = 31;
    private EditText etSearch;
    private SearchBar.Search mListener;
    private String mStartDate, mEndDate;
    private FragmentManager manager;
    private int sDay, sMonth, sYear;
    private int eDay, eMonth, eYear;
    private Button btnStartDate, btnEndDate;
    private SearchBar parentRef;


    public static FilterBarView newInstance(SearchBar.Search listener) {
        FilterBarView fragment = new FilterBarView();
        AppAnimation.setSlideAnimation(fragment, Gravity.TOP);
        fragment.mListener = listener;
        return fragment;
    }

    public static FilterBarView newInstance(SearchBar.Search listener, SearchBar parentRef, String startDate, String endDate) {
        FilterBarView fragment = new FilterBarView();
        AppAnimation.setSlideAnimation(fragment, Gravity.TOP);
        fragment.mStartDate = startDate;
        fragment.mEndDate = endDate;
        fragment.parentRef = parentRef;
        fragment.mListener = listener;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = inflater.inflate(R.layout.search_bar_filter, container, false);
        initUi(v);
        if (mStartDate == null && mEndDate == null) {
            initDate();
        } else {
            updateDate();
        }
        return v;
    }

    private void updateDate() {
        int[] date;
        date = DatePickerDialog.initDate(mStartDate);
        sDay = date[0];
        sMonth = date[1];
        sYear = date[2];

        date = DatePickerDialog.initDate(mEndDate);
        eDay = date[0];
        eMonth = date[1];
        eYear = date[2];

        btnStartDate.setText("Start Date\n" + mStartDate);
        btnEndDate.setText("End Date\n" + mEndDate);
    }

    private void initDate() {
        //Calendar set to the current date
        Calendar calendar = Calendar.getInstance();
        eYear = calendar.get(Calendar.YEAR);
        eMonth = calendar.get(Calendar.MONTH);
        eDay = calendar.get(Calendar.DAY_OF_MONTH);

        //rollback 90 days
        calendar.add(Calendar.DAY_OF_YEAR, -MONTH_GAP);
        //now the date is 90 days back
        sYear = calendar.get(Calendar.YEAR);
        sMonth = calendar.get(Calendar.MONTH);
        sDay = calendar.get(Calendar.DAY_OF_MONTH);

        mStartDate = DatePickerDialog.getFormattedDate(sDay, sMonth, sYear);
        mEndDate = DatePickerDialog.getFormattedDate(eDay, eMonth, eYear);
        btnStartDate.setText("Start Date\n" + mStartDate);
        btnEndDate.setText("End Date\n" + mEndDate);
    }

    private void initUi(View v) {
        Button btnAction = (Button) v.findViewById(R.id.btn_apply);
        btnStartDate = (Button) v.findViewById(R.id.btn_filter_start_date);
        btnEndDate = (Button) v.findViewById(R.id.btn_filter_end_date);
        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeybord(getActivity());
                parentRef.updateDate(mStartDate,mEndDate);
                mListener.onFilterButtonPressed(mStartDate, mEndDate);
                getFragmentManager().popBackStack();
            }
        });

        btnStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog.newInstance(FilterBarView.this, false, DatePickerDialog.START_DATE, sDay, sMonth, sYear)
                        .show(getFragmentManager(), "datePickerDialog");
            }
        });
        btnEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog.newInstance(FilterBarView.this, false, DatePickerDialog.END_DATE, eDay, eMonth, eYear)
                        .show(getFragmentManager(), "datePickerDialog");
            }
        });

    }


    @Override
    public void onStartDateSelected(DialogFragment dialog, int day, int month, int year, String ddMMMyy) {
        sDay = day;
        sMonth = month;
        sYear = year;
        mStartDate = DatePickerDialog.getFormattedDate(sDay, sMonth, sYear);
        btnStartDate.setText("Start Date\n" + mStartDate);
    }

    @Override
    public void onEndDateSelected(DialogFragment dialog, int day, int month, int year, String ddMMMyy) {
        eDay = day;
        eMonth = month;
        eYear = year;
        mEndDate = DatePickerDialog.getFormattedDate(eDay, eMonth, eYear);
        btnEndDate.setText("End Date\n" + mEndDate);
    }
}
