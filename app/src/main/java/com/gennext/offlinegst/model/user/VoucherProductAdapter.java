package com.gennext.offlinegst.model.user;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.panel.ProductMenu;
import com.gennext.offlinegst.user.vouchers.AddVouchers;

import java.util.ArrayList;

public class VoucherProductAdapter extends RecyclerView.Adapter<VoucherProductAdapter.ReyclerViewHolder>
        implements ProductMenu.MenuListener {

    private final AddVouchers parentRef;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<ProductModel> items;

    public VoucherProductAdapter(Activity context, ArrayList<ProductModel> items, AddVouchers parentRef) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef = parentRef;
        this.items = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_selected_product, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final ProductModel item = items.get(position);
        String quantity = item.getQuantity();
        String salePrice = item.getSalePrice();
        int qun, price;
        qun = convertToInt(quantity);
        price = convertToInt(salePrice);
        String amount = String.valueOf(qun * price);
        holder.tvName.setText(item.getProductName());
        holder.tvQuantity.setText(quantity);
        holder.tvTotal.setText(context.getString(R.string.rs)+amount);
        holder.tvPrice.setText(context.getString(R.string.rs)+salePrice);

        holder.llSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProductMenu.newInstance(VoucherProductAdapter.this,item,position)
                        .show(parentRef.getFragmentManager(),"actionMenu");
            }
        });
        holder.btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                increaseQuantity(item, position);
            }
        });
        holder.btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                decreseQuantity(item, position);
            }
        });
    }

    private void increaseQuantity(ProductModel item, int position) {
        int qty = convertToInt(item.getQuantity());
        qty++;
        item.setQuantity(String.valueOf(qty));
        items.set(position, item);
        notifyDataSetChanged();
        parentRef.refreshAdapter();
    }

    private void decreseQuantity(ProductModel item, int position) {
        int qty = convertToInt(item.getQuantity());
        qty--;
        if (qty > 0) {
            item.setQuantity(String.valueOf(qty));
            items.set(position, item);
            notifyDataSetChanged();
            parentRef.refreshAdapter();
        }
    }

    private int convertToInt(String intValue) {
        try {
            return Integer.parseInt(intValue);
        } catch (NumberFormatException e) {
            return 0;
        }
    }
    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onEditClick(DialogFragment dialog, ProductModel itemProduct, int position) {
        parentRef.editProduct(itemProduct,position);
    }

    @Override
    public void onDeleteClick(DialogFragment dialog, ProductModel itemProduct, int position) {
        items.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position,getItemCount());
        parentRef.deleteProduct(itemProduct,position);
    }
 

    @Override
    public void onQuantityClick(DialogFragment dialog, ProductModel itemProduct, int position) {
        parentRef.quantityProduct(itemProduct);
    }




    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private final ImageButton btnPlus, btnMinus;
        private LinearLayout llSlot;
        private TextView tvName,tvQuantity,tvPrice,tvTotal;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSlot = (LinearLayout) v.findViewById(R.id.layoutSlot);
            btnPlus = (ImageButton) v.findViewById(R.id.btn_plus);
            btnMinus = (ImageButton) v.findViewById(R.id.btn_minus);
            tvName = (TextView) v.findViewById(R.id.tv_slot_1);
            tvPrice = (TextView) v.findViewById(R.id.tv_slot_2);
            tvTotal = (TextView) v.findViewById(R.id.tv_slot_3);
            tvQuantity = (TextView) v.findViewById(R.id.tv_slot_4);
        }
    }
}

