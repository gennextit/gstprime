package com.gennext.offlinegst.util;

import android.content.Context;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gennext.offlinegst.R;

/**
 * Created by Admin on 8/28/2017.
 */

public class ProgressButton {

    private final View view;
    private final ProgressBar pBar;
    private final Button btnAction;
    private final Animation inAnim;
    private final Animation outAnim;
    private final ImageView ivStatus;

    public static ProgressButton newInstance(Context context, View v) {
        ProgressButton btn = new ProgressButton(context, v);
        return btn;
    }

    public ProgressButton(Context context, View view) {
        this.view = view;
        ivStatus = (ImageView) view.findViewById(R.id.iv_status);
        pBar = (ProgressBar) view.findViewById(R.id.progressBar);
        btnAction = (Button) view.findViewById(R.id.btn_action);
        inAnim = AnimationUtils.loadAnimation(context,
                android.R.anim.fade_in);
        outAnim = AnimationUtils.loadAnimation(context,
                android.R.anim.fade_out);
    }

    public void setOnEditorActionListener(EditText editText) {
        editText.setImeOptions(EditorInfo.IME_ACTION_DONE);
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                boolean handled = false;
                if (actionId == KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                    // Handle pressing "Enter" key here
                    executeTask();
                    handled = true;
                }
                return handled;
            }
        });
    }
    public void setOnEditorActionListener(EditText editText,String btnLabel) {
        editText.setImeActionLabel(btnLabel, EditorInfo.IME_ACTION_DONE);
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                boolean handled = false;
                if (actionId == KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                    // Handle pressing "Enter" key here
                    executeTask();
                    handled = true;
                }
                return handled;
            }
        });
    }

    public void executeTask() {
        btnAction.performClick();
    }

    public void setOnClickListener(View.OnClickListener click) {
        if (btnAction != null)
            btnAction.setOnClickListener(click);

    }

    public void startAnimation() {
        hideButtonAction();
        showProgressBar();
    }

    public void revertAnimation() {
        hideProgressBar();
        showButtonAction();
    }

    public void revertSuccessAnimation() {
        revertSuccessAnimation(false);
    }
    public void revertSuccessAnimation(Boolean btnVisibility) {
        hideProgressBar();
        if(btnVisibility) {
            showButtonAction();
        }else{
            showSuccessView();
        }
    }

    private void showSuccessView() {
        if(ivStatus!=null) {
            ivStatus.startAnimation(inAnim);
            ivStatus.setVisibility(View.VISIBLE);
        }
    }

    private void showButtonAction() {
        if (btnAction != null) {
            btnAction.startAnimation(inAnim);
            btnAction.setVisibility(View.VISIBLE);
        }
    }

    private void hideButtonAction() {
        if (btnAction != null) {
            btnAction.startAnimation(outAnim);
            btnAction.setVisibility(View.INVISIBLE);
        }
    }

    private void showProgressBar() {
        if (pBar != null) {
            pBar.startAnimation(inAnim);
            pBar.setVisibility(View.VISIBLE);
        }
    }

    private void hideProgressBar() {
        if (pBar != null) {
            pBar.startAnimation(outAnim);
            pBar.setVisibility(View.GONE);
        }
    }

    public void setText(String text) {
        if (btnAction != null) {
            btnAction.setText(text!=null?text:"");
        }
    }
    public String getText() {
        if (btnAction != null) {
            return btnAction.getText().toString();
        }else {
            return "";
        }
    }

}
