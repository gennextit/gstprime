package com.gennext.offlinegst.util;


import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.gennext.offlinegst.model.app.Model;
import com.gennext.offlinegst.model.user.CompanyModel;
import com.gennext.offlinegst.model.user.CustomerModel;
import com.gennext.offlinegst.model.user.DashboardReportModel;
import com.gennext.offlinegst.model.user.InvoiceModel;
import com.gennext.offlinegst.model.user.PaymentModel;
import com.gennext.offlinegst.model.user.ProductModel;
import com.gennext.offlinegst.model.user.ProfileModel;
import com.gennext.offlinegst.model.user.PurchasesModel;
import com.gennext.offlinegst.model.user.ServiceModel;
import com.gennext.offlinegst.model.user.VendorModel;
import com.gennext.offlinegst.model.user.VoucherModel;
import com.gennext.offlinegst.setting.Const;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Admin on 6/22/2017.
 */

public class DBManager {
    public static final int PRODUCT_TABLE = 1;
    public static String DATABASE_BACKUP_PREFIX = "OfflineGSTbackup_";
    private SQLiteDatabase db;
    private Activity act;
    private static String databaseName;

    private int databaseClass;
    private Boolean alterTable;

    public static DBManager newIsntance(Activity activity) {
        return new DBManager(activity, 0, false);
    }

    public static DBManager newIsntance(Activity activity, int databaseClass) {
        return newIsntance(activity, databaseClass, false);
    }

    public static DBManager newIsntance(Activity activity, int databaseClass, Boolean alterTable) {
        return new DBManager(activity, databaseClass, alterTable);
    }

    private DBManager(Activity activity, int databaseClass, Boolean alterTable) {
        this.databaseClass = databaseClass;
        this.alterTable = alterTable;
        act = activity;
        boolean output;
        createDataBase();
        if (alterTable) {
            try {
                switch (databaseClass) {
                    case PRODUCT_TABLE:
                        output = isModifyColumnInTable("product", "unitCode");
                        if (output) {
                            db.execSQL("ALTER TABLE product ADD COLUMN unitCode TEXT");
                        }
                        output = isModifyColumnInTable("product", "unitName");
                        if (output) {
                            db.execSQL("ALTER TABLE product ADD COLUMN unitName TEXT");
                        }
                        output = isModifyColumnInTable("product", "cessRate");
                        if (output) {
                            db.execSQL("ALTER TABLE product ADD COLUMN cessRate TEXT");
                        }
                        break;

                }
            } catch (Exception Exp) {
            }
        }
    }

    public static String getDatabaseName() {
        return "offgstDB001";
    }

    private void createDataBase() {
        db = act.openOrCreateDatabase("offgstDB001", Context.MODE_PRIVATE, null);
        //total 8 item with primaryKey in table
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS profile(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR ,name VARCHAR,email VARCHAR,mobile VARCHAR,pan VARCHAR,imagePath VARCHAR,imageUrl VARCHAR);");
        //total 8 item with primaryKey in table
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS companyDetail(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR , profileId VARCHAR ,companyName VARCHAR,telephone VARCHAR,email VARCHAR,gstin VARCHAR,state VARCHAR,stateCode VARCHAR,address VARCHAR,dlNumber VARCHAR,tAndC VARCHAR,accName VARCHAR,ifscCode VARCHAR,accNumber VARCHAR,branchAddress VARCHAR);");
        //total 9 item with primaryKey in table
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS product(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR ,productName VARCHAR,codeHSN VARCHAR,barcode VARCHAR,costPrice VARCHAR,salePrice VARCHAR,taxRate VARCHAR,fromDate VARCHAR,toDate VARCHAR);");
        //total 9 item with primaryKey in table
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS productStock(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR ,profileId VARCHAR,invoiceNumber VARCHAR,productId VARCHAR,quantity VARCHAR,stockType VARCHAR);");
        //total 8 item with primaryKey in table
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS services(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR ,serviceName VARCHAR,codeSAC VARCHAR,salePrice VARCHAR,taxRate VARCHAR,fromDate VARCHAR,toDate VARCHAR);");
        //total 20 item with primaryKey in table
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS customer(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR ,personName VARCHAR,companyName VARCHAR,contactEmail VARCHAR,workPhone VARCHAR,mobile VARCHAR," +
                        "contactDisplayName VARCHAR,website VARCHAR,GSTIN VARCHAR,DLNumber VARCHAR,state VARCHAR,city VARCHAR,country VARCHAR,zip VARCHAR," +
                        "address VARCHAR,fax VARCHAR,currency VARCHAR,paymentTerms VARCHAR,remarks VARCHAR,selectedStateCode VARCHAR,selectedTermCode VARCHAR);");

        //total 20 item with primaryKey in table
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS vendor(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR ,personName VARCHAR,companyName VARCHAR,contactEmail VARCHAR,workPhone VARCHAR,mobile VARCHAR," +
                        "contactDisplayName VARCHAR,website VARCHAR,GSTIN VARCHAR,DLNumber VARCHAR,state VARCHAR,city VARCHAR,country VARCHAR,zip VARCHAR," +
                        "address VARCHAR,fax VARCHAR,currency VARCHAR,paymentTerms VARCHAR,remarks VARCHAR,selectedStateCode VARCHAR,selectedTermCode VARCHAR);");
        //total 9 item with primaryKey in table
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS purchaseInvoice(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR , profileId VARCHAR ,invoiceNumber VARCHAR,invoiceDate VARCHAR,vendorId VARCHAR,vendorName VARCHAR,amount VARCHAR,placeOfPurchase VARCHAR,totalPrice VARCHAR,selectedPlaceId VARCHAR);");

        //total 13 item with primaryKey in table
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS pInvProduct(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR , profileId VARCHAR ,invoiceNumber VARCHAR,quantity VARCHAR,productId INTEGER,productName VARCHAR,codeHSN VARCHAR,barcode VARCHAR,costPrice VARCHAR,salePrice VARCHAR,taxRate VARCHAR,fromDate VARCHAR,toDate VARCHAR);");

        //total 18 item with primaryKey in table
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS sellInvoice(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR , profileId VARCHAR , companyId VARCHAR ,invoiceNumber VARCHAR,invoiceDate VARCHAR,customerId VARCHAR,customerName VARCHAR,freightAmount VARCHAR,transpoartMode VARCHAR,transpoartModeId VARCHAR,vehicleNumber VARCHAR,dateOfSupply VARCHAR,placeofSupply VARCHAR,placeofSupplyId VARCHAR,shipName VARCHAR,shipGSTIN VARCHAR,shipAddress VARCHAR,totalPrice VARCHAR,freightTaxRate VARCHAR,freightTotalAmount VARCHAR,cgstTax VARCHAR,cgstVal VARCHAR,sgstTax VARCHAR,sgstVal VARCHAR,igstTax VARCHAR,igstVal VARCHAR);");

        //total 13 item with primaryKey in table
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS sellInvProduct(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR , profileId VARCHAR , companyId VARCHAR ,invoiceNumber VARCHAR,quantity VARCHAR,productId INTEGER,productName VARCHAR,codeHSN VARCHAR,barcode VARCHAR,costPrice VARCHAR,salePrice VARCHAR,taxRate VARCHAR,fromDate VARCHAR,toDate VARCHAR,totalTaxAmount VARCHAR,cgstTax VARCHAR,cgstVal VARCHAR,sgstTax VARCHAR,sgstVal VARCHAR,igstTax VARCHAR,igstVal VARCHAR);");

        //total 18 item with primaryKey in table
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS vouchers(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR , profileId VARCHAR , companyId VARCHAR ,voucherNo VARCHAR , voucherDate VARCHAR , noteType VARCHAR , noteTypeId VARCHAR , noteReason VARCHAR , noteReasonId VARCHAR , customerId VARCHAR , customerName VARCHAR , invoiceNo VARCHAR , freightAmt VARCHAR , invoiceDate VARCHAR , preGST VARCHAR , placeOfSupply VARCHAR , placeofSupplyId VARCHAR , noteValue VARCHAR , taxRate VARCHAR , taxableValue VARCHAR , cessAmt VARCHAR);");
        //total 13 item with primaryKey in table
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS vouchersProduct(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR , profileId VARCHAR , companyId VARCHAR , voucherNo VARCHAR ,quantity VARCHAR,productId INTEGER,productName VARCHAR,codeHSN VARCHAR,barcode VARCHAR,costPrice VARCHAR,salePrice VARCHAR,taxRate VARCHAR,fromDate VARCHAR,toDate VARCHAR,totalTaxAmount VARCHAR,cgstTax VARCHAR,cgstVal VARCHAR,sgstTax VARCHAR,sgstVal VARCHAR,igstTax VARCHAR,igstVal VARCHAR);");

        //total 13 item with primaryKey in table
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS debtors(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR , profileId VARCHAR ,invoiceNo VARCHAR, customerId VARCHAR, customerName VARCHAR, paidAmount VARCHAR, payableAmount VARCHAR, balanceAmount VARCHAR, paymentMode VARCHAR, BankName VARCHAR, chequeNo VARCHAR, AccName VARCHAR, IFSC VARCHAR, AccNumber VARCHAR, BranchAddress VARCHAR, openingBalance VARCHAR, paymentReceived VARCHAR);");

        //total 13 item with primaryKey in table
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS creditors(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR , profileId VARCHAR ,invoiceNo VARCHAR, vendorId VARCHAR, vendorName VARCHAR, paidAmount VARCHAR, payableAmount VARCHAR, balanceAmount VARCHAR, paymentMode VARCHAR, BankName VARCHAR, chequeNo VARCHAR, AccName VARCHAR, IFSC VARCHAR, AccNumber VARCHAR, BranchAddress VARCHAR, openingBalance VARCHAR, paymentReceived VARCHAR);");


    }

    private boolean isModifyColumnInTable(String inTable, String columnToCheck) {
        try {
            Cursor dbCursor = db.query(inTable, null, null, null, null, null, null);
            String[] columnNames = dbCursor.getColumnNames();
            if (Arrays.asList(columnNames).contains(columnToCheck)) {
                L.m("colum exists");
                return false;
            } else {
                L.m("colum not exists");
                return true;
            }
        } catch (Exception Exp) {
            // Something went wrong. Missing the database? The table?
            L.m("colom not exists" + Exp.toString());
            return false;
        }
    }


    public void closeDB() {
        db.close();
    }

    public void dropProfileTable() {
        db.execSQL("DROP TABLE IF EXISTS profile");
    }

    public void dropProductTable() {
        db.execSQL("DROP TABLE IF EXISTS product");
    }

    public void dropServicesTable() {
        db.execSQL("DROP TABLE IF EXISTS services");
    }

    public void dropCustomerTable() {
        db.execSQL("DROP TABLE IF EXISTS customer");
    }

    public void dropVendorTable() {
        db.execSQL("DROP TABLE IF EXISTS vendor");
    }

    public void dropCompanyTable() {
        db.execSQL("DROP TABLE IF EXISTS companyDetail");
    }

    public void dropPurchaseInvoicesTable() {
        db.execSQL("DROP TABLE IF EXISTS purchaseInvoice");
        db.execSQL("DROP TABLE IF EXISTS pInvProduct");
        db.execSQL("DROP TABLE IF EXISTS creditors");
    }

    public void dropSellInvoicesTable() {
        db.execSQL("DROP TABLE IF EXISTS sellInvoice");
        db.execSQL("DROP TABLE IF EXISTS sellInvProduct");
        db.execSQL("DROP TABLE IF EXISTS debtors");
    }

    public void dropVouchersTable() {
        db.execSQL("DROP TABLE IF EXISTS vouchers");
        db.execSQL("DROP TABLE IF EXISTS vouchersProduct");
    }

    public void dropProductStockTable() {
        db.execSQL("DROP TABLE IF EXISTS productStock");
    }


    public ProfileModel updateProfile(ProfileModel result, String userId, String profileId, String name, CharSequence email, CharSequence mobile, CharSequence pan, CharSequence imagePath, CharSequence imageUrl) {
//        db.execSQL(
//                "INSERT INTO profile ('userId','name','email','mobile','pan','imagePath','imageUrl') VALUES(" +
//                        " '" + userId + "','" + name + "','" + email + "','" + mobile + "','" + pan + "','" + imagePath + "','" + imageUrl + "');");
//        result.setCompanyId(getCompanyProfileId(userId,name));
//        result.setOutputDB(Const.SUCCESS);
//        result.setOutputDBMsg("UpdateProfile created");
//        return result;
        // Searching product
        Cursor cursor = db.rawQuery("SELECT * FROM profile WHERE userId='" + userId + "' AND id='" + profileId + "'", null);
        if (!cursor.moveToFirst()) {
            L.m("Record not exist than insert query executed");
            // Inserting record
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("Profile not found");
            return result;
        } else {
            // Modifying record if found
            Cursor c = db.rawQuery("SELECT * FROM profile WHERE name='" + name + "' AND userId='" + userId + "' AND id='" + profileId + "'", null);
            if (!c.moveToFirst()) {
                L.m("Record not exist than insert query executed");
                // Inserting record
                db.execSQL("UPDATE profile SET name='" + name + "',email='" + email + "',mobile='" + mobile + "',pan='" + pan + "',imagePath='" + imagePath + "',imageUrl='" + imageUrl +
                        "' WHERE userId='" + userId + "' AND id='" + profileId + "'");

                result.setProfileId(getProfileByUserIdAndName(userId, name));
                result.setOutputDB(Const.SUCCESS);
                result.setOutputDBMsg("Profile updated");
                return result;
            } else {
                result.setOutputDB(Const.FAILURE);
                result.setOutputDBMsg("Profile name already exists");
                return result;
            }

        }

    }


    public Model insertProduct(Model result, String userId, CharSequence productName, CharSequence codeHSN, CharSequence barcode, CharSequence costPrice
            , CharSequence salePrice, CharSequence taxRate, CharSequence fromDate, CharSequence toDate, String unitCode, String unitName, String cessRate) {
        // Searching product
        Cursor c = db.rawQuery("SELECT * FROM product WHERE productName='" + productName + "' AND codeHSN='" + codeHSN + "' AND barcode='" + barcode + "'", null);
        if (!c.moveToFirst()) {
            L.m("Record not exist than insert query executed");
            // Inserting record
            db.execSQL(
                    "INSERT INTO product ('userId','productName','codeHSN','barcode','costPrice','salePrice','taxRate','fromDate','toDate','unitCode','unitName','cessRate') VALUES(" +
                            " '" + userId + "','" + productName + "','" + codeHSN + "','" + barcode + "','" + costPrice + "','" + salePrice + "','" + taxRate + "','" + fromDate + "','" + toDate + "','" + unitCode + "','" + unitName + "','" + cessRate + "');");
            result.setOutputDB(Const.SUCCESS);
            result.setOutputDBMsg("Product Added successful");
            return result;
        } else {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("Product already exist");
            return result;
        }
    }

    public Model insertVendor(Model result, String userId, String personName, String companyName, String contactEmail, String workPhone, String mobile, String contactDisplayName, String website, String GSTIN, String DLNumber, String state, String city, String country, String zip, String address, String fax, String currency, String paymentTerms, String remarks, String selectedStateCode, String selectedTermCode) {
        // Searching vendor
        Cursor c = db.rawQuery("SELECT * FROM vendor WHERE personName='" + personName + "' AND companyName='" + companyName + "'", null);
        if (!c.moveToFirst()) {
            L.m("Record not exist than insert query executed");
            // Inserting record
            db.execSQL(
                    "INSERT INTO vendor ('userId','personName','companyName','contactEmail','workPhone','mobile','contactDisplayName','website','GSTIN','DLNumber','state','city','country','zip','address','fax','currency','paymentTerms','remarks','selectedStateCode','selectedTermCode') VALUES(" +
                            " '" + userId + "','" + personName + "','" + companyName + "','" + contactEmail + "','" + workPhone + "','" + mobile + "','" + contactDisplayName + "','" + website + "','" + GSTIN + "','" + DLNumber + "','" + state + "','" + city + "','" + country + "','" + zip + "','" + address + "','" + fax + "','" + currency + "','" + paymentTerms + "','" + remarks + "','" + selectedStateCode + "','" + selectedTermCode + "');");
            result.setOutputDB(Const.SUCCESS);
            result.setOutputDBMsg("Vendor Added successful");
            return result;
        } else {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("Vendor already exist");
            return result;
        }
    }

    public Model insertCustomer(Model result, String userId, String personName, String companyName, String contactEmail, String workPhone, String mobile, String contactDisplayName, String website, String GSTIN, String DLNumber, String state, String city, String country, String zip, String address, String fax, String currency, String paymentTerms, String remarks, String selectedStateCode, String selectedTermCode) {
        // Searching product
        Cursor c = db.rawQuery("SELECT * FROM customer WHERE personName='" + personName + "' AND companyName='" + companyName + "'", null);
        if (!c.moveToFirst()) {
            L.m("Record not exist than insert query executed");
            // Inserting record
            db.execSQL(
                    "INSERT INTO customer ('userId','personName','companyName','contactEmail','workPhone','mobile','contactDisplayName','website','GSTIN','DLNumber','state','city','country','zip','address','fax','currency','paymentTerms','remarks','selectedStateCode','selectedTermCode') VALUES(" +
                            " '" + userId + "','" + personName + "','" + companyName + "','" + contactEmail + "','" + workPhone + "','" + mobile + "','" + contactDisplayName + "','" + website + "','" + GSTIN + "','" + DLNumber + "','" + state + "','" + city + "','" + country + "','" + zip + "','" + address + "','" + fax + "','" + currency + "','" + paymentTerms + "','" + remarks + "','" + selectedStateCode + "','" + selectedTermCode + "');");
            result.setOutputDB(Const.SUCCESS);
            result.setOutputDBMsg("Customer Added successful");
            return result;
        } else {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("Customer already exist");
            return result;
        }
    }


    public Model editVendor(Model result, String vendortId, String userId, String personName, String companyName, String contactEmail, String workPhone, String mobile, String contactDisplayName, String website, String GSTIN, String DLNumber, String state, String city, String country, String zip, String address, String fax, String currency, String paymentTerms, String remarks, String selectedStateCode, String selectedTermCode) {
        // Searching vendor
        Cursor c = db.rawQuery("SELECT * FROM vendor WHERE id='" + vendortId + "' AND userId='" + userId + "'", null);
        if (!c.moveToFirst()) {
            // Inserting record
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("Vendor not exist");
            return result;
        } else {
//             Modifying record if found
            db.execSQL("UPDATE vendor SET personName='" + personName + "',companyName='" + companyName + "',contactEmail='" + contactEmail + "',workPhone='" + workPhone + "',mobile='" + mobile + "',contactDisplayName='" + contactDisplayName + "',website='" + website + "',GSTIN='" + GSTIN + "',DLNumber='" + DLNumber + "',state='" + state + "',city='" + city + "',country='" + country + "',zip='" + zip + "',address='" + address + "',fax='" + fax + "',currency='" + currency + "',paymentTerms='" + paymentTerms + "',remarks='" + remarks + "',selectedStateCode='" + selectedStateCode + "',selectedTermCode='" + selectedTermCode +
                    "' WHERE id='" + vendortId + "' AND userId='" + userId + "'");

            result.setOutputDB(Const.SUCCESS);
            result.setOutputDBMsg("Vendor edited successful");
            return result;
        }
    }

    public Model editCustomer(Model result, String custometId, String userId, String personName, String companyName, String contactEmail, String workPhone, String mobile, String contactDisplayName, String website, String GSTIN, String DLNumber, String state, String city, String country, String zip, String address, String fax, String currency, String paymentTerms, String remarks, String selectedStateCode, String selectedTermCode) {
        // Searching customer
        Cursor c = db.rawQuery("SELECT * FROM customer WHERE id='" + custometId + "' AND userId='" + userId + "'", null);
        if (!c.moveToFirst()) {
            // Inserting record
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("Customer not exist");
            return result;
        } else {
//             Modifying record if found
            db.execSQL("UPDATE customer SET personName='" + personName + "',companyName='" + companyName + "',contactEmail='" + contactEmail + "',workPhone='" + workPhone + "',mobile='" + mobile + "',contactDisplayName='" + contactDisplayName + "',website='" + website + "',GSTIN='" + GSTIN + "',DLNumber='" + DLNumber + "',state='" + state + "',city='" + city + "',country='" + country + "',zip='" + zip + "',address='" + address + "',fax='" + fax + "',currency='" + currency + "',paymentTerms='" + paymentTerms + "',remarks='" + remarks + "',selectedStateCode='" + selectedStateCode + "',selectedTermCode='" + selectedTermCode +
                    "' WHERE id='" + custometId + "' AND userId='" + userId + "'");

            result.setOutputDB(Const.SUCCESS);
            result.setOutputDBMsg("Customer edited successful");
            return result;
        }
    }

    public Model copyVendor(Model result, String vendorId, String userId, String personName, String companyName, String contactEmail, String workPhone, String mobile, String contactDisplayName, String website, String GSTIN, String DLNumber, String state, String city, String country, String zip, String address, String fax, String currency, String paymentTerms, String remarks, String selectedStateCode, String selectedTermCode) {
        // Searching vendor
        Cursor c = db.rawQuery("SELECT * FROM vendor WHERE personName='" + personName + "' AND companyName='" + companyName + "'", null);
        if (!c.moveToFirst()) {
            // Inserting record
            db.execSQL(
                    "INSERT INTO vendor ('userId','personName','companyName','contactEmail','workPhone','mobile','contactDisplayName','website','GSTIN','DLNumber','state','city','country','zip','address','fax','currency','paymentTerms','remarks','selectedStateCode','selectedTermCode') VALUES(" +
                            " '" + userId + "','" + personName + "','" + companyName + "','" + contactEmail + "','" + workPhone + "','" + mobile + "','" + contactDisplayName + "','" + website + "','" + GSTIN + "','" + DLNumber + "','" + state + "','" + city + "','" + country + "','" + zip + "','" + address + "','" + fax + "','" + currency + "','" + paymentTerms + "','" + remarks + "','" + selectedStateCode + "','" + selectedTermCode + "');");
            result.setOutputDB(Const.SUCCESS);
            result.setOutputDBMsg("Vendor Copied successful");
            return result;
        } else {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("Vendor already exist");
            return result;
        }
    }

    public Model copyCustomer(Model result, String custometId, String userId, String personName, String companyName, String contactEmail, String workPhone, String mobile, String contactDisplayName, String website, String GSTIN, String DLNumber, String state, String city, String country, String zip, String address, String fax, String currency, String paymentTerms, String remarks, String selectedStateCode, String selectedTermCode) {
        // Searching customer
        Cursor c = db.rawQuery("SELECT * FROM customer WHERE personName='" + personName + "' AND companyName='" + companyName + "'", null);
        if (!c.moveToFirst()) {
            // Inserting record
            db.execSQL(
                    "INSERT INTO customer ('userId','personName','companyName','contactEmail','workPhone','mobile','contactDisplayName','website','GSTIN','DLNumber','state','city','country','zip','address','fax','currency','paymentTerms','remarks','selectedStateCode','selectedTermCode') VALUES(" +
                            " '" + userId + "','" + personName + "','" + companyName + "','" + contactEmail + "','" + workPhone + "','" + mobile + "','" + contactDisplayName + "','" + website + "','" + GSTIN + "','" + DLNumber + "','" + state + "','" + city + "','" + country + "','" + zip + "','" + address + "','" + fax + "','" + currency + "','" + paymentTerms + "','" + remarks + "','" + selectedStateCode + "','" + selectedTermCode + "');");
            result.setOutputDB(Const.SUCCESS);
            result.setOutputDBMsg("Customer Copied successful");
            return result;
        } else {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("Customer already exist");
            return result;
        }
    }

    public Model editProduct(Model result, String productId, String userId, String productName, String codeHSN, String barcode, String costPrice, String salePrice, String taxRate, String fromDate, String toDate, String unitCode, String unitName, String cessRate) {
        // Searching product
        Cursor c = db.rawQuery("SELECT * FROM product WHERE id='" + productId + "' AND userId='" + userId + "'", null);
        if (!c.moveToFirst()) {
            L.m("Record not exist than insert query executed");
            // Inserting record
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("Product not exist");
            return result;
        } else {
//             Modifying record if found
            db.execSQL("UPDATE product SET productName='" + productName + "',codeHSN='" + codeHSN + "',barcode='" + barcode + "',costPrice='" + costPrice + "',salePrice='" + salePrice + "',taxRate='" + taxRate + "',fromDate='" + fromDate + "',toDate='" + toDate + "',unitCode='" + unitCode + "',unitName='" + unitName + "',cessRate='" + cessRate +
                    "' WHERE id='" + productId + "' AND userId='" + userId + "'");

            result.setOutputDB(Const.SUCCESS);
            result.setOutputDBMsg("Product edited successful");
            return result;
        }
    }

    public Model copyProduct(Model result, String productId, String userId, String productName, String codeHSN, String barcode, String costPrice, String salePrice, String taxRate, String fromDate, String toDate, String unitCode, String unitName, String cessRate) {
        // Searching product
        Cursor c = db.rawQuery("SELECT * FROM product WHERE productName='" + productName + "' AND codeHSN='" + codeHSN + "' AND barcode='" + barcode + "'", null);
        if (!c.moveToFirst()) {
            L.m("Record not exist than insert query executed");
            // Inserting record
            db.execSQL(
                    "INSERT INTO product ('userId','productName','codeHSN','barcode','costPrice','salePrice','taxRate','fromDate','toDate','unitCode','unitName','cessRate') VALUES(" +
                            " '" + userId + "','" + productName + "','" + codeHSN + "','" + barcode + "','" + costPrice + "','" + salePrice + "','" + taxRate + "','" + fromDate + "','" + toDate + "','" + unitCode + "','" + unitName + "','" + cessRate + "');");
            result.setOutputDB(Const.SUCCESS);
            result.setOutputDBMsg("Product Added successful");
            return result;
        } else {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("Product already exist");
            return result;
        }
    }

    public String deleteProduct(ProductModel product) {
        // Searching product
        Cursor c = db.rawQuery("SELECT * FROM product WHERE id='" + product.getProductId() + "' AND userId='" + product.getProductUserId() + "'", null);
        if (c.moveToFirst()) {
            // Deleting record if found
            db.execSQL("DELETE FROM product WHERE id='" + product.getProductId() + "'AND userId='" + product.getProductUserId() + "'");
            delectStockById(product);
            return "Product Deleted";
        } else {
            return "Invalid Product Id";
        }
    }

    private void delectStockById(ProductModel product) {
        Cursor cursor = db.rawQuery("SELECT * FROM productStock WHERE userId='" + product.getProductUserId() + "' AND productId='" + product.getProductId() + "'", null);
        if (cursor.moveToFirst()) {
            // Deleting record if found
            db.execSQL("DELETE FROM productStock WHERE userId='" + product.getProductUserId() + "'AND productId='" + product.getProductId() + "'");

        } else {
            //record not found
        }
    }


    public Boolean isProductAvailable(String userId) {
        //Cursor c = db.rawQuery("SELECT * FROM product ORDER BY id DESC", null);
        Cursor c = db.rawQuery("SELECT * FROM product WHERE userId='" + userId + "' ORDER BY id DESC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            return false;
        } else {
            return true;
        }
    }


    public ProductModel getProductList(ProductModel result, String userId) {

        //Cursor c = db.rawQuery("SELECT * FROM product ORDER BY id DESC", null);
        Cursor c = db.rawQuery("SELECT * FROM product WHERE userId='" + userId + "' ORDER BY id DESC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("No record found");
            return result;
        }
        result.setOutputDB(Const.SUCCESS);
        result.setList(new ArrayList<ProductModel>());
        while (c.moveToNext()) {
            ProductModel model = new ProductModel();
            model.setProductId(c.getString(0));
            model.setProductUserId(c.getString(1));
            model.setProductName(c.getString(2));
            model.setCodeHSN(c.getString(3));
            model.setBarcode(c.getString(4));
            model.setCostPrice(c.getString(5));
            model.setSalePrice(c.getString(6));
            model.setTaxRate(c.getString(7));
            model.setFromDate(c.getString(8));
            model.setToDate(c.getString(9));
            result.getList().add(model);
        }
        return result;
    }


    public Map<String, String> viewProfile(String userId, String profileId) {
        // Retrieving all records
        Cursor c = db.rawQuery("SELECT * FROM profile WHERE id='" + profileId + "' AND userId='" + userId + "' ORDER BY id DESC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            L.m("Error No records found");
            return null;
        }
        Map<String, String> list = new HashMap<>();
        while (c.moveToNext()) {
            list.put(Const.PROFILE_USER_ID, c.getString(1));
            list.put(Const.PROFILE_NAME, c.getString(2));
            list.put(Const.PROFILE_EMAIL, c.getString(3));
            list.put(Const.PROFILE_MOBILE, c.getString(4));
            list.put(Const.PROFILE_PAN, c.getString(5));
            list.put(Const.PROFILE_IMAGE_PATH, c.getString(6));
            list.put(Const.PROFILE_IMAGE_URL, c.getString(7));
        }
        return list;
    }

    public Model insertService(Model result, String userId, String serviceName, String codeSAC, String salePrice, String taxRate, String fromDate, String toDate) {
        // Searching service
        Cursor c = db.rawQuery("SELECT * FROM services WHERE serviceName='" + serviceName + "' AND codeSAC='" + codeSAC + "'", null);
        if (!c.moveToFirst()) {
            L.m("Record not exist than insert query executed");
            // Inserting record
            db.execSQL(
                    "INSERT INTO services ('userId','serviceName','codeSAC','salePrice','taxRate','fromDate','toDate') VALUES(" +
                            " '" + userId + "','" + serviceName + "','" + codeSAC + "','" + salePrice + "','" + taxRate + "','" + fromDate + "','" + toDate + "');");
            result.setOutputDB(Const.SUCCESS);
            result.setOutputDBMsg("Service Added successful");
            return result;
        } else {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("Service already exist");
            return result;
        }
    }

    public Model editService(Model result, String serviceId, String userId, String serviceName, String codeSAC, String salePrice, String taxRate, String fromDate, String toDate) {
        // Searching service
        Cursor c = db.rawQuery("SELECT * FROM services WHERE id='" + serviceId + "' AND userId='" + userId + "'", null);
        if (!c.moveToFirst()) {
            // Inserting record
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("Service not exist");
            return result;
        } else {
            // Modifying record if found
            db.execSQL("UPDATE services SET serviceName='" + serviceName + "',codeSAC='" + codeSAC + "',salePrice='" + salePrice + "',taxRate='" + taxRate + "',fromDate='" + fromDate + "',toDate='" + toDate +
                    "' WHERE id='" + serviceId + "' AND userId='" + userId + "'");

            result.setOutputDB(Const.SUCCESS);
            result.setOutputDBMsg("Service edited successful");
            return result;
        }
    }

    public Model copyService(Model result, String serviceId, String userId, String serviceName, String codeSAC, String salePrice, String taxRate, String fromDate, String toDate) {
        // Searching service
        Cursor c = db.rawQuery("SELECT * FROM services WHERE serviceName='" + serviceName + "' AND codeSAC='" + codeSAC + "'", null);
        if (!c.moveToFirst()) {
            L.m("Record not exist than insert query executed");
            // Inserting record
            db.execSQL(
                    "INSERT INTO services ('userId','serviceName','codeSAC','salePrice','taxRate','fromDate','toDate') VALUES(" +
                            " '" + userId + "','" + serviceName + "','" + codeSAC + "','" + salePrice + "','" + taxRate + "','" + fromDate + "','" + toDate + "');");
            result.setOutputDB(Const.SUCCESS);
            result.setOutputDBMsg("Service copied successful");
            return result;
        } else {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("Service already exist");
            return result;
        }
    }

    public String deleteService(ServiceModel service) {
        // Searching service
        Cursor c = db.rawQuery("SELECT * FROM services WHERE id='" + service.getServiceId() + "' AND userId='" + service.getServiceUserId() + "'", null);
        if (c.moveToFirst()) {
            // Deleting record if found
            db.execSQL("DELETE FROM services WHERE id='" + service.getServiceId() + "'AND userId='" + service.getServiceUserId() + "'");
            return "Product Deleted";
        } else {
            return "Invalid Product Id";
        }
    }

    public ServiceModel getServicesList(ServiceModel result, String userId) {
//        Cursor c = db.rawQuery("SELECT * FROM services ORDER BY id DESC", null);
        Cursor c = db.rawQuery("SELECT * FROM services WHERE userId='" + userId + "' ORDER BY id DESC", null);
// Checking if no records found
        if (c.getCount() == 0) {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("No record found");
            return result;
        }
        result.setOutputDB(Const.SUCCESS);
        result.setList(new ArrayList<ServiceModel>());
        while (c.moveToNext()) {
            ServiceModel model = new ServiceModel();
            model.setServiceId(c.getString(0));
            model.setServiceUserId(c.getString(1));
            model.setServiceName(c.getString(2));
            model.setCodeSAC(c.getString(3));
            model.setSalePrice(c.getString(4));
            model.setTaxRate(c.getString(5));
            model.setFromDate(c.getString(6));
            model.setToDate(c.getString(7));
            result.getList().add(model);
        }
        return result;
    }


    public String deleteCustomer(CustomerModel customer) {
        // Searching customer
        Cursor c = db.rawQuery("SELECT * FROM customer WHERE id='" + customer.getCustometId() + "' AND userId='" + customer.getCustometUserId() + "'", null);
        if (c.moveToFirst()) {
            // Deleting record if found
            db.execSQL("DELETE FROM customer WHERE id='" + customer.getCustometId() + "'AND userId='" + customer.getCustometUserId() + "'");
            return "Customer Deleted";
        } else {
            return "Invalid Customer Id";
        }
    }

    public CustomerModel getCustomerList(CustomerModel result, String userId) {
//        Cursor c = db.rawQuery("SELECT * FROM customer ORDER BY id DESC", null);
        Cursor c = db.rawQuery("SELECT * FROM customer WHERE userId='" + userId + "' ORDER BY id DESC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("No record found");
            return result;
        }
        result.setOutputDB(Const.SUCCESS);
        result.setList(new ArrayList<CustomerModel>());

        while (c.moveToNext()) {
            CustomerModel model = new CustomerModel();
            model.setCustometId(c.getString(0));
            model.setCustometUserId(c.getString(1));
            model.setPersonName(c.getString(2));
            model.setCompanyName(c.getString(3));
            model.setContactEmail(c.getString(4));
            model.setWorkPhone(c.getString(5));
            model.setMobile(c.getString(6));
            model.setContactDisplayName(c.getString(7));
            model.setWebsite(c.getString(8));
            model.setGSTIN(c.getString(9));
            model.setDLNumber(c.getString(10));
            model.setState(c.getString(11));
            model.setCity(c.getString(12));
            model.setCountry(c.getString(13));
            model.setZip(c.getString(14));
            model.setAddress(c.getString(15));
            model.setFax(c.getString(16));
            model.setCurrency(c.getString(17));
            model.setPaymentTerms(c.getString(18));
            model.setRemarks(c.getString(19));
            model.setSelectedStateCode(c.getString(20));
            model.setSelectedTermCode(c.getString(21));
            result.getList().add(model);
        }
        return result;
    }


    public String deleteVendor(VendorModel vendor) {
        // Searching vendor
        Cursor c = db.rawQuery("SELECT * FROM vendor WHERE id='" + vendor.getVendorId() + "' AND userId='" + vendor.getVendorUserId() + "'", null);
        if (c.moveToFirst()) {
            // Deleting record if found
            db.execSQL("DELETE FROM vendor WHERE id='" + vendor.getVendorId() + "'AND userId='" + vendor.getVendorUserId() + "'");
            return "Vendor Deleted";
        } else {
            return "Invalid Vendor Id";
        }
    }

    public VendorModel getVendorList(VendorModel result, String userId) {
//        Cursor c = db.rawQuery("SELECT * FROM vendor ORDER BY id DESC", null);
        Cursor c = db.rawQuery("SELECT * FROM vendor WHERE userId='" + userId + "' ORDER BY id DESC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("No record found");
            return result;
        }
        result.setOutputDB(Const.SUCCESS);
        result.setList(new ArrayList<VendorModel>());

        while (c.moveToNext()) {
            VendorModel model = new VendorModel();
            model.setVendorId(c.getString(0));
            model.setVendorUserId(c.getString(1));
            model.setPersonName(c.getString(2));
            model.setCompanyName(c.getString(3));
            model.setContactEmail(c.getString(4));
            model.setWorkPhone(c.getString(5));
            model.setMobile(c.getString(6));
            model.setContactDisplayName(c.getString(7));
            model.setWebsite(c.getString(8));
            model.setGSTIN(c.getString(9));
            model.setDLNumber(c.getString(10));
            model.setState(c.getString(11));
            model.setCity(c.getString(12));
            model.setCountry(c.getString(13));
            model.setZip(c.getString(14));
            model.setAddress(c.getString(15));
            model.setFax(c.getString(16));
            model.setCurrency(c.getString(17));
            model.setPaymentTerms(c.getString(18));
            model.setRemarks(c.getString(19));
            model.setSelectedStateCode(c.getString(20));
            model.setSelectedTermCode(c.getString(21));
            result.getList().add(model);
        }
        return result;
    }

    public ProductModel
    filterProductByBarcode(String userId, String mSearchBarcode) {
        ProductModel result = new ProductModel();
        //Cursor c = db.rawQuery("SELECT * FROM product ORDER BY id DESC", null);
        Cursor c = db.rawQuery("SELECT * FROM product WHERE userId='" + userId + "' AND barcode='" + mSearchBarcode + "' ORDER BY id DESC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("Product not found in database. Are you want to add this product in directory?");
            return result;
        }
        result.setOutputDB(Const.SUCCESS);
        result.setList(new ArrayList<ProductModel>());
        while (c.moveToNext()) {
            ProductModel model = new ProductModel();
            model.setProductId(c.getString(0));
            model.setProductUserId(c.getString(1));
            model.setProductName(c.getString(2));
            model.setCodeHSN(c.getString(3));
            model.setBarcode(c.getString(4));
            model.setCostPrice(c.getString(5));
            model.setSalePrice(c.getString(6));
            model.setTaxRate(c.getString(7));
            model.setFromDate(c.getString(8));
            model.setToDate(c.getString(9));
            result.getList().add(model);
        }
        return result;
    }

    public VendorModel getVendorIdByVendorAndCompanyName(String userId, String personName, String companyName) {
        VendorModel model = new VendorModel();
        Cursor c = db.rawQuery("SELECT * FROM vendor WHERE userId='" + userId + "' AND personName='" + personName + "' AND companyName='" + companyName + "' ORDER BY id DESC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            model.setOutputDB(Const.FAILURE);
            model.setOutputDBMsg("Vendor not found");
            return model;
        }
        model.setOutputDB(Const.SUCCESS);

        while (c.moveToNext()) {
            model.setVendorId(c.getString(0));
            model.setVendorUserId(c.getString(1));
            model.setPersonName(c.getString(2));
            model.setCompanyName(c.getString(3));
            model.setContactEmail(c.getString(4));
            model.setWorkPhone(c.getString(5));
            model.setMobile(c.getString(6));
            model.setContactDisplayName(c.getString(7));
            model.setWebsite(c.getString(8));
            model.setGSTIN(c.getString(9));
            model.setDLNumber(c.getString(10));
            model.setState(c.getString(11));
            model.setCity(c.getString(12));
            model.setCountry(c.getString(13));
            model.setZip(c.getString(14));
            model.setAddress(c.getString(15));
            model.setFax(c.getString(16));
            model.setCurrency(c.getString(17));
            model.setPaymentTerms(c.getString(18));
            model.setRemarks(c.getString(19));
        }
        return model;
    }

    public String getPurchaseInvoiceNumber(String userId, String profileId) {
        String response = "";
        //        Cursor c = db.rawQuery("SELECT * FROM vendor ORDER BY id DESC", null);
        Cursor c = db.rawQuery("SELECT invoiceNumber FROM purchaseInvoice WHERE userId='" + userId + "' AND profileId='" + profileId + "' ORDER BY invoiceNumber ASC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            return "";
        }
        while (c.moveToNext()) {
            response = c.getString(0);
            L.m(response);
        }
        return response;
    }


    public Model addPurchases(Model result, String userId, String profileId, String invoiceNumber, String invoiceDate, String vendorId, String vendorName, String amount, String placeOfPurchase, String totalPrice, String selectedPlaceId, ArrayList<ProductModel> productList, PaymentModel paymentDetail, String stockType) {
        // Searching product
        Cursor c = db.rawQuery("SELECT * FROM purchaseInvoice WHERE userId='" + userId + "' AND profileId='" + profileId + "' AND invoiceNumber='" + invoiceNumber + "'", null);
        if (!c.moveToFirst()) {
            L.m("Record not exist than insert query executed");
            // Inserting record
            db.execSQL(
                    "INSERT INTO purchaseInvoice ('userId','profileId','invoiceNumber','invoiceDate','vendorId','vendorName','amount','placeOfPurchase','totalPrice','selectedPlaceId') VALUES(" +
                            " '" + userId + "','" + profileId + "','" + invoiceNumber + "','" + invoiceDate + "','" + vendorId + "','" + vendorName + "','" + amount + "','" + placeOfPurchase + "','" + totalPrice + "','" + selectedPlaceId + "');");
            setCreditorsInDatabase(userId, profileId, invoiceNumber, paymentDetail);
            for (ProductModel model : productList) {
                db.execSQL(
                        "INSERT INTO pInvProduct ('userId','profileId','invoiceNumber','quantity','productId','productName','codeHSN','barcode','costPrice','salePrice','taxRate','fromDate','toDate') VALUES(" +
                                " '" + userId + "','" + profileId + "','" + invoiceNumber + "','" + model.getQuantity() + "','" + model.getProductId() + "','" + model.getProductName() + "','" + model.getCodeHSN() + "','" + model.getBarcode() + "','" + model.getCostPrice() + "','" + model.getSalePrice() + "','" + model.getTaxRate() + "','" + model.getFromDate() + "','" + model.getToDate() + "');");
                maintainProductStock(userId, profileId, invoiceNumber, model.getProductId(), model.getQuantity(), stockType);
            }
            result.setOutputDB(Const.SUCCESS);
            result.setOutputDBMsg("Invoice created successful");
            return result;
        } else {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("Invoice already exist");
            return result;
        }
    }

    private void setCreditorsInDatabase(String userId, String profileId, String invoiceNo, PaymentModel pay) {
        Cursor c = db.rawQuery("SELECT * FROM creditors WHERE userId='" + userId + "' AND profileId='" + profileId + "' AND invoiceNo='" + invoiceNo + "'", null);
        if (!c.moveToFirst()) {
            // Inserting record
            db.execSQL(
                    "INSERT INTO creditors ('userId','profileId','invoiceNo','vendorId','vendorName','paidAmount','payableAmount','balanceAmount','paymentMode','BankName','chequeNo','AccName','IFSC','AccNumber','BranchAddress','openingBalance','paymentReceived') VALUES(" +
                            " '" + userId + "','" + profileId + "','" + pay.getInvoiceNo() + "','" + pay.getVendorId() + "','" + pay.getVendorName() + "','" + pay.getPaidAmount() + "','" + pay.getPayableAmount() + "','" + pay.getBalanceAmount() + "','" + pay.getPaymentMode() + "','" + pay.getBankName() + "','" + pay.getChequeNo() + "','" + pay.getAccName() + "','" + pay.getIFSC() + "','" + pay.getAccNumber() + "','" + pay.getBranchAddress() + "','" + pay.getOpeningBalance() + "','" + pay.getPaymentReceived() + "');");
        }
    }

    public Model setCreditorsByTrans(Model result, String userId, String profileId, PaymentModel pay) {
        db.execSQL(
                "INSERT INTO creditors ('userId','profileId','invoiceNo','vendorId','vendorName','paidAmount','payableAmount','balanceAmount','paymentMode','BankName','chequeNo','AccName','IFSC','AccNumber','BranchAddress','openingBalance','paymentReceived') VALUES(" +
                        " '" + userId + "','" + profileId + "','" + pay.getInvoiceNo() + "','" + pay.getVendorId() + "','" + pay.getVendorName() + "','" + pay.getPaidAmount() + "','" + pay.getPayableAmount() + "','" + pay.getBalanceAmount() + "','" + pay.getPaymentMode() + "','" + pay.getBankName() + "','" + pay.getChequeNo() + "','" + pay.getAccName() + "','" + pay.getIFSC() + "','" + pay.getAccNumber() + "','" + pay.getBranchAddress() + "','" + pay.getOpeningBalance() + "','" + pay.getPaymentReceived() + "');");
        result.setOutputDB(Const.SUCCESS);
        result.setOutputDBMsg("Transaction successful");
        return result;
    }


    private void maintainProductStock(String userId, String profileId, String invoiceNumber, String productId, String quantity, String stockType) {
        // Searching product
        Cursor c = db.rawQuery("SELECT * FROM productStock WHERE userId='" + userId + "' AND profileId='" + profileId + "' AND invoiceNumber='" + invoiceNumber + "' AND productId='" + productId + "' AND stockType='" + stockType + "'", null);
        if (!c.moveToFirst()) {
            L.m("Record not exist than insert query executed");
            // Inserting record
            db.execSQL(
                    "INSERT INTO productStock ('userId','profileId','invoiceNumber','productId','quantity','stockType') VALUES(" +
                            " '" + userId + "','" + profileId + "','" + invoiceNumber + "','" + productId + "','" + quantity + "','" + stockType + "');");

        } else {
//            String query = "SELECT quantity from productStock WHERE userId='" + userId + "' AND profileId='" + profileId + "' AND invoiceNumber='" + invoiceNumber + "' AND productId='" + productId + "' order by id DESC limit 1";
//            Cursor cursor = db.rawQuery(query, null);
//            int qty = 0;
//            if (cursor != null && cursor.moveToFirst()) {
//                qty = cursor.getInt(0); //The 0 is the column index, we only have 1 column, so the index is 0
//            }
//            int totalQty = qty + convertToInt(quantity);
            db.execSQL("UPDATE productStock SET quantity='" + quantity +
                    "' WHERE userId='" + userId + "' AND profileId='" + profileId + "' AND invoiceNumber='" + invoiceNumber + "' AND productId='" + productId + "' AND stockType='" + stockType + "'");
        }
    }

    public PurchasesModel getPurchasesList(PurchasesModel result, String userId, String profileId) {
        //        Cursor c = db.rawQuery("SELECT * FROM vendor ORDER BY id DESC", null);
        Cursor c = db.rawQuery("SELECT * FROM purchaseInvoice WHERE userId='" + userId + "' AND profileId='" + profileId + "' ORDER BY id DESC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("No record found");
            return result;
        }
        result.setOutputDB(Const.SUCCESS);
        result.setList(new ArrayList<PurchasesModel>());

        while (c.moveToNext()) {
            PurchasesModel model = new PurchasesModel();
            model.setInvoiceId(c.getString(0));
            model.setInvoiceUserId(c.getString(1));
            model.setInvoiceProfileId(c.getString(2));
            model.setInvoiceNumber(c.getString(3));
            model.setInvoiceDate(c.getString(4));
            model.setVendorId(c.getString(5));
            model.setVendorName(c.getString(6));
            model.setFreightAmount(c.getString(7));
            model.setPlaceOfPurchase(c.getString(8));
            model.setTotalAmount(c.getString(9));
            model.setPlaceOfPurchaseId(c.getString(10));
            model.setProductList(getInvoiceProductList(userId, profileId, c.getString(3)));
            result.getList().add(model);
        }
        return result;
    }

    private ArrayList<ProductModel> getInvoiceProductList(String userId, String profileId, String invoiceNumber) {
        Cursor c = db.rawQuery("SELECT * FROM pInvProduct WHERE userId='" + userId + "' AND profileId='" + profileId + "' AND invoiceNumber='" + invoiceNumber + "'", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            return null;
        }
        ArrayList<ProductModel> prodList = new ArrayList<>();
        while (c.moveToNext()) {
            ProductModel model = new ProductModel();
            model.setPurchasesProdId(c.getString(0));
            model.setProductUserId(c.getString(1));
            model.setProductProfileId(c.getString(2));
            model.setInvNumber(c.getString(3));
            model.setQuantity(c.getString(4));
            model.setProductId(c.getString(5));
            model.setProductName(c.getString(6));
            model.setCodeHSN(c.getString(7));
            model.setBarcode(c.getString(8));
            model.setCostPrice(c.getString(9));
            model.setSalePrice(c.getString(10));
            model.setTaxRate(c.getString(11));
            model.setFromDate(c.getString(12));
            model.setToDate(c.getString(13));
            prodList.add(model);
        }
        return prodList;
    }

    public Model editPurchases(Model result, String invoiceId, ArrayList<String> deletedProduct, String userId, String profileId, String invoiceNumber, String invoiceDate, String vendorId, String vendorName, String amount, String placeOfPurchase, String totalPrice, String selectedPlaceId, ArrayList<ProductModel> productList, PaymentModel paymentDetail, String stockType) {
        // Searching product
        Cursor c = db.rawQuery("SELECT * FROM purchaseInvoice WHERE id='" + invoiceId + "' AND userId='" + userId + "' AND profileId='" + profileId + "' AND invoiceNumber='" + invoiceNumber + "'", null);
        if (!c.moveToFirst()) {
            // Inserting record
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("Invoice not exist");
            return result;
        } else {
//             Modifying record if found
            updatCreditorsInDatabase(userId, profileId, invoiceNumber, paymentDetail);
            db.execSQL("UPDATE purchaseInvoice SET vendorId='" + vendorId + "',invoiceDate='" + invoiceDate + "',vendorName='" + vendorName + "',amount='" + amount + "',placeOfPurchase='" + placeOfPurchase + "',totalPrice='" + totalPrice + "',selectedPlaceId='" + selectedPlaceId +
                    "' WHERE id='" + invoiceId + "' AND userId='" + userId + "' AND profileId='" + profileId + "'");
            editInvoiceProductList(userId, profileId, invoiceNumber, productList, deletedProduct, stockType);
            result.setOutputDB(Const.SUCCESS);
            result.setOutputDBMsg("Invoice edited successful");
            return result;
        }
    }

    private void updatCreditorsInDatabase(String userId, String profileId, String invoiceNo, PaymentModel pay) {
        Cursor c = db.rawQuery("SELECT * FROM creditors WHERE userId='" + userId + "' AND profileId='" + profileId + "' AND invoiceNo='" + invoiceNo + "'", null);
        if (c.moveToFirst()) {
            //Modifying record if found
            db.execSQL("UPDATE creditors SET vendorId='" + pay.getVendorId() + "',vendorName='" + pay.getVendorName() + "',paidAmount='" + pay.getPaidAmount() + "',payableAmount='" + pay.getPayableAmount() + "',balanceAmount='" + pay.getBalanceAmount() + "',paymentMode='" + pay.getPaymentMode() + "',BankName='" + pay.getBankName() + "',chequeNo='" + pay.getChequeNo() + "',AccName='" + pay.getAccName() + "',IFSC='" + pay.getIFSC() + "',AccNumber='" + pay.getAccNumber() + "',BranchAddress='" + pay.getBranchAddress() + "',openingBalance='" + pay.getOpeningBalance() + "',paymentReceived='" + pay.getPaymentReceived() +
                    "' WHERE userId='" + userId + "' AND profileId='" + profileId + "' AND invoiceNo='" + invoiceNo + "'");
        }
    }

    private void editInvoiceProductList(String userId, String profileId, String invoiceNumber, ArrayList<ProductModel> productList, ArrayList<String> deletedProduct, String stockType) {
        for (String data : deletedProduct) {
            // Searching product
            Cursor c = db.rawQuery("SELECT * FROM pInvProduct WHERE productId='" + data + "' AND userId='" + userId + "' AND profileId='" + profileId + "' AND invoiceNumber='" + invoiceNumber + "'", null);
            if (c.moveToFirst()) {
                // Deleting record if found
                db.execSQL("DELETE FROM pInvProduct WHERE productId='" + data + "'AND userId='" + userId + "'AND profileId='" + profileId + "'AND invoiceNumber='" + invoiceNumber + "'");

            } else {
                //record not found
            }
            delectStockBy(userId, profileId, invoiceNumber, stockType);
        }
        for (ProductModel model : productList) {
            // Searching product
            Cursor c = db.rawQuery("SELECT * FROM pInvProduct WHERE productId='" + model.getProductId() + "' AND userId='" + userId + "' AND profileId='" + profileId + "' AND invoiceNumber='" + invoiceNumber + "'", null);
            if (!c.moveToFirst()) {
                //record not found
                db.execSQL(
                        "INSERT INTO pInvProduct ('userId','profileId','invoiceNumber','quantity','productId','productName','codeHSN','barcode','costPrice','salePrice','taxRate','fromDate','toDate') VALUES(" +
                                " '" + userId + "','" + profileId + "','" + invoiceNumber + "','" + model.getQuantity() + "','" + model.getProductId() + "','" + model.getProductName() + "','" + model.getCodeHSN() + "','" + model.getBarcode() + "','" + model.getCostPrice() + "','" + model.getSalePrice() + "','" + model.getTaxRate() + "','" + model.getFromDate() + "','" + model.getToDate() + "');");
            } else {
                //Modifying record if found
                db.execSQL("UPDATE pInvProduct SET quantity='" + model.getQuantity() + "',productName='" + model.getProductName() + "',codeHSN='" + model.getCodeHSN() + "',barcode='" + model.getBarcode() + "',costPrice='" + model.getCostPrice() + "',salePrice='" + model.getSalePrice() + "',taxRate='" + model.getTaxRate() + "',fromDate='" + model.getFromDate() + "',toDate='" + model.getToDate() +
                        "' WHERE productId='" + model.getProductId() + "' AND invoiceNumber='" + invoiceNumber + "' AND userId='" + userId + "' AND profileId='" + profileId + "'");
            }
            maintainProductStock(userId, profileId, invoiceNumber, model.getProductId(), model.getQuantity(), stockType);
        }

    }

    private void delectStockBy(String userId, String profileId, String invoiceNumber, String stockType) {
        Cursor cursor = db.rawQuery("SELECT * FROM productStock WHERE userId='" + userId + "' AND profileId='" + profileId + "' AND invoiceNumber='" + invoiceNumber + "' AND stockType='" + stockType + "'", null);
        if (cursor.moveToFirst()) {
            // Deleting record if found
            db.execSQL("DELETE FROM productStock WHERE userId='" + userId + "'AND profileId='" + profileId + "'AND invoiceNumber='" + invoiceNumber + "'AND stockType='" + stockType + "'");

        } else {
            //record not found
        }
    }

    public Model copyPurchases(Model result, String invoiceId, String userId, String profileId, String invoiceNumber, String invoiceDate, String vendorId, String vendorName, String amount, String placeOfPurchase, String totalPrice, String selectedPlaceId, ArrayList<ProductModel> productList, PaymentModel paymentDetail, String stockType) {
        // Searching product
        Cursor c = db.rawQuery("SELECT * FROM purchaseInvoice WHERE userId='" + userId + "' AND profileId='" + profileId + "' AND invoiceNumber='" + invoiceNumber + "'", null);
        if (!c.moveToFirst()) {
            L.m("Record not exist than insert query executed");
            // Inserting record
            db.execSQL(
                    "INSERT INTO purchaseInvoice ('userId','profileId','invoiceNumber','invoiceDate','vendorId','vendorName','amount','placeOfPurchase','totalPrice','selectedPlaceId') VALUES(" +
                            " '" + userId + "','" + profileId + "','" + invoiceNumber + "','" + invoiceDate + "','" + vendorId + "','" + vendorName + "','" + amount + "','" + placeOfPurchase + "','" + totalPrice + "','" + selectedPlaceId + "');");
            setCreditorsInDatabase(userId, profileId, invoiceNumber, paymentDetail);
            for (ProductModel model : productList) {
                db.execSQL(
                        "INSERT INTO pInvProduct ('userId','profileId','invoiceNumber','quantity','productId','productName','codeHSN','barcode','costPrice','salePrice','taxRate','fromDate','toDate') VALUES(" +
                                " '" + userId + "','" + profileId + "','" + invoiceNumber + "','" + model.getQuantity() + "','" + model.getProductId() + "','" + model.getProductName() + "','" + model.getCodeHSN() + "','" + model.getBarcode() + "','" + model.getCostPrice() + "','" + model.getSalePrice() + "','" + model.getTaxRate() + "','" + model.getFromDate() + "','" + model.getToDate() + "');");
                maintainProductStock(userId, profileId, invoiceNumber, model.getProductId(), model.getQuantity(), stockType);
            }
            result.setOutputDB(Const.SUCCESS);
            result.setOutputDBMsg("Invoice created successful");
            return result;
        } else {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("Invoice already exist");
            return result;
        }
    }

    public String deletePurchases(PurchasesModel itemPurchases, String stockType) {
        // Searching product
        Cursor c = db.rawQuery("SELECT * FROM purchaseInvoice WHERE id='" + itemPurchases.getInvoiceId() + "' AND userId='" + itemPurchases.getInvoiceUserId() + "' AND profileId='" + itemPurchases.getInvoiceProfileId() + "' AND invoiceNumber='" + itemPurchases.getInvoiceNumber() + "'", null);
        if (c.moveToFirst()) {
            // Deleting record if found
            db.execSQL("DELETE FROM purchaseInvoice WHERE id='" + itemPurchases.getInvoiceId() + "' AND userId='" + itemPurchases.getInvoiceUserId() + "' AND profileId='" + itemPurchases.getInvoiceProfileId() + "' AND invoiceNumber='" + itemPurchases.getInvoiceNumber() + "'");
            deleteCreditorsInDatabase(itemPurchases.getInvoiceUserId(), itemPurchases.getInvoiceProfileId(), itemPurchases.getInvoiceNumber());
            deleteInvoiceProductList(stockType, itemPurchases.getInvoiceNumber(), itemPurchases.getInvoiceUserId(), itemPurchases.getInvoiceProfileId());
            return "Invoice Deleted";
        } else {
            return "Invalid Invoice Id";
        }
    }

    private void deleteCreditorsInDatabase(String userId, String profileId, String invoiceNo) {
        Cursor c = db.rawQuery("SELECT * FROM creditors WHERE userId='" + userId + "' AND profileId='" + profileId + "' AND invoiceNo='" + invoiceNo + "'", null);
        if (c.moveToFirst()) {
            // Deleting record if found
            db.execSQL("DELETE FROM creditors WHERE userId='" + userId + "' AND profileId='" + profileId + "' AND invoiceNo='" + invoiceNo + "'");
        }
    }

    // sellInvProduct ('userId','profileId','companyId','invoiceNumber'
    private void deleteInvoiceProductList(String stockType, String invoiceNumber, String userId, String profileId) {
        Cursor c = db.rawQuery("SELECT * FROM pInvProduct WHERE userId='" + userId + "' AND profileId='" + profileId + "' AND invoiceNumber='" + invoiceNumber + "'", null);
        if (c.moveToFirst()) {
            // Deleting record if found
            db.execSQL("DELETE FROM pInvProduct WHERE userId='" + userId + "'AND profileId='" + profileId + "'AND invoiceNumber='" + invoiceNumber + "'");

        } else {
            //record not found
        }
        delectStockBy(userId, profileId, invoiceNumber, stockType);
    }

    public ProfileModel getAllProfileList(ProfileModel result, String userId, String profileId) {
        Cursor c = db.rawQuery("SELECT * FROM profile WHERE userId='" + userId + "' ORDER BY id DESC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("No record found");
            return result;
        }
        result.setOutputDB(Const.SUCCESS);
        result.setList(new ArrayList<ProfileModel>());
        while (c.moveToNext()) {
            ProfileModel model = new ProfileModel();
            model.setProfileId(c.getString(0));
            model.setProfileUserId(c.getString(1));
            model.setProfileName(c.getString(2));
            model.setEmail(c.getString(3));
            model.setMobile(c.getString(4));
            model.setPan(c.getString(5));
            model.setImagePath(c.getString(6));
            model.setImageUrl(c.getString(7));
            if (profileId.equalsIgnoreCase(c.getString(0))) {
                model.setSelected(true);
            } else {
                model.setSelected(false);
            }
            result.getList().add(model);
        }
        return result;
    }

    public ProfileModel addCompanyProfile(ProfileModel result, String userId, String name, String email, String mobile, String pan, String imagePath, String imageUrl) {
        // Searching product
        Cursor c = db.rawQuery("SELECT * FROM profile WHERE userId='" + userId + "' AND name='" + name + "'", null);
        if (!c.moveToFirst()) {
            L.m("Record not exist than insert query executed");
            // Inserting record
            db.execSQL(
                    "INSERT INTO profile ('userId','name','email','mobile','pan','imagePath','imageUrl') VALUES(" +
                            " '" + userId + "','" + name + "','" + email + "','" + mobile + "','" + pan + "','" + imagePath + "','" + imageUrl + "');");
            result.setProfileId(getProfileByUserIdAndName(userId, name));
            result.setOutputDB(Const.SUCCESS);
            result.setOutputDBMsg("Profile created");
            return result;
        } else {

            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("Company name already exist");
            return result;
        }
    }

    public String getProfileByUserIdAndName(String userId, String name) {
        String response = "";
        //        Cursor c = db.rawQuery("SELECT * FROM vendor ORDER BY id DESC", null);
        Cursor c = db.rawQuery("SELECT id FROM profile WHERE userId='" + userId + "' AND name='" + name + "' ORDER BY id ASC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            return "";
        }
        while (c.moveToNext()) {
            response = c.getString(0);
            L.m(response);
        }
        return response;
    }

    public String[] getProfileByUserId(String userId) {
        String[] response = new String[4];
        //        Cursor c = db.rawQuery("SELECT * FROM vendor ORDER BY id DESC", null);
        Cursor c = db.rawQuery("SELECT * FROM profile WHERE userId='" + userId + "' ORDER BY id ASC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            return null;
        }
        while (c.moveToNext()) {
            response[0] = c.getString(0);//id
            response[1] = c.getString(2);//name
            response[2] = c.getString(6);//imagePath
        }
        return response;
    }

    public String[] setInitialUserProfile(String userId) {
        return getProfileByUserId(userId);
    }

    public CompanyModel updateCompanyDetail(CompanyModel result, String userId, String profileId, String companyName, String telephone, String email, String gstin, String state, String stateCode, String address, String dlNumber, String tAndC, String accName, String ifscCode, String accNumber, String branchAddress) {
        db.execSQL(
                "INSERT INTO companyDetail ('userId','profileId','companyName','telephone','email','gstin','state','stateCode','address','dlNumber','tAndC','accName','ifscCode','accNumber','branchAddress') VALUES(" +
                        " '" + userId + "','" + profileId + "','" + companyName + "','" + telephone + "','" + email + "','" + gstin + "','" + state + "','" + stateCode + "','" + address + "','" + dlNumber + "','" + tAndC + "','" + accName + "','" + ifscCode + "','" + accNumber + "','" + branchAddress + "');");

        String query = "SELECT id from companyDetail order by id DESC limit 1";
        Cursor c = db.rawQuery(query, null);
        String id = null;
        if (c != null && c.moveToFirst()) {
            id = c.getString(0); //The 0 is the column index, we only have 1 column, so the index is 0
        }

        result.setCompanyId(id);
        result.setOutputDB(Const.SUCCESS);
        result.setOutputDBMsg("Company detail updated");
        return result;
    }

    public CompanyModel getCompanyDetail(String userId, String profileId, String companyId) {
        CompanyModel result = new CompanyModel();
        Cursor c = db.rawQuery("SELECT * FROM companyDetail WHERE id='" + companyId + "' AND userId='" + userId + "' AND profileId='" + profileId + "' ORDER BY id DESC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("No record found");
            return result;
        }
        result.setOutputDB(Const.SUCCESS);
        if (c != null && c.moveToFirst()) {
            result.setCompanyId(c.getString(0)); //The 0 is the column index, we only have 1 column, so the index is 0
            result.setCompanyUserId(c.getString(1));
//            result.setCompanyProfileId(c.getString(2));
            result.setCompanyName(c.getString(3));
            result.setTelephone(c.getString(4));
            result.setEmail(c.getString(5));
            result.setGstin(c.getString(6));
            result.setState(c.getString(7));
            result.setStateCode(c.getString(8));
            result.setAddress(c.getString(9));
            result.setDlNumber(c.getString(10));
            result.settAndC(c.getString(11));
            result.setAccName(c.getString(12));
            result.setIfscCode(c.getString(13));
            result.setAccNumber(c.getString(14));
            result.setBranchAddress(c.getString(15));
        }

        return result;
    }

    public Model addInvoice(Model result, String userId, String profileId, String companyId, String invoiceNumber, String invoiceDate, String customerId, String customerName, String freightAmount, String transpoartMode, String transpoartModeId, String vehicleNumber, String dateOfSupply, String placeofSupply, String placeofSupplyId, String shipName, String shipGSTIN, String shipAddress, String totalPrice, ArrayList<ProductModel> productList, Boolean isIGSTApplicable, PaymentModel paymentDetail, String stockType) {
        // Searching product
        float maxTaxRate = 0;
        for (ProductModel model : productList) {
            float taxRate = convertToFloat(model.getTaxRate());
            if (maxTaxRate < taxRate) {
                maxTaxRate = taxRate;
            }
        }
        Cursor c = db.rawQuery("SELECT * FROM sellInvoice WHERE userId='" + userId + "' AND profileId='" + profileId + "' AND companyId='" + companyId + "' AND invoiceNumber='" + invoiceNumber + "'", null);
        if (!c.moveToFirst()) {
            String allTax[] = calTax(freightAmount, String.valueOf(maxTaxRate), isIGSTApplicable);
            String freightTaxRate = String.valueOf(maxTaxRate);
            String freightTotalAmt = allTax[0];
            String cgstTax = allTax[1];
            String cgstVal = allTax[2];
            String sgstTax = allTax[3];
            String sgstVal = allTax[4];
            String igstTax = allTax[5];
            String igstVal = allTax[6];
            // Inserting record
            db.execSQL(
                    "INSERT INTO sellInvoice ('userId','profileId','companyId','invoiceNumber','invoiceDate','customerId','customerName','freightAmount','transpoartMode','transpoartModeId','vehicleNumber','dateOfSupply','placeofSupply','placeofSupplyId','shipName','shipGSTIN','shipAddress','totalPrice',freightTaxRate,freightTotalAmount,cgstTax,cgstVal,sgstTax,sgstVal,igstTax,igstVal) VALUES(" +
                            " '" + userId + "','" + profileId + "','" + companyId + "','" + invoiceNumber + "','" + invoiceDate + "','" + customerId + "','" + customerName + "','" + freightAmount + "','" + transpoartMode + "','" + transpoartModeId + "','" + vehicleNumber + "','" + dateOfSupply + "','" + placeofSupply + "','" + placeofSupplyId + "','" + shipName + "','" + shipGSTIN + "','" + shipAddress + "','" + totalPrice + "','" + freightTaxRate + "','" + freightTotalAmt + "','" + cgstTax + "','" + cgstVal + "','" + sgstTax + "','" + sgstVal + "','" + igstTax + "','" + igstVal + "');");
            setDebitorsInDatabase(userId, profileId, invoiceNumber, paymentDetail);
            for (ProductModel model : productList) {
                float totalProdPrice = convertToFloat(model.getSalePrice()) * convertToFloat(model.getQuantity());
                String allProdTax[] = calTax(String.valueOf(totalProdPrice), model.getTaxRate(), isIGSTApplicable);
                String totalTaxAmount = allProdTax[0];
                String prodCGSTTax = allProdTax[1];
                String prodCGSTVal = allProdTax[2];
                String prodSGSTTax = allProdTax[3];
                String prodSGSTVal = allProdTax[4];
                String prodIGSTTax = allProdTax[5];
                String prodIGSTVal = allProdTax[6];
                db.execSQL(
                        "INSERT INTO sellInvProduct ('userId','profileId','companyId','invoiceNumber','quantity','productId','productName','codeHSN','barcode','costPrice','salePrice','taxRate','fromDate','toDate',totalTaxAmount,cgstTax,cgstVal,sgstTax,sgstVal,igstTax,igstVal) VALUES(" +
                                "'" + userId + "','" + profileId + "','" + companyId + "','" + invoiceNumber + "','" + model.getQuantity() + "','" + model.getProductId() + "','" + model.getProductName() + "','" + model.getCodeHSN() + "','" + model.getBarcode() + "','" + model.getCostPrice() + "','" + model.getSalePrice() + "','" + model.getTaxRate() + "','" + model.getFromDate() + "','" + model.getToDate() + "','" + totalTaxAmount + "','" + prodCGSTTax + "','" + prodCGSTVal + "','" + prodSGSTTax + "','" + prodSGSTVal + "','" + prodIGSTTax + "','" + prodIGSTVal + "');");
                maintainProductStock(userId, profileId, invoiceNumber, model.getProductId(), ("-" + model.getQuantity()), stockType);
            }
            result.setOutputDB(Const.SUCCESS);
            result.setOutputDBMsg("Invoice created successful");
            return result;
        } else {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("Invoice already exist");
            return result;
        }
    }

    //"CREATE TABLE IF NOT EXISTS debtors(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR , profileId VARCHAR ,invoiceNo VARCHAR, customerId VARCHAR, customerName VARCHAR, paidAmount VARCHAR, payableAmount VARCHAR, balanceAmount VARCHAR, paymentMode VARCHAR, BankName VARCHAR, chequeNo VARCHAR, AccName VARCHAR, IFSC VARCHAR, AccNumber VARCHAR, BranchAddress VARCHAR);");

    private void setDebitorsInDatabase(String userId, String profileId, String invoiceNo, PaymentModel pay) {
        Cursor c = db.rawQuery("SELECT * FROM debtors WHERE userId='" + userId + "' AND profileId='" + profileId + "' AND invoiceNo='" + invoiceNo + "'", null);
        if (!c.moveToFirst()) {
            // Inserting record
            db.execSQL(
                    "INSERT INTO debtors ('userId','profileId','invoiceNo','customerId','customerName','paidAmount','payableAmount','balanceAmount','paymentMode','BankName','chequeNo','AccName','IFSC','AccNumber','BranchAddress','openingBalance','paymentReceived') VALUES(" +
                            " '" + userId + "','" + profileId + "','" + pay.getInvoiceNo() + "','" + pay.getCustomerId() + "','" + pay.getCustomerName() + "','" + pay.getPaidAmount() + "','" + pay.getPayableAmount() + "','" + pay.getBalanceAmount() + "','" + pay.getPaymentMode() + "','" + pay.getBankName() + "','" + pay.getChequeNo() + "','" + pay.getAccName() + "','" + pay.getIFSC() + "','" + pay.getAccNumber() + "','" + pay.getBranchAddress() + "','" + pay.getOpeningBalance() + "','" + pay.getPaymentReceived() + "');");
        }
    }

    //"CREATE TABLE IF NOT EXISTS debtors(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR , profileId VARCHAR ,invoiceNo VARCHAR, customerId VARCHAR, customerName VARCHAR, paidAmount VARCHAR, payableAmount VARCHAR, balanceAmount VARCHAR, paymentMode VARCHAR, BankName VARCHAR, chequeNo VARCHAR, AccName VARCHAR, IFSC VARCHAR, AccNumber VARCHAR, BranchAddress VARCHAR);");

    public Model setDebitorsByTrans(Model result, String userId, String profileId, PaymentModel pay) {
        db.execSQL(
                "INSERT INTO debtors ('userId','profileId','invoiceNo','customerId','customerName','paidAmount','payableAmount','balanceAmount','paymentMode','BankName','chequeNo','AccName','IFSC','AccNumber','BranchAddress','openingBalance','paymentReceived') VALUES(" +
                        " '" + userId + "','" + profileId + "','" + pay.getInvoiceNo() + "','" + pay.getCustomerId() + "','" + pay.getCustomerName() + "','" + pay.getPaidAmount() + "','" + pay.getPayableAmount() + "','" + pay.getBalanceAmount() + "','" + pay.getPaymentMode() + "','" + pay.getBankName() + "','" + pay.getChequeNo() + "','" + pay.getAccName() + "','" + pay.getIFSC() + "','" + pay.getAccNumber() + "','" + pay.getBranchAddress() + "','" + pay.getOpeningBalance() + "','" + pay.getPaymentReceived() + "');");

        result.setOutputDB(Const.SUCCESS);
        result.setOutputDBMsg("Transaction successful");
        return result;
    }

    private float convertToFloat(String salePrice) {
        try {
            return Float.parseFloat(salePrice);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    private int convertToInt(String salePrice) {
        try {
            return Integer.parseInt(salePrice);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    private String[] calTax(String freightAmount, String taxRate, Boolean isIGSTApplicable) {
        float maxTaxRate = convertToFloat(taxRate);
        float totalAmt = convertToFloat(freightAmount);
        String[] taxCal = new String[7];
        if (isIGSTApplicable) {
            taxCal[0] = String.valueOf((totalAmt * maxTaxRate) / 100);
            taxCal[1] = "0";
            taxCal[2] = "0";
            taxCal[3] = "0";
            taxCal[4] = "0";
            taxCal[5] = String.valueOf(maxTaxRate);
            taxCal[6] = String.valueOf((totalAmt * maxTaxRate) / 100);
        } else {
            taxCal[0] = String.valueOf((totalAmt * maxTaxRate) / 100);
            maxTaxRate = maxTaxRate / 2;
            taxCal[1] = String.valueOf(maxTaxRate);
            taxCal[2] = String.valueOf((totalAmt * maxTaxRate) / 100);
            taxCal[3] = String.valueOf(maxTaxRate);
            taxCal[4] = String.valueOf((totalAmt * maxTaxRate) / 100);
            taxCal[5] = "0";
            taxCal[6] = "0";
        }
        return taxCal;
    }

    public Model editInvoice(Model result, String invoiceId, ArrayList<String> deletedProduct, String userId, String profileId, String companyId, String invoiceNumber, String invoiceDate, String customerId, String customerName, String freightAmount, String transpoartMode, String transpoartModeId, String vehicleNumber, String dateOfSupply, String placeofSupply, String placeofSupplyId, String shipName, String shipGSTIN, String shipAddress, String totalPrice, ArrayList<ProductModel> productList, Boolean isIGSTApplicable, PaymentModel paymentDetail, String stockType) {
        // Searching product
        Cursor c = db.rawQuery("SELECT * FROM sellInvoice WHERE id='" + invoiceId + "' AND userId='" + userId + "' AND profileId='" + profileId + "' AND companyId='" + companyId + "' AND invoiceNumber='" + invoiceNumber + "'", null);
        if (!c.moveToFirst()) {
            // Inserting record
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("Invoice not exist");
            return result;
        } else {
            //Modifying record if found
            updatDebtorsInDatabase(userId, profileId, invoiceNumber, paymentDetail);
            db.execSQL("UPDATE sellInvoice SET invoiceDate='" + invoiceDate + "',customerId='" + customerId + "',customerName='" + customerName + "',freightAmount='" + freightAmount + "',transpoartMode='" + transpoartMode + "',transpoartModeId='" + transpoartModeId + "',vehicleNumber='" + vehicleNumber + "',dateOfSupply='" + dateOfSupply + "',placeofSupply='" + placeofSupply + "',placeofSupplyId='" + placeofSupplyId + "',shipName='" + shipName + "',shipGSTIN='" + shipGSTIN + "',shipAddress='" + shipAddress + "',totalPrice='" + totalPrice +
                    "' WHERE id='" + invoiceId + "' AND userId='" + userId + "' AND profileId='" + profileId + "' AND companyId='" + companyId + "'");
            float maxTaxRate = editSellInvoiceProductList(userId, profileId, companyId, invoiceNumber, productList, deletedProduct, isIGSTApplicable, stockType);
            updateFreightDetailOfSellInvoiceProductList(maxTaxRate, freightAmount, invoiceId, userId, profileId, companyId, isIGSTApplicable);
            result.setOutputDB(Const.SUCCESS);
            result.setOutputDBMsg("Invoice edited successful");
            return result;
        }
    }

    //"CREATE TABLE IF NOT EXISTS debtors(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR , profileId VARCHAR ,invoiceNo VARCHAR, customerId VARCHAR, customerName VARCHAR, paidAmount VARCHAR, payableAmount VARCHAR, balanceAmount VARCHAR, paymentMode VARCHAR, BankName VARCHAR, chequeNo VARCHAR, AccName VARCHAR, IFSC VARCHAR, AccNumber VARCHAR, BranchAddress VARCHAR);");
    private void updatDebtorsInDatabase(String userId, String profileId, String invoiceNo, PaymentModel pay) {
        Cursor c = db.rawQuery("SELECT * FROM debtors WHERE userId='" + userId + "' AND profileId='" + profileId + "' AND invoiceNo='" + invoiceNo + "'", null);
        if (c.moveToFirst()) {
            //Modifying record if found
            db.execSQL("UPDATE debtors SET customerId='" + pay.getCustomerId() + "',customerName='" + pay.getCustomerName() + "',paidAmount='" + pay.getPaidAmount() + "',payableAmount='" + pay.getPayableAmount() + "',balanceAmount='" + pay.getBalanceAmount() + "',paymentMode='" + pay.getPaymentMode() + "',BankName='" + pay.getBankName() + "',chequeNo='" + pay.getChequeNo() + "',AccName='" + pay.getAccName() + "',IFSC='" + pay.getIFSC() + "',AccNumber='" + pay.getAccNumber() + "',BranchAddress='" + pay.getBranchAddress() + "',openingBalance='" + pay.getOpeningBalance() + "',paymentReceived='" + pay.getPaymentReceived() +
                    "' WHERE userId='" + userId + "' AND profileId='" + profileId + "' AND invoiceNo='" + invoiceNo + "'");
        }
    }

    private void updateFreightDetailOfSellInvoiceProductList(float maxTaxRate, String freightAmount, String invoiceId, String userId, String profileId, String companyId, Boolean isIGSTApplicable) {
        String allTax[] = calTax(freightAmount, String.valueOf(maxTaxRate), isIGSTApplicable);
        String freightTaxRate = String.valueOf(maxTaxRate);
        String freightTotalAmt = allTax[0];
        String cgstTax = allTax[1];
        String cgstVal = allTax[2];
        String sgstTax = allTax[3];
        String sgstVal = allTax[4];
        String igstTax = allTax[5];
        String igstVal = allTax[6];
        //Modifying record if found
        db.execSQL("UPDATE sellInvoice SET freightTaxRate='" + freightTaxRate + "',freightTotalAmount='" + freightTotalAmt + "',cgstTax='" + cgstTax + "',cgstVal='" + cgstVal + "',sgstTax='" + sgstTax + "',sgstVal='" + sgstVal + "',igstTax='" + igstTax + "',igstVal='" + igstVal +
                "' WHERE id='" + invoiceId + "' AND userId='" + userId + "' AND profileId='" + profileId + "' AND companyId='" + companyId + "'");

    }

    private float editSellInvoiceProductList(String userId, String profileId, String companyId, String invoiceNumber, ArrayList<ProductModel> productList, ArrayList<String> deletedProduct, Boolean isIGSTApplicable, String stockType) {
        for (String data : deletedProduct) {
            // Searching product
            Cursor c = db.rawQuery("SELECT * FROM sellInvProduct WHERE productId='" + data + "' AND userId='" + userId + "' AND profileId='" + profileId + "' AND companyId='" + companyId + "' AND invoiceNumber='" + invoiceNumber + "'", null);
            if (c.moveToFirst()) {
                // Deleting record if found
                db.execSQL("DELETE FROM sellInvProduct WHERE productId='" + data + "'AND userId='" + userId + "'AND profileId='" + profileId + "'AND companyId='" + companyId + "'AND invoiceNumber='" + invoiceNumber + "'");
            } else {
                //record not found
            }
            delectStockBy(userId, profileId, invoiceNumber, stockType);
        }
        float maxTaxRate = 0;
        for (ProductModel model : productList) {
            float taxRate = convertToFloat(model.getTaxRate());
            if (maxTaxRate < taxRate) {
                maxTaxRate = taxRate;
            }
            float totalProdPrice = convertToFloat(model.getSalePrice()) * convertToFloat(model.getQuantity());
            String allProdTax[] = calTax(String.valueOf(totalProdPrice), model.getTaxRate(), isIGSTApplicable);
            String totalTaxAmount = allProdTax[0];
            String prodCGSTTax = allProdTax[1];
            String prodCGSTVal = allProdTax[2];
            String prodSGSTTax = allProdTax[3];
            String prodSGSTVal = allProdTax[4];
            String prodIGSTTax = allProdTax[5];
            String prodIGSTVal = allProdTax[6];
            // Searching product
            Cursor c = db.rawQuery("SELECT * FROM sellInvProduct WHERE productId='" + model.getProductId() + "' AND userId='" + userId + "' AND profileId='" + profileId + "' AND companyId='" + companyId + "' AND invoiceNumber='" + invoiceNumber + "'", null);
            if (!c.moveToFirst()) {
                //record not found
                db.execSQL(
                        "INSERT INTO sellInvProduct ('userId','profileId','companyId','invoiceNumber','quantity','productId','productName','codeHSN','barcode','costPrice','salePrice','taxRate','fromDate','toDate',totalTaxAmount,cgstTax,cgstVal,sgstTax,sgstVal,igstTax,igstVal) VALUES(" +
                                "'" + userId + "','" + profileId + "','" + companyId + "','" + invoiceNumber + "','" + model.getQuantity() + "','" + model.getProductId() + "','" + model.getProductName() + "','" + model.getCodeHSN() + "','" + model.getBarcode() + "','" + model.getCostPrice() + "','" + model.getSalePrice() + "','" + model.getTaxRate() + "','" + model.getFromDate() + "','" + model.getToDate() + "','" + totalTaxAmount + "','" + prodCGSTTax + "','" + prodCGSTVal + "','" + prodSGSTTax + "','" + prodSGSTVal + "','" + prodIGSTTax + "','" + prodIGSTVal + "');");
            } else {
                //Modifying record if found
                db.execSQL("UPDATE sellInvProduct SET quantity='" + model.getQuantity() + "',productName='" + model.getProductName() + "',codeHSN='" + model.getCodeHSN() + "',barcode='" + model.getBarcode() + "',costPrice='" + model.getCostPrice() + "',salePrice='" + model.getSalePrice() + "',taxRate='" + model.getTaxRate() + "',fromDate='" + model.getFromDate() + "',toDate='" + model.getToDate() + "',totalTaxAmount='" + totalTaxAmount + "',cgstTax='" + prodCGSTTax + "',cgstVal='" + prodCGSTVal + "',sgstTax='" + prodSGSTTax + "',sgstVal='" + prodSGSTVal + "',igstTax='" + prodIGSTTax + "',igstVal='" + prodIGSTVal +
                        "' WHERE productId='" + model.getProductId() + "' AND invoiceNumber='" + invoiceNumber + "' AND userId='" + userId + "' AND profileId='" + profileId + "' AND companyId='" + companyId + "'");
            }
            maintainProductStock(userId, profileId, invoiceNumber, model.getProductId(), ("-" + model.getQuantity()), stockType);
        }
        return maxTaxRate;
    }

    public Model copyInvoice(Model result, String userId, String profileId, String companyId, String invoiceNumber, String invoiceDate, String customerId, String customerName, String freightAmount, String transpoartMode, String transpoartModeId, String vehicleNumber, String dateOfSupply, String placeofSupply, String placeofSupplyId, String shipName, String shipGSTIN, String shipAddress, String totalPrice, ArrayList<ProductModel> productList, Boolean isIGSTApplicable, PaymentModel paymentDetail, String stockType) {
        // Searching product
        float maxTaxRate = 0;
        for (ProductModel model : productList) {
            float taxRate = convertToFloat(model.getTaxRate());
            if (maxTaxRate < taxRate) {
                maxTaxRate = taxRate;
            }
        }
        Cursor c = db.rawQuery("SELECT * FROM sellInvoice WHERE userId='" + userId + "' AND profileId='" + profileId + "' AND companyId='" + companyId + "' AND invoiceNumber='" + invoiceNumber + "'", null);
        if (!c.moveToFirst()) {
            String allTax[] = calTax(freightAmount, String.valueOf(maxTaxRate), isIGSTApplicable);
            String freightTaxRate = String.valueOf(maxTaxRate);
            String freightTotalAmt = allTax[0];
            String cgstTax = allTax[1];
            String cgstVal = allTax[2];
            String sgstTax = allTax[3];
            String sgstVal = allTax[4];
            String igstTax = allTax[5];
            String igstVal = allTax[6];
            // Inserting record
            setDebitorsInDatabase(userId, profileId, invoiceNumber, paymentDetail);
            db.execSQL(
                    "INSERT INTO sellInvoice ('userId','profileId','companyId','invoiceNumber','invoiceDate','customerId','customerName','freightAmount','transpoartMode','transpoartModeId','vehicleNumber','dateOfSupply','placeofSupply','placeofSupplyId','shipName','shipGSTIN','shipAddress','totalPrice',freightTaxRate,freightTotalAmount,cgstTax,cgstVal,sgstTax,sgstVal,igstTax,igstVal) VALUES(" +
                            "'" + userId + "','" + profileId + "','" + companyId + "','" + invoiceNumber + "','" + invoiceDate + "','" + customerId + "','" + customerName + "','" + freightAmount + "','" + transpoartMode + "','" + transpoartModeId + "','" + vehicleNumber + "','" + dateOfSupply + "','" + placeofSupply + "','" + placeofSupplyId + "','" + shipName + "','" + shipGSTIN + "','" + shipAddress + "','" + totalPrice + "','" + freightTaxRate + "','" + freightTotalAmt + "','" + cgstTax + "','" + cgstVal + "','" + sgstTax + "','" + sgstVal + "','" + igstTax + "','" + igstVal + "');");
            for (ProductModel model : productList) {
                float totalProdPrice = convertToFloat(model.getSalePrice()) * convertToFloat(model.getQuantity());
                String allProdTax[] = calTax(String.valueOf(totalProdPrice), model.getTaxRate(), isIGSTApplicable);
                String totalTaxAmount = allProdTax[0];
                String prodCGSTTax = allProdTax[1];
                String prodCGSTVal = allProdTax[2];
                String prodSGSTTax = allProdTax[3];
                String prodSGSTVal = allProdTax[4];
                String prodIGSTTax = allProdTax[5];
                String prodIGSTVal = allProdTax[6];
                db.execSQL(
                        "INSERT INTO sellInvProduct ('userId','profileId','companyId','invoiceNumber','quantity','productId','productName','codeHSN','barcode','costPrice','salePrice','taxRate','fromDate','toDate',totalTaxAmount,cgstTax,cgstVal,sgstTax,sgstVal,igstTax,igstVal) VALUES(" +
                                "'" + userId + "','" + profileId + "','" + companyId + "','" + invoiceNumber + "','" + model.getQuantity() + "','" + model.getProductId() + "','" + model.getProductName() + "','" + model.getCodeHSN() + "','" + model.getBarcode() + "','" + model.getCostPrice() + "','" + model.getSalePrice() + "','" + model.getTaxRate() + "','" + model.getFromDate() + "','" + model.getToDate() + "','" + totalTaxAmount + "','" + prodCGSTTax + "','" + prodCGSTVal + "','" + prodSGSTTax + "','" + prodSGSTVal + "','" + prodIGSTTax + "','" + prodIGSTVal + "');");
                maintainProductStock(userId, profileId, invoiceNumber, model.getProductId(), ("-" + model.getQuantity()), stockType);
            }
            result.setOutputDB(Const.SUCCESS);
            result.setOutputDBMsg("Invoice created successful");
            return result;
        } else {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("Invoice already exist");
            return result;
        }
    }

    public String getSellInvoiceNumber(String userId, String profileId) {
        String response = "";
        //        Cursor c = db.rawQuery("SELECT * FROM vendor ORDER BY id DESC", null);
        Cursor c = db.rawQuery("SELECT invoiceNumber FROM sellInvoice WHERE userId='" + userId + "' AND profileId='" + profileId + "' ORDER BY invoiceNumber ASC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            return "";
        }
        while (c.moveToNext()) {
            response = c.getString(0);
            L.m(response);
        }
        return response;
    }

    public String deleteInvoices(InvoiceModel itemInvoices, String stockType) {
        // Searching product
        Cursor c = db.rawQuery("SELECT * FROM sellInvoice WHERE userId='" + itemInvoices.getInvoiceUserId() + "' AND profileId='" + itemInvoices.getInvoiceProfileId() + "' AND companyId='" + itemInvoices.getInvoiceCompanyId() + "' AND invoiceNumber='" + itemInvoices.getInvoiceNumber() + "'", null);
        if (c.moveToFirst()) {
            // Deleting record if found
            db.execSQL("DELETE FROM sellInvoice WHERE userId='" + itemInvoices.getInvoiceUserId() + "' AND profileId='" + itemInvoices.getInvoiceProfileId() + "' AND companyId='" + itemInvoices.getInvoiceCompanyId() + "' AND invoiceNumber='" + itemInvoices.getInvoiceNumber() + "'");
            deleteDebtorsInDatabase(itemInvoices.getInvoiceUserId(), itemInvoices.getInvoiceProfileId(), itemInvoices.getInvoiceNumber());
            deleteSellInvoiceProductList(itemInvoices.getInvoiceNumber(), itemInvoices.getInvoiceUserId(), itemInvoices.getInvoiceProfileId(), stockType);
            return "Invoice Deleted";
        } else {
            return "Invalid Invoice Id";
        }
    }

    //"CREATE TABLE IF NOT EXISTS debtors(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR , profileId VARCHAR ,invoiceNo VARCHAR, customerId VARCHAR, customerName VARCHAR, paidAmount VARCHAR, payableAmount VARCHAR, balanceAmount VARCHAR, paymentMode VARCHAR, BankName VARCHAR, chequeNo VARCHAR, AccName VARCHAR, IFSC VARCHAR, AccNumber VARCHAR, BranchAddress VARCHAR);");
    private void deleteDebtorsInDatabase(String userId, String profileId, String invoiceNo) {
        Cursor c = db.rawQuery("SELECT * FROM debtors WHERE userId='" + userId + "' AND profileId='" + profileId + "' AND invoiceNo='" + invoiceNo + "'", null);
        if (c.moveToFirst()) {
            // Deleting record if found
            db.execSQL("DELETE FROM debtors WHERE userId='" + userId + "' AND profileId='" + profileId + "' AND invoiceNo='" + invoiceNo + "'");
        }
    }

    private void deleteSellInvoiceProductList(String invoiceNumber, String userId, String profileId, String stockType) {
        // sellInvProduct ('userId','profileId','companyId','invoiceNumber'
        Cursor c = db.rawQuery("SELECT * FROM sellInvProduct WHERE userId='" + userId + "' AND profileId='" + profileId + "' AND invoiceNumber='" + invoiceNumber + "'", null);
        if (c.moveToFirst()) {
            // Deleting record if found
            db.execSQL("DELETE FROM sellInvProduct WHERE userId='" + userId + "'AND profileId='" + profileId + "'AND invoiceNumber='" + invoiceNumber + "'");

        } else {
            //record not found
            L.m("record not found");
        }
        delectStockBy(userId, profileId, invoiceNumber, stockType);
    }

    public InvoiceModel getInvoicesList(InvoiceModel result, String userId, String profileId) {
        //        Cursor c = db.rawQuery("SELECT * FROM vendor ORDER BY id DESC", null);
        Cursor c = db.rawQuery("SELECT * FROM sellInvoice WHERE userId='" + userId + "' AND profileId='" + profileId + "' ORDER BY id DESC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("No record found");
            return result;
        }
        result.setOutputDB(Const.SUCCESS);
        result.setList(new ArrayList<InvoiceModel>());


        while (c.moveToNext()) {
            InvoiceModel model = new InvoiceModel();
            model.setInvoiceUserId(c.getString(1));
            model.setInvoiceProfileId(c.getString(2));
            model.setInvoiceCompanyId(c.getString(3));
            model.setInvoiceNumber(c.getString(4));
            model.setInvoiceDate(c.getString(5));
            model.setCustomerId(c.getString(6));
            model.setCustomerName(c.getString(7));
            model.setFreightAmount(c.getString(8));
            model.setTranspoartMode(c.getString(9));
            model.setTranspoartModeId(c.getString(10));
            model.setVehicleNumber(c.getString(11));
            model.setDateOfSupply(c.getString(12));
            model.setPlaceofSupply(c.getString(13));
            model.setPlaceofSupplyId(c.getString(14));
            model.setShipName(c.getString(15));
            model.setShipGSTIN(c.getString(16));
            model.setShipAddress(c.getString(17));
            model.setTotalAmount(c.getString(18));
            model.setFreightTaxRate(c.getString(19));
            model.setFreightTotalAmount(c.getString(20));
            model.setCgstTax(c.getString(21));
            model.setCgstVal(c.getString(22));
            model.setSgstTax(c.getString(23));
            model.setSgstVal(c.getString(24));
            model.setIgstTax(c.getString(25));
            model.setIgstVal(c.getString(26));
            model.setProductList(getSellInvoiceProductList(userId, profileId, c.getString(3), c.getString(4)));
            result.getList().add(model);
        }
        return result;
    }

    private ArrayList<ProductModel> getSellInvoiceProductList(String userId, String profileId, String companyId, String invoiceNumber) {
        Cursor c = db.rawQuery("SELECT * FROM sellInvProduct WHERE userId='" + userId + "' AND profileId='" + profileId + "' AND companyId='" + companyId + "' AND invoiceNumber='" + invoiceNumber + "'", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            return null;
        }
        ArrayList<ProductModel> prodList = new ArrayList<>();
        while (c.moveToNext()) {
            ProductModel model = new ProductModel();
            model.setPurchasesProdId(c.getString(0));
            model.setProductUserId(c.getString(1));
            model.setProductProfileId(c.getString(2));
            model.setProductCompanyId(c.getString(3));
            model.setInvNumber(c.getString(4));
            model.setQuantity(c.getString(5));
            model.setProductId(c.getString(6));
            model.setProductName(c.getString(7));
            model.setCodeHSN(c.getString(8));
            model.setBarcode(c.getString(9));
            model.setCostPrice(c.getString(10));
            model.setSalePrice(c.getString(11));
            model.setTaxRate(c.getString(12));
            model.setFromDate(c.getString(13));
            model.setToDate(c.getString(14));
            model.setTotalTaxAmount(c.getString(15));
            model.setCgstTax(c.getString(16));
            model.setCgstVal(c.getString(17));
            model.setSgstTax(c.getString(18));
            model.setSgstVal(c.getString(19));
            model.setIgstTax(c.getString(20));
            model.setIgstVal(c.getString(21));
            prodList.add(model);
        }
        return prodList;
    }

    public CustomerModel getCustomerIdByCustomerAndCompanyName(String userId, String personName, String companyName) {
        CustomerModel model = new CustomerModel();
        Cursor c = db.rawQuery("SELECT * FROM customer WHERE userId='" + userId + "' AND personName='" + personName + "' AND companyName='" + companyName + "' ORDER BY id DESC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            model.setOutputDB(Const.FAILURE);
            model.setOutputDBMsg("Customer not found");
            return model;
        }
        model.setOutputDB(Const.SUCCESS);

        //total 20 item with primaryKey in table
        while (c.moveToNext()) {
            model.setCustometId(c.getString(0));
            model.setCustometUserId(c.getString(1));
            model.setPersonName(c.getString(2));
            model.setCompanyName(c.getString(3));
            model.setContactEmail(c.getString(4));
            model.setWorkPhone(c.getString(5));
            model.setMobile(c.getString(6));
            model.setContactDisplayName(c.getString(7));
            model.setWebsite(c.getString(8));
            model.setGSTIN(c.getString(9));
            model.setDLNumber(c.getString(10));
            model.setState(c.getString(11));
            model.setCity(c.getString(12));
            model.setCountry(c.getString(13));
            model.setZip(c.getString(14));
            model.setAddress(c.getString(15));
            model.setFax(c.getString(16));
            model.setCurrency(c.getString(17));
            model.setPaymentTerms(c.getString(18));
            model.setRemarks(c.getString(19));
        }
        return model;
    }

    public CustomerModel getCustomerByCustomerId(String userId, String customerId) {
        CustomerModel model = new CustomerModel();
        Cursor c = db.rawQuery("SELECT * FROM customer WHERE id='" + customerId + "' AND userId='" + userId + "' ORDER BY id DESC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            model.setOutputDB(Const.FAILURE);
            model.setOutputDBMsg("Customer not found");
            return model;
        }
        model.setOutputDB(Const.SUCCESS);

        //total 20 item with primaryKey in table
        while (c.moveToNext()) {
            model.setCustometId(c.getString(0));
            model.setCustometUserId(c.getString(1));
            model.setPersonName(c.getString(2));
            model.setCompanyName(c.getString(3));
            model.setContactEmail(c.getString(4));
            model.setWorkPhone(c.getString(5));
            model.setMobile(c.getString(6));
            model.setContactDisplayName(c.getString(7));
            model.setWebsite(c.getString(8));
            model.setGSTIN(c.getString(9));
            model.setDLNumber(c.getString(10));
            model.setState(c.getString(11));
            model.setCity(c.getString(12));
            model.setCountry(c.getString(13));
            model.setZip(c.getString(14));
            model.setAddress(c.getString(15));
            model.setFax(c.getString(16));
            model.setCurrency(c.getString(17));
            model.setPaymentTerms(c.getString(18));
            model.setRemarks(c.getString(19));
        }
        return model;
    }

    public String setInitialUserCompanyId(String userId, String profileId) {
        String query = "SELECT id from companyDetail WHERE userId='" + userId + "' AND profileId='" + profileId + "' order by id DESC limit 1";
        Cursor c = db.rawQuery(query, null);
        String id = null;
        if (c != null && c.moveToFirst()) {
            id = c.getString(0); //The 0 is the column index, we only have 1 column, so the index is 0
        }
        return id;
    }

    public String getSelectedCompanyStateCode(String userId, String profileId, String companyId) {
        String query = "SELECT stateCode from companyDetail WHERE id='" + companyId + "' AND userId='" + userId + "' AND profileId='" + profileId + "' order by id DESC limit 1";
        Cursor c = db.rawQuery(query, null);
        String id = null;
        if (c != null && c.moveToFirst()) {
            id = c.getString(0); //The 0 is the column index, we only have 1 column, so the index is 0
        }
        return id;
    }

    public String getSelectedCustomerStateCode(String userId, String customerId) {
        String query = "SELECT selectedStateCode from customer WHERE id='" + customerId + "' AND userId='" + userId + "' order by id DESC limit 1";
        Cursor c = db.rawQuery(query, null);
        String id = null;
        if (c != null && c.moveToFirst()) {
            id = c.getString(0); //The 0 is the column index, we only have 1 column, so the index is 0
        }
        return id;
    }


    public ProductModel getProductListWithStock(ProductModel result, String userId, String profileId) {

        Cursor c = db.rawQuery("SELECT * FROM product WHERE userId='" + userId + "'ORDER BY id DESC", null);

//        Cursor cal = db.rawQuery("SELECT * FROM productStock WHERE userId='" + userId + "'ORDER BY id DESC", null);
//        ArrayList<String[]> val2 = null;
//        // Checking if records found
//        if (cal.getCount() != 0) {
//            val2 = new ArrayList<>();
//            while (cal.moveToNext()) {
//                String[] value = {cal.getString(0), cal.getString(1), cal.getString(2), cal.getString(3), cal.getString(4), cal.getString(5)};
//                val2.add(value);
//            }
//        }

        if (c.getCount() == 0) {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("No record found");
            return result;
        }

        Cursor c2 = db.rawQuery("SELECT productId,SUM(productStock.quantity) as total FROM productStock WHERE userId='" + userId + "'AND profileId='" + profileId + "' GROUP By productId", null);
        ArrayList<String[]> val = null;
        // Checking if records found
        if (c2.getCount() != 0) {
            val = new ArrayList<>();
            while (c2.moveToNext()) {
                String[] value = {c2.getString(0), c2.getString(1)};
                val.add(value);
            }
        }
        result.setOutputDB(Const.SUCCESS);
        result.setList(new ArrayList<ProductModel>());
        while (c.moveToNext()) {
            ProductModel model = new ProductModel();
            model.setProductId(c.getString(0));
            model.setProductUserId(c.getString(1));
            model.setProductName(c.getString(2));
            model.setCodeHSN(c.getString(3));
            model.setBarcode(c.getString(4));
            model.setCostPrice(c.getString(5));
            model.setSalePrice(c.getString(6));
            model.setTaxRate(c.getString(7));
            model.setFromDate(c.getString(8));
            model.setToDate(c.getString(9));
            model.setUnitCode(c.getString(10));
            model.setUnitName(c.getString(11));
            model.setCessRate(c.getString(12));
            if (val != null) {
                for (String[] temp : val) {
                    if (temp[0].equalsIgnoreCase(c.getString(0))) {
                        model.setQuantity(temp[1]);
                    }
                }
            }
            result.getList().add(model);
        }
        return result;
    }

    public Model addVoucher(Model result, String userId, String profileId, String companyId, String voucherNo, String voucherDate, String noteType, String noteTypeId, String noteReason, String noteReasonId, String customerId, String customerName, String invoiceNo, String freightAmt, String invoiceDate, String preGST, String placeOfSupply, String placeofSupplyId, String noteValue, String taxRate, String taxableValue, String cessAmt, ArrayList<ProductModel> productList, Boolean isIGSTApplicable) {
        // Searching product
        float maxTaxRate = 0;
        for (ProductModel model : productList) {
            float caltaxRate = convertToFloat(model.getTaxRate());
            if (maxTaxRate < caltaxRate) {
                maxTaxRate = caltaxRate;
            }
        }
        Cursor c = db.rawQuery("SELECT * FROM vouchers WHERE userId='" + userId + "' AND profileId='" + profileId + "' AND companyId='" + companyId + "' AND voucherNo='" + voucherNo + "'", null);
        if (!c.moveToFirst()) {
            // Inserting record
            db.execSQL(
                    "INSERT INTO vouchers ('userId','profileId','companyId','voucherNo','voucherDate','noteType','noteTypeId','noteReason','noteReasonId','customerId','customerName','invoiceNo','freightAmt','invoiceDate','preGST','placeOfSupply','placeofSupplyId','noteValue','taxRate','taxableValue','cessAmt') VALUES(" +
                            "'" + userId + "','" + profileId + "','" + companyId + "','" + voucherNo + "','" + voucherDate + "','" + noteType + "','" + noteTypeId + "','" + noteReason + "','" + noteReasonId + "','" + customerId + "','" + customerName + "','" + invoiceNo + "','" + freightAmt + "','" + invoiceDate + "','" + preGST + "','" + placeOfSupply + "','" + placeofSupplyId + "','" + noteValue + "','" + taxRate + "','" + taxableValue + "','" + cessAmt + "');");
            for (ProductModel model : productList) {
                float totalProdPrice = convertToFloat(model.getSalePrice()) * convertToFloat(model.getQuantity());
                String allProdTax[] = calTax(String.valueOf(totalProdPrice), model.getTaxRate(), isIGSTApplicable);
                String totalTaxAmount = allProdTax[0];
                String prodCGSTTax = allProdTax[1];
                String prodCGSTVal = allProdTax[2];
                String prodSGSTTax = allProdTax[3];
                String prodSGSTVal = allProdTax[4];
                String prodIGSTTax = allProdTax[5];
                String prodIGSTVal = allProdTax[6];
                db.execSQL(
                        "INSERT INTO vouchersProduct ('userId','profileId','companyId','voucherNo','quantity','productId','productName','codeHSN','barcode','costPrice','salePrice','taxRate','fromDate','toDate','totalTaxAmount','cgstTax','cgstVal','sgstTax','sgstVal','igstTax','igstVal') VALUES(" +
                                "'" + userId + "','" + profileId + "','" + companyId + "','" + voucherNo + "','" + model.getQuantity() + "','" + model.getProductId() + "','" + model.getProductName() + "','" + model.getCodeHSN() + "','" + model.getBarcode() + "','" + model.getCostPrice() + "','" + model.getSalePrice() + "','" + model.getTaxRate() + "','" + model.getFromDate() + "','" + model.getToDate() + "','" + totalTaxAmount + "','" + prodCGSTTax + "','" + prodCGSTVal + "','" + prodSGSTTax + "','" + prodSGSTVal + "','" + prodIGSTTax + "','" + prodIGSTVal + "');");
            }
            result.setOutputDB(Const.SUCCESS);
            result.setOutputDBMsg("Voucher created successful");
            return result;
        } else {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("Voucher already exist");
            return result;
        }
    }

    public Model copyVoucher(Model result, String userId, String profileId, String companyId, String voucherNo, String voucherDate, String noteType, String noteTypeId, String noteReason, String noteReasonId, String customerId, String customerName, String invoiceNo, String freightAmt, String invoiceDate, String preGST, String placeOfSupply, String placeofSupplyId, String noteValue, String taxRate, String taxableValue, String cessAmt, ArrayList<ProductModel> productList, boolean isIGSTApplicable) {
        // Searching product
        float maxTaxRate = 0;
        for (ProductModel model : productList) {
            float caltaxRate = convertToFloat(model.getTaxRate());
            if (maxTaxRate < caltaxRate) {
                maxTaxRate = caltaxRate;
            }
        }
        Cursor c = db.rawQuery("SELECT * FROM vouchers WHERE userId='" + userId + "'AND profileId='" + profileId + "'AND companyId='" + companyId + "'AND voucherNo='" + voucherNo + "'", null);
        if (!c.moveToFirst()) {
            // Inserting record
            db.execSQL(
                    "INSERT INTO vouchers ('userId','profileId','companyId','voucherNo','voucherDate','noteType','noteTypeId','noteReason','noteReasonId','customerId','customerName','invoiceNo','freightAmt','invoiceDate','preGST','placeOfSupply','placeofSupplyId','noteValue','taxRate','taxableValue','cessAmt') VALUES(" +
                            "'" + userId + "','" + profileId + "','" + companyId + "','" + voucherNo + "','" + voucherDate + "','" + noteType + "','" + noteTypeId + "','" + noteReason + "','" + noteReasonId + "','" + customerId + "','" + customerName + "','" + invoiceNo + "','" + freightAmt + "','" + invoiceDate + "','" + preGST + "','" + placeOfSupply + "','" + placeofSupplyId + "','" + noteValue + "','" + taxRate + "','" + taxableValue + "','" + cessAmt + "');");
            for (ProductModel model : productList) {
                float totalProdPrice = convertToFloat(model.getSalePrice()) * convertToFloat(model.getQuantity());
                String allProdTax[] = calTax(String.valueOf(totalProdPrice), model.getTaxRate(), isIGSTApplicable);
                String totalTaxAmount = allProdTax[0];
                String prodCGSTTax = allProdTax[1];
                String prodCGSTVal = allProdTax[2];
                String prodSGSTTax = allProdTax[3];
                String prodSGSTVal = allProdTax[4];
                String prodIGSTTax = allProdTax[5];
                String prodIGSTVal = allProdTax[6];
                db.execSQL(
                        "INSERT INTO vouchersProduct ('userId','profileId','companyId','voucherNo','quantity','productId','productName','codeHSN','barcode','costPrice','salePrice','taxRate','fromDate','toDate','totalTaxAmount','cgstTax','cgstVal','sgstTax','sgstVal','igstTax','igstVal') VALUES(" +
                                "'" + userId + "','" + profileId + "','" + companyId + "','" + voucherNo + "','" + model.getQuantity() + "','" + model.getProductId() + "','" + model.getProductName() + "','" + model.getCodeHSN() + "','" + model.getBarcode() + "','" + model.getCostPrice() + "','" + model.getSalePrice() + "','" + model.getTaxRate() + "','" + model.getFromDate() + "','" + model.getToDate() + "','" + totalTaxAmount + "','" + prodCGSTTax + "','" + prodCGSTVal + "','" + prodSGSTTax + "','" + prodSGSTVal + "','" + prodIGSTTax + "','" + prodIGSTVal + "');");
            }
            result.setOutputDB(Const.SUCCESS);
            result.setOutputDBMsg("Voucher created successful");
            return result;
        } else {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("Voucher already exist");
            return result;
        }
    }

    public Model editVoucher(Model result, String voucherId, ArrayList<String> deletedProduct, String voucherUserId, String voucherProfileId, String voucherCompanyId, String voucherNo
            , String voucherDate, String noteType, String noteTypeId, String noteReason, String noteReasonId, String customerId, String customerName, String invoiceNo, String freightAmt, String invoiceDate, String preGST, String placeOfSupply, String placeofSupplyId, String noteValue, String taxRate, String taxableValue, String cessAmt, ArrayList<ProductModel> productList, boolean isIGSTApplicable) {
        // Searching product
        Cursor c = db.rawQuery("SELECT * FROM vouchers WHERE id='" + voucherId + "'AND userId='" + voucherUserId + "'AND profileId='" + voucherProfileId + "'AND companyId='" + voucherCompanyId + "'AND voucherNo='" + voucherNo + "'", null);
        if (!c.moveToFirst()) {
            // Inserting record
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("Voucher not exist");
            return result;
        } else {
            //Modifying record if found
            db.execSQL("UPDATE vouchers SET voucherDate='" + voucherDate + "',noteType='" + noteType + "',noteTypeId='" + noteTypeId + "',noteReason='" + noteReason + "',noteReasonId='" + noteReasonId + "',customerId='" + customerId + "',customerName='" + customerName + "',invoiceNo='" + invoiceNo + "',freightAmt='" + freightAmt + "',invoiceDate='" + invoiceDate + "',preGST='" + preGST + "',placeOfSupply='" + placeOfSupply + "',placeofSupplyId='" + placeofSupplyId + "',noteValue='" + noteValue + "',taxRate='" + taxRate + "',taxableValue='" + taxableValue + "',cessAmt='" + cessAmt +
                    "'WHERE id='" + voucherId + "'AND userId='" + voucherUserId + "'AND profileId='" + voucherProfileId + "'AND companyId='" + voucherCompanyId + "'");
            editVoucherProductList(voucherUserId, voucherProfileId, voucherCompanyId, voucherNo, productList, deletedProduct, isIGSTApplicable);
            result.setOutputDB(Const.SUCCESS);
            result.setOutputDBMsg("Voucher edited successful");
            return result;
        }
    }

    private float editVoucherProductList(String userId, String profileId, String companyId, String voucherNo, ArrayList<ProductModel> productList, ArrayList<String> deletedProduct, Boolean isIGSTApplicable) {
//        for (String data : deletedProduct) {
//            // Searching product
//            Cursor c = db.rawQuery("SELECT * FROM vouchersProduct WHERE productId='" + data + "'AND userId='" + userId + "'AND profileId='" + profileId + "'AND companyId='" + companyId + "'AND voucherNo='" + voucherNo + "'", null);
//            if (c.moveToFirst()) {
//                // Deleting record if found
//                db.execSQL("DELETE FROM vouchersProduct WHERE productId='" + data + "'AND userId='" + userId + "'AND profileId='" + profileId + "'AND companyId='" + companyId + "'AND voucherNo='" + voucherNo + "'");
//            } else {
//                //record not found
//            }
//        }
        Cursor cursor = db.rawQuery("SELECT * FROM vouchersProduct WHERE userId='" + userId + "'AND profileId='" + profileId + "'AND companyId='" + companyId + "'AND voucherNo='" + voucherNo + "'", null);
        if (cursor.moveToFirst()) {
            // Deleting record if found
            db.execSQL("DELETE FROM vouchersProduct WHERE userId='" + userId + "'AND profileId='" + profileId + "'AND companyId='" + companyId + "'AND voucherNo='" + voucherNo + "'");
        } else {
            //record not found
        }

        float maxTaxRate = 0;
        for (ProductModel model : productList) {
            float taxRate = convertToFloat(model.getTaxRate());
            if (maxTaxRate < taxRate) {
                maxTaxRate = taxRate;
            }
            float totalProdPrice = convertToFloat(model.getSalePrice()) * convertToFloat(model.getQuantity());
            String allProdTax[] = calTax(String.valueOf(totalProdPrice), model.getTaxRate(), isIGSTApplicable);
            String totalTaxAmount = allProdTax[0];
            String prodCGSTTax = allProdTax[1];
            String prodCGSTVal = allProdTax[2];
            String prodSGSTTax = allProdTax[3];
            String prodSGSTVal = allProdTax[4];
            String prodIGSTTax = allProdTax[5];
            String prodIGSTVal = allProdTax[6];
            // Searching product
            Cursor c = db.rawQuery("SELECT * FROM vouchersProduct WHERE productId='" + model.getProductId() + "'AND userId='" + userId + "'AND profileId='" + profileId + "'AND companyId='" + companyId + "'AND voucherNo='" + voucherNo + "'", null);
            if (!c.moveToFirst()) {
                //record not found
                db.execSQL(
                        "INSERT INTO vouchersProduct ('userId','profileId','companyId','voucherNo','quantity','productId','productName','codeHSN','barcode','costPrice','salePrice','taxRate','fromDate','toDate','totalTaxAmount','cgstTax','cgstVal','sgstTax','sgstVal','igstTax','igstVal') VALUES(" +
                                "'" + userId + "','" + profileId + "','" + companyId + "','" + voucherNo + "','" + model.getQuantity() + "','" + model.getProductId() + "','" + model.getProductName() + "','" + model.getCodeHSN() + "','" + model.getBarcode() + "','" + model.getCostPrice() + "','" + model.getSalePrice() + "','" + model.getTaxRate() + "','" + model.getFromDate() + "','" + model.getToDate() + "','" + totalTaxAmount + "','" + prodCGSTTax + "','" + prodCGSTVal + "','" + prodSGSTTax + "','" + prodSGSTVal + "','" + prodIGSTTax + "','" + prodIGSTVal + "');");
            } else {
                //Modifying record if found
                db.execSQL("UPDATE vouchersProduct SET quantity='" + model.getQuantity() + "',productName='" + model.getProductName() + "',codeHSN='" + model.getCodeHSN() + "',barcode='" + model.getBarcode() + "',costPrice='" + model.getCostPrice() + "',salePrice='" + model.getSalePrice() + "',taxRate='" + model.getTaxRate() + "',fromDate='" + model.getFromDate() + "',toDate='" + model.getToDate() + "',totalTaxAmount='" + totalTaxAmount + "',cgstTax='" + prodCGSTTax + "',cgstVal='" + prodCGSTVal + "',sgstTax='" + prodSGSTTax + "',sgstVal='" + prodSGSTVal + "',igstTax='" + prodIGSTTax + "',igstVal='" + prodIGSTVal +
                        "'WHERE productId='" + model.getProductId() + "'AND voucherNo='" + voucherNo + "'AND userId='" + userId + "'AND profileId='" + profileId + "'AND companyId='" + companyId + "'");
            }
        }
        return maxTaxRate;
    }

    public String getVoucherNumber(String userId, String profileId) {
        String response = "";
        //        Cursor c = db.rawQuery("SELECT * FROM vendor ORDER BY id DESC", null);
        Cursor c = db.rawQuery("SELECT voucherNo FROM vouchers WHERE userId='" + userId + "'AND profileId='" + profileId + "'ORDER BY voucherNo ASC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            return "";
        }
        while (c.moveToNext()) {
            response = c.getString(0);
            L.m(response);
        }
        return response;
    }

    public VoucherModel getVouchersList(VoucherModel result, String userId, String profileId) {
        //        Cursor c = db.rawQuery("SELECT * FROM vendor ORDER BY id DESC", null);
        Cursor c = db.rawQuery("SELECT * FROM vouchers WHERE userId='" + userId + "'AND profileId='" + profileId + "'ORDER BY id DESC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("No record found");
            return result;
        }
        result.setOutputDB(Const.SUCCESS);
        result.setList(new ArrayList<VoucherModel>());


        while (c.moveToNext()) {
            VoucherModel model = new VoucherModel();
            model.setVoucherId(c.getString(0));
            model.setVoucherUserId(c.getString(1));
            model.setVoucherProfileId(c.getString(2));
            model.setVoucherCompanyId(c.getString(3));
            model.setVoucherNo(c.getString(4));
            model.setVoucherDate(c.getString(5));
            model.setNoteType(c.getString(6));
            model.setNoteTypeId(c.getString(7));
            model.setNoteReason(c.getString(8));
            model.setNoteReasonId(c.getString(9));
            model.setCustomerId(c.getString(10));
            model.setCustomerName(c.getString(11));
            model.setInvoiceNumber(c.getString(12));
            model.setFreightAmount(c.getString(13));
            model.setInvoiceDate(c.getString(14));
            model.setPreGST(c.getString(15));
            model.setPlaceofSupply(c.getString(16));
            model.setPlaceofSupplyId(c.getString(17));
            model.setNoteValue(c.getString(18));
            model.setTaxRate(c.getString(19));
            model.setTaxableValue(c.getString(20));
            model.setCessAmt(c.getString(21));

            model.setProductList(getVouchersProductList(userId, profileId, c.getString(3), c.getString(4)));
            result.getList().add(model);
        }
        return result;
    }

    private ArrayList<ProductModel> getVouchersProductList(String userId, String profileId, String companyId, String voucherNo) {
        Cursor c = db.rawQuery("SELECT * FROM vouchersProduct WHERE userId='" + userId + "'AND profileId='" + profileId + "'AND companyId='" + companyId + "'AND voucherNo='" + voucherNo + "'", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            return null;
        }

        ArrayList<ProductModel> prodList = new ArrayList<>();
        while (c.moveToNext()) {
            ProductModel model = new ProductModel();
            model.setPurchasesProdId(c.getString(0));
            model.setProductUserId(c.getString(1));
            model.setProductProfileId(c.getString(2));
            model.setProductCompanyId(c.getString(3));
            model.setVoucherNo(c.getString(4));
            model.setQuantity(c.getString(5));
            model.setProductId(c.getString(6));
            model.setProductName(c.getString(7));
            model.setCodeHSN(c.getString(8));
            model.setBarcode(c.getString(9));
            model.setCostPrice(c.getString(10));
            model.setSalePrice(c.getString(11));
            model.setTaxRate(c.getString(12));
            model.setFromDate(c.getString(13));
            model.setToDate(c.getString(14));
            model.setTotalTaxAmount(c.getString(15));
            model.setCgstTax(c.getString(16));
            model.setCgstVal(c.getString(17));
            model.setSgstTax(c.getString(18));
            model.setSgstVal(c.getString(19));
            model.setIgstTax(c.getString(20));
            model.setIgstVal(c.getString(21));
            prodList.add(model);
        }
        return prodList;
    }

    public String deleteVouchers(VoucherModel itemVouchers) {
        // Searching product
        Cursor c = db.rawQuery("SELECT * FROM vouchers WHERE id='" + itemVouchers.getVoucherId() + "'AND userId='" + itemVouchers.getVoucherUserId() + "'AND profileId='" + itemVouchers.getVoucherProfileId() + "'AND companyId='" + itemVouchers.getVoucherCompanyId() + "'AND voucherNo='" + itemVouchers.getVoucherNo() + "'", null);
        if (c.moveToFirst()) {
            // Deleting record if found
            db.execSQL("DELETE FROM vouchers WHERE id='" + itemVouchers.getVoucherId() + "'AND userId='" + itemVouchers.getVoucherUserId() + "'AND profileId='" + itemVouchers.getVoucherProfileId() + "'AND companyId='" + itemVouchers.getVoucherCompanyId() + "'AND voucherNo='" + itemVouchers.getVoucherNo() + "'");
            deleteVoucherProductList(itemVouchers.getVoucherNo(), itemVouchers.getVoucherUserId(), itemVouchers.getVoucherProfileId());
            return "Invoice Deleted";
        } else {
            return "Invalid Invoice Id";
        }
    }

    private void deleteVoucherProductList(String voucherNo, String userId, String profileId) {
        // sellInvProduct ('userId','profileId','companyId','invoiceNumber'
        Cursor c = db.rawQuery("SELECT * FROM vouchersProduct WHERE userId='" + userId + "'AND profileId='" + profileId + "'AND voucherNo='" + voucherNo + "'", null);
        if (c.moveToFirst()) {
            // Deleting record if found
            db.execSQL("DELETE FROM vouchersProduct WHERE userId='" + userId + "'AND profileId='" + profileId + "'AND voucherNo='" + voucherNo + "'");
        } else {
            //record not found
            L.m("record not found");
        }
    }

//    //"CREATE TABLE IF NOT EXISTS debtors(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR , profileId VARCHAR ,invoiceNo VARCHAR, customerId VARCHAR, customerName VARCHAR, paidAmount VARCHAR, payableAmount VARCHAR, balanceAmount VARCHAR, paymentMode VARCHAR, BankName VARCHAR, chequeNo VARCHAR, AccName VARCHAR, IFSC VARCHAR, AccNumber VARCHAR, BranchAddress VARCHAR);");
//    public String getCustomerBalanceAmount(String userId, String profileId, String customerId) {
//        String result = "";
//        Cursor c = db.rawQuery("SELECT * FROM debtors WHERE userId='" + userId + "'AND profileId='" + profileId + "'AND customerId='" + customerId + "'", null);
//        if (c.moveToFirst()) {
//            // if record  found
//            Cursor c2 = db.rawQuery("SELECT customerId,SUM(debtors.balanceAmount) as total FROM debtors WHERE userId='" + userId + "'AND profileId='" + profileId + "'AND customerId='" + customerId + "' GROUP By customerId", null);
//            // Checking if records found
//            if (c2.getCount() != 0) {
//                while (c2.moveToNext()) {
//                    String data1 = c2.getString(0);
//                    result = c2.getString(1);
//                }
//            }
//        } else {
//            //record not found
//            result = "0";
//        }
//
//        return result;
//    }


    //"CREATE TABLE IF NOT EXISTS debtors(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR , profileId VARCHAR ,invoiceNo VARCHAR, customerId VARCHAR, customerName VARCHAR, paidAmount VARCHAR, payableAmount VARCHAR, balanceAmount VARCHAR, paymentMode VARCHAR, BankName VARCHAR, chequeNo VARCHAR, AccName VARCHAR, IFSC VARCHAR, AccNumber VARCHAR, BranchAddress VARCHAR);");
    public String[] getCustomerBalanceAmountByInvoiceId(String userId, String profileId, String customerId, String invoiceNo) {
        String[] result = new String[2];

        Cursor c = db.rawQuery("SELECT openingBalance,paymentReceived FROM debtors WHERE userId='" + userId + "' AND profileId='" + profileId + "'AND customerId='" + customerId + "'AND invoiceNo='" + invoiceNo + "'", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            result[0] = "0";
            result[1] = "0";
            return result;
        }
        while (c.moveToNext()) {
            result[0] = c.getString(0);
            result[1] = c.getString(1);
        }
        return result;
    }

    //"CREATE TABLE IF NOT EXISTS debtors(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR , profileId VARCHAR ,invoiceNo VARCHAR, customerId VARCHAR, customerName VARCHAR, paidAmount VARCHAR, payableAmount VARCHAR, balanceAmount VARCHAR, paymentMode VARCHAR, BankName VARCHAR, chequeNo VARCHAR, AccName VARCHAR, IFSC VARCHAR, AccNumber VARCHAR, BranchAddress VARCHAR);");
    public String[] getCustomerBalanceAmount(String userId, String profileId, String customerId) {
        String[] result = new String[2];
        Cursor c = db.rawQuery("SELECT balanceAmount FROM debtors WHERE userId='" + userId + "' AND profileId='" + profileId + "'AND customerId='" + customerId + "' ORDER BY id ASC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            result[0] = "0";
            result[1] = "0";
            return result;
        }
        while (c.moveToNext()) {
            result[0] = c.getString(0);
            result[1] = "0";
        }
        return result;
    }

    //"CREATE TABLE IF NOT EXISTS creditors(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR , profileId VARCHAR ,invoiceNo VARCHAR, customerId VARCHAR, customerName VARCHAR, paidAmount VARCHAR, payableAmount VARCHAR, balanceAmount VARCHAR, paymentMode VARCHAR, BankName VARCHAR, chequeNo VARCHAR, AccName VARCHAR, IFSC VARCHAR, AccNumber VARCHAR, BranchAddress VARCHAR);");
    public String[] getVendorBalanceAmount(String userId, String profileId, String vendorId) {
        String[] result = new String[2];
        Cursor c = db.rawQuery("SELECT balanceAmount FROM creditors WHERE userId='" + userId + "' AND profileId='" + profileId + "'AND vendorId='" + vendorId + "' ORDER BY id ASC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            result[0] = "0";
            result[1] = "0";
            return result;
        }
        while (c.moveToNext()) {
            result[0] = c.getString(0);
            result[1] = "0";
        }
        return result;
    }

    //"CREATE TABLE IF NOT EXISTS debtors(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR , profileId VARCHAR ,invoiceNo VARCHAR, customerId VARCHAR, customerName VARCHAR, paidAmount VARCHAR, payableAmount VARCHAR, balanceAmount VARCHAR, paymentMode VARCHAR, BankName VARCHAR, chequeNo VARCHAR, AccName VARCHAR, IFSC VARCHAR, AccNumber VARCHAR, BranchAddress VARCHAR);");
    public String[] getVendorBalanceAmountByInvoiceId(String userId, String profileId, String vendorId, String invoiceNo) {
        String[] result = new String[2];

        Cursor c = db.rawQuery("SELECT openingBalance,paymentReceived FROM creditors WHERE userId='" + userId + "' AND profileId='" + profileId + "'AND vendorId='" + vendorId + "'AND invoiceNo='" + invoiceNo + "'", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            result[0] = "0";
            result[1] = "0";
            return result;
        }
        while (c.moveToNext()) {
            result[0] = c.getString(0);
            result[1] = c.getString(1);
        }
        return result;
    }

    public PaymentModel getDebtorsListWithStock(PaymentModel result, String userId, String profileId) {
        //Cursor c = db.rawQuery("SELECT * FROM product ORDER BY id DESC", null);
        Cursor c = db.rawQuery("SELECT * FROM debtors WHERE userId='" + userId + "' AND profileId='" + profileId + "' GROUP BY customerId ORDER BY id DESC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("No debtors available");
            return result;
        }
        result.setOutputDB(Const.SUCCESS);
        result.setList(new ArrayList<PaymentModel>());
        while (c.moveToNext()) {
            float balAmt=Utility.parseFloat(c.getString(8));
            if(balAmt>0) {
                PaymentModel model = new PaymentModel();
                model.setDebtorsId(c.getString(0));
                model.setUserId(c.getString(1));
                model.setProfileId(c.getString(2));
                model.setInvoiceNo(c.getString(3));
                model.setCustomerId(c.getString(4));
                model.setCustomerName(c.getString(5));
                model.setPaidAmount(c.getString(6));
                model.setPayableAmount(c.getString(7));
                model.setBalanceAmount(c.getString(8));
                model.setPaymentMode(c.getString(9));
                model.setBankName(c.getString(10));
                model.setChequeNo(c.getString(11));
                model.setAccName(c.getString(12));
                model.setIFSC(c.getString(13));
                model.setAccNumber(c.getString(14));
                model.setBranchAddress(c.getString(15));
                result.getList().add(model);
            }
        }
        return result;
    }

    public PaymentModel getAllDebtorsListWithStock(PaymentModel result, String userId, String profileId) {
        //Cursor c = db.rawQuery("SELECT * FROM product ORDER BY id DESC", null);
        Cursor c = db.rawQuery("SELECT * FROM debtors WHERE userId='" + userId + "' AND profileId='" + profileId + "' GROUP BY customerId ORDER BY id DESC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("No record found");
            return result;
        }
        result.setOutputDB(Const.SUCCESS);
        result.setList(new ArrayList<PaymentModel>());
        while (c.moveToNext()) {
            PaymentModel model = new PaymentModel();
            model.setDebtorsId(c.getString(0));
            model.setUserId(c.getString(1));
            model.setProfileId(c.getString(2));
            model.setInvoiceNo(c.getString(3));
            model.setCustomerId(c.getString(4));
            model.setCustomerName(c.getString(5));
            model.setPaidAmount(c.getString(6));
            model.setPayableAmount(c.getString(7));
            model.setBalanceAmount(c.getString(8));
            model.setPaymentMode(c.getString(9));
            model.setBankName(c.getString(10));
            model.setChequeNo(c.getString(11));
            model.setAccName(c.getString(12));
            model.setIFSC(c.getString(13));
            model.setAccNumber(c.getString(14));
            model.setBranchAddress(c.getString(15));
            model.setOpeningBalance(c.getString(16));
            result.getList().add(model);
        }
        return result;
    }


    // "CREATE TABLE IF NOT EXISTS creditors(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR , profileId VARCHAR ,invoiceNo VARCHAR, vendorId VARCHAR, vendorName VARCHAR, paidAmount VARCHAR, payableAmount VARCHAR, balanceAmount VARCHAR, paymentMode VARCHAR, BankName VARCHAR, chequeNo VARCHAR, AccName VARCHAR, IFSC VARCHAR, AccNumber VARCHAR, BranchAddress VARCHAR, openingBalance VARCHAR, paymentReceived VARCHAR);");
    public PaymentModel getCreditorsListWithStock(PaymentModel result, String userId, String profileId) {
        //Cursor c = db.rawQuery("SELECT * FROM product ORDER BY id DESC", null);
        Cursor c = db.rawQuery("SELECT * FROM creditors WHERE userId='" + userId + "' AND profileId='" + profileId + "' GROUP BY vendorId ORDER BY id DESC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("No creditors available");
            return result;
        }
        result.setOutputDB(Const.SUCCESS);
        result.setList(new ArrayList<PaymentModel>());
        while (c.moveToNext()) {
            float balAmt=Utility.parseFloat(c.getString(8));
            if(balAmt>0) {
                PaymentModel model = new PaymentModel();
                model.setDebtorsId(c.getString(0));
                model.setUserId(c.getString(1));
                model.setProfileId(c.getString(2));
                model.setInvoiceNo(c.getString(3));
                model.setVendorId(c.getString(4));
                model.setVendorName(c.getString(5));
                model.setPaidAmount(c.getString(6));
                model.setPayableAmount(c.getString(7));
                model.setBalanceAmount(c.getString(8));
                model.setPaymentMode(c.getString(9));
                model.setBankName(c.getString(10));
                model.setChequeNo(c.getString(11));
                model.setAccName(c.getString(12));
                model.setIFSC(c.getString(13));
                model.setAccNumber(c.getString(14));
                model.setBranchAddress(c.getString(15));
                result.getList().add(model);
            }
        }
        return result;
    }

    public PaymentModel getAllCreditorsListWithStock(PaymentModel result, String userId, String profileId) {
        //Cursor c = db.rawQuery("SELECT * FROM product ORDER BY id DESC", null);
        Cursor c = db.rawQuery("SELECT * FROM creditors WHERE userId='" + userId + "' AND profileId='" + profileId + "' GROUP BY vendorId ORDER BY id DESC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("No record found");
            return result;
        }
        result.setOutputDB(Const.SUCCESS);
        result.setList(new ArrayList<PaymentModel>());
        while (c.moveToNext()) {
            PaymentModel model = new PaymentModel();
            model.setDebtorsId(c.getString(0));
            model.setUserId(c.getString(1));
            model.setProfileId(c.getString(2));
            model.setInvoiceNo(c.getString(3));
            model.setVendorId(c.getString(4));
            model.setVendorName(c.getString(5));
            model.setPaidAmount(c.getString(6));
            model.setPayableAmount(c.getString(7));
            model.setBalanceAmount(c.getString(8));
            model.setPaymentMode(c.getString(9));
            model.setBankName(c.getString(10));
            model.setChequeNo(c.getString(11));
            model.setAccName(c.getString(12));
            model.setIFSC(c.getString(13));
            model.setAccNumber(c.getString(14));
            model.setBranchAddress(c.getString(15));
            model.setOpeningBalance(c.getString(16));
            result.getList().add(model);
        }
        return result;
    }

    public boolean isDebitorsAvailable(String userId, String profileId) {
        Cursor c = db.rawQuery("SELECT * FROM debtors WHERE userId='" + userId + "' AND profileId='" + profileId + "' ORDER BY id DESC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            return false;
        } else {
            return true;
        }
    }

    //"CREATE TABLE IF NOT EXISTS creditors(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR , profileId VARCHAR ,invoiceNo VARCHAR, vendorId VARCHAR, vendorName VARCHAR, paidAmount VARCHAR, payableAmount VARCHAR, balanceAmount VARCHAR, paymentMode VARCHAR, BankName VARCHAR, chequeNo VARCHAR, AccName VARCHAR, IFSC VARCHAR, AccNumber VARCHAR, BranchAddress VARCHAR, openingBalance VARCHAR, paymentReceived VARCHAR);");
    public boolean isCreditorsAvailable(String userId, String profileId) {
        Cursor c = db.rawQuery("SELECT * FROM creditors WHERE userId='" + userId + "' AND profileId='" + profileId + "' ORDER BY id DESC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            return false;
        } else {
            return true;
        }
    }

    //"CREATE TABLE IF NOT EXISTS debtors(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR , profileId VARCHAR ,invoiceNo VARCHAR, customerId VARCHAR, customerName VARCHAR, paidAmount VARCHAR, payableAmount VARCHAR, balanceAmount VARCHAR, paymentMode VARCHAR, BankName VARCHAR, chequeNo VARCHAR, AccName VARCHAR, IFSC VARCHAR, AccNumber VARCHAR, BranchAddress VARCHAR, openingBalance VARCHAR, paymentReceived VARCHAR);");
    public DashboardReportModel getPaymentReport(DashboardReportModel resultModel,String userId, String profileId) {
        float  paidByCust = 0,paidByVendor = 0, unpaidByCust = 0, unpaidByVendor = 0, overPaidByCust = 0, overPaidByVendor = 0;
        ArrayList<String> topThreeCustomer = new ArrayList<>();
        ArrayList<String> topThreeVendor = new ArrayList<>();
        resultModel.setDebitorsAvailable(false);
        resultModel.setCreditorsAvailable(false);
        if(isDebitorsAvailable(userId,profileId)) {
            resultModel.setDebitorsAvailable(true);
            Cursor c2 = db.rawQuery("SELECT customerId,customerName,SUM(debtors.paidAmount) as total FROM debtors WHERE userId='" + userId + "'AND profileId='" + profileId + "' GROUP By customerId ORDER BY total DESC", null);
            ArrayList<String[]> val1 = null;
            // Checking if records found
            if (c2.getCount() != 0) {
                val1 = new ArrayList<>();
                while (c2.moveToNext()) {
                    String[] value = {c2.getString(0), c2.getString(1), c2.getString(2)};
                    topThreeCustomer.add(". " + c2.getString(1) + " (Rs." + c2.getString(2) + ")");
                    val1.add(value);
                }
                L.m(val1.toString());
            } else {
                L.m("No record");
            }
            for (String[] data : val1) {
                paidByCust += Utility.parseFloat(data[2]);
            }

            Cursor cursor4 = db.rawQuery("SELECT customerId,customerName,balanceAmount FROM debtors WHERE userId='" + userId + "'AND profileId='" + profileId + "' GROUP By customerId ORDER BY id DESC", null);
            ArrayList<String[]> val3 = null;
            // Checking if records found
            if (cursor4.getCount() != 0) {
                val3 = new ArrayList<>();
                while (cursor4.moveToNext()) {
                    String[] value = {cursor4.getString(0), cursor4.getString(1), cursor4.getString(2)};
                    val3.add(value);
                }
                L.m(val3.toString());
            } else {
                L.m("No record");
            }
            for (String[] data : val3) {
                float result = Utility.parseFloat(data[2]);
                if(result<0) {
                    overPaidByCust +=result;
                }else{
                    unpaidByCust +=result;
                }
            }


        }

        if(isCreditorsAvailable(userId,profileId)) {
            resultModel.setCreditorsAvailable(true);
            Cursor c3 = db.rawQuery("SELECT vendorId,vendorName,SUM(creditors.paidAmount) as total FROM creditors WHERE userId='" + userId + "'AND profileId='" + profileId + "' GROUP By vendorId ORDER BY total DESC", null);
            ArrayList<String[]> val2 = null;
            // Checking if records found
            if (c3.getCount() != 0) {
                val2 = new ArrayList<>();
                while (c3.moveToNext()) {
                    String[] value = {c3.getString(0), c3.getString(1), c3.getString(2)};
                    topThreeVendor.add(". " + c3.getString(1) + " (Rs." + c3.getString(2) + ")");
                    val2.add(value);
                }
                L.m(val2.toString());
            } else {
                L.m("No record");
            }

            for (String[] data : val2) {
                paidByVendor += Utility.parseFloat(data[2]);
            }


            Cursor cursor5 = db.rawQuery("SELECT vendorId,vendorName,balanceAmount FROM creditors WHERE userId='" + userId + "'AND profileId='" + profileId + "' GROUP By vendorId ORDER BY id DESC", null);
            ArrayList<String[]> val4 = null;
            // Checking if records found
            if (cursor5.getCount() != 0) {
                val4 = new ArrayList<>();
                while (cursor5.moveToNext()) {
                    String[] value = {cursor5.getString(0), cursor5.getString(1), cursor5.getString(2)};
                    val4.add(value);
                }
                L.m(val4.toString());
            } else {
                L.m("No record");
            }

            for (String[] data : val4) {
                float result = Utility.parseFloat(data[2]);
                if (result < 0) {
                    overPaidByVendor += result;
                } else {
                    unpaidByVendor += result;
                }
            }
        }
        String temp1 = Utility.decimalFormat(paidByCust);
        String temp2 = Utility.decimalFormat(paidByVendor);
        String temp3 = Utility.decimalFormat(unpaidByCust);
        String temp4 = Utility.decimalFormat(unpaidByVendor);
        String temp5 = Utility.decimalFormat(overPaidByCust);
        String temp6 = Utility.decimalFormat(overPaidByVendor);

        resultModel.setAllFields(temp1,temp2,temp3,temp4,temp5,temp6);
        resultModel.setTopThreeCustomer(topThreeCustomer);
        resultModel.setTopThreeVendor(topThreeVendor);

        return resultModel;
    }

    //"CREATE TABLE IF NOT EXISTS debtors(id INTEGER PRIMARY KEY AUTOINCREMENT, userId VARCHAR , profileId VARCHAR ,invoiceNo VARCHAR, customerId VARCHAR, customerName VARCHAR, paidAmount VARCHAR, payableAmount VARCHAR, balanceAmount VARCHAR, paymentMode VARCHAR, BankName VARCHAR, chequeNo VARCHAR, AccName VARCHAR, IFSC VARCHAR, AccNumber VARCHAR, BranchAddress VARCHAR, openingBalance VARCHAR, paymentReceived VARCHAR);");
    private ArrayList<String[]> getTableValue(String tableName) {
        Cursor c3 = db.rawQuery("SELECT customerName,balanceAmount FROM " + tableName + " ", null);
        ArrayList<String[]> val2 = null;
        // Checking if records found
        if (c3.getCount() != 0) {
            val2 = new ArrayList<>();
            while (c3.moveToNext()) {
                String[] value = {c3.getString(0), c3.getString(1)};
                val2.add(value);
            }
            return val2;
        } else {
            return val2;
        }
    }

    public PaymentModel getBuyerUnpaidReceiptListWithStock(PaymentModel result, String userId, String profileId) {
        //Cursor c = db.rawQuery("SELECT * FROM product ORDER BY id DESC", null);
        Cursor c = db.rawQuery("SELECT * FROM creditors WHERE userId='" + userId + "' AND profileId='" + profileId + "' GROUP BY vendorId ORDER BY id DESC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("No record found");
            return result;
        }
        result.setOutputDB(Const.SUCCESS);
        result.setList(new ArrayList<PaymentModel>());
        while (c.moveToNext()) {
            float balanceAmt = Utility.parseFloat(c.getString(8));
            if(balanceAmt>0){
                PaymentModel model = new PaymentModel();
                model.setDebtorsId(c.getString(0));
                model.setUserId(c.getString(1));
                model.setProfileId(c.getString(2));
                model.setInvoiceNo(c.getString(3));
                model.setVendorId(c.getString(4));
                model.setVendorName(c.getString(5));
                model.setPaidAmount(c.getString(6));
                model.setPayableAmount(c.getString(7));
                model.setBalanceAmount(c.getString(8));
                model.setPaymentMode(c.getString(9));
                model.setBankName(c.getString(10));
                model.setChequeNo(c.getString(11));
                model.setAccName(c.getString(12));
                model.setIFSC(c.getString(13));
                model.setAccNumber(c.getString(14));
                model.setBranchAddress(c.getString(15));
                result.getList().add(model);
            }
        }
        return result;
    }

    public PaymentModel getBuyerPaidReceiptListWithStock(PaymentModel result, String userId, String profileId) {

        Cursor c = db.rawQuery("SELECT vendorId,vendorName,SUM(creditors.paidAmount) as total FROM creditors WHERE userId='" + userId + "'AND profileId='" + profileId + "' GROUP By vendorId ORDER BY total DESC", null);
        ArrayList<String[]> val2 = null;
        // Checking if records found
        if (c.getCount() != 0) {
            result.setOutputDB(Const.SUCCESS);
            result.setList(new ArrayList<PaymentModel>());
            while (c.moveToNext()) {
                PaymentModel model = new PaymentModel();
                model.setVendorName(c.getString(1));
                model.setPaidAmount(c.getString(2));
                result.getList().add(model);
            }
            return result;
        } else {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("No record found");
            return result;
        }

    }

    public PaymentModel getBuyerOverPaidReceiptListWithStock(PaymentModel result, String userId, String profileId) {
        //Cursor c = db.rawQuery("SELECT * FROM product ORDER BY id DESC", null);
        Cursor c = db.rawQuery("SELECT * FROM creditors WHERE userId='" + userId + "' AND profileId='" + profileId + "' GROUP BY vendorId ORDER BY id DESC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("No record found");
            return result;
        }
        result.setOutputDB(Const.SUCCESS);
        result.setList(new ArrayList<PaymentModel>());
        while (c.moveToNext()) {
            float balanceAmt = Utility.parseFloat(c.getString(8));
            if(balanceAmt<0){
                PaymentModel model = new PaymentModel();
                model.setDebtorsId(c.getString(0));
                model.setUserId(c.getString(1));
                model.setProfileId(c.getString(2));
                model.setInvoiceNo(c.getString(3));
                model.setVendorId(c.getString(4));
                model.setVendorName(c.getString(5));
                model.setPaidAmount(c.getString(6));
                model.setPayableAmount(c.getString(7));
                model.setBalanceAmount(c.getString(8));
                model.setPaymentMode(c.getString(9));
                model.setBankName(c.getString(10));
                model.setChequeNo(c.getString(11));
                model.setAccName(c.getString(12));
                model.setIFSC(c.getString(13));
                model.setAccNumber(c.getString(14));
                model.setBranchAddress(c.getString(15));
                result.getList().add(model);
            }
        }
        return result;
    }

    public PaymentModel getSellPaidReceiptListWithStock(PaymentModel result, String userId, String profileId) {
        Cursor c = db.rawQuery("SELECT customerId,customerName,SUM(debtors.paidAmount) as total FROM debtors WHERE userId='" + userId + "'AND profileId='" + profileId + "' GROUP By customerId ORDER BY total DESC", null);
        ArrayList<String[]> val2 = null;
        // Checking if records found
        if (c.getCount() != 0) {
            result.setOutputDB(Const.SUCCESS);
            result.setList(new ArrayList<PaymentModel>());
            while (c.moveToNext()) {
                PaymentModel model = new PaymentModel();
                model.setCustomerName(c.getString(1));
                model.setPaidAmount(c.getString(2));
                result.getList().add(model);
            }
            return result;
        } else {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("No record found");
            return result;
        }
    }

    public PaymentModel getSellUnPaidReceiptListWithStock(PaymentModel result, String userId, String profileId) {
        //Cursor c = db.rawQuery("SELECT * FROM product ORDER BY id DESC", null);
        Cursor c = db.rawQuery("SELECT * FROM debtors WHERE userId='" + userId + "' AND profileId='" + profileId + "' GROUP BY customerId ORDER BY id DESC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("No record found");
            return result;
        }
        result.setOutputDB(Const.SUCCESS);
        result.setList(new ArrayList<PaymentModel>());
        while (c.moveToNext()) {
            float balanceAmt = Utility.parseFloat(c.getString(8));
            if(balanceAmt>0){
                PaymentModel model = new PaymentModel();
                model.setDebtorsId(c.getString(0));
                model.setUserId(c.getString(1));
                model.setProfileId(c.getString(2));
                model.setInvoiceNo(c.getString(3));
                model.setCustomerId(c.getString(4));
                model.setCustomerName(c.getString(5));
                model.setPaidAmount(c.getString(6));
                model.setPayableAmount(c.getString(7));
                model.setBalanceAmount(c.getString(8));
                model.setPaymentMode(c.getString(9));
                model.setBankName(c.getString(10));
                model.setChequeNo(c.getString(11));
                model.setAccName(c.getString(12));
                model.setIFSC(c.getString(13));
                model.setAccNumber(c.getString(14));
                model.setBranchAddress(c.getString(15));
                result.getList().add(model);
            }
        }
        return result;
    }

    public PaymentModel getSellOverPaidReceiptListWithStock(PaymentModel result, String userId, String profileId) {
        //Cursor c = db.rawQuery("SELECT * FROM product ORDER BY id DESC", null);
        Cursor c = db.rawQuery("SELECT * FROM debtors WHERE userId='" + userId + "' AND profileId='" + profileId + "' GROUP BY customerId ORDER BY id DESC", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            result.setOutputDB(Const.FAILURE);
            result.setOutputDBMsg("No record found");
            return result;
        }
        result.setOutputDB(Const.SUCCESS);
        result.setList(new ArrayList<PaymentModel>());
        while (c.moveToNext()) {
            float balanceAmt = Utility.parseFloat(c.getString(8));
            if(balanceAmt<0){
                PaymentModel model = new PaymentModel();
                model.setDebtorsId(c.getString(0));
                model.setUserId(c.getString(1));
                model.setProfileId(c.getString(2));
                model.setInvoiceNo(c.getString(3));
                model.setCustomerId(c.getString(4));
                model.setCustomerName(c.getString(5));
                model.setPaidAmount(c.getString(6));
                model.setPayableAmount(c.getString(7));
                model.setBalanceAmount(c.getString(8));
                model.setPaymentMode(c.getString(9));
                model.setBankName(c.getString(10));
                model.setChequeNo(c.getString(11));
                model.setAccName(c.getString(12));
                model.setIFSC(c.getString(13));
                model.setAccNumber(c.getString(14));
                model.setBranchAddress(c.getString(15));
                result.getList().add(model);
            }
        }
        return result;
    }

    public String getCompanyNameById(String userId, String companyId) {
        String query = "SELECT companyName from companyDetail WHERE id='" + companyId + "' AND userId='" + userId + "'";
        Cursor c = db.rawQuery(query, null);

        if (c != null && c.moveToFirst()) {
            return c.getString(0); //The 0 is the column index, we only have 1 column, so the index is 0
        }
        return "";
    }
    public String getCustomerEmailId(String userId, String customerId) {
        String query = "SELECT contactEmail from customer WHERE id='" + customerId + "' AND userId='" + userId + "'";
        Cursor c = db.rawQuery(query, null);

        if (c != null && c.moveToFirst()) {
            return c.getString(0); //The 0 is the column index, we only have 1 column, so the index is 0
        }
        return "";
    }


}

