package com.gennext.offlinegst.global;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.util.AppAnimation;


public class VersionUpdateDialog extends DialogFragment {
    private Dialog dialog;
    private String versionNameAndDate;
    private String whatsNew;
    private String verUpdateMessage;

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static VersionUpdateDialog newInstance(String verUpdateMessage, String versionNameAndDate, String whatsNew) {
        VersionUpdateDialog fragment=new VersionUpdateDialog();
        fragment.verUpdateMessage=verUpdateMessage;
        fragment.versionNameAndDate=versionNameAndDate;
        fragment.whatsNew=whatsNew;
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.alert_dialog_version, null);
        dialogBuilder.setView(v);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
        TextView tvVersion = (TextView) v.findViewById(R.id.tv_alert_dialog_detail2);
        TextView tvWhatsNew = (TextView) v.findViewById(R.id.tv_alert_dialog_detail3);

        tvTitle.setTextColor(ContextCompat.getColor(getContext(),R.color.colorPrimaryDark));
        tvTitle.setText(R.string.new_update_available);
        tvDescription.setText(verUpdateMessage);
        tvVersion.setText(versionNameAndDate);
        tvWhatsNew.setText(parseHtmlData(whatsNew));

        button1.setText(R.string.download_now);
        button2.setText(getString(R.string.remind_me_later));

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                Intent browserIntent1 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id="+getActivity().getPackageName()));
                startActivity(browserIntent1);

            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();

            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }

    private String parseHtmlData(String whatsNew) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(whatsNew,Html.FROM_HTML_MODE_COMPACT).toString();
        }else{
            return Html.fromHtml(whatsNew).toString();
        }
    }
}
