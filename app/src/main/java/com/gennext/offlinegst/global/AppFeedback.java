package com.gennext.offlinegst.global;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.gennext.offlinegst.MainActivity;
import com.gennext.offlinegst.R;
import com.gennext.offlinegst.model.app.FeedbackModel;
import com.gennext.offlinegst.util.ApiCallError;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.FieldValidation;
import com.gennext.offlinegst.util.JsonParser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


/**
 * Created by Abhijit on 14-Oct-16.
 */

public class AppFeedback extends CompactFragment implements ApiCallError.ErrorListener{
    private Button btnOK;
    private FragmentManager manager;
    AssignTask assignTask;
    private EditText etFeedback;
    private AppAnimation anim;
    private DatabaseReference mDatabase;

    @Override
    public void onAttach(Context activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static AppFeedback newInstance() {
        AppFeedback appFeedback=new AppFeedback();
        AppAnimation.setDialogAnimation(appFeedback);
        return appFeedback;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_app_feedback, container, false);
        manager = getFragmentManager();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        ((MainActivity) getActivity()).updateTitle(getString(R.string.app_name));
        InitUI(v);
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseDatabase.getInstance().goOnline();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        FirebaseDatabase.getInstance().goOffline();
    }

    private void InitUI(View v) {
        btnOK = (Button) v.findViewById(R.id.btn_popup);
        LinearLayout llClose = (LinearLayout) v.findViewById(R.id.ll_whitespace);
        etFeedback = (EditText) v.findViewById(R.id.et_alert_feedback);

        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeybord(getActivity());
                manager.popBackStack();
            }
        });

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!FieldValidation.isEmpty(getContext(),etFeedback)){
                    return;
                }
                hideKeybord(getActivity());
                sendFeedback(etFeedback.getText().toString());
            }
        });
    }

    private void sendFeedback(String feedback) {
        assignTask = new AssignTask(getActivity(), feedback);
        assignTask.execute(AppSettings.SEND_FEEDBACK);
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        sendFeedback( etFeedback.getText().toString());
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }



    private class AssignTask extends AsyncTask<String, Void, Boolean> {
        Context activity;
        private String feedback;
        private ProgressDialog progressDialog;

        public void onAttach(Context activity) {
            // TODO Auto-generated method stub
            this.activity = activity;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;
        }

        public AssignTask(Activity activity, String feedback) {
            this.feedback = feedback;
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (activity != null) {
                progressDialog = new ProgressDialog(activity);
                progressDialog.setMessage(getString(R.string.processing_please_wait));
                progressDialog.setIndeterminate(true);
                progressDialog.show();
            }

        }

        @Override
        protected Boolean doInBackground(String... urls) {
            String response;
            String userId = AppUser.getUserId(activity);
            String name = AppUser.getName(activity);
            return writeToDatabase(userId,name,feedback);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (activity != null) {
                progressDialog.dismiss();
                if (result != null) {
                    if (result) {
                        PopupAlert.newInstance(getString(R.string.feedback),getString(R.string.feedback_success_message),PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(),"popupAlert");
                    }  else {
                        PopupAlert.newInstance(getString(R.string.alert),getString(R.string.try_again_later),PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(),"popupAlert");
                    }
                } else {
                    ApiCallError.newInstance(JsonParser.ERRORMESSAGE,AppFeedback.this)
                            .show(getFragmentManager(),"apiCallError");

                }
            }
        }
    }

    private Boolean writeToDatabase(String userId,String name, String feedback) {
        FeedbackModel user = new FeedbackModel(name, feedback);
        mDatabase.child("feedback").child(userId).setValue(user);
        return true;
    }
}