package com.gennext.offlinegst.user.customer;

/**
 * Created by Admin on 5/22/2017.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.PopupAlert;
import com.gennext.offlinegst.model.app.Model;
import com.gennext.offlinegst.model.user.CommonModel;
import com.gennext.offlinegst.model.user.CustomerModel;
import com.gennext.offlinegst.model.user.StateModel;
import com.gennext.offlinegst.panel.PaymentTermsSelector;
import com.gennext.offlinegst.panel.StateSelector;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.user.invoice.DebtorsTransaction;
import com.gennext.offlinegst.user.invoice.InvoiceManager;
import com.gennext.offlinegst.user.services.sale.ServiceManager;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.ApiCallError;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.FieldValidation;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.ProgressButton;
import com.gennext.offlinegst.util.RequestBuilder;
import com.gennext.offlinegst.util.Utility;

import java.util.Map;


public class CustomerManager extends CompactFragment implements ApiCallError.ErrorParamListener
        , StateSelector.SelectListener, PaymentTermsSelector.SelectListener {

    public static final int VIEW_CUSTOMER = 0, ADD_CUSTOMER = 1, EDIT_CUSTOMER = 2, COPY_CUSTOMER = 3, ADD_CUSTOMER_WITH_RESPONSE = 4;
    private EditText etPersonName, etCompanyName, etEmail, etWorkPhone, etMobile, etDisplayName, etWebsite, etGSTIN, etDLNumber, etState, etCity, etCountry, etZip, etAddress, etFax, etCurrency, etPaymentTerms, etRemarks, etOpeningBalance;
    //    private CircularProgressButton btnAction;
    private DBManager dbManager;
    private AssignTask assignTask;
    private CoordinatorLayout coordinatorLayout;
    private CustomerModel itemCustomer;
    private int customerType;
    private String mSelectedStateCode, mSelectedTermCode;
    private Customer customer;
    private InvoiceManager invoiceManager;
    private DebtorsTransaction debtorsTransaction;
    private ProgressButton btnAction;
    private Map<String, String> defaultStateList;
    private ServiceManager serviceManager;


    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }


    public static Fragment newInstance(Customer customer) {
        CustomerManager fragment = new CustomerManager();
        AppAnimation.setSlideAnimation(fragment, Gravity.RIGHT);
        fragment.customerType = ADD_CUSTOMER;
        fragment.customer = customer;
        return fragment;
    }

    public static Fragment newInstance(Customer customer, CustomerModel itemCustomer, int customerType) {
        CustomerManager fragment = new CustomerManager();
        AppAnimation.setSlideAnimation(fragment, Gravity.BOTTOM);
        fragment.customer = customer;
        fragment.itemCustomer = itemCustomer;
        fragment.customerType = customerType;
        return fragment;
    }

    public static Fragment newInstance(InvoiceManager invoiceManager, CustomerModel itemCustomer, int customerType) {
        CustomerManager fragment = new CustomerManager();
        AppAnimation.setSlideAnimation(fragment, Gravity.BOTTOM);
        fragment.invoiceManager = invoiceManager;
        fragment.itemCustomer = itemCustomer;
        fragment.customerType = customerType;
        return fragment;
    }

    public static Fragment newInstance(DebtorsTransaction debtorsTransaction, CustomerModel itemCustomer, int customerType) {
        CustomerManager fragment = new CustomerManager();
        AppAnimation.setSlideAnimation(fragment, Gravity.BOTTOM);
        fragment.debtorsTransaction = debtorsTransaction;
        fragment.itemCustomer = itemCustomer;
        fragment.customerType = customerType;
        return fragment;
    }

    public static Fragment newInstance(ServiceManager serviceManager, CustomerModel itemCustomer, int customerType) {
        CustomerManager fragment = new CustomerManager();
        AppAnimation.setSlideAnimation(fragment, Gravity.BOTTOM);
        fragment.serviceManager = serviceManager;
        fragment.itemCustomer = itemCustomer;
        fragment.customerType = customerType;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.user_customer_add, container, false);
        initToolBar(getActivity(), v, getString(R.string.customer));
        screenAnalytics(getContext(), AppUser.getUserId(getContext()), "CustomerManager");
        dbManager = DBManager.newIsntance(getActivity());
        defaultStateList = AppUser.defaultStates(getContext());
        initUI(v);

        updateUi();
        return v;
    }


    private void updateUi() {
        if (itemCustomer != null) {
            etPersonName.setText(itemCustomer.getPersonName());
            etCompanyName.setText(itemCustomer.getCompanyName());
            etEmail.setText(itemCustomer.getContactEmail());
            etWorkPhone.setText(itemCustomer.getWorkPhone());
            etMobile.setText(itemCustomer.getMobile());
            etDisplayName.setText(itemCustomer.getContactDisplayName());
            etWebsite.setText(itemCustomer.getWebsite());
            etGSTIN.setText(itemCustomer.getGSTIN());
            etDLNumber.setText(itemCustomer.getDLNumber());
            etState.setText(itemCustomer.getState());
            etCity.setText(itemCustomer.getCity());
            etCountry.setText(itemCustomer.getCountry());
            etZip.setText(itemCustomer.getZip());
            etAddress.setText(itemCustomer.getAddress());
            etFax.setText(itemCustomer.getFax());
            etCurrency.setText(itemCustomer.getCurrency());
            etPaymentTerms.setText(itemCustomer.getPaymentTerms());
            etRemarks.setText(itemCustomer.getRemarks());
            etOpeningBalance.setText(itemCustomer.getOpeningBalance() != null ? itemCustomer.getOpeningBalance() : "0");

            mSelectedStateCode = itemCustomer.getSelectedStateCode();
            mSelectedTermCode = itemCustomer.getSelectedTermCode();
        }
        if (customerType == VIEW_CUSTOMER) {
            disableEditFields();
        }
        hideKeybord(getActivity());
    }

    private void initUI(View v) {
        coordinatorLayout = (CoordinatorLayout) v.findViewById(R.id.coordinatorLayout);
        etPersonName = (EditText) v.findViewById(R.id.et_customer_field_1);
        etCompanyName = (EditText) v.findViewById(R.id.et_customer_field_2);
        etEmail = (EditText) v.findViewById(R.id.et_customer_field_3);
        etWorkPhone = (EditText) v.findViewById(R.id.et_customer_field_4);
        etMobile = (EditText) v.findViewById(R.id.et_customer_field_5);
        etDisplayName = (EditText) v.findViewById(R.id.et_customer_field_6);
        etWebsite = (EditText) v.findViewById(R.id.et_customer_field_7);
        etGSTIN = (EditText) v.findViewById(R.id.et_customer_field_8);
        etDLNumber = (EditText) v.findViewById(R.id.et_customer_field_9);
        etState = (EditText) v.findViewById(R.id.et_customer_field_10);
        etCity = (EditText) v.findViewById(R.id.et_customer_field_11);
        etCountry = (EditText) v.findViewById(R.id.et_customer_field_12);
        etZip = (EditText) v.findViewById(R.id.et_customer_field_13);
        etAddress = (EditText) v.findViewById(R.id.et_customer_field_14);
        etFax = (EditText) v.findViewById(R.id.et_customer_field_15);
        etCurrency = (EditText) v.findViewById(R.id.et_customer_field_16);
        etPaymentTerms = (EditText) v.findViewById(R.id.et_customer_field_17);
        etRemarks = (EditText) v.findViewById(R.id.et_customer_field_18);
        etOpeningBalance = (EditText) v.findViewById(R.id.et_customer_field_19);

        etState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(customerType==VIEW_CUSTOMER){
                    return;
                }
                StateSelector.newInstance(CustomerManager.this).show(getFragmentManager(), "stateSelector");
            }
        });
        etPaymentTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(customerType==VIEW_CUSTOMER){
                    return;
                }
                PaymentTermsSelector.newInstance(CustomerManager.this).show(getFragmentManager(), "paymentTermsSelector");
            }
        });
        btnAction = ProgressButton.newInstance(getContext(), v);
        btnAction.setOnEditorActionListener(etRemarks);

        etDLNumber.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        etDLNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    // Handle pressing "Enter" key here
                    StateSelector.newInstance(CustomerManager.this).show(getFragmentManager(), "stateSelector");
                    handled = true;
                }
                return handled;
            }
        });
        etGSTIN.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setStateInUi(etGSTIN.getText().toString());
            }
        });


        etFax.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        etFax.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    // Handle pressing "Enter" key here
                    PaymentTermsSelector.newInstance(CustomerManager.this).show(getFragmentManager(), "paymentTermsSelector");
                    handled = true;
                }
                return handled;
            }
        });

        switch (customerType) {
            case VIEW_CUSTOMER:
                btnAction.setText(getString(R.string.edit));
                break;
            case ADD_CUSTOMER:
                btnAction.setText(getString(R.string.add));
                break;
            case EDIT_CUSTOMER:
                btnAction.setText(getString(R.string.update));
                break;
            case COPY_CUSTOMER:
                btnAction.setText(getString(R.string.copy));
                break;
            default:
                btnAction.setText(getString(R.string.add));
                break;
        }
        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnAction.getText().equals("Edit")) {
                    btnAction.setText("Update");
                    customerType = EDIT_CUSTOMER;
                    enableEditFields();
                    etPersonName.requestFocus();
                    return;
                }
                if (!FieldValidation.isEmpty(getContext(), etPersonName, getString(R.string.empty))) {
                    showToast(getContext(), getString(R.string.empty));
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), etCompanyName, getString(R.string.invalid_company_name))) {
                    showToast(getContext(), getString(R.string.invalid_company_name));
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), etMobile, getString(R.string.empty))) {
                    showToast(getContext(), getString(R.string.invalid_mobile_no));
                    return;
                } else if (!FieldValidation.isGSTIfAvailable(getContext(), etGSTIN, getString(R.string.invalid_gst_number), true)) {
                    showToast(getContext(), getString(R.string.invalid_gst_number));
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), etState, getString(R.string.invalid_state))) {
                    showToast(getContext(), getString(R.string.invalid_state));
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), etZip, getString(R.string.empty))) {
                    showToast(getContext(), getString(R.string.empty));
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), etAddress, getString(R.string.invalid_address))) {
                    showToast(getContext(), getString(R.string.invalid_address));
                    return;
                }
                hideKeybord(getActivity());
                executeTask(etPersonName.getText().toString(), etCompanyName.getText().toString(), etEmail.getText().toString()
                        , etWorkPhone.getText().toString(), etMobile.getText().toString(), etDisplayName.getText().toString()
                        , etWebsite.getText().toString(), etGSTIN.getText().toString(), etDLNumber.getText().toString()
                        , etState.getText().toString(), etCity.getText().toString(), etCountry.getText().toString()
                        , etZip.getText().toString(), etAddress.getText().toString(), etFax.getText().toString()
                        , etCurrency.getText().toString(), etPaymentTerms.getText().toString(), etRemarks.getText().toString(), mSelectedStateCode, mSelectedTermCode, etOpeningBalance.getText().toString());

            }
        });
    }

    private void enableEditFields() {
        enableEditText(etPersonName);
        enableEditText(etCompanyName);
        enableEditText(etEmail);
        enableEditText(etWorkPhone);
        enableEditText(etMobile);
        enableEditText(etDisplayName);
        enableEditText(etWebsite);
        enableEditText(etGSTIN);
        enableEditText(etDLNumber);
        enableEditText(etCity);
        enableEditText(etCountry);
        enableEditText(etZip);
        enableEditText(etAddress);
        enableEditText(etFax);
        enableEditText(etCurrency);
        enableEditText(etRemarks);
        enableEditText(etOpeningBalance);
    }

    private void disableEditFields() {
        disableEditText(etPersonName);
        disableEditText(etCompanyName);
        disableEditText(etEmail);
        disableEditText(etWorkPhone);
        disableEditText(etMobile);
        disableEditText(etDisplayName);
        disableEditText(etWebsite);
        disableEditText(etGSTIN);
        disableEditText(etDLNumber);
        disableEditText(etCity);
        disableEditText(etCountry);
        disableEditText(etZip);
        disableEditText(etAddress);
        disableEditText(etFax);
        disableEditText(etCurrency);
        disableEditText(etRemarks);
        disableEditText(etOpeningBalance);
    }


    private void setStateInUi(String gstinNo) {
        if (gstinNo.length() == 15) {
            String stateCode = gstinNo.substring(0, 2);
            int sCode = Utility.parseInt(stateCode);
            String stateName = AppUser.getStateName(defaultStateList, String.valueOf(sCode));
            if (!TextUtils.isEmpty(stateName)) {
                mSelectedStateCode = stateCode;
                etState.setText(stateName);
            } else {
                mSelectedStateCode = null;
                etState.setText(stateName);
            }
        } else {
            mSelectedStateCode = null;
            etState.setText("");
        }
    }

    private void executeTask(String personName, String companyName, String contactEmail, String workPhone, String mobile, String contactDisplayName,
                             String website, String GSTIN, String DLNumber, String state, String city, String country, String zip, String address,
                             String fax, String currency, String paymentTerms, String Remarks, String selectedStateCode, String selectedTermCode, String openingBalance) {
        hideKeybord(getActivity());
        assignTask = new AssignTask(getActivity(), valString(personName), valString(companyName), valString(contactEmail), valString(workPhone), valString(mobile), valString(contactDisplayName), valString(website), valString(GSTIN
        ), valString(DLNumber), valString(state), valString(city), valString(country), valString(zip), valString(address), valString(fax), valString(currency), valString(paymentTerms), valString(Remarks), selectedStateCode, selectedTermCode, openingBalance);
        assignTask.execute(AppSettings.ADD_CUSTOMER);
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog, String[] param) {
        executeTask(param[0], param[1], param[2], param[3], param[4], param[5], param[6], param[7], param[8], param[9], param[10], param[11], param[12]
                , param[13], param[14], param[15], param[16], param[17], param[18], param[19], param[20]);
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog, String[] param) {

    }

    @Override
    public void onStateSelect(DialogFragment dialog, StateModel selectedState) {
        mSelectedStateCode = selectedState.getStateCode();
        etState.setText(selectedState.getStateName());
        FieldValidation.isEmpty(getContext(), etState, getString(R.string.invalid_state));
        etCity.requestFocus();
        showKeybord(getActivity());
    }

    @Override
    public void onPayTermSelect(DialogFragment dialog, CommonModel selectedTerm) {
        mSelectedTermCode = selectedTerm.getId();
        etPaymentTerms.setText(selectedTerm.getName());
        etRemarks.requestFocus();
        showKeybord(getActivity());
    }

    private class AssignTask extends AsyncTask<String, Void, Model> {
        private final String personName, companyName, contactEmail, workPhone, mobile, contactDisplayName, website, GSTIN, DLNumber, state, city, country, zip, address, fax, currency, paymentTerms, remarks, selectedStateCode, selectedTermCode, openingBalance;
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context, String personName, String companyName, String contactEmail, String workPhone, String mobile, String contactDisplayName,
                          String website, String GSTIN, String DLNumber, String state, String city, String country, String zip, String address,
                          String fax, String currency, String paymentTerms, String remarks, String selectedStateCode, String selectedTermCode, String openingBalance) {
            this.personName = personName;
            this.companyName = companyName;
            this.contactEmail = contactEmail;
            this.workPhone = workPhone;
            this.mobile = mobile;
            this.contactDisplayName = contactDisplayName;
            this.website = website;
            this.GSTIN = GSTIN;
            this.DLNumber = DLNumber;
            this.state = state;
            this.city = city;
            this.country = country;
            this.zip = zip;
            this.address = address;
            this.fax = fax;
            this.currency = currency;
            this.paymentTerms = paymentTerms;
            this.remarks = remarks;
            this.context = context;
            this.selectedStateCode = selectedStateCode;
            this.selectedTermCode = selectedTermCode;
            this.openingBalance = openingBalance;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            btnAction.startAnimation();
        }

        @Override
        protected Model doInBackground(String... urls) {
            Model result = new Model();
            String userId = AppUser.getUserId(context);
            String setupId = AppUser.getCompanyId(context);
            if (AppConfig.isUpdateToServer(context)) {
                String response;
                switch (customerType) {
                    case ADD_CUSTOMER:
                        response = ApiCall.POST(urls[0], RequestBuilder.insertCustomer(userId, setupId, personName, companyName, contactEmail, workPhone, mobile, contactDisplayName, website, GSTIN
                                , DLNumber, state, city, country, zip, address, fax, currency, paymentTerms, remarks, selectedStateCode, selectedTermCode, openingBalance));
                        return JsonParser.defaultStaticParser(response);
                    case EDIT_CUSTOMER:
                        response = ApiCall.POST(urls[0], RequestBuilder.editCustomer(itemCustomer.getCustometId(), userId, setupId, personName, companyName, contactEmail, workPhone, mobile, contactDisplayName, website, GSTIN
                                , DLNumber, state, city, country, zip, address, fax, currency, paymentTerms, remarks, selectedStateCode, selectedTermCode, openingBalance));
                        return JsonParser.defaultStaticParser(response);
                    case COPY_CUSTOMER:
                        response = ApiCall.POST(urls[0], RequestBuilder.insertCustomer(userId, setupId, personName, companyName, contactEmail, workPhone, mobile, contactDisplayName, website, GSTIN
                                , DLNumber, state, city, country, zip, address, fax, currency, paymentTerms, remarks, selectedStateCode, selectedTermCode, openingBalance));
                        return JsonParser.defaultStaticParser(response);
                    default:
                        response = ApiCall.POST(urls[0], RequestBuilder.insertCustomer(userId, setupId, personName, companyName, contactEmail, workPhone, mobile, contactDisplayName, website, GSTIN
                                , DLNumber, state, city, country, zip, address, fax, currency, paymentTerms, remarks, selectedStateCode, selectedTermCode, openingBalance));
                        return JsonParser.defaultStaticParser(response);
                }
            }
            switch (customerType) {
                case ADD_CUSTOMER:
                    return dbManager.insertCustomer(result, userId, personName, companyName, contactEmail, workPhone, mobile, contactDisplayName, website, GSTIN
                            , DLNumber, state, city, country, zip, address, fax, currency, paymentTerms, remarks, selectedStateCode, selectedTermCode);
                case EDIT_CUSTOMER:
                    return dbManager.editCustomer(result, itemCustomer.getCustometId(), userId, personName, companyName, contactEmail, workPhone, mobile, contactDisplayName, website, GSTIN
                            , DLNumber, state, city, country, zip, address, fax, currency, paymentTerms, remarks, selectedStateCode, selectedTermCode);
                case COPY_CUSTOMER:
                    return dbManager.copyCustomer(result, itemCustomer.getCustometId(), userId, personName, companyName, contactEmail, workPhone, mobile, contactDisplayName, website, GSTIN
                            , DLNumber, state, city, country, zip, address, fax, currency, paymentTerms, remarks, selectedStateCode, selectedTermCode);
                default:
                    return dbManager.insertCustomer(result, userId, personName, companyName, contactEmail, workPhone, mobile, contactDisplayName, website, GSTIN
                            , DLNumber, state, city, country, zip, address, fax, currency, paymentTerms, remarks, selectedStateCode, selectedTermCode);
            }
        }


        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                if (!AppConfig.isUpdateToServer(context)) {
                    if (result.getOutputDB().equals(Const.SUCCESS)) {
                        showSnakBar(coordinatorLayout, result.getOutputDBMsg());
                        animateButtonAndRevert(personName, companyName);
                    } else if (result.getOutputDB().equals(Const.FAILURE)) {
                        btnAction.revertAnimation();
                        showSnakBar(coordinatorLayout, result.getOutputDBMsg());
                    }
                } else {
                    if (result.getOutput().equals(Const.SUCCESS)) {
                        showSnakBar(coordinatorLayout, result.getOutputMsg());
                        animateButtonAndRevert(personName, companyName);
                    } else if (result.getOutput().equals(Const.FAILURE)) {
                        btnAction.revertAnimation();
                        PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(), "popupAlert");
                    } else {
                        btnAction.revertAnimation();
                        String[] errorSoon = {personName, companyName, contactEmail, workPhone, mobile, contactDisplayName, website, GSTIN
                                , DLNumber, state, city, country, zip, address, fax, currency, paymentTerms, remarks, selectedStateCode, selectedTermCode, openingBalance};
                        ApiCallError.newInstance(result.getOutput(), result.getOutputMsg(), errorSoon, CustomerManager.this)
                                .show(getFragmentManager(), "apiCallError");
                    }
                }
            }
        }

        private void animateButtonAndRevert(final String personName, final String companyName) {
            Handler handler = new Handler();


            Runnable runnableRevert = new Runnable() {
                @Override
                public void run() {
                    if (context != null) {
                        if (customer != null) {
                            customer.refreshList();
                        }
                        if (customerType == ADD_CUSTOMER_WITH_RESPONSE) {
                            if (invoiceManager != null) {
                                invoiceManager.fetchVendorFromDB(personName, companyName);
                            }
                            if (debtorsTransaction != null) {
                                debtorsTransaction.fetchCustomerFromDB(personName, companyName);
                            }
                            if (serviceManager != null) {
                                serviceManager.fetchCustomerFromDB(personName, companyName);
                            }

                        }
                        getActivity().onBackPressed();
                    }
                }
            };

            btnAction.revertSuccessAnimation();
            handler.postDelayed(runnableRevert, Const.BUTTON_PROGRESS_TIME);
        }

    }


}

