package com.gennext.offlinegst.user.invoice;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.DatePickerDialog;
import com.gennext.offlinegst.global.PopupAlert;
import com.gennext.offlinegst.global.PopupDialog;
import com.gennext.offlinegst.model.app.Model;
import com.gennext.offlinegst.model.user.CommonModel;
import com.gennext.offlinegst.model.user.CustomerModel;
import com.gennext.offlinegst.model.user.InvoiceModel;
import com.gennext.offlinegst.model.user.InvoiceProductAdapter;
import com.gennext.offlinegst.model.user.ProductModel;
import com.gennext.offlinegst.model.user.StateModel;
import com.gennext.offlinegst.panel.BarcodeScanner;
import com.gennext.offlinegst.panel.CustomerSelector;
import com.gennext.offlinegst.panel.InvoiceTypeSelector;
import com.gennext.offlinegst.panel.PaymentTermsSelector;
import com.gennext.offlinegst.panel.ProductQunatityDialog;
import com.gennext.offlinegst.panel.ProductSelector;
import com.gennext.offlinegst.panel.ShippingDetailDialog;
import com.gennext.offlinegst.panel.StateSelector;
import com.gennext.offlinegst.panel.TransportSelector;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.user.product.AddProduct;
import com.gennext.offlinegst.user.product.EditInvoiceProduct;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.ApiCallError;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.DateTimeUtility;
import com.gennext.offlinegst.util.FieldValidation;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.ProgressButton;
import com.gennext.offlinegst.util.RequestBuilder;
import com.gennext.offlinegst.util.Utility;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Admin on 7/4/2017.
 */

public class InvoiceManager extends CompactFragment implements View.OnClickListener
        , CustomerSelector.SelectListener, ProductSelector.SelectListener, PopupDialog.DialogTaskListener
        , DatePickerDialog.DateSelectFlagListener, StateSelector.SelectListener
        , TransportSelector.SelectListener, ShippingDetailDialog.ShipDetail
        , BarcodeScanner.Barcodelistener, ProductQunatityDialog.QunatityListener
        , InvoiceTypeSelector.SelectListener, PaymentTermsSelector.SelectListener
        , ApiCallError.ErrorListener {

    private static final int REQUEST_PRODUCT_NOT_FOUND = 1;
    public static final int EDIT_INVOICE = 1, COPY_INVOICE = 2, ADD_INVOICE = 3;
    private static final String TYPE_NO = "no", TYPE_YES = "yes";
    private static final String REVERCE_CHARGE = "N";
    //    private FloatingActionButton addProduct;
    private RecyclerView lvMain;
    private EditText etInvoiceNo, etVehicel, etPortCode, etInvoiceOperator;
    private LinearLayout llPortCode, llInvoiceOperator;
    private TextView tvDate, tvCustomer, tvPlaceOfSupply, tvTranspoartMode, tvDateOfSupply, tvInvoiceType, tvInvTerm;
    private String mCustomerId, mCustomerName;
    private InvoiceProductAdapter adapter;
    private ArrayList<ProductModel> cList;
    private int sDay, sMonth, sYear;
    private int eDay, eMonth, eYear;
    private TextView tvAddNote;
//    private FloatingActionsMenu multiMenu;

    public static final String BARCODE_KEY = "BARCODE";

    //    private Barcode barcodeResult;
    private String mSearchBarcode;
    private DBManager dbManager;
    private CoordinatorLayout coardLayout;
    private ProductModel mSearchBarcodeProduct;
    //    private MaterialBarcodeScanner.OnResultListener listener;
    private TextView tvTitle, tvQuantity, tvAmount, tvTotalAmount;
    private LinearLayout llTotalAmount;
    private float mTotalQuan = 0, mTotalAmt = 0;
    private ProgressButton btnAction;

    private InvoiceModel itemInvoice;
    private int sellingType;
    private ArrayList<String> deletedProduct;
    private String mPlaceofSupply, mPlaceofSupplyId;
    private String mTranspoartModeId = "", mTranspoartMode = "";
    private String mDateOfSupply;
    private String mShipName = "", mShipGSTIN = "", mShipAddress = "";
    private Button btnChangeShipping;
    private Invoices invoices;
    private String mInvTypeId, mInvTypeName;
    private String mInvTermId, mInvTermName;
    private ArrayList<ProductModel> mProductList;
    private String mECommerceSale;
    private RadioButton rbTypeYes, rbTypeNo;
    private String mFreightAmt;
    private String mCustomerStateCode;
    private AssignTask assignTask;
    private String mPreviousDate;
    private NestedScrollView nsView;


    //    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setRetainInstance(true);
//        if (savedInstanceState != null) {
//            Barcode restoredBarcode = savedInstanceState.getParcelable(BARCODE_KEY);
//            if (restoredBarcode != null) {
//                barcodeResult = restoredBarcode;
//            }
//        }
//    }
//
//    @Override
//    public void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        outState.putParcelable(BARCODE_KEY, barcodeResult);
//        super.onSaveInstanceState(outState);
//    }
    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static Fragment newInstance(Invoices invoices, InvoiceModel itemInvoice, int sellingType) {
        InvoiceManager fragment = new InvoiceManager();
        AppAnimation.setFadeAnimation(fragment);
        fragment.invoices = invoices;
        fragment.itemInvoice = itemInvoice;
        fragment.sellingType = sellingType;
        return fragment;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (dbManager != null)
            dbManager.closeDB();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_invoice_manager, container, false);
        initToolBar(getActivity(), v, getString(R.string.sale_goods));
        screenAnalytics(getContext(), AppUser.getUserId(getContext()), "InvoiceManager");
        dbManager = DBManager.newIsntance(getActivity());
        initUi(v);
        updateUi();
        return v;
    }

    private void updateUi() {
        if (sellingType == ADD_INVOICE || sellingType == COPY_INVOICE) {
            setInvoiceNo();
//            String invoiceNumber = dbManager.getSellInvoiceNumber(userId, profileId);
//            if (invoiceNumber.equals("")) {
//                invoiceNumber = AppConfig.getSellInvoiceNumber(getContext(), userId, profileId, companyId);
//            } else {
//                invoiceNumber = AppConfig.generateSellInvoiceWithExistingNumber(getContext(), userId, profileId, companyId, invoiceNumber);
//            }
//            etInvoiceNo.setText(invoiceNumber);
            initDate();
        } else {
            if (itemInvoice != null) {
                etInvoiceNo.setText(itemInvoice.getInvoiceNumber());
                tvDate.setText(itemInvoice.getInvoiceDate());
            }
        }
        if (itemInvoice != null) {
            mCustomerId = itemInvoice.getCustomerId();
            mCustomerName = itemInvoice.getCustomerName();
            tvCustomer.setText(itemInvoice.getCustomerName());
            mFreightAmt = itemInvoice.getFreightAmount();

//            mInvTypeId = itemInvoice.getInvTypeId();
//            mInvTypeName = itemInvoice.getInvTypeName();
//            tvInvoiceType.setText(mInvTypeName);

            mInvTypeId = "1";
            mInvTypeName = "Regular";
            tvInvoiceType.setText(mInvTypeName);


            mInvTermId = itemInvoice.getInvTermId();
            mInvTermName = itemInvoice.getInvTermName();
            tvInvTerm.setText(mInvTermName);

            etInvoiceOperator.setText(itemInvoice.getInvOperatorGSTIN());
            if (itemInvoice.getInvOperatorGSTIN().equals("")) {
                mECommerceSale = TYPE_NO;
                rbTypeNo.setChecked(true);
            } else {
                mECommerceSale = TYPE_YES;
                rbTypeYes.setChecked(true);
                llInvoiceOperator.setVisibility(View.VISIBLE);
            }
            etPortCode.setText(itemInvoice.getPortCode());
            etVehicel.setText(itemInvoice.getVehicleNumber());
            tvTranspoartMode.setText(itemInvoice.getTranspoartMode());
            mTranspoartMode = itemInvoice.getTranspoartMode();
            mTranspoartModeId = itemInvoice.getTranspoartModeId();
            tvDateOfSupply.setText(itemInvoice.getDateOfSupply());
            mDateOfSupply = itemInvoice.getDateOfSupply();

            mShipName = itemInvoice.getShipName();
            mShipGSTIN = itemInvoice.getShipGSTIN();
            mShipAddress = itemInvoice.getShipAddress();
            tvPlaceOfSupply.setText(itemInvoice.getPlaceofSupply());
            mPlaceofSupply = itemInvoice.getPlaceofSupply();
            mPlaceofSupplyId = itemInvoice.getPlaceofSupplyId();

            addListItemByArrayList(itemInvoice.getProductList());
            deletedProduct = new ArrayList<>();
        }
    }


    @Override
    public void onBarcodeDetected(String barcode) {
//        barcodeResult = barcode;
        mSearchBarcode = barcode;
        fetchFromDB(mSearchBarcode);
        if (mSearchBarcodeProduct != null) {
            AddProduct addProduct = (AddProduct) getFragmentManager().findFragmentByTag("addProduct");
            if (addProduct == null) {
                PopupDialog dialog = (PopupDialog) getFragmentManager().findFragmentByTag("popupDialog");
                if (dialog == null) {
                    PopupDialog.newInstance(getString(R.string.product_not_found), getString(R.string.barcode_new_scan_found), REQUEST_PRODUCT_NOT_FOUND, InvoiceManager.this)
                            .show(getFragmentManager(), "popupDialog");
                }
            }
        }
    }


    private void initDate() {
        final Calendar calendar = Calendar.getInstance();
        eYear = sYear = calendar.get(Calendar.YEAR);
        eMonth = sMonth = calendar.get(Calendar.MONTH);
        eDay = sDay = calendar.get(Calendar.DAY_OF_MONTH);
        tvDate.setText(DateTimeUtility.cDateDDMMMYY(sDay, sMonth, sYear));

        mDateOfSupply = DateTimeUtility.cDateDDMMMYY(sDay, sMonth, sYear);
        tvDateOfSupply.setText(mDateOfSupply);
    }

    private void initUi(View v) {
        coardLayout = (CoordinatorLayout) v.findViewById(R.id.coordinatorLayout);
        btnAction = ProgressButton.newInstance(getContext(), v);
        btnChangeShipping = (Button) v.findViewById(R.id.btn_change_shipping_detail);
        lvMain = (RecyclerView) v.findViewById(R.id.lv_main);

        LinearLayout llDate = (LinearLayout) v.findViewById(R.id.ll_invoice_date);
        LinearLayout llCustomer = (LinearLayout) v.findViewById(R.id.ll_invoice_customer);
        LinearLayout llPlaceOfSupply = (LinearLayout) v.findViewById(R.id.ll_invoice_plase_of_supply);
        LinearLayout llTranspoartMode = (LinearLayout) v.findViewById(R.id.ll_invoice_transpoart_mode);
        LinearLayout llDateOfSupply = (LinearLayout) v.findViewById(R.id.ll_invoice_date_of_supply);
        LinearLayout llInvoiceType = (LinearLayout) v.findViewById(R.id.ll_invoice_type);
        llInvoiceOperator = (LinearLayout) v.findViewById(R.id.ll_invoice_operator);
        llPortCode = (LinearLayout) v.findViewById(R.id.ll_invoice_port_code);
        LinearLayout llInvTerm = (LinearLayout) v.findViewById(R.id.ll_invoice_terms);
        etInvoiceNo = (EditText) v.findViewById(R.id.et_invoice_no);
        etVehicel = (EditText) v.findViewById(R.id.et_invoice_vehicel);
        etPortCode = (EditText) v.findViewById(R.id.et_invoice_port_code);
        etInvoiceOperator = (EditText) v.findViewById(R.id.et_invoice_operator);
        tvDate = (TextView) v.findViewById(R.id.tv_invoice_date);
        tvAddNote = (TextView) v.findViewById(R.id.tv_add_note);
        tvCustomer = (TextView) v.findViewById(R.id.tv_invoice_customer);
        tvPlaceOfSupply = (TextView) v.findViewById(R.id.tv_invoice_plase_of_supply);
        tvTranspoartMode = (TextView) v.findViewById(R.id.tv_invoice_transpoart_mode);
        tvDateOfSupply = (TextView) v.findViewById(R.id.tv_invoice_date_of_supply);
        tvInvoiceType = (TextView) v.findViewById(R.id.tv_invoice_type);
        tvInvTerm = (TextView) v.findViewById(R.id.tv_invoice_terms);
        FloatingActionButton fabAdd = (FloatingActionButton) v.findViewById(R.id.fab);
        nsView = (NestedScrollView) v.findViewById(R.id.nested_scroll);

        llTotalAmount = (LinearLayout) v.findViewById(R.id.layoutSlot);
        tvTitle = (TextView) v.findViewById(R.id.tv_slot_1);
        tvQuantity = (TextView) v.findViewById(R.id.tv_slot_2);
        tvAmount = (TextView) v.findViewById(R.id.tv_slot_3);
        tvTotalAmount = (TextView) v.findViewById(R.id.tv_slot_4);

//        multiMenu = (FloatingActionsMenu) v.findViewById(R.id.multiple_actions);
//        final FloatingActionButton btnBarcode = (FloatingActionButton) v.findViewById(R.id.fab_barcode);
//        final FloatingActionButton btnDirectory = (FloatingActionButton) v.findViewById(R.id.fab_directory);

        llDate.setOnClickListener(this);
        llCustomer.setOnClickListener(this);
        llPlaceOfSupply.setOnClickListener(this);
        llTranspoartMode.setOnClickListener(this);
        llDateOfSupply.setOnClickListener(this);
        llInvoiceType.setOnClickListener(this);
        llInvTerm.setOnClickListener(this);
//        btnBarcode.setOnClickListener(this);
//        btnDirectory.setOnClickListener(this);
        btnAction.setOnClickListener(this);
        btnChangeShipping.setOnClickListener(this);
        fabAdd.setOnClickListener(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(linearLayoutManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());

        boolean isCompositeScheme = AppUser.getCompositionScheme(getContext());
        cList = new ArrayList<>();
        adapter = new InvoiceProductAdapter(getActivity(), cList, InvoiceManager.this,isCompositeScheme);
        lvMain.setAdapter(adapter);

        RadioGroup rgPaymentType = (RadioGroup) v.findViewById(R.id.rg_sale);
        rbTypeYes = (RadioButton) v.findViewById(R.id.rb_yes);
        rbTypeNo = (RadioButton) v.findViewById(R.id.rb_no);
        mECommerceSale = TYPE_NO;
        rgPaymentType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.rb_no:
                        mECommerceSale = TYPE_NO;
                        llInvoiceOperator.setVisibility(View.GONE);
                        break;
                    case R.id.rb_yes:
                        mECommerceSale = TYPE_YES;
                        llInvoiceOperator.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });

        mInvTypeId = "1";
        mInvTypeName = "Regular";
        tvInvoiceType.setText(mInvTypeName);
    }


    private void paymentTask(String invoiceNumber, String invoiceDate, String customerId, String customerName, String freightAmount
            , String transpoartMode, String transpoartModeId, String vehicleNumber, String dateOfSupply, String placeofSupply
            , String placeofSupplyId, String shipName, String shipGSTIN, String shipAddress, String totalPrice, String invTypeId, String invTypeName, String portCode, String invTermId, String invTermName, String reverceCharge, String operatorGstin, String customerStateCode, ArrayList<ProductModel> productList) {

        String[] invoiceDetail = {invoiceNumber, invoiceDate, customerId, customerName, freightAmount, transpoartMode, transpoartModeId
                , vehicleNumber, dateOfSupply, placeofSupply, placeofSupplyId, shipName, shipGSTIN, shipAddress, totalPrice, invTypeId, invTypeName, portCode, invTermId, invTermName, reverceCharge, operatorGstin};

        addFragment(PaymentDetails.newInstance(invoices, sellingType, itemInvoice, deletedProduct, invoiceDetail, customerStateCode, productList), "paymentDetails");
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_action:
                if (!FieldValidation.isEmpty(getContext(), etInvoiceNo)) {
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), mInvTypeId, getString(R.string.please_select_invoice_type), coardLayout)) {
                    return;
                } else if (llPortCode.getVisibility() == View.VISIBLE && !FieldValidation.isEmpty(getContext(), etPortCode, getString(R.string.invalid_port_code))) {
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), mCustomerId, getString(R.string.please_select_customer), coardLayout)) {
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), mPlaceofSupplyId, getString(R.string.please_select_place_of_supply), coardLayout)) {
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), mDateOfSupply, getString(R.string.please_select_date_of_supply), coardLayout)) {
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), mInvTermId, getString(R.string.please_select_terms), coardLayout)) {
                    return;
                } else if (llInvoiceOperator.getVisibility() == View.VISIBLE && !FieldValidation.isEmpty(getContext(), etInvoiceOperator, getString(R.string.invalid_operator_gstin_message))) {
                    return;
                } else if (cList == null || cList.size() == 0) {
                    showSnakBar(coardLayout, getString(R.string.please_add_product));
                    return;
                }
                hideKeybord(getActivity());
                paymentTask(etInvoiceNo.getText().toString(), tvDate.getText().toString(), mCustomerId, mCustomerName
                        , mFreightAmt, mTranspoartMode, mTranspoartModeId, etVehicel.getText().toString()
                        , mDateOfSupply, tvPlaceOfSupply.getText().toString(), mPlaceofSupplyId, mShipName, mShipGSTIN
                        , mShipAddress, String.valueOf(mTotalAmt), mInvTypeId, mInvTypeName, etPortCode.getText().toString()
                        , mInvTermId, mInvTermName, REVERCE_CHARGE, etInvoiceOperator.getText().toString(), mCustomerStateCode
                        , cList);

                break;
//            case R.id.fab_barcode:
//                startScan();
//                multiMenu.collapse();
//                break;
//            case R.id.fab_directory:
//                ProductSelector.newInstance(InvoiceManager.this, mProductList)
//                        .show(getFragmentManager(), "productSelector");
//                multiMenu.collapse();
//                break;
            case R.id.fab:
                ProductSelector.newInstance(InvoiceManager.this, mProductList)
                        .show(getFragmentManager(), "productSelector");
                break;
            case R.id.ll_invoice_date:
                hideKeybord(getActivity());
                DatePickerDialog.newInstance(InvoiceManager.this, true, true, DateTimeUtility.getTimeInMillis(mPreviousDate), DatePickerDialog.START_DATE, sDay, sMonth, sYear)
                        .show(getFragmentManager(), "datePickerDialog");
                break;
            case R.id.ll_invoice_customer:
                hideKeybord(getActivity());
                CustomerSelector.newInstance(InvoiceManager.this)
                        .show(getFragmentManager(), "customerSelector");
                break;
            case R.id.ll_invoice_plase_of_supply:
//                hideKeybord(getActivity());
//                StateSelector.newInstance(InvoiceManager.this).show(getFragmentManager(), "stateSelector");
                break;
            case R.id.ll_invoice_transpoart_mode:
                hideKeybord(getActivity());
                TransportSelector.newInstance(InvoiceManager.this).show(getFragmentManager(), "transportSelector");
                break;
            case R.id.ll_invoice_date_of_supply:
                hideKeybord(getActivity());
                DatePickerDialog.newInstance(InvoiceManager.this, false, DatePickerDialog.END_DATE, eDay, eMonth, eYear)
                        .show(getFragmentManager(), "datePickerDialog");
                break;
            case R.id.btn_change_shipping_detail:
                hideKeybord(getActivity());
                ShippingDetailDialog.newInstance(InvoiceManager.this, mShipName, mShipGSTIN, mShipAddress, mPlaceofSupplyId, mPlaceofSupply)
                        .show(getFragmentManager(), "shippingDetailDialog");
                break;
            case R.id.ll_invoice_type:
//                hideKeybord(getActivity());
//                InvoiceTypeSelector.newInstance(InvoiceManager.this)
//                        .show(getFragmentManager(), "invoiceTypeSelector");
                break;
            case R.id.ll_invoice_terms:
                hideKeybord(getActivity());
                PaymentTermsSelector.newInstance(InvoiceManager.this)
                        .show(getFragmentManager(), "paymentTermsSelector");
                break;


        }
    }


    public void startScan() {
        /**
         * Build a new MaterialBarcodeScanner
         */
        PermissionListener onPermissionListener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                addFragment(BarcodeScanner.newInstance(InvoiceManager.this), "barcodeScanner");
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(getContext(), "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }
        };
        new TedPermission(getActivity())
                .setPermissionListener(onPermissionListener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.CAMERA)
                .check();
//        MaterialBarcodeScanner materialBarcodeScanner = new MaterialBarcodeScannerBuilder()
//                .withActivity(getActivity())
//                .withEnableAutoFocus(true)
//                .withBleepEnabled(true)
//                .withBackfacingCamera()
//                .withCenterTracker()
//                .withText("Product Scanning...")
//                .withResultListener(listener)
//                .build();
//
//        materialBarcodeScanner.startScan();
    }

    public void fetchFromDB(final String mSearchBarcode) {
        ProductModel selectedProduct = dbManager.filterProductByBarcode(AppUser.getUserId(getActivity()), mSearchBarcode);
        if (selectedProduct.getOutputDB().equals(Const.SUCCESS)) {
            mSearchBarcodeProduct = null;
            tvAddNote.setVisibility(View.GONE);
            boolean isQunatityIncreased = false;
            ArrayList<ProductModel> tempList = selectedProduct.getList();
            for (ProductModel model : tempList) {
                if (model.getQuantity() == null) {
                    model.setQuantity("1");
                    model.setInitQuantity(1);

//                    cList.add(model);
//                    adapter.notifyDataSetChanged();
//                    lvMain.smoothScrollToPosition(adapter.getItemCount());
//                    addTotalItemOnFooter();
//                    return;
                }
                for (int i = 0; i < cList.size(); i++) {
                    if (model.getProductId().equalsIgnoreCase(cList.get(i).getProductId())) {
                        isQunatityIncreased = true;
                        ProductModel proModel = cList.get(i);
                        float res = parseFloat(proModel.getQuantity()) + parseFloat(model.getQuantity());
                        proModel.setQuantity(String.valueOf(res));
                        proModel.setInitQuantity(res);
                        cList.set(i, proModel);
                    }
                }
                if (!isQunatityIncreased) {
                    cList.add(model);
                }
            }
            adapter.notifyDataSetChanged();
            lvMain.smoothScrollToPosition(adapter.getItemCount());
            addTotalItemOnFooter();
        } else {
            this.mSearchBarcodeProduct = new ProductModel();
            this.mSearchBarcodeProduct.setBarcode(mSearchBarcode);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onCustomerSelect(DialogFragment dialog, CustomerModel cust) {
        mCustomerId = cust.getCustometId();
        mCustomerName = cust.getPersonName();
        tvCustomer.setText(cust.getPersonName());
        mShipName = cust.getPersonName();
        mShipGSTIN = cust.getGSTIN();
        mShipAddress = cust.getAddress();
        mCustomerStateCode = cust.getSelectedStateCode();
        mPlaceofSupplyId = cust.getSelectedStateCode();
        mPlaceofSupply = cust.getState();
        tvPlaceOfSupply.setText(cust.getState());

        mInvTermId = cust.getSelectedTermCode();
        mInvTermName = cust.getPaymentTerms();
        tvInvTerm.setText(mInvTermName);
    }


    private void addListItemByModel(ProductModel selectedProduct) {
        if (selectedProduct.getQuantity() == null) {
            selectedProduct.setQuantity("1");
            selectedProduct.setInitQuantity(1);
        }
        if (!isProductExistsInList(selectedProduct)) {
            cList.add(selectedProduct);
        }
        adapter.notifyDataSetChanged();
//        lvMain.smoothScrollToPosition(adapter.getItemCount());
        addTotalItemOnFooter();
    }

    private void updateListItemByModel(ProductModel selectedProduct) {
        if (selectedProduct.getQuantity() == null) {
            selectedProduct.setQuantity("1");
            selectedProduct.setInitQuantity(1);
        }
        if (!isProductExistsInList(selectedProduct)) {
            cList.add(selectedProduct);
        }
        adapter.notifyDataSetChanged();
        lvMain.smoothScrollToPosition(adapter.getItemCount());

        addTotalItemOnFooter();
    }

    private boolean isProductExistsInList(ProductModel selectedProduct) {
        for (ProductModel model:cList) {
            if (selectedProduct.getProductId().equalsIgnoreCase(model.getProductId())) {
                return true;
            }
        }
        return false;
    }

    private void addListItemByArrayList(ArrayList<ProductModel> prodList) {
        if (prodList != null) {
            cList.addAll(prodList);
        }
        adapter.notifyDataSetChanged();
        lvMain.smoothScrollToPosition(adapter.getItemCount());

        addTotalItemOnFooter();
    }

    public void refreshAdapter() {
        adapter.notifyDataSetChanged();
        lvMain.smoothScrollToPosition(adapter.getItemCount());
        addTotalItemOnFooter();
    }

    private void addTotalItemOnFooter() {
        float totalQuan = 0, taxableAmount = 0, totalNetAmt = 0;
        for (int i = 0; i < cList.size(); i++) {
            float qun = parseFloat(cList.get(i).getQuantity());
            float sale = parseFloat(cList.get(i).getSalePrice());
            float dis = parseFloat(cList.get(i).getDis());
            float cessRate = parseFloat(cList.get(i).getCessRate());
            float taxRate = parseFloat(cList.get(i).getTaxRate());

            float taxable = (sale * qun) - dis;
            float totalTax = taxable + ((taxable * (taxRate + cessRate)) / 100);

            totalQuan += qun;
            taxableAmount += taxable;
            totalNetAmt += totalTax;
        }
        addTotalItemOnUI(totalQuan, taxableAmount, totalNetAmt);
    }


    private void addTotalItemOnUI(float quantity, float taxableAmount, float totalNetAmt) {
        llTotalAmount.setVisibility(View.VISIBLE);
        tvTitle.setText("Total");
        mTotalAmt = taxableAmount;
        mTotalQuan = quantity;
        tvQuantity.setText(String.valueOf(quantity));
        tvAmount.setText(Utility.decimalFormat(taxableAmount, "#.##"));
        tvTotalAmount.setText(Utility.decimalFormat(totalNetAmt, "#.##"));
        nsView.fullScroll(View.FOCUS_DOWN);
    }

    private void removeTotalItemFromUI() {
        llTotalAmount.setVisibility(View.GONE);
        tvTitle.setText("Total");
        tvQuantity.setText("0");
        tvAmount.setText("0");
        tvTotalAmount.setText("0");
    }

    public void editProduct(ProductModel itemProduct, int position) {
        addFragment(EditInvoiceProduct.newInstance(itemProduct, InvoiceManager.this), "editInvoiceProduct");
//        ChangePriceDialog.newInstance(InvoiceManager.this, ChangePriceDialog.SALE_ITEM, itemProduct, position).show(getFragmentManager(), "changePriceDialog");
    }

    public void onProductEdited(ProductModel itemProduct) {
        updateListItemByModel(itemProduct);
    }

    public void deleteProduct(ProductModel itemProduct, int position) {
        if (deletedProduct != null) {
            deletedProduct.add(itemProduct.getProductId());
        }
        if (cList.size() == 0) {
            tvAddNote.setVisibility(View.VISIBLE);
            removeTotalItemFromUI();
            return;
        }
        addTotalItemOnFooter();
    }


    public void quantityProduct(ProductModel itemProduct, int position) {
        ProductQunatityDialog.newInstance(InvoiceManager.this, itemProduct, position).show(getFragmentManager(), "productQunatityDialog");
    }

    @Override
    public void onQunatityChange(DialogFragment dialog, ProductModel itemProduct, int position) {
        updateListItemByModel(itemProduct);
    }

    @Override
    public void onStartDateSelected(DialogFragment dialog, int day, int month, int year, String ddMMMyy) {
        sDay = day;
        sMonth = month;
        sYear = year;
        tvDate.setText(ddMMMyy);
    }

    @Override
    public void onEndDateSelected(DialogFragment dialog, int day, int month, int year, String ddMMMyy) {
        eDay = day;
        eMonth = month;
        eYear = year;
        mDateOfSupply = ddMMMyy;
        tvDateOfSupply.setText(ddMMMyy);
    }


    @Override
    public void onDialogOkClick(DialogFragment dialog, int task) {
        switch (task) {
            case REQUEST_PRODUCT_NOT_FOUND:
                addFragment(AddProduct.newInstance(mSearchBarcodeProduct, AddProduct.ADD_PRODUCT_WITH_RESPONSE, InvoiceManager.this), "addProduct");
                break;
        }
    }

    @Override
    public void onDialogCancelClick(DialogFragment dialog, int task) {
        mSearchBarcodeProduct = null;
    }

    public void fetchVendorFromDB(String personName, String companyName) {
//        CustomerModel customerModel = dbManager.getCustomerIdByCustomerAndCompanyName(AppUser.getUserId(getActivity()), personName, companyName);
//        if (customerModel.getOutputDB().equals(Const.SUCCESS)) {
//            mCustomerId = customerModel.getCustometId();
//            mCustomerName = customerModel.getPersonName();
//            tvCustomer.setText(customerModel.getPersonName());
//            mShipName = customerModel.getPersonName();
//            mShipGSTIN = customerModel.getGSTIN();
//            mShipAddress = customerModel.getAddress();
//        } else {
//            showSnakBar(coardLayout, customerModel.getOutputDBMsg());
//        }
    }


    @Override
    public void onStateSelect(DialogFragment dialog, StateModel selectedState) {
        mPlaceofSupplyId = selectedState.getStateCode();
        mPlaceofSupply = selectedState.getStateName();
        tvPlaceOfSupply.setText(selectedState.getStateName());
    }

    @Override
    public void onTranspoartSelect(DialogFragment dialog, CommonModel selectedItem) {
        mTranspoartModeId = selectedItem.getId();
        mTranspoartMode = selectedItem.getName();
        tvTranspoartMode.setText(selectedItem.getName());
    }


    @Override
    public void onUpdateShippingDetailClick(DialogFragment dialog, String name, String GSTIN, String address, String shipStateCode, String shipStateName) {
        this.mShipName = name;
        this.mShipGSTIN = GSTIN;
        this.mShipAddress = address;
        this.mPlaceofSupplyId = shipStateCode;
        this.mPlaceofSupply = shipStateName;
        tvPlaceOfSupply.setText(shipStateName);
    }

    @Override
    public void onInvoiceTypeSelect(DialogFragment dialog, CommonModel selectedInvType) {
        mInvTypeId = selectedInvType.getId();
        mInvTypeName = selectedInvType.getName();
        tvInvoiceType.setText(mInvTypeName);
        if (mInvTypeId.equals("2") || mInvTypeId.equals("3")) {
            llPortCode.setVisibility(View.VISIBLE);
        } else {
            llPortCode.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPayTermSelect(DialogFragment dialog, CommonModel selectedTerm) {
        mInvTermId = selectedTerm.getId();
        mInvTermName = selectedTerm.getName();
        tvInvTerm.setText(mInvTermName);
    }

    @Override
    public void onProductSelect(DialogFragment dialog, ProductModel selectedProduct, ArrayList<ProductModel> productList) {
        mSearchBarcodeProduct = null;
        mProductList = productList;
        tvAddNote.setVisibility(View.GONE);
        addListItemByModel(selectedProduct);
    }

    public void fetchFromServer(String barcode) {
        mProductList = null;
        ProductSelector.newInstance(InvoiceManager.this, mProductList)
                .show(getFragmentManager(), "productSelector");
    }

    private void setInvoiceNo() {
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.GENERATE_INVOICE_NO);
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        setInvoiceNo();
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }


    private class AssignTask extends AsyncTask<String, Void, Model> {

        private Context context;
        private ProgressDialog pDialog;

        public void onAttach(Context context) {
            this.context = context;
        }

        public void onDetach() {
            this.context = null;
        }

        public AssignTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
//            pDialog.setTitle(getString(R.string.generating_invoice_pdf));
            pDialog.setMessage(getString(R.string.please_wait_invoice_generate));
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.show();
        }

        @Override
        protected Model doInBackground(String... params) {
            String userId = AppUser.getUserId(getActivity());
            String profileId = AppUser.getCompanyId(getActivity());
            String response = ApiCall.POST(params[0], RequestBuilder.DefaultType(userId, profileId, "G"));
            return JsonParser.generateInvoiceNumber(response);
        }

        @Override
        protected void onPostExecute(Model result) {
            super.onPostExecute(result);
            if (context != null) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                if (result.getOutput().equals(Const.SUCCESS)) {
                    etInvoiceNo.setText(result.getId());
                    mPreviousDate = result.getDate();
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    btnAction.revertAnimation();
                    PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                            .show(getFragmentManager(), "popupAlert");
                } else {
                    btnAction.revertAnimation();
                    try {
                        ApiCallError.newInstance(result.getOutput(), result.getOutputMsg(), InvoiceManager.this)
                                .show(getFragmentManager(), "apiCallError");
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }
}
