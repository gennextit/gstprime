package com.gennext.offlinegst.model.user;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.panel.ProductMenu;
import com.gennext.offlinegst.user.purchas.PurchaseManager;
import com.gennext.offlinegst.util.Utility;

import java.util.ArrayList;

public class PurchaseProductAdapter extends RecyclerView.Adapter<PurchaseProductAdapter.ReyclerViewHolder> {

    private final PurchaseManager parentRef;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<ProductModel> items;

    public PurchaseProductAdapter(Activity context, ArrayList<ProductModel> items, PurchaseManager parentRef) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef = parentRef;
        this.items = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_selected_product, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final ProductModel item = items.get(position);
        String costPrice = item.getCostPrice();
        if (costPrice.equals("")) {
            costPrice = item.getSalePrice();
        }
        float price = Utility.parseFloat(costPrice);
        float qun = Utility.parseFloat(item.getQuantity());
        float dis = Utility.parseFloat(item.getDis());
        float cessRate = Utility.parseFloat(item.getCessRate());
        float taxRate = Utility.parseFloat(item.getTaxRate());
        float taxable = (price * qun) - dis;
        float totalTax = taxable + ((taxable * (taxRate + cessRate)) / 100);
        String mTaxable = Utility.decimalFormat(taxable, "#.##");
        String mNetAmt = Utility.decimalFormat(totalTax, "#.##");

        holder.tvName.setText(item.getProductName());
        holder.tvQuantity.setText(Utility.decimalFormat(qun, "#.##"));
        holder.tvTaxable.setText(context.getString(R.string.rs) + mTaxable);
        holder.tvNet.setText(context.getString(R.string.rs) + mNetAmt);

        holder.llSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (parentRef.purchaseType != PurchaseManager.VIEW_PURCHASE) {
                    parentRef.editProduct(item, position);
                }
            }
        });
        holder.btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (parentRef.purchaseType != PurchaseManager.VIEW_PURCHASE) {
                    increaseQuantity(item, position);
                }
            }
        });
        holder.btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (parentRef.purchaseType != PurchaseManager.VIEW_PURCHASE) {
                    decreseQuantity(item, position);
                }
            }
        });
        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (parentRef.purchaseType != PurchaseManager.VIEW_PURCHASE) {
                    items.remove(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, getItemCount());
                    parentRef.deleteProduct(item, position);
                }
            }
        });

    }

    private void increaseQuantity(ProductModel item, int position) {
        float qty = Utility.parseFloat(item.getQuantity());
        float initQty = item.getInitQuantity();
        if (initQty == 0) {
            initQty = 1;
        }
        qty += initQty;
        item.setQuantity(String.valueOf(qty));
        items.set(position, item);
        notifyDataSetChanged();
        parentRef.refreshAdapter();
    }

    private void decreseQuantity(ProductModel item, int position) {
        float qty = Utility.parseFloat(item.getQuantity());
        float initQty = item.getInitQuantity();
        if (initQty == 0) {
            initQty = 1;
        }
        qty -= initQty;
        if (qty == initQty || qty > initQty) {
            item.setQuantity(String.valueOf(qty));
            items.set(position, item);
            notifyDataSetChanged();
            parentRef.refreshAdapter();
        }
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

//    @Override
//    public void onEditClick(DialogFragment dialog, ProductModel itemProduct, int position) {
//        parentRef.editProduct(itemProduct, position);
//    }
//
//    @Override
//    public void onDeleteClick(DialogFragment dialog, ProductModel itemProduct, int position) {
//        items.remove(position);
//        notifyItemRemoved(position);
//        notifyItemRangeChanged(position, getItemCount());
//        parentRef.deleteProduct(itemProduct, position);
//    }
//
//
//    @Override
//    public void onQuantityClick(DialogFragment dialog, ProductModel itemProduct, int position) {
//        parentRef.quantityProduct(itemProduct,position);
//    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private ImageButton btnPlus, btnMinus;
        private ImageButton btnDelete;
        private LinearLayout llSlot;
        private TextView tvName, tvQuantity, tvTaxable, tvNet;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSlot = (LinearLayout) v.findViewById(R.id.layoutSlot);
            btnDelete = (ImageButton) v.findViewById(R.id.btn_delete);
            btnPlus = (ImageButton) v.findViewById(R.id.btn_plus);
            btnMinus = (ImageButton) v.findViewById(R.id.btn_minus);
            tvName = (TextView) v.findViewById(R.id.tv_slot_1);
            tvTaxable = (TextView) v.findViewById(R.id.tv_slot_2);
            tvNet = (TextView) v.findViewById(R.id.tv_slot_3);
            tvQuantity = (TextView) v.findViewById(R.id.tv_slot_4);


        }
    }
}

