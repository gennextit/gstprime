package com.gennext.offlinegst.model;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.model.user.SettingModel;
import com.gennext.offlinegst.user.SettingDefaults;

import java.util.ArrayList;

public class SettingsAdapter extends RecyclerView.Adapter<SettingsAdapter.ReyclerViewHolder> {

    private final SettingDefaults parentPref;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<SettingModel> items;

    public SettingsAdapter(Activity context, ArrayList<SettingModel> items, SettingDefaults settingDefaults) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentPref = settingDefaults;
        this.items = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_setting_defaults, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, int position) {
        final SettingModel item = items.get(position);
 
        holder.tvName.setText(item.getTitle());

        final int pos = position;
        holder.llSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parentPref.onItemSelect(pos+1);
            }
        });
    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout llSlot;
        private TextView tvName;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSlot = (LinearLayout) v.findViewById(R.id.layoutSlot);
            tvName = (TextView) v.findViewById(R.id.tv_slot_1); 
        }
    }
}

