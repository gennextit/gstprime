package com.gennext.offlinegst.model.user;

import java.util.ArrayList;

/**
 * Created by Admin on 8/11/2017.
 */

public class VoucherModel {




    private String voucherId;
    private String voucherUserId;
    private String voucherProfileId;
    private String voucherCompanyId;
    private String voucherNo;
    private String voucherDate;
    private String noteType;
    private String noteTypeId;
    private String noteReason;
    private String noteReasonId;
    private String customerId;
    private String customerName;
    private String invoiceNumber;
    private String invoiceDate;
    private String freightAmount;
    private String preGST;
    private String placeofSupply;
    private String placeofSupplyId;
    private String noteValue;
    private String taxRate;
    private String taxableValue;
    private String cessAmt;

    private String output;
    private String outputMsg;
    private String outputDB;
    private String outputDBMsg;
    private ArrayList<VoucherModel> list;
    private ArrayList<ProductModel> productList;

    public String getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(String voucherId) {
        this.voucherId = voucherId;
    }

    public String getVoucherUserId() {
        return voucherUserId;
    }

    public void setVoucherUserId(String voucherUserId) {
        this.voucherUserId = voucherUserId;
    }

    public String getVoucherProfileId() {
        return voucherProfileId;
    }

    public void setVoucherProfileId(String voucherProfileId) {
        this.voucherProfileId = voucherProfileId;
    }

    public String getVoucherCompanyId() {
        return voucherCompanyId;
    }

    public void setVoucherCompanyId(String voucherCompanyId) {
        this.voucherCompanyId = voucherCompanyId;
    }

    public String getVoucherNo() {
        return voucherNo;
    }

    public void setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
    }

    public String getVoucherDate() {
        return voucherDate;
    }

    public void setVoucherDate(String voucherDate) {
        this.voucherDate = voucherDate;
    }

    public String getNoteType() {
        return noteType;
    }

    public void setNoteType(String noteType) {
        this.noteType = noteType;
    }

    public String getNoteTypeId() {
        return noteTypeId;
    }

    public void setNoteTypeId(String noteTypeId) {
        this.noteTypeId = noteTypeId;
    }

    public String getNoteReason() {
        return noteReason;
    }

    public void setNoteReason(String noteReason) {
        this.noteReason = noteReason;
    }

    public String getNoteReasonId() {
        return noteReasonId;
    }

    public void setNoteReasonId(String noteReasonId) {
        this.noteReasonId = noteReasonId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }


    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getFreightAmount() {
        return freightAmount;
    }

    public void setFreightAmount(String freightAmount) {
        this.freightAmount = freightAmount;
    }

    public String getPreGST() {
        return preGST;
    }

    public void setPreGST(String preGST) {
        this.preGST = preGST;
    }

    public String getPlaceofSupply() {
        return placeofSupply;
    }

    public void setPlaceofSupply(String placeofSupply) {
        this.placeofSupply = placeofSupply;
    }

    public String getPlaceofSupplyId() {
        return placeofSupplyId;
    }

    public void setPlaceofSupplyId(String placeofSupplyId) {
        this.placeofSupplyId = placeofSupplyId;
    }

    public String getNoteValue() {
        return noteValue;
    }

    public void setNoteValue(String noteValue) {
        this.noteValue = noteValue;
    }

    public String getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(String taxRate) {
        this.taxRate = taxRate;
    }

    public String getTaxableValue() {
        return taxableValue;
    }

    public void setTaxableValue(String taxableValue) {
        this.taxableValue = taxableValue;
    }

    public String getCessAmt() {
        return cessAmt;
    }

    public void setCessAmt(String cessAmt) {
        this.cessAmt = cessAmt;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public String getOutputDB() {
        return outputDB;
    }

    public void setOutputDB(String outputDB) {
        this.outputDB = outputDB;
    }

    public String getOutputDBMsg() {
        return outputDBMsg;
    }

    public void setOutputDBMsg(String outputDBMsg) {
        this.outputDBMsg = outputDBMsg;
    }

    public ArrayList<VoucherModel> getList() {
        return list;
    }

    public void setList(ArrayList<VoucherModel> list) {
        this.list = list;
    }

    public ArrayList<ProductModel> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<ProductModel> productList) {
        this.productList = productList;
    }
}