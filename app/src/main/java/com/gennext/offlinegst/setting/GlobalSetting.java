package com.gennext.offlinegst.setting;

/**
 * Created by Admin on 9/27/2017.
 */

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.gennext.offlinegst.MainActivity;
import com.gennext.offlinegst.R;
import com.gennext.offlinegst.SettingActivity;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.LocaleHelper;

/**
 * Created by Admin on 7/4/2017.
 */

public class GlobalSetting extends CompactFragment {
    private RadioButton rbEnglish, rbHindi;


    //    private Toolbar mToolbar;
    public static Fragment newInstance() {
        return new GlobalSetting();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.setting_default, container, false);
        initUi(v);
        return v;
    }

    private void initUi(View v) {
        RadioGroup rgTheme = (RadioGroup) v.findViewById(R.id.rg_theme);
        rbEnglish = (RadioButton) v.findViewById(R.id.rb_english);
        rbHindi = (RadioButton) v.findViewById(R.id.rb_hindi);
        if (AppUser.getLanguage(getContext()).equals(LocaleHelper.LANGUAGE_ENGLISH)) {
            rbEnglish.setChecked(true);
        } else {
            rbHindi.setChecked(true);
        }
        rgTheme.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (checkedId == R.id.rb_english) {
                    AppUser.setLanguage(getContext(),LocaleHelper.LANGUAGE_ENGLISH);
                    ((SettingActivity) getActivity()).updateLanguage();
                }
                if (checkedId == R.id.rb_hindi) {
                    AppUser.setLanguage(getContext(),LocaleHelper.LANGUAGE_HINDI);
                    ((SettingActivity) getActivity()).updateLanguage();
                }
            }

        });
    }
}

