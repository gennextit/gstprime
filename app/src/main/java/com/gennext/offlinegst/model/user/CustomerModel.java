package com.gennext.offlinegst.model.user;

import java.util.ArrayList;

/**
 * Created by Admin on 7/22/2017.
 */

public class CustomerModel {

    private String custometId;
    private String custometUserId;
    private String custometSetupId;
    private String personName;
    private String companyName;
    private String contactEmail;
    private String workPhone;
    private String mobile;
    private String contactDisplayName;
    private String website;
    private String GSTIN;
    private String DLNumber;
    private String state;
    private String city;
    private String country;
    private String zip;
    private String address;
    private String fax;
    private String currency;
    private String paymentTerms;
    private String remarks;
    private String selectedStateCode;
    private String selectedTermCode;
    private String openingBalance;



    private String output;
    private String outputMsg;
    private String outputDB;
    private String outputDBMsg;
    private ArrayList<CustomerModel> list;

    public String getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(String openingBalance) {
        this.openingBalance = openingBalance;
    }

    public String getCustometSetupId() {
        return custometSetupId;
    }

    public void setCustometSetupId(String custometSetupId) {
        this.custometSetupId = custometSetupId;
    }

    public String getSelectedStateCode() {
        return selectedStateCode;
    }

    public void setSelectedStateCode(String selectedStateCode) {
        this.selectedStateCode = selectedStateCode;
    }

    public String getSelectedTermCode() {
        return selectedTermCode;
    }

    public void setSelectedTermCode(String selectedTermCode) {
        this.selectedTermCode = selectedTermCode;
    }

    public String getCustometId() {
        return custometId;
    }

    public void setCustometId(String custometId) {
        this.custometId = custometId;
    }

    public String getCustometUserId() {
        return custometUserId;
    }

    public void setCustometUserId(String custometUserId) {
        this.custometUserId = custometUserId;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getContactDisplayName() {
        return contactDisplayName;
    }

    public void setContactDisplayName(String contactDisplayName) {
        this.contactDisplayName = contactDisplayName;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getGSTIN() {
        return GSTIN;
    }

    public void setGSTIN(String GSTIN) {
        this.GSTIN = GSTIN;
    }

    public String getDLNumber() {
        return DLNumber;
    }

    public void setDLNumber(String DLNumber) {
        this.DLNumber = DLNumber;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPaymentTerms() {
        return paymentTerms;
    }

    public void setPaymentTerms(String paymentTerms) {
        this.paymentTerms = paymentTerms;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public String getOutputDB() {
        return outputDB;
    }

    public void setOutputDB(String outputDB) {
        this.outputDB = outputDB;
    }

    public String getOutputDBMsg() {
        return outputDBMsg;
    }

    public void setOutputDBMsg(String outputDBMsg) {
        this.outputDBMsg = outputDBMsg;
    }

    public ArrayList<CustomerModel> getList() {
        return list;
    }

    public void setList(ArrayList<CustomerModel> list) {
        this.list = list;
    }
}
