package com.gennext.offlinegst.model.app;

import com.gennext.offlinegst.model.user.CompanyModel;
import com.gennext.offlinegst.model.user.ProfileModel;
import com.gennext.offlinegst.panel.ProductMaster;

public class SideMenu {
	private String name;
    private int image;
    private String imagePath;
    private CompanyModel profile;

    public SideMenu(String name, int image) {
    	this.name = name;
    	this.image = image;
    }

	public SideMenu(String name, String imagePath,CompanyModel profile) {
    	this.name = name;
    	this.imagePath = imagePath;
    	this.profile = profile;
    }

	public CompanyModel getProfile() {
		return profile;
	}

	public void CompanyModel(CompanyModel profile) {
		this.profile = profile;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getImage() {
		return image;
	}
	public void setImage(int image) {
		this.image = image;
	}

}
