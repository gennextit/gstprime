package com.gennext.offlinegst.user.invoice;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.PopupAlert;
import com.gennext.offlinegst.model.user.PaymentModel;
import com.gennext.offlinegst.model.user.ReceiptAdapter;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.ApiCallError;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.RequestBuilder;

import java.util.ArrayList;

/**
 * Created by Admin on 7/4/2017.
 */

public class SellOverPaidReceipt extends CompactFragment implements ApiCallError.ErrorListener {
    private RecyclerView lvMain;
    private ArrayList<PaymentModel> cList;
    private ReceiptAdapter adapter;
    private DBManager dbManager;

    private AssignTask assignTask;
    private Animation animSlideDown, animSlideUp;
    private LinearLayout llslotBody;
    private FragmentManager manager;
    private LinearLayout llWhitespace;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
        manager = null;
    }

    public static Fragment newInstance() {
        SellOverPaidReceipt fragment = new SellOverPaidReceipt();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(dbManager!=null)
            dbManager.closeDB();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_buyer_overpaid, container, false);
        screenAnalytics(getContext(), AppUser.getUserId(getContext()), "OverpaidReceipt");
        manager = getFragmentManager();
        dbManager = DBManager.newIsntance(getActivity());
        initUi(v);
        return v;
    }

    private void initUi(View v) {
        TextView tvChartTitle = (TextView) v.findViewById(R.id.tv_popup_1);
        tvChartTitle.setText(R.string.overpaid_customers);
        llslotBody = (LinearLayout) v.findViewById(R.id.ll_body);
        lvMain = (RecyclerView) v.findViewById(R.id.lv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());
        final LinearLayout llslot = (LinearLayout) v.findViewById(R.id.slot);
        Button btnOK = (Button) v.findViewById(R.id.btn_popup);
        llWhitespace = (LinearLayout) v.findViewById(R.id.ll_whitespace);
        llWhitespace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissFlipAnimation();
            }
        });

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissFlipAnimation();
            }
        });
        setAnimation();

        executeTask();
    }

    private void dismissFlipAnimation() {
        animSlideUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                llWhitespace.setVisibility(View.INVISIBLE);
                if (manager != null) {
                    manager.popBackStack();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        llslotBody.startAnimation(animSlideUp);
    }


    private void setAnimation() {
        animSlideDown = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_down);
        animSlideUp = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_up);

    }

    private void executeTask() {
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.GET_PRODUCTS);
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        executeTask();
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }

    public void refreshList() {
        executeTask();
    }


    private class AssignTask extends AsyncTask<String, Void, PaymentModel> {
        private Context context;

        private void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        private void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        private AssignTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected PaymentModel doInBackground(String... urls) {
            String userId = AppUser.getUserId(context);
            String profileId = AppUser.getProfileId(context);
            PaymentModel result = new PaymentModel();
            if (AppConfig.isUpdateToServer(context)) {
                String response = ApiCall.POST(urls[0], RequestBuilder.Default(userId));
                result = JsonParser.getUnpaidReceiptDetail(response);
            }
            return dbManager.getSellOverPaidReceiptListWithStock(result, userId, profileId);
        }


        @Override
        protected void onPostExecute(PaymentModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                if (!AppConfig.isUpdateToServer(context)) {
                    if (result.getOutputDB().equals(Const.SUCCESS)) {
                        cList = result.getList();
                        adapter = new ReceiptAdapter(getActivity(), cList, ReceiptAdapter.OVERPAID_SELLER);
                        lvMain.setAdapter(adapter);
                        llslotBody.setVisibility(View.VISIBLE);
                        llslotBody.startAnimation(animSlideDown);
                    } else if (result.getOutputDB().equals(Const.FAILURE)) {
                        showToastInCenter(result.getOutputDBMsg());
                        llslotBody.setVisibility(View.VISIBLE);
                        llslotBody.startAnimation(animSlideDown);
                    }
                } else {
                    if (result.getOutput().equals(Const.SUCCESS)) {
                        if (!result.getOutputDB().equals(Const.SUCCESS)) {
                            showToast(result.getOutputDBMsg());
                        }
                        cList = result.getList();
                        adapter = new ReceiptAdapter(getActivity(), cList, ReceiptAdapter.OVERPAID_SELLER);
                        lvMain.setAdapter(adapter);
                        llslotBody.setVisibility(View.VISIBLE);
                        llslotBody.startAnimation(animSlideDown);
                    } else if (result.getOutput().equals(Const.FAILURE)) {
                        if (result.getOutputDB().equals(Const.SUCCESS)) {
                            showToastInCenter(result.getOutputDBMsg());
                        }
                        llslotBody.setVisibility(View.VISIBLE);
                        llslotBody.startAnimation(animSlideDown);
                        PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(), "popupAlert");
                    } else {
                        if (result.getOutputDB().equals(Const.SUCCESS)) {
                            showToast(result.getOutputDBMsg());
                        }
                        ApiCallError.newInstance(result.getOutput(),result.getOutputMsg(), SellOverPaidReceipt.this)
                                .show(getFragmentManager(), "apiCallError");
                    }
                }
            }
        }
    }


//    private void showFlipAnimation3(LinearLayout view) {
//        int height = view.getHeight();
//        TranslateAnimation animation = new TranslateAnimation(0f, 0f, -view.getHeight(), 0f);
//        animation.setDuration(5000); // duartion in ms
//        animation.setAnimationListener(new Animation.AnimationListener() {
//            @Override
//            public void onAnimationStart(Animation animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animation animation) {
//                if(manager!=null) {
//                    manager.popBackStack();
//                }
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) {
//
//            }
//        });
////        animation.setFillAfter(false);
//        view.startAnimation(animation);
//    }
//    private void dismissFlipAnimation(LinearLayout view) {
//        TranslateAnimation animation = new TranslateAnimation(0f, 0f,  0f, -view.getHeight());
//        animation.setDuration(500); // duartion in ms
////        animation.setFillAfter(false);
//        animation.setAnimationListener(new Animation.AnimationListener() {
//            @Override
//            public void onAnimationStart(Animation animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animation animation) {
//                if(manager!=null) {
//                    manager.popBackStack();
//                }
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) {
//
//            }
//        });
//        view.startAnimation(animation);
//    }
//    private void dismissFlipAnimation3(LinearLayout view) {
//        AnimatorSet animatorSet = new AnimatorSet();
//        animatorSet.setDuration(400);
//        animatorSet.setInterpolator(new LinearInterpolator());
//        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(view, "ScaleY", 1f, -1);
//        scaleYAnimator.addListener(new Animator.AnimatorListener() {
//            @Override
//            public void onAnimationStart(Animator animation) {
//            }
//
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                if(manager!=null) {
//                    manager.popBackStack();
//                }
//            }
//
//            @Override
//            public void onAnimationCancel(Animator animation) {
//                if(manager!=null) {
//                    manager.popBackStack();
//                }
//            }
//
//            @Override
//            public void onAnimationRepeat(Animator animation) {}
//        });
//        animatorSet.playTogether(scaleYAnimator);
//        animatorSet.start();
//    }

}

