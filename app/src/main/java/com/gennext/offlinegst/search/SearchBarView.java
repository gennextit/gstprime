package com.gennext.offlinegst.search;

/**
 * Created by Admin on 3/15/2018.
 */

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;


public class SearchBarView extends CompactFragment {

 
    private EditText etSearch;
    private SearchBar.Search mListener;
    private SearchBar.SearchOrder mListener2;
    private String searchHint;


    public static SearchBarView newInstance(SearchBar.Search listener, SearchBar.SearchOrder listener2, String searchHint) {
        SearchBarView fragment=new SearchBarView();
        fragment.mListener = listener;
        fragment.mListener2 = listener2;
        fragment.searchHint = searchHint;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = inflater.inflate(R.layout.search_bar_view, container, false);
        initUi(v);

        return v;
    }


    private void initUi(View v) {
        ImageButton btnAction = (ImageButton) v.findViewById(R.id.btn_menu_action);
        etSearch = (EditText) v.findViewById(R.id.et_search);

        if(searchHint!=null){
            etSearch.setHint(searchHint);
        }

        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etSearch.getText().length() == 0) {
                    hideKeybord(getActivity());
                    getFragmentManager().popBackStack();
                } else {
                    etSearch.setText("");
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(mListener!=null) {
                    mListener.onTextChanged(charSequence);
                }else if(mListener2!=null) {
                    mListener2.onTextChanged(charSequence);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    public void resetSearchBox() {
        etSearch.setText("");
    }
}
