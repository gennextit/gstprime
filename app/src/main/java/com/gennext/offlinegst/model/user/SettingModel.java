package com.gennext.offlinegst.model.user;

/**
 * Created by Admin on 7/28/2017.
 */

public class SettingModel {

    private String id;
    private String title;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
