package com.gennext.offlinegst.panel;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.PopupAlert;
import com.gennext.offlinegst.model.user.VendorModel;
import com.gennext.offlinegst.model.user.VendorSelectorAdapter;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.user.purchas.CreditorsTransaction;
import com.gennext.offlinegst.user.purchas.PurchaseManager;
import com.gennext.offlinegst.user.services.purchase.ServicePurchaseManager;
import com.gennext.offlinegst.user.vendor.VendorManager;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.ApiCallError;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.ProgressRounded;
import com.gennext.offlinegst.util.RequestBuilder;

import java.util.ArrayList;


public class VendorSelector extends BottomSheetDialogFragment implements ApiCallError.ErrorParamListener {
    private SelectListener mListener;
    private VendorSelectorAdapter adapter;
    private PurchaseManager purchaseManager;
    private CreditorsTransaction creditorsTransaction;
    private AssignTask assignTask;
    private ListView lvMain;
    private ProgressRounded pBar;
    private ServicePurchaseManager servicePurchaseManager;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public interface SelectListener {
        void onVendorSelect(DialogFragment dialog, VendorModel selectedVendor);

    }


    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }


    public static VendorSelector newInstance(PurchaseManager purchaseManager) {
        VendorSelector fragment = new VendorSelector();
        fragment.purchaseManager = purchaseManager;
        fragment.mListener = purchaseManager;
        return fragment;
    }

    public static VendorSelector newInstance(CreditorsTransaction creditorsTransaction) {
        VendorSelector fragment = new VendorSelector();
        fragment.creditorsTransaction = creditorsTransaction;
        fragment.mListener = creditorsTransaction;
        return fragment;
    }


    public static VendorSelector newInstance(ServicePurchaseManager servicePurchaseManager) {
        VendorSelector fragment = new VendorSelector();
        fragment.servicePurchaseManager = servicePurchaseManager;
        fragment.mListener = servicePurchaseManager;
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_menu_vendor, container, false);
        ImageButton btnAction = (ImageButton) v.findViewById(R.id.btn_action);
        EditText etSearch = (EditText) v.findViewById(R.id.et_search);
        lvMain = (ListView) v.findViewById(R.id.lv_main);
        pBar = ProgressRounded.newInstance(getContext(), v);
        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (purchaseManager != null) {
                    addFragment(VendorManager.newInstance(purchaseManager, null, VendorManager.ADD_VENDOR_WITH_RESPONSE), android.R.id.content, "vendorManager");
                }
                if (creditorsTransaction != null) {
                    addFragment(VendorManager.newInstance(creditorsTransaction, null, VendorManager.ADD_VENDOR_WITH_RESPONSE), android.R.id.content, "vendorManager");
                }
                if (servicePurchaseManager != null) {
                    addFragment(VendorManager.newInstance(servicePurchaseManager, null, VendorManager.ADD_VENDOR_WITH_RESPONSE), android.R.id.content, "vendorManager");
                }

                dismiss();
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (adapter != null)
                    adapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                if (adapter != null) {
                    VendorModel model = adapter.getItem(pos);
                    if (model != null) {
                        if (mListener != null) {
                            mListener.onVendorSelect(VendorSelector.this, model);
                        }
                        dismiss();
                    }
                } else {
                    if (purchaseManager != null) {
                        addFragment(VendorManager.newInstance(purchaseManager, null, VendorManager.ADD_VENDOR_WITH_RESPONSE), android.R.id.content, "vendorManager");
                    }
                    if (creditorsTransaction != null) {
                        addFragment(VendorManager.newInstance(creditorsTransaction, null, VendorManager.ADD_VENDOR_WITH_RESPONSE), android.R.id.content, "vendorManager");
                    }
                    if (servicePurchaseManager != null) {
                        addFragment(VendorManager.newInstance(servicePurchaseManager, null, VendorManager.ADD_VENDOR_WITH_RESPONSE), android.R.id.content, "vendorManager");
                    }
                    dismiss();
                }
            }
        });
//        VendorModel cModel = dbManager.getVendorList(new VendorModel(), AppUser.getUserId(getActivity()));
//        if (cModel != null && cModel.getOutputDB().equals(Const.SUCCESS)) {
//            adapter = new VendorSelectorAdapter(getActivity(), cModel.getList());
//            lvMain.setAdapter(adapter);
//        } else {
//            ArrayList<String> errorList = new ArrayList<>();
//            errorList.add(getString(R.string.vendor_not_found));
//            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.slot_empty_product,
//                    R.id.tv_message, errorList);
//            lvMain.setAdapter(adapter);
//        }
        executeTask();

        return v;
    }

    public void addFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    private void executeTask() {
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.GET_VENDORS);
    }


    @Override
    public void onErrorRetryClick(DialogFragment dialog, String[] param) {
        executeTask();
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog, String[] param) {
        dismiss();
    }


    private class AssignTask extends AsyncTask<String, Void, VendorModel> {
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pBar.showProgressBar();
        }

        @Override
        protected VendorModel doInBackground(String... urls) {
            String userId = AppUser.getUserId(context);
            String setupId = AppUser.getCompanyId(context);
            String response = ApiCall.POST(urls[0], RequestBuilder.Default(userId, setupId));
            return JsonParser.getVendorDetail(context, new VendorModel(), response);
        }


        @Override
        protected void onPostExecute(VendorModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                pBar.hideProgressBar();
                if (result.getOutput().equals(Const.SUCCESS)) {
                    adapter = new VendorSelectorAdapter(getActivity(), result.getList());
                    lvMain.setAdapter(adapter);
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    ArrayList<String> errorList = new ArrayList<>();
//                    errorList.add(getString(R.string.employee_not_found));
                    errorList.add(result.getOutputMsg());
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.slot_empty_product,
                            R.id.tv_message, errorList);
                    lvMain.setAdapter(adapter);
                    PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                            .show(getFragmentManager(), "popupAlert");
                } else {
                    String[] errorSoon = {};
                    ApiCallError.newInstance(result.getOutput(), result.getOutputMsg(), errorSoon, VendorSelector.this)
                            .show(getFragmentManager(), "apiCallError");
                }
            }
        }
    }
}
