package com.gennext.offlinegst.util;

import android.content.Context;

import com.gennext.offlinegst.model.app.Model;
import com.gennext.offlinegst.model.user.CommonModel;
import com.gennext.offlinegst.model.user.CompanyModel;
import com.gennext.offlinegst.model.user.CustomerModel;
import com.gennext.offlinegst.model.user.DashboardModel;
import com.gennext.offlinegst.model.user.InvoiceModel;
import com.gennext.offlinegst.model.user.LoginModel;
import com.gennext.offlinegst.model.user.PaymentModel;
import com.gennext.offlinegst.model.user.ProductModel;
import com.gennext.offlinegst.model.user.ProfileModel;
import com.gennext.offlinegst.model.user.PurchasesModel;
import com.gennext.offlinegst.model.user.ServiceModel;
import com.gennext.offlinegst.model.user.StateModel;
import com.gennext.offlinegst.model.user.VendorModel;
import com.gennext.offlinegst.model.user.VoucherModel;
import com.gennext.offlinegst.setting.Const;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Abhijit on 08-Dec-16.
 */
public class JsonParser {
    public static String ERRORMESSAGE = "Not Available";
    public static final String ERROR_CODE = "ERROR-101";
    public static final String ERROR_MSG = "ERROR-101 No msg available";
    public static final String SERVER = ApiCallError.SERVER_ERROR;
    public static final String INTERNET = ApiCallError.INTERNET_ERROR;
    public static final String IO_EXCEPTION = ApiCall.IO_EXCEPTION;

    //default constant variable
    public static final String STATUS = "status";
    public static final String MESSAGE = "message";

    public Model defaultSuccessResponse(String response) {
        Model model = new Model();
        model.setOutput(Const.SUCCESS);
        return model;
    }

    public static Model defaultFailureResponse() {
        return defaultFailureResponse("Update later");
    }

    public static Model defaultFailureResponse(String errorMsg) {
        Model model = new Model();
        model.setOutput(Const.FAILURE);
        model.setOutputMsg(errorMsg);
        return model;
    }

    public static Model defaultStaticParser(String response) {
        Model jsonModel = new Model();
        jsonModel.setOutput(ERROR_CODE);
        jsonModel.setOutputMsg(ERROR_MSG);
        if (response.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(response);
            return jsonModel;
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString(STATUS).equals(Const.SUCCESS)) {
                        jsonModel.setOutput(Const.SUCCESS);
                        jsonModel.setOutputMsg(mainObject.getString(MESSAGE));
                    } else if (mainObject.getString(STATUS).equals(Const.FAILURE)) {
                        jsonModel.setOutput(Const.FAILURE);
                        jsonModel.setOutputMsg(mainObject.getString(MESSAGE));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public static Model generateInvoiceNumber(String response) {
        Model jsonModel = new Model();
        jsonModel.setOutput(ERROR_CODE);
        jsonModel.setOutputMsg(ERROR_MSG);
        if (response.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(response);
            return jsonModel;
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString(STATUS).equals(Const.SUCCESS)) {
                        jsonModel.setOutput(Const.SUCCESS);
                        jsonModel.setId(mainObject.getString(MESSAGE));
                        jsonModel.setDate(mainObject.getString("previousDate"));
                    } else if (mainObject.getString(STATUS).equals(Const.FAILURE)) {
                        jsonModel.setOutput(Const.FAILURE);
                        jsonModel.setOutputMsg(mainObject.getString(MESSAGE));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public static ProductModel getProductDetail(Context context, ProductModel jsonModel, String response) {
        jsonModel.setOutput(ERROR_CODE);
        jsonModel.setOutputMsg(ERROR_MSG);
        jsonModel.setList(new ArrayList<ProductModel>());
        Map<String, String> mUnitCodeList = AppUser.defaultUnitCodes(context);
        Map<String, String> mTaxCategoryList = AppUser.defaultTaxCategory(context);
        if (response.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(response);
            return jsonModel;
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray globArray = new JSONArray(response);
                    JSONObject globObject = globArray.getJSONObject(0);
                    if (globObject.getString(STATUS).equals(Const.SUCCESS)) {
                        jsonModel.setOutput(Const.SUCCESS);
                        JSONArray msgArray = globObject.getJSONArray(MESSAGE);
                        for (int i = 0; i < msgArray.length(); i++) {
                            JSONObject msgObject = msgArray.getJSONObject(i);
                            jsonModel.setOutput(Const.SUCCESS);
                            ProductModel model = new ProductModel();
                            model.setProductId(msgObject.optString("id"));
                            model.setProductUserId(msgObject.optString("userId"));
                            model.setProductName(msgObject.optString("productDescription"));
                            model.setCodeHSN(msgObject.optString("HSNCode"));
                            model.setOpeningStock(msgObject.optString("openingStock"));
                            model.setBarcode(msgObject.optString("barcode"));
                            model.setUnitCode(msgObject.optString("unitCode"));
                            model.setUnitName(AppUser.getUnitName(mUnitCodeList, msgObject.optString("unitCode")));
                            model.setCostPrice(msgObject.optString("salePrice"));
                            model.setSalePrice(msgObject.optString("salePrice"));
                            model.setMrp(msgObject.optString("mrp"));
                            model.setTaxRate(msgObject.optString("taxRate"));
                            model.setBatch(msgObject.optString("stockBatch"));
                            model.setExp(msgObject.optString("stockExpiry"));
                            model.setTaxCategoryId(msgObject.optString("taxCategory"));
                            model.setTaxCategory(AppUser.getTaxCategoryName(mTaxCategoryList, msgObject.optString("taxCategory")));
                            model.setCessRate(msgObject.optString("cessRate"));
                            model.setQuantity(msgObject.optString("stock").equals("Null") ? "0" : msgObject.optString("stock"));
                            model.setStockBatch(msgObject.optString("stockBatch"));
                            model.setStockExpiry(msgObject.optString("stockExpiry"));
                            model.setStockMrp(msgObject.optString("stockMrp"));
                            float price = Utility.parseFloat(model.getSalePrice());
                            float tax = Utility.parseFloat(model.getTaxRate());
                            float cess = Utility.parseFloat(model.getCessRate());
                            float stock = Utility.parseFloat(model.getQuantity());
                            float openStock = Utility.parseFloat(model.getOpeningStock());
                            model.setQuantity(Utility.decimalFormat(stock+openStock));
                            model.setNetAmount(Utility.decimalFormat(price + (price * (tax + cess) / 100), "#.##"));
                            model.setDis("");
                            jsonModel.getList().add(model);
                        }
                    } else if (globObject.getString(STATUS).equals(Const.FAILURE)) {
                        jsonModel.setOutput(Const.FAILURE);
                        jsonModel.setOutputMsg(globObject.getString(MESSAGE));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public static ProductModel getProductDetail(Context context, ProductModel jsonModel, String response, boolean isCompositeScheme) {
        jsonModel.setOutput(ERROR_CODE);
        jsonModel.setOutputMsg(ERROR_MSG);
        jsonModel.setList(new ArrayList<ProductModel>());
        Map<String, String> mUnitCodeList = AppUser.defaultUnitCodes(context);
        Map<String, String> mTaxCategoryList = AppUser.defaultTaxCategory(context);
        if (response.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(response);
            return jsonModel;
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray globArray = new JSONArray(response);
                    JSONObject globObject = globArray.getJSONObject(0);
                    if (globObject.getString(STATUS).equals(Const.SUCCESS)) {
                        jsonModel.setOutput(Const.SUCCESS);
                        JSONArray msgArray = globObject.getJSONArray(MESSAGE);
                        for (int i = 0; i < msgArray.length(); i++) {
                            JSONObject msgObject = msgArray.getJSONObject(i);
                            jsonModel.setOutput(Const.SUCCESS);
                            ProductModel model = new ProductModel();
                            model.setProductId(msgObject.optString("id"));
                            model.setProductUserId(msgObject.optString("userId"));
                            model.setProductName(msgObject.optString("productDescription"));
                            model.setCodeHSN(msgObject.optString("HSNCode"));
                            model.setOpeningStock(msgObject.optString("openingStock"));
                            model.setBarcode(msgObject.optString("barcode"));
                            model.setUnitCode(msgObject.optString("unitCode"));
                            model.setUnitName(AppUser.getUnitName(mUnitCodeList, msgObject.optString("unitCode")));
                            model.setCostPrice(msgObject.optString("salePrice"));
                            model.setSalePrice(msgObject.optString("salePrice"));
                            model.setMrp(msgObject.optString("mrp"));
                            model.setBatch(msgObject.optString("batch"));
                            model.setExp(msgObject.optString("expiry"));
                            model.setTaxCategoryId(msgObject.optString("taxCategory"));
                            model.setTaxCategory(AppUser.getTaxCategoryName(mTaxCategoryList, msgObject.optString("taxCategory")));
                            if(!isCompositeScheme) {
                                model.setTaxRate(msgObject.optString("taxRate"));
                                model.setCessRate(msgObject.optString("cessRate"));
                            }else{
                                model.setTaxRate("0");
                                model.setCessRate("0");
                            }
                            model.setQuantity(msgObject.optString("stock").equals("Null") ? "0" : msgObject.optString("stock"));
                            model.setStockBatch(msgObject.optString("stockBatch"));
                            model.setStockExpiry(msgObject.optString("stockExpiry"));
                            model.setStockMrp(msgObject.optString("stockMrp"));
                            float price = Utility.parseFloat(model.getSalePrice());
                            float tax = Utility.parseFloat(model.getTaxRate());
                            float cess = Utility.parseFloat(model.getCessRate());
                            float stock = Utility.parseFloat(model.getQuantity());
                            float openStock = Utility.parseFloat(model.getOpeningStock());
                            model.setQuantity(Utility.decimalFormat(stock+openStock));
                            model.setNetAmount(Utility.decimalFormat(price + (price * (tax + cess) / 100), "#.##"));
                            model.setDis("");
                            jsonModel.getList().add(model);
                        }
                    } else if (globObject.getString(STATUS).equals(Const.FAILURE)) {
                        jsonModel.setOutput(Const.FAILURE);
                        jsonModel.setOutputMsg(globObject.getString(MESSAGE));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public static ServiceModel getServiceDetail(String response) {
        ServiceModel jsonModel = new ServiceModel();
        jsonModel.setOutput("ERROR-102");
        jsonModel.setOutputMsg("Server not integrated");
        return jsonModel;
    }

    public static CustomerModel getCustomerDetail(Context context, CustomerModel jsonModel, String response) {
        jsonModel.setOutput(ERROR_CODE);
        jsonModel.setOutputMsg(ERROR_MSG);
        jsonModel.setList(new ArrayList<CustomerModel>());
        Map<String, String> stateList = AppUser.defaultStates(context);
        Map<String, String> termsList = AppUser.defaultTerms(context);
        if (response.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(response);
            return jsonModel;
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray globArray = new JSONArray(response);
                    JSONObject globObject = globArray.getJSONObject(0);
                    if (globObject.getString(STATUS).equals(Const.SUCCESS)) {
                        jsonModel.setOutput(Const.SUCCESS);
                        JSONArray msgArray = globObject.getJSONArray(MESSAGE);
                        for (int i = 0; i < msgArray.length(); i++) {
                            JSONObject msgObject = msgArray.getJSONObject(i);
                            jsonModel.setOutput(Const.SUCCESS);
                            CustomerModel model = new CustomerModel();
                            model.setCustometId(msgObject.optString("id"));
                            model.setCustometUserId(msgObject.optString("userId"));
                            model.setCustometSetupId(msgObject.optString("set_up"));
                            model.setPersonName(msgObject.optString("primaryName"));
                            model.setCompanyName(msgObject.optString("companyName"));
                            model.setContactEmail(msgObject.optString("email"));
                            model.setWorkPhone(msgObject.optString("workPhone"));
                            model.setMobile(msgObject.optString("mobile"));
                            model.setContactDisplayName(msgObject.optString("contactDisplayName"));
                            model.setWebsite(msgObject.optString("website"));
                            model.setGSTIN(msgObject.optString("gstin"));
                            model.setDLNumber(msgObject.optString("dlNumber"));
                            model.setState(AppUser.getStateName(stateList, msgObject.optString("customerState")));
                            model.setCity(msgObject.optString("customerCity"));
                            model.setCountry(msgObject.optString("country"));
                            model.setZip(msgObject.optString("zip"));
                            model.setAddress(msgObject.optString("address"));
                            model.setFax(msgObject.optString("fax"));
                            model.setCurrency(msgObject.optString("currency"));
                            model.setPaymentTerms(AppUser.getTermName(termsList, msgObject.optString("customerTerms")));
                            model.setRemarks(msgObject.optString("remarks"));
                            model.setSelectedStateCode(msgObject.optString("customerState"));
                            model.setSelectedTermCode(msgObject.optString("customerTerms"));
                            model.setOpeningBalance(msgObject.optString("openingBalance"));
                            jsonModel.getList().add(model);
                        }
                    } else if (globObject.getString(STATUS).equals(Const.FAILURE)) {
                        jsonModel.setOutput(Const.FAILURE);
                        jsonModel.setOutputMsg(globObject.getString(MESSAGE));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public static VendorModel getVendorDetail(Context context, VendorModel jsonModel, String response) {
        jsonModel.setOutput(ERROR_CODE);
        jsonModel.setOutputMsg(ERROR_MSG);
        jsonModel.setList(new ArrayList<VendorModel>());
        Map<String, String> stateList = AppUser.defaultStates(context);
        Map<String, String> termsList = AppUser.defaultTerms(context);
        if (response.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(response);
            return jsonModel;
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray globArray = new JSONArray(response);
                    JSONObject globObject = globArray.getJSONObject(0);
                    if (globObject.getString(STATUS).equals(Const.SUCCESS)) {
                        jsonModel.setOutput(Const.SUCCESS);
                        JSONArray msgArray = globObject.getJSONArray(MESSAGE);
                        for (int i = 0; i < msgArray.length(); i++) {
                            JSONObject msgObject = msgArray.getJSONObject(i);
                            jsonModel.setOutput(Const.SUCCESS);
                            VendorModel model = new VendorModel();
                            model.setVendorId(msgObject.optString("id"));
                            model.setVendorUserId(msgObject.optString("userId"));
                            model.setVendorSetupId(msgObject.optString("set_up"));
                            model.setPersonName(msgObject.optString("primaryName"));
                            model.setCompanyName(msgObject.optString("companyName"));
                            model.setContactEmail(msgObject.optString("email"));
                            model.setWorkPhone(msgObject.optString("workPhone"));
                            model.setMobile(msgObject.optString("mobile"));
                            model.setContactDisplayName(msgObject.optString("contactDisplayName"));
                            model.setWebsite(msgObject.optString("website"));
                            model.setGSTIN(msgObject.optString("gstin"));
                            model.setDLNumber(msgObject.optString("dlNumber"));
                            model.setState(AppUser.getStateName(stateList, msgObject.optString("vendorState")));
                            model.setCity(msgObject.optString("vendorCity"));
                            model.setCountry(msgObject.optString("country"));
                            model.setZip(msgObject.optString("zip"));
                            model.setAddress(msgObject.optString("address"));
                            model.setFax(msgObject.optString("fax"));
                            model.setCurrency(msgObject.optString("currency"));
                            model.setPaymentTerms(AppUser.getTermName(termsList, msgObject.optString("vendorTerms")));
                            model.setRemarks(msgObject.optString("remarks"));
                            model.setSelectedStateCode(msgObject.optString("vendorState"));
                            model.setSelectedTermCode(msgObject.optString("vendorTerms"));
                            model.setOpeningBalance(msgObject.optString("openingBalance"));
                            jsonModel.getList().add(model);
                        }
                    } else if (globObject.getString(STATUS).equals(Const.FAILURE)) {
                        jsonModel.setOutput(Const.FAILURE);
                        jsonModel.setOutputMsg(globObject.getString(MESSAGE));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public static PurchasesModel getPurchasesDetail(Context context, String response) {
        PurchasesModel jsonModel = new PurchasesModel();
        jsonModel.setOutput(ERROR_CODE);
        jsonModel.setOutputMsg(ERROR_MSG);
        jsonModel.setList(new ArrayList<PurchasesModel>());
//        Map<String, String> purTypeList = AppUser.defaultPurchaseType(context);
        Map<String, String> purCatModeList = AppUser.defaultPurchaseCategory(context);
        Map<String, String> stateList = AppUser.defaultStates(context);
        Map<String, String> unitCodesList = AppUser.defaultUnitCodes(context);
        Map<String, String> taxRatesList = AppUser.defaultTaxRates(context);
        if (response.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(response);
            return jsonModel;
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray globArray = new JSONArray(response);
                    JSONObject globObject = globArray.getJSONObject(0);
                    if (globObject.getString(STATUS).equals(Const.SUCCESS)) {
                        jsonModel.setOutput(Const.SUCCESS);
                        JSONArray msgArray = globObject.getJSONArray(MESSAGE);
                        for (int i = 0; i < msgArray.length(); i++) {
                            JSONObject msgObject = msgArray.getJSONObject(i);
                            jsonModel.setOutput(Const.SUCCESS);
                            PurchasesModel model = new PurchasesModel();
                            model.setInvoiceUserId(msgObject.optString("userId"));
                            model.setInvoiceProfileId(msgObject.optString("set_up"));
                            model.setInvoiceNumber(msgObject.optString("invoiceNo"));
                            model.setInvoiceDate(msgObject.optString("invoiceDate"));
                            model.setReceiveDate(msgObject.optString("receiveDate"));
                            model.setReverseCharge(msgObject.optString("reverseCharge"));
                            model.setPurchaseType(msgObject.optString("purchaseType"));
                            model.setPurchaseTypeId(msgObject.optString("purchaseType"));
                            model.setPurchaseCategory(AppUser.getPurchaseCategoryName(purCatModeList, msgObject.optString("purchaseCategory")));
                            model.setPurchaseCategoryId(msgObject.optString("purchaseCategory"));
                            model.setMentionedGST(msgObject.optString("mentionedGST"));
                            model.setVendorId(msgObject.optString("vendorId"));
                            model.setVendorName(msgObject.optString("primaryName"));
                            model.setDateOfPurchase(msgObject.optString("dateOfPurchase"));
                            model.setPlaceOfPurchase(AppUser.getStateName(stateList, msgObject.optString("placeStateId")));
                            model.setPlaceOfPurchaseId(msgObject.optString("placeStateId"));
                            model.setDiscount(msgObject.optString("discount"));
                            model.setFreightTaxRate(msgObject.optString("freightTax"));
                            model.setFreightAmount(msgObject.optString("freightAmount"));
                            model.setFreightTotalAmount(msgObject.optString("freight"));
                            model.setCgstTax(msgObject.optString("freightCgstRate"));
                            model.setCgstVal(msgObject.optString("freightCgstAmount"));
                            model.setSgstTax(msgObject.optString("freightSgstRate"));
                            model.setSgstVal(msgObject.optString("freightSgstAmount"));
                            model.setIgstTax(msgObject.optString("freightIgstRate"));
                            model.setIgstVal(msgObject.optString("freightIgstAmount"));
                            model.setTotalAmount(msgObject.optString("taxableAmount"));
                            model.setInvoiceTotal(msgObject.optString("invoiceTotal"));
                            model.setLastDate(msgObject.optString("lastDate"));
                            model.setCancellation(returnFileValidationPurchase(msgObject.optString("txnDate"),model.getLastDate()));
                            model.setProductList(getSellInvoiceProductList(taxRatesList, unitCodesList, msgObject.optJSONArray("productDetails")));
                            jsonModel.getList().add(model);
                        }
                    } else if (globObject.getString(STATUS).equals(Const.FAILURE)) {
                        jsonModel.setOutput(Const.FAILURE);
                        jsonModel.setOutputMsg(globObject.getString(MESSAGE));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public static ArrayList<StateModel> parseStateList(String response) {
        ArrayList<StateModel> stateList = new ArrayList<>();
        try {
            JSONArray stateArray = new JSONArray(response);
            for (int i = 0; i < stateArray.length(); i++) {
                JSONObject stateObj = stateArray.getJSONObject(i);
                StateModel model = new StateModel();
                model.setStateName(stateObj.optString("StateName"));
                model.setStateInitial(stateObj.optString("StateInitial"));
                model.setStateCode(stateObj.optString("StateCode"));
                model.setStateType(stateObj.optString("StateType"));
                stateList.add(model);
            }
        } catch (JSONException e) {
            return null;
        }
        return stateList;
    }

    public static ArrayList<ProductModel> parseUnitList(String response) {
        ArrayList<ProductModel> stateList = new ArrayList<>();
        try {
            JSONArray stateArray = new JSONArray(response);
            for (int i = 0; i < stateArray.length(); i++) {
                JSONObject stateObj = stateArray.getJSONObject(i);
                ProductModel model = new ProductModel();
                model.setUnitCode(stateObj.optString("code"));
                model.setUnitName(stateObj.optString("name"));
                stateList.add(model);
            }
        } catch (JSONException e) {
            return null;
        }
        return stateList;
    }

    public static ArrayList<CommonModel> parsePaymentTerms(String response) {
        ArrayList<CommonModel> stateList = new ArrayList<>();
        try {
            JSONArray stateArray = new JSONArray(response);
            for (int i = 0; i < stateArray.length(); i++) {
                JSONObject stateObj = stateArray.getJSONObject(i);
                CommonModel model = new CommonModel();
                model.setId(stateObj.optString("termId"));
                model.setName(stateObj.optString("termName"));
                stateList.add(model);
            }
        } catch (JSONException e) {
            return null;
        }
        return stateList;
    }

    public static ArrayList<CommonModel> parseInvoiceType(String response) {
        ArrayList<CommonModel> stateList = new ArrayList<>();
        try {
            JSONArray stateArray = new JSONArray(response);
            for (int i = 0; i < stateArray.length(); i++) {
                JSONObject stateObj = stateArray.getJSONObject(i);
                CommonModel model = new CommonModel();
                model.setId(stateObj.optString("id"));
                model.setName(stateObj.optString("typeName"));
                stateList.add(model);
            }
        } catch (JSONException e) {
            return null;
        }
        return stateList;
    }

    public static ArrayList<CommonModel> parseCommonModel(String response) {
        ArrayList<CommonModel> commonList = new ArrayList<>();
        try {
            JSONArray stateArray = new JSONArray(response);
            for (int i = 0; i < stateArray.length(); i++) {
                JSONObject stateObj = stateArray.getJSONObject(i);
                CommonModel model = new CommonModel();
                model.setId(stateObj.optString("id"));
                model.setName(stateObj.optString("name"));
                commonList.add(model);
            }
        } catch (JSONException e) {
            return null;
        }
        return commonList;
    }

    public static ArrayList<CommonModel> parsePurchaseCategoryModel(String response) {
        ArrayList<CommonModel> stateList = new ArrayList<>();
        try {
            JSONArray stateArray = new JSONArray(response);
            for (int i = 0; i < stateArray.length(); i++) {
                JSONObject stateObj = stateArray.getJSONObject(i);
                CommonModel model = new CommonModel();
                model.setId(stateObj.optString("id"));
                model.setName(stateObj.optString("categoryName"));
                stateList.add(model);
            }
        } catch (JSONException e) {
            return null;
        }
        return stateList;
    }

    public static ArrayList<CommonModel> parseTaxCategory(String response) {
        ArrayList<CommonModel> stateList = new ArrayList<>();
        try {
            JSONArray stateArray = new JSONArray(response);
            for (int i = 0; i < stateArray.length(); i++) {
                JSONObject stateObj = stateArray.getJSONObject(i);
                CommonModel model = new CommonModel();
                model.setId(stateObj.optString("id"));
                model.setName(stateObj.optString("category"));
                stateList.add(model);
            }
        } catch (JSONException e) {
            return null;
        }
        return stateList;
    }

    public static ArrayList<CommonModel> parseTax(String response) {
        ArrayList<CommonModel> stateList = new ArrayList<>();
        try {
            JSONArray stateArray = new JSONArray(response);
            for (int i = 0; i < stateArray.length(); i++) {
                JSONObject stateObj = stateArray.getJSONObject(i);
                CommonModel model = new CommonModel();
                model.setId(stateObj.optString("percent"));
                model.setName(stateObj.optString("percent"));
                stateList.add(model);
            }
        } catch (JSONException e) {
            return null;
        }
        return stateList;
    }

    public static CompanyModel getAllCompanyDetail(Context context, String response, String companyId) {
        CompanyModel jsonModel = new CompanyModel();
        jsonModel.setOutput(ERROR_CODE);
        jsonModel.setOutputMsg(ERROR_MSG);
        jsonModel.setList(new ArrayList<CompanyModel>());
        Map<String, String> mStateList = AppUser.defaultStates(context);
        if (response.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(response);
            return jsonModel;
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray globArray = new JSONArray(response);
                    JSONObject globObject = globArray.getJSONObject(0);
                    if (globObject.getString(STATUS).equals(Const.SUCCESS)) {
                        jsonModel.setOutput(Const.SUCCESS);
                        JSONArray msgArray = globObject.getJSONArray(MESSAGE);
                        AppUser.setCompanyDetails(context, msgArray.toString());
                        for (int i = 0; i < msgArray.length(); i++) {
                            JSONObject mainObject = msgArray.getJSONObject(i);
                            jsonModel.setOutput(Const.SUCCESS);
                            CompanyModel model = new CompanyModel();
                            model.setCompanyId(mainObject.optString("id"));
                            model.setCompanyUserId(mainObject.optString("userId"));
                            model.setCompanyName(mainObject.optString("companyName"));
                            model.setBusinessType(mainObject.optString("businessType"));
                            model.setAddress(mainObject.optString("companyAddress"));
                            model.setTelephone(mainObject.optString("telephone"));
                            model.setStateCode(mainObject.optString("stateId"));
                            model.setState(AppUser.getStateName(mStateList, mainObject.optString("stateId")));
                            model.setEmail(mainObject.optString("email"));
                            model.setTin(mainObject.optString("tin"));
                            model.setGstin(mainObject.optString("gstin"));
                            model.setInvoicePrefix(mainObject.optString("invoicePrefix"));
                            model.setInvoiceStart(mainObject.optString("invoiceStart"));
                            model.setTurnover(mainObject.optString("turnover"));
                            model.setBankName(mainObject.optString("bankName"));
                            model.setAccName(mainObject.optString("accountName"));
                            model.setIfscCode(mainObject.optString("ifsc"));
                            model.setAccNumber(mainObject.optString("accountNumber"));
                            model.setBranchAddress(mainObject.optString("branchAddress"));
                            model.settAndC(mainObject.optString("termsAndCondition"));
                            model.setLogo(mainObject.optString("logo"));
                            model.setDlNumber(mainObject.optString("dlNumber"));
                            if (companyId.equalsIgnoreCase(mainObject.optString("id"))) {
                                model.setSelected(true);
                            } else {
                                model.setSelected(false);
                            }
                            jsonModel.getList().add(model);
                        }
                    } else if (globObject.getString(STATUS).equals(Const.FAILURE)) {
                        jsonModel.setOutput(Const.FAILURE);
                        jsonModel.setOutputMsg(globObject.getString(MESSAGE));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public static Model refreshCompanyDetail(Context context, String response) {
        Model jsonModel = new Model();
        jsonModel.setOutput(ERROR_CODE);
        jsonModel.setOutputMsg(ERROR_MSG);
        if (response.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(response);
            return jsonModel;
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray globArray = new JSONArray(response);
                    JSONObject globObject = globArray.getJSONObject(0);
                    if (globObject.getString(STATUS).equals(Const.SUCCESS)) {
                        jsonModel.setOutput(Const.SUCCESS);
                        JSONArray msgArray = globObject.getJSONArray(MESSAGE);
                        AppUser.setCompanyDetails(context, msgArray.toString());
                        jsonModel.setOutputMsg(msgArray.toString());
                    } else if (globObject.getString(STATUS).equals(Const.FAILURE)) {
                        jsonModel.setOutput(Const.FAILURE);
                        jsonModel.setOutputMsg(globObject.getString(MESSAGE));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public static ProfileModel addCompanyProfile(String response) {
        ProfileModel jsonModel = new ProfileModel();
        jsonModel.setOutput("ERROR-102");
        jsonModel.setOutputMsg("Server not integrated");
        return jsonModel;
    }

    public static CompanyModel addCompanyDetail(String response) {
        CompanyModel jsonModel = new CompanyModel();
        jsonModel.setOutput(ERROR_CODE);
        jsonModel.setOutputMsg(ERROR_MSG);
        if (response.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(response);
            return jsonModel;
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray globArray = new JSONArray(response);
                    JSONObject globObject = globArray.getJSONObject(0);
                    if (globObject.getString(STATUS).equals(Const.SUCCESS)) {
                        jsonModel.setOutput(Const.SUCCESS);
                        jsonModel.setOutputMsg(MESSAGE);
                        JSONObject msgObject = globObject.getJSONObject("companyDetail");
                        jsonModel.setCompanyId(msgObject.optString("id"));
                        jsonModel.setCompanyUserId(msgObject.optString("userId"));
                        jsonModel.setCompanyName(msgObject.optString("companyName"));
                        jsonModel.setLogo(msgObject.optString("logo"));
                    } else if (globObject.getString(STATUS).equals(Const.FAILURE)) {
                        jsonModel.setOutput(Const.FAILURE);
                        jsonModel.setOutputMsg(globObject.getString(MESSAGE));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public static CompanyModel updateCompanyDetail(String response) {
        CompanyModel jsonModel = new CompanyModel();
        jsonModel.setOutput(ERROR_CODE);
        jsonModel.setOutputMsg(ERROR_MSG);
        if (response.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(response);
            return jsonModel;
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString(STATUS).equals(Const.SUCCESS)) {
                        jsonModel.setOutput(Const.SUCCESS);
                        jsonModel.setOutputMsg(mainObject.getString(MESSAGE));
                    } else if (mainObject.getString(STATUS).equals(Const.FAILURE)) {
                        jsonModel.setOutput(Const.FAILURE);
                        jsonModel.setOutputMsg(mainObject.getString(MESSAGE));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public static ProfileModel updateProfile(String response) {
        ProfileModel jsonModel = new ProfileModel();
        jsonModel.setOutput("ERROR-102");
        jsonModel.setOutputMsg("Server not integrated");
        return jsonModel;
    }

    public static InvoiceModel getInvoicesDetail(Context context, String response) {
        InvoiceModel jsonModel = new InvoiceModel();
        jsonModel.setOutput(ERROR_CODE);
        jsonModel.setOutputMsg(ERROR_MSG);
        jsonModel.setList(new ArrayList<InvoiceModel>());
        Map<String, String> invTypList = AppUser.defaultInvoiceTypes(context);
        Map<String, String> transpoartModeList = AppUser.defaultTranspoartMode(context);
        Map<String, String> stateList = AppUser.defaultStates(context);
        Map<String, String> termsList = AppUser.defaultTerms(context);
        Map<String, String> unitCodesList = AppUser.defaultUnitCodes(context);
        Map<String, String> taxRatesList = AppUser.defaultTaxRates(context);
        if (response.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(response);
            return jsonModel;
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray globArray = new JSONArray(response);
                    JSONObject globObject = globArray.getJSONObject(0);
                    if (globObject.getString(STATUS).equals(Const.SUCCESS)) {
                        jsonModel.setOutput(Const.SUCCESS);
                        JSONArray msgArray = globObject.getJSONArray(MESSAGE);
                        for (int i = 0; i < msgArray.length(); i++) {
                            JSONObject msgObject = msgArray.getJSONObject(i);
                            jsonModel.setOutput(Const.SUCCESS);
                            InvoiceModel model = new InvoiceModel();
                            model.setInvoiceUserId(msgObject.optString("userId"));
                            model.setInvoiceProfileId(msgObject.optString("set_up"));
                            model.setInvoiceCompanyId(msgObject.optString("set_up"));
                            model.setInvoiceNumber(msgObject.optString("invoiceNo"));
                            model.setInvTypeId(msgObject.optString("invoiceType"));
                            model.setInvTypeName(AppUser.getInvoiceTypesName(invTypList, msgObject.optString("invoiceType")));
                            model.setPortCode(msgObject.optString("portCode"));
                            model.setInvoiceDate(msgObject.optString("invoiceDate"));
                            model.setCustomerId(msgObject.optString("customerId"));
                            model.setCustomerName(msgObject.optString("primaryName"));
                            model.setReverseCharge(msgObject.optString("reverseCharge"));
                            model.setTranspoartMode(AppUser.getTranspoartModeName(transpoartModeList, msgObject.optString("transportMode")));
                            model.setTranspoartModeId(msgObject.optString("transportMode"));
                            model.setVehicleNumber(msgObject.optString("vehicleNo"));
                            model.setDateOfSupply(msgObject.optString("dateOfSupply"));
                            model.setDiscount(msgObject.optString("discount"));
                            model.setFreightTaxRate(msgObject.optString("freightTax"));
                            model.setFreightAmount(msgObject.optString("freightAmount"));
                            model.setFreightTotalAmount(msgObject.optString("freight"));
                            model.setCgstTax(msgObject.optString("freightCgstRate"));
                            model.setCgstVal(msgObject.optString("freightCgstAmount"));
                            model.setSgstTax(msgObject.optString("freightSgstRate"));
                            model.setSgstVal(msgObject.optString("freightSgstAmount"));
                            model.setIgstTax(msgObject.optString("freightIgstRate"));
                            model.setIgstVal(msgObject.optString("freightIgstAmount"));
                            model.setTotalAmount(msgObject.optString("taxableAmount"));
                            model.setInvoiceTotal(msgObject.optString("invoiceTotal"));
                            model.setShipName(msgObject.optString("shipName"));
                            model.setShipGSTIN(msgObject.optString("shipGSTIN"));
                            model.setShipAddress(msgObject.optString("shipAddress"));
                            model.setPlaceofSupply(AppUser.getStateName(stateList, msgObject.optString("shipStateId")));
                            model.setPlaceofSupplyId(msgObject.optString("shipStateId"));
                            model.setInvTermId(msgObject.optString("termId"));
                            model.setInvTermName(AppUser.getTermName(termsList, msgObject.optString("termId")));
                            model.setInvOperatorGSTIN(msgObject.optString("operatorGSTIN"));
                            model.setCessAmount(msgObject.optString("cessAmount"));
                            model.setCustomerState(AppUser.getStateName(stateList, msgObject.optString("customerState")));
                            model.setCustomerCompany(msgObject.optString("companyName"));
                            model.setCustomerStateCode(msgObject.optString("customerState"));
                            model.setCustomerAddress(msgObject.optString("address"));
                            model.setCustomerDLNumber(msgObject.optString("dlNumber"));
                            model.setCustomerGSTIN(msgObject.optString("gstin"));
                            model.setCustomerEmail(msgObject.optString("email"));
                            model.setCancelled(msgObject.optString("cancelled"));
                            model.setCurrency(msgObject.optString("currency"));
                            model.setLastDate(msgObject.optString("lastDate"));
                            model.setCancellation(returnFileValidationSale(model.getInvoiceDate(),model.getLastDate()));
                            model.setProductList(getSellInvoiceProductList(taxRatesList, unitCodesList, msgObject.optJSONArray("productDetails")));
                            jsonModel.getList().add(model);
                        }
                    } else if (globObject.getString(STATUS).equals(Const.FAILURE)) {
                        jsonModel.setOutput(Const.FAILURE);
                        jsonModel.setOutputMsg(globObject.getString(MESSAGE));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    private static Boolean returnFileValidationSale(String invoiceDate, String lastDate) {
        int mInvDate = generateTimeStamp(invoiceDate,false);
        int mLastDate = generateTimeStamp(lastDate,true);
        if(mInvDate<mLastDate){
            return false;//not cancelled
        }else {
            return true;//cancelled
        }
    }

    private static Boolean returnFileValidationPurchase(String txnDate, String lastDate) {
        int mInvDate = generateTimeStampCustom(txnDate);
        int mLastDate = generateTimeStamp(lastDate,true);
        if(mInvDate<mLastDate){
            return false;//not cancelled
        }else {
            return true;//cancelled
        }
    }

    public static int generateTimeStamp(String date, boolean isMiddlescore) {
        String timeStamp;
        try {
            String[] tempDate;
            if(isMiddlescore){
                tempDate = date.split("-");
                timeStamp=String.valueOf(tempDate[0])+String.valueOf(tempDate[1])+String.valueOf(tempDate[2]);
            }else {
                tempDate = date.split("/");
                timeStamp=String.valueOf(tempDate[2])+String.valueOf(tempDate[1])+String.valueOf(tempDate[0]);
            }
        }catch (Exception e){
            return 0;
        }
        return Integer.parseInt(timeStamp);
    }

    public static int generateTimeStampCustom(String date) {
        String timeStamp;
        try {
            String[] tempDate = convertDateSplit(date).split("-");
            timeStamp=String.valueOf(tempDate[0])+String.valueOf(tempDate[1])+String.valueOf(tempDate[2]);
        }catch (Exception e){
            return 0;
        }
        return Integer.parseInt(timeStamp);
    }

    public static String convertDateSplit(String inputDate) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        String outputDateStr = null;
        try {
            Date date = inputFormat.parse(inputDate);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException e) {
            L.m(e.toString());
        }
        return outputDateStr;
    }

    private static ArrayList<ProductModel> getSellInvoiceProductList(Map<String, String> taxRatesList, Map<String, String> unitCodesList, JSONArray jsonArray) throws JSONException {
        ArrayList<ProductModel> prodList = new ArrayList<>();
        if (jsonArray != null)
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject msgObject = jsonArray.getJSONObject(i);
                ProductModel model = new ProductModel();
                model.setPurchasesProdId(msgObject.optString("id"));
                model.setProductUserId(msgObject.optString("userId"));
                model.setProductProfileId(msgObject.optString("set_up"));
                model.setProductCompanyId(msgObject.optString("set_up"));
                model.setInvNumber(msgObject.optString("invoiceNo"));
                model.setQuantity(msgObject.optString("qty"));
                model.setProductId(msgObject.optString("productId"));
                model.setProductName(msgObject.optString("productDes"));
                model.setCodeHSN(msgObject.optString("hsnCode"));
                model.setBarcode(msgObject.optString("barcode"));
                model.setTaxCategoryId(msgObject.optString("taxCategory"));
                model.setTaxCategory(AppUser.getTaxCategoryName(taxRatesList, msgObject.optString("taxCategory")));
                model.setUnitCode(msgObject.optString("unitCode"));
                model.setUnitName(AppUser.getUnitName(unitCodesList, msgObject.optString("unitCode")));
                model.setMrp(msgObject.optString("mrp"));
                model.setBatch(msgObject.optString("batch"));
                model.setExp(msgObject.optString("expiry"));
                model.setCessRate(msgObject.optString("cessTax"));
                model.setTaxRate(msgObject.optString("rate"));
                model.setCostPrice(msgObject.optString("amount"));
                model.setSalePrice(msgObject.optString("amount"));
                model.setDis(msgObject.optString("discount"));
//            float qty = Utility.parseFloat(model.getQuantity());
//            float dis = Utility.parseFloat(model.getDis());
//            float price = Utility.parseFloat(model.getSalePrice());
//            float tax = Utility.parseFloat(model.getTaxRate());
//            float cess = Utility.parseFloat(model.getCessRate());
//            float taxableAmt = (qty * price) - dis;
//            float netAmt = taxableAmt+(taxableAmt*(tax+cess)/100);
//            String res=Utility.decimalFormat(netAmt/qty,"#.##");
                model.setNetAmount("0");
                model.setCgstTax(msgObject.optString("cgstRate"));
                model.setCgstVal(msgObject.optString("cgstAmount"));
                model.setSgstTax(msgObject.optString("sgstRate"));
                model.setSgstVal(msgObject.optString("sgstAmount"));
                model.setIgstTax(msgObject.optString("igstRate"));
                model.setIgstVal(msgObject.optString("igstAmount"));
                prodList.add(model);
            }
        return prodList;
    }

    private static ArrayList<ServiceModel> getSellInvoiceServiceList(JSONArray jsonArray) throws JSONException {
        ArrayList<ServiceModel> prodList = new ArrayList<>();
        if (jsonArray != null)
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject msgObject = jsonArray.getJSONObject(i);
                ServiceModel model = new ServiceModel();
                model.setPurchasesServiceId(msgObject.optString("id"));
                model.setServiceUserId(msgObject.optString("userId"));
                model.setServiceProfileId(msgObject.optString("set_up"));
                model.setServiceCompanyId(msgObject.optString("set_up"));
                model.setInvoiceNumber(msgObject.optString("invoiceNo"));
//            model.setQuantity(msgObject.optString("qty"));
                model.setServiceId(msgObject.optString("serviceId"));
                model.setServiceName(msgObject.optString("serviceDes"));
                model.setCodeSAC(msgObject.optString("sacCode"));
//            model.setBarcode(msgObject.optString("barcode"));
//            model.setTaxCategoryId(msgObject.optString("taxCategory"));
//            model.setTaxCategory(AppUser.getTaxCategoryName(taxRatesList,msgObject.optString("taxCategory")));
//            model.setUnitCode(msgObject.optString("unitCode"));
//            model.setUnitName(AppUser.getUnitName(unitCodesList,msgObject.optString("unitCode")));
//            model.setMrp(msgObject.optString("mrp"));
//            model.setBatch(msgObject.optString("batch"));
//            model.setExp(msgObject.optString("expiry"));
//            model.setCessRate(msgObject.optString("cessTax"));
                model.setTaxRate(msgObject.optString("rate"));
                model.setSalePrice(msgObject.optString("amount"));
                model.setDis(msgObject.optString("discount"));
                float price = Utility.parseFloat(model.getSalePrice());
                float tax = Utility.parseFloat(model.getTaxRate());
//            float cess = Utility.parseFloat(model.getCessRate());
                model.setNetAmount(Utility.decimalFormat(price + (price * (tax) / 100), "#.##"));
                model.setCgstTax(msgObject.optString("cgstRate"));
                model.setCgstVal(msgObject.optString("cgstAmount"));
                model.setSgstTax(msgObject.optString("sgstRate"));
                model.setSgstVal(msgObject.optString("sgstAmount"));
                model.setIgstTax(msgObject.optString("igstRate"));
                model.setIgstVal(msgObject.optString("igstAmount"));
                prodList.add(model);
            }
        return prodList;
    }

    public static VoucherModel getVouchersDetail(String response) {
        VoucherModel jsonModel = new VoucherModel();
        jsonModel.setOutput("ERROR-102");
        jsonModel.setOutputMsg("Server not integrated");
        return jsonModel;
    }

    public static PaymentModel getDebtorsDetail(String response) {
        PaymentModel jsonModel = new PaymentModel();
        jsonModel.setOutput("ERROR-102");
        jsonModel.setOutputMsg("Server not integrated");
        return jsonModel;
    }

    public static PaymentModel getCreditorsDetail(String response) {
        PaymentModel jsonModel = new PaymentModel();
        jsonModel.setOutput("ERROR-102");
        jsonModel.setOutputMsg("Server not integrated");
        return jsonModel;
    }

    public static PaymentModel getUnpaidReceiptDetail(String response) {
        PaymentModel jsonModel = new PaymentModel();
        jsonModel.setOutput("ERROR-102");
        jsonModel.setOutputMsg("Server not integrated");
        return jsonModel;
    }

    public static LoginModel login(LoginModel jsonModel, Context context, String response) {
        jsonModel.setOutput(ERROR_CODE);
        jsonModel.setOutputMsg(ERROR_MSG);
        if (response.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(response);
            return jsonModel;
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString(STATUS).equals(Const.SUCCESS)) {
                        jsonModel.setOutput(Const.SUCCESS);
                        JSONObject msgObject = mainObject.getJSONObject(MESSAGE);
                        JSONObject userDetails = msgObject.getJSONObject("userDetails");
                        jsonModel.setUserId(userDetails.getString("userId"));
                        jsonModel.setUserName(userDetails.optString("userName"));
                        jsonModel.setPan(userDetails.optString("pan"));
                        jsonModel.setEmail(userDetails.optString("email"));
                        jsonModel.setPassword(userDetails.optString("password"));
                        jsonModel.setPhone(userDetails.optString("phone"));
                        jsonModel.setRole(userDetails.optString("role"));
                        jsonModel.setImage(userDetails.optString("image"));
                        jsonModel.setSet_up(userDetails.optString("set_up"));
                        jsonModel.setStatus(userDetails.optString("status"));
                        jsonModel.setCreatedAt(userDetails.optString("createdAt"));
                        jsonModel.setUserDetails(msgObject.optJSONObject("userDetails"));
                        jsonModel.setCompanyDetails(msgObject.optJSONArray("companyDetails"));
                        jsonModel.setUnitQuantityCode(msgObject.optJSONArray("unitQuantityCode"));
                        jsonModel.setTaxDetails(msgObject.optJSONArray("taxDetails"));
                        jsonModel.setStates(msgObject.optJSONArray("states"));
                        jsonModel.setTerms(msgObject.optJSONArray("terms"));
                        jsonModel.setInvoiceTypes(msgObject.optJSONArray("invoiceTypes"));
                        jsonModel.setPurchaseCategory(msgObject.optJSONArray("purchaseCategory"));
                        jsonModel.setTaxCategories(msgObject.optJSONArray("productCategory"));
                        jsonModel.setPurchaseType(msgObject.optJSONArray("purchaseType"));
                        jsonModel.setTransportMode(msgObject.optJSONArray("transportMode"));
                        if (context != null)
                            initilizeBasicSetup(context, jsonModel);
                    } else if (mainObject.getString(STATUS).equals(Const.FAILURE)) {
                        jsonModel.setOutput(Const.FAILURE);
                        jsonModel.setOutputMsg(mainObject.getString(MESSAGE));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    private static void initilizeBasicSetup(Context context, LoginModel user) {
        AppUser.setUserId(context, user.getUserId());
        AppUser.setGmailId(context, user.getEmail());

        AppUser.setUserDetails(context, user.getUserDetails() != null ? user.getUserDetails().toString() : "");
        AppUser.setCompanyDetails(context, user.getCompanyDetails() != null ? user.getCompanyDetails().toString() : "");
        AppUser.setUnitQuantityCode(context, user.getUnitQuantityCode() != null ? user.getUnitQuantityCode().toString() : "");
        AppUser.setTaxDetails(context, user.getTaxDetails() != null ? user.getTaxDetails().toString() : "");
        AppUser.setStatesList(context, user.getStates() != null ? user.getStates().toString() : "");
        AppUser.setTerms(context, user.getTerms() != null ? user.getTerms().toString() : "");
        AppUser.setInvoiceTypes(context, user.getInvoiceTypes() != null ? user.getInvoiceTypes().toString() : "");
        AppUser.setPurchaseType(context, user.getPurchaseType() != null ? user.getPurchaseType().toString() : "");
        AppUser.setPurchaseCategory(context, user.getPurchaseCategory() != null ? user.getPurchaseCategory().toString() : "");
        AppUser.setTaxCategories(context, user.getTaxCategories() != null ? user.getTaxCategories().toString() : "");
        AppUser.setTransportMode(context, user.getTransportMode() != null ? user.getTransportMode().toString() : "");

        try {
            JSONArray companyArr = new JSONArray(user.getCompanyDetails() != null ? user.getCompanyDetails().toString() : "");
            JSONObject firstCompany = companyArr.getJSONObject(0);
            AppUser.setProfileId(context, firstCompany.optString("id"));
            AppUser.setCompanyId(context, firstCompany.optString("id"));
            AppUser.setName(context, firstCompany.optString("companyName"));
            AppUser.setImage(context, firstCompany.optString("logo"));
            AppUser.setBusinessType(context, firstCompany.optString("businessType"));
            AppUser.setCompositionScheme(context, firstCompany.optString("compositScheme"));
        } catch (JSONException e) {
            AppUser.setProfileId(context, "");
            AppUser.setCompanyId(context, "");
            AppUser.setName(context, "");
            AppUser.setImage(context, "");
            AppUser.setBusinessType(context, "");
            AppUser.setCompositionScheme(context, "");
        }
    }

    public static CompanyModel getCompanyDetail(Context context, String companyJson, String companyId) {
        CompanyModel jsonModel = new CompanyModel();
        jsonModel.setOutput(ERROR_CODE);
        jsonModel.setOutputMsg(ERROR_MSG);
        Map<String, String> mStateList = AppUser.defaultStates(context);
        if (companyJson.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(companyJson);
            return jsonModel;
        } else {
            if (companyJson.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(companyJson);
                    for (int i = 0; i < mainArray.length(); i++) {
                        JSONObject mainObject = mainArray.getJSONObject(i);
                        if (companyId.equalsIgnoreCase(mainObject.optString("id"))) {
                            jsonModel.setOutput(Const.SUCCESS);
                            jsonModel.setCompanyId(mainObject.optString("id"));
                            jsonModel.setCompanyUserId(mainObject.optString("userId"));
                            jsonModel.setCompanyName(mainObject.optString("companyName"));
                            jsonModel.setBusinessType(mainObject.optString("businessType"));
                            jsonModel.setAddress(mainObject.optString("companyAddress"));
                            jsonModel.setTelephone(mainObject.optString("telephone"));
                            jsonModel.setStateCode(mainObject.optString("stateId"));
                            jsonModel.setState(AppUser.getStateName(mStateList, mainObject.optString("stateId")));
                            jsonModel.setEmail(mainObject.optString("email"));
                            jsonModel.setTin(mainObject.optString("tin"));
                            jsonModel.setGstin(mainObject.optString("gstin"));
                            jsonModel.setInvoicePrefix(mainObject.optString("invoicePrefix"));
                            jsonModel.setInvoiceStart(mainObject.optString("invoiceStart"));
                            jsonModel.setTurnover(mainObject.optString("turnover"));
                            jsonModel.setBankName(mainObject.optString("bankName"));
                            jsonModel.setAccName(mainObject.optString("accountName"));
                            jsonModel.setIfscCode(mainObject.optString("ifsc"));
                            jsonModel.setAccNumber(mainObject.optString("accountNumber"));
                            jsonModel.setBranchAddress(mainObject.optString("branchAddress"));
                            jsonModel.settAndC(mainObject.optString("termsAndCondition"));
                            jsonModel.setLogo(mainObject.optString("logo"));
                            jsonModel.setDlNumber(mainObject.optString("dlNumber"));
                            jsonModel.setCompositScheme(mainObject.optString("compositScheme"));
                            jsonModel.setSwiftCode(mainObject.optString("swift"));
                            return jsonModel;
                        }
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + companyJson;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + companyJson);
                }
            } else {
                ERRORMESSAGE = companyJson;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(companyJson);
            }
        }
        return jsonModel;
    }


    public static CompanyModel getAllCompanyList(CompanyModel jsonModel, String response, String companyId) {
        jsonModel.setOutput(ERROR_CODE);
        jsonModel.setOutputMsg(ERROR_MSG);
        jsonModel.setList(new ArrayList<CompanyModel>());
        if (response.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(response);
            return jsonModel;
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    for (int i = 0; i < mainArray.length(); i++) {
                        JSONObject mainObject = mainArray.getJSONObject(i);
                        jsonModel.setOutput(Const.SUCCESS);
                        CompanyModel model = new CompanyModel();
                        model.setCompanyId(mainObject.optString("id"));
                        model.setCompanyUserId(mainObject.optString("userId"));
                        model.setCompanyName(mainObject.optString("companyName"));
                        model.setEmail(mainObject.optString("email"));
                        model.setGstin(mainObject.optString("gstin"));
                        model.setLogo(mainObject.optString("logo"));
                        if (companyId.equalsIgnoreCase(mainObject.optString("id"))) {
                            model.setSelected(true);
                        } else {
                            model.setSelected(false);
                        }
                        jsonModel.getList().add(model);
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public static String getSelectedCompanyStateCode(String response, String companyId) {
        String stateCode = "";
        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                for (int i = 0; i < mainArray.length(); i++) {
                    JSONObject mainObject = mainArray.getJSONObject(i);
                    if (mainObject.optString("id").equals(companyId)) {
                        stateCode = mainObject.optString("stateId");
                    }
                }
            } catch (JSONException e) {
                stateCode = e.toString();
            }
        } else {
            stateCode = "Invalid response";
        }
        return stateCode;
    }

    public static String generateInvoiceProductJSON(String userId, String companyId, String invoiceNumber, ArrayList<ProductModel> productList, Boolean isIGSTApplicable) {
        try {
            // In this case we need a json array to hold the java list
            JSONArray jsonArr = new JSONArray();
            for (ProductModel model : productList) {
//                float totalProdPrice = convertToFloat(model.getSalePrice()) * convertToFloat(model.getQuantity());
//                String allProdTax[] = calTax(String.valueOf(totalProdPrice), model.getTaxRate(), model.getCessRate(), isIGSTApplicable);
//                String totalTaxAmount = allProdTax[0];
//                String prodCGSTTax = allProdTax[1];
//                String prodCGSTVal = allProdTax[2];
//                String prodSGSTTax = allProdTax[3];
//                String prodSGSTVal = allProdTax[4];
//                String prodIGSTTax = allProdTax[5];
//                String prodIGSTVal = allProdTax[6];

//                String taxableAmt=calAmount(convertToFloat(model.getSalePrice()),convertToFloat(model.getQuantity()));
//                String cessAmt=getCessAmount(convertToFloat(taxableAmt),convertToFloat(model.getCessRate()));

                JSONObject pnObj = new JSONObject();
                pnObj.put("userId", userId);
                pnObj.put("set_up", companyId);
                pnObj.put("invoiceNo", invoiceNumber != null ? invoiceNumber : "");
                pnObj.put("productId", model.getProductId() != null ? model.getProductId() : "");
                pnObj.put("barcode", model.getBarcode() != null ? model.getBarcode() : "");
                pnObj.put("productDes", model.getProductName() != null ? model.getProductName() : "");
                pnObj.put("hsnCode", model.getCodeHSN() != null ? model.getCodeHSN() : "");
                pnObj.put("unitCode", model.getUnitCode() != null ? model.getUnitCode() : "");
                pnObj.put("mrp", model.getMrp() != null ? model.getMrp() : "");
                pnObj.put("batch", model.getBatch() != null ? model.getBatch() : "");
                pnObj.put("expiry", model.getExp() != null ? model.getExp() : "");
                pnObj.put("rate", model.getTaxRate() != null ? model.getTaxRate() : "");
                pnObj.put("discount", model.getDis() != null ? model.getDis() : "");
                pnObj.put("amount", model.getSalePrice() != null ? model.getSalePrice() : "");
                pnObj.put("qty", model.getQuantity() != null ? model.getQuantity() : "");
//                pnObj.put("taxableAmount", taxableAmt!=null?taxableAmt:"");
//                pnObj.put("total", totalTaxAmount!=null?totalTaxAmount:"");
//                pnObj.put("cgstRate", prodCGSTTax!=null?prodCGSTTax:"");
//                pnObj.put("cgstAmount",prodCGSTVal!=null?prodCGSTVal:"");
//                pnObj.put("sgstRate",prodSGSTTax!=null?prodSGSTTax:"");
//                pnObj.put("sgstAmount",prodSGSTVal!=null?prodSGSTVal:"");
//                pnObj.put("igstRate",prodIGSTTax!=null?prodIGSTTax:"");
//                pnObj.put("igstAmount",prodIGSTVal!=null?prodIGSTVal:"");
//                pnObj.put("cessAmount", cessAmt!=null?cessAmt:"");
                pnObj.put("cessTax", model.getCessRate() != null ? model.getCessRate() : "");
                pnObj.put("taxCategory", model.getTaxCategory() != null ? model.getTaxCategory() : "");
                jsonArr.put(pnObj);
            }

            return jsonArr.toString();

        } catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static String generateInvoiceServiceJSON(String userId, String companyId, String invoiceNumber, ArrayList<ServiceModel> productList, Boolean isIGSTApplicable) {
        try {
            // In this case we need a json array to hold the java list
            JSONArray jsonArr = new JSONArray();
            for (ServiceModel model : productList) {
                float totalProdPrice = convertToFloat(model.getSalePrice());
                String allProdTax[] = calTax(String.valueOf(totalProdPrice), model.getTaxRate(), isIGSTApplicable);
                String totalTaxAmount = allProdTax[0];
                String prodCGSTTax = allProdTax[1];
                String prodCGSTVal = allProdTax[2];
                String prodSGSTTax = allProdTax[3];
                String prodSGSTVal = allProdTax[4];
                String prodIGSTTax = allProdTax[5];
                String prodIGSTVal = allProdTax[6];

                String taxableAmt = model.getSalePrice();

                JSONObject pnObj = new JSONObject();
                pnObj.put("userId", userId);
                pnObj.put("set_up", companyId);
                pnObj.put("invoiceNo", invoiceNumber != null ? invoiceNumber : "");
                pnObj.put("serviceDes", model.getServiceName() != null ? model.getServiceName() : "");
                pnObj.put("sacCode", model.getCodeSAC() != null ? model.getCodeSAC() : "");
//                pnObj.put("salePrice", model.getSalePrice()!=null?model.getSalePrice():"");
                pnObj.put("rate", model.getTaxRate() != null ? model.getTaxRate() : "");
                pnObj.put("amount", model.getSalePrice() != null ? model.getSalePrice() : "");
                pnObj.put("discount", model.getDis() != null ? model.getDis() : "");
                pnObj.put("taxableAmount", taxableAmt != null ? taxableAmt : "");
                pnObj.put("total", totalTaxAmount != null ? totalTaxAmount : "");
                pnObj.put("cgstRate", prodCGSTTax != null ? prodCGSTTax : "");
                pnObj.put("cgstAmount", prodCGSTVal != null ? prodCGSTVal : "");
                pnObj.put("sgstRate", prodSGSTTax != null ? prodSGSTTax : "");
                pnObj.put("sgstAmount", prodSGSTVal != null ? prodSGSTVal : "");
                pnObj.put("igstRate", prodIGSTTax != null ? prodIGSTTax : "");
                pnObj.put("igstAmount", prodIGSTVal != null ? prodIGSTVal : "");
//                pnObj.put("taxCategory", model.getTaxCategory()!=null?model.getTaxCategory():"");
                jsonArr.put(pnObj);
            }

            return jsonArr.toString();

        } catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private static String getTotalAmount(float taxableAmt, float cessRate, float texRate) {
        return Utility.decimalFormat(((taxableAmt * (cessRate + texRate)) / 100), "#.##");
    }

    private static String getCessAmount(float taxableAmt, float cessRate) {
        return Utility.decimalFormat(((taxableAmt * cessRate) / 100), "#.##");
    }

    private static String calAmount(float salePrice, float qty) {
        return Utility.decimalFormat((salePrice * qty), "#.##");
    }


    private static float convertToFloat(String salePrice) {
        try {
            return Float.parseFloat(salePrice);
        } catch (NumberFormatException e) {
            return 0;
        }catch (NullPointerException e) {
            return 0;
        }
    }

    private static String[] calTax(String freightAmount, String taxRate, String cessRate, Boolean isIGSTApplicable) {
        float maxTaxRate = convertToFloat(taxRate);
        float maxCessRate = convertToFloat(cessRate);
        float totalAmt = convertToFloat(freightAmount);
        String[] taxCal = new String[7];
        if (isIGSTApplicable) {
            taxCal[0] = String.valueOf(totalAmt + ((totalAmt * (maxTaxRate + maxCessRate)) / 100));
            taxCal[1] = "0";
            taxCal[2] = "0";
            taxCal[3] = "0";
            taxCal[4] = "0";
            taxCal[5] = String.valueOf(maxTaxRate);
            taxCal[6] = String.valueOf((totalAmt * maxTaxRate) / 100);
        } else {
            taxCal[0] = String.valueOf(totalAmt + ((totalAmt * (maxTaxRate + maxCessRate)) / 100));
            maxTaxRate = maxTaxRate / 2;
            taxCal[1] = String.valueOf(maxTaxRate);
            taxCal[2] = String.valueOf((totalAmt * maxTaxRate) / 100);
            taxCal[3] = String.valueOf(maxTaxRate);
            taxCal[4] = String.valueOf((totalAmt * maxTaxRate) / 100);
            taxCal[5] = "0";
            taxCal[6] = "0";
        }
        return taxCal;
    }

    private static String[] calTax(String freightAmount, String taxRate, Boolean isIGSTApplicable) {
        float maxTaxRate = convertToFloat(taxRate);
        float totalAmt = convertToFloat(freightAmount);
        String[] taxCal = new String[7];
        if (isIGSTApplicable) {
            taxCal[0] = String.valueOf(totalAmt + ((totalAmt * (maxTaxRate)) / 100));
            taxCal[1] = "0";
            taxCal[2] = "0";
            taxCal[3] = "0";
            taxCal[4] = "0";
            taxCal[5] = String.valueOf(maxTaxRate);
            taxCal[6] = String.valueOf((totalAmt * maxTaxRate) / 100);
        } else {
            taxCal[0] = String.valueOf(totalAmt + ((totalAmt * (maxTaxRate)) / 100));
            maxTaxRate = maxTaxRate / 2;
            taxCal[1] = String.valueOf(maxTaxRate);
            taxCal[2] = String.valueOf((totalAmt * maxTaxRate) / 100);
            taxCal[3] = String.valueOf(maxTaxRate);
            taxCal[4] = String.valueOf((totalAmt * maxTaxRate) / 100);
            taxCal[5] = "0";
            taxCal[6] = "0";
        }
        return taxCal;
    }

    public static String generatePurchasesProductJSON(String userId, String companyId, String invoiceNumber, ArrayList<ProductModel> productList, boolean isIGSTApplicable) {
        return null;
    }

    public static InvoiceModel cancelInvoice(String response) {
        InvoiceModel jsonModel = new InvoiceModel();
        jsonModel.setOutput(ERROR_CODE);
        jsonModel.setOutputMsg(ERROR_MSG);
        if (response.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(response);
            return jsonModel;
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString(STATUS).equals(Const.SUCCESS)) {
                        jsonModel.setOutput(Const.SUCCESS);
                        jsonModel.setOutputMsg(mainObject.getString(MESSAGE));
                    } else if (mainObject.getString(STATUS).equals(Const.FAILURE)) {
                        jsonModel.setOutput(Const.FAILURE);
                        jsonModel.setOutputMsg(mainObject.getString(MESSAGE));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public static CustomerModel deleteCustomer(String response) {
        CustomerModel jsonModel = new CustomerModel();
        jsonModel.setOutput(ERROR_CODE);
        jsonModel.setOutputMsg(ERROR_MSG);
        if (response.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(response);
            return jsonModel;
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString(STATUS).equals(Const.SUCCESS)) {
                        jsonModel.setOutput(Const.SUCCESS);
                        jsonModel.setOutputMsg(mainObject.getString(MESSAGE));
                    } else if (mainObject.getString(STATUS).equals(Const.FAILURE)) {
                        jsonModel.setOutput(Const.FAILURE);
                        jsonModel.setOutputMsg(mainObject.getString(MESSAGE));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public static ProductModel deleteProduct(String response) {
        ProductModel jsonModel = new ProductModel();
        jsonModel.setOutput(ERROR_CODE);
        jsonModel.setOutputMsg(ERROR_MSG);
        if (response.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(response);
            return jsonModel;
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString(STATUS).equals(Const.SUCCESS)) {
                        jsonModel.setOutput(Const.SUCCESS);
                        jsonModel.setOutputMsg(mainObject.getString(MESSAGE));
                    } else if (mainObject.getString(STATUS).equals(Const.FAILURE)) {
                        jsonModel.setOutput(Const.FAILURE);
                        jsonModel.setOutputMsg(mainObject.getString(MESSAGE));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public static VendorModel deleteVendor(String response) {
        VendorModel jsonModel = new VendorModel();
        jsonModel.setOutput(ERROR_CODE);
        jsonModel.setOutputMsg(ERROR_MSG);
        if (response.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(response);
            return jsonModel;
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString(STATUS).equals(Const.SUCCESS)) {
                        jsonModel.setOutput(Const.SUCCESS);
                        jsonModel.setOutputMsg(mainObject.getString(MESSAGE));
                    } else if (mainObject.getString(STATUS).equals(Const.FAILURE)) {
                        jsonModel.setOutput(Const.FAILURE);
                        jsonModel.setOutputMsg(mainObject.getString(MESSAGE));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public static PurchasesModel cancelPurchaseInvoice(String response) {
        PurchasesModel jsonModel = new PurchasesModel();
        jsonModel.setOutput(ERROR_CODE);
        jsonModel.setOutputMsg(ERROR_MSG);
        if (response.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(response);
            return jsonModel;
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString(STATUS).equals(Const.SUCCESS)) {
                        jsonModel.setOutput(Const.SUCCESS);
                        jsonModel.setOutputMsg(mainObject.getString(MESSAGE));
                    } else if (mainObject.getString(STATUS).equals(Const.FAILURE)) {
                        jsonModel.setOutput(Const.FAILURE);
                        jsonModel.setOutputMsg(mainObject.getString(MESSAGE));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public static CompanyModel parseCompanydetail(Context context,String response, String invoiceUserId, String invoiceSetupId) {
        CompanyModel jsonModel = new CompanyModel();
        jsonModel.setOutput(ERROR_CODE);
        jsonModel.setOutputMsg(ERROR_MSG);
        Map<String, String> mStateList = AppUser.defaultStates(context);
        if (response.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(response);
            return jsonModel;

        } else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    for (int i = 0; i < mainArray.length(); i++) {
                        JSONObject mainObject = mainArray.getJSONObject(i);
                        if (mainObject.getString("userId").equals(invoiceUserId) && mainObject.getString("id").equals(invoiceSetupId)) {
                            jsonModel.setOutput(Const.SUCCESS);
                            jsonModel.setCompanyId(mainObject.optString("id"));
                            jsonModel.setCompanyUserId(mainObject.optString("userId"));
                            jsonModel.setCompanyName(mainObject.optString("companyName"));
                            jsonModel.setBusinessType(mainObject.optString("businessType"));
                            jsonModel.setAddress(mainObject.optString("companyAddress"));
                            jsonModel.setTelephone(mainObject.optString("telephone"));
                            jsonModel.setStateCode(mainObject.optString("stateId"));
                            jsonModel.setState(AppUser.getStateName(mStateList, mainObject.optString("stateId")));
                            jsonModel.setEmail(mainObject.optString("email"));
                            jsonModel.setGstin(mainObject.optString("gstin"));
                            jsonModel.setInvoicePrefix(mainObject.optString("invoicePrefix"));
                            jsonModel.setInvoiceStart(mainObject.optString("invoiceStart"));
                            jsonModel.setBankName(mainObject.optString("bankName"));
                            jsonModel.setAccName(mainObject.optString("accountName"));
                            jsonModel.setIfscCode(mainObject.optString("ifsc"));
                            jsonModel.setSwiftCode(mainObject.optString("swift"));
                            jsonModel.setAccNumber(mainObject.optString("accountNumber"));
                            jsonModel.setBranchAddress(mainObject.optString("branchAddress"));
                            jsonModel.settAndC(mainObject.optString("termsAndCondition"));
                            jsonModel.setLogo(mainObject.optString("logo"));
                            jsonModel.setDlNumber(mainObject.optString("dlNumber"));
                            jsonModel.setDateModified(mainObject.optString("dateModified"));
                        }
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public static InvoiceModel getServicesSaleDetail(Context context, String response) {
        InvoiceModel jsonModel = new InvoiceModel();
        jsonModel.setOutput(ERROR_CODE);
        jsonModel.setOutputMsg(ERROR_MSG);
        jsonModel.setList(new ArrayList<InvoiceModel>());
        Map<String, String> invTypList = AppUser.defaultInvoiceTypes(context);
        Map<String, String> transpoartModeList = AppUser.defaultTranspoartMode(context);
        Map<String, String> stateList = AppUser.defaultStates(context);
        Map<String, String> termsList = AppUser.defaultTerms(context);
        if (response.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(response);
            return jsonModel;
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray globArray = new JSONArray(response);
                    JSONObject globObject = globArray.getJSONObject(0);
                    if (globObject.getString(STATUS).equals(Const.SUCCESS)) {
                        jsonModel.setOutput(Const.SUCCESS);
                        JSONArray msgArray = globObject.getJSONArray(MESSAGE);
                        for (int i = 0; i < msgArray.length(); i++) {
                            JSONObject msgObject = msgArray.getJSONObject(i);
                            jsonModel.setOutput(Const.SUCCESS);
                            InvoiceModel model = new InvoiceModel();
                            model.setInvoiceUserId(msgObject.optString("userId"));
                            model.setInvoiceProfileId(msgObject.optString("set_up"));
                            model.setInvoiceCompanyId(msgObject.optString("set_up"));
                            model.setInvoiceNumber(msgObject.optString("invoiceNo"));
                            model.setInvTypeId(msgObject.optString("invoiceType"));
                            model.setInvTypeName(AppUser.getInvoiceTypesName(invTypList, msgObject.optString("invoiceType")));
                            model.setPortCode(msgObject.optString("portCode"));
                            model.setInvoiceDate(msgObject.optString("invoiceDate"));
                            model.setCustomerId(msgObject.optString("customerId"));
                            model.setCustomerName(msgObject.optString("primaryName"));
                            model.setReverseCharge(msgObject.optString("reverseCharge"));
                            model.setTranspoartMode(AppUser.getTranspoartModeName(transpoartModeList, msgObject.optString("transportMode")));
                            model.setTranspoartModeId(msgObject.optString("transportMode"));
                            model.setVehicleNumber(msgObject.optString("vehicleNo"));
                            model.setDateOfSupply(msgObject.optString("dateOfSupply"));
                            model.setDiscount(msgObject.optString("discount"));
                            model.setFreightTaxRate(msgObject.optString("freightTax"));
                            model.setFreightAmount(msgObject.optString("freightAmount"));
                            model.setFreightTotalAmount(msgObject.optString("freight"));
                            model.setCgstTax(msgObject.optString("freightCgstRate"));
                            model.setCgstVal(msgObject.optString("freightCgstAmount"));
                            model.setSgstTax(msgObject.optString("freightSgstRate"));
                            model.setSgstVal(msgObject.optString("freightSgstAmount"));
                            model.setIgstTax(msgObject.optString("freightIgstRate"));
                            model.setIgstVal(msgObject.optString("freightIgstAmount"));
                            model.setTotalAmount(msgObject.optString("taxableAmount"));
                            model.setInvoiceTotal(msgObject.optString("invoiceTotal"));
                            model.setShipName(msgObject.optString("shipName"));
                            model.setShipGSTIN(msgObject.optString("shipGSTIN"));
                            model.setShipAddress(msgObject.optString("shipAddress"));
                            model.setPlaceofSupply(AppUser.getStateName(stateList, msgObject.optString("shipStateId")));
                            model.setPlaceofSupplyId(msgObject.optString("shipStateId"));
                            model.setInvTermId(msgObject.optString("termId"));
                            model.setInvTermName(AppUser.getTermName(termsList, msgObject.optString("termId")));
                            model.setInvOperatorGSTIN(msgObject.optString("operatorGSTIN"));
                            model.setCessAmount(msgObject.optString("cessAmount"));
                            model.setCustomerState(AppUser.getStateName(stateList, msgObject.optString("customerState")));
                            model.setCustomerCompany(msgObject.optString("companyName"));
                            model.setCustomerStateCode(msgObject.optString("customerState"));
                            model.setCustomerAddress(msgObject.optString("address"));
                            model.setCustomerDLNumber(msgObject.optString("dlNumber"));
                            model.setCustomerGSTIN(msgObject.optString("gstin"));
                            model.setCustomerEmail(msgObject.optString("email"));
                            model.setCancelled(msgObject.optString("cancelled"));
                            model.setCurrency(msgObject.optString("currency"));
                            model.setLastDate(msgObject.optString("lastDate"));
                            model.setCancellation(returnFileValidationSale(model.getInvoiceDate(),model.getLastDate()));
                            model.setServicesList(getSellInvoiceServiceList(msgObject.optJSONArray("serviceDetails")));
                            jsonModel.getList().add(model);
                        }
                    } else if (globObject.getString(STATUS).equals(Const.FAILURE)) {
                        jsonModel.setOutput(Const.FAILURE);
                        jsonModel.setOutputMsg(globObject.getString(MESSAGE));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public static InvoiceModel cancelServiceInvoice(String response) {
        InvoiceModel model = new InvoiceModel();
        model.setOutput(Const.SUCCESS);
        return model;
    }

    public static PurchasesModel getServicesPurchaseDetail(Context context, String response) {
        PurchasesModel jsonModel = new PurchasesModel();
        jsonModel.setOutput(ERROR_CODE);
        jsonModel.setOutputMsg(ERROR_MSG);
        jsonModel.setList(new ArrayList<PurchasesModel>());
//        Map<String, String> purTypeList = AppUser.defaultPurchaseType(context);
        Map<String, String> purCatModeList = AppUser.defaultPurchaseCategory(context);
        Map<String, String> stateList = AppUser.defaultStates(context);
        if (response.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(response);
            return jsonModel;
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray globArray = new JSONArray(response);
                    JSONObject globObject = globArray.getJSONObject(0);
                    if (globObject.getString(STATUS).equals(Const.SUCCESS)) {
                        jsonModel.setOutput(Const.SUCCESS);
                        JSONArray msgArray = globObject.getJSONArray(MESSAGE);
                        for (int i = 0; i < msgArray.length(); i++) {
                            JSONObject msgObject = msgArray.getJSONObject(i);
                            jsonModel.setOutput(Const.SUCCESS);
                            PurchasesModel model = new PurchasesModel();
                            model.setInvoiceUserId(msgObject.optString("userId"));
                            model.setInvoiceProfileId(msgObject.optString("set_up"));
                            model.setInvoiceNumber(msgObject.optString("invoiceNo"));
                            model.setInvoiceDate(msgObject.optString("invoiceDate"));
                            model.setReceiveDate(msgObject.optString("receiveDate"));
                            model.setReverseCharge(msgObject.optString("reverseCharge"));
                            model.setPurchaseType(msgObject.optString("purchaseType"));
                            model.setPurchaseTypeId(msgObject.optString("purchaseType"));
                            model.setPurchaseCategory(AppUser.getPurchaseCategoryName(purCatModeList, msgObject.optString("purchaseCategory")));
                            model.setPurchaseCategoryId(msgObject.optString("purchaseCategory"));
                            model.setMentionedGST(msgObject.optString("mentionedGST"));
                            model.setVendorId(msgObject.optString("vendorId"));
                            model.setVendorName(msgObject.optString("primaryName"));
                            model.setDateOfPurchase(msgObject.optString("dateOfPurchase"));
                            model.setPlaceOfPurchase(AppUser.getStateName(stateList, msgObject.optString("placeStateId")));
                            model.setPlaceOfPurchaseId(msgObject.optString("placeStateId"));
                            model.setDiscount(msgObject.optString("discount"));
                            model.setFreightTaxRate(msgObject.optString("freightTax"));
                            model.setFreightAmount(msgObject.optString("freightAmount"));
                            model.setFreightTotalAmount(msgObject.optString("freight"));
                            model.setCgstTax(msgObject.optString("freightCgstRate"));
                            model.setCgstVal(msgObject.optString("freightCgstAmount"));
                            model.setSgstTax(msgObject.optString("freightSgstRate"));
                            model.setSgstVal(msgObject.optString("freightSgstAmount"));
                            model.setIgstTax(msgObject.optString("freightIgstRate"));
                            model.setIgstVal(msgObject.optString("freightIgstAmount"));
                            model.setTotalAmount(msgObject.optString("taxableAmount"));
                            model.setInvoiceTotal(msgObject.optString("invoiceTotal"));
                            model.setLastDate(msgObject.optString("lastDate"));
                            model.setCancellation(returnFileValidationPurchase(msgObject.optString("txnDate"),model.getLastDate()));
                            model.setServicesList(getSellInvoiceServiceList(msgObject.optJSONArray("details")));
                            jsonModel.getList().add(model);
                        }
                    } else if (globObject.getString(STATUS).equals(Const.FAILURE)) {
                        jsonModel.setOutput(Const.FAILURE);
                        jsonModel.setOutputMsg(globObject.getString(MESSAGE));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public static DashboardModel getDashboardDetail(Context context, String response, String userId, String profileId) {
        DashboardModel jsonModel = new DashboardModel();
        jsonModel.setOutput(ERROR_CODE);
        jsonModel.setOutputMsg(ERROR_MSG);
        if (response.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(response);
            return jsonModel;
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString(STATUS).equals(Const.SUCCESS)) {
                        jsonModel.setOutput(Const.SUCCESS);
                        JSONObject msgObj = mainObject.optJSONObject(MESSAGE);
                        jsonModel.setSales(msgObj.optString("totalSales"));
                        jsonModel.setPurchases(msgObj.optString("totalPurchases"));
                        jsonModel.setCredit(msgObj.optString("totalCredit"));
                        jsonModel.setDebit(msgObj.optString("totalDebit"));
                        jsonModel.setTaxRecieved(msgObj.optString("totalTaxRecieved"));
                        jsonModel.setInputTaxCredit(msgObj.optString("totalInputTaxCredit"));
                        jsonModel.setTotalInvoices(msgObj.optString("totalInvoices"));
                        jsonModel.setTotalCancelledInvoices(msgObj.optString("totalCancelledInvoices"));
                        jsonModel.setStatus(msgObj.optString("status"));
                        jsonModel.setMessage(msgObj.optString("message"));
                        JSONArray msgArray = msgObj.optJSONArray("companyDetails");
                        if(msgArray!=null) {
                            AppUser.setCompanyDetails(context, msgArray.toString());
                            for (int i = 0; i < msgArray.length(); i++) {
                                JSONObject compObj = msgArray.getJSONObject(i);
                                if (compObj.optString("id").equals(profileId) && compObj.optString("userId").equals(userId)) {
                                    jsonModel.setBusinessType(compObj.optString("businessType"));
                                    jsonModel.setCompanyName(compObj.optString("companyName"));
                                    jsonModel.setSignature(compObj.optString("signature"));
                                    AppUser.setName(context,compObj.optString("companyName"));
                                    AppUser.setImage(context,compObj.optString("logo"));
                                    AppUser.setSignature(context,compObj.optString("signature"));
                                }
                            }
                        }
                    } else if (mainObject.getString(STATUS).equals(Const.FAILURE)) {
                        jsonModel.setOutput(Const.FAILURE);
                        jsonModel.setOutputMsg(mainObject.getString(MESSAGE));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }
}