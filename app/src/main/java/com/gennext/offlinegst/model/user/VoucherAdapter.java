package com.gennext.offlinegst.model.user;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.panel.ActionMenuInvoice;
import com.gennext.offlinegst.user.vouchers.Vouchers;

import java.util.ArrayList;

public class VoucherAdapter extends RecyclerView.Adapter<VoucherAdapter.ReyclerViewHolder>
        implements ActionMenuInvoice.MenuVouchers {

    private final Vouchers parentRef;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<VoucherModel> items;

    public VoucherAdapter(Activity context, ArrayList<VoucherModel> items,Vouchers parentRef) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef = parentRef;
        this.items = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_user_seller, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final VoucherModel item = items.get(position);

        holder.tvVoucherNumber.setText(item.getVoucherNo());
        holder.tvInvDate.setText(item.getVoucherDate());
        holder.tvVendorName.setText(item.getCustomerName());
        holder.tvPlaceOfSupply.setText(item.getPlaceofSupply());

        String produsts="";
        for (ProductModel prod:item.getProductList()){
            produsts+=prod.getProductName()+"("+prod.getQuantity()+"), ";
        }
        holder.tvProducts.setText(produsts);

        holder.llSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActionMenuInvoice.newInstance(VoucherAdapter.this,item,position)
                        .show(parentRef.getFragmentManager(),"actionMenu");
            }
        });
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onEditClick(DialogFragment dialog, VoucherModel itemVouchers, int position) {
        parentRef.editVouchers(itemVouchers);
    }

    @Override
    public void onDeleteClick(DialogFragment dialog, VoucherModel itemVouchers, int position) {
        parentRef.deleteVouchers(itemVouchers);
        items.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position,getItemCount());
    }

    @Override
    public void onCopyClick(DialogFragment dialog, VoucherModel itemVouchers, int position) {
        parentRef.copyVouchers(itemVouchers);
    }

    @Override
    public void onViewClick(DialogFragment dialog, VoucherModel itemVouchers, int position) {
        parentRef.viewVoucher(itemVouchers);
    }

    @Override
    public void onPrintClick(DialogFragment dialog, VoucherModel itemVouchers, int position) {
        parentRef.printVoucher(itemVouchers);
    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout llSlot;
        private TextView tvVoucherNumber,tvInvDate,tvVendorName,tvPlaceOfSupply,tvProducts;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSlot = (LinearLayout) v.findViewById(R.id.layoutSlot);
            tvVoucherNumber = (TextView) v.findViewById(R.id.tv_slot_1);
            tvInvDate = (TextView) v.findViewById(R.id.tv_slot_2);
            tvVendorName = (TextView) v.findViewById(R.id.tv_slot_3);
            tvPlaceOfSupply = (TextView) v.findViewById(R.id.tv_slot_4);
            tvProducts = (TextView) v.findViewById(R.id.tv_slot_5);
        }
    }
}

