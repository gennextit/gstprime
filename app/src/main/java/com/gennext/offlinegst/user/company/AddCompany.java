package com.gennext.offlinegst.user.company;


/**
 * Created by Admin on 5/22/2017.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.gennext.offlinegst.MainActivity;
import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.PopupAlert;
import com.gennext.offlinegst.model.app.Model;
import com.gennext.offlinegst.model.user.CommonModel;
import com.gennext.offlinegst.model.user.CompanyModel;
import com.gennext.offlinegst.model.user.StateModel;
import com.gennext.offlinegst.panel.BusinessTypeSelector;
import com.gennext.offlinegst.panel.StateSelector;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.ApiCallError;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.FieldValidation;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.ProgressButtonRounded;
import com.gennext.offlinegst.util.ProgressRounded;
import com.gennext.offlinegst.util.RequestBuilder;
import com.gennext.offlinegst.util.Utility;

import java.util.Map;


public class AddCompany extends CompactFragment implements BusinessTypeSelector.SelectListener, ApiCallError.ErrorTaskWithParamListener, StateSelector.SelectListener {

    private EditText etCompanyName, etTelephone, etEmail, etGSTIN, etState, etCompanyAddress, etDLNumber, etBusinessType, etTinNo, etTermsCond, etAccName, etBankName,etIFSC,swiftCode, etAccNumber, etBranchAddress,etTurnOver,etInvoicePrefix,etInvoiceStart;
    private ProgressButtonRounded btnAction;
    private DBManager dbManager;
    private AssignTask assignTask;
    private String mSelectedStateId;
    private ManageProfile manageProfile;
    private ProgressRounded pBar;
    private GetCompanyDetails refreshTask;
    private String mRefreshTask;
    private Map<String, String> defaultStateList;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
        if (refreshTask != null) {
            refreshTask.onAttach(context);
        }

    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
        if (refreshTask != null) {
            refreshTask.onDetach();
        }

    }


    public static Fragment newInstance() {
        AddCompany fragment = new AddCompany();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    public static Fragment newInstance(ManageProfile manageProfile) {
        AddCompany fragment = new AddCompany();
        fragment.manageProfile=manageProfile;
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.user_company_detail, container, false);
        initToolBar(getActivity(), v, getString(R.string.company));
        screenAnalytics(getContext(), AppUser.getUserId(getContext()), "Company Detail");
        dbManager = DBManager.newIsntance(getActivity());
        defaultStateList = AppUser.defaultStates(getContext());
        initUI(v);
        updateUi();
        return v;
    }

    private void updateUi() {
        etCompanyName.setText(AppUser.getName(getContext()));
        btnAction.setText(getString(R.string.add_company));
    }

    private void refreshCompanyDetail(String task) {
        mRefreshTask=task;
        refreshTask=new GetCompanyDetails(getContext(),task);
        refreshTask.execute();
    }


    private void initUI(View v) {
        etCompanyName = (EditText) v.findViewById(R.id.et_company_name);
        etTelephone = (EditText) v.findViewById(R.id.et_company_telephone);
        etEmail = (EditText) v.findViewById(R.id.et_company_email);
        etGSTIN = (EditText) v.findViewById(R.id.et_company_gstin);
        etState = (EditText) v.findViewById(R.id.et_company_state);
        etCompanyAddress = (EditText) v.findViewById(R.id.et_company_address);
        etDLNumber = (EditText) v.findViewById(R.id.et_company_dlno);
        etBusinessType = (EditText) v.findViewById(R.id.et_company_business_type);
        etTinNo = (EditText) v.findViewById(R.id.et_company_tin);
        etTermsCond = (EditText) v.findViewById(R.id.et_company_tc);
        etTurnOver = (EditText) v.findViewById(R.id.et_company_turnover);
        etInvoicePrefix = (EditText) v.findViewById(R.id.et_company_inv_prefix);
        etInvoiceStart = (EditText) v.findViewById(R.id.et_company_inv_started_from);
        pBar = ProgressRounded.newInstance(getContext(), v);

        etAccName = (EditText) v.findViewById(R.id.et_bank_account_name);
        etBankName = (EditText) v.findViewById(R.id.et_bank_bank_name);
        etIFSC = (EditText) v.findViewById(R.id.et_bank_ifsc);
        swiftCode = (EditText) v.findViewById(R.id.et_bank_swift);
        etAccNumber = (EditText) v.findViewById(R.id.et_bank_acc_number);
        etBranchAddress = (EditText) v.findViewById(R.id.et_bank_branch_address);

        btnAction = ProgressButtonRounded.newInstance(getContext(), v);
        btnAction.setOnEditorActionListener(etBranchAddress);

        etBusinessType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BusinessTypeSelector.newInstance(AddCompany.this).show(getFragmentManager(),"businessTypeSelector");
            }
        });

        etState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mSelectedStateId==null){
                    showToast(getContext(), getString(R.string.invalid_gst_number2));
                }
//                StateSelector.newInstance(AddCompany.this).show(getFragmentManager(), "stateSelector");
            }
        });
        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FieldValidation.isEmpty(getContext(), etCompanyName, getString(R.string.invalid_company_name))) {
                    showToast(getContext(), getString(R.string.invalid_company_name));
                    return;
                } else if (!FieldValidation.isGST(getContext(), etGSTIN, getString(R.string.invalid_gst_number),true)) {
                    showToast(getContext(), getString(R.string.invalid_gst_number));
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), etState, getString(R.string.invalid_state))) {
                    showToast(getContext(), getString(R.string.invalid_state));
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), etCompanyAddress, getString(R.string.invalid_address))) {
                    showToast(getContext(), getString(R.string.invalid_address));
                    return;
                }else if (!FieldValidation.isEmpty(getContext(), etBusinessType, getString(R.string.invalid_business_type))) {
                    showToast(getContext(), getString(R.string.invalid_business_type));
                    return;
                }
                hideKeybord(getActivity());
                executeTask(etCompanyName.getText().toString(), etTelephone.getText().toString(), etEmail.getText().toString(), etGSTIN.getText().toString()
                        , etState.getText().toString(), mSelectedStateId, etCompanyAddress.getText().toString(), etDLNumber.getText().toString(), etTermsCond.getText().toString()
                        , etAccName.getText().toString(), etIFSC.getText().toString(), swiftCode.getText().toString(), etAccNumber.getText().toString(), etBranchAddress.getText().toString()
                        ,etBusinessType.getText().toString(),etTinNo.getText().toString(),etBankName.getText().toString(),etTurnOver.getText().toString(),etInvoicePrefix.getText().toString(),etInvoiceStart.getText().toString());
            }
        });
//        etGSTIN.setImeOptions(EditorInfo.IME_ACTION_NEXT);
//        etGSTIN.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId,
//                                          KeyEvent event) {
//                boolean handled = false;
//                if (actionId == EditorInfo.IME_ACTION_NEXT) {
//                    // Handle pressing "Enter" key here
////                    StateSelector.newInstance(AddCompany.this).show(getFragmentManager(), "stateSelector");
//                    handled = true;
//                }
//                return handled;
//            }
//        });
        etGSTIN.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setStateInUi(etGSTIN.getText().toString());
            }
        });

//        etGSTIN.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                setStateInUi(etGSTIN.getText().toString());
//            }
//        });
        etCompanyAddress.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        etCompanyAddress.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    // Handle pressing "Enter" key here
                    BusinessTypeSelector.newInstance(AddCompany.this).show(getFragmentManager(),"businessTypeSelector");
                    handled = true;
                }
                return handled;
            }
        });

    }

    private void setStateInUi(String gstinNo) {
        if(gstinNo.length()==15){
            String stateCode = gstinNo.substring(0, 2);
            int sCode= Utility.parseInt(stateCode);
            String stateName = AppUser.getStateName(defaultStateList, String.valueOf(sCode));
            if(!TextUtils.isEmpty(stateName)){
                mSelectedStateId=stateCode;
                etState.setText(stateName);
            }else{
                mSelectedStateId=null;
                etState.setText(stateName);
            }
        }else{
            mSelectedStateId=null;
            etState.setText("");
        }
    }

    private void executeTask(String companyName, String telephone, String email, String gstin, String state, String stateCode, String address, String dlNumber, String tAndC, String accName, String ifscCode, String swiftCode, String accNumber, String branchAddress, String businessType, String tinNo, String bankName,String turnover, String invoicePrefix, String invoiceStart) {
        assignTask = new AssignTask(getActivity(), valString(companyName), valString(telephone), valString(email), valString(gstin), valString(state), valString(stateCode), valString(address), valString(dlNumber), valString(tAndC), valString(accName)
                , valString(ifscCode), valString(swiftCode), valString(accNumber), valString(branchAddress), valString(businessType), valString(tinNo), valString(bankName), valString(turnover), valString(invoicePrefix), valString(invoiceStart));
        assignTask.execute(AppSettings.SAVE_COMPANY_DETAIL);
    }


    @Override
    public void onErrorRetryClick(DialogFragment dialog, int task, String[] param) {
        if(task==TASK_ACTION){
            executeTask(param[0], param[1], param[2], param[3], param[4], param[5], param[6], param[7], param[8], param[9], param[10], param[11], param[12], param[13], param[14], param[15], param[16], param[17], param[18], param[19]);
        }else{
            refreshCompanyDetail(mRefreshTask);
        }
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog, int task, String[] param) {

    }

    @Override
    public void onStateSelect(DialogFragment dialog, StateModel selectedState) {
        mSelectedStateId = selectedState.getStateCode();
        etState.setText(selectedState.getStateName());
        FieldValidation.isEmpty(getContext(), etState, getString(R.string.invalid_state));
        etCompanyAddress.requestFocus();
        showKeybord(getActivity());
    }

    @Override
    public void onBusinessTypeSelect(DialogFragment dialog, CommonModel selectedItem) {
        etBusinessType.setText(selectedItem.getName());
        etTurnOver.requestFocus();
        showKeybord(getActivity());
    }



    private class AssignTask extends AsyncTask<String, Void, CompanyModel> {
        private final String companyName, telephone, email, gstin, state, stateCode, address, dlNumber, tAndC, accName, ifscCode,swiftCode, accNumber, branchAddress,businessType,tinNo,bankName,turnover, invoicePrefix, invoiceStart;
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context, String companyName, String telephone, String email, String gstin, String state, String stateCode, String address
                , String dlNumber, String tAndC, String accName, String ifscCode, String swiftCode, String accNumber, String branchAddress
                , String businessType, String tinNo, String bankName,String turnover, String invoicePrefix, String invoiceStart) {
            this.companyName = companyName;
            this.telephone = telephone;
            this.email = email;
            this.gstin = gstin;
            this.state = state;
            this.stateCode = stateCode;
            this.address = address;
            this.dlNumber = dlNumber;
            this.tAndC = tAndC;
            this.accName = accName;
            this.ifscCode = ifscCode;
            this.swiftCode = swiftCode;
            this.accNumber = accNumber;
            this.branchAddress = branchAddress;
            this.businessType = businessType;
            this.tinNo = tinNo;
            this.bankName = bankName;
            this.turnover =turnover;
            this.invoicePrefix =invoicePrefix;
            this.invoiceStart =invoiceStart;
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            btnAction.startAnimation();
        }

        @Override
        protected CompanyModel doInBackground(String... urls) {
            CompanyModel result = new CompanyModel();
            String userId = AppUser.getUserId(context);
            String response = ApiCall.POST(urls[0], RequestBuilder.addCompanyDetail(userId, companyName, telephone, email, gstin, state, stateCode, address, dlNumber, tAndC, accName, ifscCode, swiftCode, accNumber, branchAddress, businessType, tinNo, bankName, turnover, invoicePrefix, invoiceStart));
            return JsonParser.addCompanyDetail(response);
        }


        @Override
        protected void onPostExecute(CompanyModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                if (result.getOutput().equals(Const.SUCCESS)) {
                    if(manageProfile==null) {
                        AppUser.setCompanyId(context, result.getCompanyId());
                        AppUser.setProfileId(context, result.getCompanyId());
                        AppUser.setName(context, result.getCompanyName());
                        AppUser.setImage(context, result.getLogo());
                    }
                    refreshCompanyDetail("completed");
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    btnAction.revertAnimation();
                    PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                            .show(getFragmentManager(), "popupAlert");
                } else {
                    btnAction.revertAnimation();
                    String[] errorSoon = {companyName, telephone, email, gstin, state, stateCode, address, dlNumber, tAndC, accName, ifscCode, swiftCode, accNumber, branchAddress, businessType, tinNo, bankName, turnover, invoicePrefix, invoiceStart};
                    ApiCallError.newInstance(result.getOutput(),result.getOutputMsg(), TASK_ACTION, errorSoon, AddCompany.this)
                            .show(getFragmentManager(), "apiCallError");
                }
            }
        }


    }

    private void animateButtonAndRevert(final Context context) {
        Handler handler = new Handler();

        Runnable runnableRevert = new Runnable() {
            @Override
            public void run() {
                if (context != null) {
                    if(manageProfile!=null) {
                        manageProfile.refreshList();
                        getActivity().onBackPressed();
                    }else{
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }
                }
            }
        };

        btnAction.revertSuccessAnimation();
        handler.postDelayed(runnableRevert, Const.BUTTON_PROGRESS_TIME);
    }

    private class GetCompanyDetails extends AsyncTask<String, Void, Model> {

        private final String task;
        private Context context;
        private ProgressDialog pDialog;

        public void onAttach(Context context) {
            this.context=context;
        }
        public void onDetach() {
            this.context=null;
        }

        public GetCompanyDetails(Context context, String task) {
            this.context = context;
            this.task = task;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pBar.showProgressBar();

//            pDialog = new ProgressDialog(context);
////            pDialog.setTitle(getString(R.string.generating_invoice_pdf));
//            pDialog.setMessage(getString(R.string.please_wait_invoice_generate));
//            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//            pDialog.show();
        }

        @Override
        protected Model doInBackground(String... params) {
            String userId = AppUser.getUserId(getActivity());
            String response = ApiCall.POST(AppSettings.GET_COMPANY_DETAILS, RequestBuilder.Default(userId));
            return JsonParser.refreshCompanyDetail(context,response);
        }

        @Override
        protected void onPostExecute(Model result) {
            super.onPostExecute(result);
            if(context!=null) {
//                if (pDialog != null) {
//                    pDialog.dismiss();
//                }
                pBar.hideProgressBar();
                if (result.getOutput().equals(Const.SUCCESS)) {
                    animateButtonAndRevert(context);
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                            .show(getFragmentManager(), "popupAlert");
                } else {
                    try {
                        ApiCallError.newInstance(result.getOutput(), result.getOutputMsg(),TASK_LOAD_LIST,new String[0], AddCompany.this)
                                .show(getFragmentManager(), "apiCallError");
                    }catch (NullPointerException e){
                        e.printStackTrace();
                    }
                }
            }
        }

    }

}
