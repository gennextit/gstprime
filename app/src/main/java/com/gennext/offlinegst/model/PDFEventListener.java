package com.gennext.offlinegst.model;

/**
 * Created by Admin on 11/21/2017.
 */

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;


public class PDFEventListener extends PdfPageEventHelper
{

    private final int size;
    private final int sizeX;
    private final int sizeY;

    public PDFEventListener() {
        this.size=70;
        this.sizeX=295;
        this.sizeY=600;
    }

    public PDFEventListener(int i, int sizeX, int sizeY) {
        this.size=i;
        this.sizeX=sizeX;
        this.sizeY=sizeY;
    }

    @Override
    public void onEndPage(PdfWriter writer, Document document)
    {
        PdfContentByte canvas = writer.getDirectContentUnder();
//        Phrase watermark = new Phrase("CANCELLED", new Font(FontFamily.TIMES_ROMAN, 100, Font.NORMAL, BaseColor.RED));
        Phrase watermark = new Phrase("CANCELLED", new Font(FontFamily.TIMES_ROMAN, size, Font.NORMAL, new BaseColor(238, 205, 204)));
        ColumnText.showTextAligned(canvas, Element.ALIGN_CENTER, watermark, sizeX, sizeY, 25);
    }
}

