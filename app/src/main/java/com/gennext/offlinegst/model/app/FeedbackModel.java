package com.gennext.offlinegst.model.app;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by Admin on 8/16/2017.
 */

@IgnoreExtraProperties
public class FeedbackModel {

    public String username;
    public String feedback;

    public FeedbackModel() {
        // Default constructor required for calls to DataSnapshot.getValue(FeedbackModel.class)
    }

    public FeedbackModel(String username, String feedback) {
        this.feedback = feedback;
        this.username = username;
    }

}