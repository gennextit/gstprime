package com.gennext.offlinegst.setting;

/**
 * Created by Admin on 7/4/2017.
 */

public class Const {

    // also check file xml/remote_config_defaults
//    public static final String WHATS_NEW = ""
//            + "1. This update contains bug fixes and can not affect your last saved data.\n"
//            + "2. Backup option is now available in setting menu.";

    public static final String WHATS_NEW = "whats_new";

    public static final String PROFILE_NAME = "name";
    public static final String PROFILE_EMAIL = "email";
    public static final String PROFILE_MOBILE = "mobile";
    public static final String PROFILE_PAN = "pan";
    public static final String PROFILE_IMAGE_PATH = "imagePath";
    public static final String PROFILE_IMAGE_URL = "imageUrl";
    public static final String PROFILE_USER_ID = "userId";

    public static final String DELETE_PROFILE = "profile";
    public static final String DELETE_PRODUCT = "product";
    public static final String DELETE_SERVICES = "services";
    public static final String DELETE_CUSTOMER = "customer";
    public static final String DELETE_VENDOR = "vendor";
    public static final String DELETE_PURCHASE_INVOICES = "PurchaseInvoices";
    public static final String DELETE_SELL_INVOICES = "SELLInvoices";
    public static final String DELETE_COMPANY = "company";
    public static final int MENU_PRODUCT = 1;
    public static final int MENU_PURCHASES = 2;
    public static final int MENU_INVOICES = 3;
    public static final String DELETE_PRODUCT_STOCK = "stock";
    public static final int VIEW_PDF = 1;
    public static final int PRINT_PDF = 2;
    public static final int SHARE_PDF = 3;
    public static final int SEND_EMAIL_PDF = 4;

    public static final int MENU_VOUCHERS = 4;
    public static final String DELETE_VOUCHERS = "delete_vouchers";
    public static final String FOOTER_PDF_1 = "@ GSTPrime";
    public static final String FOOTER_PDF_2 = "    Website:www.gennextit.com";
    public static final String FOOTER_PDF_3 = "    Email:info@gennextit.com";
    public static final String FOOTER_PDF_4 = "    Mobile: +91-7840079095";

    // Remote Config keys
    public static final String VERSION_CODE = "version_code";
    public static final String VERSION_NAME = "version_name";
    public static final String RELEASE_DATE = "release_date";
    public static final String RATING_STATUS = "rating_status";

    public static final long BUTTON_PROGRESS_TIME = 1000;
    public static final String VERSION_UPDATE_MESSAGE = "version_update_message";
    public static final String CREDIT = "credit";
    public static final String SUCCESS = "success";
    public static final String FAILURE = "error";
    public static final String RECEIVE = "Receive";
    public static final String RETURN = "Return";

    //Business types
    public static final String TRADER_BUSINESS_TYPE = "trader";
    public static final String BOTH_BUSINESS_TYPE = "both";
    public static final String SERVICE_BUSINESS_TYPE = "service provider";
    public static final String PAID = "paid";
    public static final String TRIAL = "trial";
    public static final String EXPIRED = "expired";
    public static final String CASH_PURCHASE = "Cash Purchase";
    public static final String YTD = "YTD";
    public static final String MTD = "MTD";
}
