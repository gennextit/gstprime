package com.gennext.offlinegst.panel;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.PopupAlert;
import com.gennext.offlinegst.model.user.CustomerModel;
import com.gennext.offlinegst.model.user.CustomerSelectorAdapter;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.user.customer.CustomerManager;
import com.gennext.offlinegst.user.invoice.DebtorsTransaction;
import com.gennext.offlinegst.user.invoice.InvoiceManager;
import com.gennext.offlinegst.user.services.sale.ServiceManager;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.ApiCallError;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.ProgressRounded;
import com.gennext.offlinegst.util.RequestBuilder;

import java.util.ArrayList;


public class CustomerSelector extends BottomSheetDialogFragment implements ApiCallError.ErrorParamListener{
    private SelectListener mListener;
    private CustomerSelectorAdapter adapter;
//    private DBManager dbManager;
    private InvoiceManager invoiceManager;
    private DebtorsTransaction debtorsTransaction;
    private AssignTask assignTask;
    private ProgressRounded pBar;
    private ListView lvMain;
    private ServiceManager serviceManager;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }
    public interface SelectListener {
        void onCustomerSelect(DialogFragment dialog, CustomerModel selectedCustomer);

    }


    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }


    public static CustomerSelector newInstance(InvoiceManager invoiceManager) {
        CustomerSelector fragment = new CustomerSelector();
        fragment.mListener = invoiceManager;
        fragment.invoiceManager = invoiceManager;
        return fragment;
    }

    public static CustomerSelector newInstance(DebtorsTransaction debtorsTransaction) {
        CustomerSelector fragment = new CustomerSelector();
        fragment.mListener = debtorsTransaction;
        fragment.debtorsTransaction = debtorsTransaction;
        return fragment;
    }


    public static CustomerSelector newInstance(ServiceManager serviceManager) {
        CustomerSelector fragment = new CustomerSelector();
        fragment.mListener = serviceManager;
        fragment.serviceManager = serviceManager;
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_menu_customer, container, false);
//        dbManager = DBManager.newIsntance(getActivity());
        ImageButton btnAction = (ImageButton) v.findViewById(R.id.btn_action);
        EditText etSearch = (EditText) v.findViewById(R.id.et_search);

        lvMain = (ListView) v.findViewById(R.id.lv_main);
        pBar = ProgressRounded.newInstance(getContext(), v);

        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(invoiceManager!=null) {
                    addFragment(CustomerManager.newInstance(invoiceManager, null, CustomerManager.ADD_CUSTOMER_WITH_RESPONSE), android.R.id.content, "customerManager");
                }
                if(debtorsTransaction!=null) {
                    addFragment(CustomerManager.newInstance(debtorsTransaction, null, CustomerManager.ADD_CUSTOMER_WITH_RESPONSE), android.R.id.content, "customerManager");
                }
                if(serviceManager!=null) {
                    addFragment(CustomerManager.newInstance(serviceManager, null, CustomerManager.ADD_CUSTOMER_WITH_RESPONSE), android.R.id.content, "customerManager");
                }
                dismiss();
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (adapter != null)
                    adapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                if (adapter != null) {
                    CustomerModel model = adapter.getItem(pos);
                    if (model != null) {
                        if (mListener != null) {
                            mListener.onCustomerSelect(CustomerSelector.this, model);
                        }
                        dismiss();
                    }
                } else {
                    if(invoiceManager!=null) {
                        addFragment(CustomerManager.newInstance(invoiceManager, null, CustomerManager.ADD_CUSTOMER_WITH_RESPONSE), android.R.id.content, "customerManager");
                    }
                    if(debtorsTransaction!=null) {
                        addFragment(CustomerManager.newInstance(debtorsTransaction, null, CustomerManager.ADD_CUSTOMER_WITH_RESPONSE), android.R.id.content, "customerManager");
                    }
                    dismiss();
                }
            }
        });
//        CustomerModel cModel = dbManager.getCustomerList(new CustomerModel(), AppUser.getUserId(getActivity()));
//        if (cModel != null && cModel.getOutputDB().equals(Const.SUCCESS)) {
//            adapter = new CustomerSelectorAdapter(getActivity(), cModel.getList());
//            lvMain.setAdapter(adapter);
//        } else {
//            ArrayList<String> errorList = new ArrayList<>();
//            errorList.add(getString(R.string.customer_not_found));
//            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.slot_empty_product,
//                    R.id.tv_message, errorList);
//            lvMain.setAdapter(adapter);
//        }
        executeTask();

        return v;
    }

    public void addFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }


    private void executeTask() {
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.GET_CUSTOMERS);
    }


    @Override
    public void onErrorRetryClick(DialogFragment dialog, String[] param) {
        executeTask();
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog, String[] param) {
        dismiss();
    }


    private class AssignTask extends AsyncTask<String, Void, CustomerModel> {
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pBar.showProgressBar();
        }

        @Override
        protected CustomerModel doInBackground(String... urls) {
            String userId = AppUser.getUserId(context);
            String setupId = AppUser.getCompanyId(context);
            String response = ApiCall.POST(urls[0], RequestBuilder.Default(userId,setupId));
            return JsonParser.getCustomerDetail(context,new CustomerModel(),response);
        }


        @Override
        protected void onPostExecute(CustomerModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                pBar.hideProgressBar();
                if (result.getOutput().equals(Const.SUCCESS)) {
                    adapter = new CustomerSelectorAdapter(getActivity(), result.getList());
                    lvMain.setAdapter(adapter);
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    ArrayList<String> errorList = new ArrayList<>();
//                    errorList.add(getString(R.string.employee_not_found));
                    errorList.add(result.getOutputMsg());
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.slot_empty_product,
                            R.id.tv_message, errorList);
                    lvMain.setAdapter(adapter);
                    PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                            .show(getFragmentManager(), "popupAlert");
                } else {
                    String[] errorSoon = {};
                    ApiCallError.newInstance(result.getOutput(), result.getOutputMsg(), errorSoon, CustomerSelector.this)
                            .show(getFragmentManager(), "apiCallError");
                }
            }
        }
    }

}
