package com.gennext.offlinegst.model;

import android.app.Activity;

import com.gennext.offlinegst.model.user.InvoiceModel;
import com.gennext.offlinegst.model.user.ProductModel;
import com.gennext.offlinegst.model.user.PurchasesModel;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.DateTimeUtility;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Admin on 8/10/2017.
 */

public class EXELMaker {

    private Activity mContext;

    public static EXELMaker newInstance(Activity mContext) {
        EXELMaker exelMaker = new EXELMaker();
        exelMaker.mContext = mContext;
        return exelMaker;
    }

    public File exportProductInExel(ArrayList<ProductModel> prodList) {
        String userId = AppUser.getUserId(mContext);
        String fileName = "Product_List_" + userId + DateTimeUtility.getTimeStamp() + ".xls";

        if (prodList==null) {
            return null;
        }

        //New Workbook
        Workbook wb = new HSSFWorkbook();

        Cell cell = null;

        //Cell style for header row
        CellStyle cs = wb.createCellStyle();
        cs.setFillForegroundColor(HSSFColor.LIME.index);
        cs.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        //New Sheet
        Sheet sheet1 = null;
        sheet1 = wb.createSheet("Products");

        // Generate column headings
        Row row = sheet1.createRow(0);

        createHeaderRowColumn(sheet1, cell, row, cs, 100, 0, "S.No.");
        createHeaderRowColumn(sheet1, cell, row, cs, 140, 1, "productId");
        createHeaderRowColumn(sheet1, cell, row, cs, 200, 2, "userId");
        createHeaderRowColumn(sheet1, cell, row, cs, 500, 3, "productName");
        createHeaderRowColumn(sheet1, cell, row, cs, 160, 4, "codeHSN");
        createHeaderRowColumn(sheet1, cell, row, cs, 250, 5, "barcode");
        createHeaderRowColumn(sheet1, cell, row, cs, 130, 6, "costPrice");
        createHeaderRowColumn(sheet1, cell, row, cs, 130, 7, "salePrice");
        createHeaderRowColumn(sheet1, cell, row, cs, 130, 8, "taxRate");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 9, "fromDate");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 10, "toDate");

        Row multiRow;
        for (int rowPos = 1; rowPos <= prodList.size(); rowPos++) {
            int listPos = rowPos - 1;
            // Generate column headings
            multiRow = sheet1.createRow(rowPos);
            setRowEntry(cell, multiRow, 0, String.valueOf(rowPos));
            setRowEntry(cell, multiRow, 1, prodList.get(listPos).getProductId());
            setRowEntry(cell, multiRow, 2, prodList.get(listPos).getProductUserId());
            setRowEntry(cell, multiRow, 3, prodList.get(listPos).getProductName());
            setRowEntry(cell, multiRow, 4, prodList.get(listPos).getCodeHSN());
            setRowEntry(cell, multiRow, 5, prodList.get(listPos).getBarcode());
            setRowEntry(cell, multiRow, 6, prodList.get(listPos).getCostPrice());
            setRowEntry(cell, multiRow, 7, prodList.get(listPos).getSalePrice());
            setRowEntry(cell, multiRow, 8, prodList.get(listPos).getTaxRate());
            setRowEntry(cell, multiRow, 9, prodList.get(listPos).getFromDate());
            setRowEntry(cell, multiRow, 10, prodList.get(listPos).getToDate());
        }

        File myExternalFile = new File(mContext.getExternalFilesDir(AppConfig.FOLDER_XLS), fileName);
        FileOutputStream fileOut = null;
        try {
            fileOut = new FileOutputStream(myExternalFile);
            wb.write(fileOut);
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        System.out.print("file created");

        return myExternalFile;
    }

    private Cell createHeaderRowColumn(Sheet sheet1, Cell cell, Row row, CellStyle cs, int width, int columnPosition, String columnName) {
        cell = row.createCell(columnPosition);
        cell.setCellValue(columnName);
        cell.setCellStyle(cs);
        sheet1.setColumnWidth(columnPosition, (15 * width));
        return cell;
    }

    private Cell setRowEntry(Cell cell, Row multiRow, int listPos, String listValue) {
        cell = multiRow.createCell(listPos);
        cell.setCellValue(listValue);
        return cell;
    }

    public File exportPurchasesInExelFormat1(DBManager dbManager) {
        String userId = AppUser.getUserId(mContext);
        String fileName = "Purchase_List_" + userId + DateTimeUtility.getTimeStamp() + ".xls";
        PurchasesModel purchasesModel = dbManager.getPurchasesList(new PurchasesModel(), AppUser.getUserId(mContext), AppUser.getProfileId(mContext));

        if (!purchasesModel.getOutputDB().equals(Const.SUCCESS)) {
            return null;
        }

        //New Workbook
        Workbook wb = new HSSFWorkbook();

        Cell cell = null;

        //Cell style for header row
        CellStyle cs = wb.createCellStyle();
        cs.setFillForegroundColor(HSSFColor.LIME.index);
        cs.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        //New Sheet
        Sheet sheet1 = null;
        Row row;
        sheet1 = wb.createSheet("Purchase");

        // Generate column headings
        row = sheet1.createRow(0);

        createHeaderRowColumn(sheet1, cell, row, cs, 100, 0, "S.No.");
        createHeaderRowColumn(sheet1, cell, row, cs, 140, 1, "InvoiceId");
        createHeaderRowColumn(sheet1, cell, row, cs, 200, 2, "UserId");
        createHeaderRowColumn(sheet1, cell, row, cs, 140, 3, "ProfileId");
        createHeaderRowColumn(sheet1, cell, row, cs, 250, 4, "InvoiceNumber");
        createHeaderRowColumn(sheet1, cell, row, cs, 250, 5, "InvoiceDate");
        createHeaderRowColumn(sheet1, cell, row, cs, 130, 6, "VendorId");
        createHeaderRowColumn(sheet1, cell, row, cs, 250, 7, "VendorName");
        createHeaderRowColumn(sheet1, cell, row, cs, 240, 8, "FreightAmount");
        createHeaderRowColumn(sheet1, cell, row, cs, 240, 9, "PlaceOfPurchase");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 10, "TotalAmount");

        Row multiRow;
        ArrayList<PurchasesModel> purchaseList = purchasesModel.getList();
        for (int rowPos = 1; rowPos <= purchaseList.size(); rowPos++) {
            int listPos = rowPos - 1;
            // Generate column headings
            multiRow = sheet1.createRow(rowPos);
            setRowEntry(cell, multiRow, 0, String.valueOf(rowPos));
            setRowEntry(cell, multiRow, 1, purchaseList.get(listPos).getInvoiceId());
            setRowEntry(cell, multiRow, 2, purchaseList.get(listPos).getInvoiceUserId());
            setRowEntry(cell, multiRow, 3, purchaseList.get(listPos).getInvoiceProfileId());
            setRowEntry(cell, multiRow, 4, purchaseList.get(listPos).getInvoiceNumber());
            setRowEntry(cell, multiRow, 5, purchaseList.get(listPos).getInvoiceDate());
            setRowEntry(cell, multiRow, 6, purchaseList.get(listPos).getVendorId());
            setRowEntry(cell, multiRow, 7, purchaseList.get(listPos).getVendorName());
            setRowEntry(cell, multiRow, 8, purchaseList.get(listPos).getFreightAmount());
            setRowEntry(cell, multiRow, 9, purchaseList.get(listPos).getPlaceOfPurchase());
            setRowEntry(cell, multiRow, 10, purchaseList.get(listPos).getTotalAmount());
        }

        //New Sheet
        sheet1 = wb.createSheet("Products");

        // Generate column headings
        row = sheet1.createRow(0);

        createHeaderRowColumn(sheet1, cell, row, cs, 100, 0, "S.No.");
        createHeaderRowColumn(sheet1, cell, row, cs, 140, 1, "id");
        createHeaderRowColumn(sheet1, cell, row, cs, 200, 2, "UserId");
        createHeaderRowColumn(sheet1, cell, row, cs, 140, 3, "ProfileId");
        createHeaderRowColumn(sheet1, cell, row, cs, 250, 4, "InvoiceNumber");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 5, "Quantity");
        createHeaderRowColumn(sheet1, cell, row, cs, 140, 6, "ProductId");
        createHeaderRowColumn(sheet1, cell, row, cs, 500, 7, "ProductName");
        createHeaderRowColumn(sheet1, cell, row, cs, 140, 8, "CodeHSN");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 9, "Barcode");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 10, "CostPrice");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 11, "SalePrice");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 12, "TaxRate");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 13, "FromDate");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 14, "ToDate");

        purchaseList = purchasesModel.getList();
        int sheetRowPos = 0;
        for (PurchasesModel model : purchaseList) {
            ArrayList<ProductModel> prodList = model.getProductList();
            for (int listPos = 0; listPos < prodList.size(); listPos++) {
                sheetRowPos++;
                // Generate column headings
                multiRow = sheet1.createRow(sheetRowPos);
                setRowEntry(cell, multiRow, 0, String.valueOf(sheetRowPos));
                setRowEntry(cell, multiRow, 1, prodList.get(listPos).getPurchasesProdId());
                setRowEntry(cell, multiRow, 2, prodList.get(listPos).getProductUserId());
                setRowEntry(cell, multiRow, 3, prodList.get(listPos).getProductProfileId());
                setRowEntry(cell, multiRow, 4, prodList.get(listPos).getInvNumber());
                setRowEntry(cell, multiRow, 5, prodList.get(listPos).getQuantity());
                setRowEntry(cell, multiRow, 6, prodList.get(listPos).getProductId());
                setRowEntry(cell, multiRow, 7, prodList.get(listPos).getProductName());
                setRowEntry(cell, multiRow, 8, prodList.get(listPos).getCodeHSN());
                setRowEntry(cell, multiRow, 9, prodList.get(listPos).getBarcode());
                setRowEntry(cell, multiRow, 10, prodList.get(listPos).getCostPrice());
                setRowEntry(cell, multiRow, 11, prodList.get(listPos).getSalePrice());
                setRowEntry(cell, multiRow, 12, prodList.get(listPos).getTaxRate());
                setRowEntry(cell, multiRow, 13, prodList.get(listPos).getFromDate());
                setRowEntry(cell, multiRow, 14, prodList.get(listPos).getToDate());
            }
        }

        File myExternalFile = new File(mContext.getExternalFilesDir(AppConfig.FOLDER_XLS), fileName);
        FileOutputStream fileOut = null;
        try {
            fileOut = new FileOutputStream(myExternalFile);
            wb.write(fileOut);
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        System.out.print("file created");

        return myExternalFile;
    }

    public File exportPurchasesInExelFormat2(DBManager dbManager) {
        String userId = AppUser.getUserId(mContext);
        String fileName = "Purchase_List_" + userId + DateTimeUtility.getTimeStamp() + ".xls";
        PurchasesModel purchasesModel = dbManager.getPurchasesList(new PurchasesModel(), AppUser.getUserId(mContext), AppUser.getProfileId(mContext));

        if (!purchasesModel.getOutputDB().equals(Const.SUCCESS)) {
            return null;
        }

        //New Workbook
        Workbook wb = new HSSFWorkbook();

        Cell cell = null;

        //Cell style for header row
        CellStyle cs = wb.createCellStyle();
        cs.setFillForegroundColor(HSSFColor.LIME.index);
        cs.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        //New Sheet
        Sheet sheet1 = null;
        Row row;
        sheet1 = wb.createSheet("Purchase");

        // Generate column headings
        row = sheet1.createRow(0);

        createHeaderRowColumn(sheet1, cell, row, cs, 100, 0, "S.No.");
        createHeaderRowColumn(sheet1, cell, row, cs, 140, 1, "InvoiceId");
        createHeaderRowColumn(sheet1, cell, row, cs, 200, 2, "UserId");
        createHeaderRowColumn(sheet1, cell, row, cs, 140, 3, "ProfileId");
        createHeaderRowColumn(sheet1, cell, row, cs, 250, 4, "InvoiceNumber");
        createHeaderRowColumn(sheet1, cell, row, cs, 250, 5, "InvoiceDate");
        createHeaderRowColumn(sheet1, cell, row, cs, 130, 6, "VendorId");
        createHeaderRowColumn(sheet1, cell, row, cs, 250, 7, "VendorName");
        createHeaderRowColumn(sheet1, cell, row, cs, 240, 8, "FreightAmount");
        createHeaderRowColumn(sheet1, cell, row, cs, 240, 9, "PlaceOfPurchase");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 10, "TotalAmount");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 11, "Quantity");
        createHeaderRowColumn(sheet1, cell, row, cs, 140, 12, "ProductId");
        createHeaderRowColumn(sheet1, cell, row, cs, 500, 13, "ProductName");
        createHeaderRowColumn(sheet1, cell, row, cs, 140, 14, "CodeHSN");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 15, "Barcode");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 16, "CostPrice");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 17, "SalePrice");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 18, "TaxRate");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 19, "FromDate");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 20, "ToDate");

        Row multiRow;
        int sheetRowPos=0;
        ArrayList<PurchasesModel> purchaseList = purchasesModel.getList();
        for (PurchasesModel model: purchaseList) {

            ArrayList<ProductModel> prodList = model.getProductList();
            for (int listPos = 0; listPos < prodList.size(); listPos++) {
                // Generate column headings
                sheetRowPos++;
                multiRow = sheet1.createRow(sheetRowPos);
                setRowEntry(cell, multiRow, 0, String.valueOf(sheetRowPos));
                setRowEntry(cell, multiRow, 1, model.getInvoiceId());
                setRowEntry(cell, multiRow, 2, model.getInvoiceUserId());
                setRowEntry(cell, multiRow, 3, model.getInvoiceProfileId());
                setRowEntry(cell, multiRow, 4, model.getInvoiceNumber());
                setRowEntry(cell, multiRow, 5, model.getInvoiceDate());
                setRowEntry(cell, multiRow, 6, model.getVendorId());
                setRowEntry(cell, multiRow, 7, model.getVendorName());
                setRowEntry(cell, multiRow, 8, model.getFreightAmount());
                setRowEntry(cell, multiRow, 9, model.getPlaceOfPurchase());
                setRowEntry(cell, multiRow, 10, model.getTotalAmount());
                setRowEntry(cell, multiRow, 11, prodList.get(listPos).getQuantity());
                setRowEntry(cell, multiRow, 12, prodList.get(listPos).getProductId());
                setRowEntry(cell, multiRow, 13, prodList.get(listPos).getProductName());
                setRowEntry(cell, multiRow, 14, prodList.get(listPos).getCodeHSN());
                setRowEntry(cell, multiRow, 15, prodList.get(listPos).getBarcode());
                setRowEntry(cell, multiRow, 16, prodList.get(listPos).getCostPrice());
                setRowEntry(cell, multiRow, 17, prodList.get(listPos).getSalePrice());
                setRowEntry(cell, multiRow, 18, prodList.get(listPos).getTaxRate());
                setRowEntry(cell, multiRow, 19, prodList.get(listPos).getFromDate());
                setRowEntry(cell, multiRow, 20, prodList.get(listPos).getToDate());
            }
        }



        File myExternalFile = new File(mContext.getExternalFilesDir(AppConfig.FOLDER_XLS), fileName);
        FileOutputStream fileOut = null;
        try {
            fileOut = new FileOutputStream(myExternalFile);
            wb.write(fileOut);
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        System.out.print("file created");

        return myExternalFile;
    }


    public File exportInvoicesInExelFormat1(DBManager dbManager) {
        String userId = AppUser.getUserId(mContext);
        String fileName = "Invoices_List_" + userId + DateTimeUtility.getTimeStamp() + ".xls";
        InvoiceModel invoiceModel = dbManager.getInvoicesList(new InvoiceModel(), AppUser.getUserId(mContext), AppUser.getProfileId(mContext));

        if (!invoiceModel.getOutputDB().equals(Const.SUCCESS)) {
            return null;
        }

        //New Workbook
        Workbook wb = new HSSFWorkbook();

        Cell cell = null;

        //Cell style for header row
        CellStyle cs = wb.createCellStyle();
        cs.setFillForegroundColor(HSSFColor.LIME.index);
        cs.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        //New Sheet
        Sheet sheet1 = null;
        Row row;
        sheet1 = wb.createSheet("Invoices");

        // Generate column headings
        row = sheet1.createRow(0);

        createHeaderRowColumn(sheet1, cell, row, cs, 100, 0, "S.No.");
        createHeaderRowColumn(sheet1, cell, row, cs, 140, 1, "InvoiceId");
        createHeaderRowColumn(sheet1, cell, row, cs, 200, 2, "UserId");
        createHeaderRowColumn(sheet1, cell, row, cs, 140, 3, "ProfileId");
        createHeaderRowColumn(sheet1, cell, row, cs, 250, 4, "CompanyId");
        createHeaderRowColumn(sheet1, cell, row, cs, 250, 5, "InvoiceNumber");
        createHeaderRowColumn(sheet1, cell, row, cs, 250, 6, "InvoiceDate");
        createHeaderRowColumn(sheet1, cell, row, cs, 130, 7, "CustomerId");
        createHeaderRowColumn(sheet1, cell, row, cs, 250, 8, "CustomerName");
        createHeaderRowColumn(sheet1, cell, row, cs, 240, 9, "FreightAmount");
        createHeaderRowColumn(sheet1, cell, row, cs, 240, 10, "TranspoartMode");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 11, "TranspoartModeId");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 12, "VehicleNumber");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 13, "DateOfSupply");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 14, "PlaceofSupply");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 15, "PlaceofSupplyId");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 16, "ShipName");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 17, "ShipGSTIN");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 18, "ShipAddress");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 19, "TotalAmount");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 20, "FreightTaxRate");

        Row multiRow;
        ArrayList<InvoiceModel> invoiceList = invoiceModel.getList();
        for (int rowPos = 1; rowPos <= invoiceList.size(); rowPos++) {
            int listPos = rowPos - 1;
            // Generate column headings
            multiRow = sheet1.createRow(rowPos);
            setRowEntry(cell, multiRow, 0, String.valueOf(rowPos));
            setRowEntry(cell, multiRow, 1,  String.valueOf(rowPos));
            setRowEntry(cell, multiRow, 2, invoiceList.get(listPos).getInvoiceUserId());
            setRowEntry(cell, multiRow, 3, invoiceList.get(listPos).getInvoiceProfileId());
            setRowEntry(cell, multiRow, 4, invoiceList.get(listPos).getInvoiceCompanyId());
            setRowEntry(cell, multiRow, 5, invoiceList.get(listPos).getInvoiceNumber());
            setRowEntry(cell, multiRow, 6, invoiceList.get(listPos).getInvoiceDate());
            setRowEntry(cell, multiRow, 7, invoiceList.get(listPos).getCustomerId());
            setRowEntry(cell, multiRow, 8, invoiceList.get(listPos).getCustomerName());
            setRowEntry(cell, multiRow, 9, invoiceList.get(listPos).getFreightAmount());
            setRowEntry(cell, multiRow, 10, invoiceList.get(listPos).getTranspoartMode());
            setRowEntry(cell, multiRow, 11, invoiceList.get(listPos).getTranspoartModeId());
            setRowEntry(cell, multiRow, 12, invoiceList.get(listPos).getVehicleNumber());
            setRowEntry(cell, multiRow, 13, invoiceList.get(listPos).getDateOfSupply());
            setRowEntry(cell, multiRow, 14, invoiceList.get(listPos).getPlaceofSupply());
            setRowEntry(cell, multiRow, 15, invoiceList.get(listPos).getPlaceofSupplyId());
            setRowEntry(cell, multiRow, 16, invoiceList.get(listPos).getShipName());
            setRowEntry(cell, multiRow, 17, invoiceList.get(listPos).getShipGSTIN());
            setRowEntry(cell, multiRow, 18, invoiceList.get(listPos).getShipAddress());
            setRowEntry(cell, multiRow, 19, invoiceList.get(listPos).getTotalAmount());
            setRowEntry(cell, multiRow, 20, invoiceList.get(listPos).getFreightTaxRate());
        }

        //New Sheet
        sheet1 = wb.createSheet("Products");

        // Generate column headings
        row = sheet1.createRow(0);


        createHeaderRowColumn(sheet1, cell, row, cs, 100, 0, "S.No.");
        createHeaderRowColumn(sheet1, cell, row, cs, 140, 1, "id");
        createHeaderRowColumn(sheet1, cell, row, cs, 200, 2, "UserId");
        createHeaderRowColumn(sheet1, cell, row, cs, 140, 3, "ProfileId");
        createHeaderRowColumn(sheet1, cell, row, cs, 140, 4, "CompanyId");
        createHeaderRowColumn(sheet1, cell, row, cs, 250, 5, "InvoiceNumber");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 6, "Quantity");
        createHeaderRowColumn(sheet1, cell, row, cs, 140, 7, "ProductId");
        createHeaderRowColumn(sheet1, cell, row, cs, 500, 8, "ProductName");
        createHeaderRowColumn(sheet1, cell, row, cs, 140, 9, "CodeHSN");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 10, "Barcode");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 11, "CostPrice");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 12, "SalePrice");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 13, "TaxRate");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 14, "FromDate");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 15, "ToDate");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 16, "CGSTTax");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 17, "CGSTVal");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 18, "SGSTTax");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 19, "SGSTVal");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 20, "IGSTTax");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 21, "IGSTVal");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 22, "TotalTaxAmt");

        invoiceList = invoiceModel.getList();
        int sheetRowPos = 0;
        for (InvoiceModel model : invoiceList) {
            ArrayList<ProductModel> prodList = model.getProductList();
            for (int listPos = 0; listPos < prodList.size(); listPos++) {
                sheetRowPos++;
                // Generate column headings
                multiRow = sheet1.createRow(sheetRowPos);
                setRowEntry(cell, multiRow, 0, String.valueOf(sheetRowPos));
                setRowEntry(cell, multiRow, 1, prodList.get(listPos).getPurchasesProdId());
                setRowEntry(cell, multiRow, 2, prodList.get(listPos).getProductUserId());
                setRowEntry(cell, multiRow, 3, prodList.get(listPos).getProductProfileId());
                setRowEntry(cell, multiRow, 4, prodList.get(listPos).getProductCompanyId());
                setRowEntry(cell, multiRow, 5, prodList.get(listPos).getInvNumber());
                setRowEntry(cell, multiRow, 6, prodList.get(listPos).getQuantity());
                setRowEntry(cell, multiRow, 7, prodList.get(listPos).getProductId());
                setRowEntry(cell, multiRow, 8, prodList.get(listPos).getProductName());
                setRowEntry(cell, multiRow, 9, prodList.get(listPos).getCodeHSN());
                setRowEntry(cell, multiRow, 10, prodList.get(listPos).getBarcode());
                setRowEntry(cell, multiRow, 11, prodList.get(listPos).getCostPrice());
                setRowEntry(cell, multiRow, 12, prodList.get(listPos).getSalePrice());
                setRowEntry(cell, multiRow, 13, prodList.get(listPos).getTaxRate());
                setRowEntry(cell, multiRow, 14, prodList.get(listPos).getFromDate());
                setRowEntry(cell, multiRow, 15, prodList.get(listPos).getToDate());
                setRowEntry(cell, multiRow, 16, prodList.get(listPos).getCgstTax());
                setRowEntry(cell, multiRow, 17, prodList.get(listPos).getCgstVal());
                setRowEntry(cell, multiRow, 18, prodList.get(listPos).getSgstTax());
                setRowEntry(cell, multiRow, 19, prodList.get(listPos).getSgstVal());
                setRowEntry(cell, multiRow, 20, prodList.get(listPos).getIgstTax());
                setRowEntry(cell, multiRow, 21, prodList.get(listPos).getIgstVal());
                setRowEntry(cell, multiRow, 22, prodList.get(listPos).getTotalTaxAmount());
            }
        }

        File myExternalFile = new File(mContext.getExternalFilesDir(AppConfig.FOLDER_XLS), fileName);
        FileOutputStream fileOut = null;
        try {
            fileOut = new FileOutputStream(myExternalFile);
            wb.write(fileOut);
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        System.out.print("file created");

        return myExternalFile;
    }


    public File exportInvoicesInExelFormat2(DBManager dbManager) {
        String userId = AppUser.getUserId(mContext);
        String fileName = "Invoices_List_" + userId + DateTimeUtility.getTimeStamp() + ".xls";
        InvoiceModel invoiceModel = dbManager.getInvoicesList(new InvoiceModel(), AppUser.getUserId(mContext), AppUser.getProfileId(mContext));


        if (!invoiceModel.getOutputDB().equals(Const.SUCCESS)) {
            return null;
        }

        //New Workbook
        Workbook wb = new HSSFWorkbook();

        Cell cell = null;

        //Cell style for header row
        CellStyle cs = wb.createCellStyle();
        cs.setFillForegroundColor(HSSFColor.LIME.index);
        cs.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        //New Sheet
        Sheet sheet1 = null;
        Row row;
        sheet1 = wb.createSheet("Invoices");

        // Generate column headings
        row = sheet1.createRow(0);

        createHeaderRowColumn(sheet1, cell, row, cs, 100, 0, "S.No.");
        createHeaderRowColumn(sheet1, cell, row, cs, 140, 1, "InvoiceId");
        createHeaderRowColumn(sheet1, cell, row, cs, 200, 2, "UserId");
        createHeaderRowColumn(sheet1, cell, row, cs, 140, 3, "ProfileId");
        createHeaderRowColumn(sheet1, cell, row, cs, 250, 4, "CompanyId");
        createHeaderRowColumn(sheet1, cell, row, cs, 250, 5, "InvoiceNumber");
        createHeaderRowColumn(sheet1, cell, row, cs, 250, 6, "InvoiceDate");
        createHeaderRowColumn(sheet1, cell, row, cs, 130, 7, "CustomerId");
        createHeaderRowColumn(sheet1, cell, row, cs, 250, 8, "CustomerName");
        createHeaderRowColumn(sheet1, cell, row, cs, 240, 9, "FreightAmount");
        createHeaderRowColumn(sheet1, cell, row, cs, 240, 10, "TranspoartMode");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 11, "TranspoartModeId");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 12, "VehicleNumber");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 13, "DateOfSupply");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 14, "PlaceofSupply");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 15, "PlaceofSupplyId");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 16, "ShipName");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 17, "ShipGSTIN");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 18, "ShipAddress");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 19, "TotalAmount");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 20, "FreightTaxRate");

        createHeaderRowColumn(sheet1, cell, row, cs, 180, 21, "Quantity");
        createHeaderRowColumn(sheet1, cell, row, cs, 140, 22, "ProductId");
        createHeaderRowColumn(sheet1, cell, row, cs, 500, 23, "ProductName");
        createHeaderRowColumn(sheet1, cell, row, cs, 140, 24, "CodeHSN");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 25, "Barcode");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 26, "CostPrice");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 27, "SalePrice");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 28, "TaxRate");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 29, "FromDate");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 30, "ToDate");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 31, "CGSTTax");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 32, "CGSTVal");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 33, "SGSTTax");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 34, "SGSTVal");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 35, "IGSTTax");
        createHeaderRowColumn(sheet1, cell, row, cs, 180, 36, "IGSTVal");
        createHeaderRowColumn(sheet1, cell, row, cs, 200, 37, "TotalTaxAmt");

        Row multiRow;
        int sheetRowPos=0;
        ArrayList<InvoiceModel> purchaseList = invoiceModel.getList();
        for (InvoiceModel model: purchaseList) {

            ArrayList<ProductModel> prodList = model.getProductList();
            for (int listPos = 0; listPos < prodList.size(); listPos++) {
                // Generate column headings
                sheetRowPos++;
                multiRow = sheet1.createRow(sheetRowPos);
                setRowEntry(cell, multiRow, 0, String.valueOf(sheetRowPos));
                setRowEntry(cell, multiRow, 1, String.valueOf(sheetRowPos));
                setRowEntry(cell, multiRow, 2, model.getInvoiceUserId());
                setRowEntry(cell, multiRow, 3, model.getInvoiceProfileId());
                setRowEntry(cell, multiRow, 4, model.getInvoiceCompanyId());
                setRowEntry(cell, multiRow, 5, model.getInvoiceNumber());
                setRowEntry(cell, multiRow, 6, model.getInvoiceDate());
                setRowEntry(cell, multiRow, 7, model.getCustomerId());
                setRowEntry(cell, multiRow, 8, model.getCustomerName());
                setRowEntry(cell, multiRow, 9, model.getFreightAmount());
                setRowEntry(cell, multiRow, 10, model.getTranspoartMode());
                setRowEntry(cell, multiRow, 11, model.getTranspoartModeId());
                setRowEntry(cell, multiRow, 12, model.getVehicleNumber());
                setRowEntry(cell, multiRow, 13, model.getDateOfSupply());
                setRowEntry(cell, multiRow, 14, model.getPlaceofSupply());
                setRowEntry(cell, multiRow, 15, model.getPlaceofSupplyId());
                setRowEntry(cell, multiRow, 16, model.getShipName());
                setRowEntry(cell, multiRow, 17, model.getShipGSTIN());
                setRowEntry(cell, multiRow, 18, model.getShipAddress());
                setRowEntry(cell, multiRow, 19, model.getTotalAmount());
                setRowEntry(cell, multiRow, 20, model.getFreightTaxRate());

                setRowEntry(cell, multiRow, 21, prodList.get(listPos).getQuantity());
                setRowEntry(cell, multiRow, 22, prodList.get(listPos).getProductId());
                setRowEntry(cell, multiRow, 23, prodList.get(listPos).getProductName());
                setRowEntry(cell, multiRow, 24, prodList.get(listPos).getCodeHSN());
                setRowEntry(cell, multiRow, 25, prodList.get(listPos).getBarcode());
                setRowEntry(cell, multiRow, 26, prodList.get(listPos).getCostPrice());
                setRowEntry(cell, multiRow, 27, prodList.get(listPos).getSalePrice());
                setRowEntry(cell, multiRow, 28, prodList.get(listPos).getTaxRate());
                setRowEntry(cell, multiRow, 29, prodList.get(listPos).getFromDate());
                setRowEntry(cell, multiRow, 30, prodList.get(listPos).getToDate());
                setRowEntry(cell, multiRow, 31, prodList.get(listPos).getCgstTax());
                setRowEntry(cell, multiRow, 32, prodList.get(listPos).getCgstVal());
                setRowEntry(cell, multiRow, 33, prodList.get(listPos).getSgstTax());
                setRowEntry(cell, multiRow, 34, prodList.get(listPos).getSgstVal());
                setRowEntry(cell, multiRow, 35, prodList.get(listPos).getIgstTax());
                setRowEntry(cell, multiRow, 36, prodList.get(listPos).getIgstVal());
                setRowEntry(cell, multiRow, 37, prodList.get(listPos).getTotalTaxAmount());
            }
        }



        File myExternalFile = new File(mContext.getExternalFilesDir(AppConfig.FOLDER_XLS), fileName);
        FileOutputStream fileOut = null;
        try {
            fileOut = new FileOutputStream(myExternalFile);
            wb.write(fileOut);
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        System.out.print("file created");

        return myExternalFile;
    }

}
