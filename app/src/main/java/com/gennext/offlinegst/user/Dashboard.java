package com.gennext.offlinegst.user;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.gennext.offlinegst.MainActivity;
import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.PopupAlert;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.user.company.AddCompany;
import com.gennext.offlinegst.user.company.ManageProfile;
import com.gennext.offlinegst.user.customer.Customer;
import com.gennext.offlinegst.user.dashboard.SalePurchasePieChart;
import com.gennext.offlinegst.user.invoice.Invoices;
import com.gennext.offlinegst.user.product.Product;
import com.gennext.offlinegst.user.purchas.Purchases;
import com.gennext.offlinegst.user.services.purchase.ServicesPurchase;
import com.gennext.offlinegst.user.services.sale.ServicesSale;
import com.gennext.offlinegst.user.vendor.Vendor;
import com.gennext.offlinegst.user.vouchers.Vouchers;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;

/**
 * Created by Admin on 7/4/2017.
 */

public class Dashboard extends CompactFragment implements View.OnClickListener {

    private Context mContext;

    private RelativeLayout rlContainer1, rlContainer2, rlContainer3;
    private DBManager dbManager;
    private LinearLayout llMenu,llPurchase,llInvoice,llProduct,llServiceSale,llServicePurchase;

    public static Fragment newInstance() {
        Dashboard fragment = new Dashboard();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_dashboard, container, false);
        mContext = getContext();
        ((MainActivity) getActivity()).updateTitle(getString(R.string.dashboard));
        dbManager = DBManager.newIsntance(getActivity());
        screenAnalytics(getContext(), AppUser.getUserId(getContext()), getString(R.string.dashboard));
        initUi(v);
        updateUi();
        return v;
    }


    private void initUi(View v) {
        rlContainer1 = (RelativeLayout) v.findViewById(R.id.container1);
        rlContainer2 = (RelativeLayout) v.findViewById(R.id.container2);
        rlContainer3 = (RelativeLayout) v.findViewById(R.id.container3);
        llMenu = (LinearLayout) v.findViewById(R.id.ll_menu_2);
        llPurchase = (LinearLayout) v.findViewById(R.id.ll_option1);
        llInvoice = (LinearLayout) v.findViewById(R.id.ll_option2);
        LinearLayout llVouchers = (LinearLayout) v.findViewById(R.id.ll_option3);
        llProduct = (LinearLayout) v.findViewById(R.id.ll_option4);
        LinearLayout llVendors = (LinearLayout) v.findViewById(R.id.ll_option5);
        LinearLayout llCustomers = (LinearLayout) v.findViewById(R.id.ll_option6);
        LinearLayout llProfile = (LinearLayout) v.findViewById(R.id.ll_option7);
        llServiceSale = (LinearLayout) v.findViewById(R.id.ll_option8);
        llServicePurchase = (LinearLayout) v.findViewById(R.id.ll_option9);

        llPurchase.setOnClickListener(this);
        llInvoice.setOnClickListener(this);
        llVouchers.setOnClickListener(this);
        llProduct.setOnClickListener(this);
        llVendors.setOnClickListener(this);
        llCustomers.setOnClickListener(this);
        llProfile.setOnClickListener(this);
        llServiceSale.setOnClickListener(this);
        llServicePurchase.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        if(AppUser.getPackageStatus(getContext()).toLowerCase().equals(Const.EXPIRED)){
            PopupAlert.newInstance("Alert",AppUser.getPackageMessage(getContext()),PopupAlert.POPUP_DIALOG)
                    .show(getFragmentManager(),"popupAlert");
            return;
        }
        switch (v.getId()) {
            case R.id.ll_option1:
                addFragment(Purchases.newInstance(), "purchases");
                break;
            case R.id.ll_option2:
                if (AppUser.getCompanyId(mContext).equals("")) {
                    showToast(getString(R.string.app_needs_to_setup_company_detail));
                    addFragment(AddCompany.newInstance(), "addCompany");
                } else {
                    addFragment(Invoices.newInstance(), "invoices");
                }
                break;
            case R.id.ll_option3:
                addFragment(Vouchers.newInstance(), R.id.container, "vouchers");
                break;
            case R.id.ll_option4:
                addFragment(Product.newInstance(), "product");
                break;
            case R.id.ll_option5:
                addFragment(Vendor.newInstance(), "vendor");
                break;
            case R.id.ll_option6:
                addFragment(Customer.newInstance(), "customer");
                break;
            case R.id.ll_option7:
                addFragment(ManageProfile.newInstance(), R.id.container, "manageProfile");
                break;
            case R.id.ll_option8:
                addFragment(ServicesSale.newInstance(), "servicesSale");
                break;
            case R.id.ll_option9:
                addFragment(ServicesPurchase.newInstance(), "servicesPurchase");
                break;

        }
    }

    public void closeHistory() {
        getFragmentManager().popBackStack("dashboard", FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }


    public void hidePurchaseReport() {
        rlContainer2.setVisibility(View.GONE);
    }

    public void hideInvoiceReport() {
        rlContainer3.setVisibility(View.GONE);
    }

    public void updateUi() {

        String businessType = AppUser.getBusinessType(getContext());
        if(businessType.toLowerCase().equals(Const.BOTH_BUSINESS_TYPE)){
            llPurchase.setVisibility(View.VISIBLE);
            llInvoice.setVisibility(View.VISIBLE);
            llProduct.setVisibility(View.VISIBLE);
            llServiceSale.setVisibility(View.VISIBLE);
            llServicePurchase.setVisibility(View.VISIBLE);
        }else if(businessType.toLowerCase().equals(Const.SERVICE_BUSINESS_TYPE)){
            llPurchase.setVisibility(View.GONE);
            llInvoice.setVisibility(View.GONE);
            llProduct.setVisibility(View.GONE);
            llServiceSale.setVisibility(View.VISIBLE);
            llServicePurchase.setVisibility(View.VISIBLE);
        }else{
            llPurchase.setVisibility(View.VISIBLE);
            llInvoice.setVisibility(View.VISIBLE);
            llProduct.setVisibility(View.VISIBLE);
            llServiceSale.setVisibility(View.GONE);
            llServicePurchase.setVisibility(View.GONE);
        }

        boolean isCompositionScheme = AppUser.getCompositionScheme(getContext());
        if(isCompositionScheme){
            llServiceSale.setVisibility(View.GONE);
        }else{
            llServiceSale.setVisibility(View.VISIBLE);
        }

        if(AppConfig.getDashboardMenu(getContext())){
            llMenu.setVisibility(View.VISIBLE);
        }else{
            llMenu.setVisibility(View.GONE);
        }

        rlContainer1.setVisibility(View.VISIBLE);
        replaceFragmentWithoutBackstack(SalePurchasePieChart.newInstance(Dashboard.this), R.id.container1, "salePurchasePieChart");

//        replaceFragmentWithoutBackstack(InvoicesReport.newInstance(Dashboard.this), R.id.container3, "invoicesReport");
//        rlContainer3.setVisibility(View.VISIBLE);
//
//        replaceFragmentWithoutBackstack(PurchaseReport.newInstance(Dashboard.this), R.id.container2, "purchaseReport");
//        rlContainer2.setVisibility(View.VISIBLE);

    }


    public void setProfileNameImage(String name, String image) {

    }

}
