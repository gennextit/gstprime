package com.gennext.offlinegst.user.reports;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.model.user.PaymentModel;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.Utility;

/**
 * Created by Admin on 7/4/2017.
 */

public class ReportDetail extends CompactFragment {

    public static final int BUYER = 1,SELLER=2;
    private Animation animSlideDown, animSlideUp;
    private LinearLayout llslotBody;
    private FragmentManager manager;
    private LinearLayout llWhitespace;
    private TextView tvName,tvInvoiceNo,tvPaymentMode,tvPayableAmt,tvPaidAmt,tvOpeningBal,tvBalanceAmt,tvBalanceAmtTag;
    private PaymentModel itemDetail;
    private int type;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        manager = null;
    }

    public static Fragment newInstance(PaymentModel itemDetail, int type) {
        ReportDetail fragment = new ReportDetail();
        fragment.itemDetail=itemDetail;
        fragment.type=type;
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_buyer_detail, container, false);
        screenAnalytics(getContext(), AppUser.getUserId(getContext()), "OverpaidReceipt");
        manager = getFragmentManager();
        initUi(v);
        updateUi();
        return v;
    }

    private void updateUi() {
        Context context = getContext();
        if(itemDetail!=null){
            if(type==BUYER){
                tvName.setText(itemDetail.getVendorName());
            }else{
                tvName.setText(itemDetail.getCustomerName());
            }
            tvInvoiceNo.setText(itemDetail.getInvoiceNo());
            if(itemDetail.getPaymentMode().equalsIgnoreCase("credit")){
                tvPaymentMode.setText("Unpaid");
                tvPaymentMode.setTextColor(ContextCompat.getColor(context,R.color.statusCancelled));
            }else{
                float balAmt = Utility.parseFloat(itemDetail.getBalanceAmount());
                if(balAmt<=0){
                    tvPaymentMode.setText("Paid");
                    tvPaymentMode.setTextColor(ContextCompat.getColor(context,R.color.statusInprogress));
                }else{
                    tvPaymentMode.setText("Partially");
                    tvPaymentMode.setTextColor(ContextCompat.getColor(context,R.color.statusComplete));
                }
            }
            tvPaymentMode.setText("By "+itemDetail.getPaymentMode());
            tvPayableAmt.setText(itemDetail.getPayableAmount());
            if(itemDetail.getOpeningBalance().contains("-")){
                tvOpeningBal.setText(itemDetail.getOpeningBalance().replace("-","")+" (adv)");
            }else{
                tvOpeningBal.setText(itemDetail.getOpeningBalance()+" (borrow)");
            }
            tvPaidAmt.setText(itemDetail.getPaidAmount());
            if(itemDetail.getBalanceAmount().contains("-")){
                tvBalanceAmt.setText(itemDetail.getBalanceAmount().replace("-",""));
                tvBalanceAmtTag.setText("Advance Amt.");
            }else {
                tvBalanceAmt.setText(itemDetail.getBalanceAmount());
                tvBalanceAmtTag.setText("Borrow Amt.");
            }

            showFlipAnimation();
        }else{
            showToast("No Record found");
            if (manager != null) {
                manager.popBackStack();
            }
        }
    }

    private void initUi(View v) {
        TextView tvChartTitle = (TextView) v.findViewById(R.id.tv_popup_1);
        if(type==BUYER){
            tvChartTitle.setText("Vendor Detail");
        }else{
            tvChartTitle.setText("Customer Detail");
        }
        Button btnLedger = (Button) v.findViewById(R.id.btn_report_ledger);
        tvName = (TextView) v.findViewById(R.id.tv_popup_2);
        tvInvoiceNo = (TextView) v.findViewById(R.id.tv_popup_3);
        tvPaymentMode = (TextView) v.findViewById(R.id.tv_popup_4);
        tvPayableAmt = (TextView) v.findViewById(R.id.tv_popup_5);
        tvPaidAmt = (TextView) v.findViewById(R.id.tv_popup_6);
        tvOpeningBal = (TextView) v.findViewById(R.id.tv_popup_10);
        tvBalanceAmt = (TextView) v.findViewById(R.id.tv_popup_7);
        tvBalanceAmtTag = (TextView) v.findViewById(R.id.tv_popup_8);

        llslotBody = (LinearLayout) v.findViewById(R.id.ll_body);
        Button btnOK = (Button) v.findViewById(R.id.btn_popup);
        llWhitespace = (LinearLayout) v.findViewById(R.id.ll_whitespace);
        llWhitespace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissFlipAnimation();
            }
        });

        btnLedger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToastInCenter("This option available in next release of app version.");
            }
        });
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissFlipAnimation();
            }
        });
        setAnimation();

    }

    private void showFlipAnimation() {
        llslotBody.setVisibility(View.VISIBLE);
        llslotBody.startAnimation(animSlideDown);
    }
    private void dismissFlipAnimation() {
        animSlideUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                llWhitespace.setVisibility(View.INVISIBLE);
                if (manager != null) {
                    manager.popBackStack();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        llslotBody.startAnimation(animSlideUp);
    }


    private void setAnimation() {
        animSlideDown = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_down);
        animSlideUp = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_up);

    }


}


