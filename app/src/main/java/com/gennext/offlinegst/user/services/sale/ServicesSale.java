package com.gennext.offlinegst.user.services.sale;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.offlinegst.MainActivity;
import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.DatePickerDialog;
import com.gennext.offlinegst.global.PopupAlert;
import com.gennext.offlinegst.global.PopupDialog;
import com.gennext.offlinegst.model.PDFServiceMaker;
import com.gennext.offlinegst.model.Sort;
import com.gennext.offlinegst.model.user.CompanyModel;
import com.gennext.offlinegst.model.user.InvoiceModel;
import com.gennext.offlinegst.model.user.ServicesAdapter;
import com.gennext.offlinegst.panel.PrintTypeSelector;
import com.gennext.offlinegst.search.FilterBarView;
import com.gennext.offlinegst.search.SearchBar;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.user.invoice.Invoices;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.ApiCallError;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.RequestBuilder;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.itextpdf.text.DocumentException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Admin on 7/4/2017.
 */

public class ServicesSale extends CompactFragment implements ApiCallError.ErrorListener ,PopupDialog.DialogListener, PrintTypeSelector.SelectListener, SearchBar.Search{
    public static final String STOCK_SALE = "stockSale";
    private RecyclerView lvMain;
    private ArrayList<InvoiceModel> cList;
    private ServicesAdapter adapter;
    private DBManager dbManager;

    private AssignTask assignTask;
    private FloatingActionButton btnAdd;
    private CoordinatorLayout coordinatorLayout;
    private PDFServiceMaker pdfMaker;
    private ProgressDialog progressDialog;
    private GeneratePDFTask genPdfTask;
    private TextView tvAddNote;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private int mSelectedTask;
    private int mSelectedItemPos;
    private InvoiceModel mSelectedItem;
    private String mStartDate,mEndDate;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
        if (genPdfTask != null) {
            genPdfTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
        if (genPdfTask != null) {
            genPdfTask.onDetach();
        }
    }

    public static Fragment newInstance() {
        ServicesSale fragment = new ServicesSale();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if(dbManager!=null)
            dbManager.closeDB();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_services, container, false);
        initToolBarHome(getActivity(),v,getString(R.string.sale_services),"serviceSale");
        screenAnalytics(getContext(),AppUser.getUserId(getContext()),"Services");
        pdfMaker = PDFServiceMaker.newInstance(getActivity());
        dbManager = DBManager.newIsntance(getActivity());
        initDate();
        initUi(v);
        addSearchTag(savedInstanceState);
        return v;
    }

    private void initDate() {
        //Calendar set to the current date
        Calendar calendar = Calendar.getInstance();
        int eYear = calendar.get(Calendar.YEAR);
        int eMonth = calendar.get(Calendar.MONTH);
        int eDay = calendar.get(Calendar.DAY_OF_MONTH);
        mEndDate = DatePickerDialog.getFormattedDate(eDay, eMonth, eYear);

        //rollback 90 days
        calendar.add(Calendar.DAY_OF_YEAR, -FilterBarView.MONTH_GAP);
        //now the date is 90 days back
        int sYear = calendar.get(Calendar.YEAR);
        int sMonth = calendar.get(Calendar.MONTH);
        int sDay = calendar.get(Calendar.DAY_OF_MONTH);
        mStartDate = DatePickerDialog.getFormattedDate(sDay, sMonth, sYear);

    }

    private void addSearchTag(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            Fragment fragment = SearchBar.newInstance(ServicesSale.this, mStartDate, mEndDate, "Search by name, date, invoice no.");
            AppAnimation.setSlideAnimation(fragment, Gravity.TOP);
            getFragmentManager().beginTransaction()
                    .replace(R.id.container_bar, fragment, "searchBar")
                    .commit();
        }
    }

    @Override
    public void onTextChanged(CharSequence charSequence) {
        if (adapter != null)
            adapter.getFilter().filter(charSequence.toString());
    }

    @Override
    public void onShortButtonPressed(String type) {
        sortArray(type);
    }

    private void sortArray(String type) {
        if (cList==null){
            return;
        }
        if(type.equals(Sort.Type1)) {
            Collections.sort(cList, new Comparator<InvoiceModel>() {
                @Override
                public int compare(InvoiceModel item, InvoiceModel item2) {
                    Date date = getDate(item.getInvoiceDate());
                    Date date2 = getDate(item2.getInvoiceDate());
                    return date2.compareTo(date);
                }
            });
        } else if(type.equals(Sort.Type2)) {
            Collections.sort(cList, new Comparator<InvoiceModel>() {
                @Override
                public int compare(InvoiceModel item, InvoiceModel item2) {
                    String s1 = item.getCustomerName();
                    String s2 = item2.getCustomerName();
                    return s1.compareToIgnoreCase(s2);
                }
            });
        }else if(type.equals(Sort.Type3)) {
            Collections.sort(cList, new Comparator<InvoiceModel>() {
                @Override
                public int compare(InvoiceModel item, InvoiceModel item2) {
                    String s1 = item.getInvoiceNumber();
                    String s2 = item2.getInvoiceNumber();
//                    Integer data = Integer.valueOf(32);
//                    data.compareTo()
                    return s2.compareToIgnoreCase(s1);
                }
            });
        }
        adapter.notifyDataSetChanged();

    }

    private Date getDate(String invoiceDate) {
        Date date = new Date();
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
            date = format.parse(invoiceDate);
            //calculate the difference in days here
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return date;
    }


    @Override
    public void onFilterButtonPressed(String startDate, String endDate) {
        this.mStartDate = startDate;
        this.mEndDate = endDate;

        executeTask(TASK_LOAD_LIST);
    }

    private void initUi(View v) {
        coordinatorLayout = (CoordinatorLayout) v.findViewById(R.id.coordinatorLayout);
        tvAddNote = (TextView) v.findViewById(R.id.tv_add_note);
        lvMain = (RecyclerView) v.findViewById(R.id.lv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());
        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                executeTask(TASK_LOAD_LIST);
            }
        });

        btnAdd = (FloatingActionButton) v.findViewById(R.id.fab);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFragment(ServiceManager.newInstance(ServicesSale.this,null, ServiceManager.ADD_INVOICE), "serviceManager");
            }
        });
        lvMain.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && btnAdd.getVisibility() == View.VISIBLE) {
                    btnAdd.hide();
                } else if (dy < 0 && btnAdd.getVisibility() != View.VISIBLE) {
                    btnAdd.show();
                }
            }
        });
        executeTask(TASK_LOAD_LIST);
    }


    private void executeTask(int task) {
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
        mSelectedTask=task;
        if(task==TASK_LOAD_LIST) {
            assignTask = new AssignTask(getActivity());
            assignTask.execute(AppSettings.GET_SERVICE_INVOICES);
        }else{
            assignTask = new AssignTask(getActivity());
            assignTask.execute(AppSettings.CANCEL_INVOICES);
        }
    }

    private void hideProgressBar() {
        if(mSwipeRefreshLayout!=null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        executeTask(mSelectedTask);
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }

    public void refreshList() {
        executeTask(TASK_LOAD_LIST);
    }

    public void deleteServicesSale(InvoiceModel itemServicesSale, int position) {
        mSelectedItem = itemServicesSale;
        mSelectedItemPos = position;
        PopupDialog.newInstance(getString(R.string.delete_invoice), getString(R.string.are_you_sure_to_cancel_this)+" " + itemServicesSale.getInvoiceNumber() +" " + getString(R.string.invoice_lowercase), ServicesSale.this)
                .show(getFragmentManager(), "popupDialog");
    }

    public void editServicesSale(InvoiceModel itemServicesSale) {
        addFragment(ServiceManager.newInstance(ServicesSale.this,itemServicesSale, ServiceManager.EDIT_INVOICE), "serviceManager");
    }

    public void copyServicesSale(InvoiceModel itemServicesSale) {
        addFragment(ServiceManager.newInstance(ServicesSale.this,itemServicesSale, ServiceManager.COPY_INVOICE), "serviceManager");
    }

    public void viewInvoice(final InvoiceModel inv) {
        screenAnalytics(getContext(),AppUser.getUserId(getContext()),"ViewInvoicePDF");
        generateInvoice(Const.VIEW_PDF,inv);
    }

    public void printInvoice(final InvoiceModel inv) {
        screenAnalytics(getContext(),AppUser.getUserId(getContext()),"PrintInvoicePDF");
        generateInvoice(Const.PRINT_PDF,inv);
    }


    public void shareInvoice(final InvoiceModel inv) {
        screenAnalytics(getContext(),AppUser.getUserId(getContext()),"PrintInvoicePDF");
        generateInvoice(Const.SHARE_PDF,inv);

    }

    public void sendEmail(InvoiceModel itemServicesSale) {
        generateInvoice(Const.SEND_EMAIL_PDF,itemServicesSale);
    }

    private Uri getUriFromFile(File docOutput) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            return FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", docOutput);
        } else {
            return Uri.fromFile(docOutput);
        }
    }
    @Override
    public void onOkClick(DialogFragment dialog) {
        executeTask(TASK_DELETE);
    }

    @Override
    public void onCancelClick(DialogFragment dialog) {

    }


    private class AssignTask extends AsyncTask<String, Void, InvoiceModel> {
        private Context context;

        private void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        private void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        private AssignTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected InvoiceModel doInBackground(String... urls) {
            String userId = AppUser.getUserId(context);
            String profileId = AppUser.getProfileId(context);
            InvoiceModel result = new InvoiceModel();
            if(mSelectedTask==TASK_LOAD_LIST) {
                String response = ApiCall.POST(urls[0], RequestBuilder.DefaultType(userId, profileId,"G",mStartDate,mEndDate));
                return JsonParser.getServicesSaleDetail(context,response);
            }else{
                String invNo = null;
                if( mSelectedItem!=null){
                    invNo = mSelectedItem.getInvoiceNumber();
                }
                String type="S";
                String response = ApiCall.POST(urls[0], RequestBuilder.cancelInvoice(type,userId, profileId, invNo != null ? invNo : ""));
                return JsonParser.cancelServiceInvoice(response);
            }
        }


        @Override
        protected void onPostExecute(InvoiceModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressBar();
                if (!AppConfig.isUpdateToServer(context)) {
                    if (result.getOutputDB().equals(Const.SUCCESS)) {
                        if(mSelectedTask==TASK_LOAD_LIST) {
                            cList = result.getList();
                            adapter = new ServicesAdapter(getActivity(), cList, ServicesSale.this);
                            lvMain.setAdapter(adapter);
                            tvAddNote.setVisibility(View.GONE);
                        }else{
//                            if(mSelectedItem!=null) {
//                                adapter.deleteItem(mSelectedItemPos);
//                            }
                            showSnakBar(coordinatorLayout, result.getOutputMsg());
                        }

                    } else if (result.getOutputDB().equals(Const.FAILURE)) {
                        addFragment(ServiceManager.newInstance(ServicesSale.this,null, ServiceManager.ADD_INVOICE), "serviceManager");
                    }
                } else {
                    if (result.getOutput().equals(Const.SUCCESS)) {
                        if(mSelectedTask==TASK_LOAD_LIST) {
                            cList = result.getList();
                            adapter = new ServicesAdapter(getActivity(), cList, ServicesSale.this);
                            lvMain.setAdapter(adapter);
                            tvAddNote.setVisibility(View.GONE);
                        }else{
//                            if(mSelectedItem!=null) {
//                                adapter.deleteItem(mSelectedItemPos);
//                            }
                            refreshList();
                            showSnakBar(coordinatorLayout, result.getOutputMsg());
                        }
                    } else if (result.getOutput().equals(Const.FAILURE)) {
                        showSnakBar(coordinatorLayout,result.getOutputMsg());
                    } else {
                        ApiCallError.newInstance(result.getOutput(),result.getOutputMsg(), ServicesSale.this)
                                .show(getFragmentManager(), "apiCallError");
                    }
                }
            }
        }
    }


    private void generateInvoice(final int taskType, final InvoiceModel inv) {
        PermissionListener onPermissionListener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
//                if(taskType==Const.VIEW_PDF){
                    genPdfTask = new GeneratePDFTask(getContext(),taskType , inv,"Original");
                    genPdfTask.execute();
//                }else{
//                    PrintTypeSelector.newInstance(ServicesSale.this,taskType,inv).show(getFragmentManager(),"printTypeSelector");
//                }
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(getContext(), getString(R.string.permission_denied)+"\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }
        };

        new TedPermission(getActivity())
                .setPermissionListener(onPermissionListener)
                .setDeniedMessage(getString(R.string.if_you_reject_permission_you_can_not_use_this_service_please_turn_on_permissions))
                .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE)
                .check();
    }

    @Override
    public void onPrintTypeSelected(DialogFragment dialog, int taskType, InvoiceModel serviceModel, String selectedPrintType) {
        generateInvoiceWithType(taskType,serviceModel, selectedPrintType);
    }
    private void generateInvoiceWithType(int taskType,InvoiceModel inv, String selectedPrintType) {
        genPdfTask = new GeneratePDFTask(getContext(),taskType , inv,selectedPrintType);
        genPdfTask.execute();
    }


    private class GeneratePDFTask extends AsyncTask<String, Void, File> {
        private final String selectedPrintType;
        private Context context;
        private int task;
        private InvoiceModel inv;
        private ProgressDialog pDialog;
        private String errorMessage;
        private CompanyModel companyDetail;

        private void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        private void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        private GeneratePDFTask(Context context,int task,InvoiceModel inv,String selectedPrintType) {
            this.context = context;
            this.task = task;
            this.inv = inv;
            this.selectedPrintType = selectedPrintType;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setTitle(getString(R.string.generating_invoice_pdf));
            pDialog.setMessage(getString(R.string.please_wait));
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.show();

        }

        @Override
        protected File doInBackground(String... urls) {
            if(task==Const.VIEW_PDF){
                File mOutputFile = null;
                companyDetail = JsonParser.parseCompanydetail(context,AppUser.getCompanyDetails(context),inv.getInvoiceUserId(), inv.getInvoiceCompanyId());
                try {
                    mOutputFile = pdfMaker.generateInvoicePDF(inv, companyDetail,selectedPrintType);
                    return mOutputFile;
                } catch (IOException | DocumentException e) {
                    e.printStackTrace();
                    errorMessage=e.toString();
                    return null;
                }
            }else{
                File mOutputFile = null;
                companyDetail = JsonParser.parseCompanydetail(context,AppUser.getCompanyDetails(context),inv.getInvoiceUserId(), inv.getInvoiceCompanyId());
                try {
                    mOutputFile = pdfMaker.generateInvoicePDF(inv, companyDetail,selectedPrintType);
                    return mOutputFile;
                } catch (IOException | DocumentException e) {
                    e.printStackTrace();
                    errorMessage=e.toString();
                    return null;
                }
            }
        }


        @Override
        protected void onPostExecute(File mOutputFile) {
            // TODO Auto-generated method stub
            super.onPostExecute(mOutputFile);
            if (context != null) {
                pDialog.dismiss();
                if(mOutputFile!=null){
                    if(task==Const.VIEW_PDF) {
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            Uri uri = getUriFromFile(mOutputFile);
                            intent.setDataAndType(uri, "application/pdf");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivity(intent);
                        }catch (ActivityNotFoundException e){
                            Toast.makeText(getActivity(), R.string.no_pdf_reader_found_message, Toast.LENGTH_LONG).show();
                        }
                    } else if(task==Const.SHARE_PDF) {
                        sharePDFFile(mOutputFile);
                    } else if (task == Const.PRINT_PDF) {
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                            doPrint("InvoicePdf", mOutputFile);
                        }else {
                            sharePDFFile(mOutputFile);
                        }
                    } else if(task==Const.SEND_EMAIL_PDF) {
                        String emailId="",companyName="",emailBody="";
                        if(inv!=null){
                            emailId =inv.getCustomerEmail() ;
                            if(companyDetail!=null){
                                companyName = companyDetail.getCompanyName();
                            }
                            String invEmailBody = AppConfig.getPurchaseInvoiceEmailBody(getContext(), inv.getInvoiceUserId(), inv.getInvoiceProfileId(), getString(R.string.invoice_email_body));
                            emailBody=getString(R.string.dear)+" "+inv.getCustomerName()+",\n\n" +
                                    invEmailBody+"\n\n" +
                                    getString(R.string.invoice_no)+": "+inv.getInvoiceNumber()+"\n" +
                                    getString(R.string.invoice_date)+": "+inv.getInvoiceDate()+"\n\n" +
                                    getString(R.string.sincerly)+",\n"+companyName;
                        }

                        Uri uri =getUriFromFile(mOutputFile);


                        Intent intent = new Intent(Intent.ACTION_SEND);
//                        Intent intent = FileUtils.createGetContentIntent();

                        intent.setType("pdf/*");

                        List<Intent> targets = new ArrayList<Intent>();
                        List<ResolveInfo> candidates = getActivity().getPackageManager().queryIntentActivities(intent, 0);
                        for (ResolveInfo candidate : candidates) {
                            String packageName = candidate.activityInfo.packageName;

                            if (packageName.equals("com.google.android.gm") || packageName.equals("com.yahoo.mobile.client.android.mail") || packageName.equals("com.microsoft.office.outlook")|| packageName.equals("com.google.android.apps.inbox")) {
                                Intent iWantThis = new Intent(Intent.ACTION_SEND);
                                iWantThis.setPackage(packageName);
                                iWantThis.setType("pdf/*");
                                String[] recipient = {emailId};
                                iWantThis.putExtra(Intent.EXTRA_EMAIL, recipient);
                                iWantThis.putExtra(Intent.EXTRA_SUBJECT, inv.getInvoiceNumber()+" from "+(companyName.equals("")?"UltraGST":companyName));
                                iWantThis.putExtra(Intent.EXTRA_TEXT, emailBody);
                                iWantThis.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                iWantThis.putExtra(Intent.EXTRA_STREAM, uri);
                                targets.add(iWantThis);
                            }

                        }

                        try {
                            Intent chooser = Intent.createChooser(targets.remove(0), getString(R.string.share_with_email));
                            chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, targets.toArray(new Parcelable[targets.size()]));
                            startActivityForResult(chooser, 1);
                        }catch (Exception e){
                            showSnakBarWithButton(coordinatorLayout,getString(R.string.no_app_found_error_msg));
                        }
//                        Intent intent = new Intent(Intent.ACTION_SEND);
//                        String[] recipient = {emailId};
//                        intent.putExtra(Intent.EXTRA_EMAIL, recipient);
//                        intent.putExtra(Intent.EXTRA_SUBJECT, inv.getInvoiceNumber()+" from "+(companyName.equals("")?"UltraGST":companyName));
//                        intent.putExtra(Intent.EXTRA_TEXT, emailBody);
//                        intent.setType("pdf/*");
//                        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                        intent.putExtra(Intent.EXTRA_STREAM, uri);
//                        try {
//                            startActivity(Intent.createChooser(intent, "Share With Email"));
//                        } catch (Exception e) {
//                            Toast.makeText(getActivity(), "Error: Cannot open or share created PDF report.", Toast.LENGTH_SHORT).show();
//                        }
                    }
                }else{
                    if(errorMessage!=null){
                        PopupAlert.newInstance(getString(R.string.alert), errorMessage, PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(), "popupAlert");
                    }
                }
            }
        }

        private void sharePDFFile(File mOutputFile) {
            String companyName="",invBody="";
            if(inv!=null){
                if(companyDetail!=null){
                    companyName = companyDetail.getCompanyName();
                }
                String invEmailBody = AppConfig.getPurchaseInvoiceEmailBody(getContext(), inv.getInvoiceUserId(), inv.getInvoiceProfileId(), getString(R.string.invoice_email_body));
                invBody=getString(R.string.dear)+" "+inv.getCustomerName()+",\n\n" +
                        invEmailBody+"\n\n" +
                        getString(R.string.invoice_no)+": "+inv.getInvoiceNumber()+"\n" +
                        getString(R.string.invoice_date)+": "+inv.getInvoiceDate()+"\n\n" +
                        getString(R.string.sincerly)+",\n"+companyName;
            }

            Uri uri =getUriFromFile(mOutputFile);
            Intent in = new Intent(Intent.ACTION_SEND);
            in.putExtra(Intent.EXTRA_TEXT, invBody);
            in.putExtra(Intent.EXTRA_STREAM, uri);
            in.setType("pdf/*");
            in.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            try {
                startActivity(Intent.createChooser(in, getString(R.string.share_with_fiends)));
            } catch (Exception e) {
                showSnakBarWithButton(coordinatorLayout,getString(R.string.no_application_found_to_take_action));
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void doPrint(final String fileName, final File fileToPrint) {

        PrintDocumentAdapter pda = new PrintDocumentAdapter() {

            @Override
            public void onWrite(PageRange[] pages, ParcelFileDescriptor destination, CancellationSignal cancellationSignal, WriteResultCallback callback) {
                InputStream input = null;
                OutputStream output = null;

                try {
                    input = new FileInputStream(fileToPrint);
                    output = new FileOutputStream(destination.getFileDescriptor());

                    byte[] buf = new byte[1024];
                    int bytesRead;

                    while ((bytesRead = input.read(buf)) > 0) {
                        output.write(buf, 0, bytesRead);
                    }

                    callback.onWriteFinished(new PageRange[]{PageRange.ALL_PAGES});

                } catch (Exception e) {
                    //Catch exception
                } finally {
                    try {
                        if (input != null) {
                            input.close();
                        }
                        if (output != null) {
                            output.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onLayout(PrintAttributes oldAttributes, PrintAttributes newAttributes, CancellationSignal cancellationSignal, LayoutResultCallback callback, Bundle extras) {

                if (cancellationSignal.isCanceled()) {
                    callback.onLayoutCancelled();
                    return;
                }

                PrintDocumentInfo pdi = new PrintDocumentInfo.Builder(fileName).setContentType(PrintDocumentInfo.CONTENT_TYPE_DOCUMENT).build();

                callback.onLayoutFinished(pdi, true);
            }
        };
        ((MainActivity)getActivity()).printJob(pda);
//        PrintManager printManager = (PrintManager) getActivity().getSystemService(Context.PRINT_SERVICE);
//        String jobName = getString(R.string.app_name) + " Document";
//        printManager.print(jobName, pda, null);

    }
}
