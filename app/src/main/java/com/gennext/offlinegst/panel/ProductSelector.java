package com.gennext.offlinegst.panel;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.PopupAlert;
import com.gennext.offlinegst.model.user.CustomerModel;
import com.gennext.offlinegst.model.user.CustomerSelectorAdapter;
import com.gennext.offlinegst.model.user.ProductModel;
import com.gennext.offlinegst.model.user.ProductSelectorAdapter;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.user.invoice.InvoiceManager;
import com.gennext.offlinegst.user.product.AddProduct;
import com.gennext.offlinegst.user.purchas.PurchaseManager;
import com.gennext.offlinegst.user.vouchers.AddVouchers;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.ApiCallError;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.ProgressRounded;
import com.gennext.offlinegst.util.RequestBuilder;

import java.util.ArrayList;


public class ProductSelector extends BottomSheetDialogFragment implements ApiCallError.ErrorParamListener{
    private SelectListener mListener;
    private ProductSelectorAdapter adapter;
    private PurchaseManager purchaseManager;
    private InvoiceManager invoiceManager;
    private AddVouchers addVouchers;

    private AssignTask assignTask;
    private ProgressRounded pBar;
    private ListView lvMain;
    private ArrayList<ProductModel> mProductList;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public interface SelectListener {
        void onProductSelect(DialogFragment dialog, ProductModel selectedProduct,ArrayList<ProductModel> productList);

    }


    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }


    public static ProductSelector newInstance(PurchaseManager purchaseManager, ArrayList<ProductModel> mProductList) {
        ProductSelector fragment = new ProductSelector();
        fragment.purchaseManager = purchaseManager;
        fragment.mListener = purchaseManager;
        fragment.mProductList=mProductList;
        return fragment;
    }

    public static ProductSelector newInstance(InvoiceManager invoiceManager, ArrayList<ProductModel> mProductList) {
        ProductSelector fragment = new ProductSelector();
        fragment.invoiceManager = invoiceManager;
        fragment.mProductList=mProductList;
        fragment.mListener = invoiceManager;
        return fragment;
    }

    public static ProductSelector newInstance(AddVouchers addVouchers, ArrayList<ProductModel> mProductList) {
        ProductSelector fragment = new ProductSelector();
        fragment.addVouchers = addVouchers;
        fragment.mProductList=mProductList;
        fragment.mListener = addVouchers;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_menu_product, container, false);
        final EditText etQuantity = (EditText) v.findViewById(R.id.et_product_quantity);
        ImageButton btnAction = (ImageButton) v.findViewById(R.id.btn_action);
        EditText etSearch = (EditText) v.findViewById(R.id.et_search);
        lvMain = (ListView) v.findViewById(R.id.lv_main);
        pBar = ProgressRounded.newInstance(getContext(), v);

        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (purchaseManager != null) {
                    addFragment(AddProduct.newInstance(null, AddProduct.ADD_PRODUCT_WITH_RESPONSE, purchaseManager), android.R.id.content, "addProduct");
                } else if (invoiceManager != null) {
                    addFragment(AddProduct.newInstance(null, AddProduct.ADD_PRODUCT_WITH_RESPONSE, invoiceManager), android.R.id.content, "addProduct");
                } else if (addVouchers != null) {
                    addFragment(AddProduct.newInstance(null, AddProduct.ADD_PRODUCT_WITH_RESPONSE, addVouchers), android.R.id.content, "addProduct");
                }
                dismiss();
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (adapter != null)
                    adapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                if (adapter != null) {
                    String quantity = etQuantity.getText().toString();
                    if (quantity.equals("")) {
                        quantity = "1";
                    }
                    ProductModel model = adapter.getItem(pos);
                    model.setQuantity(quantity);
                    if (mListener != null) {
                        mListener.onProductSelect(ProductSelector.this, model,mProductList);
                    }
                    dismiss();
                } else {
                    if (purchaseManager != null) {
                        addFragment(AddProduct.newInstance(null, AddProduct.ADD_PRODUCT_WITH_RESPONSE, purchaseManager), android.R.id.content, "addProduct");
                    } else if (invoiceManager != null) {
                        addFragment(AddProduct.newInstance(null, AddProduct.ADD_PRODUCT_WITH_RESPONSE, invoiceManager), android.R.id.content, "addProduct");
                    } else if (addVouchers != null) {
                        addFragment(AddProduct.newInstance(null, AddProduct.ADD_PRODUCT_WITH_RESPONSE, addVouchers), android.R.id.content, "addProduct");
                    }
                    dismiss();
                }
            }
        });
//        ProductModel cModel = dbManager.getProductList(new ProductModel(), AppUser.getUserId(getActivity()));
//        if (cModel != null && cModel.getOutputDB().equals(Const.SUCCESS)) {
//            adapter = new ProductSelectorAdapter(getActivity(), cModel.getList());
//            lvMain.setAdapter(adapter);
//        } else {
//            ArrayList<String> errorList = new ArrayList<>();
//            errorList.add(getString(R.string.product_not_found));
//            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.slot_empty_product,
//                    R.id.tv_message, errorList);
//            lvMain.setAdapter(adapter);
//        }

        executeTask();

        return v;
    }

    public void addFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }


    private void executeTask() {
        if(mProductList!=null){
            adapter = new ProductSelectorAdapter(getActivity(), mProductList);
            lvMain.setAdapter(adapter);
        }else {
            assignTask = new AssignTask(getActivity());
            assignTask.execute(AppSettings.GET_PRODUCTS);
        }
    }


    @Override
    public void onErrorRetryClick(DialogFragment dialog, String[] param) {
        executeTask();
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog, String[] param) {
        dismiss();
    }


    private class AssignTask extends AsyncTask<String, Void, ProductModel> {
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pBar.showProgressBar();
        }

        @Override
        protected ProductModel doInBackground(String... urls) {
            String userId = AppUser.getUserId(context);
            String companyId = AppUser.getCompanyId(context);
            boolean isCompositeScheme = AppUser.getCompositionScheme(getContext());
            String response = ApiCall.POST(urls[0], RequestBuilder.Default(userId,companyId));
            return JsonParser.getProductDetail(context,new ProductModel(),response,isCompositeScheme);
        }


        @Override
        protected void onPostExecute(ProductModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                pBar.hideProgressBar();
                if (result.getOutput().equals(Const.SUCCESS)) {
                    mProductList=result.getList();
                    adapter = new ProductSelectorAdapter(getActivity(), result.getList());
                    lvMain.setAdapter(adapter);
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    ArrayList<String> errorList = new ArrayList<>();
//                    errorList.add(getString(R.string.employee_not_found));
                    errorList.add(result.getOutputMsg());
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.slot_empty_product,
                            R.id.tv_message, errorList);
                    lvMain.setAdapter(adapter);
                    PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                            .show(getFragmentManager(), "popupAlert");
                } else {
                    String[] errorSoon = {};
                    ApiCallError.newInstance(result.getOutput(), result.getOutputMsg(), errorSoon, ProductSelector.this)
                            .show(getFragmentManager(), "apiCallError");
                }
            }
        }
    }
}