package com.gennext.offlinegst.panel;


import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;


public class BarcodeScanner extends CompactFragment implements ZXingScannerView.ResultHandler {
    private Barcodelistener mListener;
    private ZXingScannerView mScannerView;
    private static final String FLASH_STATE = "FLASH_STATE";
    private boolean mFlash;
    private ImageButton btnFlash;
    private MediaPlayer mp;

    public interface Barcodelistener {
        void onBarcodeDetected(String barcode);


    }

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }


    public static BarcodeScanner newInstance(Barcodelistener listener) {
        BarcodeScanner fragment = new BarcodeScanner();
        fragment.mListener = listener;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.panel_barcode_scanner, container, false);
        initToolBar(v,getString(R.string.product_scanner));
        ViewGroup contentFrame = (ViewGroup) v.findViewById(R.id.content_frame);
        btnFlash = (ImageButton) v.findViewById(R.id.btn_flash);
        mScannerView = new ZXingScannerView(getActivity());
        contentFrame.addView(mScannerView);
        mp = MediaPlayer.create(getActivity(), R.raw.camera_sound);

        btnFlash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleFlash();
            }
        });


        return v;
    }

    public void initToolBar(View v, String title) {
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_action_title);
        ImageView btnBack = (ImageView) v.findViewById(R.id.iv_action_back);
        tvTitle.setText(title);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
//        mScannerView.setAspectTolerance(0.2f);
        mScannerView.startCamera();
        mScannerView.setFlash(mFlash);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(FLASH_STATE, mFlash);
    }


    @Override
    public void handleResult(final Result rawResult) {
        try {
            if (mp.isPlaying()) {
                mp.stop();
                mp.release();
                mp = MediaPlayer.create(getContext(), R.raw.camera_sound);
            } mp.start();
        } catch(Exception e) { e.printStackTrace(); }
//        Toast.makeText(getActivity(), "Contents = " + rawResult.getText() +
//                ", Format = " + rawResult.getBarcodeFormat().toString(), Toast.LENGTH_SHORT).show();
        // Note:
        // * Wait 2 seconds to resume the preview.
        // * On older devices continuously stopping and resuming camera preview can result in freezing the app.
        // * I don't know why this is the case but I don't have the time to figure out.
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mScannerView.resumeCameraPreview(BarcodeScanner.this);
                getFragmentManager().popBackStack();
                mListener.onBarcodeDetected(rawResult.getText());
            }
        }, 2000);
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    public void toggleFlash() {
        mFlash = !mFlash;
        if (mFlash) {
            btnFlash.setImageResource(R.drawable.ic_flash_on_white_24dp);
        } else {
            btnFlash.setImageResource(R.drawable.ic_flash_off_white_24dp);
        }
        mScannerView.setFlash(mFlash);
    }
}
