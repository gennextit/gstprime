package com.gennext.offlinegst.model.user;

import java.util.ArrayList;

/**
 * Created by Admin on 7/29/2017.
 */

public class InvoiceModel {

//    private String invoiceId;
    private String invoiceUserId;
    private String invoiceProfileId;
    private String invoiceCompanyId;
    private String invoiceNumber;
    private String invoiceDate;
    private String customerId;
    private String customerName;
    private String customerState;
    private String customerStateCode;
    private String freightAmount;
    private String transpoartMode;
    private String transpoartModeId;
    private String vehicleNumber;
    private String dateOfSupply;
    private String placeofSupply;
    private String placeofSupplyId;
    private String shipName;
    private String shipGSTIN;
    private String shipAddress;
    private String totalAmount;
    private String freightTaxRate;
    private String freightTotalAmount;
    private String cgstTax;
    private String cgstVal;
    private String sgstTax;
    private String sgstVal;
    private String igstTax;
    private String igstVal;
    private String invTypeId;
    private String invTypeName;
    private String portCode;
    private String invTermId;
    private String invTermName;
    private String invOperatorGSTIN;
    private String reverseCharge;
    private String discount;
    private String invoiceTotal;
    private String cessAmount;
    private String cancelled;
    private String currency;
    //for customer
    private String customerCompany;
    private String customerAddress;
    private String customerDLNumber;
    private String customerGSTIN;
    private String customerEmail;
    private String lastDate;
    private Boolean cancellation;

    private String output;
    private String outputMsg;
    private String outputDB;
    private String outputDBMsg;
    private ArrayList<InvoiceModel> list;
    private ArrayList<ProductModel> productList;
    private ArrayList<ServiceModel> servicesList;

    public Boolean getCancellation() {
        return cancellation;
    }

    public void setCancellation(Boolean cancellation) {
        this.cancellation = cancellation;
    }

    public String getLastDate() {
        return lastDate;
    }

    public void setLastDate(String lastDate) {
        this.lastDate = lastDate;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCancelled() {
        return cancelled;
    }

    public void setCancelled(String cancelled) {
        this.cancelled = cancelled;
    }

    public String getCustomerCompany() {
        return customerCompany;
    }

    public void setCustomerCompany(String customerCompany) {
        this.customerCompany = customerCompany;
    }

    public ArrayList<ServiceModel> getServicesList() {
        return servicesList;
    }

    public void setServicesList(ArrayList<ServiceModel> servicesList) {
        this.servicesList = servicesList;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerDLNumber() {
        return customerDLNumber;
    }

    public void setCustomerDLNumber(String customerDLNumber) {
        this.customerDLNumber = customerDLNumber;
    }

    public String getCustomerGSTIN() {
        return customerGSTIN;
    }

    public void setCustomerGSTIN(String customerGSTIN) {
        this.customerGSTIN = customerGSTIN;
    }

    public String getCustomerState() {
        return customerState;
    }

    public void setCustomerState(String customerState) {
        this.customerState = customerState;
    }

    public String getCustomerStateCode() {
        return customerStateCode;
    }

    public void setCustomerStateCode(String customerStateCode) {
        this.customerStateCode = customerStateCode;
    }

    public String getCessAmount() {
        return cessAmount;
    }

    public void setCessAmount(String cessAmount) {
        this.cessAmount = cessAmount;
    }

    public String getInvoiceTotal() {
        return invoiceTotal;
    }

    public void setInvoiceTotal(String invoiceTotal) {
        this.invoiceTotal = invoiceTotal;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getInvOperatorGSTIN() {
        return invOperatorGSTIN;
    }

    public void setInvOperatorGSTIN(String invOperatorGSTIN) {
        this.invOperatorGSTIN = invOperatorGSTIN;
    }

    public String getReverseCharge() {
        return reverseCharge;
    }

    public void setReverseCharge(String reverseCharge) {
        this.reverseCharge = reverseCharge;
    }

    public String getInvTermId() {
        return invTermId;
    }

    public void setInvTermId(String invTermId) {
        this.invTermId = invTermId;
    }

    public String getInvTermName() {
        return invTermName;
    }

    public void setInvTermName(String invTermName) {
        this.invTermName = invTermName;
    }

    public String getPortCode() {
        return portCode;
    }

    public void setPortCode(String portCode) {
        this.portCode = portCode;
    }

    public String getInvTypeId() {
        return invTypeId;
    }

    public void setInvTypeId(String invTypeId) {
        this.invTypeId = invTypeId;
    }

    public String getInvTypeName() {
        return invTypeName;
    }

    public void setInvTypeName(String invTypeName) {
        this.invTypeName = invTypeName;
    }

    public String getFreightTaxRate() {
        return freightTaxRate;
    }

    public void setFreightTaxRate(String freightTaxRate) {
        this.freightTaxRate = freightTaxRate;
    }

    public String getFreightTotalAmount() {
        return freightTotalAmount;
    }

    public void setFreightTotalAmount(String freightTotalAmount) {
        this.freightTotalAmount = freightTotalAmount;
    }

    public String getCgstTax() {
        return cgstTax;
    }

    public void setCgstTax(String cgstTax) {
        this.cgstTax = cgstTax;
    }

    public String getCgstVal() {
        return cgstVal;
    }

    public void setCgstVal(String cgstVal) {
        this.cgstVal = cgstVal;
    }

    public String getSgstTax() {
        return sgstTax;
    }

    public void setSgstTax(String sgstTax) {
        this.sgstTax = sgstTax;
    }

    public String getSgstVal() {
        return sgstVal;
    }

    public void setSgstVal(String sgstVal) {
        this.sgstVal = sgstVal;
    }

    public String getIgstTax() {
        return igstTax;
    }

    public void setIgstTax(String igstTax) {
        this.igstTax = igstTax;
    }

    public String getIgstVal() {
        return igstVal;
    }

    public void setIgstVal(String igstVal) {
        this.igstVal = igstVal;
    }

    public String getPlaceofSupplyId() {
        return placeofSupplyId;
    }

    public void setPlaceofSupplyId(String placeofSupplyId) {
        this.placeofSupplyId = placeofSupplyId;
    }

    public String getTranspoartModeId() {
        return transpoartModeId;
    }

    public void setTranspoartModeId(String transpoartModeId) {
        this.transpoartModeId = transpoartModeId;
    }

    public String getInvoiceUserId() {
        return invoiceUserId;
    }

    public void setInvoiceUserId(String invoiceUserId) {
        this.invoiceUserId = invoiceUserId;
    }

    public String getInvoiceProfileId() {
        return invoiceProfileId;
    }

    public void setInvoiceProfileId(String invoiceProfileId) {
        this.invoiceProfileId = invoiceProfileId;
    }

    public String getInvoiceCompanyId() {
        return invoiceCompanyId;
    }

    public void setInvoiceCompanyId(String invoiceCompanyId) {
        this.invoiceCompanyId = invoiceCompanyId;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getFreightAmount() {
        return freightAmount;
    }

    public void setFreightAmount(String freightAmount) {
        this.freightAmount = freightAmount;
    }

    public String getTranspoartMode() {
        return transpoartMode;
    }

    public void setTranspoartMode(String transpoartMode) {
        this.transpoartMode = transpoartMode;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getDateOfSupply() {
        return dateOfSupply;
    }

    public void setDateOfSupply(String dateOfSupply) {
        this.dateOfSupply = dateOfSupply;
    }

    public String getPlaceofSupply() {
        return placeofSupply;
    }

    public void setPlaceofSupply(String placeofSupply) {
        this.placeofSupply = placeofSupply;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public String getShipGSTIN() {
        return shipGSTIN;
    }

    public void setShipGSTIN(String shipGSTIN) {
        this.shipGSTIN = shipGSTIN;
    }

    public String getShipAddress() {
        return shipAddress;
    }

    public void setShipAddress(String shipAddress) {
        this.shipAddress = shipAddress;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public String getOutputDB() {
        return outputDB;
    }

    public void setOutputDB(String outputDB) {
        this.outputDB = outputDB;
    }

    public String getOutputDBMsg() {
        return outputDBMsg;
    }

    public void setOutputDBMsg(String outputDBMsg) {
        this.outputDBMsg = outputDBMsg;
    }

    public ArrayList<InvoiceModel> getList() {
        return list;
    }

    public void setList(ArrayList<InvoiceModel> list) {
        this.list = list;
    }

    public ArrayList<ProductModel> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<ProductModel> productList) {
        this.productList = productList;
    }
}
