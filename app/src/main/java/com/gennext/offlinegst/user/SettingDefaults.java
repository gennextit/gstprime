package com.gennext.offlinegst.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gennext.offlinegst.MainActivity;
import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.model.SettingsAdapter;
import com.gennext.offlinegst.model.user.SettingModel;
import com.gennext.offlinegst.setting.BackupSetting;
import com.gennext.offlinegst.setting.DashboardSetting;
import com.gennext.offlinegst.setting.DeleteSetting;
import com.gennext.offlinegst.setting.GlobalSetting;
import com.gennext.offlinegst.setting.InvoiceSetting;
import com.gennext.offlinegst.util.AppUser;

import java.util.ArrayList;

/**
 * Created by Admin on 7/4/2017.
 */

public class SettingDefaults extends CompactFragment {
    private RecyclerView lvMain;
    private ArrayList<SettingModel> cList;
    private SettingsAdapter adapter;

    public static Fragment newInstance() {
        SettingDefaults fragment = new SettingDefaults();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.setting_defaults, container, false);
        screenAnalytics(getContext(), AppUser.getUserId(getContext()),"Settings");
        initUi(v);
        return v;
    }

    private void initUi(View v) {
        lvMain = (RecyclerView) v.findViewById(R.id.lv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());


        cList = getRepresentationsList();
        adapter = new SettingsAdapter(getActivity(), cList, SettingDefaults.this);
        lvMain.setAdapter(adapter);

    }

    // Const params are must be same
    private ArrayList<SettingModel> getRepresentationsList() {
        ArrayList<SettingModel> list = new ArrayList<>();
        list.add(setItem("Invoice print setting"));
        list.add(setItem("Dashboard setting"));
//        list.add(setItem("Backup and Restore"));
        return list;
    }

    private SettingModel setItem(String title) {
        SettingModel model = new SettingModel();
        model.setTitle(title);
        return model;
    }

    public void onItemSelect(int position) {
        switch (position) {
            case 1:
                addFragment(InvoiceSetting.newInstance(), "invoiceSetting");
                break;
            case 2:
                addFragment(DashboardSetting.newInstance(), "dashboardSetting");
                break;
            case 3:
//                addFragment(BackupSetting.newInstance(), "backupSetting");
                break;


        }
    }
}
