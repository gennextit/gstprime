package com.gennext.offlinegst.panel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.model.user.CommonModel;
import com.gennext.offlinegst.model.user.CommonSelectorAdapter;

import java.io.File;
import java.util.ArrayList;


public class FileSelector extends DialogFragment{
    private SelectListener mListener;
    private CommonSelectorAdapter adapter;
    private File folderThatContainsAllFiles;

    public void onSelectedItem(CommonModel item) {
        dismiss();
        if(mListener!=null){
            mListener.onFileSelect(FileSelector.this,item);
        }
    }

    public interface SelectListener {
        void onFileSelect(DialogFragment dialog, CommonModel fileSelected);
    }


    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }


    public static FileSelector newInstance(SelectListener listener,File folderThatContainsAllFiles) {
        FileSelector fragment = new FileSelector();
        fragment.mListener = listener;
        fragment.folderThatContainsAllFiles = folderThatContainsAllFiles;
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_menu_fileselector, container, false);
        ImageButton btnClose = (ImageButton) v.findViewById(R.id.btn_close);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        TextView etS = (TextView) v.findViewById(R.id.et_search);
        etS.setText(getString(R.string.select_file));
        RecyclerView lvMain = (RecyclerView) v.findViewById(R.id.lv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());

        if(folderThatContainsAllFiles!=null) {
            File[] fileList = folderThatContainsAllFiles.listFiles();

            ArrayList<CommonModel> list = getList(fileList);
            if (list!=null) {
                adapter = new CommonSelectorAdapter(getActivity(), list,FileSelector.this);
                lvMain.setAdapter(adapter);
            }
        }else{
            Toast.makeText(getContext(),"No file found in directory",Toast.LENGTH_LONG).show();
            dismiss();
        }


        return v;
    }

    private ArrayList<CommonModel> getList(File[] fileList) {
        ArrayList<CommonModel> temp=new ArrayList<>();
        for (File file:fileList){
            CommonModel model=new CommonModel();
            model.setId(file.getAbsolutePath());
            model.setName(file.getName());
            temp.add(model);
        }
        return temp;
    }
}
