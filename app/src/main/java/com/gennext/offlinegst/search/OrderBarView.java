package com.gennext.offlinegst.search;

/**
 * Created by Admin on 3/15/2018.
 */

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.DatePickerDialog;
import com.gennext.offlinegst.model.Sort;
import com.gennext.offlinegst.util.AppAnimation;

import java.util.Calendar;


public class OrderBarView extends CompactFragment {


    private SearchBar.SearchOrder mListener;
    private SearchBar parentRef;
    private int mType;


    public static OrderBarView newInstance(SearchBar.SearchOrder listener,SearchBar parentRef, int orderType) {
        OrderBarView fragment = new OrderBarView();
        AppAnimation.setSlideAnimation(fragment, Gravity.TOP);
        fragment.mListener = listener;
        fragment.parentRef = parentRef;
        fragment.mType = orderType;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = inflater.inflate(R.layout.search_bar_order, container, false);
        initUi(v);

        return v;
    }


    private void initUi(View v) {
        Button btnAction = (Button) v.findViewById(R.id.btn_apply);

        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeybord(getActivity());
                parentRef.updateOrderType(mType);
                mListener.onShortOrderPressed(mType);
                getFragmentManager().popBackStack();
            }
        });

        RadioGroup rgType = (RadioGroup) v.findViewById(R.id.rg_type);
        RadioButton rbAsc = (RadioButton) v.findViewById(R.id.type_1);
        RadioButton rbDsc = (RadioButton) v.findViewById(R.id.type_2);
        if(mType == Sort.Ascending){
            rbAsc.setChecked(true);
        }else {
            rbDsc.setChecked(true);
        }
        rgType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.type_1:
                        mType = Sort.Ascending;
                        break;
                    case R.id.type_2:
                        mType = Sort.Descending;
                        break;

                }
            }
        });

    }


}
