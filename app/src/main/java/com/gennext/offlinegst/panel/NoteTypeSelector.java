package com.gennext.offlinegst.panel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.model.user.CommonModel;
import com.gennext.offlinegst.model.user.CommonSelectorAdapter;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.JsonParser;

import java.util.ArrayList;


public class NoteTypeSelector extends BottomSheetDialogFragment{
    private static final String NOTE_TYPE_FILE_NAME = "global/noteType.json";
    private SelectListener mListener;
    private CommonSelectorAdapter adapter;

    public interface SelectListener {

        void onNoteTypeSelect(DialogFragment dialog, CommonModel selectedItem);
    }

    public void onSelectedItem(CommonModel item) {
        dismiss();
        if(mListener!=null){
            mListener.onNoteTypeSelect(NoteTypeSelector.this,item);
        }
    }


    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }


    public static NoteTypeSelector newInstance(SelectListener listener) {
        NoteTypeSelector fragment = new NoteTypeSelector();
        fragment.mListener = listener;
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_menu_transpoart, container, false);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_search);
        tvTitle.setText(getString(R.string.select_note_type));
        RecyclerView lvMain = (RecyclerView) v.findViewById(R.id.lv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());

        ArrayList<CommonModel> cModel = JsonParser.parseCommonModel(ApiCall.getAssets(getContext(),NOTE_TYPE_FILE_NAME));
        if (cModel!=null) {
            adapter = new CommonSelectorAdapter(getActivity(), cModel,NoteTypeSelector.this);
            lvMain.setAdapter(adapter);
        }

        return v;
    }


}
