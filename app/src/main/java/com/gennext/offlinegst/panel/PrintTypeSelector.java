package com.gennext.offlinegst.panel;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.model.user.InvoiceModel;


public class PrintTypeSelector extends DialogFragment {
    private static final String TYPE_ORIGINAL = "Original",TYPE_DUPLICATE="Duplicate",TYPE_TRIPLICATE="Triplicate";
    private Dialog dialog;
    private SelectListener mListener;
    private String mSelectPrintType;
    private int taskType;
    private InvoiceModel invoiceModel;


    public interface SelectListener {
        void onPrintTypeSelected(DialogFragment dialog, int taskType, InvoiceModel invoiceModel, String selectedPrintType);
    }

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static PrintTypeSelector newInstance(SelectListener listener, int taskType, InvoiceModel invoiceModel) {
        PrintTypeSelector fragment = new PrintTypeSelector();
        fragment.mListener = listener;
        fragment.taskType = taskType;
        fragment.invoiceModel = invoiceModel;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_print_type, null);
        dialogBuilder.setView(v);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);

        RadioGroup rgType = (RadioGroup) v.findViewById(R.id.rg_type);
        mSelectPrintType = TYPE_ORIGINAL;
        rgType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.type_1:
                        mSelectPrintType = TYPE_ORIGINAL;
                        break;
                    case R.id.type_2:
                        mSelectPrintType = TYPE_DUPLICATE;
                        break;
                    case R.id.type_3:
                        mSelectPrintType = TYPE_TRIPLICATE;
                        break;

                }
            }
        });
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                if (mListener != null) {
                    mListener.onPrintTypeSelected(PrintTypeSelector.this, taskType, invoiceModel,mSelectPrintType);
                }

            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }
}
