package com.gennext.offlinegst.user.purchas;

import android.Manifest;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.DatePickerDialog;
import com.gennext.offlinegst.global.PopupDialog;
import com.gennext.offlinegst.model.user.CommonModel;
import com.gennext.offlinegst.model.user.ProductModel;
import com.gennext.offlinegst.model.user.PurchaseProductAdapter;
import com.gennext.offlinegst.model.user.PurchaseProductViewAdapter;
import com.gennext.offlinegst.model.user.PurchasesModel;
import com.gennext.offlinegst.model.user.StateModel;
import com.gennext.offlinegst.model.user.VendorModel;
import com.gennext.offlinegst.panel.BarcodeScanner;
import com.gennext.offlinegst.panel.ProductQunatityDialog;
import com.gennext.offlinegst.panel.ProductSelector;
import com.gennext.offlinegst.panel.PurchaseCategorySelector;
import com.gennext.offlinegst.panel.PurchaseTypeSelector;
import com.gennext.offlinegst.panel.StateSelector;
import com.gennext.offlinegst.panel.VendorSelector;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.user.product.AddProduct;
import com.gennext.offlinegst.user.product.EditPurchaseProduct;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.DateTimeUtility;
import com.gennext.offlinegst.util.FieldValidation;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.ProgressButton;
import com.gennext.offlinegst.util.Utility;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Admin on 7/4/2017.
 */

public class PurchaseManager extends CompactFragment implements View.OnClickListener
        , VendorSelector.SelectListener, ProductSelector.SelectListener, PopupDialog.DialogTaskListener
        , DatePickerDialog.DateSelectFlagListener, StateSelector.SelectListener
        , BarcodeScanner.Barcodelistener, ProductQunatityDialog.QunatityListener
        , PurchaseTypeSelector.SelectListener
        , PurchaseCategorySelector.SelectListener {

    private static final int REQUEST_PRODUCT_NOT_FOUND = 1,REQUEST_SWITCH_VENDOR = 2;
    public static final int VIEW_PURCHASE = 0, EDIT_PURCHASE = 1, COPY_PURCHASE = 2, ADD_PURCHASE = 3;
    private static final String TYPE_NO = "n", TYPE_YES = "y";
    private RecyclerView lvMain;
    private EditText etInvoiceNo;
    private TextView tvDate, tvVendor, tvPlaceOfPurchase, tvPurchaseType, tvReceiveDate;
    //    private FloatingActionButton addProduct;
    private String mVendorId, mVendorName;
    private PurchaseProductAdapter adapter;
    private PurchaseProductViewAdapter adapterView;
    private ArrayList<ProductModel> cList;
    private int sDay, sMonth, sYear;
    private int eDay, eMonth, eYear;
    private TextView tvAddNote;
//    private FloatingActionsMenu multiMenu;

    public static final String BARCODE_KEY = "BARCODE";

    //    private Barcode barcodeResult;
    private String mSearchBarcode;
    private DBManager dbManager;
    private CoordinatorLayout coardLayout;
    private ProductModel mSearchBarcodeProduct;
    private TextView tvTitle, tvQuantity, tvAmount, tvTotalAmount;
    private LinearLayout llTotalAmount;
    private float mTotalQuan = 0, mTotalAmtWithoutTax = 0;
    private ProgressButton btnAction;

    private PurchasesModel itemPurchases;
    public int purchaseType;
    private ArrayList<String> deletedProduct;
    private String mSelectedPlaceId;
    private Purchases purchases;
    private ArrayList<ProductModel> mProductList;
    private String mPurchaseTypeId;
    private String mReceiveDate;
    private String mVendorStateCode;
    private TextView tvPurchaseCategory;
    private RadioButton rbTypeYes, rbTypeNo;
    private String mMentionedGST;
    private String mPurchaseCategoryId;
    private NestedScrollView nsView;
    private FloatingActionButton fabAdd;
    private boolean isIGSTApplicable;


//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setRetainInstance(true);
//        if (savedInstanceState != null) {
//            Barcode restoredBarcode = savedInstanceState.getParcelable(BARCODE_KEY);
//            if (restoredBarcode != null) {
//                barcodeResult = restoredBarcode;
//            }
//        }
//    }
//
//    @Override
//    public void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        outState.putParcelable(BARCODE_KEY, barcodeResult);
//        super.onSaveInstanceState(outState);
//    }


    public static Fragment newInstance(Purchases purchases, PurchasesModel itemPurchases, int purchaseType) {
        PurchaseManager fragment = new PurchaseManager();
        AppAnimation.setFadeAnimation(fragment);
        fragment.purchases = purchases;
        fragment.itemPurchases = itemPurchases;
        fragment.purchaseType = purchaseType;
        return fragment;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (dbManager != null)
            dbManager.closeDB();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_purchase_manager_new, container, false);
        initToolBar(getActivity(), v, getString(R.string.purchases_goods));
        screenAnalytics(getContext(), AppUser.getUserId(getContext()), "PurchasesManager");
        dbManager = DBManager.newIsntance(getActivity());
        initUi(v);
//        setBarcodeListener();
        updateUi();

        return v;
    }

    private void updateUi() {
        if (purchaseType == ADD_PURCHASE || purchaseType == COPY_PURCHASE) {
            initDate();
        } else {
            if (itemPurchases != null) {
                etInvoiceNo.setText(itemPurchases.getInvoiceNumber());
                tvDate.setText(itemPurchases.getInvoiceDate());
            }
        }
        if (itemPurchases != null) {
            mVendorId = itemPurchases.getVendorId();
            mVendorName = itemPurchases.getVendorName();
            tvVendor.setText(itemPurchases.getVendorName());
            tvPlaceOfPurchase.setText(itemPurchases.getPlaceOfPurchase());
            mSelectedPlaceId = itemPurchases.getPlaceOfPurchaseId();
            addListItemByArrayList(itemPurchases.getProductList());
            deletedProduct = new ArrayList<>();

            mReceiveDate = itemPurchases.getReceiveDate();
            tvReceiveDate.setText(itemPurchases.getReceiveDate());
            mPurchaseTypeId = itemPurchases.getPurchaseTypeId();
            tvPurchaseType.setText(itemPurchases.getPurchaseType());

            mPurchaseCategoryId = itemPurchases.getPurchaseCategoryId();
            tvPurchaseCategory.setText(itemPurchases.getPurchaseCategory());

            mPurchaseCategoryId = itemPurchases.getPurchaseCategoryId();
            tvPurchaseCategory.setText(itemPurchases.getPurchaseCategory());
            if (itemPurchases.getMentionedGST().toLowerCase().equals(TYPE_YES) || itemPurchases.getMentionedGST().toLowerCase().equals("yes")) {
                mMentionedGST = TYPE_YES;
                rbTypeYes.setChecked(true);
            } else {
                mMentionedGST = TYPE_NO;
                rbTypeNo.setChecked(true);
            }
        }
        if (purchaseType == VIEW_PURCHASE) {
            btnAction.setText("Next");
            disableEditFields();
            hideKeybord(getActivity());
        }
    }


    @Override
    public void onBarcodeDetected(String barcode) {
//        barcodeResult = barcode;
        mSearchBarcode = barcode;
        fetchFromDB(mSearchBarcode);
        if (mSearchBarcodeProduct != null) {
            AddProduct addProduct = (AddProduct) getFragmentManager().findFragmentByTag("addProduct");
            if (addProduct == null) {
                PopupDialog dialog = (PopupDialog) getFragmentManager().findFragmentByTag("popupDialog");
                if (dialog == null) {
                    PopupDialog.newInstance(getString(R.string.product_not_found), getString(R.string.barcode_new_scan_found), REQUEST_PRODUCT_NOT_FOUND, PurchaseManager.this)
                            .show(getFragmentManager(), "popupDialog");
                }
            }
        }
    }

    private void initDate() {
        final Calendar calendar = Calendar.getInstance();
        eYear = sYear = calendar.get(Calendar.YEAR);
        eMonth = sMonth = calendar.get(Calendar.MONTH);
        eDay = sDay = calendar.get(Calendar.DAY_OF_MONTH);
        tvDate.setText(DateTimeUtility.cDateDDMMMYY(sDay, sMonth, sYear));

        mReceiveDate = DateTimeUtility.cDateDDMMMYY(sDay, sMonth, sYear);
        tvReceiveDate.setText(mReceiveDate);
    }

    private void initUi(View v) {
        coardLayout = (CoordinatorLayout) v.findViewById(R.id.coordinatorLayout);
        btnAction = ProgressButton.newInstance(getContext(), v);
        lvMain = (RecyclerView) v.findViewById(R.id.lv_main);

        etInvoiceNo = (EditText) v.findViewById(R.id.et_purchases_inv_no);
        LinearLayout llDate = (LinearLayout) v.findViewById(R.id.ll_purchases_inv_date);
        LinearLayout llVendor = (LinearLayout) v.findViewById(R.id.ll_purchases_inv_vendor);
        LinearLayout llPurchasePlace = (LinearLayout) v.findViewById(R.id.ll_purchases_place);
        LinearLayout llPurchaseType = (LinearLayout) v.findViewById(R.id.ll_purchases_type);
        LinearLayout llReceiveDate = (LinearLayout) v.findViewById(R.id.ll_purchases_receive_date);
        LinearLayout llPurchaseCategory = (LinearLayout) v.findViewById(R.id.ll_purchases_category);
        tvDate = (TextView) v.findViewById(R.id.tv_purchases_inv_date);
        tvAddNote = (TextView) v.findViewById(R.id.tv_add_note);
        tvVendor = (TextView) v.findViewById(R.id.tv_purchases_inv_vendor);
        tvPlaceOfPurchase = (TextView) v.findViewById(R.id.tv_purchases_place);
        tvPurchaseType = (TextView) v.findViewById(R.id.tv_purchases_type);
        tvReceiveDate = (TextView) v.findViewById(R.id.tv_purchases_receive_date);
        tvPurchaseCategory = (TextView) v.findViewById(R.id.tv_purchases_category);
        fabAdd = (FloatingActionButton) v.findViewById(R.id.fab);

        llTotalAmount = (LinearLayout) v.findViewById(R.id.layoutSlot);
        tvTitle = (TextView) v.findViewById(R.id.tv_slot_1);
        tvQuantity = (TextView) v.findViewById(R.id.tv_slot_2);
        tvAmount = (TextView) v.findViewById(R.id.tv_slot_3);
        tvTotalAmount = (TextView) v.findViewById(R.id.tv_slot_4);
        nsView = (NestedScrollView) v.findViewById(R.id.nested_scroll);

//        multiMenu = (FloatingActionsMenu) v.findViewById(R.id.multiple_actions);
//        final FloatingActionButton btnBarcode = (FloatingActionButton) v.findViewById(R.id.fab_barcode);
//        final FloatingActionButton btnDirectory = (FloatingActionButton) v.findViewById(R.id.fab_directory);

        llDate.setOnClickListener(this);
        llVendor.setOnClickListener(this);
        llPurchasePlace.setOnClickListener(this);
        llPurchaseType.setOnClickListener(this);
        llReceiveDate.setOnClickListener(this);
//        btnBarcode.setOnClickListener(this);
//        btnDirectory.setOnClickListener(this);
        llPurchaseCategory.setOnClickListener(this);
        btnAction.setOnClickListener(this);
        fabAdd.setOnClickListener(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(linearLayoutManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());

        cList = new ArrayList<>();
        if (!isPurchaseView()) {
            adapter = new PurchaseProductAdapter(getActivity(), cList, PurchaseManager.this);
            lvMain.setAdapter(adapter);
        } else {
            if (itemPurchases != null) {
                mSelectedPlaceId = itemPurchases.getPlaceOfPurchaseId();
            } else {
                mSelectedPlaceId = "";
            }
            Context context = getActivity();
            String companyId = AppUser.getCompanyId(context);
            String companyStateCode = JsonParser.getSelectedCompanyStateCode(AppUser.getCompanyDetails(context), companyId);
            if (companyStateCode.equalsIgnoreCase(mSelectedPlaceId)) {
                isIGSTApplicable = false;
            } else {
                isIGSTApplicable = true;
            }
            adapterView = new PurchaseProductViewAdapter(getActivity(), cList, PurchaseManager.this, isIGSTApplicable);
            lvMain.setAdapter(adapterView);
        }

        RadioGroup rgPaymentType = (RadioGroup) v.findViewById(R.id.rg_sale);
        rbTypeYes = (RadioButton) v.findViewById(R.id.rb_yes);
        rbTypeNo = (RadioButton) v.findViewById(R.id.rb_no);
        rgPaymentType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.rb_no:
                        mMentionedGST = TYPE_NO;
                        break;
                    case R.id.rb_yes:
                        mMentionedGST = TYPE_YES;
                        break;
                }
            }
        });

    }

    private void executeTask(String invoiceNumber, String invoiceDate, String vendorId, String vendorName, String placeOfPurchase, String totalPrice, String selectedPlaceId, String receiveDate, String invoicePurchaseTypeId, String purchaseCategoryId, String mentionedGST, ArrayList<ProductModel> productList) {
        hideKeybord(getActivity());
        String[] purchaseDetail = {invoiceNumber, invoiceDate, vendorId, vendorName, placeOfPurchase, totalPrice, selectedPlaceId, receiveDate, invoicePurchaseTypeId, purchaseCategoryId, mentionedGST};

        addFragment(PaymentCreditors.newInstance(purchases, purchaseType, itemPurchases, deletedProduct, purchaseDetail, mVendorStateCode, productList), "paymentCreditors");

    }


    @Override
    public void onClick(View v) {
        if (v.getId() != R.id.btn_action && purchaseType == VIEW_PURCHASE) {
            showSnakBar(coardLayout, getString(R.string.could_not_edit_this_field));
            return;
        }
        switch (v.getId()) {
            case R.id.btn_action:
//                if(btnAction.getText().equals("Next")){
//                    btnAction.setText("Submit");
//                    purchaseType=EDIT_PURCHASE;
//                    enableEditFields();
//                    return;
//                }
                if (!FieldValidation.isEmpty(getContext(), etInvoiceNo)) {
                    showSnakBar(coardLayout, getString(R.string.invalid_invoice_no));
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), mVendorId, getString(R.string.please_select_vendor))) {
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), mReceiveDate, getString(R.string.please_select_receive_date))) {
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), mPurchaseTypeId, getString(R.string.please_select_purchase_type))) {
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), mPurchaseCategoryId, getString(R.string.please_select_purchase_category))) {
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), mSelectedPlaceId, getString(R.string.please_select_place_of_purchase))) {
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), mMentionedGST, getString(R.string.mentioned_gst_error))) {
                    return;
                } else if (cList == null || cList.size() == 0) {
                    showSnakBar(coardLayout, getString(R.string.please_add_product));
                    return;
                }
                hideKeybord(getActivity());
                executeTask(etInvoiceNo.getText().toString(), tvDate.getText().toString(), mVendorId, mVendorName
                        , tvPlaceOfPurchase.getText().toString(), String.valueOf(mTotalAmtWithoutTax), mSelectedPlaceId, mReceiveDate, mPurchaseTypeId, mPurchaseCategoryId, mMentionedGST, cList);
                break;
//            case R.id.fab_barcode:
//                startScan();
//                multiMenu.collapse();
//                break;
//            case R.id.fab_directory:
//                ProductSelector.newInstance(PurchaseManager.this,mProductList)
//                        .show(getFragmentManager(), "productSelector");
//                multiMenu.collapse();
//                break;
            case R.id.fab:
                if (!FieldValidation.isEmpty(getContext(), mVendorId, getString(R.string.please_select_vendor))) {
                    return;
                }
                ProductSelector.newInstance(PurchaseManager.this, mProductList)
                        .show(getFragmentManager(), "productSelector");
                break;
            case R.id.ll_purchases_inv_date:
                DatePickerDialog.newInstance(PurchaseManager.this, false, true, 0, DatePickerDialog.START_DATE, sDay, sMonth, sYear)
                        .show(getFragmentManager(), "datePickerDialog");
                break;
            case R.id.ll_purchases_inv_vendor:
                if(tvVendor.getText().toString().equalsIgnoreCase(Const.CASH_PURCHASE)&&cList!=null&&cList.size()>0){
                    PopupDialog.newInstance("Alset!","If you switch from Cash Purchase to another vendor, all products will be clear from list below.",REQUEST_SWITCH_VENDOR,PurchaseManager.this)
                            .show(getFragmentManager(),"popupDialog");
                }else {
                    VendorSelector.newInstance(PurchaseManager.this)
                            .show(getFragmentManager(), "vendorSelector");
                }
                break;
            case R.id.ll_purchases_place:
                StateSelector.newInstance(PurchaseManager.this).show(getFragmentManager(), "stateSelector");
                break;
            case R.id.ll_purchases_type:
                PurchaseTypeSelector.newInstance(PurchaseManager.this).show(getFragmentManager(), "purchaseTypeSelector");
                break;
            case R.id.ll_purchases_receive_date:
                DatePickerDialog.newInstance(PurchaseManager.this, false, DatePickerDialog.END_DATE, eDay, eMonth, eYear)
                        .show(getFragmentManager(), "datePickerDialog");
                break;
            case R.id.ll_purchases_category:
                PurchaseCategorySelector.newInstance(PurchaseManager.this).show(getFragmentManager(), "purchaseCategorySelector");
                break;

        }
    }

    private void enableEditFields() {
        fabAdd.show();
        enableEditText(etInvoiceNo);
    }

    private void disableEditFields() {
        fabAdd.hide();
        disableEditText(etInvoiceNo);
        disableRadioButton(rbTypeYes);
        disableRadioButton(rbTypeNo);
    }

    public void startScan() {
        /**
         * Build a new MaterialBarcodeScanner
         */
        PermissionListener onPermissionListener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                addFragment(BarcodeScanner.newInstance(PurchaseManager.this), "barcodeScanner");
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(getContext(), getString(R.string.permission_denied) + "\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }
        };
        new TedPermission(getActivity())
                .setPermissionListener(onPermissionListener)
                .setDeniedMessage(getString(R.string.if_you_reject_permission_you_can_not_use_this_service_please_turn_on_permissions))
                .setPermissions(Manifest.permission.CAMERA)
                .check();
    }

    public void fetchFromServer(String barcode) {
        mProductList=null;
        ProductSelector.newInstance(PurchaseManager.this, mProductList)
                .show(getFragmentManager(), "productSelector");
    }


    public void fetchFromDB(final String mSearchBarcode) {
        ProductModel selectedProduct = dbManager.filterProductByBarcode(AppUser.getUserId(getActivity()), mSearchBarcode);
        if(mVendorName.equalsIgnoreCase(Const.CASH_PURCHASE)) {
            selectedProduct.setCessRate("0");
            selectedProduct.setTaxRate("0");
        }
        if (selectedProduct.getOutputDB().equals(Const.SUCCESS)) {
            mSearchBarcodeProduct = null;
            tvAddNote.setVisibility(View.GONE);
            boolean isQunatityIncreased = false;
            ArrayList<ProductModel> tempList = selectedProduct.getList();
            for (ProductModel model : tempList) {
                if (model.getQuantity() == null) {
                    model.setQuantity("1");
                    model.setInitQuantity(1);
                }
                for (int i = 0; i < cList.size(); i++) {
                    if (model.getProductId().equalsIgnoreCase(cList.get(i).getProductId())) {
                        isQunatityIncreased = true;
                        ProductModel proModel = cList.get(i);
                        float res = parseFloat(proModel.getQuantity()) + parseFloat(model.getQuantity());
                        proModel.setQuantity(String.valueOf(res));
                        proModel.setInitQuantity(res);
                        cList.set(i, proModel);
                    }
                }
                if (!isQunatityIncreased) {
                    cList.add(model);
                }
            }
            NotifyDataSetChanged();

            addTotalItemOnFooter();
        } else {
            this.mSearchBarcodeProduct = new ProductModel();
            this.mSearchBarcodeProduct.setBarcode(mSearchBarcode);
        }
    }


    private void addListItemByModel(ProductModel selectedProduct) {
        if(mVendorName.equalsIgnoreCase(Const.CASH_PURCHASE)) {
            selectedProduct.setCessRate("0");
            selectedProduct.setTaxRate("0");
        }
        if (selectedProduct.getQuantity() == null) {
            selectedProduct.setQuantity("1");
            selectedProduct.setInitQuantity(1);
        }
        if (!isProductExistsInList(selectedProduct)) {
            cList.add(selectedProduct);
        }

        NotifyDataSetChanged();
        addTotalItemOnFooter();
    }

    private void updateListItemByModel(ProductModel selectedProduct) {
        if(mVendorName.equalsIgnoreCase(Const.CASH_PURCHASE)) {
            selectedProduct.setCessRate("0");
            selectedProduct.setTaxRate("0");
        }
        if (selectedProduct.getQuantity() == null) {
            selectedProduct.setQuantity("1");
            selectedProduct.setInitQuantity(1);
        }
        if (!isProductExistsInList(selectedProduct)) {
            cList.add(selectedProduct);
        }
        NotifyDataSetChanged();
        addTotalItemOnFooter();
    }

    private boolean isProductExistsInList(ProductModel selectedProduct) {
        for (ProductModel model:cList) {
            if (selectedProduct.getProductId().equalsIgnoreCase(model.getProductId())) {
                return true;
            }
        }
        return false;
    }

    private void addListItemByArrayList(ArrayList<ProductModel> prodList) {
        if (prodList != null) {
            cList.addAll(prodList);
        }
        NotifyDataSetChanged();

        addTotalItemOnFooter();
    }

    public void refreshAdapter() {
        NotifyDataSetChanged();

        addTotalItemOnFooter();
    }


    private void addTotalItemOnFooter() {
        float totalQuan = 0, totalTaxable = 0, totalAmt = 0;
        for (int i = 0; i < cList.size(); i++) {
            ProductModel item = cList.get(i);
            String costPrice = item.getCostPrice();
            if (costPrice.equals("")) {
                costPrice = item.getSalePrice();
            }
            float amt = parseFloat(costPrice);
            float price = parseFloat(costPrice);
            float qun = parseFloat(item.getQuantity());
            float dis = parseFloat(item.getDis());
            float cessRate = parseFloat(item.getCessRate());
            float taxRate = parseFloat(item.getTaxRate());
            float taxable = (price * qun) - dis;
            float totalTax = taxable + ((taxable * (taxRate + cessRate)) / 100);
            totalQuan += qun;
            totalTaxable += taxable;
            totalAmt += totalTax;
        }
        addTotalItemOnUI(totalQuan, totalTaxable, totalAmt);
    }


    private void NotifyDataSetChanged() {
        if (!isPurchaseView()) {
            adapter.notifyDataSetChanged();
            lvMain.smoothScrollToPosition(adapter.getItemCount());
        } else {
            adapterView.notifyDataSetChanged();
            lvMain.smoothScrollToPosition(adapterView.getItemCount());
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onVendorSelect(DialogFragment dialog, VendorModel selectedVendor) {
        mVendorId = selectedVendor.getVendorId();
        mVendorName = selectedVendor.getPersonName();
        tvVendor.setText(selectedVendor.getPersonName());

        mVendorStateCode = selectedVendor.getSelectedStateCode();
        mSelectedPlaceId = selectedVendor.getSelectedStateCode();
        tvPlaceOfPurchase.setText(selectedVendor.getState());
    }


    @Override
    public void onProductSelect(DialogFragment dialog, ProductModel selectedProduct, ArrayList<ProductModel> productList) {
        mSearchBarcodeProduct = null;
        mProductList = productList;
        tvAddNote.setVisibility(View.GONE);
        addListItemByModel(selectedProduct);
    }

    private void addTotalItemOnUI(float quantity, float totalTaxable, float totalAmt) {
        llTotalAmount.setVisibility(View.VISIBLE);
        tvTitle.setText(getString(R.string.total));
        mTotalAmtWithoutTax = totalTaxable;
        mTotalQuan = quantity;
        tvQuantity.setText(String.valueOf(quantity));
        tvAmount.setText(Utility.decimalFormat(totalAmt, "#.##"));
        tvTotalAmount.setText(Utility.decimalFormat(totalAmt, "#.##"));
        nsView.fullScroll(View.FOCUS_DOWN);
    }

    private void removeTotalItemFromUI() {
        llTotalAmount.setVisibility(View.GONE);
        tvTitle.setText(getString(R.string.total));
        tvQuantity.setText("0");
        tvAmount.setText("0");
    }

    public void editProduct(ProductModel itemProduct, int position) {
        addFragment(EditPurchaseProduct.newInstance(itemProduct, PurchaseManager.this, mVendorName), "editPurchaseProduct");
    }

    public void onProductEdited(ProductModel itemProduct) {
        updateListItemByModel(itemProduct);
    }

    public void deleteProduct(ProductModel itemProduct, int position) {
        if (deletedProduct != null) {
            deletedProduct.add(itemProduct.getProductId());
        }
        if (cList.size() == 0) {
            tvAddNote.setVisibility(View.VISIBLE);
            removeTotalItemFromUI();
            return;
        }
        addTotalItemOnFooter();
    }


    public void quantityProduct(ProductModel itemProduct, int position) {
        ProductQunatityDialog.newInstance(PurchaseManager.this, itemProduct, position).show(getFragmentManager(), "productQunatityDialog");
    }

    @Override
    public void onQunatityChange(DialogFragment dialog, ProductModel itemProduct, int position) {
        updateListItemByModel(itemProduct);
    }


    @Override
    public void onDialogOkClick(DialogFragment dialog, int task) {
        switch (task) {
            case REQUEST_PRODUCT_NOT_FOUND:
                addFragment(AddProduct.newInstance(mSearchBarcodeProduct, AddProduct.ADD_PRODUCT_WITH_RESPONSE, PurchaseManager.this), "addProduct");
                break;
            case REQUEST_SWITCH_VENDOR:
                mProductList = null;
                cList.clear();
                refreshAdapter();
                VendorSelector.newInstance(PurchaseManager.this)
                        .show(getFragmentManager(), "vendorSelector");
                break;
        }
    }

    @Override
    public void onDialogCancelClick(DialogFragment dialog, int task) {
        mSearchBarcodeProduct = null;
    }

    public void fetchVendorFromDB(String personName, String companyName) {
        VendorModel vendorModel = dbManager.getVendorIdByVendorAndCompanyName(AppUser.getUserId(getActivity()), personName, companyName);
        if (vendorModel.getOutputDB().equals(Const.SUCCESS)) {
            mVendorId = vendorModel.getVendorId();
            mVendorName = vendorModel.getPersonName();
            tvVendor.setText(vendorModel.getPersonName());
        } else {
            showSnakBar(coardLayout, vendorModel.getOutputDBMsg());
        }
    }


    @Override
    public void onStateSelect(DialogFragment dialog, StateModel selectedState) {
        mSelectedPlaceId = selectedState.getStateCode();
        tvPlaceOfPurchase.setText(selectedState.getStateName());
    }

    @Override
    public void onStartDateSelected(DialogFragment dialog, int day, int month, int year, String ddMMMyy) {
        sDay = day;
        sMonth = month;
        sYear = year;
        tvDate.setText(ddMMMyy);
    }

    @Override
    public void onEndDateSelected(DialogFragment dialog, int day, int month, int year, String ddMMMyy) {
        eDay = day;
        eMonth = month;
        eYear = year;
        mReceiveDate = ddMMMyy;
        tvReceiveDate.setText(ddMMMyy);
    }

    @Override
    public void onPurchaseTypeSelect(DialogFragment dialog, CommonModel selectedItem) {
        mPurchaseTypeId = selectedItem.getName();
        tvPurchaseType.setText(selectedItem.getName());
    }

    @Override
    public void onPurchaseCategorySelect(DialogFragment dialog, CommonModel selectedItem) {
        mPurchaseCategoryId = selectedItem.getId();
        tvPurchaseCategory.setText(selectedItem.getName());
    }

    public boolean isPurchaseView() {
        return purchaseType == VIEW_PURCHASE;
    }



//    private class AssignTask extends AsyncTask<String, Void, Model> {
//        private final String invoiceNumber, invoiceDate, vendorId, vendorName, amount, placeOfPurchase, totalPrice, selectedPlaceId;
//        private final ArrayList<ProductModel> productList;
//        private Context context;
//
//        public AssignTask(Context context, String invoiceNumber, String invoiceDate, String vendorId, String vendorName
//                , String amount, String placeOfPurchase, String totalPrice, String selectedPlaceId, ArrayList<ProductModel> productList) {
//            this.invoiceNumber = invoiceNumber;
//            this.invoiceDate = invoiceDate;
//            this.vendorId = vendorId;
//            this.vendorName = vendorName;
//            this.amount = amount;
//            this.placeOfPurchase = placeOfPurchase;
//            this.totalPrice = totalPrice;
//            this.productList = productList;
//            this.selectedPlaceId = selectedPlaceId;
//            this.context = context;
//        }
//
//        public void onAttach(Context context) {
//            // TODO Auto-generated method stub
//            this.context = context;
//        }
//
//        public void onDetach() {
//            // TODO Auto-generated method stub
//            this.context = null;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            btnAction.startAnimation();
//        }
//
//        @Override
//        protected Model doInBackground(String... urls) {
//            Model result = new Model();
//            String userId = AppUser.getUserId(context);
//            String profileId = AppUser.getProfileId(context);
//            if (AppConfig.isUpdateToServer(context)) {
//                String productJson = "";
//                String response = ApiCall.POST(urls[0], RequestBuilder.addPurchases(userId, profileId, invoiceNumber, invoiceDate, vendorId, vendorName, amount, placeOfPurchase, totalPrice, selectedPlaceId, productJson));
//                result = JsonParser.defaultStaticParser(response);
//            }
//            switch (purchaseType) {
//                case ADD_PURCHASE:
//                    return dbManager.addPurchases(result, userId, profileId, invoiceNumber, invoiceDate, vendorId, vendorName, amount, placeOfPurchase, totalPrice, selectedPlaceId, productList);
//                case EDIT_PURCHASE:
//                    return dbManager.editPurchases(result, itemPurchases.getInvoiceId(), deletedProduct, itemPurchases.getInvoiceUserId(), itemPurchases.getInvoiceProfileId(), invoiceNumber, invoiceDate, vendorId, vendorName, amount, placeOfPurchase, totalPrice, selectedPlaceId, productList);
//                case COPY_PURCHASE:
//                    return dbManager.copyPurchases(result, itemPurchases.getInvoiceId(), itemPurchases.getInvoiceUserId(), itemPurchases.getInvoiceProfileId(), invoiceNumber, invoiceDate, vendorId, vendorName, amount, placeOfPurchase, totalPrice, selectedPlaceId, productList);
//                default:
//                    return dbManager.addPurchases(result, userId, profileId, invoiceNumber, invoiceDate, vendorId, vendorName, amount, placeOfPurchase, totalPrice, selectedPlaceId, productList);
//            }
//
//        }
//
//
//        @Override
//        protected void onPostExecute(Model result) {
//            // TODO Auto-generated method stub
//            super.onPostExecute(result);
//            if (context != null) {
//                if (!AppConfig.isUpdateToServer(context)) {
//                    if (result.getOutputDB().equals(Const.SUCCESS)) {
//                        showSnakBar(coardLayout, result.getOutputDBMsg());
//                        if (purchases != null) {
//                            purchases.refreshList();
//                        }
//                        animateButtonAndRevert(btnAction, ContextCompat.getColor(getActivity(), R.color.colorPrimary),
//                                BitmapFactory.decodeResource(getResources(), R.drawable.ic_checked));
//                    } else if (result.getOutputDB().equals(Const.FAILURE)) {
//                        btnAction.revertAnimation();
//                        showSnakBar(coardLayout, result.getOutputDBMsg());
//                    }
//                } else {
//                    if (result.getOutput().equals(Const.SUCCESS)) {
//                        if (!result.getOutputDB().equals(Const.SUCCESS)) {
//                            showSnakBar(coardLayout, result.getOutputDBMsg());
//                        }
//                        showSnakBar(coardLayout, result.getOutputMsg());
//                        if (purchases != null) {
//                            purchases.refreshList();
//                        }
//                        animateButtonAndRevert(btnAction, ContextCompat.getColor(getActivity(), R.color.colorPrimary),
//                                BitmapFactory.decodeResource(getResources(), R.drawable.ic_checked));
//                    } else if (result.getOutput().equals(Const.FAILURE)) {
//                        if (result.getOutputDB().equals(Const.SUCCESS)) {
//                            showSnakBar(coardLayout, "Local " + result.getOutputDBMsg());
//                        }
//                        btnAction.revertAnimation();
//                        PopupAlert.newInstance("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
//                                .show(getFragmentManager(), "popupAlert");
//                    } else {
//                        if (result.getOutputDB().equals(Const.SUCCESS)) {
//                            showSnakBar(coardLayout, "Local " + result.getOutputDBMsg());
//                        }
//                        btnAction.revertAnimation();
//                        String[] errorSoon = {invoiceNumber, invoiceDate, vendorId, vendorName, amount, placeOfPurchase, totalPrice, selectedPlaceId};
//                        ApiCallError.newInstance(result.getOutput(),result.getOutputMsg(), errorSoon, PurchaseManager.this)
//                                .show(getFragmentManager(), "apiCallError");
//                    }
//                }
//            }
//        }
//        private void animateButtonAndRevert(final CircularProgressButton circularProgressButton, final int fillColor, final Bitmap bitmap) {
//            Handler handler = new Handler();
//
//            circularProgressButton.doneLoadingAnimation(
//                    fillColor,
//                    bitmap);
//
//            Runnable runnableRevert = new Runnable() {
//                @Override
//                public void run() {
//                    circularProgressButton.revertAnimation(new OnAnimationEndListener() {
//                        @Override
//                        public void onAnimationEnd() {
//                            if(context!=null) {
//                             getActivity().onBackPressed();
//                            }
//                        }
//                    });
//                }
//            };
//
//            circularProgressButton.startAnimation();
//            handler.postDelayed(runnableRevert, Const.BUTTON_PROGRESS_TIME);
//        }
//
//    }
}
