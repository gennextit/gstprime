package com.gennext.offlinegst.user.vendor;

/**
 * Created by Admin on 5/22/2017.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.PopupAlert;
import com.gennext.offlinegst.model.app.Model;
import com.gennext.offlinegst.model.user.CommonModel;
import com.gennext.offlinegst.model.user.StateModel;
import com.gennext.offlinegst.model.user.VendorModel;
import com.gennext.offlinegst.panel.PaymentTermsSelector;
import com.gennext.offlinegst.panel.StateSelector;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.user.purchas.CreditorsTransaction;
import com.gennext.offlinegst.user.purchas.PurchaseManager;
import com.gennext.offlinegst.user.services.purchase.ServicePurchaseManager;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.ApiCallError;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.FieldValidation;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.ProgressButton;
import com.gennext.offlinegst.util.RequestBuilder;
import com.gennext.offlinegst.util.Utility;

import java.util.Map;


public class VendorManager extends CompactFragment implements ApiCallError.ErrorParamListener
        , StateSelector.SelectListener, PaymentTermsSelector.SelectListener {

    public static final int ADD_VENDOR_WITH_RESPONSE = 0, VIEW_VENDOR = -1, ADD_VENDOR = 1, EDIT_VENDOR = 2, COPY_VENDOR = 3;
    private EditText etPersonName, etCompanyName, etEmail, etWorkPhone, etMobile, etDisplayName, etWebsite, etGSTIN, etDLNumber, etState, etCity, etCountry, etZip, etAddress, etFax, etCurrency, etPaymentTerms, etRemarks, etOpeningBalance;
    private ProgressButton btnAction;
    private DBManager dbManager;
    private AssignTask assignTask;
    private CoordinatorLayout coordinatorLayout;
    private VendorModel itemVendor;
    private int vendorType;
    private String mSelectedStateCode, mSelectedTermCode;
    private Vendor vendor;
    private PurchaseManager purchaseManager;
    private CreditorsTransaction creditorsTransaction;
    private Map<String, String> defaultStateList;
    private ServicePurchaseManager servicePurchaseManager;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }


    public static Fragment newInstance(Vendor vendor) {
        VendorManager fragment = new VendorManager();
        AppAnimation.setSlideAnimation(fragment, Gravity.RIGHT);
        fragment.vendor = vendor;
        fragment.vendorType = ADD_VENDOR;
        return fragment;
    }

    public static Fragment newInstance(Vendor vendor, VendorModel itemVendor, int vendorType) {
        VendorManager fragment = new VendorManager();
        AppAnimation.setSlideAnimation(fragment, Gravity.BOTTOM);
        fragment.vendor = vendor;
        fragment.itemVendor = itemVendor;
        fragment.vendorType = vendorType;
        return fragment;
    }

    public static Fragment newInstance(PurchaseManager purchaseManager, VendorModel itemVendor, int vendorType) {
        VendorManager fragment = new VendorManager();
        AppAnimation.setSlideAnimation(fragment, Gravity.BOTTOM);
        fragment.purchaseManager = purchaseManager;
        fragment.itemVendor = itemVendor;
        fragment.vendorType = vendorType;
        return fragment;
    }

    public static Fragment newInstance(CreditorsTransaction creditorsTransaction, VendorModel itemVendor, int vendorType) {
        VendorManager fragment = new VendorManager();
        AppAnimation.setSlideAnimation(fragment, Gravity.BOTTOM);
        fragment.creditorsTransaction = creditorsTransaction;
        fragment.itemVendor = itemVendor;
        fragment.vendorType = vendorType;
        return fragment;
    }


    public static Fragment newInstance(ServicePurchaseManager servicePurchaseManager, VendorModel itemVendor, int vendorType) {
        VendorManager fragment = new VendorManager();
        AppAnimation.setSlideAnimation(fragment, Gravity.BOTTOM);
        fragment.servicePurchaseManager = servicePurchaseManager;
        fragment.itemVendor = itemVendor;
        fragment.vendorType = vendorType;
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_vendor_add, container, false);

        initToolBar(getActivity(), v, getString(R.string.vendor));
        screenAnalytics(getContext(), AppUser.getUserId(getContext()), "VendorManager");
        dbManager = DBManager.newIsntance(getActivity());
        defaultStateList = AppUser.defaultStates(getContext());
        initUI(v);

        updateUi();
        return v;
    }


    private void updateUi() {
        if (itemVendor != null) {
            etPersonName.setText(itemVendor.getPersonName());
            etCompanyName.setText(itemVendor.getCompanyName());
            etEmail.setText(itemVendor.getContactEmail());
            etWorkPhone.setText(itemVendor.getWorkPhone());
            etMobile.setText(itemVendor.getMobile());
            etDisplayName.setText(itemVendor.getContactDisplayName());
            etWebsite.setText(itemVendor.getWebsite());
            etGSTIN.setText(itemVendor.getGSTIN());
            etDLNumber.setText(itemVendor.getDLNumber());
            etState.setText(itemVendor.getState());
            mSelectedStateCode = itemVendor.getSelectedStateCode();
            etCity.setText(itemVendor.getCity());
            etCountry.setText(itemVendor.getCountry());
            etZip.setText(itemVendor.getZip());
            etAddress.setText(itemVendor.getAddress());
            etFax.setText(itemVendor.getFax());
            etCurrency.setText(itemVendor.getCurrency());
            etPaymentTerms.setText(itemVendor.getPaymentTerms());
            mSelectedTermCode = itemVendor.getSelectedTermCode();
            etRemarks.setText(itemVendor.getRemarks());
            etOpeningBalance.setText(itemVendor.getOpeningBalance() != null ? itemVendor.getOpeningBalance() : "0");
        }
        if (vendorType == VIEW_VENDOR) {
            disableEditFields();
        }
        hideKeybord(getActivity());
    }

    private void enableEditFields() {
        enableEditText(etPersonName);
        enableEditText(etCompanyName);
        enableEditText(etEmail);
        enableEditText(etWorkPhone);
        enableEditText(etMobile);
        enableEditText(etDisplayName);
        enableEditText(etWebsite);
        enableEditText(etGSTIN);
        enableEditText(etDLNumber);
        enableEditText(etCity);
        enableEditText(etCountry);
        enableEditText(etZip);
        enableEditText(etAddress);
        enableEditText(etFax);
        enableEditText(etCurrency);
        enableEditText(etRemarks);
        enableEditText(etOpeningBalance);
    }

    private void disableEditFields() {
        disableEditText(etPersonName);
        disableEditText(etCompanyName);
        disableEditText(etEmail);
        disableEditText(etWorkPhone);
        disableEditText(etMobile);
        disableEditText(etDisplayName);
        disableEditText(etWebsite);
        disableEditText(etGSTIN);
        disableEditText(etDLNumber);
        disableEditText(etCity);
        disableEditText(etCountry);
        disableEditText(etZip);
        disableEditText(etAddress);
        disableEditText(etFax);
        disableEditText(etCurrency);
        disableEditText(etRemarks);
        disableEditText(etOpeningBalance);
    }

    private void initUI(View v) {
        coordinatorLayout = (CoordinatorLayout) v.findViewById(R.id.coordinatorLayout);
        etPersonName = (EditText) v.findViewById(R.id.et_vendor_field_1);
        etCompanyName = (EditText) v.findViewById(R.id.et_vendor_field_2);
        etEmail = (EditText) v.findViewById(R.id.et_vendor_field_3);
        etWorkPhone = (EditText) v.findViewById(R.id.et_vendor_field_4);
        etMobile = (EditText) v.findViewById(R.id.et_vendor_field_5);
        etDisplayName = (EditText) v.findViewById(R.id.et_vendor_field_6);
        etWebsite = (EditText) v.findViewById(R.id.et_vendor_field_7);
        etGSTIN = (EditText) v.findViewById(R.id.et_vendor_field_8);
        etDLNumber = (EditText) v.findViewById(R.id.et_vendor_field_9);
        etState = (EditText) v.findViewById(R.id.et_vendor_field_10);
        etCity = (EditText) v.findViewById(R.id.et_vendor_field_11);
        etCountry = (EditText) v.findViewById(R.id.et_vendor_field_12);
        etZip = (EditText) v.findViewById(R.id.et_vendor_field_13);
        etAddress = (EditText) v.findViewById(R.id.et_vendor_field_14);
        etFax = (EditText) v.findViewById(R.id.et_vendor_field_15);
        etCurrency = (EditText) v.findViewById(R.id.et_vendor_field_16);
        etPaymentTerms = (EditText) v.findViewById(R.id.et_vendor_field_17);
        etRemarks = (EditText) v.findViewById(R.id.et_vendor_field_18);
        etOpeningBalance = (EditText) v.findViewById(R.id.et_customer_field_19);

        btnAction = ProgressButton.newInstance(getContext(), v);
        btnAction.setOnEditorActionListener(etRemarks);

        etState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vendorType == VIEW_VENDOR) {
                    return;
                }
                StateSelector.newInstance(VendorManager.this).show(getFragmentManager(), "stateSelector");
            }
        });
        etPaymentTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vendorType == VIEW_VENDOR) {
                    return;
                }
                PaymentTermsSelector.newInstance(VendorManager.this).show(getFragmentManager(), "paymentTermsSelector");
            }
        });
        etDLNumber.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        etDLNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    // Handle pressing "Enter" key here
                    StateSelector.newInstance(VendorManager.this).show(getFragmentManager(), "stateSelector");
                    handled = true;
                }
                return handled;
            }
        });

        etGSTIN.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setStateInUi(etGSTIN.getText().toString());
            }
        });


        etFax.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        etFax.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    // Handle pressing "Enter" key here
                    PaymentTermsSelector.newInstance(VendorManager.this).show(getFragmentManager(), "paymentTermsSelector");
                    handled = true;
                }
                return handled;
            }
        });
        switch (vendorType) {
            case VIEW_VENDOR:
                btnAction.setText(getString(R.string.edit));
                break;
            case ADD_VENDOR:
                btnAction.setText(getString(R.string.add));
                break;
            case EDIT_VENDOR:
                btnAction.setText(getString(R.string.update));
                break;
            case COPY_VENDOR:
                btnAction.setText(getString(R.string.copy));
                break;
            default:
                btnAction.setText(getString(R.string.add));
                break;
        }
        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnAction.getText().equals("Edit")) {
                    btnAction.setText("Update");
                    vendorType = EDIT_VENDOR;
                    enableEditFields();
                    etPersonName.requestFocus();
                    return;
                }
                if (!FieldValidation.isEmpty(getContext(), etPersonName, getString(R.string.empty))) {
                    showToast(getContext(), getString(R.string.invalid_name));
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), etCompanyName, getString(R.string.invalid_company_name))) {
                    showToast(getContext(), getString(R.string.invalid_company_name));
                    return;
                } else if (!FieldValidation.isGSTIfAvailable(getContext(), etGSTIN, getString(R.string.invalid_gst_number), true)) {
                    showToast(getContext(), getString(R.string.invalid_gst_number));
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), etState, getString(R.string.invalid_state))) {
                    showToast(getContext(), getString(R.string.invalid_state));
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), etCity, getString(R.string.empty))) {
                    showToast(getContext(), getString(R.string.invalid_city));
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), etZip, getString(R.string.empty))) {
                    showToast(getContext(), getString(R.string.invalid_pin_code));
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), etAddress, getString(R.string.invalid_address))) {
                    showToast(getContext(), getString(R.string.invalid_address));
                    return;
                }
                hideKeybord(getActivity());
                executeTask(etPersonName.getText().toString(), etCompanyName.getText().toString(), etEmail.getText().toString()
                        , etWorkPhone.getText().toString(), etMobile.getText().toString(), etDisplayName.getText().toString()
                        , etWebsite.getText().toString(), etGSTIN.getText().toString(), etDLNumber.getText().toString()
                        , etState.getText().toString(), etCity.getText().toString(), etCountry.getText().toString()
                        , etZip.getText().toString(), etAddress.getText().toString(), etFax.getText().toString()
                        , etCurrency.getText().toString(), etPaymentTerms.getText().toString(), etRemarks.getText().toString(), mSelectedStateCode, mSelectedTermCode, etOpeningBalance.getText().toString());

            }
        });
    }


    private void setStateInUi(String gstinNo) {
        if (gstinNo.length() == 15) {
            String stateCode = gstinNo.substring(0, 2);
            int sCode = Utility.parseInt(stateCode);
            String stateName = AppUser.getStateName(defaultStateList, String.valueOf(sCode));
            if (!TextUtils.isEmpty(stateName)) {
                mSelectedStateCode = stateCode;
                etState.setText(stateName);
            } else {
                mSelectedStateCode = null;
                etState.setText(stateName);
            }
        } else {
            mSelectedStateCode = null;
            etState.setText("");
        }
    }

    private void executeTask(String personName, String companyName, String contactEmail, String workPhone, String mobile, String contactDisplayName,
                             String website, String GSTIN, String DLNumber, String state, String city, String country, String zip, String address,
                             String fax, String currency, String paymentTerms, String Remarks, String selectedStateCode, String selectedTermCode, String openingBalance) {
        hideKeybord(getActivity());
        assignTask = new AssignTask(getActivity(), valString(personName), valString(companyName), valString(contactEmail), valString(workPhone), valString(mobile), valString(contactDisplayName), valString(website), valString(GSTIN
        ), valString(DLNumber), valString(state), valString(city), valString(country), valString(zip), valString(address), valString(fax), valString(currency), valString(paymentTerms), valString(Remarks), selectedStateCode, selectedTermCode, openingBalance);
        assignTask.execute(AppSettings.ADD_VENDOR);
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog, String[] param) {
        executeTask(param[0], param[1], param[2], param[3], param[4], param[5], param[6], param[7], param[8], param[9], param[10], param[11], param[12]
                , param[13], param[14], param[15], param[16], param[17], param[18], param[19], param[20]);
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog, String[] param) {

    }

    @Override
    public void onStateSelect(DialogFragment dialog, StateModel selectedState) {
        mSelectedStateCode = selectedState.getStateCode();
        etState.setText(selectedState.getStateName());
        etCity.requestFocus();
        showKeybord(getActivity());
    }

    @Override
    public void onPayTermSelect(DialogFragment dialog, CommonModel selectedTerm) {
        mSelectedTermCode = selectedTerm.getId();
        etPaymentTerms.setText(selectedTerm.getName());
        etRemarks.requestFocus();
        showKeybord(getActivity());
    }


    private class AssignTask extends AsyncTask<String, Void, Model> {
        private final String personName, companyName, contactEmail, workPhone, mobile, contactDisplayName, website, GSTIN, DLNumber, state, city, country, zip, address, fax, currency, paymentTerms, remarks, selectedStateCode, selectedTermCode, openingBalance;
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context, String personName, String companyName, String contactEmail, String workPhone, String mobile, String contactDisplayName,
                          String website, String GSTIN, String DLNumber, String state, String city, String country, String zip, String address,
                          String fax, String currency, String paymentTerms, String remarks, String selectedStateCode, String selectedTermCode, String openingBalance) {
            this.personName = personName;
            this.companyName = companyName;
            this.contactEmail = contactEmail;
            this.workPhone = workPhone;
            this.mobile = mobile;
            this.contactDisplayName = contactDisplayName;
            this.website = website;
            this.GSTIN = GSTIN;
            this.DLNumber = DLNumber;
            this.state = state;
            this.city = city;
            this.country = country;
            this.zip = zip;
            this.address = address;
            this.fax = fax;
            this.currency = currency;
            this.paymentTerms = paymentTerms;
            this.remarks = remarks;
            this.selectedStateCode = selectedStateCode;
            this.selectedTermCode = selectedTermCode;
            this.openingBalance = openingBalance;
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            btnAction.startAnimation();
        }

        @Override
        protected Model doInBackground(String... urls) {
            Model result = new Model();
            String userId = AppUser.getUserId(context);
            String setupId = AppUser.getCompanyId(context);
            if (AppConfig.isUpdateToServer(context)) {
                String response;
                switch (vendorType) {
                    case ADD_VENDOR:
                        response = ApiCall.POST(urls[0], RequestBuilder.insertVendor(userId, setupId, personName, companyName, contactEmail, workPhone, mobile, contactDisplayName, website, GSTIN
                                , DLNumber, state, city, country, zip, address, fax, currency, paymentTerms, remarks, selectedStateCode, selectedTermCode, openingBalance));
                        return JsonParser.defaultStaticParser(response);
                    case EDIT_VENDOR:
                        response = ApiCall.POST(urls[0], RequestBuilder.editVendor(itemVendor.getVendorId(), userId, setupId, personName, companyName, contactEmail, workPhone, mobile, contactDisplayName, website, GSTIN
                                , DLNumber, state, city, country, zip, address, fax, currency, paymentTerms, remarks, selectedStateCode, selectedTermCode, openingBalance));
                        return JsonParser.defaultStaticParser(response);
                    case COPY_VENDOR:
                        response = ApiCall.POST(urls[0], RequestBuilder.insertVendor(userId, setupId, personName, companyName, contactEmail, workPhone, mobile, contactDisplayName, website, GSTIN
                                , DLNumber, state, city, country, zip, address, fax, currency, paymentTerms, remarks, selectedStateCode, selectedTermCode, openingBalance));
                        return JsonParser.defaultStaticParser(response);
                    default:
                        response = ApiCall.POST(urls[0], RequestBuilder.insertVendor(userId, setupId, personName, companyName, contactEmail, workPhone, mobile, contactDisplayName, website, GSTIN
                                , DLNumber, state, city, country, zip, address, fax, currency, paymentTerms, remarks, selectedStateCode, selectedTermCode, openingBalance));
                        return JsonParser.defaultStaticParser(response);
                }
            }
            switch (vendorType) {
                case ADD_VENDOR:
                    return dbManager.insertVendor(result, userId, personName, companyName, contactEmail, workPhone, mobile, contactDisplayName, website, GSTIN
                            , DLNumber, state, city, country, zip, address, fax, currency, paymentTerms, remarks, selectedStateCode, selectedTermCode);
                case EDIT_VENDOR:
                    return dbManager.editVendor(result, itemVendor.getVendorId(), userId, personName, companyName, contactEmail, workPhone, mobile, contactDisplayName, website, GSTIN
                            , DLNumber, state, city, country, zip, address, fax, currency, paymentTerms, remarks, selectedStateCode, selectedTermCode);
                case COPY_VENDOR:
                    return dbManager.copyVendor(result, itemVendor.getVendorId(), userId, personName, companyName, contactEmail, workPhone, mobile, contactDisplayName, website, GSTIN
                            , DLNumber, state, city, country, zip, address, fax, currency, paymentTerms, remarks, selectedStateCode, selectedTermCode);
                default:
                    return dbManager.insertVendor(result, userId, personName, companyName, contactEmail, workPhone, mobile, contactDisplayName, website, GSTIN
                            , DLNumber, state, city, country, zip, address, fax, currency, paymentTerms, remarks, selectedStateCode, selectedTermCode);
            }
        }


        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub 9044095922
            super.onPostExecute(result);
            if (context != null) {
                if (!AppConfig.isUpdateToServer(context)) {
                    if (result.getOutputDB().equals(Const.SUCCESS)) {
                        showSnakBar(coordinatorLayout, result.getOutputDBMsg());
                        if (vendor != null) {
                            vendor.refreshList();
                        }
                        if (vendorType == ADD_VENDOR_WITH_RESPONSE) {
                            if (purchaseManager != null) {
                                purchaseManager.fetchVendorFromDB(personName, companyName);
                            }
                            if (creditorsTransaction != null) {
                                creditorsTransaction.fetchVendorFromDB(personName, companyName);
                            }
                            if (servicePurchaseManager != null) {
                                servicePurchaseManager.fetchVendorFromDB(personName, companyName);
                            }

                        }
                        animateButtonAndRevert(personName, companyName);
                    } else if (result.getOutputDB().equals(Const.FAILURE)) {
                        showSnakBar(coordinatorLayout, result.getOutputDBMsg());
                    }
                } else {
                    if (result.getOutput().equals(Const.SUCCESS)) {
                        showSnakBar(coordinatorLayout, result.getOutputMsg());
                        if (vendor != null) {
                            vendor.refreshList();
                        }
                        if (vendorType == ADD_VENDOR_WITH_RESPONSE) {
                            if (purchaseManager != null) {
                                purchaseManager.fetchVendorFromDB(personName, companyName);
                            }
                            if (creditorsTransaction != null) {
                                creditorsTransaction.fetchVendorFromDB(personName, companyName);
                            }
                            if (servicePurchaseManager != null) {
                                servicePurchaseManager.fetchVendorFromDB(personName, companyName);
                            }

                        }
                        animateButtonAndRevert(personName, companyName);
                    } else if (result.getOutput().equals(Const.FAILURE)) {
                        btnAction.revertAnimation();
                        PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(), "popupAlert");
                    } else {
                        btnAction.revertAnimation();
                        String[] errorSoon = {personName, companyName, contactEmail, workPhone, mobile, contactDisplayName, website, GSTIN
                                , DLNumber, state, city, country, zip, address, fax, currency, paymentTerms, remarks, selectedStateCode, selectedTermCode, openingBalance};
                        ApiCallError.newInstance(result.getOutput(), result.getOutputMsg(), errorSoon, VendorManager.this)
                                .show(getFragmentManager(), "apiCallError");
                    }
                }
            }
        }

        private void animateButtonAndRevert(final String personName, final String companyName) {
            Handler handler = new Handler();


            Runnable runnableRevert = new Runnable() {
                @Override
                public void run() {
                    if (context != null) {
                        getActivity().onBackPressed();
                    }
                }
            };

            btnAction.revertSuccessAnimation();
            handler.postDelayed(runnableRevert, Const.BUTTON_PROGRESS_TIME);
        }

    }

}

