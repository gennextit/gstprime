package com.gennext.offlinegst.user.company;


/**
 * Created by Admin on 5/22/2017.
 */

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.gennext.offlinegst.ImageMaster;
import com.gennext.offlinegst.MainActivity;
import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.PopupAlert;
import com.gennext.offlinegst.model.user.ProfileModel;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.ApiCallError;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppSettings;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.FieldValidation;
import com.gennext.offlinegst.util.JsonParser;
import com.gennext.offlinegst.util.ProgressButtonRounded;
import com.gennext.offlinegst.util.RequestBuilder;
import com.bumptech.glide.Glide;


public class AddProfile extends CompactFragment implements ApiCallError.ErrorParamListener {

    private EditText etName, etEmail, etMobile, etPAN;
    private ProgressButtonRounded btnSigninOtp;
    private DBManager dbManager;
    private String mImagePath = "",mImageUrl="";
    private ImageView ivProfile;
    private ManageProfile manageProfile;
    private AssignTask assignTask;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static Fragment newInstance(ManageProfile manageProfile) {
        AddProfile fragment=new AddProfile();
        AppAnimation.setFadeAnimation(fragment);
        fragment.manageProfile=manageProfile;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.user_profile_add, container, false);
        initToolBar(getActivity(),v,getString(R.string.add_profile));
        screenAnalytics(getContext(),AppUser.getUserId(getContext()),"Add Profile");
        dbManager = DBManager.newIsntance(getActivity());
        initUI(v);
        updateUi();
        return v;
    }

    private void updateUi() {
        etEmail.setText(AppUser.getGmailId(getContext()));
        if (!TextUtils.isEmpty(mImagePath)) {
            Glide.with(getActivity())
                    .load(mImagePath)
                    .into(ivProfile);
        } else {
            Glide.with(getActivity())
                    .load(R.drawable.profile)
                    .into(ivProfile);
        }
    }

    private void initUI(View v) {
        ivProfile = (ImageView) v.findViewById(R.id.iv_profile);
        etName = (EditText) v.findViewById(R.id.et_profile_name);
        etEmail = (EditText) v.findViewById(R.id.et_profile_email);
        etMobile = (EditText) v.findViewById(R.id.et_profile_mobile);
        etPAN = (EditText) v.findViewById(R.id.et_profile_pan);

        btnSigninOtp = ProgressButtonRounded.newInstance(getContext(),v);

        btnSigninOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!FieldValidation.isAlpNum(getContext(),etName,true)){
                    return;
                }else if(!FieldValidation.isEmail(getContext(),etEmail,true)){
                    return;
                }else if(!FieldValidation.isMobileNumber(getContext(),etMobile,true)){
                    return;
                }
                hideKeybord(getActivity());
                executeTask(etName.getText().toString(), etEmail.getText().toString(), etMobile.getText().toString(), etPAN.getText().toString(), mImagePath, mImageUrl);
            }
        });
        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImageMasterActivity();
            }
        });
    }

    private void openImageMasterActivity() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Intent intent = new Intent(getActivity(), ImageMaster.class);
            ActivityOptions options = ActivityOptions
                    .makeSceneTransitionAnimation(getActivity(),ivProfile,getSt(R.string.transition_image));
            startActivityForResult(intent,ImageMaster.REQUEST_PROFILE, options.toBundle());
            return;
        }
        Intent intent = new Intent(getActivity(), ImageMaster.class);
        startActivityForResult(intent, ImageMaster.REQUEST_PROFILE);
    }

    private void executeTask(String name, String email, String mobile, String pan, String imagePath, String imageUrl) {
        assignTask = new AssignTask(getActivity(), valString(name), valString(email), valString(mobile), valString(pan), valString(imagePath), valString(imageUrl));
        assignTask.execute(AppSettings.ADD_PROFILE);
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog, String[] param) {
        executeTask(param[0],param[1],param[2],param[3],param[4],param[5]);
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog, String[] param) {

    }

    public void OnActivityResult(int requestCode, int resultCode, Intent data) {
        Uri uri = data.getData();
        if (uri != null) {
            mImagePath=uri.toString();
            if (!TextUtils.isEmpty(mImagePath)) {
                Glide.with(getActivity())
                        .load(mImagePath)
                        .into(ivProfile);
            } else {
                Glide.with(getActivity())
                        .load(R.drawable.profile)
                        .into(ivProfile);
            }
            AppUser.setImage(getContext(),uri.toString());
        }
    }


    private class AssignTask extends AsyncTask<String, Void, ProfileModel> {
        private final String name, email, mobile, pan, imagePath, imageUrl;
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context, String name, String email, String mobile, String pan, String imagePath, String imageUrl) {
            this.name = name;
            this.email = email;
            this.mobile = mobile;
            this.pan = pan;
            this.imagePath = imagePath;
            this.imageUrl = imageUrl;
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            btnSigninOtp.startAnimation();
        }

        @Override
        protected ProfileModel doInBackground(String... urls) {

            ProfileModel result = new ProfileModel();
            String userId = AppUser.getUserId(context);
            if (AppConfig.isUpdateToServer(context)) {
                String response = ApiCall.POST(urls[0], RequestBuilder.addCompany(userId,name, email, mobile, pan));
                result = JsonParser.addCompanyProfile(response);
            }
            return dbManager.addCompanyProfile(result, AppUser.getUserId(context), name, email, mobile, pan, imagePath, imageUrl);
        }


        @Override
        protected void onPostExecute(ProfileModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                if (!AppConfig.isUpdateToServer(context)) {
                    if (result.getOutputDB().equals(Const.SUCCESS)) {
                        AppUser.setName(context,name);
                        AppUser.setImage(context,imagePath);
                        AppUser.setProfileId(context,result.getProfileId());
                        animateButtonAndRevert(getActivity());
                    } else if (result.getOutputDB().equals(Const.FAILURE)) {
                        btnSigninOtp.revertAnimation();
                        PopupAlert.newInstance(getString(R.string.alert), result.getOutputDBMsg(), PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(), "popupAlert");
                    }
                } else {
                    if (result.getOutput().equals(Const.SUCCESS)) {
                        if (!result.getOutputDB().equals(Const.SUCCESS)) {
                            showToast(context, result.getOutputDBMsg());
                        }
                        AppUser.setName(context,name);
                        AppUser.setImage(context,imagePath);
                        AppUser.setProfileId(context,result.getProfileId());
                        animateButtonAndRevert(getActivity());
                    } else if (result.getOutput().equals(Const.FAILURE)) {
                        if (result.getOutputDB().equals(Const.SUCCESS)) {
                            showToast(context,"Local "+ result.getOutputDBMsg());
                        }
                        btnSigninOtp.revertAnimation();
                        PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(), "popupAlert");
                    } else {
                        if (result.getOutputDB().equals(Const.SUCCESS)) {
                            showToast(context,"Local "+ result.getOutputDBMsg());
                        }
                        btnSigninOtp.revertAnimation();
                        String[] errorSoon = {name,email,mobile,pan,imagePath,imageUrl};
                        ApiCallError.newInstance(result.getOutput(),result.getOutputMsg(), errorSoon, AddProfile.this)
                                .show(getFragmentManager(), "apiCallError");
                    }
                }
            }
        }

        private void animateButtonAndRevert(final Activity activity) {
            Handler handler = new Handler();


            Runnable runnableRevert = new Runnable() {
                @Override
                public void run() {
                    if(context!=null) {
                        if (manageProfile != null) {
                            manageProfile.refreshList();
                            activity.onBackPressed();
                        } else {
                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }
                }
            };

            btnSigninOtp.revertSuccessAnimation();
            handler.postDelayed(runnableRevert, Const.BUTTON_PROGRESS_TIME);
        }

    }


}
