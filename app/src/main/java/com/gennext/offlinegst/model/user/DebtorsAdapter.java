package com.gennext.offlinegst.model.user;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.user.invoice.Debtors;

import java.util.ArrayList;

public class DebtorsAdapter extends RecyclerView.Adapter<DebtorsAdapter.ReyclerViewHolder> {

    private final Debtors parentRef;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<PaymentModel> items;

    public DebtorsAdapter(Activity context, ArrayList<PaymentModel> items,Debtors parentRef) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef = parentRef;
        this.items = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_user_debtors, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final PaymentModel item = items.get(position);

        holder.tvInvoiceNo.setText(item.getInvoiceNo());
        holder.tvPayMode.setText(item.getPaymentMode());
        holder.tvCustName.setText(item.getCustomerName());
        holder.tvPayableAmt.setText(item.getPayableAmount());
        holder.tvPaidAmt.setText(item.getPaidAmount());
        if(item.getBalanceAmount().contains("-")){
            holder.tvBalanceTag.setText(context.getString(R.string.advance_balance));
            holder.tvBalance.setText(item.getBalanceAmount().replace("-",""));
        }else {
            holder.tvBalanceTag.setText(context.getString(R.string.borrow_amount));
            holder.tvBalance.setText(item.getBalanceAmount());
        }

        holder.llSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout llSlot;
        private TextView tvCustName,tvPayMode,tvInvoiceNo,tvPayableAmt,tvPaidAmt,tvBalance,tvBalanceTag;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSlot = (LinearLayout) v.findViewById(R.id.layoutSlot);
            tvCustName= (TextView) v.findViewById(R.id.tv_slot_0);
            tvPayMode = (TextView) v.findViewById(R.id.tv_slot_1);
            tvInvoiceNo = (TextView) v.findViewById(R.id.tv_slot_2);
            tvPayableAmt = (TextView) v.findViewById(R.id.tv_slot_3);
            tvPaidAmt = (TextView) v.findViewById(R.id.tv_slot_4);
            tvBalance = (TextView) v.findViewById(R.id.tv_slot_5);
            tvBalanceTag = (TextView) v.findViewById(R.id.tv_slot_6);
        }
    }
}

