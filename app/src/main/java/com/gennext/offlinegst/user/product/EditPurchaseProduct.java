package com.gennext.offlinegst.user.product;


/**
 * Created by Admin on 5/22/2017.
 */

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.global.CompactFragment;
import com.gennext.offlinegst.global.DatePickerDialog;
import com.gennext.offlinegst.model.user.CommonModel;
import com.gennext.offlinegst.model.user.ProductModel;
import com.gennext.offlinegst.panel.TaxCategorySelector;
import com.gennext.offlinegst.panel.TaxSelector;
import com.gennext.offlinegst.panel.UnitSelector;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.user.purchas.PurchaseManager;
import com.gennext.offlinegst.util.AppAnimation;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.FieldValidation;
import com.gennext.offlinegst.util.ProgressButton;
import com.gennext.offlinegst.util.Utility;


public class EditPurchaseProduct extends CompactFragment implements TaxCategorySelector.SelectListener, UnitSelector.SelectListener, TaxSelector.SelectListener, DatePickerDialog.DateSelectFlagListener {

    private EditText etQuantity,etName, etHSN, etCostPrice, etSalePrice, etTaxRate,etTaxCategory, etBatch, etExpiry;
    private static final int TYPE_RUPEE = 0,TYPE_PERCENT=1;
    private CoordinatorLayout coordinatorLayout;
    private ProductModel itemProduct;
    private EditText etBarcode;


    private int sDay, sMonth, sYear;

    private int eDay, eMonth, eYear;
    private PurchaseManager purchaseManager;
    private String mTaxRate;
    private EditText etProductMRP,etNetAmount;
    private ProgressButton btnAction;
    private EditText etUnit,etCessRate;
    private String mSelectedUnitCode,mSelectedUnitName;
    private String mTaxCategoryId="",mTaxCategoryName;
    private String mNetAmount;
    private String mVendorName;


    public static Fragment newInstance(ProductModel itemProduct, PurchaseManager purchaseManager, String mVendorName) {
        EditPurchaseProduct fragment = new EditPurchaseProduct();
        fragment.itemProduct = itemProduct;
        fragment.purchaseManager = purchaseManager;
        fragment.mVendorName = mVendorName;
        AppAnimation.setSlideAnimation(fragment, Gravity.BOTTOM);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.user_edit_purchase_product, container, false);

        initToolBar(getActivity(), v, getString(R.string.product));
        initUI(v);

        updateUi();
        return v;
    }


    private void updateUi() {
        if (itemProduct != null) {
            etQuantity.setText(itemProduct.getQuantity() != null ? itemProduct.getQuantity() : "");
            etName.setText(itemProduct.getProductName() != null ? itemProduct.getProductName() : "");
            etHSN.setText(itemProduct.getCodeHSN() != null ? itemProduct.getCodeHSN() : "");
            etBarcode.setText(itemProduct.getBarcode() != null ? itemProduct.getBarcode() : "");
            etCostPrice.setText(itemProduct.getCostPrice() != null ? itemProduct.getCostPrice() : "");
            etSalePrice.setText(itemProduct.getSalePrice() != null ? itemProduct.getSalePrice() : "");
            etTaxRate.setText(itemProduct.getTaxRate() != null ? itemProduct.getTaxRate() : "");
            etProductMRP.setText(itemProduct.getMrp() != null ? itemProduct.getMrp() : "");
            mTaxRate=itemProduct.getTaxRate();
            mTaxCategoryId=itemProduct.getTaxCategoryId();
            mTaxCategoryName=itemProduct.getTaxCategory();
            if(mTaxRate.equals("0")||mTaxRate.equals("00")||mTaxRate.equals("0.0")){
                etTaxCategory.setVisibility(View.VISIBLE);
            }else{
                etTaxCategory.setVisibility(View.GONE);
            }
            etTaxCategory.setText(mTaxCategoryName);
            etUnit.setText(itemProduct.getUnitName() != null ? itemProduct.getUnitName() : "");
            mSelectedUnitName=itemProduct.getUnitName();
            mSelectedUnitCode=itemProduct.getUnitCode();
            etCessRate.setText(itemProduct.getCessRate() != null ? itemProduct.getCessRate() : "");
            String itemSale=itemProduct.getSalePrice();
            String taxRate=itemProduct.getTaxRate();

            etBatch.setText(itemProduct.getBatch() != null ? itemProduct.getBatch() : "");
            etExpiry.setText(itemProduct.getExp() != null ? itemProduct.getExp() : "");

            if(itemSale!=null&& taxRate!=null){
                setProductNetAmount( parseFloat(etQuantity.getText().toString()), parseFloat(etSalePrice.getText().toString()), parseFloat(etTaxRate.getText().toString()), parseFloat(etCessRate.getText().toString()));
            }

            if(mVendorName!=null&&mVendorName.equalsIgnoreCase(Const.CASH_PURCHASE)) {
                etTaxRate.setText("0");
                etCessRate.setText("0");
                etTaxCategory.setVisibility(View.GONE);
                etTaxRate.setClickable(true);
                etTaxRate.setFocusable(false);
                etTaxRate.setFocusableInTouchMode(false);
                etCessRate.setClickable(true);
                etCessRate.setFocusable(false);
                etCessRate.setFocusableInTouchMode(false);
                etTaxRate.setBackgroundResource(R.drawable.et_row_disable);
                etCessRate.setBackgroundResource(R.drawable.et_row_disable);
            }
//            else{
//                etTaxRate.setText("0");
//                etCessRate.setText("0");
//                etTaxCategory.setVisibility(View.GONE);
//            }
        }
    }

    private void setProductNetAmount(float quantity ,float salePrice, float taxRate, float cessRate) {
        float totalTax = (salePrice * (taxRate + cessRate)) / 100;
        mNetAmount=Utility.decimalFormat((salePrice+totalTax), "#.##");
        etNetAmount.setText(Utility.decimalFormat((salePrice+totalTax)*quantity, "#.##"));
    }
    private void initUI(View v) {
        coordinatorLayout = (CoordinatorLayout) v.findViewById(R.id.coordinatorLayout);
        etQuantity = (EditText) v.findViewById(R.id.et_product_qty);
        etName = (EditText) v.findViewById(R.id.et_product_name);
        etUnit = (EditText) v.findViewById(R.id.et_product_unit);
        etHSN = (EditText) v.findViewById(R.id.et_product_hsn);
        etBarcode = (EditText) v.findViewById(R.id.et_product_barcode);
        etCostPrice = (EditText) v.findViewById(R.id.et_product_cost);
        etSalePrice = (EditText) v.findViewById(R.id.et_product_sale);
        etTaxRate = (EditText) v.findViewById(R.id.et_product_tax);
        etTaxCategory = (EditText) v.findViewById(R.id.et_product_tax_category);
        etBatch = (EditText) v.findViewById(R.id.et_product_batch);
        etExpiry = (EditText) v.findViewById(R.id.et_product_exp);
        etCessRate = (EditText) v.findViewById(R.id.et_product_cess);
        etProductMRP = (EditText) v.findViewById(R.id.et_product_mrp);
        etNetAmount = (EditText) v.findViewById(R.id.et_product_net_amount);

        etSalePrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setProductNetAmount( parseFloat(etQuantity.getText().toString()), parseFloat(etSalePrice.getText().toString()), parseFloat(etTaxRate.getText().toString()), parseFloat(etCessRate.getText().toString()));
            }
        });

        etCessRate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setProductNetAmount( parseFloat(etQuantity.getText().toString()), parseFloat(etSalePrice.getText().toString()), parseFloat(etTaxRate.getText().toString()), parseFloat(etCessRate.getText().toString()));
            }
        });

        etQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setProductNetAmount( parseFloat(etQuantity.getText().toString()), parseFloat(etSalePrice.getText().toString()), parseFloat(etTaxRate.getText().toString()), parseFloat(etCessRate.getText().toString()));
            }
        });

        etExpiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeybord(getActivity());
                DatePickerDialog.newInstance(EditPurchaseProduct.this, false, DatePickerDialog.END_DATE, eDay, eMonth, eYear)
                        .show(getFragmentManager(), "datePickerDialog");
            }
        });

        btnAction= ProgressButton.newInstance(getContext(),v);
        btnAction.setText(getString(R.string.apply));

        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!FieldValidation.isProductName(getContext(),etName,true)){
                    return;
                }else if(!FieldValidation.isEmpty(getContext(),mSelectedUnitCode,getString(R.string.please_select_product_unit))){
                    return;
                }else if(!FieldValidation.isHSNCode(getContext(),etHSN,getString(R.string.invalid_hsn_code))){
                    return;
                }else if(!FieldValidation.isEmpty(getContext(),etSalePrice,getString(R.string.invalid_sale_price))){
                    return;
                }else if(!FieldValidation.isEmpty(getContext(),etTaxRate,getString(R.string.invalid_tax_rate))){
                    return;
                }else if (etTaxCategory.getVisibility()==View.VISIBLE && !FieldValidation.isEmpty(getContext(), etTaxCategory,getString(R.string.invalid_tax_category))) {
                    return;
                }else if (!validateMRP(Utility.parseFloat(etNetAmount.getText().toString()),Utility.parseFloat(etProductMRP.getText().toString()))) {
                    return;
                }
                hideKeybord(getActivity());
                executeTask(etName.getText().toString(),etQuantity.getText().toString(), etHSN.getText().toString(), etBarcode.getText().toString(), etCostPrice.getText().toString()
                        , etSalePrice.getText().toString(), etTaxRate.getText().toString(), etBatch.getText().toString(), etExpiry.getText().toString(),mSelectedUnitCode,mSelectedUnitName,etCessRate.getText().toString(),etProductMRP.getText().toString(),etTaxCategory.getText().toString(),mTaxCategoryId);


            }
        });


        etTaxRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!mVendorName.equalsIgnoreCase(Const.CASH_PURCHASE)) {
                    hideKeybord(getActivity());
                    TaxSelector.newInstance(EditPurchaseProduct.this).show(getFragmentManager(), "taxSelector");
                }
            }
        });
        etTaxCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeybord(getActivity());
                TaxCategorySelector.newInstance(EditPurchaseProduct.this).show(getFragmentManager(),"taxCategorySelector");
            }
        });
        etUnit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeybord(getActivity());
                UnitSelector.newInstance(EditPurchaseProduct.this).show(getFragmentManager(),"unitSelector");
            }
        });
        if(AppConfig.isBatchAvailable(getContext())){
            etBatch.setVisibility(View.VISIBLE);
        }else{
            etBatch.setVisibility(View.GONE);
        }
        if(AppConfig.isExpAvailable(getContext())){
            etExpiry.setVisibility(View.VISIBLE);
        }else{
            etExpiry.setVisibility(View.GONE);
        }
        if(AppConfig.getCessAvailable(getContext())){
            etCessRate.setVisibility(View.VISIBLE);
        }else{
            etCessRate.setVisibility(View.GONE);
        }
    }

    private boolean validateMRP(float netAmt, float prodMRP) {
        if(netAmt<=prodMRP){
            return true;
        }else{
            showSnakBar(coordinatorLayout,getString(R.string.mrp_error_msg));
            return true;
        }
    }



    private void executeTask(String productDescription,String quentity, String codeHSN, String barcode
            , String costPrice, String salePrice, String taxRate, String batch , String expiry
            , String unitCode, String unitName, String cessRate,String mrp,String taxCategory
            ,String taxCategoryId) {
        itemProduct.setProductName(productDescription);
        itemProduct.setCodeHSN(codeHSN);
        itemProduct.setBarcode(barcode);
        itemProduct.setUnitCode(unitCode);
        itemProduct.setUnitName(unitName);
        itemProduct.setCostPrice(salePrice);
        itemProduct.setSalePrice(salePrice);
        itemProduct.setMrp(mrp);
        itemProduct.setTaxRate(taxRate);
        itemProduct.setBatch(batch);
        itemProduct.setExp(expiry);
        itemProduct.setTaxCategoryId(taxCategoryId);
        itemProduct.setTaxCategory(taxCategory);
        itemProduct.setCessRate(cessRate);
        itemProduct.setQuantity(quentity);
        itemProduct.setNetAmount(mNetAmount);
        itemProduct.setDis("0");
        if(purchaseManager!=null){
//            ((InvoiceManager)getParentFragment()).onProductEdited(itemProduct);
            purchaseManager.onProductEdited(itemProduct);
            getFragmentManager().popBackStack();
        }
    }

    @Override
    public void onStartDateSelected(DialogFragment dialog, int day, int month, int year, String ddMMMyy) {
        sDay = day;
        sMonth = month;
        sYear = year;
        etBatch.setText(ddMMMyy);
    }

    @Override
    public void onEndDateSelected(DialogFragment dialog, int day, int month, int year, String ddMMMyy) {
        eDay = day;
        eMonth = month;
        eYear = year;
        etExpiry.setText(ddMMMyy);
    }

    @Override
    public void onTaxSelect(DialogFragment dialog, CommonModel selectedItem) {
        mTaxRate=selectedItem.getId();
        etTaxRate.setText(selectedItem.getName());
        FieldValidation.isEmpty(getContext(),etTaxRate,getString(R.string.invalid_tax_rate));
        setProductNetAmount( parseFloat(etQuantity.getText().toString()), parseFloat(etSalePrice.getText().toString()), parseFloat(etTaxRate.getText().toString()), parseFloat(etCessRate.getText().toString()));
        if(mTaxRate.equals("0")||mTaxRate.equals("00")||mTaxRate.equals("0.0")){
            etTaxCategory.setVisibility(View.VISIBLE);
        }else{
            etTaxCategory.setVisibility(View.GONE);
        }
    }

    @Override
    public void onUnitSelect(DialogFragment dialog, ProductModel selectUnit) {
        mSelectedUnitCode=selectUnit.getUnitCode();
        mSelectedUnitName=selectUnit.getUnitName();
        etUnit.setText(selectUnit.getUnitName());
    }

    @Override
    public void onTaxCategorySelect(DialogFragment dialog, CommonModel selectedTerm) {
        mTaxCategoryId=selectedTerm.getId();
        mTaxCategoryName=selectedTerm.getName();
        etTaxCategory.setText(mTaxCategoryName);
        FieldValidation.isEmpty(getContext(), etTaxCategory,getString(R.string.invalid_tax_category));
    }

}
