package com.gennext.offlinegst.model.user;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.user.purchas.PurchaseManager;
import com.gennext.offlinegst.util.Utility;

import java.util.ArrayList;
import java.util.Random;

public class PurchaseProductViewAdapter extends RecyclerView.Adapter<PurchaseProductViewAdapter.ReyclerViewHolder> {

    private final PurchaseManager parentRef;
    private final boolean isIGSTApplicable;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<ProductModel> items;

    public PurchaseProductViewAdapter(Activity context, ArrayList<ProductModel> items, PurchaseManager parentRef, boolean isIGSTApplicable) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef = parentRef;
        this.isIGSTApplicable = isIGSTApplicable;
        this.items = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_selected_product_view, parent, false);
        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final ProductModel item = items.get(position);
        String costPrice = item.getCostPrice();
        if (costPrice.equals("")) {
            costPrice = item.getSalePrice();
        }
        float price = convertToFloat(costPrice);
        float qun = convertToFloat(item.getQuantity());
        float dis = convertToFloat(item.getDis());
        float cessRate = convertToFloat(item.getCessRate());
        float taxRate = convertToFloat(item.getTaxRate());
        float taxable = (price * qun) - dis;
        float totalTax = taxable + ((taxable * (taxRate + cessRate)) / 100);
        String mTaxable = Utility.decimalFormat(taxable, "#.##");
        String mNetAmt = Utility.decimalFormat(totalTax, "#.##");
        float allTax = calTax(taxable, taxRate);
        float allCessTax = calTax(taxable, cessRate);
        String mCessAmt = Utility.decimalFormat(allCessTax, "#.##");
        holder.tvName.setText(item.getProductName());
        holder.tvQuantity.setText(generateBoldTitle("Qty:", Utility.decimalFormat(qun, "#.##")));
        holder.tvUnit.setText(generateBoldTitle("Unit:", item.getUnitName()));
        holder.tvPrice.setText(generateBoldTitle("Price:", context.getString(R.string.rs) + item.getSalePrice()));
        holder.tvCessRate.setText(generateBoldTitle("Cess:", mCessAmt));
        if (isIGSTApplicable) {
            String allTx = Utility.decimalFormat(allTax, "#.##");
            holder.tvTaxRate.setText(generateBoldTitle("IGST:", allTx));
        } else {
            String allTx = Utility.decimalFormat(allTax, "#.##");
            holder.tvTaxRate.setText(generateBoldTitle("SGST+CGST:", allTx));
        }
        holder.tvTaxable.setText(generateBoldTitle("Taxable:", context.getString(R.string.rs) + mTaxable));
        holder.tvNet.setText(generateBoldTitle("Net:", context.getString(R.string.rs) + mNetAmt));
        setColor(holder.llSlot);
    }

    public void setColor(LinearLayout tv) {
        Random rnd = new Random();
        int color = Utility.SIDE_LINE_COLORS[rnd.nextInt(21)];
        tv.setBackgroundColor(color);
    }

    public static SpannableString generateBoldTitle(String title, String noramalText) {
        SpannableString s = new SpannableString(title + " " + noramalText);
        s.setSpan(new StyleSpan(Typeface.BOLD), 0, title.length(), 0);
        return s;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout llSlot;
        private TextView tvName, tvQuantity, tvUnit, tvPrice, tvTaxRate, tvCessRate, tvTaxable, tvNet;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSlot = (LinearLayout) v.findViewById(R.id.ll_slot);
            tvName = (TextView) v.findViewById(R.id.tv_slot_0);
            tvQuantity = (TextView) v.findViewById(R.id.tv_slot_1);
            tvUnit = (TextView) v.findViewById(R.id.tv_slot_2);
            tvPrice = (TextView) v.findViewById(R.id.tv_slot_3);
            tvTaxRate = (TextView) v.findViewById(R.id.tv_slot_4);
            tvCessRate = (TextView) v.findViewById(R.id.tv_slot_5);
            tvTaxable = (TextView) v.findViewById(R.id.tv_slot_6);
            tvNet = (TextView) v.findViewById(R.id.tv_slot_7);

        }
    }
    private static float calTax(float taxableAmount, float maxTaxRate) {
        return (taxableAmount * maxTaxRate) / 100;
    }
    private static String[] calTax(float taxableAmount, float maxTaxRate, float maxCessRate, Boolean isIGSTApplicable) {
        String[] taxCal = new String[4];
        if (isIGSTApplicable) {
            taxCal[0] = String.valueOf((taxableAmount * (maxTaxRate + maxCessRate)) / 100);
            taxCal[1] = "0";
            taxCal[2] = "0";
            taxCal[3] = String.valueOf((taxableAmount * maxTaxRate) / 100);
        } else {
            taxCal[0] = String.valueOf((taxableAmount * (maxTaxRate + maxCessRate)) / 100);
            maxTaxRate = maxTaxRate / 2;
            taxCal[1] = String.valueOf((taxableAmount * maxTaxRate) / 100);
            taxCal[2] = String.valueOf((taxableAmount * maxTaxRate) / 100);
            taxCal[3] = "0";
        }
        return taxCal;
    }

    private static float convertToFloat(String salePrice) {
        try {
            return Float.parseFloat(salePrice);
        } catch (NumberFormatException e) {
            return 0;
        } catch (NullPointerException e) {
            return 0;
        }
    }

}

