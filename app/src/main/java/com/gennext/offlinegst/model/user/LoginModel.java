package com.gennext.offlinegst.model.user;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Admin on 9/28/2017.
 */

public class LoginModel {

    private String userId;
    private String userName;
    private String pan;
    private String email;
    private String password;
    private String phone;
    private String role;
    private String image;
    private String set_up;
    private String status;
    private String createdAt;

    private JSONObject userDetails;
    private JSONArray companyDetails;
    private JSONArray unitQuantityCode;
    private JSONArray taxDetails;
    private JSONArray states;
    private JSONArray terms;
    private JSONArray invoiceTypes;
    private JSONArray purchaseCategory;
    private JSONArray purchaseType;
    private JSONArray taxCategories;
    private JSONArray transportMode;

    private String output;
    private String outputMsg;

    public JSONArray getTransportMode() {
        return transportMode;
    }

    public void setTransportMode(JSONArray transportMode) {
        this.transportMode = transportMode;
    }

    public JSONArray getPurchaseCategory() {
        return purchaseCategory;
    }

    public void setPurchaseCategory(JSONArray purchaseCategory) {
        this.purchaseCategory = purchaseCategory;
    }

    public JSONArray getPurchaseType() {
        return purchaseType;
    }

    public void setPurchaseType(JSONArray purchaseType) {
        this.purchaseType = purchaseType;
    }

    public JSONArray getTaxCategories() {
        return taxCategories;
    }

    public void setTaxCategories(JSONArray taxCategories) {
        this.taxCategories = taxCategories;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSet_up() {
        return set_up;
    }

    public void setSet_up(String set_up) {
        this.set_up = set_up;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public JSONObject getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(JSONObject userDetails) {
        this.userDetails = userDetails;
    }

    public JSONArray getCompanyDetails() {
        return companyDetails;
    }

    public void setCompanyDetails(JSONArray companyDetails) {
        this.companyDetails = companyDetails;
    }

    public JSONArray getUnitQuantityCode() {
        return unitQuantityCode;
    }

    public void setUnitQuantityCode(JSONArray unitQuantityCode) {
        this.unitQuantityCode = unitQuantityCode;
    }

    public JSONArray getTaxDetails() {
        return taxDetails;
    }

    public void setTaxDetails(JSONArray taxDetails) {
        this.taxDetails = taxDetails;
    }

    public JSONArray getStates() {
        return states;
    }

    public void setStates(JSONArray states) {
        this.states = states;
    }

    public JSONArray getTerms() {
        return terms;
    }

    public void setTerms(JSONArray terms) {
        this.terms = terms;
    }

    public JSONArray getInvoiceTypes() {
        return invoiceTypes;
    }

    public void setInvoiceTypes(JSONArray invoiceTypes) {
        this.invoiceTypes = invoiceTypes;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }
}
