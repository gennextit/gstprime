package com.gennext.offlinegst.panel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.model.user.CustomerModel;
import com.gennext.offlinegst.model.user.InvoiceModel;
import com.gennext.offlinegst.model.user.ProductModel;
import com.gennext.offlinegst.model.user.PurchasesModel;
import com.gennext.offlinegst.model.user.ServiceModel;
import com.gennext.offlinegst.model.user.VendorModel;


public class ActionMenu extends BottomSheetDialogFragment implements View.OnClickListener {
    private MenuListener mListener;
    private MenuProduct mProductListener;
    private MenuService mServiceListener;
    private ProductModel itemProduct;
    private int position;
    private ServiceModel itemService;
    private CustomerModel itemCustomer;
    private MenuCustomer mCustomerListener;
    private VendorModel itemVendor;
    private MenuVendor mVendorListener;
    private PurchasesModel itemPurchases;
    private MenuPurchases mPurchasesListener;

    public interface MenuListener {
        void onViewClick(DialogFragment dialog);

        void onEditClick(DialogFragment dialog);

        void onDeleteClick(DialogFragment dialog);

        void onCopyClick(DialogFragment dialog);
    }

    public interface MenuProduct {
        void onViewClick(DialogFragment dialog, ProductModel product, int position);

        void onEditClick(DialogFragment dialog, ProductModel product, int position);

        void onDeleteClick(DialogFragment dialog, ProductModel product, int position);

        void onCopyClick(DialogFragment dialog, ProductModel product, int position);
    }

    public interface MenuService {
        void onViewClick(DialogFragment dialog, ServiceModel service, int position);

        void onEditClick(DialogFragment dialog, ServiceModel service, int position);

        void onDeleteClick(DialogFragment dialog, ServiceModel service, int position);

        void onCopyClick(DialogFragment dialog, ServiceModel service, int position);
    }

    public interface MenuCustomer {
        void onViewClick(DialogFragment dialog, CustomerModel customer, int position);

        void onEditClick(DialogFragment dialog, CustomerModel customer, int position);

        void onDeleteClick(DialogFragment dialog, CustomerModel customer, int position);

        void onCopyClick(DialogFragment dialog, CustomerModel customer, int position);
    }

    public interface MenuVendor {
        void onViewClick(DialogFragment dialog, VendorModel customer, int position);

        void onEditClick(DialogFragment dialog, VendorModel customer, int position);

        void onDeleteClick(DialogFragment dialog, VendorModel customer, int position);

        void onCopyClick(DialogFragment dialog, VendorModel customer, int position);
    }

    public interface MenuPurchases {
        void onViewClick(DialogFragment dialog, PurchasesModel customer, int position);

        void onEditClick(DialogFragment dialog, PurchasesModel customer, int position);

        void onDeleteClick(DialogFragment dialog, PurchasesModel customer, int position);

        void onCopyClick(DialogFragment dialog, PurchasesModel customer, int position);
    }

    public interface MenuInvoice {
        void onViewClick(DialogFragment dialog, InvoiceModel customer, int position);

        void onEditClick(DialogFragment dialog, InvoiceModel customer, int position);

        void onDeleteClick(DialogFragment dialog, InvoiceModel customer, int position);

        void onCopyClick(DialogFragment dialog, InvoiceModel customer, int position);
    }

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }


    public static ActionMenu newInstance(MenuListener listener) {
        ActionMenu fragment = new ActionMenu();
        fragment.mListener = listener;
        return fragment;
    }

    public static ActionMenu newInstance(MenuProduct listener, ProductModel itemProduct, int position) {
        ActionMenu fragment = new ActionMenu();
        fragment.mProductListener = listener;
        fragment.itemProduct = itemProduct;
        fragment.position = position;
        return fragment;
    }

    public static ActionMenu newInstance(MenuService listener, ServiceModel itemService, int position) {
        ActionMenu fragment = new ActionMenu();
        fragment.mServiceListener = listener;
        fragment.itemService = itemService;
        fragment.position = position;
        return fragment;
    }


    public static ActionMenu newInstance(MenuCustomer mCustomerListener, CustomerModel itemCustomer, int position) {
        ActionMenu fragment = new ActionMenu();
        fragment.mCustomerListener = mCustomerListener;
        fragment.itemCustomer = itemCustomer;
        fragment.position = position;
        return fragment;
    }

    public static ActionMenu newInstance(MenuVendor mVendorListener, VendorModel itemVendor, int position) {
        ActionMenu fragment = new ActionMenu();
        fragment.mVendorListener = mVendorListener;
        fragment.itemVendor = itemVendor;
        fragment.position = position;
        return fragment;
    }

    public static ActionMenu newInstance(MenuPurchases mPurchasesListener, PurchasesModel itemPurchases, int position) {
        ActionMenu fragment = new ActionMenu();
        fragment.mPurchasesListener = mPurchasesListener;
        fragment.itemPurchases = itemPurchases;
        fragment.position = position;
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.menu_action, container, false);

        TextView tvTitle = (TextView) v.findViewById(R.id.action_menu_title);
        ImageView ivClose = (ImageView) v.findViewById(R.id.action_menu_close);
        LinearLayout view = (LinearLayout) v.findViewById(R.id.action_menu_view);
        LinearLayout edit = (LinearLayout) v.findViewById(R.id.action_menu_edit);
        LinearLayout delete = (LinearLayout) v.findViewById(R.id.action_menu_delete);
        ImageView ivDelete = (ImageView) v.findViewById(R.id.iv_delete);
        TextView tvDelete = (TextView) v.findViewById(R.id.tv_delete);
        LinearLayout copy = (LinearLayout) v.findViewById(R.id.action_menu_copy);

        if (itemPurchases != null) {
            edit.setVisibility(View.GONE);
            tvTitle.setText(getString(R.string.options_for)+" " + itemPurchases.getInvoiceNumber());
            ivDelete.setImageResource(R.mipmap.ic_action_delete);
            tvDelete.setText(getString(R.string.delete));
            if(itemPurchases.getCancellation()){
                delete.setVisibility(View.VISIBLE);
            }else {
                delete.setVisibility(View.GONE);
            }
        }
        if (itemProduct != null) {
            delete.setVisibility(View.GONE);
            edit.setVisibility(View.VISIBLE);
            tvTitle.setText(itemProduct.getProductName());
        }
        if (itemVendor!= null) {
            delete.setVisibility(View.GONE);
            tvTitle.setText(itemVendor.getPersonName());
        }
        if (itemCustomer!= null) {
            delete.setVisibility(View.GONE);
            tvTitle.setText(itemCustomer.getPersonName());
        }
        if (itemService != null) {
            edit.setVisibility(View.VISIBLE);
            tvTitle.setText(itemService.getServiceName());
        }

        ivClose.setOnClickListener(this);
        view.setOnClickListener(this);
        edit.setOnClickListener(this);
        delete.setOnClickListener(this);
        copy.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.action_menu_close:
                dismiss();
                break;
            case R.id.action_menu_view:
                dismiss();
                if (mListener != null) {
                    mListener.onViewClick(ActionMenu.this);
                }
                if (mProductListener != null) {
                    mProductListener.onViewClick(ActionMenu.this, itemProduct, position);
                }
                if (mServiceListener != null) {
                    mServiceListener.onViewClick(ActionMenu.this, itemService, position);
                }
                if (mCustomerListener != null) {
                    mCustomerListener.onViewClick(ActionMenu.this, itemCustomer, position);
                }
                if (mVendorListener != null) {
                    mVendorListener.onViewClick(ActionMenu.this, itemVendor, position);
                }
                if (mPurchasesListener != null) {
                    mPurchasesListener.onViewClick(ActionMenu.this, itemPurchases, position);
                }
                break;
            case R.id.action_menu_edit:
                dismiss();
                if (mListener != null) {
                    mListener.onEditClick(ActionMenu.this);
                }
                if (mProductListener != null) {
                    mProductListener.onEditClick(ActionMenu.this, itemProduct, position);
                }
                if (mServiceListener != null) {
                    mServiceListener.onEditClick(ActionMenu.this, itemService, position);
                }
                if (mCustomerListener != null) {
                    mCustomerListener.onEditClick(ActionMenu.this, itemCustomer, position);
                }
                if (mVendorListener != null) {
                    mVendorListener.onEditClick(ActionMenu.this, itemVendor, position);
                }
                if (mPurchasesListener != null) {
                    mPurchasesListener.onEditClick(ActionMenu.this, itemPurchases, position);
                }
                break;
            case R.id.action_menu_delete:
                dismiss();
                if (mListener != null) {
                    mListener.onDeleteClick(ActionMenu.this);
                }
                if (mProductListener != null) {
                    mProductListener.onDeleteClick(ActionMenu.this, itemProduct, position);
                }
                if (mServiceListener != null) {
                    mServiceListener.onDeleteClick(ActionMenu.this, itemService, position);
                }
                if (mCustomerListener != null) {
                    mCustomerListener.onDeleteClick(ActionMenu.this, itemCustomer, position);
                }
                if (mVendorListener != null) {
                    mVendorListener.onDeleteClick(ActionMenu.this, itemVendor, position);
                }
                if (mPurchasesListener != null) {
                    mPurchasesListener.onDeleteClick(ActionMenu.this, itemPurchases, position);
                }
                break;
            case R.id.action_menu_copy:
                dismiss();
                if (mListener != null) {
                    mListener.onCopyClick(ActionMenu.this);
                }
                if (mProductListener != null) {
                    mProductListener.onCopyClick(ActionMenu.this, itemProduct, position);
                }
                if (mServiceListener != null) {
                    mServiceListener.onCopyClick(ActionMenu.this, itemService, position);
                }
                if (mCustomerListener != null) {
                    mCustomerListener.onCopyClick(ActionMenu.this, itemCustomer, position);
                }
                if (mVendorListener != null) {
                    mVendorListener.onCopyClick(ActionMenu.this, itemVendor, position);
                }
                if (mPurchasesListener != null) {
                    mPurchasesListener.onCopyClick(ActionMenu.this, itemPurchases, position);
                }
                break;

        }


    }


}
