package com.gennext.offlinegst.model.user;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gennext.offlinegst.R;

import java.util.ArrayList;

public class ReceiptAdapter extends RecyclerView.Adapter<ReceiptAdapter.ReyclerViewHolder> {

    public static final int PAID_BUYER = 1, UNPAID_BUYER = 2, OVERPAID_BUYER = 3;
    public static final int PAID_SELLER = 4, UNPAID_SELLER = 5, OVERPAID_SELLER = 6;
    private final int type;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<PaymentModel> items;

    public ReceiptAdapter(Activity context, ArrayList<PaymentModel> items, int type) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.items = items;
        this.type = type;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_user_receipt, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final PaymentModel item = items.get(position);

        holder.tvSNo.setText(String.valueOf(position + 1));

        switch (type) {
            case PAID_BUYER:
                holder.tvName.setText(item.getVendorName());
                holder.tvBalance.setText(item.getPaidAmount());
                break;
            case UNPAID_BUYER:
                holder.tvName.setText(item.getVendorName());
                holder.tvBalance.setText(item.getBalanceAmount());
                break;
            case OVERPAID_BUYER:
                holder.tvName.setText(item.getVendorName());
                holder.tvBalance.setText(item.getBalanceAmount().replace("-", ""));
                break;
            case PAID_SELLER:
                holder.tvName.setText(item.getCustomerName());
                holder.tvBalance.setText(item.getPaidAmount());
                break;
            case UNPAID_SELLER:
                holder.tvName.setText(item.getCustomerName());
                holder.tvBalance.setText(item.getBalanceAmount());
                break;
            case OVERPAID_SELLER:
                holder.tvName.setText(item.getCustomerName());
                holder.tvBalance.setText(item.getBalanceAmount().replace("-", ""));
                break;


        }

    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView tvSNo, tvName, tvBalance;

        private ReyclerViewHolder(final View v) {
            super(v);
            tvSNo = (TextView) v.findViewById(R.id.tv_slot_0);
            tvName = (TextView) v.findViewById(R.id.tv_slot_1);
            tvBalance = (TextView) v.findViewById(R.id.tv_slot_2);
        }
    }
}

