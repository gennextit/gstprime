package com.gennext.offlinegst;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.DateTimeUtility;
import com.gennext.offlinegst.util.imageMaster.FileUtils;
import com.bumptech.glide.Glide;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.util.ArrayList;

public class ImageMaster extends NavDrawer {

    public static final String IMAGE_CACHE = "imageCache";
    private static final int IMAGE_CAMERA = 1;
    private static final int IMAGE_PICKER = 3;
    public static final int REQUEST_PROFILE = 101;
    private ImageView ivPreview;
    private Uri mImageUri;
    private Dialog dialog;
    private TextView tvUri;
    private ImageView ivPlus;
    private PermissionListener permissionlistener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_master);
        setNavBar(getString(R.string.profile));
        initUi();
        setProfileImage();
        String imageCache = getIntent().getStringExtra(IMAGE_CACHE);
        updatePic(imageCache);
//        OptionImageAlert(this);
//        ImageMasterPermissionsDispatcher.OptionImageAlertWithCheck(ImageMaster.this,this);
    }

    private void updatePic(String imageCache) {
        if (!TextUtils.isEmpty(imageCache)) {
            mImageUri=Uri.parse(imageCache);
            Glide.with(ImageMaster.this)
                    .load(imageCache)
                    .into(ivPreview);
        } else {
            Glide.with(ImageMaster.this)
                    .load(R.drawable.profile)
                    .into(ivPreview);
        }
    }

    private void setProfileImage() {
         ivPreview.setImageResource(R.drawable.profile);

    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(mImageUri!=null) {
            outState.putString("mImageUri", mImageUri.toString());
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mImageUri=Uri.parse(savedInstanceState.getString("mImageUri"));
    }


    private void initUi() {
        ivPreview = (ImageView) findViewById(R.id.iv_preview);
        ivPlus = (ImageView) findViewById(R.id.iv_plus);
        tvUri = (TextView) findViewById(R.id.tvUri);
        Button btnDone = (Button) findViewById(R.id.btn_done);
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mImageUri != null) {
//                    tvUri.setText(mImageUri.toString());
                    startActivityForResult(mImageUri);
                } else {
                    tvUri.setText(R.string.no_image_found);
                }
            }
        });
        ivPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OptionImageAlertWithCheck();
            }
        });
        ivPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OptionImageAlertWithCheck();
            }
        });
        permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                OptionImageAlert(ImageMaster.this);
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(getApplicationContext(), getString(R.string.permission_denied)+"\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }


        };
    }

    private void startActivityForResult(Uri mImageUri) {
        Intent backIntent = new Intent();
        backIntent.setData(mImageUri);
        setResult(REQUEST_PROFILE, backIntent);
        finish();
    }

    public void OptionImageAlertWithCheck(){
//        new TedPermission(this)
//                .setPermissionListener(permissionlistener)
//                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
//                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                .check();
        new TedPermission(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage(getString(R.string.if_you_reject_permission_you_can_not_use_this_service_please_turn_on_permissions))
                .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .check();
    }

    public void OptionImageAlert(Activity Act) {
        tvUri.setText("");
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Act);
        LayoutInflater inflater = Act.getLayoutInflater();
        View v = inflater.inflate(R.layout.alert_image_option, null);
        dialogBuilder.setView(v);
        Button button1 = (Button) v.findViewById(R.id.button1);
        Button button2 = (Button) v.findViewById(R.id.button2);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                String fileName = "profile_" + DateTimeUtility.getTimeStamp() + ".png";
                File profileFile = new File(getExternalFilesDir(AppConfig.FOLDER_PROFILE), fileName);

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                mImageUri = getUriFromFile(profileFile);
                if (mImageUri != null) {
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
                try {
                    intent.putExtra("return-data", true);
                    /*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);*/
                    startActivityForResult(intent, IMAGE_CAMERA);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }

            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent target = FileUtils.createGetContentIntent();
                // Create the chooser Intent
                Intent intent = Intent.createChooser(target, getString(R.string.select_image));
                try {
                    startActivityForResult(intent, IMAGE_PICKER);
                } catch (ActivityNotFoundException e) {
                    // The reason for the existence of aFileChooser
                }

            }
        });
        dialog = dialogBuilder.create();
        dialog.show();

    }

    private Uri getUriFromFile(File docOutput) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            return FileProvider.getUriForFile(ImageMaster.this, getPackageName() + ".provider", docOutput);
        } else {
            return Uri.fromFile(docOutput);
        }
    }

    public void OpenFilePicker() {
        Intent target = FileUtils.createGetContentIntent();
        // Create the chooser Intent
        Intent intent = Intent.createChooser(target, getString(R.string.select_image));
        try {
            startActivityForResult(intent, IMAGE_PICKER);
        } catch (ActivityNotFoundException e) {
            // The reason for the existence of aFileChooser
        }
    }




    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                setImageAndStore(result.getUri());
//                mImageUri = result.getUri();
//                ivPreview.setImageURI(mImageUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Toast.makeText(this, "Cropping failed: please select a valid Image" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case IMAGE_CAMERA:
                    startCropImageActivity(mImageUri);
                    break;

                case IMAGE_PICKER:
                    if (data != null) {
                        // Get the URI of the selected file
                        final Uri uri = data.getData();
                        try {
                            mImageUri = uri;
                            startCropImageActivity(uri);
                        } catch (Exception e) {
                        }
                    }
                    break;
            }
        }
    }

    private void setImageAndStore(Uri uri) {
        mImageUri = uri;
        ivPreview.setImageURI(mImageUri);
//        Bitmap photo = FileUtils.uriToBitmap(ImageMaster.this, mImageUri);
//
//        String encodedImage = FileUtils.convertImageBitmapToString(photo);
//        AppUser.setProfileImage(getApplicationContext(), encodedImage);
    }

    private void startCropImageActivity(Uri imageUri) {
        if(imageUri!=null) {
            CropImage.activity(imageUri)
                    .setAspectRatio(1, 1)
                    .setFixAspectRatio(true)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this);
        }else {
            Toast.makeText(ImageMaster.this, "Please Reselect image ", Toast.LENGTH_SHORT).show();
        }
    }
}
