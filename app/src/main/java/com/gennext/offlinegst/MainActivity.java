package com.gennext.offlinegst;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.gennext.offlinegst.global.PopupAlert;
import com.gennext.offlinegst.global.PopupDialog;
import com.gennext.offlinegst.global.RatingPopupAlert;
import com.gennext.offlinegst.global.VersionUpdateDialog;
import com.gennext.offlinegst.model.EXELMaker;
import com.gennext.offlinegst.setting.BackupSetting;
import com.gennext.offlinegst.setting.Const;
import com.gennext.offlinegst.user.Dashboard;
import com.gennext.offlinegst.user.company.AddProfile;
import com.gennext.offlinegst.user.company.UpdateProfile;
import com.gennext.offlinegst.util.AppConfig;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.L;
import com.gennext.offlinegst.util.LocaleHelper;
import com.gennext.offlinegst.util.Rating;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import java.io.File;
import java.io.IOException;

public class MainActivity extends NavDrawer implements PopupDialog.DialogTaskListener{

    private static final int PERMISSION_REQUEST_STORAGE = 1001;
    private Toolbar mToolbar;
    private DBManager dbManager;
    private ImageButton btnActionMenu,btnActionHome;
    private Context mContext;
    //    private String filename = "SampleFile.xls";
    private String filepath = "BackupInExel";
    private EXELMaker exelMaker;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    private String mFCMId;
    private Context mainActivity;
    private boolean isAppValidityCheck;
//    private File myExternalFile;

    @Override
    protected void onStart() {
        super.onStart();
        String profileId = AppUser.getProfileId(this);
        if (AppUser.getUserId(this).equals("")) {
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        }else if (profileId.equals("")) {
            startActivity(new Intent(MainActivity.this, ProfileActivity.class));
            finish();
        }else if(!LocaleHelper.getLanguage(this).equals(AppUser.getLanguage(this))){
            startActivity(new Intent(MainActivity.this,MainActivity.class));
            finish();
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        mainActivity=base;
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String profileId = AppUser.getProfileId(this);
        if (!AppUser.getUserId(this).equals("")&&!profileId.equals("")) {
            setContentView(R.layout.activity_main);
            mContext = this;
//            dbManager = DBManager.newIsntance(this);
            dbManager = DBManager.newIsntance(this,DBManager.PRODUCT_TABLE,true);
            exelMaker= EXELMaker.newInstance(this);
            mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
            mToolbar = setToolBar(getSt(R.string.app_name));
            btnActionMenu = (ImageButton) findViewById(R.id.btn_action_menu);
            btnActionHome = (ImageButton) findViewById(R.id.btn_action_home);
            SetDrawer(this, mToolbar);
            initUi();
//        mFCMId=AppUser.getFCMId(this);
            mFCMId= FirebaseInstanceId.getInstance().getToken();
            checkUpdateAvailability();
        }
    }


    private void initUi() {
        isAppValidityCheck=true;
        addFragmentWithoutBackstack(Dashboard.newInstance(), R.id.container, "dashboard");
    }

    public void checkAppValidity() {
        if(isAppValidityCheck) {
            String result = AppUser.getPackageStatus(MainActivity.this);
            String resultMessage = AppUser.getPackageMessage(MainActivity.this);
            if (!result.equals("") && !result.toLowerCase().equals(Const.PAID)) {
                isAppValidityCheck = false;
                PopupAlert.newInstance("Alert", resultMessage, PopupAlert.POPUP_DIALOG)
                        .show(getSupportFragmentManager(), "popupAlert");
            }
        }
    }

    public void requestStoragePermission() {
        // Permission has not been granted and must be requested.
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            PopupDialog.newInstance("Permission Request","Storage access is required to store the backup file.",1,MainActivity.this)
                    .show(getSupportFragmentManager(),"popupDialog");

        } else {
            showToast("Permission is not available. Requesting storage permission.");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_STORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        // BEGIN_INCLUDE(onRequestPermissionsResult)
        if (requestCode == PERMISSION_REQUEST_STORAGE) {
            // Request for camera permission.
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission has been granted. Start camera preview Activity.
                showToast("Storage permission was granted. Starting preview.");
                BackupSetting backupSetting= (BackupSetting) getSupportFragmentManager().findFragmentByTag("backupSetting");
                if(backupSetting!=null){
                    backupSetting.allowStoragePermission();
                }
            } else {
                // Permission request was denied.
                showToast("Storage permission request was denied.");
            }
        }
        // END_INCLUDE(onRequestPermissionsResult)
    }


    @Override
    public void onDialogOkClick(DialogFragment dialog, int task) {
        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                PERMISSION_REQUEST_STORAGE);
    }

    @Override
    public void onDialogCancelClick(DialogFragment dialog, int task) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(dbManager!=null) {
            dbManager.closeDB();
            deleteFiles();
        }
    }

    public void updateTitle(String title) {
        btnActionMenu.setVisibility(View.GONE);
        if(title.equals("Dashboard")){
            btnActionHome.setVisibility(View.GONE);
        }else{
            btnActionHome.setVisibility(View.VISIBLE);
        }
        mToolbar.setTitle(title);
        btnActionHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void updateTitleWithMenu(String title, final int type) {
        mToolbar.setTitle(title);
        btnActionMenu.setVisibility(View.GONE);
//        btnActionMenu.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showPopup(btnActionMenu, type);
//            }
//        });
        if(title.equals("Dashboard")){
            btnActionHome.setVisibility(View.GONE);
        }else{
            btnActionHome.setVisibility(View.VISIBLE);
        }
        btnActionHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void showPopup(ImageButton more, int type) {
        final PopupMenu popup = new PopupMenu(mContext, more);
        switch (type) {
            case Const.MENU_PRODUCT:
                popup.getMenuInflater().inflate(R.menu.product_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        int i = item.getItemId();
                        if (i == R.id.product_export_send) {

//                            Product product= (Product) getSupportFragmentManager().findFragmentByTag("product");
//                            if(product!=null){
//                                product.exportInExel();
//                            }
                            return true;
                        } else if (i == R.id.product_export_open) {
//                            File mOutputFile = exelMaker.exportProductInExel(dbManager);
//                            if (mOutputFile != null) {
//                                Intent intent = new Intent(Intent.ACTION_VIEW);
//                                Uri uri = getUriFromFile(mOutputFile);
//                                intent.setDataAndType(uri, "application/vnd.ms-excel");
//                                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                                try {
//                                    startActivity(intent);
//                                } catch (ActivityNotFoundException e) {
//                                    Toast.makeText(MainActivity.this, "No application found to open Excel file.", Toast.LENGTH_SHORT).show();
//                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.android.apps.docs.editors.sheets")));
//                                }
//                            }
                            return true;
                        } else {
                            return onMenuItemClick(item);
                        }
                    }
                });
                popup.show();
                break;
            case Const.MENU_PURCHASES:
                popup.getMenuInflater().inflate(R.menu.product_menu2, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        int i = item.getItemId();
                        if (i == R.id.product_open_format1) {
                            File mOutputFile = exelMaker.exportPurchasesInExelFormat1(dbManager);
                            if (mOutputFile != null) {
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                Uri uri = getUriFromFile(mOutputFile);
                                intent.setDataAndType(uri, "application/vnd.ms-excel");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                try {
                                    startActivity(intent);
                                } catch (ActivityNotFoundException e) {
                                    Toast.makeText(MainActivity.this, "No application found to open Excel file.", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.android.apps.docs.editors.sheets")));
                                }
                            }
                            return true;
                        }else  if (i == R.id.product_export_format1) {
                            File mOutputFile = exelMaker.exportPurchasesInExelFormat1(dbManager);
                            if(mOutputFile!=null){
                                Uri uri =getUriFromFile(mOutputFile);
                                Intent in = new Intent(Intent.ACTION_SEND);
                                in.setDataAndType(uri, "application/vnd.ms-excel");
                                in.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                in.putExtra(Intent.EXTRA_STREAM, uri);
                                try {
                                    startActivity(Intent.createChooser(in, "Share With Fiends"));
                                } catch (Exception e) {
                                    Toast.makeText(MainActivity.this, "Error: Cannot open or share created PDF report.", Toast.LENGTH_SHORT).show();
                                }

                            }
                            return true;
                        } else if (i == R.id.product_open_format2) {
                            File mOutputFile = exelMaker.exportPurchasesInExelFormat2(dbManager);
                            if (mOutputFile != null) {
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                Uri uri = getUriFromFile(mOutputFile);
                                intent.setDataAndType(uri, "application/vnd.ms-excel");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                try {
                                    startActivity(intent);
                                } catch (ActivityNotFoundException e) {
                                    Toast.makeText(MainActivity.this, "No application found to open Excel file.", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.android.apps.docs.editors.sheets")));
                                }
                            }
                            return true;
                        }  else if (i == R.id.product_export_format2) {
                            File mOutputFile = exelMaker.exportPurchasesInExelFormat2(dbManager);
                            if(mOutputFile!=null){
                                Uri uri =getUriFromFile(mOutputFile);
                                Intent in = new Intent(Intent.ACTION_SEND);
                                in.setDataAndType(uri, "application/vnd.ms-excel");
                                in.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                in.putExtra(Intent.EXTRA_STREAM, uri);
                                try {
                                    startActivity(Intent.createChooser(in, "Share With Fiends"));
                                } catch (Exception e) {
                                    Toast.makeText(MainActivity.this, "Error: Cannot open or share created PDF report.", Toast.LENGTH_SHORT).show();
                                }

                            }
                            return true;
                        } else {
                            return onMenuItemClick(item);
                        }
                    }
                });
                popup.show();
                break;
            case Const.MENU_INVOICES:
                popup.getMenuInflater().inflate(R.menu.product_menu2, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        int i = item.getItemId();
                        if (i == R.id.product_open_format1) {
                            File mOutputFile = exelMaker.exportInvoicesInExelFormat1(dbManager);
                            if (mOutputFile != null) {
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                Uri uri = getUriFromFile(mOutputFile);
                                intent.setDataAndType(uri, "application/vnd.ms-excel");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                try {
                                    startActivity(intent);
                                } catch (ActivityNotFoundException e) {
                                    Toast.makeText(MainActivity.this, "No application found to open Excel file.", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.android.apps.docs.editors.sheets")));
                                }
                            }
                            return true;
                        }else  if (i == R.id.product_export_format1) {
                            File mOutputFile = exelMaker.exportInvoicesInExelFormat1(dbManager);
                            if(mOutputFile!=null){
                                Uri uri =getUriFromFile(mOutputFile);
                                Intent in = new Intent(Intent.ACTION_SEND);
                                in.setDataAndType(uri, "application/vnd.ms-excel");
                                in.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                in.putExtra(Intent.EXTRA_STREAM, uri);
                                try {
                                    startActivity(Intent.createChooser(in, "Share With Fiends"));
                                } catch (Exception e) {
                                    Toast.makeText(MainActivity.this, "Error: Cannot open or share created PDF report.", Toast.LENGTH_SHORT).show();
                                }

                            }
                            return true;
                        } else if (i == R.id.product_open_format2) {
                            File mOutputFile = exelMaker.exportInvoicesInExelFormat2(dbManager);
                            if (mOutputFile != null) {
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                Uri uri = getUriFromFile(mOutputFile);
                                intent.setDataAndType(uri, "application/vnd.ms-excel");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                try {
                                    startActivity(intent);
                                } catch (ActivityNotFoundException e) {
                                    Toast.makeText(MainActivity.this, "No application found to open Excel file.", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.android.apps.docs.editors.sheets")));
                                }
                            }
                            return true;
                        }  else if (i == R.id.product_export_format2) {
                            File mOutputFile = exelMaker.exportInvoicesInExelFormat2(dbManager);
                            if(mOutputFile!=null){
                                Uri uri =getUriFromFile(mOutputFile);
                                Intent in = new Intent(Intent.ACTION_SEND);
                                in.setDataAndType(uri, "application/vnd.ms-excel");
                                in.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                in.putExtra(Intent.EXTRA_STREAM, uri);
                                try {
                                    startActivity(Intent.createChooser(in, "Share With Fiends"));
                                } catch (Exception e) {
                                    Toast.makeText(MainActivity.this, "Error: Cannot open or share created PDF report.", Toast.LENGTH_SHORT).show();
                                }

                            }
                            return true;
                        } else {
                            return onMenuItemClick(item);
                        }
                    }
                });
                popup.show();
                break;

        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == ImageMaster.REQUEST_PROFILE) {
            AddProfile addProfile= (AddProfile) getSupportFragmentManager().findFragmentByTag("addProfile");
            if(addProfile!=null){
                addProfile.OnActivityResult(requestCode, resultCode, data);
            }
            UpdateProfile updateProfile= (UpdateProfile) getSupportFragmentManager().findFragmentByTag("updateProfile");
            if(updateProfile!=null){
                updateProfile.OnActivityResult(requestCode, resultCode, data);
            }

        } else {
            L.m("ERROR-103 Image not found");
        }
    }

    private Uri getUriFromFile(File docOutput) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            return FileProvider.getUriForFile(MainActivity.this, getPackageName() + ".provider", docOutput);
        } else {
            return Uri.fromFile(docOutput);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Dashboard dashboard= (Dashboard) getSupportFragmentManager().findFragmentByTag("dashboard");
        if(dashboard!=null){
            dashboard.updateUi();
        }
    }

    @Override
    public void onBackPressed() {
        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();

        if(backStackEntryCount==1){
            updateTitle(getString(R.string.dashboard));
            Dashboard dashboard= (Dashboard) getSupportFragmentManager().findFragmentByTag("dashboard");
            if(dashboard!=null){
                dashboard.updateUi();
            }
        }
        if(backStackEntryCount > 0){
            getSupportFragmentManager().popBackStack();
            if(backStackEntryCount==1) {
                disableDrawerSelector();
            }
        }else{
//            PopupDialog.newInstance("Exit Alert!","Are you sure to exit now ?",this).show(getSupportFragmentManager(),"popupDialog");
            super.onBackPressed();
        }
    }



    public void deleteFiles() {
        File dir1 = getExternalFilesDir(AppConfig.FOLDER_INVOICES);
        File dir2 = getExternalFilesDir(AppConfig.FOLDER_XLS);

        if (dir1!=null&&dir1.exists()) {
            String deleteCmd = "rm -r " + dir1;
            Runtime runtime = Runtime.getRuntime();
            try {
                runtime.exec(deleteCmd);
            } catch (IOException ignored) { }
        }
        if (dir2!=null&&dir2.exists()) {
            String deleteCmd = "rm -r " + dir2;
            Runtime runtime = Runtime.getRuntime();
            try {
                runtime.exec(deleteCmd);
            } catch (IOException ignored) { }
        }

    }

    private void checkUpdateAvailability() {
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);
//        long cacheExpiration = 3600; // 1 hour in seconds.
        long cacheExpiration = 0; // 1 hour in seconds.
        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }
        mFirebaseRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            // After config data is successfully fetched, it must be activated before newly fetched
                            // values are returned.
                            mFirebaseRemoteConfig.activateFetched();
                        }
                        final int serverVersionCode;
                        serverVersionCode=parseInt(mFirebaseRemoteConfig.getString(Const.VERSION_CODE));

                        String isRatingShow=mFirebaseRemoteConfig.getString(Const.RATING_STATUS);
                        String verName=mFirebaseRemoteConfig.getString(Const.VERSION_NAME);
                        String releaseDate=mFirebaseRemoteConfig.getString(Const.RELEASE_DATE);
                        String whatsNew=mFirebaseRemoteConfig.getString(Const.WHATS_NEW);
                        String verUpdateMessage=mFirebaseRemoteConfig.getString(Const.VERSION_UPDATE_MESSAGE);
                        int currentVersion=BuildConfig.VERSION_CODE;
                        if(serverVersionCode>currentVersion){
                            VersionUpdateDialog.newInstance(verUpdateMessage,verName+"\n"+releaseDate,whatsNew).show(getSupportFragmentManager(),"versionUpdateDialog");
                        }
                        if(isRatingShow.equals("true")){
                            setAppRating();
                        }
                    }
                });
    }

    private void setAppRating() {
        if (Rating.showRating(getApplicationContext())) {
            addFragment(new RatingPopupAlert(), "ratingPopupAlert");
        }
    }


    public void openInvoicesReport() {
//        if (AppUser.getCompanyId(MainActivity.this).equals("")) {
//            showToast("App needs to setup company detail");
//            addFragment(CompanyDetail.newInstance(), "companyDetail");
//        } else {
//            addFragment(Invoices.newInstance(), R.id.container, "invoices");
//        }
    }

    public void openPaymentReport() {
//        addFragment(Purchases.newInstance(), R.id.container, "purchases");
    }

    public void updateLanguage() {

    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void printJob(PrintDocumentAdapter pda) {
        PrintManager printManager = (PrintManager) mainActivity.getSystemService(Context.PRINT_SERVICE);
        String jobName = getString(R.string.app_name) + " Document";
        printManager.print(jobName, pda, null);
    }

    public void updateNavCompanyList() {
        updateCompanyProfileInDrawer(MainActivity.this);
    }


    //    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        if (requestCode != MaterialBarcodeScanner.RC_HANDLE_CAMERA_PERM) {
//            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//            return;
//        }
//        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
////            AddProduct fragment= (AddProduct) getSupportFragmentManager().findFragmentByTag("addProduct");
////            if(fragment!=null) {
////                fragment.startScan();
////            }
////            PurchaseManager fragment2= (PurchaseManager) getSupportFragmentManager().findFragmentByTag("purchaseManager");
////            if(fragment2!=null) {
////                fragment2.startScan();
////            }
//            return;
//        }
//        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.cancel();
//            }
//        };
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle("Error")
//                .setMessage(R.string.no_camera_permission)
//                .setPositiveButton(android.R.string.ok, listener)
//                .show();
//    }
}
