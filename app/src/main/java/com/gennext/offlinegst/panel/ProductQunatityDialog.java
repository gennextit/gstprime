package com.gennext.offlinegst.panel;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.model.user.ProductModel;
import com.gennext.offlinegst.util.FieldValidation;
import com.gennext.offlinegst.util.Utility;


public class ProductQunatityDialog extends DialogFragment {
    private Dialog dialog;
    private QunatityListener mListener;
    private ProductModel itemProduct;
    private int position;


    public interface QunatityListener {
        void onQunatityChange(DialogFragment dialog, ProductModel itemProduct, int position);
    }

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static ProductQunatityDialog newInstance(QunatityListener listener, ProductModel itemProduct, int position) {
        ProductQunatityDialog fragment = new ProductQunatityDialog();
        fragment.mListener = listener;
        fragment.itemProduct = itemProduct;
        fragment.position = position;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_change_quantity, null);
        dialogBuilder.setView(v);
        final EditText etQuantity = (EditText) v.findViewById(R.id.et_product_quantity);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);

        etQuantity.setText(itemProduct.getQuantity());
        etQuantity.setSelection(itemProduct.getQuantity().length());

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                if (mListener != null) {
                    if(!FieldValidation.isEmpty(getContext(),etQuantity)){
                        return;
                    }
                    itemProduct.setQuantity(etQuantity.getText().toString());
                    itemProduct.setInitQuantity(Utility.parseFloat(etQuantity.getText().toString()));
                    mListener.onQunatityChange(ProductQunatityDialog.this,itemProduct,position);
                }

            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }
}
