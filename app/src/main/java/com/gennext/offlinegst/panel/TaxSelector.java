package com.gennext.offlinegst.panel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gennext.offlinegst.R;
import com.gennext.offlinegst.model.user.CommonModel;
import com.gennext.offlinegst.model.user.CommonSelectorAdapter;
import com.gennext.offlinegst.util.ApiCall;
import com.gennext.offlinegst.util.AppUser;
import com.gennext.offlinegst.util.DBManager;
import com.gennext.offlinegst.util.JsonParser;

import java.util.ArrayList;


public class TaxSelector extends BottomSheetDialogFragment{
//    private static final String TAX_TYPE_FILE_NAME = "global/taxType.json";
    private SelectListener mListener;
    private CommonSelectorAdapter adapter;

    public void onSelectedItem(CommonModel item) {
        dismiss();
        if(mListener!=null){
            mListener.onTaxSelect(TaxSelector.this,item);
        }
    }

    public interface SelectListener {
        void onTaxSelect(DialogFragment dialog, CommonModel selectedItem);

    }


    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }


    public static TaxSelector newInstance(SelectListener listener) {
        TaxSelector fragment = new TaxSelector();
        fragment.mListener = listener;
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_menu_transpoart, container, false);
        RecyclerView lvMain = (RecyclerView) v.findViewById(R.id.lv_main);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_search);
        tvTitle.setText(getString(R.string.select_tax));
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());

//        ArrayList<CommonModel> cModel = JsonParser.parseTranspoartMode(ApiCall.getAssets(getContext(),TAX_TYPE_FILE_NAME));
        ArrayList<CommonModel> cModel = JsonParser.parseTax(AppUser.getTaxDetails(getContext()));
        if (cModel!=null) {
            adapter = new CommonSelectorAdapter(getActivity(), cModel,TaxSelector.this);
            lvMain.setAdapter(adapter);
        }

        return v;
    }


}
